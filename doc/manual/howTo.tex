\chapter{How to}
\minitoc
This chapter gives the main guidelines to add different functionnality to the yalla library.

\section{Plug a new data structure}
Adding a new data structure in yalla is quite straightforward. \\
First one has to implement the data structure, sequential or distributed. \\
Then, one has to implement the rows and columns iterators. \\
The rows iterator has to be implemented in a way such that:
\begin{itemize}
\item The increment operator (operator++) allows to move from one row to the next one
\item The comparison operator (operator==, and/or operator!=) allows to compare the position of two rows iterator
\item The iterator is able to give the local/global index of the row
\end{itemize}
The columns iterator has to be implemented in a way such that:
\begin{itemize}
\item It takes a rows iterator as an input
\item The increment operator (operator++) allows to move from one column of a row to the next one
\item The comparison operator (operator==, and/or operator!=) allows to compare the position of two columns iterator
\item The iterator is able to give the local/global index of the column
\end{itemize}

Once the data structure and the iterators have been implemented, the data structures need to implement several methods. \\
In the case of a sequential matrix:
\begin{itemize}
\item rowBegin: this method should return an iterator on the first element of the first row
\item rowEnd: this method should return an iterator on the first element of the last row
\item colBegin: this method should return an iterator on the first element of the given row
\item colEnd: this method should return an iterator on the last element of the given row
\end{itemize}
In the case of a distributed matrix:
\begin{itemize}
\item rowBegin: this method should return an iterator on the first element of the first non-diagonal row
\item rowEnd: this method should return an iterator on the last element of the first non-diagonal row
\item colBegin: this method should return an iterator on the first non-diagonal element of the given row
\item colEnd: this method should return an iterator on the last non-diagonal element of the given row
\item ownRowBegin: this method should return an iterator on the first element of the first diagonal row
\item ownRowEnd: this method should return an iterator on the last element of the first diagonal row
\item ownColBegin: this method should return an iterator on the first diagonal element of the given row
\item ownColEnd: this method should return an iterator on the last diagonal element of the given row
\end{itemize}
All those iterators should exist both in their const and non const version. \\
Now the newly implemented data structure can be used with most algorithms in yalla.

For a detailed explanation on the diagonal and non-diagonal block, please refer to the descritpion of the distributed CSR matrices in the Data structures chapter.

\section{Implement a generic method/algorithm}
To add a fully generic method or algorithm, one just has to implement the method without any template specialization and using only iterators. Here is the example for a generic gemv:
\begin{lstlisting}[escapechar=\@]
template<class @{\color{blue}DataType}@, class @{\color{blue}MatrixType}@>
void gemv(const @{\color{blue}MatrixType}@& _A, const SeqVector<@{\color{blue}DataType}@>& _x, SeqVector<@{\color{blue}DataType}@>& _y)
{
  typedef typename MatrixType::impl_type::const_row_iterator_type rowIt;
  typedef typename MatrixType::impl_type::const_col_iterator_type colIt;
  for(rowIt row=_A.rowBegin(); row != _A.rowEnd();++row)
  {
    const int rowIndex = row.index();
    for(colIt col=_A.colBegin(row);col!=_A.colEnd(row);++col)
      _y(rowIndex) += *col * _x(col.index());
  }
}
\end{lstlisting}
This method will run properly or any kind of matrix (as long as the iterators are properly implemented) and SeqVector of any data type.

\section{Implement a method/algorithm for a specific type of matrix}
To add a method or algorithm that is specialized for a family of matrix, e.g. any sequential/distributed dense matrix or any sequential/distributed sparse matrix, one has to use template specialization as well as iterators. Here is the example for a partly specialized gemv:
\begin{lstlisting}[escapechar=\@]
template<class @{\color{blue}DataType}@, class @{\color{blue}MatrixImpl}@>
void gemv(const @{\color{red}DistDenseMatrix}@<@{\color{blue}DataType}@, @{\color{blue}MatrixImpl}@<@{\color{blue}DataType}@> >& _A, const DistVector<@{\color{blue}DataType}@>& _x, DistVector<@{\color{blue}DataType}@>& _y)
{
// DO GENERIC DENSE GEMV WITH ITERATORS
}

template<class @{\color{blue}DataType}@, class @{\color{blue}MatrixImpl@}>
void gemv(const @{\color{red}DistSparseMatrix}@<@{\color{blue}DataType}@, @{\color{blue}MatrixImpl}@<@{\color{blue}DataType}@> >& _A, const DistVector<@{\color{blue}DataType}@>& _x, DistVector<@{\color{blue}DataType}@>& _y)
{
// DO GENERIC SPARSE GEMV WITH ITERATORS
}
\end{lstlisting}
The first gemv will be used for any kind of distributed dense matrix and DistVector of any data type, whereas the second one will be used for any kind of distributed sparse matrix and DistVector of any data type, provided iterators are implemented properly.

\section{Implement a method/algorithm for a specific data structure}
To add a method or algorithm that is specialized for a particular data structure, e.g. a sequential dense row major matrix or a distributed CSR matrix, one has to fully specialize a method. Here is the example for a fully specialized gemv:
\begin{lstlisting}[escapechar=\@]
void gemv(const @{\color{red}DistSparseMatrix}@<@{\color{red}float}@, @{\color{red}DistCSRMKLMatrix}@<@{\color{red}float}@> >& _A, const DistVector<@{\color{red}float}@>& _x, DistVector<@{\color{red}float}@>& _y)
{
// DO SPECIFIC CSR GEMV
}
\end{lstlisting}
This method will run properly only for distributed CSR matrix of type float. As the method is specific to a particular data structure, the use of iterators here is irrelevant, and therefore iterators might or might not be used. \\
It is to note that the data type can still be a template parameter for a specific data structure method/algorithm.

\section{Add a unitary test}
It is supposed here that the yalla library is used with the google test framework, as it makes the implementation of tests really straightforward. \\
To add a unitary test, it is advised to look in the src/test/unitary directory how test are ``implemented'', as well as to check the gtest documentation. \\
The global idea is to put and register the test inside a TEST macro provided by gtest. The parameters of this macro are the test case name et the test name. \\
In the body of this macro, one will just test the code using the assertions provided by gtest. \\
Finally, to register the test, one just have to include the test file in the unitary\_test\_main.cpp to be added in the unitary test database.

\section{Add a performance test}
It is supposed here that the yalla library is used with the google test framework, as it makes the implementation of tests really straightforward. \\
To add a performance test, it is advised to look in the src/test/perfs directory how test are ``implemented'', as well as to check the gtest documentation. \\
Adding a performance test is a little bit more complicated than a unitary test. One needs to differentiate the set up of the case with what need to be measured. \\
The test case is set up in the SetUpTestCase method, using static variables. Everything that need to be done as a preparation of the test has to be done in this method. \\
The actual kernel that one wants to benchmark is in the TEST\_F macro. Everything that will be done in this macro will be benchmarked and logged. \\
Finally, to register the test, one just have to include the test file in the PerfTestMain.cpp to be added in the perf test database.
