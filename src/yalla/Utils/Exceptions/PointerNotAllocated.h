#pragma once
#ifndef yalla_pointer_not_allocated_h
#define yalla_pointer_not_allocated_h

/*!
 *  \file PointerNotAllocated.h
 *  \brief Throw an PtrNotAllocated exception
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdexcept>
#include <string>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
//#include "yalla/Utils/stackTrace/StackTrace.h"

YALLA_USING_NAMESPACE(yalla)


YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace PointerNotAllocated
 *  \brief Throw a PtrNotAllocated exception
 */
YALLA_BEGIN_NAMESPACE(PointerNotAllocated)
/*! \brief Check if a pointer has been allocated
 *  
 *  \param[in] _ptr The pointer to check
 *
 *  \return 1 if the pointer is not allocated
 */
static unsigned int Check(const void* _ptr)
{
	if(_ptr==0)
		return 1;
	return 0;
}
YALLA_END_NAMESPACE

/*!
 *  \class PtrNotAllocated
 *  \brief Throw a PtrNotAllocated exception
 * 
 *  \details
 *  Generate an error message containing the file, the line and the callstack leading to this exception
 */
class PtrNotAllocated : virtual std::exception
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef PtrNotAllocated this_type;
	//! \brief Type of the super class
	typedef std::exception super_type;
	//@}
 private:
	//! \brief The error message
	std::string m_error_message;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class PtrNotAllocated
	 */
	//@{

	/*! 
	 *  \brief Generates the error message
	 *
	 *  The error message includes:
	 *  - The file throwing the exception
	 *  - The line throwing the exception
	 *  - The callstack
	 *
	 *  \param[in] _file The file throwing the exception
	 *  \param[in] _line The line throwing the exception
	 */
	PtrNotAllocated(const char* _file, const int _line);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class PtrNotAllocated
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~PtrNotAllocated() throw();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class PtrNotAllocated. \n
	 *  These methods give access in read-only mode to the datas/information of the class PtrNotAllocated
	 */
	//@{
	/*!
	 *  \brief Return the error message
	 *
	 *  \return The error message
	 */
	const std::string what();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "PtrNotAllocated" The name of the class
	 */
	const char* getClassName() const
	{
		return "PtrNotAllocated";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
