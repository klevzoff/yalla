#pragma once
#ifndef yalla_file_not_found_h
#define yalla_file_not_found_h

/*!
 *  \file FileNotFound.h
 *  \brief Throw an SpecifiedFileNotFound exception
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdexcept>
#include <string>
#include <sstream>
#include <fstream>
#include "yalla/Utils/Types/TypesAndDef.h"
//#include "yalla/Utils/stackTrace/StackTrace.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace FileNotFound
 *  \brief Check if a filename is found
 */
YALLA_BEGIN_NAMESPACE(FileNotFound)
/*! \brief Check if a filename is found
 *  
 *  \param[in] _file The file to check
 *
 *  \return 1 if the file is not found
 */
static unsigned int Check(const std::ifstream& _file)
{
	if(!_file)
		return 1;
	return 0;
}
YALLA_END_NAMESPACE

/*!
 *  \class SpecifiedFileNotFound
 *  \brief Throw an SpecifiedFileNotFound exceptionGenere une exception de type SpecifiedFileNotFound
 * 
 *  \details
 *  Generate an error message containing the file, the line and the callstack leading to this exception
 */
class SpecifiedFileNotFound : virtual std::exception
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef SpecifiedFileNotFound this_type;
	//! \brief Type of the super class
	typedef std::exception super_type;
	//@}
 private:
	//! \brief The error message
	std::string m_error_message;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SpecifiedFileNotFound
	 */
	//@{

	/*! 
	 *  \brief Generates the error message
	 *
	 *  The error message includes:
	 *  - The file throwing the exception
	 *  - The line throwing the exception
	 *  - The callstack
	 *
	 *  \param[in] _file The file throwing the exception
	 *  \param[in] _line The line throwing the exception
	 */
	SpecifiedFileNotFound(const char* _file, const int _line);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SpecifiedFileNotFound
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~SpecifiedFileNotFound() throw();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SpecifiedFileNotFound. \n
	 *  These methods give access in read-only mode to the datas/information of the class SpecifiedFileNotFound
	 */
	//@{
	/*!
	 *  \brief Return the error message
	 *
	 *  \return The error message
	 */
	const std::string what();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "isOutOfRange" The name of the class
	 */
	const char* getClassName() const
	{
		return "SpecifiedFileNotFound";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
