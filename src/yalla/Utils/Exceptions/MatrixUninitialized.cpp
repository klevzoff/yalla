/*!
*  \file MatrixUninitialized.cpp
*  \brief Implementation of the MatrixUninitialized exception
*  \date 12/27/2012
*  \author Xavier TUNC
*  \version 1.0
*/

#include "yalla/Utils/Exceptions/MatrixUninitialized.h"

MatrixUninitialized::
MatrixUninitialized(const char* _file, const int _line)
{
  std::string msg = "Error : Matrix is not initialized.\nDeclared in file : ";
  msg.append(_file);
  msg.append("\nAt line : ");
  std::ostringstream oss;
  oss << _line;
  msg.append(oss.str());
  m_error_message = msg;
}

MatrixUninitialized::
~MatrixUninitialized() throw()
{
  ;
}

const std::string
MatrixUninitialized::
what()
{
  return m_error_message;
}
