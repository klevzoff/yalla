/*!
*  \file FileNotFound.cpp
*  \brief Implementation of the SpecifiedFileNotFound exception
*  \date 12/27/2012
*  \author Xavier TUNC
*  \version 1.0
*/

#include "yalla/Utils/Exceptions/FileNotFound.h"

SpecifiedFileNotFound::
SpecifiedFileNotFound(const char* _file, const int _line)
{
  std::string msg = "Error : Specified file not found.\nDeclared in file : ";
  msg.append(_file);
  msg.append("\nAt line : ");
  std::ostringstream oss;
  oss << _line;
  msg.append(oss.str());
  m_error_message = msg;
}

SpecifiedFileNotFound::
~SpecifiedFileNotFound() throw()
{
  ;
}

const std::string
SpecifiedFileNotFound::
what()
{
  return m_error_message;
}
