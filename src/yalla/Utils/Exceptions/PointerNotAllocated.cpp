/*!
 *  \file PointerNotAllocated.cpp
 *  \brief Implementation of the PointerNotAllocated exception
 *  \date 12/27/2012
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Exceptions/PointerNotAllocated.h"

PtrNotAllocated::
PtrNotAllocated(const char* _file, const int _line)
{
  std::string msg = "Error : Accessing a not allocating pointer.\nDeclared in file : ";
  msg.append(_file);
  msg.append("\nAt line : ");
  std::ostringstream oss;
  oss << _line;
  msg.append(oss.str());
  m_error_message = msg;
}

PtrNotAllocated::
~PtrNotAllocated() throw()
{
  ;
}

const std::string
PtrNotAllocated::
what()
{
  return m_error_message;
}
