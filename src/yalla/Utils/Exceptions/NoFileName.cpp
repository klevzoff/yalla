/*!
 *  \file NoFileName.cpp
 *  \brief Implementation of the NoFileNameSpecified exception
 *  \date 12/27/2012
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Exceptions/NoFileName.h"

NoFileNameSpecified::
NoFileNameSpecified(const char* _file, const int _line)
{
  std::string msg = "Error : No filename specified.\nDeclared in file : ";
  msg.append(_file);
  msg.append("\nAt line : ");
  std::ostringstream oss;
  oss << _line;
  msg.append(oss.str());
  m_error_message = msg;
}

NoFileNameSpecified::~
NoFileNameSpecified() throw()
{
  ;
}

const std::string
NoFileNameSpecified::
what()
{
  return m_error_message;
}
