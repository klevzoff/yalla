#pragma once
#ifndef yalla_no_file_name_h
#define yalla_no_file_name_h

/*!
 *  \file NoFileName.h
 *  \brief Throw an NoFileNameSpecified exception
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdexcept>
#include <string>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
//#include "yalla/Utils/stackTrace/StackTrace.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace NoFileName
 *  \brief Check if a file name has been provided
 */
YALLA_BEGIN_NAMESPACE(NoFileName)
/*! \brief Check if a file name has been provided
 *  
 *  \param[in] _filename Filename to test
 *
 *  \return 1 If the filename is null
 */
static unsigned int Check(const std::string _filename)
{
	if(_filename=="")
		return 1;
	return 0;
}
YALLA_END_NAMESPACE

/*!
 *  \class NoFileNameSpecified
 *  \brief Throw an NoFileNameSpecified exception
 * 
 *  \details
 *  Generate an error message containing the file, the line and the callstack leading to this exception
 */
class NoFileNameSpecified : virtual std::exception
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef NoFileNameSpecified this_type;
	//! \brief Type of the super class
	typedef std::exception super_type;
	//@}
 private:
	//! \brief The error message
	std::string m_error_message;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class NoFileNameSpecified
	 */
	//@{

	/*! 
	 *  \brief Generates the error message
	 *
	 *  The error message includes:
	 *  - The file throwing the exception
	 *  - The line throwing the exception
	 *  - The callstack
	 *
	 *  \param[in] _file The file throwing the exception
	 *  \param[in] _line The line throwing the exception
	 */
	NoFileNameSpecified(const char* _file, const int _line);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class NoFileNameSpecified
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~NoFileNameSpecified() throw();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class NoFileNameSpecified. \n
	 *  These methods give access in read-only mode to the datas/information of the class NoFileNameSpecified
	 */
	//@{
	/*!
	 *  \brief Return the error message
	 *
	 *  \return The error message
	 */
	const std::string what();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "NoFileNameSpecified" The name of the class
	 */
	const char* getClassName() const
	{
		return "NoFileNameSpecified";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
