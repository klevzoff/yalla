#pragma once
#ifndef yalla_check_bounds_h
#define yalla_check_bounds_h

/*!
 *  \file CheckBounds.h
 *  \brief Throw an isOutOfRange exception
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdexcept>
#include <string>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
//#include "yalla/Utils/stackTrace/StackTrace.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace CheckBounds
 *  \brief Check if an index is between some upper and lower bounds
 */
YALLA_BEGIN_NAMESPACE(CheckBounds)
/*! \brief Check if an index is between some upper and lower bounds
 *  
 *  \param[in] _index The index to check
 *  \param[in] _lowerBound The lower bound
 *  \param[in] _upperBound The upper bound
 *
 *  \return 1 if the index is out of bound
 */
static unsigned int Check(const int _index, const int _lowerBound, const int _upperBound)
{
	if(_lowerBound <= _index && _index < _upperBound)
		return 0;
	return 1;
}
YALLA_END_NAMESPACE

/*!
 *  \class isOutOfRange
 *  \brief Throw an isOutOfRange exception
 * 
 *  \details
 *  Generate an error message containing the file, the line and the callstack leading to this exception
 */
class isOutOfRange : virtual std::exception
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef isOutOfRange this_type;
	//! \brief Type of the super class
	typedef std::exception super_type;
	//@}
 private:
	//! \brief The error message
	std::string m_error_message;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class isOutOfRange
	 */
	//@{

	/*! 
	 *  \brief Generates the error message
	 *
	 *  The error message includes:
	 *  - The file throwing the exception
	 *  - The line throwing the exception
	 *  - The callstack
	 *
	 *  \param[in] _file The file throwing the exception
	 *  \param[in] _line The line throwing the exception
	 */
	isOutOfRange(const char* _file, const int _line);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class isOutOfRange
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~isOutOfRange() throw();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class isOutOfRange. \n
	 *  These methods give access in read-only mode to the datas/information of the class isOutOfRange
	 */
	//@{
	/*!
	 *  \brief Return the error message
	 *
	 *  \return The error message
	 */
	const std::string what();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "isOutOfRange" The name of the class
	 */
	const char* getClassName() const
	{
		return "isOutOfRange";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
