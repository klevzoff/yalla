#pragma once
#ifndef yalla_matrix_dimensions_incompatibles_h
#define yalla_matrix_dimensions_incompatibles_h

/*!
 *  \file MatrixDimensionsCompatibles.h
 *  \brief Throw an MatrixDimensionsIncompatibles exception
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdexcept>
#include <string>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
//#include "yalla/Utils/stackTrace/StackTrace.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace MatrixDimensionsCompatibles
 *  \brief Check if two matrices dimensions are compatibles
 */
YALLA_BEGIN_NAMESPACE(MatrixDimensionsCompatibles)
/*! \brief Check if the matrices dimensions are equals
 *  
 *  \param[in] _dim1 First dimension
 *  \param[in] _dim2 Second dimension
 *
 *  \return 1 If the matrices dimensions are compatibles
 */
static unsigned int Check(const unsigned int _dim1, const unsigned int _dim2)
{
	if(_dim1 == _dim2)
		return 1;
	return 0;
}
YALLA_END_NAMESPACE

/*!
 *  \class MatrixDimensionsIncompatibles
 *  \brief Throw an MatrixDimensionsIncompatibles exception
 * 
 *  \details
 *  Generate an error message containing the file, the line and the callstack leading to this exception
 */
class MatrixDimensionsIncompatibles : virtual std::exception
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef MatrixDimensionsIncompatibles this_type;
	//! \brief Type of the super class
	typedef std::exception super_type;
	//@}
 private:
	//! \brief The error message
	std::string m_error_message;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class MatrixDimensionsIncompatibles
	 */
	//@{

	/*! 
	 *  \brief Generates the error message
	 *
	 *  The error message includes:
	 *  - The file throwing the exception
	 *  - The line throwing the exception
	 *  - The callstack
	 *
	 *  \param[in] _file The file throwing the exception
	 *  \param[in] _line The line throwing the exception
	 */
	MatrixDimensionsIncompatibles(const char* _file, const int _line);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class MatrixDimensionsIncompatibles
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~MatrixDimensionsIncompatibles() throw();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class MatrixDimensionsIncompatibles. \n
	 *  These methods give access in read-only mode to the datas/information of the class MatrixDimensionsIncompatibles
	 */
	//@{
	/*!
	 *  \brief Return the error message
	 *
	 *  \return The error message
	 */
	const std::string what();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "MatrixDimensionsIncompatibles" The name of the class
	 */
	const char* getClassName() const
	{
		return "MatrixDimensionsIncompatibles";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
