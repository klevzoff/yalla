/*!
 *  \file FailedAllocation.cpp
 *  \brief Implementation of the AllocationFailed exception
 *  \date 12/27/2012
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Exceptions/FailedAllocation.h"

AllocationFailed::
AllocationFailed(const char* _file, const int _line)
{
  std::string msg = "Error : Pointeur not allocated, probably no ressources left. \nDeclared in file : ";
  msg.append(_file);
  msg.append("\nAt line : ");
  std::ostringstream oss;
  oss << _line;
  msg.append(oss.str());
  m_error_message = msg;
}

AllocationFailed::
~AllocationFailed() throw()
{
  ;
}

const std::string
AllocationFailed::
what()
{
  return m_error_message;
}
