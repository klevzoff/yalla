#pragma once
#ifndef yalla_vector_bidimensionnal_not_allowed_h
#define yalla_vector_bidimensionnal_not_allowed_h

/*!
 *  \file VectorBidimensionnalNotAllowed.h
 *  \brief Throw an VectorBidimensionnalNotAllowed exception
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdexcept>
#include <string>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
//#include "yalla/Utils/stackTrace/StackTrace.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace isVectorBidimensionnal
 *  \brief Check if at leat one dimension of a vector is one
 */
YALLA_BEGIN_NAMESPACE(isVectorBidimensionnal)
/*! \brief Check if at leat one dimension of a vector is one
 *  
 *  \param[in] _nbRows The number of rows
 *  \param[in] _nbCols The number of cols
 *
 *  \return 1 If rows and cols are not equal to one
 */
static unsigned int Check(const unsigned int _nbRows, const unsigned int _nbCols)
{
	if(_nbRows != 1 && _nbCols != 1)
		return 1;
	return 0;
}
YALLA_END_NAMESPACE

/*!
 *  \class VectorBidimensionnalNotAllowed
 *  \brief Throw an VectorBidimensionnalNotAllowed exception
 * 
 *  \details
 *  Generate an error message containing the file, the line and the callstack leading to this exception
 */
class VectorBidimensionnalNotAllowed : virtual std::exception
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef VectorBidimensionnalNotAllowed this_type;
	//! \brief Type of the super class
	typedef std::exception super_type;
	//@}
 private:
	//! \brief The error message
	std::string m_error_message;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class VectorBidimensionnalNotAllowed
	 */
	//@{

	/*! 
	 *  \brief Generates the error message
	 *
	 *  The error message includes:
	 *  - The file throwing the exception
	 *  - The line throwing the exception
	 *  - The callstack
	 *
	 *  \param[in] _file The file throwing the exception
	 *  \param[in] _line The line throwing the exception
	 */
	VectorBidimensionnalNotAllowed(const char* _file, const int _line);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class VectorBidimensionnalNotAllowed
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~VectorBidimensionnalNotAllowed() throw();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class VectorBidimensionnalNotAllowed. \n
	 *  These methods give access in read-only mode to the datas/information of the class VectorBidimensionnalNotAllowed
	 */
	//@{
	/*!
	 *  \brief Return the error message
	 *
	 *  \return The error message
	 */
	const std::string what();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "VectorBidimensionnalNotAllowed" The name of the class
	 */
	const char* getClassName() const
	{
		return "VectorBidimensionnalNotAllowed";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
