#pragma once
#ifndef yalla_failed_allocation_h
#define yalla_failed_allocation_h

/*!
 *  \file FailedAllocation.h
 *  \brief Throw an AllocationFailed exception
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdexcept>
#include <string>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
//#include "yalla/Utils/stackTrace/StackTrace.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace FailedAllocation
 *  \brief Throw an AllocationFailed exception
 */
YALLA_BEGIN_NAMESPACE(FailedAllocation)
/*! \brief Check if a pointer has been allocated
 *  
 *  \param[in] _ptr The pointer to check
 *
 *  \return 1 if the pointer is not allocated
 */
static unsigned int Check(const void * _ptr)
{
	if(_ptr==0)
		return 1;
	return 0;
}
YALLA_END_NAMESPACE

/*!
 *  \class AllocationFailed
 *  \brief Throw an AllocationFailed exception
 * 
 *  \details
 *  Generate an error message containing the file, the line and the callstack leading to this exception
 */
class AllocationFailed : virtual std::exception
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef AllocationFailed this_type;
	//! \brief Type of the super class
	typedef std::exception super_type;
	//@}
 private:
	//! \brief The error message
	std::string m_error_message;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class AllocationFailed
	 */
	//@{

	/*! 
	 *  \brief Generates the error message
	 *
	 *  The error message includes:
	 *  - The file throwing the exception
	 *  - The line throwing the exception
	 *  - The callstack
	 *
	 *  \param[in] _file The file throwing the exception
	 *  \param[in] _line The line throwing the exception
	 */
	AllocationFailed(const char* _file, const int _line);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class AllocationFailed
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~AllocationFailed() throw();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class AllocationFailed. \n
	 *  These methods give access in read-only mode to the datas/information of the class AllocationFailed
	 */
	//@{
	/*!
	 *  \brief Return the error message
	 *
	 *  \return The error message
	 */
	const std::string what();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "AllocationFailed" The name of the class
	 */
	const char* getClassName() const
	{
		return "AllocationFailed";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
