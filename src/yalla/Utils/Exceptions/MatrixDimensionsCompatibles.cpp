/*!
*  \file MatrixDimensionsCompatibles.cpp
*  \brief Implementation of the MatrixDimensionsIncompatibles exception
*  \date 12/27/2012
*  \author Xavier TUNC
*  \version 1.0
*/

#include "yalla/Utils/Exceptions/MatrixDimensionsCompatibles.h"

MatrixDimensionsIncompatibles::
MatrixDimensionsIncompatibles(const char* _file, const int _line)
{
  std::string msg = "Error : Matrices dimensions are incompatibles.\nDeclared in file : ";
  msg.append(_file);
  msg.append("\nAt line : ");
  std::ostringstream oss;
  oss << _line;
  msg.append(oss.str());
  m_error_message = msg;
}

MatrixDimensionsIncompatibles::
~MatrixDimensionsIncompatibles() throw()
{
  ;
}

const std::string
MatrixDimensionsIncompatibles::
what()
{
  return m_error_message;
}
