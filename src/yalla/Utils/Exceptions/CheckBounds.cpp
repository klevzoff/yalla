/*!
*  \file CheckBounds.cpp
*  \brief Implementation of the isOutOfRange exception
*  \date 12/27/2012
*  \author Xavier TUNC
*  \version 1.0
*/

#include "yalla/Utils/Exceptions/CheckBounds.h"

isOutOfRange::
isOutOfRange(const char* _file, const int _line)
{
  std::string msg = "Error : Index is out of bounds.\nDeclared in file : ";
  msg.append(_file);
  msg.append("\nAt line : ");
  std::ostringstream oss;
  oss << _line;
  msg.append(oss.str());
  m_error_message = msg;
}

isOutOfRange::
~isOutOfRange() throw()
{
  ;
}

const std::string
isOutOfRange::
what()
{
  return m_error_message;
}
