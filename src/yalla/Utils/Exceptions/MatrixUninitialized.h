#pragma once
#ifndef yalla_matrix_uninitialized_h
#define yalla_matrix_uninitialized_h

/*!
 *  \file MatrixUninitialized.h
 *  \brief Throw a MatrixUninitialized exception
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdexcept>
#include <string>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
//#include "yalla/Utils/stackTrace/StackTrace.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace CheckUninitialization
 *  \brief Check if given dimensions are not zeros
 */
YALLA_BEGIN_NAMESPACE(CheckUninitialization)
/*! \brief Check if given dimensions are not zeros
 *  
 *  \param[in] _dim1 First dimension
 *  \param[in] _dim2 Second dimension
 *
 *  \return 1 if at least one dimension is null
 */
static unsigned int Check(const unsigned int _dim1, const unsigned int _dim2)
{
	if(_dim1 == 0 || _dim2 == 0)
		return 1;
	return 0;
}
YALLA_END_NAMESPACE

/*!
 *  \class MatrixUninitialized
 *  \brief Throw a MatrixUninitialized exception
 * 
 *  \details
 *  Generate an error message containing the file, the line and the callstack leading to this exception
 */
class MatrixUninitialized : virtual std::exception
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef MatrixUninitialized this_type;
	//! \brief Type of the super class
	typedef std::exception super_type;
	//@}
 private:
	//! \brief The error message
	std::string m_error_message;
 public:
 	/*! \name Public Constructors
	 *
	 *  Public constructors of the class MatrixUninitialized
	 */
	//@{

	/*! 
	 *  \brief Generates the error message
	 *
	 *  The error message includes:
	 *  - The file throwing the exception
	 *  - The line throwing the exception
	 *  - The callstack
	 *
	 *  \param[in] _file The file throwing the exception
	 *  \param[in] _line The line throwing the exception
	 */
	MatrixUninitialized(const char* _file, const int _line);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class MatrixUninitialized
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~MatrixUninitialized() throw();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class MatrixUninitialized. \n
	 *  These methods give access in read-only mode to the datas/information of the class MatrixUninitialized
	 */
	//@{
	/*!
	 *  \brief Return the error message
	 *
	 *  \return The error message
	 */
	const std::string what();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "MatrixUninitialized" The name of the class
	 */
	const char* getClassName() const
	{
		return "MatrixUninitialized";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
