/*!
 *  \file VectorBidimensionnalNotAllowed.cpp
 *  \brief Implementation of the VectorBidimensionnalNotAllowed exception
 *  \date 12/27/2012
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Exceptions/VectorBidimensionnalNotAllowed.h"

VectorBidimensionnalNotAllowed::
VectorBidimensionnalNotAllowed(const char* _file, const int _line)
{
  std::string msg = "Error : Bidimensionnal vectors not allowed.\nDeclared in file : ";
  msg.append(_file);
  msg.append("\nAt line : ");
  std::ostringstream oss;
  oss << _line;
  msg.append(oss.str());
  m_error_message = msg;
}

VectorBidimensionnalNotAllowed::
~VectorBidimensionnalNotAllowed() throw()
{
  ;
}

const std::string
VectorBidimensionnalNotAllowed::
what()
{
  return m_error_message;
}
