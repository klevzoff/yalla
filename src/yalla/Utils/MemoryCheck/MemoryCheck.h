#pragma once
#ifndef yalla_memory_check_h
#define yalla_memory_check_h
#define _CRTDBG_MAP_ALLOC
#define _CRTBLD

/*!
 *  \file MemoryCheck.h
 *  \brief Count the memory allocated between two checkpoints
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include <map>
#include <string>
#ifdef _WIN32
#include <windows.h>
#include <crtdbg.h>
#include <dbghelp.h>
#endif
#include "yalla/Utils/Types/TypesAndDef.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class MemoryCheck
 *  \brief Count the memory allocated between two checkpoints
 * 
 *  \details
 *  \todo Implement for unix
 */
class MemoryCheck
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef MemoryCheck this_type;
	//@}
 private:
	//! \brief Map to store the information
	std::map<std::string, size_t> m_memory_stats;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class MemoryCheck
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Do nothing
	 */
	explicit MemoryCheck();
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class MemoryCheck
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated by this class)
	 */
	virtual ~MemoryCheck();
	//@}

	/*!
	 *  \brief Register memory consumption now
	 *  
	 *  Store the memory consumption now in the map at the index specified
	 *  
	 *  \param[in] _inString Key to register the memory used
	 */
	void start(const std::string _inString);

	/*!
	 *  \brief Compare memory consumption
	 *  
	 *   Compare memory consumption between now and the one stored at the same index
	 *  
	 *  \param[in] _inString Key to register the memory used
	 */
	void stop(const std::string _inString);

	/*!
	 *  \brief Display information stored by this class
	 */
	void print();

	/*!
	 *  \brief Return the memory in Mb associated with the specified key
	 *  
	 *  \param[in] _inString Key to look for memory consumption
	 *
	 *  \return The value locate in the map at the specified index, 0 if the key doesn't exist
	 */
	double getMem(const std::string& _inString) const;

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "MemoryCheck" The name of the class
	 */
	const char* getClassName() const
	{
		return "MemoryCheck";
	}
};
YALLA_END_NAMESPACE
#endif
