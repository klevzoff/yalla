#include "MemoryCheck.h"
#include "yalla/IO/console/log.h"

/*!
*  \file MemoryCheck.cpp
*  \brief Memory allocation check implementation
*  \date 12/30/2013
*  \author Xavier TUNC
*  \version 1.0
*/

MemoryCheck::
MemoryCheck()
{
  ;
}

MemoryCheck::
~MemoryCheck()
{
  ;
}

void
MemoryCheck::
start(const std::string _inString)
{
#ifdef _WIN32
  _CrtMemState ckpt;
  _CrtMemCheckpoint(&ckpt);
  _CrtMemDumpStatistics(&ckpt);
  m_memory_stats[_inString] = ckpt.lTotalCount;
#endif
}

void
MemoryCheck::
stop(const std::string _inString)
{
#ifdef _WIN32
  _CrtMemState ckpt;
  _CrtMemCheckpoint(&ckpt);
  m_memory_stats[_inString] = ckpt.lTotalCount - m_memory_stats[_inString];
#endif
}

void
MemoryCheck::
print()
{
#ifdef _WIN32
  std::map<std::string, size_t>::iterator it;
  if(m_memory_stats.size()!=0)
  {
    for(it=m_memory_stats.begin();it!=m_memory_stats.end();++it)
    {
      YALLA_LOG("Total Mbytes allocated between the checkpoints " << it->first << " : " << it->second/1024./1024.)
    }
  }
#endif
}

double
MemoryCheck::
getMem(const std::string& _inString) const
{
#ifdef _WIN32
  std::map<std::string, size_t>::const_iterator it = m_memory_stats.find(_inString);
  if(it!=m_memory_stats.end())
    return it->second/1024./1024.;
#endif
  return 0;
}
