#pragma once
#ifndef yalla_quick_sort_h
#define yalla_quick_sort_h

/*!
 *  \file QuickSort.h
 *  \brief Quicksort algorithm implementation
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class QuickSort
 *  \brief Quicksort algorithm implementation
 * 
 *  \details 
 *  This class allows to sort an array in ascending order.
 *
 *  - \tparam DataType The data type of the array to sort
 */
template<typename DataType>
class QuickSort
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the array
	typedef DataType data_type;
	//! \brief Type of this class
	typedef QuickSort this_class;
	//@}
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class QuickSort
	 */
	//@{

	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Do nothing
	 */
	explicit QuickSort()
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class QuickSort
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated by this class)
	 */
	virtual ~QuickSort()
	{
		;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class QuickSort. \n
	 *  These methods give access in read-only mode to the datas/information of the class QuickSort
	 */

	//@{
	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "QuickSort" The name of the class
	 */
	const char* getClassName() const
	{
		return "QuickSort";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class QuickSort. \n
	 *  These methods provide various utilities to the class QuickSort
	 */
	//@{

	/*!
	 *  \brief Sort an array following the quicksort algorithm
	 *
	 *  \details
	 *  Call the method splitArray to find the pivot.
	 *
	 *  \param[in,out] _datas The array to sort
	 *  \param[in] _startIndex The first index of the array to sort
	 *  \param[in] _endIndex The last index of the array to sort
	 */
	static void sort(data_type* _datas, const int _startIndex, const int _endIndex)
	{
		const data_type pivot = _datas[_startIndex];

		if(_endIndex > _startIndex)
		{
			const int splitPoint = QuickSort::splitArray(_datas, pivot, _startIndex, _endIndex);
			_datas[splitPoint] = pivot;
			QuickSort::sort(_datas, _startIndex, splitPoint-1);
			QuickSort::sort(_datas, splitPoint+1, _endIndex);
		}
	}
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class QuickSort. \n
	 *  These methods provide various utilities to the class QuickSort
	 */
	//@{

	/*!
	 *  \brief Find the index of the pivot in the array to sort
	 *
	 *  \details
	 *
	 *  \param[in] _datas The array to sort
	 *  \param[in] _pivot The initial pivot
	 *  \param[in] _startIndex The first index of the array to sort
	 *  \param[in] _endIndex The last index of the array to sort
	 *
	 *  \return The new pivot
	 */
 private:
	static int splitArray(data_type* _datas, const data_type _pivot, const int _startIndex, const int _endIndex)
	{
		int leftBoundary = _startIndex;
		int rightBoundary = _endIndex;
			
		while(leftBoundary < rightBoundary)
		{
			while( _pivot < _datas[rightBoundary] && rightBoundary > leftBoundary)
			{
				--rightBoundary;
			}
			std::swap(_datas[leftBoundary], _datas[rightBoundary]);
				
			while( _pivot >= _datas[leftBoundary] && leftBoundary < rightBoundary)
			{
				++leftBoundary;
			}
			std::swap(_datas[rightBoundary], _datas[leftBoundary]);
		}
		return leftBoundary;
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
