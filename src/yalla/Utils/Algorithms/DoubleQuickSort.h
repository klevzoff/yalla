#pragma once
#ifndef yalla_double_quick_sort_h
#define yalla_double_quick_sort_h

/*!
 *  \file DoubleQuickSort.h
 *  \brief Quicksort algorithm implementation for two arrays
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class DoubleQuickSort
 *  \brief Quicksort algorithm implementation for two arrays
 * 
 *  \details 
 *  This class allows to sort two arrays in ascending order. \n
 *  The first array is sorted and all the modifications in this array are also applied on the second array. \n
 *  This is especially usefull when one needs to sort a matrix column pointer (for exemple) and keep the data pointer consistent.
 *
 *  - \tparam DataType1 The data type of the first array to sort
 *  - \tparam DataType2 The data type of the second array to sort
 */
template<typename DataType1, typename DataType2>
	class DoubleQuickSort
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the first array
	typedef DataType1 data_type1;
	//! \brief Data type of the second array
	typedef DataType2 data_type2;
	//! \brief Type of this class
	typedef DoubleQuickSort this_class;
	//@}
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DoubleQuickSort
	 */
	//@{

	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Do nothing
	 */
	explicit DoubleQuickSort()
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DoubleQuickSort
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated by this class)
	 */
	virtual ~DoubleQuickSort()
	{
		;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class DoubleQuickSort. \n
	 *  These methods give access in read-only mode to the datas/information of the class DoubleQuickSort
	 */

	//@{
	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DoubleQuickSort" The name of the class
	 */
	const char* getClassName() const
	{
		return "DoubleQuickSort";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class DoubleQuickSort. \n
	 *  These methods provide various utilities to the class DoubleQuickSort
	 */
	//@{

	/*!
	 *  \brief Sort tow array following the quicksort algorithm
	 *
	 *  \details
	 *  Call the method splitArray to find the pivot.
	 *
	 *  \param[in,out] _datas1 The first array to sort
	 *  \param[in,out] _datas2 The second array that will be sorted consistently with the first one
	 *  \param[in] _startIndex The first index of the array to sort
	 *  \param[in] _endIndex The last index of the array to sort
	 */
	static void sort(data_type1* _datas1, data_type2* _datas2, const int _startIndex, const int _endIndex)
	{
		const data_type1 pivot = _datas1[_startIndex];
		const data_type2 pivot2 = _datas2[_startIndex];

		if(_endIndex > _startIndex)
		{
			const int splitPoint = DoubleQuickSort::splitArray(_datas1,_datas2, pivot, _startIndex, _endIndex);
			_datas1[splitPoint] = pivot;
			_datas2[splitPoint] = pivot2;
			DoubleQuickSort::sort(_datas1,_datas2, _startIndex, splitPoint-1);
			DoubleQuickSort::sort(_datas1,_datas2, splitPoint+1, _endIndex);
		}
	}
		
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class DoubleQuickSort. \n
	 *  These methods provide various utilities to the class DoubleQuickSort
	 */
	//@{

	/*!
	 *  \brief Find the index of the pivot in the array to sort
	 *
	 *  \details
	 *
	 *  \param[in,out] _datas1 The first array to sort
	 *  \param[in,out] _datas2 The second array that will be sorted consistently with the first one
	 *  \param[in] _pivot The initial pivot
	 *  \param[in] _startIndex The first index of the array to sort
	 *  \param[in] _endIndex The last index of the array to sort
	 *
	 *  \return The new pivot
	 */
	static int splitArray(data_type1* _datas1, data_type2* _datas2, const data_type1 _pivot, const int _startIndex, const int _endIndex)
	{
		int leftBoundary = _startIndex;
		int rightBoundary = _endIndex;
			
		while(leftBoundary < rightBoundary)
		{
			while( _pivot < _datas1[rightBoundary] && rightBoundary > leftBoundary)
			{
				--rightBoundary;
			}
			std::swap(_datas1[leftBoundary], _datas1[rightBoundary]);
			std::swap(_datas2[leftBoundary], _datas2[rightBoundary]);
				
			while( _pivot >= _datas1[leftBoundary] && leftBoundary < rightBoundary)
			{
				++leftBoundary;
			}
			std::swap(_datas1[leftBoundary], _datas1[rightBoundary]);
			std::swap(_datas2[leftBoundary], _datas2[rightBoundary]);
		}
		return leftBoundary;
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
