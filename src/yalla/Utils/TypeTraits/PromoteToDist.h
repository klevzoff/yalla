#pragma once
#ifndef yalla_promote_to_dist_h
#define yalla_promote_to_dist_h

/*!
 *  \file PromoteToDist.h
 *  \brief Type traits for distributed basic types.
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/Undefined.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*! 
 *  \class PromoteToDist
 *  \brief Type traits for distributed basic types.
 *
 *  \details
 *  This class Promotes the distributed matrix or vector type from a sequential matrix or vector. \n
 *  In addition, the data type of the matrix or the vector can be changed through the second template parameter. \n
 *  By default, that is if no specialization is implemented, this class promotes an object of type Undefined.
 *
 *  \tparam FromType The initial type
 *  \tparam ToType The final data type
 */
template<typename FromType, typename ToType>
  class PromoteToDist
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Initial matrix or vector type
	typedef FromType from_type;
	//! \brief Final data type of the promoted matrix or vector
	typedef ToType to_type;
	//! \brief Type of this class
	typedef PromoteToDist< from_type, to_type> this_type;
	//! \brief Type of the promoted class
	typedef Undefined data_type;
	//@}

	/*! 
	 *  \brief Return the class name
	 *
	 *  \details
	 *  \return "PromoteToDist" The name of the class
	 */
	const char* getClassName() const
	{
		return "PromoteToDist";
	}
};

/*!
 *  \brief Specialization of the class PromoteToDist for implemented sequential matrices types.
 *
 *  \param SEQMATRIXTYPE Type of the sequential matrix
 *  \param SEQMATRIXDATATYPE Data type of the sequential matrix
 *  \param SEQMATRIXIMPLEMENTATIONTYPE Implementation type of the sequential matrix
 *  \param DISTMATRIXTYPE Type of the distributed matrix
 *  \param DISTMATRIXIMPLEMENTATIONTYPE Implementation type of the distributed matrix
 */
#define PROMOTETODISTMATRIX(SEQMATRIXTYPE, SEQMATRIXDATATYPE, SEQMATRIXIMPLEMENTATIONTYPE, DISTMATRIXTYPE, DISTMATRIXIMPLEMENTATIONTYPE) \
  template<typename ToType>																							\
  class PromoteToDist<SEQMATRIXTYPE<SEQMATRIXDATATYPE, SEQMATRIXIMPLEMENTATIONTYPE<SEQMATRIXDATATYPE> >, ToType> \
  {																																			\
  public:																																\
		typedef SEQMATRIXDATATYPE from_type;																\
		typedef ToType to_type;																							\
		typedef PromoteToDist< from_type, to_type> this_type;								\
		typedef DISTMATRIXTYPE<to_type, DISTMATRIXIMPLEMENTATIONTYPE<to_type> > data_type; \
		const char* getClassName() const																		\
		{																																		\
			return "PromoteToDist";																						\
		}																																		\
  };																																		\

PROMOTETODISTMATRIX(SeqDenseMatrix, double, SeqDenseMatrixRowMajor, DistDenseMatrix, DistDenseMatrixRowMajor)
PROMOTETODISTMATRIX(SeqDenseMatrix, float, SeqDenseMatrixRowMajor, DistDenseMatrix, DistDenseMatrixRowMajor)
PROMOTETODISTMATRIX(SeqDenseMatrix, int, SeqDenseMatrixRowMajor, DistDenseMatrix, DistDenseMatrixRowMajor)

PROMOTETODISTMATRIX(SeqSparseMatrix, double, SeqCSRMatrix, DistSparseMatrix, DistCSRMatrix)
PROMOTETODISTMATRIX(SeqSparseMatrix, double, SeqCSRMKLMatrix, DistSparseMatrix, DistCSRMKLMatrix)
PROMOTETODISTMATRIX(SeqSparseMatrix, float, SeqCSRMatrix, DistSparseMatrix, DistCSRMatrix)
PROMOTETODISTMATRIX(SeqSparseMatrix, int, SeqCSRMatrix, DistSparseMatrix, DistCSRMatrix)
#undef PROMOTETODISTMATRIX

/*!
 *  \brief Specialization of the class PromoteToDist for implemented sequential vector types.
 *
 *  \param SEQVECTORTYPE Type of the sequential vector
 *  \param SEQVECTORDATATYPE Data type of the sequential vector
 *  \param DISTVECTORTYPE Type of the distributed vector
 */
#define PROMOTETODISTVECTOR(SEQVECTORTYPE, SEQVECTORDATATYPE, DISTVECTORTYPE) \
  template<typename ToType>																							\
  class PromoteToDist<SEQVECTORTYPE<SEQVECTORDATATYPE>, ToType>					\
  {																																			\
  public:																																\
		typedef SEQVECTORDATATYPE from_type;																\
		typedef ToType to_type;																							\
		typedef PromoteToDist< from_type, to_type> this_type;								\
		typedef DISTVECTORTYPE<to_type> data_type;													\
		const char* getClassName() const																		\
		{																																		\
			return "PromoteToDist";																						\
		}																																		\
  };																																		\

PROMOTETODISTVECTOR(SeqVector, double, DistVector)
PROMOTETODISTVECTOR(SeqVector, float, DistVector)
PROMOTETODISTVECTOR(SeqVector, int, DistVector)
#undef PROMOTETODISTVECTOR
YALLA_END_NAMESPACE
#endif
