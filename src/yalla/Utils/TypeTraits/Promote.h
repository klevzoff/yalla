#pragma once
#ifndef yalla_promote_h
#define yalla_promote_h

/*!
 *  \file Promote.h
 *  \brief Type traits
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "Undefined.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*! 
 *  \class Promote
 *  \brief Type traits
 *
 *  \details
 *  By default, that is if no specialization is implemented, this class promotes an object of type Undefined.
 *
 *  \tparam TypeLHS The type of the LHS
 *  \tparam TypeRHS The type of the RHS
 */
template<typename TypeLHS, typename TypeRHS>
  class Promote
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the left hand side
	typedef TypeLHS lhs_type;
	//! \brief Type of the left hand side
	typedef TypeRHS rhs_type;
	//! \brief Type of this class
	typedef Promote<TypeLHS, TypeRHS> this_type;
	//! \brief Type of the promoted class
	typedef Undefined data_type;
	//@}

	/*! 
	 *  \brief Return the class name
	 *
	 *  \details
	 *  \return "Promote" The name of the class
	 */
	const char* getClassName() const
	{
		return "Promote";
	}
};

/*!
 *  \brief Specialization of the class Promote in the case where the two data types are identical.
 *
 *  \param typeLHS Data type of the left hand side
 *  \param typeRHS Data type of the right hand side
 */
#define STD_OP_UN(typeLHS,typeRHS)								\
  template <> class Promote<typeLHS,typeRHS>			\
  {																								\
  public :																				\
		typedef Promote<typeLHS, typeRHS> this_type;	\
		typedef typeLHS lhs_type;											\
		typedef typeRHS rhs_type;											\
		typedef typeLHS data_type;										\
		const char* getClassName() const							\
		{																							\
			return "Promote";														\
		}																							\
  };																							\

STD_OP_UN(int, int)
STD_OP_UN(double,double)
STD_OP_UN(float,float)
#undef STD_OP_UN

/*!
 *  \brief Specialization of the class Promote in the case where the left hand side data type is the less restrictive.
 *
 *  \param typeLHS Data type of the left hand side
 *  \param typeRHS Data type of the right hand side
 */
#define STD_OP_GAUCHE(typeLHS,typeRHS)						\
  template <> class Promote<typeLHS,typeRHS>			\
  {																								\
  public :																				\
		typedef Promote<typeLHS, typeRHS> this_type;	\
		typedef typeLHS lhs_type;											\
		typedef typeRHS rhs_type;											\
		typedef typeLHS data_type;										\
		const char* getClassName() const							\
		{																							\
			return "Promote";														\
		}																							\
  };																							\

STD_OP_GAUCHE(double,int)
STD_OP_GAUCHE(float,int)
STD_OP_GAUCHE(double,float)
#undef STD_OP_GAUCHE

/*!
 *  \brief Specialization of the class Promote in the case where the right hand side data type is the less restrictive.
 *
 *  \param typeLHS Data type of the left hand side
 *  \param typeRHS Data type of the right hand side
 */
#define STD_OP_DROITE(typeLHS,typeRHS)						\
  template <> class Promote<typeLHS,typeRHS> {		\
  public :																				\
		typedef Promote<typeLHS, typeRHS> this_type;	\
		typedef typeLHS lhs_type;											\
		typedef typeRHS rhs_type;											\
		typedef typeRHS data_type;										\
		const char* getClassName() const							\
		{																							\
			return "Promote";														\
		}																							\
  };																							\

STD_OP_DROITE(int,double)
STD_OP_DROITE(int,float)
STD_OP_DROITE(float,double)
#undef STD_OP_DROITE
YALLA_END_NAMESPACE
#endif
