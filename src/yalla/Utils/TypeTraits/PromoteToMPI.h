#pragma once
#ifndef yalla_promote_to_mpi_h
#define yalla_promote_to_mpi_h

/*!
 *  \file PromoteToMPI.h
 *  \brief Type traits for MPI basic types.
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/Undefined.h"

#ifdef WITH_MPI
#include <mpi.h>
#else
YALLA_BEGIN_NAMESPACE(yalla)
struct MPI_Datatype
{
	;
};
YALLA_END_NAMESPACE
#endif

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*! 
 *  \class PromoteToMPI
 *  \brief Type traits for MPI basic types.
 *
 *  \details
 *  This class Promotes the basic MPI data type from a classic data type. \n
 *  By default, that is if no specialization is implemented, this class promotes an object of type MPI_BYTE.
 *
 *  \tparam DataType The classic data type
 */
		 template<typename DataType>
		 class PromoteToMPI
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Classic data type
	typedef DataType from_type;
	//! \brief Type of this class
	typedef PromoteToMPI<from_type> this_type;
	//! \brief Type of the promoted class
#ifdef WITH_MPI
	static inline MPI_Datatype data_type() { return MPI_BYTE; }
#else
	static inline MPI_Datatype data_type() { return MPI_Datatype(); }
#endif
	//@}

	/*! 
	 *  \brief Return the class name
	 *
	 *  \details
	 *  \return "PromoteToMPI" The name of the class
	 */
	const char* getClassName() const
	{
		return "PromoteToMPI";
	}
};

/*!
 *  \brief Specialization of the class PromoteToMPI for used data types. \n
 *
 *  \param DATATYPE Classic data type
 *  \param MPIDATATYPE MPI data type
 */
#ifdef WITH_MPI
#define PROMOTETOMPI(DATATYPE, MPIDATATYPE)													\
  template<>																												\
  class PromoteToMPI<DATATYPE>																			\
  {																																	\
  public:																														\
		typedef DATATYPE from_type;																			\
		typedef PromoteToMPI<from_type> this_type;											\
		static inline MPI_Datatype data_type() { return MPIDATATYPE; }	\
		const char* getClassName() const																\
		{																																\
			return "PromoteToMPI";																				\
		}																																\
  };																																\

PROMOTETOMPI(double, MPI_DOUBLE)
PROMOTETOMPI(float, MPI_FLOAT)
PROMOTETOMPI(int, MPI_INT)
PROMOTETOMPI(unsigned int, MPI_UNSIGNED)
#undef PROMOTETOMPI
#endif
YALLA_END_NAMESPACE
#endif
