#pragma once
#ifndef yalla_promote_to_blas_order_h
#define yalla_promote_to_blas_order_h

/*!
 *  \file PromoteToBLASOrder.h
 *  \brief Type traits for BLAS order
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/Undefined.h"
#ifdef WITH_ATLAS
#ifdef __cplusplus
extern "C"
{
#endif
#include "atlas/cblas.h"
#ifdef __cplusplus
}
#endif
#endif
#ifdef WITH_MKL
#include "mkl.h"
#endif

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*! 
 *  \class PromoteToBLASOrder
 *  \brief Type traits for BLAS order
 *
 *  \details
 *  By default, that is if no specialization is implemented, this class promotes a CblasRowMajor type. \n
 *
 *  \tparam FromType The matrix type
 */
template<typename FromType>
  class PromoteToBLASOrder
  {
  public:
    //! \name Public Typedefs
    //@{
    //! \brief Initial matrix or vector type
    typedef FromType from_type;
    //! \brief Type of this class
    typedef PromoteToBLASOrder<from_type> this_type;
    //! \brief Type of the promoted class
    static inline CBLAS_ORDER data_type() { return CblasRowMajor; } \
    //@}

    /*! 
    *  \brief Return the class name
    *
    *  \details
    *  \return "PromoteToBLASOrder" The name of the class
    */
    const char* getClassName() const
    {
      return "PromoteToBLASOrder";
    }
  };

/*!
 *  \brief Specialization of the class PromoteToBLASOrder for the different data structures implemented so far
 *
 *  \param MATRIXTYPE The type of the matrix
 *  \param MATRIXDATATYPE The data type of the matrix
 *  \param MATRIXIMPLEMENTATIONTYPE The actual matrix implementation
 *  \param BLASORDER The BLAS order that will be promoted
 */
  #define PROMOTETOBLASORDER(MATRIXTYPE, MATRIXDATATYPE, MATRIXIMPLEMENTATIONTYPE, BLASORDER) \
  template<> \
  class PromoteToBLASOrder<MATRIXTYPE<MATRIXDATATYPE, MATRIXIMPLEMENTATIONTYPE<MATRIXDATATYPE> > > \
  { \
  public: \
  typedef MATRIXTYPE<MATRIXDATATYPE, MATRIXIMPLEMENTATIONTYPE<MATRIXDATATYPE> > from_type; \
  typedef PromoteToBLASOrder< from_type > this_type; \
  static inline CBLAS_ORDER data_type() { return BLASORDER; } \
  const char* getClassName() const \
  { \
  return "PromoteToBLASOrder"; \
  } \
  }; \

  PROMOTETOBLASORDER(SeqDenseMatrix, double, SeqDenseMatrixRowMajor, CblasRowMajor)
    PROMOTETOBLASORDER(SeqDenseMatrix, double, SeqDenseMatrixColMajor, CblasRowMajor)
    PROMOTETOBLASORDER(DistDenseMatrix, double, DistDenseMatrixRowMajor, CblasRowMajor)

    PROMOTETOBLASORDER(SeqDenseMatrix, float, SeqDenseMatrixRowMajor, CblasRowMajor)
    PROMOTETOBLASORDER(SeqDenseMatrix, float, SeqDenseMatrixColMajor, CblasRowMajor)
    PROMOTETOBLASORDER(DistDenseMatrix, float, DistDenseMatrixRowMajor, CblasRowMajor)
#undef PROMOTETOBLASORDER
YALLA_END_NAMESPACE
#endif
