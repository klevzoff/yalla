#pragma once
#ifndef yalla_promote_to_seq_h
#define yalla_promote_to_seq_h

/*!
*  \file PromoteToSeq.h
*  \brief Type traits for sequential basic types.
*  \date 13/05/2012
*  \author Xavier TUNC
*  \version 0.2
*/

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/Undefined.h"

/*!
*  \namespace yalla
*  \brief Global namespace of the project
*
*  Global namespace of the project
*/
YALLA_BEGIN_NAMESPACE(yalla)
  /*! 
  *  \class PromoteToSeq
  *  \brief Type traits for sequential basic types.
  *
  *  \details
  *  This class Promotes the sequential matrix or vector type from a distributed matrix or vector. \n
  *  In addition, the data type of the matrix or the vector can be changed through the second template parameter. \n
  *  By default, that is if no specialization is implemented, this class promotes an object of type Undefined.
  */
  template<typename FromType, typename ToType>
  class PromoteToSeq
  {
  public:
    //! \name Public Typedefs
    //@{
    //! \brief Initial matrix or vector type
    typedef FromType from_type;
    //! \brief Final data type of the promoted matrix or vector
    typedef ToType to_type;
    //! \brief Type of this class
    typedef PromoteToSeq< from_type, to_type> this_type;
    //! \brief Type of the promoted class
    typedef Undefined data_type;
    //@}

    /*! 
    *  \brief Return the class name
    *
    *  \details
    *  \return <c> const char* </c> "PromoteToSeq" The name of the class
    */
    const char* getClassName() const
    {
      return "PromoteToSeq";
    }
  };

  /*!
  *  \brief Specialization of the class PromoteToSeq for implemented sequential and distributed types.
  *
  *  \param DISTMATRIXTYPE Type of the distributed matrix
  *  \param DISTMATRIXDATATYPE Data type of the distributed matrix
  *  \param DISTMATRIXIMPLEMENTATIONTYPE Implementation type of the distributed matrix
  *  \param SEQMATRIXTYPE Type of the sequential matrix
  *  \param SEQMATRIXIMPLEMENTATIONTYPE Implementation type of the sequential matrix
  */
#define PROMOTETOSEQ(DISTMATRIXTYPE, DISTMATRIXDATATYPE, DISTMATRIXIMPLEMENTATIONTYPE, SEQMATRIXTYPE, SEQMATRIXIMPLEMENTATIONTYPE) \
  template<typename ToType> \
  class PromoteToSeq<DISTMATRIXTYPE<DISTMATRIXDATATYPE, DISTMATRIXIMPLEMENTATIONTYPE<DISTMATRIXDATATYPE> >, ToType> \
  { \
  public: \
  typedef DISTMATRIXDATATYPE from_type; \
  typedef ToType to_type; \
  typedef PromoteToSeq< from_type, to_type> this_type; \
  typedef SEQMATRIXTYPE<to_type, SEQMATRIXIMPLEMENTATIONTYPE<to_type> > data_type; \
  const char* getClassName() const \
  { \
  return "PromoteToSeq"; \
  } \
  }; \

  PROMOTETOSEQ(DistDenseMatrix, double, DistDenseMatrixRowMajor, SeqDenseMatrix, SeqDenseMatrixRowMajor)
    PROMOTETOSEQ(DistDenseMatrix, float, DistDenseMatrixRowMajor, SeqDenseMatrix, SeqDenseMatrixRowMajor)
    PROMOTETOSEQ(DistDenseMatrix, int, DistDenseMatrixRowMajor, SeqDenseMatrix, SeqDenseMatrixRowMajor)
#undef PROMOTETOSEQ
YALLA_END_NAMESPACE
#endif
