#pragma once
#ifndef yalla_promote_to_vector_h
#define yalla_promote_to_vector_h

/*!
 *  \file PromoteToVector.h
 *  \brief Type traits to promote a vector from a matrix.
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/Undefined.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*! 
 *  \class PromoteToVector
 *  \brief Type traits to promote a vector from a matrix.
 *
 *  \details
 *  This class Promotes a vector from a matrix. \n
 *  The purpose is to have coherent data sets. \n
 *  In addition, the data type of the the vector can be changed through the second template parameter. \n
 *  By default, that is if no specialization is implemented, this class promotes an object of type Undefined.
 *
 *  \tparam FromType The initial matrix type
 *  \tparam ToType The final data type of the vector
 */
template<typename FromType, typename ToType>
  class PromoteToVector
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Initial matrix or vector type
	typedef FromType from_type;
	//! \brief Final data type of the promoted matrix or vector
	typedef ToType to_type;
	//! \brief Type of this class
	typedef PromoteToVector< from_type, to_type> this_type;
	//! \brief Type of the promoted class
	typedef Undefined data_type;
	//@}

	/*! 
	 *  \brief Return the class name
	 *
	 *  \details
	 *  \return "PromoteToVector" The name of the class
	 */
	const char* getClassName() const
	{
		return "PromoteToVector";
	}
};

/*!
 *  \brief Specialization of the class PromoteToVector for implemented sequential and distributed types.
 *
 *  \param MATRIXTYPE Type of the matrix
 *  \param MATRIXDATATYPE Data type of the matrix
 *  \param MATRIXIMPLEMENTATIONTYPE Implementation type of the matrix
 *  \param VECTORTYPE Type of the vector
 */
#define PROMOTETOVECTOR(MATRIXTYPE, MATRIXDATATYPE, MATRIXIMPLEMENTATIONTYPE, VECTORTYPE) \
  template<typename ToType>																							\
  class PromoteToVector<MATRIXTYPE<MATRIXDATATYPE, MATRIXIMPLEMENTATIONTYPE<MATRIXDATATYPE> >, ToType> \
  {																																			\
  public:																																\
		typedef MATRIXDATATYPE from_type;																		\
		typedef ToType to_type;																							\
		typedef PromoteToVector< from_type, to_type> this_type;							\
		typedef VECTORTYPE<to_type> data_type;															\
		const char* getClassName() const																		\
		{																																		\
			return "PromoteToVector";																					\
		}																																		\
  };																																		\

PROMOTETOVECTOR(SeqDenseMatrix, double, SeqDenseMatrixRowMajor, SeqVector)
PROMOTETOVECTOR(SeqDenseMatrix, float, SeqDenseMatrixRowMajor, SeqVector)
PROMOTETOVECTOR(SeqDenseMatrix, int, SeqDenseMatrixRowMajor, SeqVector)

PROMOTETOVECTOR(SeqDenseMatrix, double, SeqDenseMatrixColMajor, SeqVector)
PROMOTETOVECTOR(SeqDenseMatrix, float, SeqDenseMatrixColMajor, SeqVector)
PROMOTETOVECTOR(SeqDenseMatrix, int, SeqDenseMatrixColMajor, SeqVector)

PROMOTETOVECTOR(SeqSparseMatrix, double, SeqCSRMatrix, SeqVector)
PROMOTETOVECTOR(SeqSparseMatrix, float, SeqCSRMatrix, SeqVector)
PROMOTETOVECTOR(SeqSparseMatrix, int, SeqCSRMatrix, SeqVector)

PROMOTETOVECTOR(SeqSparseMatrix, double, SeqCSRMKLMatrix, SeqVector)
PROMOTETOVECTOR(SeqSparseMatrix, float, SeqCSRMKLMatrix, SeqVector)
PROMOTETOVECTOR(SeqSparseMatrix, int, SeqCSRMKLMatrix, SeqVector)

PROMOTETOVECTOR(DistDenseMatrix, double, DistDenseMatrixRowMajor, DistVector)
PROMOTETOVECTOR(DistDenseMatrix, float, DistDenseMatrixRowMajor, DistVector)
PROMOTETOVECTOR(DistDenseMatrix, int, DistDenseMatrixRowMajor, DistVector)

PROMOTETOVECTOR(DistSparseMatrix, double, DistCSRMatrix, DistVector)
PROMOTETOVECTOR(DistSparseMatrix, double, DistCSRMKLMatrix, DistVector)
PROMOTETOVECTOR(DistSparseMatrix, float, DistCSRMatrix, DistVector)
PROMOTETOVECTOR(DistSparseMatrix, float, DistCSRMKLMatrix, DistVector)
PROMOTETOVECTOR(DistSparseMatrix, int, DistCSRMatrix, DistVector)
PROMOTETOVECTOR(DistSparseMatrix, int, DistCSRMKLMatrix, DistVector)

#undef PROMOTETOVECTOR

#ifdef WITH_TRILINOS
/*!
 *  \brief Specialization of the class PromoteToVector for implemented sequential and distributed types.
 *
 *  \param MATRIXTYPE Type of the matrix
 *  \param MATRIXDATATYPE Data type of the matrix
 *  \param MATRIXIMPLEMENTATIONTYPE Implementation type of the matrix
 *  \param TRILINOSIMPLENTATIONTYPE The Trilinos matrix type
 *  \param VECTORTYPE Type of the vector
 */
#define PROMOTETOVECTOR(MATRIXTYPE, MATRIXDATATYPE, MATRIXIMPLEMENTATIONTYPE, TRILINOSIMPLENTATIONTYPE, VECTORTYPE) \
  template<typename ToType>																							\
  class PromoteToVector<MATRIXTYPE<MATRIXDATATYPE, MATRIXIMPLEMENTATIONTYPE<TRILINOSIMPLENTATIONTYPE> >, ToType> \
  {																																			\
  public:																																\
		typedef MATRIXDATATYPE from_type;																		\
		typedef ToType to_type;																							\
		typedef PromoteToVector< from_type, to_type> this_type;							\
		typedef VECTORTYPE<to_type> data_type;															\
		const char* getClassName() const																		\
		{																																		\
			return "PromoteToVector";																					\
		}																																		\
  };																																		\

PROMOTETOVECTOR(DistSparseMatrix, double, EpetraMatrix, Epetra_CrsMatrix, EpetraVector)
#undef PROMOTETOVECTOR
#endif
YALLA_END_NAMESPACE
#endif
