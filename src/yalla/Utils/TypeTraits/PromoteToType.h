#pragma once
#ifndef yalla_promote_to_type_h
#define yalla_promote_to_type_h

/*!
 *  \file PromoteToType.h
 *  \brief Type traits to change the data type of basic types.
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/Undefined.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*! 
 *  \class PromoteToType
 *  \brief Type traits to change the data type of basic types.
 *
 *  \details
 *  This class Promotes any implemented basic types changing its data type. \n
 *  By default, that is if no specialization is implemented, this class promotes an object of type Undefined.
 *
 *  \tparam FromType The initial type
 *  \tparam ToType The final data type
 */
template<typename FromType, typename ToType>
  class PromoteToType
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Initial matrix or vector type
	typedef FromType from_type;
	//! \brief Final data type of the promoted matrix or vector
	typedef ToType to_type;
	//! \brief Type of this class
	typedef PromoteToType< from_type, to_type> this_type;
	//! \brief Type of the promoted class
	typedef Undefined data_type;
	//@}

	/*! 
	 *  \brief Return the class name
	 *
	 *  \details
	 *  \return "PromoteToType" The name of the class
	 */
	const char* getClassName() const
	{
		return "PromoteToType";
	}
};

/*!
 *  \brief Specialization of the class PromoteToType for used data types for matrices. \n
 *
 *  \param BASICTYPE Type of the matrix or the vector
 *  \param DATATYPE Data type of the matrix or the vector
 *  \param IMPLEMENTATIONTYPE Type of the implementation
 */
#define PROMOTETOTYPEMATRIX(BASICTYPE,DATATYPE,IMPLEMENTATIONTYPE)			\
  template<typename ToType>																							\
  class PromoteToType<BASICTYPE<DATATYPE, IMPLEMENTATIONTYPE<DATATYPE> >, ToType> \
  {																																			\
  public:																																\
		typedef DATATYPE from_type;																					\
		typedef ToType to_type;																							\
		typedef PromoteToType< from_type, to_type> this_type;								\
		typedef BASICTYPE<to_type, IMPLEMENTATIONTYPE<to_type> > data_type; \
		const char* getClassName() const																		\
		{																																		\
			return "PromoteToType";																						\
		}																																		\
  };																																		\

PROMOTETOTYPEMATRIX(SeqDenseMatrix, double, SeqDenseMatrixRowMajor)
PROMOTETOTYPEMATRIX(SeqDenseMatrix, float, SeqDenseMatrixRowMajor)
PROMOTETOTYPEMATRIX(SeqDenseMatrix, int, SeqDenseMatrixRowMajor)

PROMOTETOTYPEMATRIX(SeqDenseMatrix, double, SeqDenseMatrixColMajor)
PROMOTETOTYPEMATRIX(SeqDenseMatrix, float, SeqDenseMatrixColMajor)
PROMOTETOTYPEMATRIX(SeqDenseMatrix, int, SeqDenseMatrixColMajor)

PROMOTETOTYPEMATRIX(SeqSparseMatrix, double, SeqCSRMatrix)
PROMOTETOTYPEMATRIX(SeqSparseMatrix, float, SeqCSRMatrix)
PROMOTETOTYPEMATRIX(SeqSparseMatrix, int, SeqCSRMatrix)

PROMOTETOTYPEMATRIX(DistSparseMatrix, double, DistCSRMatrix)
PROMOTETOTYPEMATRIX(DistSparseMatrix, float, DistCSRMatrix)
PROMOTETOTYPEMATRIX(DistSparseMatrix, int, DistCSRMatrix)
#undef PROMOTETOTYPEMATRIX

/*!
 *  \brief Specialization of the class PromoteToType for used data types for vectors. \n
 *
 *  \param BASICTYPE Type of the matrix or the vector
 *  \param DATATYPE Data type of the matrix or the vector
 */
#define PROMOTETOTYPEVECTOR(BASICTYPE,DATATYPE)						\
  template<typename ToType>																\
  class PromoteToType<BASICTYPE<DATATYPE>, ToType>				\
  {																												\
  public:																									\
		typedef DATATYPE from_type;														\
		typedef ToType to_type;																\
		typedef PromoteToType< from_type, to_type> this_type; \
		typedef BASICTYPE<to_type> data_type;									\
		const char* getClassName() const											\
		{																											\
			return "PromoteToType";															\
		}																											\
  };																											\

PROMOTETOTYPEVECTOR(SeqVector, double)
PROMOTETOTYPEVECTOR(SeqVector, float)
PROMOTETOTYPEVECTOR(SeqVector, int)
#undef PROMOTETOTYPEVECTOR
YALLA_END_NAMESPACE
#endif

