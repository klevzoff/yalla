#pragma once
#ifndef yalla_undefined_h
#define yalla_undefined_h

/*!
 *  \file Undefined.h
 *  \brief Generic class for type traits unspecialized promotion
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*! 
 *  \class Undefined
 *  \brief Generic class for type traits unspecialized promotion
 *
 *  \details
 *  This class is promoted in the most of the case of unspecialized templated promotion classes.\n
 */
class Undefined
{
	;
};
YALLA_END_NAMESPACE
#endif
