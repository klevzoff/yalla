#pragma once
#ifndef yalla_math_h
#define yalla_math_h

/*!
 *  \file Math.h
 *  \brief Some "math" functions
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*! 
 *  \class Math
 *  \brief Some "math" functions
 *
 *  The maAbs function returns the max absolute value of an array. \n
 *  The myMax function returns the max value of an array. \n
 *  The myMin function returns the min value of an array. \n
 *  The PosOfMax function returns the index of the max value in an array. \n
 *  The normL2 function returns the L2 norm of an array.
 *  The AlmostEqual method check if two values are almost equals. \n
 *  The AlmostEqualToZero method check if a value is almost equal to zero. \n
 *  The modulo method returns the modulo of a number in the mathematical sense.
 */
class Math
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef Math this_type;
	//@}
 public:
	/*!
	 *  \brief Returns the max absolute value of an array
	 *
	 *  \tparam Data The type of the array
	 *
	 *  \param[in] _src The array we're looking fro the maxAbs
	 *
	 *  \return The abs max value of the array
	 */  
	template<typename Data>
    static typename Data::data_type maxAbs(const Data& _src)
	{
		typename Data::data_type max = std::abs(_src(0));
		for(unsigned int i=1;i<_src.getSize();++i)
		{
			if(max < std::abs(_src(i)))
				max = _src(i);
		}
		return max;
	}

	/*!
	 *  \brief Returns the max value of an array
	 *
	 *  \tparam T The type of the array
	 *
	 *  \param[in] _in The array we're looking for the max
	 *  \param[in] _length The length of the array
	 *
	 *  \return The max of the array
	 */
	template<typename T>
    static T myMax(const T* _in, const unsigned int _length)
	{
		T myMax = _in[0];
		for(unsigned int i=1;i<_length;++i)
		{
			if(myMax < _in[i])
				myMax = _in[i];
		}
		return myMax;
	}

	/*!
	 *  \brief Returns the min value of an array
	 *
	 *  \tparam T The type of the array
	 *
	 *  \param[in] _in The array we're looking for the min
	 *  \param[in] _length The length of the array
	 *
	 *  \return The min of the array
	 */
	template<typename T>
    static T myMin(const T* _in, const unsigned int _length)
	{
		T myMin = _in[0];
		for(unsigned int i=1;i<_length;++i)
		{
			if(myMin > _in[i])
				myMin = _in[i];
		}
		return myMin;
	}

	/*!
	 *  \brief Returns the index of the max value in an array
	 *
	 *  \tparam T The type of the array
	 *
	 *  \param[in] _in The array we're looking for the index of the max
	 *  \param[in] _length The length of the array
	 *
	 *  \return The inde of the max value in the array
	 */
	template<typename T>
    static unsigned int PosOfMax(const T* _in, const unsigned int _length)
	{
		T max = std::abs(_in[0]);
		unsigned int PosOfMax = 0;
		for(unsigned int i=1;i<_length;++i)
		{
			if(max < _in[i])
			{
				max = _in[i];
				PosOfMax = i;
			}
		}
		return PosOfMax;
	}

	/*!
	 *  \brief Returns the L2 norm of an array
	 *
	 *  \tparam DataType The data type of the array
	 *
	 *  \param[in] _vector the vector to compute the L2 norm
	 *
	 *  \return The L2 norm of the vector
	 */
	template <typename DataType>
    static DataType normL2(const SeqVector<DataType>& _vector)
	{
		DataType sum = 0;
		const unsigned int size = _vector.getSize();
		for(unsigned int i=0;i<size;++i)
		{
			sum += _vector(i) * _vector(i);
		}
		return std::sqrt(sum);
	}

	/*!
	 *  \brief Check if two values are almost equals
	 *
	 *  \tparam DataType The data type of the values
	 *
	 *  \param[in] _val1 The first value to compare
	 *  \param[in] _val2 The second value to compare
	 *  \param[in] maxDiff The max absolute difference value tolerated for these two number to be considered as equal
	 *  \param[in] maxRelDiff The max relative difference value tolerated for these two number to be considered as equal
	 *
	 *  \return true If the two number are considered equals, false otherwise
	 */
	template<typename DataType>
    static bool AlmostEqual(const DataType& _val1, const DataType& _val2, const DataType& maxDiff = 1e-14, const DataType& maxRelDiff = 1e-14)
	{
		const DataType diff = fabs(_val1 - _val2);
		if (diff <= maxDiff)
			return true;

		const DataType val1 = fabs(_val1);
		const DataType val2 = fabs(_val2);
		const DataType largest = (val1 > _val2) ? val1 : val2;
		if (diff <= largest * maxRelDiff)
			return true;

		return false;
	}

	/*!
	 *  \brief Check if a value is almost zero
	 *
	 *  \tparam DataType The data type of the values
	 *
	 *  \param[in] _val The value to compare to zero
	 *  \param[in] maxDiff The max absolute difference value tolerated for these two number to be considered as equal
	 *  \param[in] maxRelDiff The max relative difference value tolerated for these two number to be considered as equal
	 *
	 *  \return true If the number is considered equal to zero, false otherwise
	 */
	template<typename DataType>
    static bool AlmostEqualToZero(const DataType& _val, const DataType& maxDiff = 1e-14, const DataType& maxRelDiff = 1e-14)
	{
		return Math::AlmostEqual(_val, 0, maxDiff, maxRelDiff);
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "Math" The name of the class
	 */
	const char* getClassName() const
	{
		return "Math";
	}

	/*!
	 *  \brief Returns the modulo of a value
	 *
	 *  \tparam DataType The data type of the values
	 *
	 *  \param[in] _val The value
	 *  \param[in] _mod The modulo
	 *
	 *  \return The modulo of the value
	 */
	template<typename DataType>
    static DataType modulo(const DataType& _val, const DataType& _mod)
	{
		DataType returnType = _val%_mod;
		if(returnType<0)
			return returnType + _mod;
		return returnType;
	}
};
YALLA_END_NAMESPACE
#endif
