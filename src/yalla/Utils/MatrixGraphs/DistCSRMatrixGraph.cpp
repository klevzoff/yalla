#include "DistCSRMatrixGraph.h"

/*!
 *  \file DistCSRMatrixGraph.cpp
 *  \brief Implementation of distributed CSR matrices graph
 *  \date 12/29/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_USING_NAMESPACE(yalla)

DistCSRMatrixGraph::
DistCSRMatrixGraph() : m_nb_rows(0), m_global_nb_rows(0), m_nb_cols(0), m_global_nb_cols(0), m_nb_nnz(0)
{
  m_rows_ptr = 0;
  m_cols_ptr = 0;
  m_mapping = 0;
  m_own_pointers = false;
}

DistCSRMatrixGraph::
DistCSRMatrixGraph(const DistCSRMatrixGraph& _matrixGraph) : m_nb_rows(_matrixGraph.getNbRows()), m_global_nb_rows(_matrixGraph.getGlobalNbRows()), m_nb_cols(_matrixGraph.getNbCols()), m_global_nb_cols(_matrixGraph.getGlobalNbCols()), m_nb_nnz(_matrixGraph.getNnz())
{
  m_rows_ptr = new int[m_nb_rows+1];
  m_cols_ptr = new int[m_nb_nnz];
  m_mapping = new int[m_nb_cols];
#ifdef _DEBUG
  this->checkAllocation(m_rows_ptr);
  this->checkAllocation(m_cols_ptr);
  this->checkAllocation(m_mapping);
#endif
  for(int i=0;i<m_nb_rows+1;++i)
    m_rows_ptr[i] = _matrixGraph.getRowPtr(i);
  for(int i=0;i<m_nb_cols;++i)
    m_mapping[i] = _matrixGraph.getMapping(i);
  for(int i=0;i<m_nb_nnz;++i)
    m_cols_ptr[i] = _matrixGraph.getColPtr(i);
  m_own_pointers = true;
}

DistCSRMatrixGraph::
DistCSRMatrixGraph(const int _rows, const int _cols, const int _nbNnz, const int _globalNbRows, const int _globalNbCols, int* _mapping, int* _rowPtr, int* _colPtr, bool _deepCopy) : m_nb_rows(_rows), m_global_nb_rows(_globalNbRows), m_nb_cols(_cols), m_global_nb_cols(_globalNbCols), m_nb_nnz(_nbNnz)
{
	if(!_deepCopy)
	{
		m_rows_ptr = _rowPtr;
		m_cols_ptr = _colPtr;
		m_mapping = _mapping;
		m_own_pointers = false;
	}
	else
	{
		m_rows_ptr = new int[m_nb_rows+1];
		m_cols_ptr = new int[m_nb_nnz];
		m_mapping = new int[m_nb_cols];
#ifdef _DEBUG
		this->checkAllocation(m_rows_ptr);
		this->checkAllocation(m_cols_ptr);
		this->checkAllocation(m_mapping);
#endif
		for(int i=0;i<m_nb_rows+1;++i)
			m_rows_ptr[i] = _rowPtr[i];
		for(int i=0;i<m_nb_cols;++i)
			m_mapping[i] = _mapping[i];
		for(int i=0;i<m_nb_nnz;++i)
			m_cols_ptr[i] = _colPtr[i];
		m_own_pointers = true;
	}
}

DistCSRMatrixGraph::
DistCSRMatrixGraph(const int _rows, const int _cols, const int _nnz, const int _globalNbRows, const int _globalNbCols, const int* _mapping, const std::vector<int> _rowPtr, const std::vector<int> _colPtr) : m_nb_rows(_rows), m_global_nb_rows(_globalNbRows), m_nb_cols(_cols), m_global_nb_cols(_globalNbCols), m_nb_nnz(_nnz)
{
  m_rows_ptr = new int[m_nb_rows+1];
  m_cols_ptr = new int[m_nb_nnz];
  m_mapping = new int[m_nb_cols];
#ifdef _DEBUG
  this->checkAllocation(m_rows_ptr);
  this->checkAllocation(m_cols_ptr);
  this->checkAllocation(m_mapping);
#endif
  for(int i=0;i<m_nb_rows+1;++i)
    m_rows_ptr[i] = 0;
  for(int i=0;i<m_nb_cols;++i)
    m_mapping[i] = _mapping[i];
  for(unsigned int i=0;i<_rowPtr.size();++i)
  {
    for(int j=_rowPtr[i];j<m_nb_rows;++j)
      ++m_rows_ptr[j+1];
    m_cols_ptr[i] = _colPtr[i];
  }
  m_own_pointers = true;
}

DistCSRMatrixGraph::
~DistCSRMatrixGraph()
{
  if(m_own_pointers)
  {
    delete[] m_rows_ptr;
    delete[] m_cols_ptr;
    delete[] m_mapping;
  }
}

int
DistCSRMatrixGraph::
getNbRows() const
{
  return m_nb_rows;
}

int
DistCSRMatrixGraph::
getGlobalNbRows() const
{
  return m_global_nb_rows;
}

int
DistCSRMatrixGraph::
getNbCols() const
{
  return m_nb_cols;
}

int
DistCSRMatrixGraph::
getGlobalNbCols() const
{
  return m_global_nb_cols;
}

int
DistCSRMatrixGraph::
getNnz() const
{
  return m_nb_nnz;
}

const int* 
DistCSRMatrixGraph::
getRowPtr() const
{
#ifdef _DEBUG
	this->checkAllocated(m_rows_ptr);
#endif
  return m_rows_ptr;
}

const int* 
DistCSRMatrixGraph::
getColPtr() const
{
#ifdef _DEBUG
	this->checkAllocated(m_cols_ptr);
#endif
  return m_cols_ptr;
}

int
DistCSRMatrixGraph::
getRowPtr(const int _index) const
{
#ifdef _DEBUG
  this->checkAllocated(m_rows_ptr);
  this->checkBounds(_index,0,m_nb_rows+1);
#endif
  return m_rows_ptr[_index];
}

int
DistCSRMatrixGraph::
getColPtr(const int _index) const
{
#ifdef _DEBUG
  this->checkAllocated(m_cols_ptr);
  this->checkBounds(_index,0,m_nb_nnz);
#endif
  return m_cols_ptr[_index];
}

const int*
DistCSRMatrixGraph::
getMapping() const
{
#ifdef _DEBUG
  this->checkAllocated(m_mapping);
#endif
  return m_mapping;
}

int
DistCSRMatrixGraph::
getMapping(const int _index) const
{
#ifdef _DEBUG
  this->checkAllocated(m_mapping);
  this->checkBounds(_index,0,m_nb_nnz);
#endif
  return m_mapping[_index];
}

int
DistCSRMatrixGraph::
operator()(const int _row, const int _col) const
{
#ifdef _DEBUG
	this->checkAllocated(m_rows_ptr);
	this->checkAllocated(m_cols_ptr);
  this->checkBounds(_row,0,m_nb_rows);
  this->checkBounds(_col,0,m_nb_cols);
#endif
  for(int i=m_rows_ptr[_row];i<m_rows_ptr[_row+1];++i)
  {
    if(m_cols_ptr[i]==_col)
      return 1;
  }
  return 0;
}

void 
DistCSRMatrixGraph::
checkAllocation(const void* _datas) const
{
  try
  {
    if(FailedAllocation::Check(_datas))
      throw AllocationFailed(__FILE__,__LINE__);
  }
  catch ( AllocationFailed &ptrFailed )
  {
    YALLA_ERR(ptrFailed.what());
	}
}

void
DistCSRMatrixGraph::
checkAllocated(const void* _datas) const
{
	try
	{
		if(PointerNotAllocated::Check(_datas))
			throw AllocationFailed(__FILE__,__LINE__);
	}
	catch ( PtrNotAllocated &ptrFailed )
	{
		YALLA_ERR(ptrFailed.what());
	}
}

void 
DistCSRMatrixGraph::
checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
{
  try
  {
    if(CheckBounds::Check(_index,_lowerBound,_upperBound))
      throw isOutOfRange(__FILE__,__LINE__);
  }
  catch ( isOutOfRange &isOut )
  {
    YALLA_ERR(isOut.what());
	}
}
