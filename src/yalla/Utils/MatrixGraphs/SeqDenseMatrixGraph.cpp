#include "SeqDenseMatrixGraph.h"

/*!
*  \file SeqDenseMatrixGraph.cpp
*  \brief Implementation of sequential dense matrices graph
*  \date 12/29/2013
*  \author Xavier TUNC
*  \version 1.0
*/

YALLA_USING_NAMESPACE(yalla)

SeqDenseMatrixGraph::
  SeqDenseMatrixGraph() : m_nb_rows(0), m_nb_cols(0), m_nb_nnz(0)
{
  ;
}

SeqDenseMatrixGraph::
  SeqDenseMatrixGraph(const SeqDenseMatrixGraph& _matrixGraph) : m_nb_rows(_matrixGraph.getNbRows()), m_nb_cols(_matrixGraph.getNbCols()), m_nb_nnz(_matrixGraph.getNnz())
{
  ;
}

SeqDenseMatrixGraph::
  SeqDenseMatrixGraph(const int _rows, const int _cols, const int _nbNnz) : m_nb_rows(_rows), m_nb_cols(_cols), m_nb_nnz(_nbNnz)
{
  ;
}

SeqDenseMatrixGraph::
  ~SeqDenseMatrixGraph()
{
  ;
}

int
  SeqDenseMatrixGraph::
  getNbRows() const
{
  return m_nb_rows;
}

int
  SeqDenseMatrixGraph::
  getNbCols() const
{
  return m_nb_cols;
}

int
  SeqDenseMatrixGraph::
  getNnz() const
{
  return m_nb_nnz;
}

int
  SeqDenseMatrixGraph::
  operator()(const int _row, const int _col) const
{
#ifdef _DEBUG
  this->checkBounds(_row,0,m_nb_rows);
  this->checkBounds(_col,0,m_nb_cols);
#endif
  return 1;
}

void 
SeqDenseMatrixGraph::
checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
{
  try
  {
    if(CheckBounds::Check(_index,_lowerBound,_upperBound))
      throw isOutOfRange(__FILE__,__LINE__);
  }
  catch ( isOutOfRange &isOut )
  {
    YALLA_ERR(isOut.what());
	}
}
