#pragma once
#ifndef yalla_dist_csr_matrix_graph_h
#define yalla_dist_csr_matrix_graph_h

/*!
 *  \file DistCSRMatrixGraph.h
 *  \brief Graph of distributed CSR matrices
 *  \date 12/29/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <vector>
#include <iostream>
#include <iomanip>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Exceptions/CheckBounds.h"
#include "yalla/Utils/Exceptions/PointerNotAllocated.h"
#include "yalla/Utils/Exceptions/FailedAllocation.h"
#include "yalla/Utils/ParUtils/ParUtils.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class DistCSRMatrixGraph
 *  \brief Graph of distributed CSR matrices
 * 
 *  \details 
 *  This class provides the graph of a CSR matrix, storing the elements describing the structure of a distributed CSR matrix. \n
 *
 *  Since this class can build the structure of the CSR matrix, an additionnal attribute, m_own_pointers is required, to define the ownership of the graph. \n
 *
 *  The class can throw three types of exception, only in debug mode:
 *  - isOutOfRange exception. If someones try to access an element at index (row,col), such that m_nb_rows < row < 0 or m_nb_cols < col < 0. This exception can be thrown in almost all setter/getter functions.
 *  - AllocationFailed exception. If an allocation failed, the function throws this exception. This exception can be thrown in all functions that allocates memory
 *  - PointerNotAllocated exception. If the user tries to access a non allocated pointer. This exception can be thrown in almost all setter/getter functions
 */
class DistCSRMatrixGraph
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef DistCSRMatrixGraph this_type;
	//@}
 private:
	//! \brief Number of local rows
	int m_nb_rows;
	//! \brief Global number of rows
	int m_global_nb_rows;
	//! \brief Number of local columns
	int m_nb_cols;
	//! \brief Global number of cols
	int m_global_nb_cols;
	//! \brief Number of local non zero elements
	int m_nb_nnz;
	//! \brief Row pointer
	int* m_rows_ptr;
	//! \brief Col pointer
	int* m_cols_ptr;
	//! \brief Mapping local/global ID of rows/cols
	int* m_mapping;
	//! Flag indicating the ownership of the pointers
	bool m_own_pointers;
	//@}
 private:
	/*! \name Private Constructors
	 *
	 *  Private constructors of the class DistCSRMatrixGraph
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Disabled
	 */
	explicit DistCSRMatrixGraph();
	//@}

 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DistCSRMatrixGraph
	 */
	//@{
	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Copy in m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows and m_global_nb_cols the values of _matrixGraph.getNbRows(), _matrixGraph.getNbCols(), _matrixGraph.getNnz(), _matrixGraph.getGlobalNbRows() and _matrixGraph.getGlobalNbCols(). \n
	 *  In addition, this constructor copy the row, col and mapping pointer of the _matrixGraph. \n
	 *  Therefore the class owns the pointer m_rows_ptr, m_cols_ptr and m_mapping and the flag m_own_pointers is set to true. \n
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr or m_mapping failed
	 *
	 *  \param[in] _matrixGraph Graph to copy
	 */
	DistCSRMatrixGraph(const DistCSRMatrixGraph& _matrixGraph);

	/*!
	 *  \brief Data structure constructor
	 *
	 *  \details
	 *  Copy in m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows and m_global_nb_cols the values of _rows, _cols, _nnz, _globalNbRows and_globalNbCols. \n
	 *  The flag _deepCopy defines if the graph has to be deep copied or not, and then the allocation/ownership of the pointers. \n
	 *  If _deepCopy is set to false, the pointers are copied. \n
	 *  If _deepCopy is set to true, the pointers are allocated and then their values are copied.
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr or m_mapping failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _globalNbRows Number of global rows in the matrix
	 *  \param[in] _globalNbCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 *  \param[in] _deepCopy Deep copy flag
	 */
	explicit DistCSRMatrixGraph(const int _rows, const int _cols, const int _nbNnz, const int _globalNbRows, const int _globalNbCols, int* _mapping, int* _rowPtr, int* _colPtr, bool _deepCopy = false);

	/*!
	 *  \brief Vector data structure constructor
	 *
	 *  \details
	 *  Copy in m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows and m_global_nb_cols the values of _rows, _cols, _nnz, _globalNbRows and_globalNbCols. \n
	 *  The values are always deep copied as the data structure is given in a vector. \n
	 *  The row and col pointers are processed to match a CSR graph representation. The expected format here for the row and col pointers is COO.
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr or m_mapping failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _globalNbRows Number of global rows in the matrix
	 *  \param[in] _globalNbCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 *  \param[in] _rowPtr Row pointer in COO format
	 *  \param[in] _colPtr Col pointer in COO format
	 */
	explicit DistCSRMatrixGraph(const int _rows, const int _cols, const int _nbNnz, const int _globalNbRows, const int _globalNbCols, const int* _mapping, const std::vector<int> _rowPtr, const std::vector<int> _colPtr);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DistCSRMatrixGraph
	 */
	//@{
	/*! 
	 *  \brief Destructor
	 *
	 *  \details
	 *  Free the memory eventually allocated by the class, i.e. the row, col and mapping pointer if the class own them. \n
	 */
	virtual ~DistCSRMatrixGraph();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class DistCSRMatrixGraph. \n
	 *  These methods give access in read-only mode to the datas/information of the class DistCSRMatrixGraph
	 */

	//@{
	/*! 
	 *  \brief Return the number of local rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of rows in the matrix
	 */
	int getNbRows() const;

	/*! 
	 *  \brief Return the number of global rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of global rows in the matrix
	 */
	int getGlobalNbRows() const;

	/*! 
	 *  \brief Return the number of local cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of local cols in the matrix
	 */
	int getNbCols() const;

	/*! 
	 *  \brief Return the number of global cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of global cols in the matrix
	 */
	int getGlobalNbCols() const;

	/*! 
	 *  \brief Return the number of local non zero elements in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of local non zero elements in the matrix
	 */	
	int getNnz() const;

	/*! 
	 *  \brief Return the row pointer of the graph
	 *
	 *  \details
	 *
	 *  \return The row pointer of the graph
	 *
	 *  \exception PtrNotAllocated m_rows_ptr is not allocated
	 */
	const int* getRowPtr() const;

	/*! 
	 *  \brief Return the col pointer of the graph
	 *
	 *  \details
	 *
	 *  \return The col pointer of the graph
	 *
	 *  \exception PtrNotAllocated m_cols_ptr is not allocated
	 */
	const int* getColPtr() const;

	/*! 
	 *  \brief Return the value of the row pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index of the we want to access the value
	 *
	 *  \return The value of the row pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_rows_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getRowPtr(const int _index) const;

	/*! 
	 *  \brief Return the value of the col pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index of the we want to access the value
	 *
	 *  \return The value of the col pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_cols_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getColPtr(const int _index) const;

	/*! 
	 *  \brief Return the global mapping.
	 *
	 *  \details
	 *
	 *  \return The local/global ID mapping array
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 */
	const int* getMapping() const;

	/*! 
	 *  \brief Return the global ID of a row/column
	 *
	 *  \details
	 *
	 *  \param[in] _index The local ID of the row/col
	 *
	 *  \return The global ID of the local row/col
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	int getMapping(const int _index) const;

	/*! 
	 *  \brief Check if the entry exists in the matrix
	 *
	 *  \details
	 *  Return 1 if the entry exists i.e. there is a nnz at this index, 0 otherwise. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return 1 if an entry exists in the matrix at the index (_row,_col) , 0 otherwise
	 *
	 *  \exception PtrNotAllocated m_rows_ptr or m_cols_ptr is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	int operator()(const int _row, const int _col) const;

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DistCSRMatrixGraph" The name of the class
	 */
	const char* getClassName() const
	{
		return "DistCSRMatrixGraph";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class DistCSRMatrixGraph. \n
	 *  These methods provide various utilities to the class DistCSRMatrixGraph
	 */
	//@{
	/*!
	 *  \brief Display a sparse graph on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _graph The graph to output
	 */
	friend std::ostream& operator<<(std::ostream& _os, const DistCSRMatrixGraph& _graph)
	{
		_os << _graph.getClassName() << " - ";
		_os << "LocSize: " << _graph.getNbRows() << "x" << _graph.getNbCols() << " - GlobSize: " << _graph.getGlobalNbRows() << "x" << _graph.getGlobalNbCols() << "\n";
		_os << std::setw(4) << "CPU" << " - " << std::setw(9) << "GID" << " - " << std::setw(9) << "LID \n";
			
		for(int i=0;i<_graph.getNbRows();++i)
		{
			for(int j=_graph.getRowPtr(i);j<_graph.getRowPtr(i+1);++j)
			{
				_os << std::right << std::setw(4) << ParUtils::getProcRank() << " - (" << std::setw(3) << _graph.getMapping(i) << "," << std::left << std::setw(3) << _graph.getColPtr(j) << std::right << ") - (" << std::setw(3) << i << "," << std::left << std::setw(3) << _graph.getColPtr(j) << std::right << ")\n";
			}
		}
		return _os;
	}
	//@}

 private:
	/*! \name Private Checkeur
	 *
	 *  Private Checkeur of the class DistCSRMatrixGraph. \n
	 *  These internal methods provides various check utilities for the class DistCSRMatrixGraph
	 */
	//@{
	/*!
	 *  \brief Check if an allocation was successful
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception AllocationFailed _datas was not properly allocated
	 */
	void checkAllocation(const void* _datas) const;

	/*!
	 *  \brief Check if a pointer was allocated
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception PtrNotAllocated _datas is not allocated
	 */
	void checkAllocated(const void* _datas) const;

	/*!
	 *  \brief Check if an index is between specified bounds
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to  check
	 *  \param[in] _lowerBound The lower bound
	 *  \param[in] _upperBound The upper bound
	 *
	 *  \exception isOutOfRange _index is not between lower/upper bound
	 */
	void checkBounds(const int _index, const int _lowerBound, const int _upperBound) const;
	//@}
};
YALLA_END_NAMESPACE
#endif
