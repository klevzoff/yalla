#include "SeqCSRMatrixGraph.h"

/*!
 *  \file SeqCSRMatrixGraph.cpp
 *  \brief Implementation of sequential CSR matrices graph
 *  \date 12/29/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_USING_NAMESPACE(yalla)

SeqCSRMatrixGraph::
SeqCSRMatrixGraph() : m_nb_rows(0), m_nb_cols(0), m_nb_nnz(0)
{
  m_rows_ptr = 0;
  m_cols_ptr = 0;
  m_own_pointers = false;
}

SeqCSRMatrixGraph::
SeqCSRMatrixGraph(const SeqCSRMatrixGraph& _matrixGraph) : m_nb_rows(_matrixGraph.getNbRows()), m_nb_cols(_matrixGraph.getNbCols()), m_nb_nnz(_matrixGraph.getNnz())
{
  m_rows_ptr = new int[m_nb_rows+1];
  m_cols_ptr = new int[m_nb_nnz];
#ifdef _DEBUG
  this->checkAllocation(m_rows_ptr);
  this->checkAllocation(m_cols_ptr);
#endif
  for(int i=0;i<m_nb_rows+1;++i)
    m_rows_ptr[i] = _matrixGraph.getRowPtr(i);
  for(int i=0;i<m_nb_nnz;++i)
    m_cols_ptr[i] = _matrixGraph.getColPtr(i);
  m_own_pointers = true;
}

SeqCSRMatrixGraph::
SeqCSRMatrixGraph(const int _rows, const int _cols, const int _nbNnz, int* _rowPtr, int* _colPtr) : m_nb_rows(_rows), m_nb_cols(_cols), m_nb_nnz(_nbNnz)
{
  m_rows_ptr = _rowPtr;
  m_cols_ptr = _colPtr;
  m_own_pointers = false;
}

SeqCSRMatrixGraph::
SeqCSRMatrixGraph(const int _rows, const int _cols, const int _nbNnz, const std::vector<int> _rowPtr, const std::vector<int> _colPtr) : m_nb_rows(_rows), m_nb_cols(_cols), m_nb_nnz(_nbNnz)
{
  m_rows_ptr = new int[m_nb_rows+1];
  m_cols_ptr = new int[m_nb_nnz];
#ifdef _DEBUG
  this->checkAllocation(m_rows_ptr);
  this->checkAllocation(m_cols_ptr);
#endif
  for(int i=0;i<m_nb_rows+1;++i)
    m_rows_ptr[i] = 0;
  for(unsigned int i=0;i<_rowPtr.size();++i)
  {
    for(int j=_rowPtr[i];j<m_nb_rows;++j)
      ++m_rows_ptr[j+1];
    m_cols_ptr[i] = _colPtr[i];
  }
  m_own_pointers = true;
}

SeqCSRMatrixGraph::
~SeqCSRMatrixGraph()
{
  if(m_own_pointers)
  {
    delete[] m_rows_ptr;
    delete[] m_cols_ptr;
  }
}

int
SeqCSRMatrixGraph::
getNbRows() const
{
  return m_nb_rows;
}

int
SeqCSRMatrixGraph::
getNbCols() const
{
  return m_nb_cols;
}

int
SeqCSRMatrixGraph::
getNnz() const
{
  return m_nb_nnz;
}

const int* 
SeqCSRMatrixGraph::
getRowPtr() const
{
#ifdef _DEBUG
	this->checkAllocated(m_rows_ptr);
#endif
  return m_rows_ptr;
}

const int* 
SeqCSRMatrixGraph::
getColPtr() const
{
#ifdef _DEBUG
	this->checkAllocated(m_cols_ptr);
#endif
  return m_cols_ptr;
}

int
SeqCSRMatrixGraph::
getRowPtr(const int _index) const
{
#ifdef _DEBUG
  this->checkAllocated(m_rows_ptr);
  this->checkBounds(_index,0,m_nb_rows+1);
#endif
  return m_rows_ptr[_index];
}

int
SeqCSRMatrixGraph::
getColPtr(const int _index) const
{
#ifdef _DEBUG
  this->checkAllocated(m_cols_ptr);
  this->checkBounds(_index,0,m_nb_nnz);
#endif
  return m_cols_ptr[_index];
}

int
SeqCSRMatrixGraph::
operator()(const int _row, const int _col) const
{
#ifdef _DEBUG
	this->checkAllocated(m_rows_ptr);
	this->checkAllocated(m_cols_ptr);
  this->checkBounds(_row,0,m_nb_rows);
  this->checkBounds(_col,0,m_nb_cols);
#endif
  for(int i=m_rows_ptr[_row];i<m_rows_ptr[_row+1];++i)
  {
    if(m_cols_ptr[i]==_col)
      return 1;
  }
  return 0;
}

void 
SeqCSRMatrixGraph::
checkAllocation(const void* _datas) const
{
  try
  {
    if(FailedAllocation::Check(_datas))
      throw AllocationFailed(__FILE__,__LINE__);
  }
  catch ( AllocationFailed &ptrFailed )
  {
    YALLA_ERR(ptrFailed.what());
	}
}

void
SeqCSRMatrixGraph::
checkAllocated(const void* _datas) const
{
	try
	{
		if(PointerNotAllocated::Check(_datas))
			throw AllocationFailed(__FILE__,__LINE__);
	}
	catch ( PtrNotAllocated &ptrFailed )
	{
		YALLA_ERR(ptrFailed.what());
	}
}

void 
SeqCSRMatrixGraph::
checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
{
  try
  {
    if(CheckBounds::Check(_index,_lowerBound,_upperBound))
      throw isOutOfRange(__FILE__,__LINE__);
  }
  catch ( isOutOfRange &isOut )
  {
    YALLA_ERR(isOut.what());
	}
}
