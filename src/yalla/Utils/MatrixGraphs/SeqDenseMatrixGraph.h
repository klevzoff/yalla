#pragma once
#ifndef yalla_seq_dense_matrix_graph_h
#define yalla_seq_dense_matrix_graph_h

/*!
 *  \file SeqDenseMatrixGraph.h
 *  \brief Graph of sequential dense matrices
 *  \date 12/29/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <vector>
#include <iostream>
#include <iomanip>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Exceptions/CheckBounds.h"
#include "yalla/Utils/Exceptions/PointerNotAllocated.h"
#include "yalla/Utils/Exceptions/FailedAllocation.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SeqDenseMatrixGraph
 *  \brief Graph of sequential dense matrices
 * 
 *  \details
 *  The class can throw one type of exception, only in debug mode:
 *  - isOutOfRange exception. If someones try to access an element at index (row,col), such that m_nb_rows < row < 0 or m_nb_cols < col < 0. This exception can be thrown in almost all setter/getter functions.
 */
class SeqDenseMatrixGraph
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef SeqDenseMatrixGraph this_type;
	//@}
 private:
	//! \brief Number of rows
	int m_nb_rows;
	//! \brief Number of columns
	int m_nb_cols;
	//! \brief Number of non zero elements
	int m_nb_nnz;
 private:
	/*! \name Private Constructors
	 *
	 *  Private constructors of the class SeqDenseMatrixGraph
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Disabled
	 */
	explicit SeqDenseMatrixGraph();
	//@}

 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SeqDenseMatrixGraph
	 */
	//@{
	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Copy in m_nb_rows, m_nb_cols and m_nb_nnz the values of _matrixGraph.getNbRows(), _matrixGraph.getNbCols() and _matrixGraph.getNnz(). \n
	 *
	 *  \param[in] _matrixGraph Graph to copy
	 */
	SeqDenseMatrixGraph(const SeqDenseMatrixGraph& _matrixGraph);

	/*! 
	 *  \brief Data structure constructor
	 *
	 *  \details
	 *  Copy in m_nb_rows, m_nb_cols and m_nb_nnz the values of _rows, _cols and _nbNnnz. \n
	 *
	 *  \param[in] _rows Number of rows in the matrix
	 *  \param[in] _cols Number of cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 */
	explicit SeqDenseMatrixGraph(const int _rows, const int _cols, const int _nbNnz);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SeqDenseMatrixGraph
	 */
	//@{
	/*! 
	 *  \brief Destructor
	 *
	 *  \details
	 *  Do nothing (no memory allocated).
	 */
	virtual ~SeqDenseMatrixGraph();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SeqDenseMatrixGraph. \n
	 *  These methods give access in read-only mode to the datas/information of the class SeqDenseMatrixGraph
	 */

	//@{

/*! 
	 *  \brief Return the number of rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of rows in the matrix
	 */
	int getNbRows() const;

	/*! 
	 *  \brief Return the number of cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of cols in the matrix
	 */
	int getNbCols() const;

	/*! 
	 *  \brief Return the number of non zero elements in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of non zero elements in the matrix
	 */	
	int getNnz() const;
	/*! 
	 *  \brief Check if the entry exists in the matrix
	 *
	 *  \details
	 *  Always return 1 as it is a dense matrix graph
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return 1 
	 *
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	int operator()(const int _row, const int _col) const;

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SeqDenseMatrixGraph" The name of the class
	 */
	const char* getClassName() const
	{
		return "SeqDenseMatrixGraph";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SeqDenseMatrixGraph. \n
	 *  These methods provide various utilities to the class SeqDenseMatrixGraph
	 */
	//@{
	/*!
	 *  \brief Display a sparse graph on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _graph The graph to output
	 */
	friend std::ostream& operator<<(std::ostream& _os, const SeqDenseMatrixGraph& _graph)
	{
		_os << _graph.getClassName() << " - ";
		_os << "Size: " << _graph.getNbRows() << "x" << _graph.getNbCols() << "\n";
		_os << std::setw(9) << "GID \n";
			
		for(int i=0;i<_graph.getNbRows();++i)
		{
			for(int j=0;j<_graph.getNbCols();++j)
			{
				_os << std::right << "(" << std::setw(3) << i << "," << std::left << std::setw(3) << j << std::right << ")\n";
			}
		}
		return _os;
	}

 private:
	/*! \name Private Checkeur
	 *
	 *  Private Checkeur of the class DistDenseMatrixGraph. \n
	 *  These internal methods provides various check utilities for the class DistDenseMatrixGraph
	 */
	//@{
	/*!
	 *  \brief Check if an index is between specified bounds
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to  check
	 *  \param[in] _lowerBound The lower bound
	 *  \param[in] _upperBound The upper bound
	 *
	 *  \exception isOutOfRange _index is not between lower/upper bound
	 */
	void checkBounds(const int _index, const int _lowerBound, const int _upperBound) const;
	//@}
};
YALLA_END_NAMESPACE
#endif
