#include "DistDenseMatrixGraph.h"

/*!
 *  \file DistDenseMatrixGraph.cpp
 *  \brief Implementation of distributed dense matrices graph
 *  \date 12/29/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_USING_NAMESPACE(yalla)

DistDenseMatrixGraph::
DistDenseMatrixGraph() : m_nb_rows(0), m_global_nb_rows(0), m_nb_cols(0), m_global_nb_cols(0), m_col_mapping(0), m_own_pointers(false)
{
  ;
}

DistDenseMatrixGraph::
DistDenseMatrixGraph(const DistDenseMatrixGraph& _matrixGraph) : m_nb_rows(_matrixGraph.getNbRows()), m_global_nb_rows(_matrixGraph.getGlobalNbRows()), m_nb_cols(_matrixGraph.getNbCols()), m_global_nb_cols(_matrixGraph.getGlobalNbCols()), m_nb_nnz(_matrixGraph.getNnz())
{
  m_col_mapping = new int[m_nb_cols];
#ifdef _DEBUG
  this->checkAllocation(m_col_mapping);
#endif
  for(int i=0;i<m_nb_cols;++i)
    m_col_mapping[i] = _matrixGraph.getMapping(i);
  m_own_pointers = true;
}

DistDenseMatrixGraph::
DistDenseMatrixGraph(const int _rows, const int _cols, const int _nbNnz, const int _globalNbRows, const int _globalNbCols, int* _mappingCols) : m_nb_rows(_rows), m_global_nb_rows(_globalNbRows), m_nb_cols(_cols), m_global_nb_cols(_globalNbCols)
{
  m_col_mapping = _mappingCols;
  m_own_pointers = false;
}

DistDenseMatrixGraph::
~DistDenseMatrixGraph()
{
  if(m_own_pointers)
    delete[] m_col_mapping;
}

int
DistDenseMatrixGraph::
getNbRows() const
{
  return m_nb_rows;
}

int
DistDenseMatrixGraph::
getGlobalNbRows() const
{
  return m_global_nb_rows;
}

int
DistDenseMatrixGraph::
getNbCols() const
{
  return m_nb_cols;
}

int
DistDenseMatrixGraph::
getGlobalNbCols() const
{
  return m_global_nb_cols;
}

int
DistDenseMatrixGraph::
getNnz() const
{
  return m_nb_nnz;
}

const int*
DistDenseMatrixGraph::
getMapping() const
{
#ifdef _DEBUG
  this->checkAllocated(m_col_mapping);
#endif
  return m_col_mapping;
}

int
DistDenseMatrixGraph::
getMapping(const int _index) const
{
#ifdef _DEBUG
  this->checkAllocated(m_col_mapping);
  this->checkBounds(_index,0,m_nb_nnz);
#endif
  return m_col_mapping[_index];
}

int
DistDenseMatrixGraph::
operator()(const int _row, const int _col) const
{
#ifdef _DEBUG
  this->checkBounds(_row,0,m_nb_rows);
  this->checkBounds(_col,0,m_nb_cols);
#endif
  return 1;
}

void 
DistDenseMatrixGraph::
checkAllocation(const void* _datas) const
{
  try
  {
    if(FailedAllocation::Check(_datas))
      throw AllocationFailed(__FILE__,__LINE__);
  }
  catch ( AllocationFailed &ptrFailed )
  {
    YALLA_ERR(ptrFailed.what());
	}
}

void
DistDenseMatrixGraph::
checkAllocated(const void* _datas) const
{
	try
	{
		if(PointerNotAllocated::Check(_datas))
			throw AllocationFailed(__FILE__,__LINE__);
	}
	catch ( PtrNotAllocated &ptrFailed )
	{
		YALLA_ERR(ptrFailed.what());
	}
}

void 
DistDenseMatrixGraph::
checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
{
  try
  {
    if(CheckBounds::Check(_index,_lowerBound,_upperBound))
      throw isOutOfRange(__FILE__,__LINE__);
  }
  catch ( isOutOfRange &isOut )
  {
    YALLA_ERR(isOut.what());
	}
}
