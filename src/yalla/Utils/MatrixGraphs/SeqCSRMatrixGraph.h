#pragma once
#ifndef yalla_seq_csr_matrix_graph_h
#define yalla_seq_csr_matrix_graph_h

/*!
 *  \file SeqCSRMatrixGraph.h
 *  \brief Graph of sequential CSR matrices
 *  \date 12/29/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <vector>
#include <iostream>
#include <iomanip>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Exceptions/CheckBounds.h"
#include "yalla/Utils/Exceptions/PointerNotAllocated.h"
#include "yalla/Utils/Exceptions/FailedAllocation.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SeqCSRMatrixGraph
 *  \brief Graph of sequential CSR matrices
 * 
 *  \details 
 *  This class provides the graph of a CSR matrix, storing the elements describing the structure of a sequential CSR matrix. \n
 *
 *  Since this class can build the structure of the CSR matrix, an additionnal attribute, m_own_pointers is required, to define the ownership of the graph. \n
 *
 *  The class can throw three types of exception, only in debug mode:
 *  - isOutOfRange exception. If someones try to access an element at index (row,col), such that m_nb_rows < row < 0 or m_nb_cols < col < 0. This exception can be thrown in almost all setter/getter functions.
 *  - AllocationFailed exception. If an allocation failed, the function throws this exception. This exception can be thrown in all functions that allocates memory
 *  - PointerNotAllocated exception. If the user tries to access a non allocated pointer. This exception can be thrown in almost all setter/getter functions
 */
class SeqCSRMatrixGraph
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef SeqCSRMatrixGraph this_type;
	//@}
 private:
	//! \brief Number of rows
	int m_nb_rows;
	//! \brief Number of columns
	int m_nb_cols;
	//! \brief Number of non zero elements
	int m_nb_nnz;
	//! Col pointer
	int* m_rows_ptr;
	//! Row pointer
	int* m_cols_ptr;
	//! Flag indicating the ownership of the row and col pointer
	bool m_own_pointers;
	//@}
 private:
	/*! \name Private Constructors
	 *
	 *  Private constructors of the class SeqCSRMatrixGraph
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Disabled
	 */
	explicit SeqCSRMatrixGraph();
	//@}

 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SeqCSRMatrixGraph
	 */
	//@{
	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Copy in m_nb_rows, m_nb_cols and m_nb_nnz the values of _matrixGraph.getNbRows(), _matrixGraph.getNbCols() and _matrixGraph.getNnz(). \n
	 *  In addition, this constructor copy the row and pointer of the _matrixGraph. \n
	 *  Therefore the class owns the pointer m_rows_ptr and m_cols_ptr and the flag m_own_pointers is set to true. \n
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr or m_cols_ptr failed
	 *
	 *  \param[in] _matrixGraph Graph to copy
	 */
	SeqCSRMatrixGraph(const SeqCSRMatrixGraph& _matrixGraph);

	/*!
	 *  \brief Data structure constructor
	 *
	 *  \details
	 *  Copy in m_nb_rows, m_nb_cols and m_nb_nnz the values of _rows, _cols and _nnz. \n
	 *  The row and col pointers are copied. \n
	 *
	 *  \param[in] _rows Number of rows in the matrix
	 *  \param[in] _cols Number of cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 */
	explicit SeqCSRMatrixGraph(const int _rows, const int _cols, const int _nbNnz, int* _rowPtr, int* _colPtr);

	/*!
	 *  \brief Vector data structure constructor
	 *
	 *  \details
	 *  Copy in m_nb_rows, m_nb_cols and m_nb_nnz the values of _rows, _cols and _nnz .\n
	 *  The values are always deep copied as the data structure is given in a vector. \n
	 *  The row and col pointers are processed to match a CSR graph representation. The expected format here for the row and col pointers is COO.
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr or m_cols_ptr failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Row pointer in COO format
	 *  \param[in] _colPtr Col pointer in COO format
	 */
	explicit SeqCSRMatrixGraph(const int _rows, const int _cols, const int _nbNnz, const std::vector<int> _rowPtr, const std::vector<int> _colPtr);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SeqCSRMatrixGraph
	 */
	//@{
	/*! 
	 *  \brief Destructor
	 *
	 *  \details
	 *  Free the memory eventually allocated by the class, i.e. the row, col and mapping pointer if the class own them. \n
	 */
	virtual ~SeqCSRMatrixGraph();

	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SeqCSRMatrixGraph. \n
	 *  These methods give access in read-only mode to the datas/information of the class SeqCSRMatrixGraph
	 */

	//@{
	/*! 
	 *  \brief Return the number of rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of rows in the matrix
	 */
	int getNbRows() const;

	/*! 
	 *  \brief Return the number of cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of cols in the matrix
	 */
	int getNbCols() const;

	/*! 
	 *  \brief Return the number of non zero elements in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of non zero elements in the matrix
	 */	
	int getNnz() const;

	/*! 
	 *  \brief Return the row pointer of the graph
	 *
	 *  \details
	 *
	 *  \return The row pointer of the graph
	 *
	 *  \exception PtrNotAllocated m_rows_ptr is not allocated
	 */
	const int* getRowPtr() const;

	/*! 
	 *  \brief Return the col pointer of the graph
	 *
	 *  \details
	 *
	 *  \return The col pointer of the graph
	 *
	 *  \exception PtrNotAllocated m_cols_ptr is not allocated
	 */
	const int* getColPtr() const;

	/*! 
	 *  \brief Return the value of the row pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index of the we want to access the value
	 *
	 *  \return The value of the row pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_rows_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getRowPtr(const int _index) const;

	/*! 
	 *  \brief Return the value of the col pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index of the we want to access the value
	 *
	 *  \return The value of the col pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_cols_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getColPtr(const int _index) const;

	/*! 
	 *  \brief Check if the entry exists in the matrix
	 *
	 *  \details
	 *  Return 1 if the entry exists i.e. there is a nnz at this index, 0 otherwise. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return 1 if an entry exists in the matrix at the index (_row,_col) , 0 otherwise
	 *
	 *  \exception PtrNotAllocated m_rows_ptr or m_cols_ptr is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	int operator()(const int _row, const int _col) const;

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SeqCSRMatrixGraph" The name of the class
	 */
	const char* getClassName() const
	{
		return "SeqCSRMatrixGraph";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SeqCSRMatrixGraph. \n
	 *  These methods provide various utilities to the class SeqCSRMatrixGraph
	 */
	//@{
	/*!
	 *  \brief Display a sparse graph on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _graph The graph to output
	 */
	friend std::ostream& operator<<(std::ostream& _os, const SeqCSRMatrixGraph& _graph)
	{
		_os << _graph.getClassName() << " - ";
		_os << "Size: " << _graph.getNbRows() << "x" << _graph.getNbCols() << "\n";
		_os << std::setw(9) << "GID\n";
			
		for(int i=0;i<_graph.getNbRows();++i)
		{
			for(int j=_graph.getRowPtr(i);j<_graph.getRowPtr(i+1);++j)
			{
				_os << std::right << "(" << std::setw(3) << i << "," << std::left << std::setw(3) << _graph.getColPtr(j) << std::right << ") \n";
			}
		}
		return _os;
	}
	//@}

 private:
	/*! \name Private Checkeur
	 *
	 *  Private Checkeur of the class SeqCSRMatrixGraph. \n
	 *  These internal methods provides various check utilities for the class SeqCSRMatrixGraph
	 */
	//@{

	/*!
	 *  \brief Check if an allocation was successful
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception AllocationFailed _datas was not properly allocated
	 */
	void checkAllocation(const void* _datas) const;

	/*!
	 *  \brief Check if a pointer was allocated
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception PtrNotAllocated _datas is not allocated
	 */
	void checkAllocated(const void* _datas) const;

	/*!
	 *  \brief Check if an index is between specified bounds
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to  check
	 *  \param[in] _lowerBound The lower bound
	 *  \param[in] _upperBound The upper bound
	 *
	 *  \exception isOutOfRange _index is not between lower/upper bound
	 */
	void checkBounds(const int _index, const int _lowerBound, const int _upperBound) const;
	//@}
};
YALLA_END_NAMESPACE
#endif
