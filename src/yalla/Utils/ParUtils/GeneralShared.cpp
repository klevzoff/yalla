#include "GeneralShared.h"

/*!
 *  \file GeneralShared.cpp
 *  \brief General shared system implementation
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

GeneralShared::
GeneralShared()
{
	m_nb_elements = 0;
	m_nb_receivers = 0;
	m_receiver_list = 0;
	m_nb_elements_by_receiver = 0;
	m_global_ids = 0;
}

GeneralShared::
GeneralShared(const int _nbElements, const int _nbReceivers, const int* _receiverList, const int* _nbElementsByReceiver, const int* _globalIds)
{
	m_nb_elements = _nbElements;
	m_nb_receivers = _nbReceivers;
	m_receiver_list = new int[m_nb_receivers];
	for(int i=0;i<m_nb_receivers;++i)
		m_receiver_list[i] = _receiverList[i];
	m_nb_elements_by_receiver = new int[m_nb_receivers+1];
	for(int i=0;i<m_nb_receivers+1;++i)
		m_nb_elements_by_receiver[i] = _nbElementsByReceiver[i];
	m_global_ids = new int[m_nb_elements];
	for(int i=0;i<m_nb_elements;++i)
		m_global_ids[i] = _globalIds[i];
}

GeneralShared::
~GeneralShared()
{
		delete[] m_receiver_list;
		m_receiver_list = 0;
		delete[] m_nb_elements_by_receiver;
		m_nb_elements_by_receiver = 0;
		delete[] m_global_ids;
		m_global_ids = 0;
}

int
GeneralShared::
getNbElements() const
{
	return m_nb_elements;
}

int
GeneralShared::
getNbReceiver() const
{
	return m_nb_receivers;
}

const int*
GeneralShared::
getReceiverList() const
{
	return m_receiver_list;
}

int
GeneralShared::
getNbElementsByProc(const int _procRank) const
{
	return m_nb_elements_by_receiver[_procRank+1] - m_nb_elements_by_receiver[_procRank];
}

const int*
GeneralShared::
getNbElementsByProcList() const
{
	return m_nb_elements_by_receiver;
}

const int*
GeneralShared::
getGID() const
{
	return m_global_ids;
}

const int*
GeneralShared::
getGIDsForProc(const int _procRank) const
{
	return &(m_global_ids[m_nb_elements_by_receiver[_procRank]]);
}
