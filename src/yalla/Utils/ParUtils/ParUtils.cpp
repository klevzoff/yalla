#include "ParUtils.h"

/*!
 *  \file ParUtils.cpp
 *  \brief Parallel utilities implementation
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_USING_NAMESPACE(yalla)

int
ParUtils::
getProcRank()
{
#ifdef WITH_MPI
  int procRank;
  MPI_Comm_rank(ThisComm, &procRank);
  return procRank;
#else
  return 0;
#endif
}

int
ParUtils::
getNumProc()
{
#ifdef WITH_MPI
  int numProc;
  MPI_Comm_size(ThisComm, &numProc);
  return numProc;
#else
  return 1;
#endif
}


int
ParUtils::
masterProc()
{
  return 0;
}


bool
ParUtils::
iAmMaster()
{
#ifdef WITH_MPI
  int procRank;
  MPI_Comm_rank(ThisComm, &procRank);
  return (procRank==0);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
SendTo(void* _datas, const int _size, const MPI_Datatype _dataType, const int _toProc, const int _tag, const MPI_Comm _comm)
{
#ifdef WITH_MPI
  return MPI_Send(_datas, _size, _dataType, _toProc, _tag , _comm);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
IsendTo(void* _datas, const int _size, const MPI_Datatype _dataType, const int _toProc, const int _tag, const MPI_Comm _comm, MPI_Request* _request)
{
#ifdef WITH_MPI
  if(!_request)
  {
    MPI_Request request;
    return MPI_Isend(_datas, _size, _dataType, _toProc, _tag , _comm, &request);
  }
  return MPI_Isend(_datas, _size, _dataType, _toProc, _tag , _comm, _request);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
SendToMaster(void* _datas, const int _size, const MPI_Datatype _dataType, const int _tag, const MPI_Comm _comm)
{
#ifdef WITH_MPI
  return MPI_Send(_datas, _size, _dataType, ParUtils::masterProc(), _tag , _comm);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
IsendToMaster(void* _datas, const int _size, const MPI_Datatype _dataType, const int _tag, const MPI_Comm _comm, MPI_Request* _request)
{
#ifdef WITH_MPI
  if(!_request)
  {
    MPI_Request request;
    return MPI_Isend(_datas, _size, _dataType, ParUtils::masterProc(), _tag , _comm, &request);
  }
  return MPI_Isend(_datas, _size, _dataType, ParUtils::masterProc(), _tag , _comm, _request);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
RecvFrom(void* _datas, const int _size, const MPI_Datatype _dataType, const int _fromProc, const int _tag, const MPI_Comm _comm , MPI_Status* _status)
{
#ifdef WITH_MPI
  if(!_status)
  {
    MPI_Status msg_status;
    return MPI_Recv(_datas,_size,_dataType, _fromProc, _tag, _comm, &msg_status);
  }
  return MPI_Recv(_datas,_size,_dataType, _fromProc, _tag, _comm, _status);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
IRecvFrom(void* _datas, const int _size, const MPI_Datatype _dataType, const int _fromProc, const int _tag, const MPI_Comm _comm, MPI_Request* _request)
{
#ifdef WITH_MPI
  if(!_request)
  {
    MPI_Request request;
    return MPI_Irecv(_datas,_size,_dataType, _fromProc, _tag, _comm, &request);
  }
  return MPI_Irecv(_datas,_size,_dataType, _fromProc, _tag, _comm, _request);
#else
  return YALLA_SUCCESS;
#endif
}


int
ParUtils::
RecvFromMaster(void* _datas, const int _size, const MPI_Datatype _dataType, const int _tag, const MPI_Comm _comm, MPI_Status* _status)
{
#ifdef WITH_MPI
  if(!_status)
  {
    MPI_Status msg_status;
    return MPI_Recv(_datas,_size,_dataType, ParUtils::masterProc(), _tag, _comm, &msg_status);
  }
  return MPI_Recv(_datas,_size,_dataType, ParUtils::masterProc(), _tag, _comm, _status);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
IRecvFromMaster(void* _datas, const int _size, const MPI_Datatype _dataType, const int _tag, const MPI_Comm _comm, MPI_Request* _request)
{
#ifdef WITH_MPI
  if(!_request)
  {
    MPI_Request request;
    return MPI_Irecv(_datas,_size,_dataType, ParUtils::masterProc(), _tag, _comm, &request);
  }
  return MPI_Irecv(_datas,_size,_dataType, ParUtils::masterProc(), _tag, _comm, _request);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
ProbeFrom(const int _fromProc, MPI_Status* _status, const int _tag, const MPI_Comm _comm)
{
#ifdef WITH_MPI
  return  MPI_Probe(_fromProc, _tag, _comm, _status);
#else
  return YALLA_SUCCESS;
#endif
}

int ParUtils::ProbeFromMaster(MPI_Status* _status, const int _tag, const MPI_Comm _comm)
{
#ifdef WITH_MPI
  return  MPI_Probe(ParUtils::masterProc(), _tag, _comm, _status);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
getCount(MPI_Status* _status, const MPI_Datatype _dataType, int * _count)
{
#ifdef WITH_MPI
  return  MPI_Get_count(_status, _dataType, _count);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
reduceScatter(void* _outDatas, void* _inDatas, int* _numReceivedElements, const MPI_Datatype _dataType, MPI_Op _op, MPI_Comm _comm)
{
#ifdef WITH_MPI
  return MPI_Reduce_scatter(_outDatas, _inDatas, _numReceivedElements, _dataType, _op, _comm);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
Allreduce(void* _outDatas, void* _inDatas, int _numElements, const MPI_Datatype _dataType, MPI_Op _op, MPI_Comm _comm)
{
#ifdef WITH_MPI
  return MPI_Allreduce(_outDatas,_inDatas,_numElements,_dataType,_op,_comm);
#else
  return YALLA_SUCCESS;
#endif
}

int
ParUtils::
Waitall(int _count, MPI_Request* _requests, MPI_Status* _statuses = 0)
{
#ifdef WITH_MPI
	return MPI_Waitall(_count, _requests, _statuses);
#else
	return YALLA_SUCCESS;
#endif
}

int
ParUtils::
Reduce(void* _sendbuf, void* _recvbuf, int _count, MPI_Datatype _datatype, MPI_Op _op, int _root, MPI_Comm _comm)
{
#ifdef WITH_MPI
	return MPI_Reduce(_sendbuf,_recvbuf,_count,_datatype,_op,_root,_comm);
#else
	return YALLA_SUCCESS;
#endif
}
