#include "GeneralGhosts.h"

/*!
 *  \file GeneralGhosts.cpp
 *  \brief General ghost system implementation
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

GeneralGhosts::
GeneralGhosts()
{
	m_nb_elements = 0;
	m_nb_sender = 0;
	m_nb_elements_by_sender = 0;
	m_global_ids = 0;
	m_sender_list = 0;
}

GeneralGhosts::
GeneralGhosts(const int _nbElements, const int _nbSender, const int* _senderList, const int* _nbElementsBySender, const int* _globalIds)
{
	m_nb_elements = _nbElements;
	m_nb_sender = _nbSender;
	m_sender_list = new int[m_nb_sender];
	m_nb_elements_by_sender = new int[m_nb_sender+1];
	m_global_ids = new int[m_nb_elements];
	for(int i=0;i<m_nb_sender;++i)
		m_sender_list[i] = _senderList[i];
	for(int i=0;i<m_nb_sender+1;++i)
		m_nb_elements_by_sender[i] = _nbElementsBySender[i];
	for(int i=0;i<m_nb_elements;++i)
		m_global_ids[i] = _globalIds[i];
}

GeneralGhosts::
~GeneralGhosts()
{
	delete[] m_nb_elements_by_sender;
	m_nb_elements_by_sender = 0;
	delete[] m_global_ids;
	m_global_ids = 0;
	delete[] m_sender_list;
	m_sender_list = 0;
}

int
GeneralGhosts::
getNbElements() const
{
	return m_nb_elements;
}

int
GeneralGhosts::
getNbSender() const
{
	return m_nb_sender;
}

const int*
GeneralGhosts::
getSenderList() const
{
	return m_sender_list;
}

int
GeneralGhosts::
getNbElementsForProc(const int _procRank) const
{
	return m_nb_elements_by_sender[_procRank+1] - m_nb_elements_by_sender[_procRank];
}

const int*
GeneralGhosts::
getNbElementsForProcList() const
{
	return m_nb_elements_by_sender;
}

const int*
GeneralGhosts::
getGIDs() const
{
	return m_global_ids;
}

const int*
GeneralGhosts::
getGIDsForProc(const int _procRank) const
{
	return &(m_global_ids[m_nb_elements_by_sender[_procRank]]);
}

