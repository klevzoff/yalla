#pragma once
#ifndef yalla_spmv_shared_h
#define yalla_spmv_shared_h

/*!
 *  \file SPMVShared.h
 *  \brief SPMV shared system
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/ParUtils/ParUtils.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SPMVShared
 *  \brief SPMV shared system
 * 
 *  \details
 *  This class allows to specify the required information that needs to be communicated to a proc for a SPMV. \n
 *  The different informations are stored in the following attributes:
 *  - The total number of elements that need to be sent (m_nb_elements)
 *  - The total number of receiver (m_nb_receiver)
 *  - The list of receiver (m_nb_receiver_list)
 *  - The number of elements by receiver (m_nb_elements_by_receiver)
 *  - The LIDs of the elements that will be sent (m_local_ids)
 *
 *  Together with the SPMVGhosts, this class is stored in a synchronizer that allows to update the required elements. \n
 *  The main difference between this class and the GeneralShared lies in the fact that here we only need to use LIDs instead of GIDs.
 */
class SPMVShared
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef SPMVShared this_type;
	//@}
 private:
	//! \brief The total number of elements that need to be sent
	int m_nb_elements;
	//! \brief The total number of receivers
	int m_nb_receivers;
	//! \brief The receiver list
	int* m_receiver_list;
	//! \brief The number of elements by receiver
	int* m_nb_elements_by_receiver;
	//! \brief The GIDs of the required elements
	int* m_local_ids;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SPMVShared
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Set all values to zero
	 */
	explicit SPMVShared();

	/*! 
	 *  \brief Constructor
	 *
	 *  Copy in m_nb_elements and m_nb_receivers the values of _nbElements, _nbReceivers. \n
	 *  Allocate the different pointers and copy the values of _receiverList, _nbElementsByReceiver and _globalIds in m_receiver_list, m_nb_elements_by_receiver and m_global_ids.
	 *
	 *  \param[in] _nbElements The total number of elements
	 *  \param[in] _nbReceivers The total number of receivers
	 *  \param[in] _receiverList The receiver list
	 *  \param[in] _nbElementsByReceiver The number of elements by receiver
	 *  \param[in] _localIds The GIDs of the elements
	 */
	explicit SPMVShared(const int _nbElements, const int _nbReceivers, const int* _receiverList, const int* _nbElementsByReceiver, const int* _localIds);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SPMVShared
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class, i.e. m_receiver_list, m_global_ids and m_nb_elements_by_receiver.
	 */
	virtual ~SPMVShared();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SPMVShared. \n
	 *  These methods give access in read-only mode to the datas/information of the class SPMVShared
	 */
	//@{

	/*!
	 *  \brief Return the total number of elements
	 *
	 *  \return The total number of elements
	 */
	int getNbElements() const;

	/*!
	 *  \brief Return the total number of receivers
	 *
	 *  \return The total number of receivers
	 */
	int getNbReceiver() const;

	/*!
	 *  \brief Return the receiver list
	 *
	 *  \return The receiver list
	 */
	const int* getReceiverList() const;

	/*!
	 *  \brief Return the number of element for a receiver
	 *
	 *  \param[in] _procRank The receiver
	 *
	 *  \return The number of elements for a receiver
	 */
	int getNbElementsByProc(const int _procRank) const;

	/*!
	 *  \brief Return the list of the number of element for all receivers
	 *
	 *  \return The list of the number of element for all receivers
	 */
	const int* getNbElementsByProcList() const;

	/*!
	 *  \brief Return the list of the LIDs that need to be sent
	 *
	 *  \return The list of the LIDs that need to be sent
	 */
	const int* getLID() const;

	/*!
	 *  \brief Return the list of LIDs that need to be sent for a receiver
	 *
	 *  \param[in] _procRank The source
	 *
	 *  \return The list of LIDs that need to be sent for a receiver
	 */
	const int* getLIDsForProc(const int _procRank) const;
	
	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SPMVShared" The name of the class
	 */
	const char* getClassName() const
	{
		return "SPMVShared";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SPMVShared. \n
	 *  These methods provide various utilities to the class SPMVShared
	 */
	//@{
	/*!
	 *  \brief Display shared information on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _shared The shared elements to display
	 */
	friend std::ostream& operator<<(std::ostream& _os, const SPMVShared& _shared)
	{
		_os << _shared.getClassName() << " - ";
		_os << "CPU: " << ParUtils::getProcRank() << " - NumSharedElements: " << _shared.getNbElements() << " - NumSharedDest: " << _shared.getNbReceiver() << "\n";
		for(int i=0;i<_shared.getNbReceiver();++i)
		{
			_os << "NumOfElementsToSend: " << _shared.getNbElementsByProc(i) << " to dest: " << _shared.getReceiverList()[i] << "\n";
			for(int j=0; j<_shared.getNbElementsByProc(i);++j)
				_os << "LID: " << _shared.getLIDsForProc(i)[j] << "\n";
		}
		return _os;
	}
	//@}
};

YALLA_END_NAMESPACE

#endif
