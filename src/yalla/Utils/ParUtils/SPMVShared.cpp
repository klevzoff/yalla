#include "SPMVShared.h"

/*!
 *  \file SPMVShared.cpp
 *  \brief SPMV shared system implementation
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

SPMVShared::
SPMVShared()
{
	m_nb_elements = 0;
	m_nb_receivers = 0;
	m_receiver_list = 0;
	m_nb_elements_by_receiver = 0;
	m_local_ids = 0;
}

SPMVShared::
SPMVShared(const int _nbElements, const int _nbReceivers, const int* _receiverList, const int* _nbElementsByReceiver, const int* _localIds)
{
	m_nb_elements = _nbElements;
	m_nb_receivers = _nbReceivers;
	m_receiver_list = new int[m_nb_receivers];
	for(int i=0;i<m_nb_receivers;++i)
		m_receiver_list[i] = _receiverList[i];
	m_nb_elements_by_receiver = new int[m_nb_receivers+1];
	for(int i=0;i<m_nb_receivers+1;++i)
		m_nb_elements_by_receiver[i] = _nbElementsByReceiver[i];
	m_local_ids = new int[m_nb_elements];
	for(int i=0;i<m_nb_elements;++i)
		m_local_ids[i] = _localIds[i];
}

SPMVShared::
~SPMVShared()
{
	delete[] m_receiver_list;
	m_receiver_list = 0;
	delete[] m_nb_elements_by_receiver;
	m_nb_elements_by_receiver = 0;
	delete[] m_local_ids;
	m_local_ids = 0;
}

int
SPMVShared::
getNbElements() const
{
	return m_nb_elements;
}

int
SPMVShared::
getNbReceiver() const
{
	return m_nb_receivers;
}

const int*
SPMVShared::
getReceiverList() const
{
	return m_receiver_list;
}

int
SPMVShared::
getNbElementsByProc(const int _procRank) const
{
	return m_nb_elements_by_receiver[_procRank+1] - m_nb_elements_by_receiver[_procRank];
}

const int*
SPMVShared::
getNbElementsByProcList() const
{
	return m_nb_elements_by_receiver;
}

const int*
SPMVShared::
getLID() const
{
	return m_local_ids;
}

const int*
SPMVShared::
getLIDsForProc(const int _procRank) const
{
	return &(m_local_ids[m_nb_elements_by_receiver[_procRank]]);
}

