#pragma once
#ifndef yalla_spmv_synchronization_h
#define yalla_spmv_synchronization_h

/*!
 *  \file SPMVSynchronization.h
 *  \brief SPMV synchronizer system
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "SPMVGhosts.h"
#include "SPMVShared.h"
#include "ParUtils.h"
#include "yalla/Utils/TypeTraits/PromoteToMPI.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SPMVSynchronization
 *  \brief SPMV synchronizer system
 * 
 *  \details
 *  This class allows to synchronize elements between different procs for a SPMV. \n
 *  It stores a SPMVGhosts for updating the elements that need to be received from other sources and a SPMVShared for updating elements that needs to be sent to other procs. \n
 *  The synchronization is done by non blocking communication through the synchronize method. \n
 *  The method isSynchronized allow to ensure that all informations have been received before proceeding. \n
 *  The send buffer is allocated in the constructor and filled with the proper value before sending the datas. This way, non contiguous datas an be sent. \n
 *  The receive buffer is the data array of the vector that needs to be updated.
 *
 *  \tparam DataType The data type of the vector that needs to be synchronized
 *
 *  \warning This class should always take the ownership of the ghosts and shared information and therefore is in charge of freeing the memory for those objects
 */
template<typename DataType>
class SPMVSynchronization
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the buffers
	typedef DataType data_type;
	//! \brief Type of this class
	typedef SPMVSynchronization this_type;
	//@}
 private:
	//! \brief Ghosts information
	const SPMVGhosts* m_ghosts_infos;
	//! \brief Shared information
	const SPMVShared* m_shared_infos;
	//! \brief Number of message to receive
	const int m_nb_msg;
	//! \brief MPI request status for incoming messages
	MPI_Request* m_request_status;
	//! \brief Synchronization state flag
	mutable bool m_synchronized;
	//! \brief The send buffer
	mutable data_type* m_datas;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SPMVSynchronization
	 */
	//@{
	/*! 
	 *  \brief Unique constructor
	 *
	 *  Store the ghosts and shared information
	 */
	explicit SPMVSynchronization(const SPMVGhosts* _ghostsInfos, const SPMVShared* _sharedInfos): m_ghosts_infos(_ghostsInfos), m_shared_infos(_sharedInfos) , m_nb_msg(m_ghosts_infos->getNbSender())
	{
		m_synchronized = false;
		m_request_status = new MPI_Request[m_nb_msg];
		const int nbSharedElements = m_shared_infos->getNbElements();
		m_datas = new data_type[nbSharedElements];
		for(int i=0;i<nbSharedElements;++i)
			m_datas[i] = 0;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SPMVSynchronization
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class, i.e. the m_request_status, m_ghosts_infos, m_shared_infos and m_datas pointers.
	 */
	virtual ~SPMVSynchronization()
	{
		delete[] m_request_status;
		m_request_status = 0;
		delete m_ghosts_infos;
		m_ghosts_infos = 0;
		delete m_shared_infos;
		m_shared_infos = 0;
		delete[] m_datas;
		m_datas = 0;
	}
	//@}
	/*! \name Public Getter
	 *
	 *  Public Getter of the class SPMVSynchronization. \n
	 *  These methods give access in read-only mode to the datas/information of the class SPMVSynchronization
	 */
	//@{

	/*!
	 *  \brief Return a const ref on the shared information
	 *
	 *  \return The shared information
	 */
	const SPMVShared& getSPMVShared() const
	{
		return *m_shared_infos;
	}

	/*!
	 *  \brief Return a const ref on the ghost information
	 *
	 *  \return The ghost information
	 */
	const SPMVGhosts& getSPMVGhosts() const
	{
		return *m_ghosts_infos;
	}

	/*!
	 *  \brief Return the synchronized state flag
	 *
	 *  \return True when ghost datas are up to date
	 */
	bool isSynchronized() const
	{
		MPI_Status* msg_status = new MPI_Status[m_nb_msg];
		YALLA_ERROR_CHECK(ParUtils::Waitall(m_nb_msg, m_request_status, msg_status));
		delete[] msg_status;
		m_synchronized = true;
		return m_synchronized;
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "GeneralSynchronizer" The name of the class
	 */
	const char* getClassName() const
	{
		return "SPMVSynchronization";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SPMVSynchronization. \n
	 *  These methods provide various utilities to the class SPMVSynchronization
	 */
	//@{

	/*!
	 *  \brief Synchronize the datas
	 *
	 *  This method initiates the synchronization between procs by sending all shared infomation to the relevant receivers and receiving all ghosts information from the relevant sources. \n
	 *  Communications are non blocking, and therefore the datas are up to date only when the isSynchronized returns. \n
	 *
	 *  \param[in] _datas The buffer to receive ghosts elements
	 *  \param[in] _ownSize The offset where shared elements begin in the DistVector
	 */
	void synchronize(data_type* _datas, const int _ownSize) const
	{
		m_synchronized = false;
		const int nbSharedElements = m_shared_infos->getNbElements();
		const int sharedNbReceiver = m_shared_infos->getNbReceiver();
		const int* sharedLIDs = m_shared_infos->getLID();
		const int* sharedNbElementsByProc = m_shared_infos->getNbElementsByProcList();
		const int* sharedDest = m_shared_infos->getReceiverList();
		for(int i=0;i<nbSharedElements;++i)
			m_datas[i] = _datas[sharedLIDs[i]];

		for(int i=0;i<sharedNbReceiver;++i)
		{
			const int offset = sharedNbElementsByProc[i];
			const int dest = sharedDest[i];
			const int nbElements = m_shared_infos->getNbElementsByProc(i);
			YALLA_ERROR_CHECK(ParUtils::IsendTo(&m_datas[offset], nbElements, PromoteToMPI<DataType>::data_type(), dest));
		}

		const int* ghostSenderList = m_ghosts_infos->getSenderList();
		const int* nbElementsForProcList = m_ghosts_infos->getNbElementsForProcList();
		for(int i=0;i<m_nb_msg;++i)
		{
			const int src = ghostSenderList[i];
			const int offset = _ownSize+nbElementsForProcList[i];
			const int nbElements = m_ghosts_infos->getNbElementsForProc(i);
			YALLA_ERROR_CHECK(ParUtils::IRecvFrom(&_datas[offset], nbElements, PromoteToMPI<DataType>::data_type(), src, -1, ParUtils::ThisComm, &m_request_status[i]));
		}
	}

	/*!
	 *  \brief Display synchronizer information on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _sync The synchronizer elements to display
	 */
	friend std::ostream& operator<<(std::ostream& _os, const SPMVSynchronization& _sync)
	{
		_os << _sync.getClassName() << "\n";
		_os << _sync.getSPMVGhosts() << "\n";
		_os << _sync.getSPMVShared();
		return _os;
	}
	//@}
};

YALLA_END_NAMESPACE

#endif
