#include "GeneralSynchronizer.h"

/*!
 *  \file GeneralSynchronizer.cpp
 *  \brief General synchronizer system implementation
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

GeneralSynchronizer::
GeneralSynchronizer(const GeneralGhosts* _ghostsInfos, const GeneralShared* _sharedInfos) : m_ghosts_infos(_ghostsInfos), m_shared_infos(_sharedInfos), m_nb_msg(m_ghosts_infos->getNbSender())
{
	m_synchronized = false;
	m_request_status = new MPI_Request[m_nb_msg];
}

GeneralSynchronizer::
~GeneralSynchronizer()
{
	delete[] m_request_status;
	delete m_ghosts_infos;
	delete m_shared_infos;
}

bool
GeneralSynchronizer::
isSynchronized() const
{
	MPI_Status* msg_status;
	msg_status = new MPI_Status[m_nb_msg];
	YALLA_ERROR_CHECK(ParUtils::Waitall(m_nb_msg, m_request_status, msg_status));
	delete[] msg_status;
	return true;
}
