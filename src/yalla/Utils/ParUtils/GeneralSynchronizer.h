#pragma once
#ifndef yalla_general_synchronizer_h
#define yalla_general_synchronizer_h

/*!
 *  \file GeneralSynchronizer.h
 *  \brief General synchronizer system
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "GeneralGhosts.h"
#include "GeneralShared.h"
#include "ParUtils.h"
#include "yalla/Utils/TypeTraits/PromoteToMPI.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class GeneralSynchronizer
 *  \brief General synchronizer system
 * 
 *  \details
 *  This class allows to synchronize elements between different procs. \n
 *  It stores a GeneralGhosts for updating the elements that need to be received from other sources and a GeneralShared for updating elements that needs to be sent to other procs. \n
 *  The synchronization is done by non blocking communication through the synchronize method. \n
 *  The method isSynchronized allow to ensure that all informations have been received before proceeding.
 *
 *  \warning This class should always take the ownership of the ghosts and shared information and therefore is in charge of freeing the memory for those objects
 */
class GeneralSynchronizer
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef GeneralSynchronizer this_type;
	//@}
 private:
	//! \brief Ghosts information
	const GeneralGhosts* m_ghosts_infos;
	//! \brief Shared information
	const GeneralShared* m_shared_infos;
	//! \brief Number of message to receive
	const int m_nb_msg;
	//! \brief MPI request status for incoming messages
	MPI_Request* m_request_status;
	//! \brief Synchronization state flag
	bool m_synchronized;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class GeneralSynchronizer
	 */
	//@{
	/*! 
	 *  \brief Unique constructor
	 *
	 *  Store the ghosts and shared information
	 */
	explicit GeneralSynchronizer(const GeneralGhosts* _ghostsInfos, const GeneralShared* _sharedInfos);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class GeneralSynchronizer
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class, i.e. the m_request_status, m_ghosts_infos and m_shared_infos pointers.
	 */
	virtual ~GeneralSynchronizer();
	//@}
	/*! \name Public Getter
	 *
	 *  Public Getter of the class GeneralSynchronizer. \n
	 *  These methods give access in read-only mode to the datas/information of the class GeneralSynchronizer
	 */
	//@{

	/*!
	 *  \brief Return a const ref on the shared information
	 *
	 *  \return The shared information
	 */
	const GeneralShared& getGeneralShared() const
	{
		return *m_shared_infos;
	}

	/*!
	 *  \brief Return a const ref on the ghost information
	 *
	 *  \return The ghost information
	 */
	const GeneralGhosts& getGeneralGhosts() const
	{
		return *m_ghosts_infos;
	}

	/*!
	 *  \brief Return the synchronized state flag
	 *
	 *  \return True when ghost datas are up to date
	 */
	bool isSynchronized() const;

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "GeneralSynchronizer" The name of the class
	 */
	const char* getClassName() const
	{
		return "GeneralSynchronizer";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class GeneralSynchronizer. \n
	 *  These methods provide various utilities to the class GeneralSynchronizer
	 */
	//@{

	/*!
	 *  \brief Synchronize the datas
	 *
	 *  This method initiates the synchronization between procs by sending all shared infomation to the relevant receivers and receiving all ghosts information from the relevant sources. \n
	 *  Communications are non blocking, and therefore the datas are up to date only when the isSynchronized returns. \n
	 *
	 *  \param[in] _recvBuffer The buffer to receive ghosts elements
	 *  \param[in] _sndBuffer The buffer to send shared elements
	 *
	 *  \warning Datas in the send buffer are supposed to be store contiguously
	 */
	template<typename DataType>
		void synchronize(DataType* _recvBuffer, DataType* _sndBuffer) const
	{
		int totalNbSentElements = 0;
		for(int i=0;i<m_shared_infos->getNbReceiver();++i)
		{
			const int dest = m_shared_infos->getReceiverList()[i];
			// WARNING: if the elements to send are not contiguous, the following line will not be correct
			const int offset = totalNbSentElements;
			const int nbElements = m_shared_infos->getNbElementsByProc(i);
			totalNbSentElements+= nbElements;
			YALLA_ERROR_CHECK(ParUtils::IsendTo(&_sndBuffer[offset], nbElements, PromoteToMPI<DataType>::data_type(), dest));
		}
		int totalNbReceivedElements = 0;
		for(int i=0;i<m_nb_msg;++i)
		{
			const int src = m_ghosts_infos->getSenderList()[i];
			const int offset = totalNbReceivedElements;
			const int nbElements = m_ghosts_infos->getNbElementsForProc(i);
			totalNbReceivedElements +=nbElements;
			YALLA_ERROR_CHECK(ParUtils::IRecvFrom(&_recvBuffer[offset], nbElements, PromoteToMPI<DataType>::data_type(), src, -1, ParUtils::ThisComm, &m_request_status[i]));
		}
	}

	/*!
	 *  \brief Display synchronizer information on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _sync The synchronizer elements to display
	 */
	friend std::ostream& operator<<(std::ostream& _os, const GeneralSynchronizer& _sync)
	{
		_os << _sync.getClassName() << "\n";
		_os << _sync.getGeneralGhosts() << "\n";
		_os << _sync.getGeneralShared();
		return _os;
	}
	//@}
};

YALLA_END_NAMESPACE

#endif
