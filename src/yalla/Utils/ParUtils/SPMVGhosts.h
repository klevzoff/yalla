#pragma once
#ifndef yalla_spmv_ghosts_h
#define yalla_spmv_ghosts_h

/*!
 *  \file SPMVGhosts.h
 *  \brief SPMV ghost system
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/ParUtils/ParUtils.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SPMVGhosts
 *  \brief SMPV ghost system
 * 
 *  \details
 *  This class allows to specify the required information that needs to be communicated to a proc for a SPMV. \n
 *  The different informations are stored in the following attributes:
 *  - The total number of elements that need to be received (m_nb_elements)
 *  - The total number of sources (m_nb_sender)
 *  - The list of sources (m_nb_sender_list)
 *  - The number of elements by source (m_nb_elements_by_sender)
 *  - The GIDs of the elements that will be received (m_global_ids)
 *
 *  Together with the SPMVShared, this class is stored in a synchronizer that allows to update the required elements. \n
 *  This class probably became a duplicate of the GeneralGhosts and one of them might be removed after deeper verification
 */
class SPMVGhosts
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef SPMVGhosts this_type;
	//@}
 private:
	//! \brief The total number of elements that need to be received
	int m_nb_elements;
	//! \brief The total number of sources
	int m_nb_sender;
	//! \brief The source list
	int* m_sender_list;
	//! \brief The number of elements by source
	int* m_nb_elements_by_sender;
	//! \brief The GIDs of the required elements
	int* m_global_ids;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SPMVGhosts
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Set all values to zero
	 */
	explicit SPMVGhosts();

	/*! 
	 *  \brief Constructor
	 *
	 *  Copy in m_nb_elements and m_nb_sender the values of _nbElements, _nbSender. \n
	 *  Allocate the different pointers and copy the values of _senderList, _nbElementsBySender and _globalIds in m_sender_list, m_nb_elements_by_sender and m_global_ids.
	 *
	 *  \param[in] _nbElements The total number of elements
	 *  \param[in] _nbSender The total number of sources
	 *  \param[in] _senderList The source list
	 *  \param[in] _nbElementsBySender The number of elements by source
	 *  \param[in] _globalIds The GIDs of the elements
	 */
	explicit SPMVGhosts(const int _nbElements, const int _nbSender, const int* _senderList, const int* _nbElementsBySender, const int* _globalIds);
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SPMVGhosts
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class, i.e. m_nb_elements_by_sender, m_global_ids and m_sender_list.
	 */
	virtual ~SPMVGhosts();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SPMVGhosts. \n
	 *  These methods give access in read-only mode to the datas/information of the class SPMVGhosts
	 */
	//@{

	/*!
	 *  \brief Return the total number of elements
	 *
	 *  \return The total number of elements
	 */
	int getNbElements() const;

	/*!
	 *  \brief Return the total number of sources
	 *
	 *  \return The total number of sources
	 */
	int getNbSender() const;

	/*!
	 *  \brief Return the sender list
	 *
	 *  \return The sender list
	 */
	const int* getSenderList() const;

	/*!
	 *  \brief Return the number of element for a source
	 *
	 *  \param[in] _procRank The source
	 *
	 *  \return The number of elements for a source
	 */
	int getNbElementsForProc(const int _procRank) const;

	/*!
	 *  \brief Return the list of the number of element for all sources
	 *
	 *  \return The list of the number of element for all sources
	 */
	const int* getNbElementsForProcList() const;

	/*!
	 *  \brief Return the list of the GIDs that need to be received
	 *
	 *  \return The list of the GIDs that need to be received
	 */
	const int* getGIDs() const;

	/*!
	 *  \brief Return the list of GIDs that need to be received for a source
	 *
	 *  \param[in] _procRank The source
	 *
	 *  \return The list of GIDs that need to be received for a source
	 */
	const int* getGIDsForProc(const int _procRank) const;

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SPMVGhosts" The name of the class
	 */
	const char* getClassName() const
	{
		return "SPMVGhosts";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SPMVGhosts. \n
	 *  These methods provide various utilities to the class SPMVGhosts
	 */
	//@{
	/*!
	 *  \brief Display ghosts information on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _ghost The ghosts elements to display
	 */
	friend std::ostream& operator<<(std::ostream& _os, const SPMVGhosts& _ghost)
	{
		_os << _ghost.getClassName() << " - ";
		_os << "CPU: " << ParUtils::getProcRank() << " - NumGhostsElements: " << _ghost.getNbElements() << " - NumGhostsSources: " << _ghost.getNbSender() << "\n";
		for(int i=0;i<_ghost.getNbSender();++i)
		{
			_os << "NumOfElementsToBeReceived: " << _ghost.getNbElementsForProc(i) << " from source: " << _ghost.getSenderList()[i] << "\n";
			for(int j=0; j<_ghost.getNbElementsForProc(i);++j)
				_os << "GID: " << _ghost.getGIDsForProc(i)[j] << "\n";
		}
		return _os;
	}
	//@}
};

YALLA_END_NAMESPACE

#endif
