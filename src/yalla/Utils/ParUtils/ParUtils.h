#pragma once
#ifndef yalla_par_utils_h
#define yalla_par_utils_h

/*!
 *  \file ParUtils.h
 *  \brief Parallel utilities
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#ifdef WITH_MPI
#include "mpi.h"
#endif
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToMPI.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
#ifndef WITH_MPI
//! \brief Empty MPI_Request struct for use without MPI library
struct MPI_Request
{
	;
};

//! \brief Empty MPI_Status struct for use without MPI library
struct MPI_Status
{
	int MPI_SOURCE;
	MPI_Status()
	{
		MPI_SOURCE = 0;
	}
};

//! \brief Empty MPI_Op struct for use without MPI library
struct MPI_Op
{
	;
};
#endif

/*!
 *  \namespace ParUtils
 *  \brief Parallel utilities
 *
 *  \details
 *  This class provides different utilities that wraps MPI functions. \n
 */
YALLA_BEGIN_NAMESPACE(ParUtils)
#ifdef WITH_MPI
//! \brief Mpi communicator
static MPI_Comm ThisComm = MPI_COMM_WORLD;
#else
#define MPI_Comm int
//! \brief Mpi communicator
static MPI_Comm ThisComm = 0;
#endif

/*! 
 *  \brief Return the rank of the current process
 *
 *  \details
 *  In the case of a distributed execution, this method returns the rank of the process. \n
 *  In the case of a sequential execution, this method returns 0. \n
 *
 *  \return The rank of the current process
 */
int getProcRank();

/*! 
 *  \brief Return the number of processes
 *
 *  \details
 *  In the case of a distributed execution, this method returns the number of processes. \n
 *  In the case of a sequential execution, this method returns 1. \n
 *
 *  \return The number of processes
 */
int getNumProc();

/*! 
 *  \brief Return the id of the master proc
 *
 *  \details
 *
 *  \return 0 The id of the master proc
 */
int masterProc();

/*! 
 *  \brief Indicates if the current process is the master process.
 *
 *  \details
 *  In the case of a distributed execution, this method returns true if the current process is the master process. \n
 *  In the case of a sequential execution, this method returns true. \n
 *
 *  \return A flag indicating if the current process is the master process
 */
bool iAmMaster();

/*! 
 *  \brief Blocking send a message to a proc
 *
 *  \details
 *  In the case of a distributed execution, this method sends the message _datas to the process _toProc. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _datas The message to send
 *  \param[in] _size The size of the message to send
 *  \param[in] _dataType The type of the datas to send
 *  \param[in] _toProc The id of the proc to send
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *
 *  \return The error code
 */
int SendTo(void* _datas, const int _size, const MPI_Datatype _dataType, const int _toProc, const int _tag = 0, const MPI_Comm _comm = ThisComm);

/*! 
 *  \brief Non blocking send a message to a proc
 *
 *  \details
 *  In the case of a distributed execution, this method sends the message _datas to the process _toProc. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _datas The message to send
 *  \param[in] _size The size of the message to send
 *  \param[in] _dataType The type of the datas to send
 *  \param[in] _toProc The id of the proc to send
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *  \param[in] _request The request of the message
 *
 *  \return The error code
 */
int IsendTo(void* _datas, const int _size, const MPI_Datatype _dataType, const int _toProc, const int _tag = 0, const MPI_Comm _comm = ThisComm, MPI_Request* _request = 0);

/*! 
 *  \brief Blocking send a message to the master proc
 *
 *  \details
 *  In the case of a distributed execution, this method sends the message _datas to the master process. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _datas The message to send
 *  \param[in] _size The size of the message to send
 *  \param[in] _dataType The type of the datas to send
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *
 *  \return The error code
 */
int SendToMaster(void* _datas, const int _size, const MPI_Datatype _dataType, const int _tag = 0, const MPI_Comm _comm = ThisComm);

/*! 
 *  \brief Non blocking send a message to the master proc
 *
 *  \details
 *  In the case of a distributed execution, this method sends the message _datas to the master process. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _datas The message to send
 *  \param[in] _size The size of the message to send
 *  \param[in] _dataType The type of the datas to send
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *  \param[in] _request The request of the message
 *
 *  \return The error code
 */
int IsendToMaster(void* _datas, const int _size, const MPI_Datatype _dataType, const int _tag = 0, const MPI_Comm _comm = ThisComm, MPI_Request* _request = 0);

/*! 
 *  \brief Blocking receive a message from a proc
 *
 *  \details
 *  In the case of a distributed execution, this method receive the message _datas from the process _fromProc. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _datas The message to send
 *  \param[in] _size The size of the message to send
 *  \param[in] _dataType The type of the datas to send
 *  \param[in] _fromProc The id of the proc to send
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *  \param[in] _status The status of the message
 *
 *  \return The error code
 */
int RecvFrom(void* _datas, const int _size, const MPI_Datatype _dataType, const int _fromProc, const int _tag = -1, const MPI_Comm _comm = ThisComm, MPI_Status* _status = 0);

/*! 
 *  \brief Non blocking receive a message from a proc
 *
 *  \details
 *  In the case of a distributed execution, this method receive the message _datas from the process _fromProc. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _datas The message to send
 *  \param[in] _size The size of the message to send
 *  \param[in] _dataType The type of the datas to send
 *  \param[in] _fromProc The id of the proc to send
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *  \param[in] _request The request of the message
 *
 *  \return The error code
 */
int IRecvFrom(void* _datas, const int _size, const MPI_Datatype _dataType, const int _fromProc, const int _tag = -1, const MPI_Comm _comm = ThisComm, MPI_Request* _request = 0);

/*! 
 *  \brief Blocking receive a message from the master proc
 *
 *  \details
 *  In the case of a distributed execution, this method receive the message _datas from the master process. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _datas The message to send
 *  \param[in] _size The size of the message to send
 *  \param[in] _dataType The type of the datas to send
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *  \param[in] _status The status of the message
 *
 *  \return The error code
 */
int RecvFromMaster(void* _datas, const int _size, const MPI_Datatype _dataType, const int _tag = -1, const MPI_Comm _comm = ThisComm, MPI_Status* _status = 0);

/*! 
 *  \brief Non blocking receive a message from the master proc
 *
 *  \details
 *  In the case of a distributed execution, this method receive the message _datas from the master process. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _datas The message to send
 *  \param[in] _size The size of the message to send
 *  \param[in] _dataType The type of the datas to send
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *  \param[in] _request The request of the message
 *
 *  \return The error code
 */
int IRecvFromMaster(void* _datas, const int _size, const MPI_Datatype _dataType, const int _tag = -1, const MPI_Comm _comm = ThisComm, MPI_Request* _request = 0);

/*! 
 *  \brief Probe a message from a proc
 *
 *  \details
 *  In the case of a distributed execution, this method probe a message from the process _fromProc. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _fromProc The message to send
 *  \param[in] _status The status of the message
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *
 *  \return The error code
 */
int ProbeFrom(const int _fromProc, MPI_Status* _status, const int _tag = -1, const MPI_Comm _comm = ThisComm);

/*! 
 *  \brief Probe a message from the master proc
 *
 *  \details
 *  In the case of a distributed execution, this method probe a message from the master process. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _status The status of the message
 *  \param[in] _tag The tag of the message
 *  \param[in] _comm The communicator used to send the message
 *
 *  \return The error code
 */
int ProbeFromMaster(MPI_Status* _status, const int _tag = -1, const MPI_Comm _comm = ThisComm);

/*! 
 *  \brief Count the size of a message
 *
 *  \details
 *  In the case of a distributed execution, this method counts the size of a message. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _status The status of the message
 *  \param[in] _dataType The type of the datas to send
 *  \param[out] _count The size of the message
 *
 *  \return The error code
 */
int getCount(MPI_Status* _status, const MPI_Datatype _dataType, int * _count);

/*! 
 *  \brief Reduce scatter operation
 *
 *  \details
 *  In the case of a distributed execution, this method reduce-scatter a message between all procs. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _outDatas The datas to reduce-scatter
 *  \param[in,out] _inDatas The buffer to stored the scattered datas
 *  \param[in] _numReceivedElements Number of elements to scatter for each proc
 *  \param[in] _dataType The MPI data type
 *  \param[in] _op The reduce operator
 *  \param[in] _comm The communicator
 *
 *  \return The error code
 */
int reduceScatter(void* _outDatas, void* _inDatas, int* _numReceivedElements, const MPI_Datatype _dataType, MPI_Op _op, MPI_Comm _comm = ThisComm);

/*! 
 *  \brief All reduce operation
 *
 *  \details
 *  In the case of a distributed execution, this method reduce a message to all procs. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _outDatas The datas to reduce
 *  \param[in,out] _inDatas The buffer to stored the received datas
 *  \param[in] _numElements Number of elements in the send buffer
 *  \param[in] _dataType The MPI data type
 *  \param[in] _op The reduce operator
 *  \param[in] _comm The communicator
 *
 *  \return The error code
 */
int Allreduce(void* _outDatas, void* _inDatas, int _numElements, const MPI_Datatype _dataType, MPI_Op _op, MPI_Comm _comm = ThisComm);

/*! 
 *  \brief Wait all operation
 *
 *  \details
 *  In the case of a distributed execution, this method waits that all message are finished before returning. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _count Number of message request
 *  \param[in] _requests The requests list
 *  \param[in,out] _statuses The status of the requests
 *
 *  \return The error code
 */
int Waitall(int _count, MPI_Request* _requests, MPI_Status* _statuses);

/*! 
 *  \brief Reduce operation
 *
 *  \details
 *  In the case of a distributed execution, this method reduce a message to a specified proc. \n
 *  In the case of a sequential execution, this method returns YALLA_SUCCESS. \n
 *
 *  \param[in] _sendbuf The datas to reduce
 *  \param[in,out] _recvbuf The buffer to stored the received datas
 *  \param[in] _count Number of elements in the send buffer
 *  \param[in] _dataType The MPI data type
 *  \param[in] _op The MPI operation
 *  \param[in] _root Rank of root process
 *  \param[in] _comm The communicator
 *
 *  \return The error code
 */
int Reduce(void* _sendbuf, void* _recvbuf, int _count, MPI_Datatype _dataType, MPI_Op _op, int _root = ParUtils::masterProc(), MPI_Comm _comm = ThisComm);

YALLA_END_NAMESPACE

YALLA_END_NAMESPACE
#endif
