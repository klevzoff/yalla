#pragma once
#ifndef yalla_types_and_def_h
#define yalla_types_and_def_h

/*!
 *  \file TypesAndDef.h
 *  \brief Typedefs, forward declaration and usefull macros
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#ifndef _WIN32
#include <stdint.h>
#endif

#ifdef _MSC_VER
//! \brief To force the non-inlining of a function
#define YALLA_NOINLINE __declspec(noinline)
//! \brief To force the inlining of a function
#define YALLA_INLINE __declspec(__forceinline)
#else
//! \brief To force the non-inlining of a function
#define YALLA_NOINLINE noinline
//! \brief To force the inlining of a function
#define YALLA_INLINE inline
#endif
#ifndef WITH_MPI
/*! \brief To exit the code
 *
 *  \param err Error code
 */
#define YALLA_ABORT(err) abort()
#else
/*! \brief To exit the code
 *
 *  \param err Error code
 */
#define YALLA_ABORT(err) MPI_Barrier(ParUtils::ThisComm); MPI_Abort(ParUtils::ThisComm, err);
#endif

/*! \brief Begin namespace macro
 *
 *  \param name The namespace name
 */
#define YALLA_BEGIN_NAMESPACE(name) namespace name {
//! \brief End namespace macro
#define YALLA_END_NAMESPACE }
/*! \brief Using namespace macro
 *
 *  \param name The namespace name
 */
#define YALLA_USING_NAMESPACE(name) using namespace name;

/*! \brief Return code check for runtime errors
 *
 *  \param x The error code
 */
#define YALLA_ERROR_CHECK(x) do {																				\
		int retval = (x);																										\
		if (retval != 0) {																									\
			fprintf(stderr, "Runtime error: %s returned %d at %s:%d\n", #x, retval, __FILE__, __LINE__); \
		}																																		\
	} while (0)

//! \brief Forward declaration
class Epetra_CrsMatrix;
//! \brief Forward declaration
class Epetra_Vector;

/*!
 *  \namespace yalla
 *  \brief Global namespace du projet
 *
 *  Global namespace du projet
 */
YALLA_BEGIN_NAMESPACE(yalla)
//! \brief Enum for transposition
enum Transpose
{
	//! \brief Transpose
	Trans,
	//! \brief No transpose
	NoTrans
};
	
//! \brief Enum for return code
enum YallaErrorCode
	{
		//! \brief No error
		YALLA_SUCCESS = 0,
		//! \brief Error
		YALLA_ERROR = 1
	};

//! Forward declaration
template<typename DataType, typename MatrixImpl>
  class DistDenseMatrix;

//! Forward declaration
template<typename DataType, typename MatrixImpl>
  class DistSparseMatrix;

//! Forward declaration
template<typename DataType, typename MatrixImpl>
  class SeqDenseMatrix;

//! Forward declaration
template<typename DataType, typename MatrixImpl>
  class SeqSparseMatrix;

//! Forward declaration
template<typename DataType>
class SeqDenseMatrixRowMajor;

//! Forward declaration
template<typename DataType>
class SeqDenseMatrixColMajor;

//! Forward declaration
template<typename DataType>
class SeqCSRMatrix;

//! Forward declaration
template<typename DataType>
class SeqCSRMKLMatrix;

//! Forward declaration
template<typename DataType>
class DistDenseMatrixRowMajor;

//! Forward declaration
template<typename DataType>
class DistCSRMatrix;

template<typename DataType>
class DistCSRMKLMatrix;

template<typename TrilinosImplType>
class EpetraMatrix;

//! Forward declaration
template<typename DataType>
class DistVector;

//! Forward declaration
template<typename DataType>
class SeqVector;

template<typename TrilinosImplType>
class EpetraVector;

//! Forward declaration
class Profiler;

//! Forward declaration
class MemoryCheck;

//! Forward declaration
class SeqDenseMatrixGraph;

//! Forward declaration
class SeqCSRMatrixGraph;

//! Forward declaration
class DistCSRMatrixGraph;

//! \brief Integer sequential dense row major matrix
typedef SeqDenseMatrix<int, SeqDenseMatrixRowMajor<int> > iDenseMatrixRowMajor;
//! \brief Single precision sequential dense row major matrix
typedef SeqDenseMatrix<float, SeqDenseMatrixRowMajor<float> > sDenseMatrixRowMajor;
//! \brief Double precision sequential dense row major matrix
typedef SeqDenseMatrix<double, SeqDenseMatrixRowMajor<double> > dDenseMatrixRowMajor;

//! \brief Integer sequential dense col major matrix
typedef SeqDenseMatrix<int, SeqDenseMatrixColMajor<int> > iDenseMatrixColMajor;
//! \brief Single precision sequential dense col major matrix
typedef SeqDenseMatrix<float, SeqDenseMatrixColMajor<float> > sDenseMatrixColMajor;
//! \brief Double precision sequential dense col major matrix
typedef SeqDenseMatrix<double, SeqDenseMatrixColMajor<double> > dDenseMatrixColMajor;

//! \brief Integer sequential CSR matrix
typedef SeqSparseMatrix<int, SeqCSRMatrix<int> > iCSRMatrix;
//! \brief Single precision sequential CSR matrix
typedef SeqSparseMatrix<float, SeqCSRMatrix<float> > sCSRMatrix;
//! \brief Double precision sequential CSR matrix
typedef SeqSparseMatrix<double, SeqCSRMatrix<double> > dCSRMatrix;

//! \brief Integer sequential vector
typedef SeqVector<int> iDenseVector;
//! \brief Single precision sequential vector
typedef SeqVector<float> sDenseVector;
//! \brief Double precision sequential vector
typedef SeqVector<double> dDenseVector;

//! \brief Integer distributed vector
typedef DistVector<int> iDistDenseVector;
//! \brief Single precision distributed vector
typedef DistVector<float> sDistDenseVector;
//! \brief Double precision distributed vector
typedef DistVector<double> dDistDenseVector;

//! \brief Integer distributed dense row major matrix
typedef DistDenseMatrix<int, DistDenseMatrixRowMajor<int> > iDistDenseMatrixRowMajor;
//! \brief Single precision distributed dense row major matrix
typedef DistDenseMatrix<float, DistDenseMatrixRowMajor<float> > sDistDenseMatrixRowMajor;
//! \brief Double precision distributed dense row major matrix
typedef DistDenseMatrix<double, DistDenseMatrixRowMajor<double> > dDistDenseMatrixRowMajor;

//! \brief Integer distributed CSR matrix
typedef DistSparseMatrix<int, DistCSRMatrix<int> > iDistCSRMatrix;
//! \brief Single precision distributed CSR matrix
typedef DistSparseMatrix<float, DistCSRMatrix<float> > sDistCSRMatrix;
//! \brief Double precision distributed CSR matrix
typedef DistSparseMatrix<double, DistCSRMatrix<double> > dDistCSRMatrix;

#ifdef WITH_TRILINOS
//! \brief Double precision distributed Epetra CSR matrix
typedef DistSparseMatrix<double, EpetraMatrix<Epetra_CrsMatrix> > dDistEpetraCSRMatrix;
//! \brief Double precision distributed Epetra vector
typedef EpetraVector<Epetra_Vector> dDistEpetraVector;
#endif
YALLA_END_NAMESPACE
#endif
