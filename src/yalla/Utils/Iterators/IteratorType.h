#pragma once
#ifndef yalla_iterator_type_h
#define yalla_iterator_type_h

/*!
 *  \file IteratorType.h
 *  \brief Define the type of iterator used for the data structures
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class IteratorType
 *  \brief Define the type of iterator used for the data structures
 * 
 *  \details 
 *  This class allows to define the type of iterator used for the data structures through three templates parameters:
 *  \tparam flag If the iterator is const or not
 *  \tparam isTrue Data type if the iterator is const
 *  \tparam isFalse Data type if the iterator is non const
 */
template<bool flag, typename isTrue, typename isFalse>
	struct IteratorType;

/*!
 *  \class IteratorType
 *  \brief Specialization for the const case
 * 
 *  \details 
 *  If the const flag is true, then the data_type is set to isTrue
 *
 *  \tparam isTrue Data type if the iterator is const
 *  \tparam isFalse Data type if the iterator is non const
 */
template<typename isTrue, typename isFalse>
	struct IteratorType<true, isTrue, isFalse>
{
	typedef isTrue data_type;
};

/*!
 *  \class IteratorType
 *  \brief Specialization for the non const case
 * 
 *  \details 
 *  If the const flag is false, then the data_type is set to isFalse
 *
 *  \tparam isTrue Data type if the iterator is const
 *  \tparam isFalse Data type if the iterator is non const
 */
template<typename isTrue, typename isFalse>
	struct IteratorType<false, isTrue, isFalse>
{
	typedef isFalse data_type;
};

/*!
 *  \class hasMapping
 *  \brief Class defining that a row iterator is using a special mapping
 * 
 *  \details 
 *  This class is used only for DistCSRMKL matrices. Indeed, for the non-diagonal block, the index is obtained by using the LIDs in the non-diagonal block to the mapping in the diagonal block. To specialize the row iterator accordingly, we need a special class.
 */
struct hasMapping
{
	;
};

/*!
 *  \class hasMapping
 *  \brief Class defining that a row iterator is not using a special mapping
 * 
 *  \details 
 *  This class is used only for DistCSRMKL matrices. Indeed, for the diagonal block, the index is directly obtained by using the LIDs.
 */
struct hasNoMapping
{
	;
};

/*!
 *  \class MappingType
 *  \brief Define the type of mapping used in the row iterator
 * 
 *  \details 
 *  This class allows to define the type of mapping used in the row iterator used for the data structures through three templates parameters:
 *  \tparam flag If the iterator uses a special mapping or not
 *  \tparam isTrue Data type if the iterator uses a special mapping
 *  \tparam isFalse Data type if the iterator doesn't use a special mapping
 */
template<bool flag, typename isTrue, typename isFalse>
	struct MappingType;

/*!
 *  \class MappingType
 *  \brief Specialization for the special mapping case
 * 
 *  \details 
 *  If the const flag is true, then the data_type is set to isTrue
 *
 *  \tparam isTrue Data type if the iterator uses a special mapping
 *  \tparam isFalse Data type if the iterator doesn't use a special mapping
 */
template<typename isTrue, typename isFalse>
	struct MappingType<true, isTrue, isFalse>
{
	typedef isTrue data_type;
};

/*!
 *  \class MappingType
 *  \brief Specialization for the usual mapping case
 * 
 *  \details 
 *  If the const flag is false, then the data_type is set to isFalse
 *
 *  \tparam isTrue Data type if the iterator uses a special mapping
 *  \tparam isFalse Data type if the iterator doesn't use a special mapping
 */
template<typename isTrue, typename isFalse>
	struct MappingType<false, isTrue, isFalse>
{
	typedef isFalse data_type;
};
YALLA_END_NAMESPACE
#endif
