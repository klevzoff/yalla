#pragma once
#ifndef yalla_row_iterator_dist_csr_matrix_h
#define yalla_row_iterator_dist_csr_matrix_h

/*!
 *  \file RowIteratorDistCSRMatrix.h
 *  \brief Row iterator for distributed CSR matrix
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "IteratorType.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class RowIteratorDistCSRMatrix
 *  \brief Row iterator for distributed CSR matrix
 * 
 *  \details 
 *  Allow to iterate over the rows of a distributed CSR matrix. \n
 *
 *  \tparam MatrixType The type of the matrix
 *  \tparam isConst Const qualifier of the iterator
 *  \tparam hasSpecialMapping If the iterator should use a special mapping or not
 */
template<typename MatrixType, bool isConst = false, bool hasSpecialMapping = false>
	class RowIteratorDistCSRMatrix
{
 public:
 //! \name Public Typedefs
 //@{
 //! \brief Type of the matrix
 typedef MatrixType matrix_type;
 //! \brief Data type of the matrix
 typedef typename matrix_type::data_type data_type;
 //! \brief Type of this class
 typedef RowIteratorDistCSRMatrix<matrix_type, isConst, hasSpecialMapping> this_class;
 //! \brief Type of the reference
 typedef typename IteratorType<isConst, const data_type&, data_type&>::data_type reference_type;
 //! \brief Type of the pointer
 typedef typename IteratorType<isConst, const data_type*, data_type*>::data_type pointer_type;
 //! \brief This type
 typedef typename IteratorType<isConst, const this_class, this_class>::data_type this_type;
 //! \brief Type of the mapping
 typedef typename MappingType<hasSpecialMapping, hasMapping, hasNoMapping>::data_type mapping_type;
 //@}
 private:
 //! \brief The current position
 pointer_type m_position;
 //! \brief The index
 int m_index;
 //! \brief The matrix
 const matrix_type& m_matrix;
 //! \brief The row pointer
 const int* m_row_ptr;
 //! \brief The col pointer
 const int* m_col_ptr;
 //! \brief The mapping pointer
 const int* m_mapping;
 //! \brief The datas
 const pointer_type m_data;
 public:
 /*! \name Public Constructors
	*
	*  Public constructors of the class RowIteratorDistCSRMatrix
	*/
 //@{
 /*! 
	*  \brief Constructor
	*
	*  Copy the current position in m_position. \n
	*  Copy the current index in m_index. \n
	*  Copy the reference on the matrix, the row, col, mapping and data pointers.
	*
	*  \param[in] _val The current position
	*  \param[in] _index The current index
	*  \param[in] _matrix The matrix this class iterates on
	*  \param[in] _rowPtr The row pointer
	*  \param[in] _colPtr The col pointer
	*  \param[in] _mapping The mapping
	*  \param[in] _data The data pointer
	*/
 RowIteratorDistCSRMatrix(pointer_type _val, const int _index, const matrix_type& _matrix, const int* _rowPtr, const int* _colPtr, const int* _mapping, const pointer_type _data) : m_matrix(_matrix), m_row_ptr(_rowPtr), m_col_ptr(_colPtr), m_mapping(_mapping), m_data(_data)
 {
	 m_position = _val;
	 m_index = _index;
 }

 /*! 
	*  \brief Copy constructor
	*
	*  Copy the current position in m_position. \n
	*  Copy the current index in m_index. \n
	*  Copy the reference on the matrix, the row, col, mapping and data pointers.
	*
	*  \param[in] _val The row iterator to copy
	*/
 RowIteratorDistCSRMatrix(this_type& _val) : m_matrix(_val.getMatrix()), m_row_ptr(_val.getRowPtr()), m_col_ptr(_val.getColPtr()), m_mapping(_val.getMapping()), m_data(_val.getData())
 {
	 m_position = _val.getPosition();
	 m_index = _val.index();
 }

 /*! 
	*  \brief Non const to const constructor
	*
	*  Copy the current position in m_position. \n
	*  Copy the current index in m_index. \n
	*  Copy the reference on the matrix, the row, col, mapping and data pointers.
	*
	*  \param[in] _val The row iterator to copy
	*/
 RowIteratorDistCSRMatrix(const RowIteratorDistCSRMatrix<matrix_type, false>& _val) : m_matrix(_val.getMatrix()), m_row_ptr(_val.getRowPtr()), m_col_ptr(_val.getColPtr()), m_mapping(_val.getMapping()), m_data(_val.getData())
 {
	 m_position = _val.getPosition();
	 m_index = _val.index();
 }
 //@}

 /*! \name Public Destructor
	*
	*  Public destructor of the class RowIteratorDistCSRMatrix
	*/
 //@{
 /*! 
	*  \brief Destructor.
	*
	*  \details
	*  Do nothing (no memory allocated here)
	*/
 virtual ~RowIteratorDistCSRMatrix()
 {
	 ;
 }
 //@}

 /*! \name Public Getter
	*
	*  Public Getter of the class RowIteratorDistCSRMatrix. \n
	*  These methods give access in read-only mode to the datas/information of the class RowIteratorDistCSRMatrix
	*/
 //@{
 /*! 
	*  \brief Return the index of the current row
	*
	*  \details
	*  Calls the method trueIndex which will either use the special mapping, or return the LIDs directly, depending on the mapping template parameter
	*
	*  \return The index of the current row
	*/
 int index() const
 {
	 return this->trueIndex(mapping_type());
 }

 /*! 
	*  \brief Return the real index of the current row
	*
	*  \details
	*  Return the realInde of a row, no matter the mapping template parameter.
	*
	*  \return The index of the current row
	*/
 int realIndex() const
 {
	 return m_index;
 }

 /*! 
	*  \brief Return the trueIndex of the current row
	*
	*  \details
	*  Index when no special mapping is required.
	*
	*  \return The index of the current row
	*/
 int trueIndex( hasNoMapping ) const
 {
	 return m_index;
 }
 
 /*! 
	*  \brief Return the trueIndex of the current row
	*
	*  \details
	*  Index when a special mapping is required.
	*
	*  \return The index of the current row
	*/
 int trueIndex( hasMapping ) const
 {
	 return m_mapping[m_index];
 }

 /*! 
	*  \brief Return the global index of the current row
	*
	*  \details
	*
	*  \return The global index of the current row
	*/
 int globalIndex() const
 {
	 return m_matrix.getMapping(this->trueIndex(mapping_type()));
 }

 /*! 
	*  \brief Return the current position
	*
	*  \details
	*
	*  \return The current position
	*/
 pointer_type getPosition() const
 {
	 return m_position;
 }

 /*! 
	*  \brief Return the matrix over which the class iterates
	*
	*  \details
	*
	*  \return The matrix over which the class iterates
	*/		
 const matrix_type& getMatrix() const
 {
	 return m_matrix;
 }

 /*! 
	*  \brief Return the row pointer of the matrix over which the class iterates
	*
	*  \details
	*
	*  \return Return the row pointer of the matrix over which the class iterates
	*/		
 const int* getRowPtr() const
 {
	 return m_row_ptr;
 }

 /*! 
	*  \brief Return the col pointer of the matrix over which the class iterates
	*
	*  \details
	*
	*  \return Return the col pointer of the matrix over which the class iterates
	*/		
 const int* getColPtr() const
 {
	 return m_col_ptr;
 }

 /*! 
	*  \brief Return the mapping of the matrix over which the class iterates
	*
	*  \details
	*
	*  \return Return the mapping of the matrix over which the class iterates
	*/		
 const int* getMapping() const
 {
	 return m_mapping;
 }

 /*! 
	*  \brief Return the datas pointer of the matrix over which the class iterates
	*
	*  \details
	*
	*  \return Return the datas pointer of the matrix over which the class iterates
	*/		
 const pointer_type getData() const
 {
	 return m_data;
 }
 //@}

 /*! \name Public operators
	*
	*  Public operators of the class RowIteratorDistCSRMatrix. \n
	*/
 //@{

 /*! 
	*  \brief Dereference the current pointer
	*
	*  \details
	*
	*  \return Dereference the current pointer
	*/		
 reference_type operator*() const
 {
	 return *m_position;
 }

 /*! 
	*  \brief Difference operator
	*
	*  \details
	*
	*  \param[in] _val The value to compare
	*
	*  \return True if the position is different than the one in _val
	*/		
 bool operator!=(const this_class& _val) const
 {
	 return (m_position!=_val.getPosition());
 }

 /*! 
	*  \brief Compare operator
	*
	*  \details
	*
	*  \param[in] _val The value to compare
	*
	*  \return True if the position is the same than the one in _val
	*/	
 bool operator==(const this_class& _val) const
 {
	 return (m_position==_val.getPosition());
 }

 /*! 
	*  \brief Increment the current position
	*
	*  \details
	*
	*  \return This
	*/	
 this_class& operator++()
 {
	 m_position += (m_row_ptr[m_index+1] - m_row_ptr[m_index]);
	 ++m_index;
	 return *this;
 }
 //@}
};
YALLA_END_NAMESPACE
#endif
