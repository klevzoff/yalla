#pragma once
#ifndef yalla_col_iterator_seq_dense_col_major_matrix_h
#define yalla_col_iterator_seq_dense_col_major_matrix_h

/*!
 *  \file ColIteratorSeqDenseColMajorMatrix.h
 *  \brief Column iterator for sequential col major dense matrix
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "IteratorType.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class ColIteratorSeqDenseColMajorMatrix
 *  \brief Column iterator for sequential col major dense matrix
 * 
 *  \details 
 *  Allow to iterate over the columns of a sequential col major dense matrix for a given row. \n
 *
 *  \tparam MatrixType The type of the matrix
 *  \tparam isConst Const qualifier of the iterator
 */
template<typename MatrixType, bool isConst = false>
	class ColIteratorSeqDenseColMajorMatrix
{
 public:
 //! \name Public Typedefs
 //@{
 //! \brief Type of the matrix
 typedef MatrixType matrix_type;
 //! \brief Data type of the matrix
 typedef typename matrix_type::data_type data_type;
 //! \brief Type of this class
 typedef ColIteratorSeqDenseColMajorMatrix<matrix_type, isConst> this_class;
 //! \brief Type of the row iterator
 typedef RowIteratorSeqDenseColMajorMatrix<matrix_type, isConst> row_iterator_type;
 //! \brief Type of the reference
 typedef typename IteratorType<isConst, const data_type&, data_type&>::data_type reference_type;
 //! \brief Type of the pointer
 typedef typename IteratorType<isConst, const data_type*, data_type*>::data_type pointer_type;
 //@}
 private:
 //! \brief The row iterator
 const row_iterator_type& m_row_iterator;
 //! \brief The current position
 pointer_type m_position;
 //! \brief The matrix
 const matrix_type& m_matrix;
 public:
 /*! \name Public Constructors
	*
	*  Public constructors of the class ColIteratorSeqDenseColMajorMatrix
	*/
 //@{
 /*! 
	*  \brief Constructor
	*
	*  Copy the current position in m_position. \n
	*  Copy the reference on the row iterator and on the matrix
	*
	*  \param[in] _val The current position
	*  \param[in] _rowIterator The current row
	*/
 ColIteratorSeqDenseColMajorMatrix(pointer_type _val, const row_iterator_type& _rowIterator) : m_row_iterator(_rowIterator), m_matrix(_rowIterator.getMatrix())
 {
	 m_position = _val;
 }
			
 /*! 
	*  \brief Copy constructor
	*
	*  Copy the current position in m_position. \n
	*  Copy the reference on the row iterator and on the matrix
	*
	*  \param[in] _colIterator The col iterator to copy
	*/
 ColIteratorSeqDenseColMajorMatrix(const this_class& _colIterator) :  m_row_iterator(_colIterator.getRowIterator()), m_matrix(_colIterator.getMatrix())
 {
	 m_position = _colIterator.getPosition();
 }
 //@}

 /*! \name Public Destructor
	*
	*  Public destructor of the class ColIteratorSeqDenseColMajorMatrix
	*/
 //@{
 /*! 
	*  \brief Destructor.
	*
	*  \details
	*  Do nothing (no memory allocated here)
	*/
 virtual ~ColIteratorSeqDenseColMajorMatrix()
 {
	 ;
 }
 //@}

 /*! \name Public Getter
	*
	*  Public Getter of the class ColIteratorSeqDenseColMajorMatrix. \n
	*  These methods give access in read-only mode to the datas/information of the class ColIteratorSeqDenseColMajorMatrix
	*/
 //@{
 /*! 
	*  \brief Return the index of the current column
	*
	*  \details
	*
	*  \return The index of the current column
	*/
 int index() const
 {
	 return (m_position - m_row_iterator.getPosition())/m_matrix.getNbRows();
 }

 /*! 
	*  \brief Return the current position
	*
	*  \details
	*
	*  \return The current position
	*/
 pointer_type getPosition() const
 {
	 return m_position;
 }

 /*! 
	*  \brief Return the matrix over which the class iterates
	*
	*  \details
	*
	*  \return The matrix over which the class iterates
	*/		
 const matrix_type& getMatrix() const
 {
	 return m_matrix;
 }

 /*! 
	*  \brief Return the row iterator of the column
	*
	*  \details
	*
	*  \return The row iterator of the column
	*/		
 const row_iterator_type& getRowIterator() const
 {
	 return m_row_iterator;
 }
 //@}

 /*! \name Public operators
	*
	*  Public operators of the class ColIteratorSeqDenseColMajorMatrix. \n
	*/
 //@{

 /*! 
	*  \brief Dereference the current pointer
	*
	*  \details
	*
	*  \return Dereference the current pointer
	*/		
 reference_type operator*() const
 {
	 return *m_position;
 }

 /*! 
	*  \brief Difference operator
	*
	*  \details
	*
	*  \param[in] _val The value to compare
	*
	*  \return True if the position is different than the one in _val
	*/		
 bool operator!=(const this_class& _val) const
 {
	 return (m_position!=_val.getPosition());
 }

 /*! 
	*  \brief Compare operator
	*
	*  \details
	*
	*  \param[in] _val The value to compare
	*
	*  \return True if the position is the same than the one in _val
	*/			
 bool operator==(const this_class& _val) const
 {
	 return (m_position==_val.getPosition());
 }

 /*! 
	*  \brief Increment the current position
	*
	*  \details
	*
	*  \return This
	*/
 this_class& operator++()
 {
	 m_position += m_matrix.getNbRows();
	 return *this;
 }

 /*! 
	*  \brief Decrement the current position
	*
	*  \details
	*
	*  \return This
	*/
 this_class& operator--()
 {
	 m_position -= m_matrix.getNbRows();
	 return *this;
 }

 /*! 
	*  \brief Increment the current position if there is a next element
	*
	*  \details
	*
	*  \return This
	*/
 this_class& next()
 {
	 if(*this!=m_matrix.colEnd(m_row_iterator))
	 {
		 m_position += m_matrix.getNbRows();
		 return *this;
	 }
	 else
		 return *this;
 }

 /*! 
	*  \brief Decrement the current position if there is a previous element
	*
	*  \details
	*
	*  \return This
	*/	
 this_class& prev()
 {
	 if(*this!=m_matrix.colBegin(m_row_iterator))
	 {
		 m_position -= m_matrix.getNbRows();
		 return *this;
	 }
	 else
		 return *this;
 }
 //@}
};
YALLA_END_NAMESPACE
#endif


