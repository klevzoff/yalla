#pragma once
#ifndef yalla_row_iterator_seq_csr_matrix_h
#define yalla_row_iterator_seq_csr_matrix_h

/*!
 *  \file RowIteratorSeqCSRMatrix.h
 *  \brief Row iterator for sequential CSR matrix
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iterator>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "IteratorType.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class RowIteratorSeqCSRMatrix
 *  \brief Row iterator for sequential CSR matrix
 * 
 *  \details 
 *  Allow to iterate over the rows of a sequential CSR matrix. \n
 *
 *  \tparam MatrixType The type of the matrix
 *  \tparam isConst Const qualifier of the iterator
 */
template<typename MatrixType, bool isConst = false>
	class RowIteratorSeqCSRMatrix
	: public std::iterator<std::bidirectional_iterator_tag, typename MatrixType::data_type>
{
 public:
 //! \name Public Typedefs
 //@{
 //! \brief Type of the matrix
 typedef MatrixType matrix_type;
 //! \brief Data type of the matrix
 typedef typename matrix_type::data_type data_type;
 //! \brief Type of this class
 typedef RowIteratorSeqCSRMatrix<matrix_type, isConst> this_class;
 //! \brief Type of the reference
 typedef typename IteratorType<isConst, const data_type&, data_type&>::data_type reference_type;
 //! \brief Type of the pointer
 typedef typename IteratorType<isConst, const data_type*, data_type*>::data_type pointer_type;
 //@}
 private:
 //! \brief The current position
 pointer_type m_position;
 //! \brief The matrix
 const matrix_type& m_matrix;
 //! \brief The current index
 int m_index;
 public:
 /*! \name Public Constructors
	*
	*  Public constructors of the class RowIteratorSeqCSRMatrix
	*/
 //@{
 /*! 
	*  \brief Constructor
	*
	*  Copy the current position in m_position. \n
	*  Copy the current index in m_index. \n
	*  Copy the reference on the matrix
	*
	*  \param[in] _val The current position
	*  \param[in] _index The current index
	*  \param[in] _matrix The matrix this class iterates on
	*/
 RowIteratorSeqCSRMatrix(pointer_type _val, const int _index, const matrix_type& _matrix) : m_matrix(_matrix)
 {
	 m_position = _val;
	 m_index = _index;
 }

 /*! 
	*  \brief Copy constructor
	*
	*  Copy the current position in m_position. \n
	*  Copy the current index in m_index. \n
	*  Copy the reference on the matrix
	*
	*  \param[in] _val The row iterator to copy
	*/
 RowIteratorSeqCSRMatrix(const RowIteratorSeqCSRMatrix<matrix_type, false>& _val) : m_matrix(_val.getMatrix())
 {
	 m_position = _val.getPosition();
	 m_index = _val.index();
 }
 //@}

 /*! \name Public Destructor
	*
	*  Public destructor of the class RowIteratorSeqCSRMatrix
	*/
 //@{
 /*! 
	*  \brief Destructor.
	*
	*  \details
	*  Do nothing (no memory allocated here)
	*/
 virtual ~RowIteratorSeqCSRMatrix()
 {
	 ;
 }
 //@}

 /*! \name Public Getter
	*
	*  Public Getter of the class RowIteratorSeqCSRMatrix. \n
	*  These methods give access in read-only mode to the datas/information of the class RowIteratorSeqCSRMatrix
	*/
 //@{

 /*! 
	*  \brief Return the index of the current row
	*
	*  \details
	*
	*  \return The index of the current row
	*/
 int index() const
 {
	 return m_index;
 }

 /*! 
	*  \brief Return the current position
	*
	*  \details
	*
	*  \return The current position
	*/
 pointer_type getPosition() const
 {
	 return m_position;
 }

 /*! 
	*  \brief Return the matrix over which the class iterates
	*
	*  \details
	*
	*  \return The matrix over which the class iterates
	*/	
 const matrix_type& getMatrix() const
 {
	 return m_matrix;
 }
 //@}

 /*! \name Public operators
	*
	*  Public operators of the class RowIteratorSeqCSRMatrix. \n
	*/
 //@{

 /*! 
	*  \brief Dereference the current pointer
	*
	*  \details
	*
	*  \return Dereference the current pointer
	*/		
 reference_type operator*() const
 {
	 return *m_position;
 }

 /*! 
	*  \brief Difference operator
	*
	*  \details
	*
	*  \param[in] _val The value to compare
	*
	*  \return True if the position is different than the one in _val
	*/		
 bool operator!=(const this_class& _val) const
 {
	 return (m_position!=_val.getPosition());
 }

 /*! 
	*  \brief Compare operator
	*
	*  \details
	*
	*  \param[in] _val The value to compare
	*
	*  \return True if the position is the same than the one in _val
	*/	
 bool operator==(const this_class& _val) const
 {
	 return (m_position==_val.getPosition());
 }

 /*! 
	*  \brief Increment the current position
	*
	*  \details
	*
	*  \return This
	*/
 this_class& operator++()
 {
	 m_position += m_matrix.getRowNnz(m_index);
	 ++m_index;
	 return *this;
 }
 //@}
};
YALLA_END_NAMESPACE
#endif
