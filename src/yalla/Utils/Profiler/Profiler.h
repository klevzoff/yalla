#pragma once
#ifndef yalla_profiler_h
#define yalla_profiler_h

/*!
 *  \file Profiler.h
 *  \brief Profiler
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <map>
#include <string>
#include <utility>
#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#ifdef _WIN32
#include <windows.h>
#include <stdint.h>
#else // UNIX
#include <ctime>
#endif

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class Profiler
 *  \brief Profiler
 * 
 *  \details
 */
class Profiler
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef Profiler this_type;
#ifdef _WIN32
	//! \brief Type of the container (windows)
	typedef std::map<std::string, int64_t> container_type;
#else
	//! \brief Type of the container (unix)
	typedef std::map<std::string, double> container_type;
#endif
	//! \brief Type of the container counter
	typedef std::map<std::string, int> container_counter_type;
 private:
	//! \brief Store the elapsed time between two checkpoints
	container_type m_elapsed_time;
	//! \brief Store the total elapsed time between for a key
	container_type m_total_elapsed_time;
	//! \brief Store the number of time the profiler has been used for a specified key
	container_type m_total_called_time;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class Profiler
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Do nothing
	 */
	explicit Profiler();
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class Profiler
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated by this class)
	 */
	virtual ~Profiler();	
	//@}

	/*!
	 *  \brief Start the profiler for a given key
	 *  
	 *  \param _inString Key to register
	 */
	void start(const std::string& _inString);

	/*!
	 *  \brief Stop the profiler for a given key
	 *  
	 *  \param _inString Key to register
	 */
	void stop(const std::string& _inString);

	/*!
	 *  \brief Return the time in seconds stored for a given key
	 *  
	 *  \param _inString Key registered
	 *
	 *  \return The total elapsed time for a key if the key exists, 0 otherwise
	 */
	double getTime(const std::string& _inString) const;

	/*!
	 *  \brief Return the elapsed time container
	 *  
	 *  \return The elapsed time container
	 */
	const container_type& getElapsedTime() const
	{
		return m_elapsed_time;
	}

	/*!
	 *  \brief Return the total elapsed time container
	 *  
	 *  \return The total elapsed time container
	 */
	const container_type& getTotalTime() const
	{
		return m_total_elapsed_time;
	}

	/*!
	 *  \brief Return the call counter container
	 *  
	 *  \return The call counter container
	 */
	const container_type& getNumberOfCalls() const
	{
		return m_total_called_time;
	}

	/*!
	 *  \brief Display the Profiler information on the standard output
	 *  
	 *  \param[in] _os The output stream
	 *  \param[in] _profil The profiler to display
	 *
	 *  \return The output stream
	 */
	friend std::ostream& operator<<(std::ostream& _os, const Profiler& _profil)
	{
		Profiler::container_type::const_iterator timeIt = _profil.getTotalTime().begin();
		Profiler::container_type::const_iterator numCallIt = _profil.getNumberOfCalls().begin();
#ifdef _WIN32
		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);	
		double toSeconds = static_cast<double>(freq.QuadPart);
#else
		double toSeconds = static_cast<double>(CLOCKS_PER_SEC);
#endif
		for(timeIt = _profil.getTotalTime().begin(); timeIt!=_profil.getTotalTime().end();++timeIt)
		{
			_os << "Fonction :" << numCallIt->first << "\n";
			_os << "Nombre d'appels : " << numCallIt->second << "\n";
			_os << "Temps total ecoule : " << static_cast<double>(timeIt->second) / toSeconds << "\n";
			_os << "Temps moyen par appel : " << static_cast<double>(timeIt->second) / toSeconds / static_cast<double>(numCallIt->second) << "\n";
			++numCallIt;
		}
		return _os;
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "Profiler" The name of the class
	 */
	const char* getClassName() const
	{
		return "Profiler";
	}
};
YALLA_END_NAMESPACE
#endif
