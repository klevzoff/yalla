#include "Profiler.h"
#include <cstdio>
#include <iostream>

/*!
 *  \file Profiler.cpp
 *  \brief Profiler implementation
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

Profiler::
Profiler()
{
  ;
}

Profiler::
~Profiler()
{
  ;
}

void
Profiler::
start(const std::string& _inString)
{
#ifdef _WIN32
  LARGE_INTEGER startCounter;
  DWORD	retval = QueryPerformanceCounter (&startCounter);
  if(retval)
    m_elapsed_time[_inString] = startCounter.QuadPart;
#else
  m_elapsed_time[_inString] = static_cast<double>(std::clock());
#endif
}

void
Profiler::
stop(const std::string& _inString)
{
#ifdef _WIN32
  LARGE_INTEGER endCounter;
  DWORD	retval = QueryPerformanceCounter (&endCounter);
  if(retval)
  {
    int64_t tmp = m_elapsed_time[_inString];
    m_elapsed_time[_inString] = endCounter.QuadPart - tmp;
  }
#else
  double tmp = m_elapsed_time[_inString];
  m_elapsed_time[_inString] = static_cast<double>(std::clock()) - tmp;
#endif
  m_total_elapsed_time[_inString] += m_elapsed_time[_inString];
  ++m_total_called_time[_inString];
}

double
Profiler::
getTime(const std::string& _inString) const
{
  container_type::const_iterator timeIt = m_total_elapsed_time.find(_inString);
#ifdef _WIN32
  LARGE_INTEGER freq;
  QueryPerformanceFrequency(&freq);
	double toSeconds = static_cast<double>(freq.QuadPart);
#else
	double toSeconds = static_cast<double>(CLOCKS_PER_SEC);
#endif
  if(timeIt!=m_total_elapsed_time.end())
    return static_cast<double>(timeIt->second) / toSeconds;
  return 0;
}
