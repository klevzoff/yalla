#pragma once
#ifndef yalla_row_distribution_h
#define yalla_row_distribution_h

/*!
 *  \file RowDistribution.h
 *  \brief Block row distribution scheme
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include <stdlib.h>
#include <set>
#include <map>
#include <vector>
#include <set>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToDist.h"
#include "yalla/Utils/TypeTraits/PromoteToSeq.h"
#include "yalla/Utils/ParUtils/ParUtils.h"
#include "yalla/Utils/ParUtils/SPMVGhosts.h"
#include "yalla/Utils/ParUtils/SPMVShared.h"
#include "yalla/Utils/ParUtils/SPMVSynchronization.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class RowDistribution
 *  \brief Block row distribution scheme
 * 
 *  \details 
 *  Distribute matrices and vector following a block row the partitionning scheme. \n
 *  The partitionning scheme is computed with the first matrix given. \n
 *  All other matrices are distributed following the same partitionning scheme. \n
 *  Vectors are distributed according to this same partitionning scheme.
 *
 *  \tparam DataType The data type of the objects to distribute
 *
 *  \todo There is cleary some cleaning to do in this class. Some code is duplicated and badly organized. A refactoring would be a good idea.
 */
template<typename DataType>
class RowDistribution
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the objects to distribute
	typedef DataType data_type;
	//! \brief Type of this class
	typedef RowDistribution<data_type> this_type;
	//@}
 public:
	//! \brief The number of procs
	int m_num_proc;
	//! \brief Partition
	int* m_partition;
	//! \brief Ghosts information stored on the master proc
	SPMVGhosts** m_global_ghosts_infos;
	//! \brief Shared information stored on the master proc
	SPMVShared** m_global_shared_infos;
	//! \brief Local SPMV synchronizer
	SPMVSynchronization<data_type>* m_synchronizer;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class RowDistribution
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Set all attributes to zero. \n
	 *  The number of procs is stored there.
	 */
	explicit RowDistribution()
	{
		m_partition = 0;
		m_global_ghosts_infos = 0;
		m_global_shared_infos = 0;
		m_synchronizer = 0;
		m_num_proc = ParUtils::getNumProc();
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class RowDistribution
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class, i.e. the m_partition pointer, all the ghosts and shared infos and the local synchronizer.
	 */
	virtual ~RowDistribution()
	{
		delete[] m_partition;
		m_partition = 0;
		if(m_global_ghosts_infos)
		{
			for(int i=0;i<m_num_proc;++i)
			{
				delete m_global_ghosts_infos[i];
				m_global_ghosts_infos[i] = 0;
			}
			delete[] m_global_ghosts_infos;
			m_global_ghosts_infos = 0;
		}
		if(m_global_shared_infos)
		{
			for(int i=0;i<m_num_proc;++i)
			{
				delete m_global_shared_infos[i];
				m_global_shared_infos[i] = 0;
			}
			delete[] m_global_shared_infos;
			m_global_shared_infos = 0;
		}
		delete m_synchronizer;
		m_synchronizer = 0;
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class RowDistribution. \n
	 */
	//@{

	/*! 
	 *  \brief Distribute a matrix with a block row partition
	 *
	 *  \details
	 *  The master proc is in charge of distributing the matrix following a bloc row partition. \n
	 *  It starts by computing the partition, then all SPMV required informations. \n
	 *  Then the master proc packs and send the datas to all other procs. \n
	 *  Then all procs build their local matrix. \n
	 *  Rows and columns share the same LIDs/GIDs mapping. LIDs for the row starts at 0 and ends at nbLocalRows. Then all cols that are not in this mapping are sequentially numbered starting at nbLoalRows+1 to nbLocalCols.
	 *
	 *  Datas are packed in the following pattern [numberOfElements]:
	 *  - number of local rows [1]
	 *  - number of local cols [1]
	 *  - number of global rows [1]
	 *  - number of global cols [1]
	 *  - number of local nnz [1]
	 *  - mapping [nbLocalCols]
	 *  - Row pointer [nbLocalRows+1]
	 *  - Col pointer [nbLocalNnz]
	 *  - datas [nbLocalNnz]
	 *
	 *  \tparam MatrixType The type of the matrix
	 *
	 *  \param[in] _globalMatrix The sequential dense matrix
	 *
	 *  \return The distributed local matrix
	 */
	template<typename MatrixType>
		typename PromoteToDist<MatrixType, typename MatrixType::data_type>::data_type* 
		DistributeMatrix(const MatrixType& _globalMatrix)
	{
		typedef typename MatrixType::data_type data_type;
		typedef typename PromoteToDist<MatrixType, typename MatrixType::data_type>::data_type distMatrixType;
		if(ParUtils::iAmMaster())
    {
      // Compute the distribution
      const typename MatrixType::impl_type::graph_type graph = _globalMatrix.getMatrixGraph();
      const int nbGlobalRows = graph.getNbRows();
      const int nbGlobalCols = graph.getNbCols();
      const int* rowPtr = graph.getRowPtr();
      const int* colPtr = graph.getColPtr();
      const int numPartitions = ParUtils::getNumProc();

      if(!m_partition)
        YALLA_ERROR_CHECK(this->computePartition(nbGlobalRows,nbGlobalCols));

			if(!m_global_ghosts_infos && !m_global_shared_infos)
				YALLA_ERROR_CHECK(this->computeGlobalSPMVInfos(_globalMatrix));
			else
				YALLA_ERR("Something is wrong with the partitionning (ghosts infos exists but not shared infos or vice versa");

			for(int i=numPartitions-1;i>=0;--i)
			{
				int numRowsForEachProcess = m_partition[i+1] - m_partition[i];
				int numElementsForEachProcess = 0;
				for(int j=m_partition[i];j<m_partition[i+1];++j)
					numElementsForEachProcess += _globalMatrix.getRowNnz(j);

				std::map<int, int> uniqueColsSet;
				for(int j=rowPtr[m_partition[i]]; j<rowPtr[m_partition[i+1]];++j)
					uniqueColsSet[colPtr[j]] = 0;

				const int nbLocalCols = uniqueColsSet.size();

				const int matrixSize = 5;
				const int mappingSize = nbLocalCols;
				const int rowPtrSize = numRowsForEachProcess+1;
				const int colPtrSize = numElementsForEachProcess;
				const int datasSize = numElementsForEachProcess;
				int sizeMessage = matrixSize+mappingSize+rowPtrSize+colPtrSize+datasSize;

				data_type* dataToSendToProcI = new data_type[sizeMessage];

				dataToSendToProcI[0] = numRowsForEachProcess;
				dataToSendToProcI[1] = nbLocalCols;
				dataToSendToProcI[2] = nbGlobalRows;
				dataToSendToProcI[3] = nbGlobalCols;
				dataToSendToProcI[4] = numElementsForEachProcess;

				int counter = 5;

				// Mapping
				typedef std::map<int,int>::iterator uniqueColsSetIterator;
				typedef std::map<int,int>::const_iterator uniqueColsSetConstIterator;
				int mappingCounter = 0;
				// Row and col shared mapping
				for(uniqueColsSetIterator it = uniqueColsSet.find(m_partition[i]); it!=uniqueColsSet.find(m_partition[i+1]);++it)
				{
					it->second = mappingCounter;
					++mappingCounter;
				}

				// Col mapping for cols that are not in the local row set
				for(uniqueColsSetIterator it = uniqueColsSet.begin(); it != uniqueColsSet.find(m_partition[i]); ++it)
				{
					it->second = mappingCounter;
					++mappingCounter;
				}

				// Col mapping for cols that are not in the local row set
				for(uniqueColsSetIterator it = uniqueColsSet.find(m_partition[i+1]); it != uniqueColsSet.end(); ++it)
				{
					it->second = mappingCounter;
					++mappingCounter;
				}

				// Row mapping
				for(uniqueColsSetConstIterator it = uniqueColsSet.find(m_partition[i]); it!=uniqueColsSet.find(m_partition[i+1]);++it)
				{
					dataToSendToProcI[counter] = it->first;
					++counter;
				}

				// Col mapping
				for(uniqueColsSetConstIterator it = uniqueColsSet.begin(); it != uniqueColsSet.find(m_partition[i]); ++it)
				{
					dataToSendToProcI[counter] = it->first;
					++counter;
				}

				// Col mapping
				for(uniqueColsSetConstIterator it = uniqueColsSet.find(m_partition[i+1]); it != uniqueColsSet.end(); ++it)
				{
					dataToSendToProcI[counter] = it->first;
					++counter;
				}

				// RowPtr
				dataToSendToProcI[counter] = 0;
				++counter;
				for(int j=m_partition[i];j<m_partition[i+1];++j)
				{
					dataToSendToProcI[counter] = dataToSendToProcI[counter-1] + _globalMatrix.getRowNnz(j);
					++counter;
				}

				// ColPtr
				for(int j=m_partition[i];j<m_partition[i+1];++j)
				{
					for(int k=rowPtr[j];k<rowPtr[j+1];++k)
					{
						dataToSendToProcI[counter] = uniqueColsSet[colPtr[k]];
						++counter;
					}
				}

				// Datas
				for(int j=m_partition[i];j<m_partition[i+1];++j)
				{
					for(int k=rowPtr[j];k<rowPtr[j+1];++k)
					{
						dataToSendToProcI[counter] = _globalMatrix[k];
						++counter;
					}
				}

				if(i!=0)
				{
					YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));
					delete[] dataToSendToProcI;
				}
				else
				{
					distMatrixType* localMatrix = this->computeLocalMatrix<distMatrixType>(dataToSendToProcI);
					delete[] dataToSendToProcI;
					return localMatrix;
				}
			}
    }
    else
    {
      MPI_Status msg_status;
      int count = 0;
      ParUtils::ProbeFromMaster(&msg_status);
      ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &count);
      data_type* datas = new data_type[count];

      YALLA_ERROR_CHECK(ParUtils::RecvFromMaster(datas,count,PromoteToMPI<data_type>::data_type()));
      distMatrixType* localMatrix = this->computeLocalMatrix<distMatrixType>(datas);
      delete[] datas;
      return localMatrix;
    }
    return 0;
  }

	/*! 
	 *  \brief Distribute a vector with a block row partition
	 *
	 *  \details
	 *  The master proc is in charge of distributing the vector following a block row scheme. \n
	 *  The partition is supposed to have been computed when calling the DistributeMatrix method. \n
	 *  The master proc packs and send the datas to all other procs. \n
	 *  Then all procs build their local vectors. \n
	 *  If the local SPMV information doesn't exist yet, they are send with the datas (send/receiveVectorAndSPMVInfos or send/receiveVector)
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _globalVector The sequential vector
	 *
	 *  \return The distributed local vector
	 */
  template<typename VectorType>
		typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type*
    DistributeVector(const VectorType& _globalVector)
  {
    if(ParUtils::iAmMaster())
    {
			if(m_synchronizer==0)
				return this->sendVectorAndSPMVInfos(_globalVector);
			else
				return this->sendVector(_globalVector);
    }
    else
    {
			if(m_synchronizer==0)
				return this->receiveVectorAndSPMVInfos<VectorType>();
			else
				return this->receiveVector<VectorType>();
    }
    return 0;
  }
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class RowDistribution. \n
	 */
	//@{
 private:
	/*! 
	 *  \brief Compute the block row partition
	 *
	 *  \details
	 *  Just take the number of global rows in the matrix and divide it by the number of procs. \n
	 *  The last proc take all remaining rows
	 *
	 *  \param[in] _rows The number of global rows
	 *  \param[in] _cols The number of global col
	 *
	 *  \return The error code
	 */
	int computePartition(const int _rows, const int _cols)
	{
		const int numPartitions = ParUtils::getNumProc();
		m_partition = new int[numPartitions+1];
		const int nbRowsByProc = _rows/numPartitions;
			
		for(int i=0;i<numPartitions;++i)
			m_partition[i] = i*nbRowsByProc;
			
		m_partition[numPartitions] = _rows;
		return YALLA_SUCCESS;
	}

	/*! 
	 *  \brief Packs and sends vector
	 *
	 *  \details
	 *  First we start with counting the number of rows and the rows ids that need to be sent to each process, by looking into the partition. \n
	 *  Then vector datas are packed in the following pattern [numberOfElements]:
	 *  - Number of local rows [1]
	 *  - Number of local cols [1]
	 *  - Number of own elements [1]
	 *  - Number of global rows [1]
	 *  - Number of global cols [1]
	 *  - Mapping LIDs/GIDs [nbLocalRows]
	 *  - Datas [nbLocalRows]
	 *
	 *  Once all info have been packed, the buffer is sent to the relevant proc.
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _globalVector The sequential vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type* 
    sendVector(const VectorType& _globalVector)
	{
    typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
    typedef typename distVectorType::data_type data_type;

		// Compute the distribution
		const int size = _globalVector.getSize();
		const int numPartitions = ParUtils::getNumProc();

		for(int i=numPartitions-1;i>=0;--i)
		{
			const int numRowsForEachProcess = m_partition[i+1] - m_partition[i] + m_global_ghosts_infos[i]->getNbElements();
			const int numOwnsElements = m_partition[i+1] - m_partition[i];

			int sizeMessage = 5 + numRowsForEachProcess + numRowsForEachProcess;
				
			data_type* dataToSendToProcI = new data_type[sizeMessage];
			dataToSendToProcI[0] = numRowsForEachProcess;
			dataToSendToProcI[1] = 1;
			dataToSendToProcI[2] = numOwnsElements;
			dataToSendToProcI[3] = size;
			dataToSendToProcI[4] = 1;
			int counter = 5;

			for(int j=m_partition[i];j<m_partition[i+1];++j)
			{
				dataToSendToProcI[counter] = j;
				dataToSendToProcI[numRowsForEachProcess+counter] = _globalVector(j);
				++counter;
			}

			for(int j=0;j<m_global_ghosts_infos[i]->getNbElements();++j)
			{
				const int GID = m_global_ghosts_infos[i]->getGIDs()[j];
				dataToSendToProcI[counter] = GID;
				dataToSendToProcI[numRowsForEachProcess+counter] = _globalVector(GID);
				++counter;
			}

			if(i!=0)
			{
				YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));
				delete[] dataToSendToProcI;
			}
			else
			{
				distVectorType* localRHS = this->computeLocalVector<distVectorType>(dataToSendToProcI);
				delete[] dataToSendToProcI;
				return localRHS;
			}
		}
		return 0;
	}

	/*! 
	 *  \brief Packs and sends vector and SPMV information
	 *
	 *  \details
	 *  First we start with counting the number of rows and the rows ids that need to be sent to each process, by looking into the partition. \n
	 *  Then vector datas are packed in the following pattern [numberOfElements]:
	 *  - Number of local rows [1]
	 *  - Number of local cols [1]
	 *  - Number of own elements [1]
	 *  - Number of global rows [1]
	 *  - Number of global cols [1]
	 *  - Mapping LIDs/GIDs [nbLocalRows]
	 *  - Datas [nbLocalRows]
	 *
	 *  SPMV information are sent after the distributed vector information. First, ghosts information, then shared information. \n
	 *  So the first index containing the ghosts information is: 5 + 2*nbLocalRows \n
	 *  Ghosts datas are then packed in the following pattern [numberOfElements]:
	 *  - Total number of ghosts elements [1]
	 *  - Total number of sources [1]
	 *  - Sender list [nbSources]
	 *  - Number of elements for each source [nbSources]
	 *  - Ghosts GIDs [nbGhostsElements]
	 *
	 *  Once the ghosts datas have been packed, it is time for the shared datas. \n
	 *  Shared datas are then packed in the following pattern [numberOfElements]:
	 *  - Total number of elements to send [1]
	 *  - Total number of receiver [1]
	 *  - Receiver list [nbReceiver]
	 *  - Number of element for each receiver [nbReceiver]
	 *  - Shared LIDs [nbSharedElements]
	 *
	 *  Once all info have been packed, the buffer is sent to the relevant proc.
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _globalVector The sequential vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type* 
		sendVectorAndSPMVInfos(const VectorType& _globalVector)
	{
    typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
    typedef typename distVectorType::data_type data_type;

		// Compute the distribution
		const int size = _globalVector.getSize();
		const int numPartitions = ParUtils::getNumProc();

		for(int i=numPartitions-1;i>=0;--i)
		{
			const int numRowsForEachProcess = m_partition[i+1] - m_partition[i] + m_global_ghosts_infos[i]->getNbElements();
			const int numOwnsElements = m_partition[i+1] - m_partition[i];
			const int nbGhostsElements = m_global_ghosts_infos[i]->getNbElements();
			const int nbGhostsSender = m_global_ghosts_infos[i]->getNbSender();
			const int nbSharedElements = m_global_shared_infos[i]->getNbElements();
			const int nbSharedReceiver = m_global_shared_infos[i]->getNbReceiver();
			const int sizeMessage = 5 + numRowsForEachProcess + numRowsForEachProcess + 2 + nbGhostsSender + nbGhostsSender + 1 + nbGhostsElements + 2 + nbSharedReceiver + nbSharedReceiver + 1 + nbSharedElements;
				
			data_type* dataToSendToProcI = new data_type[sizeMessage];
			dataToSendToProcI[0] = numRowsForEachProcess;
			dataToSendToProcI[1] = 1;
			dataToSendToProcI[2] = numOwnsElements;
			dataToSendToProcI[3] = size;
			dataToSendToProcI[4] = 1;
			int counter = 5;

			for(int j=m_partition[i];j<m_partition[i+1];++j)
			{
				dataToSendToProcI[counter] = j;
				dataToSendToProcI[numRowsForEachProcess+counter] = _globalVector(j);
				++counter;
			}

			for(int j=0;j<m_global_ghosts_infos[i]->getNbElements();++j)
			{
				const int GID = m_global_ghosts_infos[i]->getGIDs()[j];
				dataToSendToProcI[counter] = GID;
				dataToSendToProcI[numRowsForEachProcess+counter] = _globalVector(GID);
				++counter;
			}

			counter+= numRowsForEachProcess;

			dataToSendToProcI[counter] = nbGhostsElements;
			++counter;
			dataToSendToProcI[counter] = nbGhostsSender;
			++counter;

			for(int j=0;j<nbGhostsSender;++j)
			{
				dataToSendToProcI[counter] = m_global_ghosts_infos[i]->getSenderList()[j];
				++counter;
			}

			for(int j=0;j<nbGhostsSender+1;++j)
			{
				dataToSendToProcI[counter] = m_global_ghosts_infos[i]->getNbElementsForProcList()[j];
				++counter;
			}

			for(int j=0;j<nbGhostsElements;++j)
			{
				dataToSendToProcI[counter] = m_global_ghosts_infos[i]->getGIDs()[j];
				++counter;
			}

			dataToSendToProcI[counter] = nbSharedElements;
			++counter;
			dataToSendToProcI[counter] = nbSharedReceiver;
			++counter;

			for(int j=0;j<nbSharedReceiver;++j)
			{
				dataToSendToProcI[counter] = m_global_shared_infos[i]->getReceiverList()[j];
				++counter;
			}

			for(int j=0;j<nbSharedReceiver+1;++j)
			{
				dataToSendToProcI[counter] = m_global_shared_infos[i]->getNbElementsByProcList()[j];
				++counter;
			}

			for(int j=0;j<nbSharedElements;++j)
			{
				dataToSendToProcI[counter] = m_global_shared_infos[i]->getLID()[j];
				++counter;
			}

			if(i!=0)
			{
				YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));
				delete[] dataToSendToProcI;
			}
			else
			{
				YALLA_ERROR_CHECK(this->computeLocalSPMVInfos(dataToSendToProcI));
				distVectorType* localRHS = this->computeLocalVector<distVectorType>(dataToSendToProcI);
				delete[] dataToSendToProcI;
				return localRHS;
			}
		}
		return 0;
	}

	/*! 
	 *  \brief Receive a vector
	 *
	 *  \details
	 *  Receive a vector and call the computeLocalVector method to build the local vector
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type*
		receiveVector()
	{
		typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
		typedef typename distVectorType::data_type data_type;

		MPI_Status msg_status;
		int count = 0;
		ParUtils::ProbeFromMaster(&msg_status);
		ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &count);
		data_type* datas = new data_type[count];

		YALLA_ERROR_CHECK(ParUtils::RecvFromMaster(datas,count,PromoteToMPI<data_type>::data_type()));
		distVectorType* localRHS = this->computeLocalVector<distVectorType>(datas);
		delete[] datas;
		return localRHS;
	}

	/*! 
	 *  \brief Receive a vector and SPMV information
	 *
	 *  \details
	 *  Receive a vector and SPMV information and call the computeLocalSPMVInfos method to build the local vector and SPMV information
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type*
		receiveVectorAndSPMVInfos()
	{
		typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
		typedef typename distVectorType::data_type data_type;

		MPI_Status msg_status;
		int count = 0;
		ParUtils::ProbeFromMaster(&msg_status);
		ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &count);
		data_type* datas = new data_type[count];

		YALLA_ERROR_CHECK(ParUtils::RecvFromMaster(datas,count,PromoteToMPI<data_type>::data_type()));
		YALLA_ERROR_CHECK(this->computeLocalSPMVInfos(datas));
		distVectorType* localRHS = this->computeLocalVector<distVectorType>(datas);
		delete[] datas;
		return localRHS;
	}

	/*! 
	 *  \brief Compute a local distributed CSR matrix
	 *
	 *  \details
	 *  Datas are unpacked from the buffer and the local distributed CSR matrix is then built.
	 *
	 *  Datas are unpacked in the following pattern [numberOfElements]:
	 *  - number of local rows [1]
	 *  - number of local cols [1]
	 *  - number of global rows [1]
	 *  - number of global cols [1]
	 *  - number of local nnz [1]
	 *  - mapping [nbLocalCols]
	 *  - Row pointer [nbLocalRows+1]
	 *  - Col pointer [nbLocalNnz]
	 *  - datas [nbLocalNnz]
	 *
	 *  \tparam MatrixType The type of the matrix
	 *
	 *  \param[in] _datas The buffer containing the packed datas
	 *
	 *  \return The distributed local matrix
	 */
  template<typename MatrixType> 
		MatrixType* 
		computeLocalMatrix(const typename MatrixType::data_type* _datas) const
  {
    const int locRows = static_cast<int>(_datas[0]);
    const int locCols = static_cast<int>(_datas[1]);
    const int globRows = static_cast<int>(_datas[2]);
    const int globCols = static_cast<int>(_datas[3]);
    const int locNnz = static_cast<int>(_datas[4]);
    int* globIndices = new int[locCols];
    for(int i=0;i<locCols;++i)
      globIndices[i] = static_cast<int>(_datas[5+i]);
    int* locRowPtr = new int[locRows+1];
    for(int i=0;i<locRows+1;++i)
      locRowPtr[i] = static_cast<int>(_datas[5+locCols+i]);

    int* locColPtr = new int[locNnz];
    for(int i=0;i<locNnz;++i)
      locColPtr[i] = static_cast<int>(_datas[5+locCols+locRows+1+i]);
    MatrixType* localMatrix = new MatrixType(locRows,locCols,locNnz,globRows,globCols,globIndices,locRowPtr,locColPtr,&_datas[5+locCols+locRows+1+locNnz]);
    delete[] globIndices;
    delete[] locRowPtr;
    delete[] locColPtr;
    return localMatrix;
  }

	/*! 
	 *  \brief Compute a local distributed vector
	 *
	 *  \details
	 *  Datas are unpacked from the buffer and the local distributed vector is then built.
	 *
	 *  Datas are unpacked in the following pattern [numberOfElements]:
	 *  - Number of local rows [1]
	 *  - Number of local cols [1]
	 *  - Number of own elements [1]
	 *  - Number of global rows [1]
	 *  - Number of global cols [1]
	 *  - Mapping LIDs/GIDs [nbLocalRows]
	 *  - Datas [nbLocalRows]
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _datas The buffer containing the packed datas
	 *
	 *  \return The distributed local vector
	 */
  template<typename VectorType>
		VectorType* 
		computeLocalVector(const typename VectorType::data_type* _datas) const
  {
    const int locRows = static_cast<int>(_datas[0]);
    const int locCols = static_cast<int>(_datas[1]);
		const int nbOwns = static_cast<int>(_datas[2]);
    const int globRows = static_cast<int>(_datas[3]);
    const int globCols = static_cast<int>(_datas[4]);
    int* globIndices = new int[locRows];
    for(int i=0;i<locRows;++i)
      globIndices[i] = static_cast<int>(_datas[5+i]);
    VectorType* localVec = new VectorType(locRows,locCols,nbOwns,globRows,globCols,globIndices,&_datas[5+locRows], m_synchronizer);
    delete[] globIndices;
    return localVec;
  }

	/*! 
	 *  \brief Compute all SPMV information for all procs
	 *
	 *  \details
	 *  Starts by analyzing the matrix and partition scheme. \n
	 *  Every row in the partition set that include a column that is not on the partition scheme will be added to the ghost list. To eliminate duplicates elements, we use a set here. The proc that will own the element is also stored in the mean time. \n
	 *  Once we got all ghosts elements, we can build the Ghosts and Shared (same information used the other way around)
	 *  Finally, the shared and ghosts infomation are stored for each proc in the m_global_shared_infos and m_global_ghosts_infos pointers.
	 *
	 *  \tparam MatrixType The type of the matrix
	 *
	 *  \param[in] _globalMatrix The matrix to distribute
	 *
	 *  \return The error code
	 */
	template<typename MatrixType>
		int
		computeGlobalSPMVInfos(const MatrixType& _globalMatrix)
	{
		const int numProc = ParUtils::getNumProc();
		m_global_ghosts_infos = new SPMVGhosts*[numProc];
		m_global_shared_infos	= new SPMVShared*[numProc];
		std::set<int>* ghostElements = new std::set<int>[numProc];
		std::set<int>* nbSender = new std::set<int>[numProc];

		std::vector<int>* sharedElementsByProc = new std::vector<int>[numProc];
		std::vector<int>* nbReceiver = new std::vector<int>[numProc];

		const typename MatrixType::impl_type::graph_type graph = _globalMatrix.getMatrixGraph();
		const int nbRows = graph.getNbRows();
		const int* rowPtr = graph.getRowPtr();
		const int* colPtr = graph.getColPtr();
		const int averageRowByProc = nbRows/numProc;

		for(int i=0;i<numProc;++i)
		{
			for(int j=rowPtr[m_partition[i]];j<rowPtr[m_partition[i+1]];++j)
			{
				if(colPtr[j]<m_partition[i] || colPtr[j]>m_partition[i+1]-1)
				{
					int colJIsOnProc = (colPtr[j]-(colPtr[j]%averageRowByProc))/averageRowByProc;
					if(colJIsOnProc==numProc)
						--colJIsOnProc;
					// Ghosts infos
					ghostElements[i].insert(colPtr[j]);
					nbSender[i].insert(colJIsOnProc);
				}
			}
		}

		for(int i=0;i<numProc;++i)
		{
			const int nbGhostsElements = ghostElements[i].size();
			const int nbSend = nbSender[i].size();
			int* senderList = new int[nbSend];
			int* nbElementsBySender = new int[nbSend+1];
			for(int j=0;j<nbSend+1;++j)
				nbElementsBySender[j] = 0;

			std::map<int,int> ghostProcToIndexMap;
			int counter = 0;
			for(std::set<int>::iterator it = nbSender[i].begin();it != nbSender[i].end();++it)
			{
				senderList[counter] = *it;
				ghostProcToIndexMap[*it] = counter;
				++counter;
			}

			int* globalIds = new int[nbGhostsElements];
			for(int j=0;j<nbGhostsElements;++j)
				globalIds[j] = 0;

			counter = 0;
			for(std::set<int>::iterator it = ghostElements[i].begin(); it != ghostElements[i].end(); ++it)
			{
				globalIds[counter] = *it;
				int currentColIsOnProc = (*it-(*it%averageRowByProc))/averageRowByProc;
				if(currentColIsOnProc==numProc)
					--currentColIsOnProc;

				++nbElementsBySender[ghostProcToIndexMap[currentColIsOnProc]+1];
				++counter;
				// Shared infos
				sharedElementsByProc[currentColIsOnProc].push_back(*it-averageRowByProc*currentColIsOnProc);
				nbReceiver[currentColIsOnProc].push_back(i);
			}

			for(int j=1;j<nbSend+1;++j)
				nbElementsBySender[j] += nbElementsBySender[j-1];

			m_global_ghosts_infos[i] = new SPMVGhosts(nbGhostsElements,nbSend,senderList,nbElementsBySender,globalIds);
			delete[] senderList;
			delete[] nbElementsBySender;
			delete[] globalIds;
		}
		delete[] ghostElements;
		delete[] nbSender;

		for(int i=0;i<m_num_proc;++i)
		{
			const int nbSharedElements = sharedElementsByProc[i].size();
			std::set<int> uniqueProcNumber(nbReceiver[i].begin(), nbReceiver[i].end());
			const int nbReceive = uniqueProcNumber.size();
		 
			int* receiverList = new int[nbReceive];
			int* nbElementsByReceiver = new int[nbReceive+1];
			for(int j=0;j<nbReceive+1;++j)
				nbElementsByReceiver[j] = 0;
		 
			std::map<int,int> sharedProcToIndexMap;
			int counter = 0;
			for(std::set<int>::iterator it = uniqueProcNumber.begin();it != uniqueProcNumber.end();++it)
			{
				receiverList[counter] = *it;
				sharedProcToIndexMap[*it] = counter;
				++counter;
			}

			int* localIds = new int[nbSharedElements];
			for(int j=0;j<nbSharedElements;++j)
				localIds[j] = 0;

			counter = 0;
			for(int j=0;j<nbSharedElements;++j)
			{
				localIds[j] = sharedElementsByProc[i][j];
				++nbElementsByReceiver[sharedProcToIndexMap[nbReceiver[i][j]]+1];
			}

			for(int j=1;j<nbReceive+1;++j)
				nbElementsByReceiver[j] += nbElementsByReceiver[j-1];

			m_global_shared_infos[i] = new SPMVShared(nbSharedElements,nbReceive,receiverList,nbElementsByReceiver,localIds);
			delete[] receiverList;
			delete[] nbElementsByReceiver;
			delete[] localIds;
		}
		delete[] sharedElementsByProc;
		delete[] nbReceiver;

		return YALLA_SUCCESS;
	}

	int computeLocalSPMVInfos(data_type* _datas)
	{
		const int nbVectorDatas = static_cast<int>(_datas[0]);
		const int ghostsInfosOffset = 5 + 2*nbVectorDatas;
		const int nbGhostsElements = static_cast<int>(_datas[ghostsInfosOffset]);
		const int nbGhostsSender = static_cast<int>(_datas[ghostsInfosOffset+1]);
		int* senderList = new int[nbGhostsSender];
		for(int i=0;i<nbGhostsSender;++i)
			senderList[i] = static_cast<int>(_datas[ghostsInfosOffset+2+i]);
		int* nbReceivedElementByProc = new int[nbGhostsSender+1];
		for(int i=0;i<nbGhostsSender+1;++i)
			nbReceivedElementByProc[i] = static_cast<int>(_datas[ghostsInfosOffset+2+nbGhostsSender+i]);
		int* ghostsElements = new int[nbGhostsElements];
		for(int i=0;i<nbGhostsElements;++i)
			ghostsElements[i] = static_cast<int>(_datas[ghostsInfosOffset+2+nbGhostsSender+nbGhostsSender+1+i]);

		const int sharedInfosOffset = ghostsInfosOffset + 2 + nbGhostsSender + nbGhostsSender + 1 + nbGhostsElements;
		const int nbSharedElements = static_cast<int>(_datas[sharedInfosOffset]);
		const int nbSharedReceivers = static_cast<int>(_datas[sharedInfosOffset+1]);
			
		int* receiverList = new int[nbSharedReceivers];
		for(int i=0;i<nbSharedReceivers;++i)
			receiverList[i] = static_cast<int>(_datas[sharedInfosOffset+2+i]);
		int* nbSendElementByProc = new int[nbSharedReceivers+1];
		for(int i=0;i<nbSharedReceivers+1;++i)
			nbSendElementByProc[i] = static_cast<int>(_datas[sharedInfosOffset+2+nbSharedReceivers+i]);
		int* sendElements = new int[nbSharedElements];
		for(int i=0;i<nbSharedElements;++i)
			sendElements[i] = static_cast<int>(_datas[sharedInfosOffset+2+nbSharedReceivers+nbSharedReceivers+1+i]);

		m_synchronizer = new SPMVSynchronization<data_type>(new SPMVGhosts(nbGhostsElements, nbGhostsSender, senderList, nbReceivedElementByProc, ghostsElements), new SPMVShared(nbSharedElements, nbSharedReceivers, receiverList, nbSendElementByProc, sendElements));

		delete[] senderList;
		delete[] nbReceivedElementByProc;
		delete[] ghostsElements;
		delete[] receiverList;
		delete[] nbSendElementByProc;
		delete[] sendElements;

		return YALLA_SUCCESS;
	}
};
YALLA_END_NAMESPACE
#endif
