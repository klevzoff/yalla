#include "ColumnDistribution.h"

/*!
 *  \file ColumnDistribution.cpp
 *  \brief Block column distribution scheme implementation
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

ColumnDistribution::ColumnDistribution()
{
  m_partition = 0;
}

ColumnDistribution::~ColumnDistribution()
{
  delete[] m_partition;
}

int
ColumnDistribution::
ComputePartition(const int _rows, const int _cols)
{
  const int numPartitions = ParUtils::getNumProc();
  const int nbColsByProc = _cols/numPartitions;

  m_partition = new int[numPartitions+1];
  for(int i=0;i<numPartitions;++i)
    m_partition[i] = i*nbColsByProc;

  m_partition[numPartitions] = _cols;
  return YALLA_SUCCESS;
}
