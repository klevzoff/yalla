#pragma once
#ifndef yalla_metis_distribution_h
#define yalla_metis_distribution_h

/*!
 *  \file MetisDistribution.h
 *  \brief Metis distribution scheme
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#ifdef WITH_METIS

#include <iostream>
#include <stdlib.h>
#include <set>
#include <map>
#include <vector>
#include <string>
#include "metis.h"
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToDist.h"
#include "yalla/Utils/TypeTraits/PromoteToSeq.h"
#include "yalla/Utils/ParUtils/ParUtils.h"
#include "yalla/Utils/ParUtils/SPMVGhosts.h"
#include "yalla/Utils/ParUtils/SPMVShared.h"
#include "yalla/Utils/ParUtils/SPMVSynchronization.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class MetisDistribution
 *  \brief Metis distribution scheme
 * 
 *  \details 
 *  Distribute matrices and vector following the partitionning scheme computed with Metis. \n
 *  The partitionning scheme is computed with the first matrix given. \n
 *  All other matrices are distributed following the same partitionning scheme. \n
 *  Vectors are distributed according to this same partitionning scheme.
 *
 *  \tparam DataType The data type of the objects to distribute
 *
 *  \todo There is cleary some cleaning to do in this class. Some code is duplicated and badly organized. A refactoring would be a good idea.
 */
template<typename DataType>
class MetisDistribution
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the objects to distribute
	typedef DataType data_type;
	//! \brief Type of this class
	typedef MetisDistribution<data_type> this_type;
	//@}
 private:
	//! \brief The number of procs
	int m_num_proc;
	//! \brief Partition
	int* m_partition;
	//! \brief Ghosts information stored on the master proc
	SPMVGhosts** m_global_ghosts_infos;
	//! \brief Shared information stored on the master proc
	SPMVShared** m_global_shared_infos;
	//! \brief Local SPMV synchronizer
	SPMVSynchronization<data_type>* m_synchronizer;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class MetisDistribution
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Set all attributes to zero. \n
	 *  The number of procs is stored there.
	 */
	explicit MetisDistribution()
	{
		m_partition = 0;
		m_global_ghosts_infos = 0;
		m_global_shared_infos = 0;
		m_synchronizer = 0;
		m_num_proc = ParUtils::getNumProc();
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class MetisDistribution
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class, i.e. the m_partition pointer, all the ghosts and shared infos and the local synchronizer.
	 */
	virtual ~MetisDistribution()
	{
		delete[] m_partition;
		m_partition = 0;
		if(m_global_ghosts_infos)
		{
			for(int i=0;i<m_num_proc;++i)
			{
				delete m_global_ghosts_infos[i];
				m_global_ghosts_infos[i] = 0;
			}
			delete[] m_global_ghosts_infos;
			m_global_ghosts_infos = 0;
		}
		if(m_global_shared_infos)
		{
			for(int i=0;i<m_num_proc;++i)
			{
				delete m_global_shared_infos[i];
				m_global_shared_infos[i] = 0;
			}
			delete[] m_global_shared_infos;
			m_global_shared_infos = 0;
		}
		delete m_synchronizer;
		m_synchronizer = 0;
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class MetisDistribution. \n
	 */
	//@{

	/*! 
	 *  \brief Distribute a matrix with a Metis partition
	 *
	 *  \details
	 *  The master proc is in charge of distributing the matrix following the partition computed by Metis. \n
	 *  It starts by computing the partition, then all SPMV required informations. \n
	 *  Then the master proc packs and send the datas to all other procs. \n
	 *  Then all procs build their local matrix. \n
	 *  Rows and columns share the same LIDs/GIDs mapping. LIDs for the row starts at 0 and ends at nbLocalRows. Then all cols that are not in this mapping are sequentially numbered starting at nbLoalRows+1 to nbLocalCols.
	 *
	 *  Datas are packed in the following pattern [numberOfElements]:
	 *  - number of local rows [1]
	 *  - number of local cols [1]
	 *  - number of global rows [1]
	 *  - number of global cols [1]
	 *  - number of local nnz [1]
	 *  - mapping [nbLocalCols]
	 *  - Row pointer [nbLocalRows+1]
	 *  - Col pointer [nbLocalNnz]
	 *  - datas [nbLocalNnz]
	 *
	 *  \tparam MatrixType The type of the matrix
	 *
	 *  \param[in] _globalMatrix The sequential dense matrix
	 *
	 *  \return The distributed local matrix
	 */
	template<typename MatrixType>
    typename PromoteToDist<MatrixType, typename MatrixType::data_type>::data_type* DistributeMatrix(const MatrixType& _globalMatrix)
	{
		typedef typename MatrixType::data_type data_type;
		typedef typename PromoteToDist<MatrixType, typename MatrixType::data_type>::data_type distMatrixType;
		if(ParUtils::iAmMaster())
    {
			// Compute the distribution
      const typename MatrixType::impl_type::graph_type graph = _globalMatrix.getMatrixGraph();
      const int nbGlobalRows = graph.getNbRows();
      const int nbGlobalCols = graph.getNbCols();
			const int nnz = _globalMatrix.getNnz();
      const int* rowPtr = graph.getRowPtr();
      const int* colPtr = graph.getColPtr();

			if(!m_partition)
        YALLA_ERROR_CHECK(this->computePartition(nbGlobalRows, nbGlobalCols, nnz, rowPtr, colPtr));

			int* localIds = new int[nbGlobalRows];
			int* numRowByProc = new int[m_num_proc];
			int* numElementsByProc = new int[m_num_proc];
			std::map<int, int>* uniqueColsSet = new std::map<int, int>[m_num_proc];

			for(int i=0;i<m_num_proc;++i)
			{
				numRowByProc[i] = 0;
				numElementsByProc[i] = 0;
			}

			for(int i=0;i<nbGlobalRows;++i)
			{
				localIds[i] = numRowByProc[m_partition[i]];
				++numRowByProc[m_partition[i]];
				numElementsByProc[m_partition[i]] += _globalMatrix.getRowNnz(i);
				for(int j=rowPtr[i];j<rowPtr[i+1];++j)
					uniqueColsSet[m_partition[i]][colPtr[j]] = 0;
			}

			if(!m_global_ghosts_infos && !m_global_shared_infos)
				YALLA_ERROR_CHECK(this->computeGlobalSPMVInfos(_globalMatrix, localIds));
			else
				YALLA_ERR("Something is wrong with the partitionning (ghosts infos exists but not shared infos or vice versa");

			for(int i=m_num_proc-1;i>=0;--i)
			{
				const int numRowsForEachProcess = numRowByProc[i];
				const int numElementsForEachProcess = numElementsByProc[i];

				// Computing once and for all the set of local rows
				int* localRowsGIDs = new int[numRowsForEachProcess];
				int localRowGIDsCounter = 0;
				for(int j=0;j<nbGlobalRows;++j)
				{
					if(m_partition[j]==i)
					{
						localRowsGIDs[localRowGIDsCounter] = j;
						++localRowGIDsCounter;
					}
				}

				const int nbLocalCols = uniqueColsSet[i].size();

				const int matrixSize = 5;
				const int mappingSize = nbLocalCols;
				const int rowPtrSize = numRowsForEachProcess+1;
				const int colPtrSize = numElementsForEachProcess;
				const int datasSize = numElementsForEachProcess;
				int sizeMessage = matrixSize+mappingSize+rowPtrSize+colPtrSize+datasSize;

				data_type* dataToSendToProcI = new data_type[sizeMessage];

				dataToSendToProcI[0] = numRowsForEachProcess;
				dataToSendToProcI[1] = nbLocalCols;
				dataToSendToProcI[2] = nbGlobalRows;
				dataToSendToProcI[3] = nbGlobalCols;
				dataToSendToProcI[4] = numElementsForEachProcess;

				int counter = 5;

				typedef std::map<int,int>::iterator uniqueColsSetIterator;
				typedef std::map<int,int>::const_iterator uniqueColsSetConstIterator;
				int rowMappingCounter = 0;
				int colMappingCounter = numRowsForEachProcess;
				// Row and col shared mapping
				for(uniqueColsSetIterator it = uniqueColsSet[i].begin(); it!=uniqueColsSet[i].end();++it)
				{
					if(m_partition[it->first]==i)
					{
						it->second = rowMappingCounter;
						++rowMappingCounter;
					}
					else
					{
						it->second = colMappingCounter;
						++colMappingCounter;
					}
				}

				// Col mapping for cols that are not in the local row set
				for(uniqueColsSetConstIterator it = uniqueColsSet[i].begin(); it!=uniqueColsSet[i].end();++it)
				{
					if(m_partition[it->first]==i)
					{
						dataToSendToProcI[counter] = it->first;
						++counter;
					}
				}

				// Col mapping for cols that are not in the local row set
				for(uniqueColsSetConstIterator it = uniqueColsSet[i].begin(); it!=uniqueColsSet[i].end();++it)
				{
					if(m_partition[it->first]!=i)
					{
						dataToSendToProcI[counter] = it->first;
						++counter;
					}
				}

				// RowPtr
				dataToSendToProcI[counter] = 0;
				++counter;

				for(int j=0;j<numRowsForEachProcess;++j)
				{
					dataToSendToProcI[counter] = dataToSendToProcI[counter-1] + _globalMatrix.getRowNnz(localRowsGIDs[j]);
					++counter;
				}

				// ColPtr
				for(int j=0;j<numRowsForEachProcess;++j)
				{
					for(int k=rowPtr[localRowsGIDs[j]];k<rowPtr[localRowsGIDs[j]+1];++k)
					{
						dataToSendToProcI[counter] = uniqueColsSet[i][colPtr[k]];
						++counter;
					}
				}
				
				// Datas
				for(int j=0;j<numRowsForEachProcess;++j)
				{
					for(int k=rowPtr[localRowsGIDs[j]];k<rowPtr[localRowsGIDs[j]+1];++k)
					{
						dataToSendToProcI[counter] = _globalMatrix[k];
						++counter;
					}
				}

				if(i!=0)
				{
					YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));
					delete[] dataToSendToProcI;
				}
				else
				{
					distMatrixType* localMatrix = this->computeLocalMatrix<distMatrixType>(dataToSendToProcI);
					delete[] dataToSendToProcI;
					return localMatrix;
				}
			}
			delete[] localIds;
			delete[] numRowByProc;
			delete[] numElementsByProc;
			delete[] uniqueColsSet;
		}
		else
    {
      MPI_Status msg_status;
      int count = 0;
      ParUtils::ProbeFromMaster(&msg_status);
      ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &count);
      data_type* datas = new data_type[count];
			
      YALLA_ERROR_CHECK(ParUtils::RecvFromMaster(datas,count,PromoteToMPI<data_type>::data_type()));
      distMatrixType* localMatrix = this->computeLocalMatrix<distMatrixType>(datas);
      delete[] datas;
      return localMatrix;
    }
		return 0;
	}

	/*! 
	 *  \brief Distribute a vector with a Metis partition
	 *
	 *  \details
	 *  The master proc is in charge of distributing the vector following a Metis partition. \n
	 *  The partition is supposed to have been computed when calling the DistributeMatrix method. \n
	 *  The master proc packs and send the datas to all other procs. \n
	 *  Then all procs build their local vectors. \n
	 *  If the local SPMV information doesn't exist yet, they are send with the datas (send/receiveVectorAndSPMVInfos or send/receiveVector)
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _globalVector The sequential vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type* DistributeVector(const VectorType& _globalVector)
	{
		if(ParUtils::iAmMaster())
    {
			if(m_synchronizer==0)
				return this->sendVectorAndSPMVInfos(_globalVector);
			else
				return this->sendVector(_globalVector);
    }
    else
    {
			if(m_synchronizer==0)
				return this->receiveVectorAndSPMVInfos<VectorType>();
			else
				return this->receiveVector<VectorType>();
    }
    return 0;
	}
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class MetisDistribution. \n
	 */
	//@{
 private:
	/*! 
	 *  \brief Compute the Metis partition
	 *
	 *  \details
	 *  Extract the graph from the matrix and format it following Metis requirements. \n
	 *  Then call the Metis library to get the partition scheme.
	 *
	 *  \param[in] _rows The number of global rows
	 *  \param[in] _cols The number of global col
	 *  \param[in] _nnz The number of nnz elements
	 *  \param[in] _rowPtr The row pointer in standard CSR 3 array variation
	 *  \param[in] _colPtr The col pointer in standard CSR 3 array variation
	 *
	 *  \return The error code
	 */
	int computePartition(int _rows, const int _cols, const int _nnz, const int* _rowPtr, const int* _colPtr)
	{
		idx_t nvtxs = _rows; // Number of vertices
		idx_t ncon = 1; // Number of balancing constraints
		idx_t* xadj = new int[nvtxs+1]; // Adjacency structure of the graph
		idx_t* adjncy = new int[_nnz-nvtxs]; // Adjacency structure of the graph
		idx_t* vwgt = 0; // Weight of vertices
		idx_t* vsize = 0; // Size of the vertices for computing total communication volume
		idx_t* adjwgt = 0; // Weights of edges
		idx_t nparts = m_num_proc; // Number of parts to partition the graph
		real_t* tpwgts = 0; // Desired weight for each partition and constraint
		real_t* ubvec = 0; // Allowed load imbalance tolerance for each constraint
		idx_t options[METIS_NOPTIONS]; // Metis options
		idx_t objval = 0; // Edge cut or total communication volume of the partitionning solution
		m_partition = new idx_t[nvtxs]; // Partition vector of the graph

		for(int i=0;i<_rows+1;++i)
			xadj[i] = _rowPtr[i]-i;

		int counter = 0;
		for(int i=0;i<_rows;++i)
		{
			for(int j=_rowPtr[i];j<_rowPtr[i+1];++j)
			{
				if(i!=_colPtr[j])
				{
					adjncy[counter] = _colPtr[j];
					++counter;
				}
			}
		}
  
		METIS_SetDefaultOptions(options);
		
		int error = 0;
		if(m_num_proc==1)
		{
			for(int i=0;i<nvtxs;++i)
				m_partition[i] = 0;
		}
		else if(m_num_proc<8) // Recommended in Metis manual
			error = METIS_PartGraphRecursive(&nvtxs, &ncon, xadj, adjncy, vwgt, vsize, adjwgt, &nparts, tpwgts, ubvec, options, &objval, m_partition);
		else
			error = METIS_PartGraphKway(&nvtxs, &ncon, xadj, adjncy, vwgt, vsize, adjwgt, &nparts, tpwgts, ubvec, options, &objval, m_partition);

		delete[] xadj;
		delete[] adjncy;

		if(error==1)
			return YALLA_SUCCESS;
		else
			return error;
	}

	/*! 
	 *  \brief Compute all SPMV information for all procs
	 *
	 *  \details
	 *  Starts by analyzing the matrix and partition scheme. \n
	 *  Every row in the partition set that include a column that is not on the partition scheme will be added to the ghost list. To eliminate duplicates elements, we use a set here. The proc that will own the element is also stored in the mean time. \n
	 *  Once we got all ghosts elements, we can build the Ghosts and Shared (same information used the other way around)
	 *  Finally, the shared and ghosts infomation are stored for each proc in the m_global_shared_infos and m_global_ghosts_infos pointers.
	 *
	 *  \tparam MatrixType The type of the matrix
	 *
	 *  \param[in] _globalMatrix The matrix to distribute
	 *  \param[in] _lids The LIDs of the matrix
	 *
	 *  \return The error code
	 */
	template<typename MatrixType>
		int
		computeGlobalSPMVInfos(const MatrixType& _globalMatrix, const int* _lids)
	{
		const int numProc = ParUtils::getNumProc();
		m_global_ghosts_infos = new SPMVGhosts*[numProc];
		m_global_shared_infos	= new SPMVShared*[numProc];
		std::set<int>* ghostElements = new std::set<int>[numProc];
		std::set<int>* nbSender = new std::set<int>[numProc];

		std::vector<int>* sharedElementsByProc = new std::vector<int>[numProc];
		std::vector<int>* nbReceiver = new std::vector<int>[numProc];

		const typename MatrixType::impl_type::graph_type graph = _globalMatrix.getMatrixGraph();
		const int nbRows = graph.getNbRows();
		const int* rowPtr = graph.getRowPtr();
		const int* colPtr = graph.getColPtr();

		for(int i=0;i<nbRows;++i)
		{
			for(int j=rowPtr[i];j<rowPtr[i+1];++j)
			{
				if(m_partition[colPtr[j]]!=m_partition[i])
				{
					ghostElements[m_partition[i]].insert(colPtr[j]);
					nbSender[m_partition[i]].insert(m_partition[colPtr[j]]);
				}
			}
		}

		for(int i=0;i<numProc;++i)
		{
			const int nbGhostsElements = ghostElements[i].size();
			const int nbSend = nbSender[i].size();
			int* senderList = new int[nbSend];
			int* nbElementsBySender = new int[nbSend+1];
			for(int j=0;j<nbSend+1;++j)
				nbElementsBySender[j] = 0;
			
			std::map<int,int> ghostProcToIndexMap;
			int counter = 0;
			for(std::set<int>::iterator it = nbSender[i].begin();it != nbSender[i].end();++it)
			{
				senderList[counter] = *it;
				ghostProcToIndexMap[*it] = counter;
				++counter;
			}

			int* globalIds = new int[nbGhostsElements];
			for(int j=0;j<nbGhostsElements;++j)
				globalIds[j] = 0;

			for(std::set<int>::iterator it = ghostElements[i].begin(); it != ghostElements[i].end(); ++it)
			{
				const int currentColIsOnProc = m_partition[*it];
				++nbElementsBySender[ghostProcToIndexMap[currentColIsOnProc]+1];
			}

			for(int j=1;j<nbSend+1;++j)
				nbElementsBySender[j] += nbElementsBySender[j-1];

			int nbElementsByProcCounter[nbSend];
			for(int j=0;j<nbSend;++j)
				nbElementsByProcCounter[j] = 0;

			for(std::set<int>::iterator it = ghostElements[i].begin(); it != ghostElements[i].end(); ++it)
			{
				const int currentColIsOnProc = m_partition[*it];
				const int procOffset = nbElementsBySender[ghostProcToIndexMap[currentColIsOnProc]];
				globalIds[procOffset+nbElementsByProcCounter[ghostProcToIndexMap[currentColIsOnProc]]] = *it;

				++nbElementsByProcCounter[ghostProcToIndexMap[currentColIsOnProc]];
				// Shared infos
				sharedElementsByProc[currentColIsOnProc].push_back(_lids[*it]);
				nbReceiver[currentColIsOnProc].push_back(i);
			}

			m_global_ghosts_infos[i] = new SPMVGhosts(nbGhostsElements,nbSend,senderList,nbElementsBySender,globalIds);

			delete[] senderList;
			delete[] nbElementsBySender;
			delete[] globalIds;
		}
		delete[] ghostElements;
		delete[] nbSender;

		for(int i=0;i<m_num_proc;++i)
		{
			const int nbSharedElements = sharedElementsByProc[i].size();
			std::set<int> uniqueProcNumber(nbReceiver[i].begin(), nbReceiver[i].end());
			const int nbReceive = uniqueProcNumber.size();
		 
			int* receiverList = new int[nbReceive];
			int* nbElementsByReceiver = new int[nbReceive+1];
			for(int j=0;j<nbReceive+1;++j)
				nbElementsByReceiver[j] = 0;
			
			std::map<int,int> sharedProcToIndexMap;
			int counter = 0;
			for(std::set<int>::iterator it = uniqueProcNumber.begin();it != uniqueProcNumber.end();++it)
			{
				receiverList[counter] = *it;
				sharedProcToIndexMap[*it] = counter;
				++counter;
			}

			int* localIds = new int[nbSharedElements];
			for(int j=0;j<nbSharedElements;++j)
				localIds[j] = 0;
			
			counter = 0;
			for(int j=0;j<nbSharedElements;++j)
			{
				localIds[j] = sharedElementsByProc[i][j];
				++nbElementsByReceiver[sharedProcToIndexMap[nbReceiver[i][j]]+1];
			}
			for(int j=1;j<nbReceive+1;++j)
				nbElementsByReceiver[j] += nbElementsByReceiver[j-1];

			m_global_shared_infos[i] = new SPMVShared(nbSharedElements,nbReceive,receiverList,nbElementsByReceiver,localIds);

			delete[] receiverList;
			delete[] nbElementsByReceiver;
			delete[] localIds;
		}
		delete[] sharedElementsByProc;
		delete[] nbReceiver;

		return YALLA_SUCCESS;
	}

	/*! 
	 *  \brief Compute local SPMV information from the buffer
	 *
	 *  \details
	 *  SPMV information are sent after the distributed vector information. First, ghosts information, then shared information. \n
	 *  So the first index containing the ghosts information is: 5 + 2*nbVectorDatas \n
	 *  Ghosts datas are then unpacked in the following pattern [numberOfElements]:
	 *  - Total number of ghosts elements [1]
	 *  - Total number of sources [1]
	 *  - Sender list [nbSources]
	 *  - Number of elements for each source [nbSources]
	 *  - Ghosts GIDs [nbGhostsElements]
	 *
	 *  Once the ghosts datas have been unpacked, it is time for the shared datas. \n
	 *  Shared datas are then unpacked in the following pattern [numberOfElements]:
	 *  - Total number of elements to send [1]
	 *  - Total number of receiver [1]
	 *  - Receiver list [nbReceiver]
	 *  - Number of element for each receiver [nbReceiver]
	 *  - Shared LIDs [nbSharedElements]
	 *
	 *  Once all info have been extracted, the synchronizer is build and stored in the m_synchronizer attribute.
	 *
	 *  \param[in] _datas Buffer containing the SPMV information
	 *
	 *  \return The error code
	 */
	int computeLocalSPMVInfos(data_type* _datas)
	{
		const int nbVectorDatas = static_cast<int>(_datas[0]);
		const int ghostsInfosOffset = 5 + 2*nbVectorDatas;
		const int nbGhostsElements = static_cast<int>(_datas[ghostsInfosOffset]);
		const int nbGhostsSender = static_cast<int>(_datas[ghostsInfosOffset+1]);
		int* senderList = new int[nbGhostsSender];
		for(int i=0;i<nbGhostsSender;++i)
			senderList[i] = static_cast<int>(_datas[ghostsInfosOffset+2+i]);
		int* nbReceivedElementByProc = new int[nbGhostsSender+1];
		for(int i=0;i<nbGhostsSender+1;++i)
			nbReceivedElementByProc[i] = static_cast<int>(_datas[ghostsInfosOffset+2+nbGhostsSender+i]);
		int* ghostsElements = new int[nbGhostsElements];
		for(int i=0;i<nbGhostsElements;++i)
			ghostsElements[i] = static_cast<int>(_datas[ghostsInfosOffset+2+nbGhostsSender+nbGhostsSender+1+i]);

		const int sharedInfosOffset = ghostsInfosOffset + 2 + nbGhostsSender + nbGhostsSender + 1 + nbGhostsElements;
		const int nbSharedElements = static_cast<int>(_datas[sharedInfosOffset]);
		const int nbSharedReceivers = static_cast<int>(_datas[sharedInfosOffset+1]);
			
		int* receiverList = new int[nbSharedReceivers];
		for(int i=0;i<nbSharedReceivers;++i)
			receiverList[i] = static_cast<int>(_datas[sharedInfosOffset+2+i]);
		int* nbSendElementByProc = new int[nbSharedReceivers+1];
		for(int i=0;i<nbSharedReceivers+1;++i)
			nbSendElementByProc[i] = static_cast<int>(_datas[sharedInfosOffset+2+nbSharedReceivers+i]);
		int* sendElements = new int[nbSharedElements];
		for(int i=0;i<nbSharedElements;++i)
			sendElements[i] = static_cast<int>(_datas[sharedInfosOffset+2+nbSharedReceivers+nbSharedReceivers+1+i]);

		m_synchronizer = new SPMVSynchronization<data_type>(new SPMVGhosts(nbGhostsElements, nbGhostsSender, senderList, nbReceivedElementByProc, ghostsElements), new SPMVShared(nbSharedElements, nbSharedReceivers, receiverList, nbSendElementByProc, sendElements));

		delete[] senderList;
		delete[] nbReceivedElementByProc;
		delete[] ghostsElements;
		delete[] receiverList;
		delete[] nbSendElementByProc;
		delete[] sendElements;

		return YALLA_SUCCESS;
	}

	/*! 
	 *  \brief Packs and sends vector and SPMV information
	 *
	 *  \details
	 *  First we start with counting the number of rows and the rows ids that need to be sent to each process, by looking into the partition. \n
	 *  Then vector datas are packed in the following pattern [numberOfElements]:
	 *  - Number of local rows [1]
	 *  - Number of local cols [1]
	 *  - Number of own elements [1]
	 *  - Number of global rows [1]
	 *  - Number of global cols [1]
	 *  - Mapping LIDs/GIDs [nbLocalRows]
	 *  - Datas [nbLocalRows]
	 *
	 *  SPMV information are sent after the distributed vector information. First, ghosts information, then shared information. \n
	 *  So the first index containing the ghosts information is: 5 + 2*nbLocalRows \n
	 *  Ghosts datas are then packed in the following pattern [numberOfElements]:
	 *  - Total number of ghosts elements [1]
	 *  - Total number of sources [1]
	 *  - Sender list [nbSources]
	 *  - Number of elements for each source [nbSources]
	 *  - Ghosts GIDs [nbGhostsElements]
	 *
	 *  Once the ghosts datas have been packed, it is time for the shared datas. \n
	 *  Shared datas are then packed in the following pattern [numberOfElements]:
	 *  - Total number of elements to send [1]
	 *  - Total number of receiver [1]
	 *  - Receiver list [nbReceiver]
	 *  - Number of element for each receiver [nbReceiver]
	 *  - Shared LIDs [nbSharedElements]
	 *
	 *  Once all info have been packed, the buffer is sent to the relevant proc.
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _globalVector The sequential vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type* 
		sendVectorAndSPMVInfos(const VectorType& _globalVector)
	{
		typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
    typedef typename distVectorType::data_type data_type;

		// Compute the distribution
		const int size = _globalVector.getSize();

		int* numRowByProc = new int[m_num_proc];

		for(int i=0;i<m_num_proc;++i)
			numRowByProc[i] = 0;

		for(int i=0;i<size;++i)
			++numRowByProc[m_partition[i]];

		for(int i=m_num_proc-1;i>=0;--i)
		{
			int* localRowsGIDs = new int[numRowByProc[i]];
			int localRowGIDsCounter = 0;
			for(int j=0;j<size;++j)
			{
				if(m_partition[j]==i)
				{
					localRowsGIDs[localRowGIDsCounter] = j;
					++localRowGIDsCounter;
				}
			}

			const int numOwnsElements = localRowGIDsCounter;
			const int numRowsForEachProcess = numOwnsElements + m_global_ghosts_infos[i]->getNbElements();
			const int nbGhostsElements = m_global_ghosts_infos[i]->getNbElements();
			const int nbGhostsSender = m_global_ghosts_infos[i]->getNbSender();
			const int nbSharedElements = m_global_shared_infos[i]->getNbElements();
			const int nbSharedReceiver = m_global_shared_infos[i]->getNbReceiver();
			const int sizeMessage = 5 + numRowsForEachProcess + numRowsForEachProcess + 2 + nbGhostsSender + nbGhostsSender + 1 + nbGhostsElements + 2 + nbSharedReceiver + nbSharedReceiver + 1 + nbSharedElements;

			data_type* dataToSendToProcI = new data_type[sizeMessage];
			dataToSendToProcI[0] = numRowsForEachProcess;
			dataToSendToProcI[1] = 1;
			dataToSendToProcI[2] = numOwnsElements;
			dataToSendToProcI[3] = size;
			dataToSendToProcI[4] = 1;
			int counter = 5;

			for(int j=0;j<numOwnsElements;++j)
			{
				dataToSendToProcI[counter] = j;
				dataToSendToProcI[numRowsForEachProcess+counter] = _globalVector(localRowsGIDs[j]);
				++counter;
			}

			for(int j=0;j<m_global_ghosts_infos[i]->getNbElements();++j)
			{
				const int GID = m_global_ghosts_infos[i]->getGIDs()[j];
				dataToSendToProcI[counter] = GID;
				dataToSendToProcI[numRowsForEachProcess+counter] = _globalVector(GID);
				++counter;
			}

			counter+= numRowsForEachProcess;

			dataToSendToProcI[counter] = nbGhostsElements;
			++counter;
			dataToSendToProcI[counter] = nbGhostsSender;
			++counter;

			for(int j=0;j<nbGhostsSender;++j)
			{
				dataToSendToProcI[counter] = m_global_ghosts_infos[i]->getSenderList()[j];
				++counter;
			}

			for(int j=0;j<nbGhostsSender+1;++j)
			{
				dataToSendToProcI[counter] = m_global_ghosts_infos[i]->getNbElementsForProcList()[j];
				++counter;
			}

			for(int j=0;j<nbGhostsElements;++j)
			{
				dataToSendToProcI[counter] = m_global_ghosts_infos[i]->getGIDs()[j];
				++counter;
			}

			dataToSendToProcI[counter] = nbSharedElements;
			++counter;
			dataToSendToProcI[counter] = nbSharedReceiver;
			++counter;

			for(int j=0;j<nbSharedReceiver;++j)
			{
				dataToSendToProcI[counter] = m_global_shared_infos[i]->getReceiverList()[j];
				++counter;
			}

			for(int j=0;j<nbSharedReceiver+1;++j)
			{
				dataToSendToProcI[counter] = m_global_shared_infos[i]->getNbElementsByProcList()[j];
				++counter;
			}

			for(int j=0;j<nbSharedElements;++j)
			{
				dataToSendToProcI[counter] = m_global_shared_infos[i]->getLID()[j];
				++counter;
			}

			if(i!=0)
			{
				YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));
				delete[] dataToSendToProcI;
				delete[] localRowsGIDs;
			}
			else
			{
				YALLA_ERROR_CHECK(this->computeLocalSPMVInfos(dataToSendToProcI));
				distVectorType* localRHS = this->computeLocalVector<distVectorType>(dataToSendToProcI);
				delete[] dataToSendToProcI;
				delete[] numRowByProc;
				delete[] localRowsGIDs;
				return localRHS;
			}
		}
		return 0;
	}

	/*! 
	 *  \brief Packs and sends vector
	 *
	 *  \details
	 *  First we start with counting the number of rows and the rows ids that need to be sent to each process, by looking into the partition. \n
	 *  Then vector datas are packed in the following pattern [numberOfElements]:
	 *  - Number of local rows [1]
	 *  - Number of local cols [1]
	 *  - Number of own elements [1]
	 *  - Number of global rows [1]
	 *  - Number of global cols [1]
	 *  - Mapping LIDs/GIDs [nbLocalRows]
	 *  - Datas [nbLocalRows]
	 *
	 *  Once all info have been packed, the buffer is sent to the relevant proc.
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _globalVector The sequential vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type* 
    sendVector(const VectorType& _globalVector)
	{
		typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
    typedef typename distVectorType::data_type data_type;

		// Compute the distribution
		const int size = _globalVector.getSize();

		int* numRowByProc = new int[m_num_proc];

		for(int i=0;i<m_num_proc;++i)
			numRowByProc[i] = 0;

		for(int i=0;i<size;++i)
			++numRowByProc[m_partition[i]];

		for(int i=m_num_proc-1;i>=0;--i)
		{
			int* localRowsGIDs = new int[numRowByProc[i]];
			int localRowGIDsCounter = 0;
			for(int j=0;j<size;++j)
			{
				if(m_partition[j]==i)
				{
					localRowsGIDs[localRowGIDsCounter] = j;
					++localRowGIDsCounter;
				}
			}

			const int numOwnsElements = localRowGIDsCounter;
			const int numRowsForEachProcess = numOwnsElements + m_global_ghosts_infos[i]->getNbElements();
			const int nbGhostsElements = m_global_ghosts_infos[i]->getNbElements();
			const int nbGhostsSender = m_global_ghosts_infos[i]->getNbSender();
			const int nbSharedElements = m_global_shared_infos[i]->getNbElements();
			const int nbSharedReceiver = m_global_shared_infos[i]->getNbReceiver();
			const int sizeMessage = 5 + numRowsForEachProcess + numRowsForEachProcess + 2 + nbGhostsSender + nbGhostsSender + 1 + nbGhostsElements + 2 + nbSharedReceiver + nbSharedReceiver + 1 + nbSharedElements;

			data_type* dataToSendToProcI = new data_type[sizeMessage];
			dataToSendToProcI[0] = numRowsForEachProcess;
			dataToSendToProcI[1] = 1;
			dataToSendToProcI[2] = numOwnsElements;
			dataToSendToProcI[3] = size;
			dataToSendToProcI[4] = 1;
			int counter = 5;

			for(int j=0;j<numOwnsElements;++j)
			{
				dataToSendToProcI[counter] = j;
				dataToSendToProcI[numRowsForEachProcess+counter] = _globalVector(localRowsGIDs[j]);
				++counter;
			}

			if(i!=0)
			{
				YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));
				delete[] dataToSendToProcI;
				delete[] localRowsGIDs;
			}
			else
			{
				distVectorType* localRHS = this->computeLocalVector<distVectorType>(dataToSendToProcI);
				delete[] dataToSendToProcI;
				delete[] numRowByProc;
				delete[] localRowsGIDs;
				return localRHS;
			}
		}
		return 0;
	}

	/*! 
	 *  \brief Receive a vector
	 *
	 *  \details
	 *  Receive a vector and call the computeLocalVector method to build the local vector
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type*
		receiveVector()
	{
		typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
		typedef typename distVectorType::data_type data_type;

		MPI_Status msg_status;
		int count = 0;
		ParUtils::ProbeFromMaster(&msg_status);
		ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &count);
		data_type* datas = new data_type[count];

		YALLA_ERROR_CHECK(ParUtils::RecvFromMaster(datas,count,PromoteToMPI<data_type>::data_type()));
		distVectorType* localRHS = this->computeLocalVector<distVectorType>(datas);
		delete[] datas;
		return localRHS;
	}

	/*! 
	 *  \brief Receive a vector and SPMV information
	 *
	 *  \details
	 *  Receive a vector and SPMV information and call the computeLocalSPMVInfos method to build the local vector and SPMV information
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type*
		receiveVectorAndSPMVInfos()
	{
		typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
		typedef typename distVectorType::data_type data_type;

		MPI_Status msg_status;
		int count = 0;
		ParUtils::ProbeFromMaster(&msg_status);
		ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &count);
		data_type* datas = new data_type[count];

		YALLA_ERROR_CHECK(ParUtils::RecvFromMaster(datas,count,PromoteToMPI<data_type>::data_type()));
		YALLA_ERROR_CHECK(this->computeLocalSPMVInfos(datas));
		distVectorType* localRHS = this->computeLocalVector<distVectorType>(datas);
		delete[] datas;
		return localRHS;
	}

	/*! 
	 *  \brief Compute a local distributed CSR matrix
	 *
	 *  \details
	 *  Datas are unpacked from the buffer and the local distributed CSR matrix is then built.
	 *
	 *  Datas are unpacked in the following pattern [numberOfElements]:
	 *  - number of local rows [1]
	 *  - number of local cols [1]
	 *  - number of global rows [1]
	 *  - number of global cols [1]
	 *  - number of local nnz [1]
	 *  - mapping [nbLocalCols]
	 *  - Row pointer [nbLocalRows+1]
	 *  - Col pointer [nbLocalNnz]
	 *  - datas [nbLocalNnz]
	 *
	 *  \tparam MatrixType The type of the matrix
	 *
	 *  \param[in] _datas The buffer containing the packed datas
	 *
	 *  \return The distributed local matrix
	 */
	template<typename MatrixType>
		MatrixType* computeLocalMatrix(const typename MatrixType::data_type* _datas) const
	{
		const int locRows = static_cast<int>(_datas[0]);
    const int locCols = static_cast<int>(_datas[1]);
    const int globRows = static_cast<int>(_datas[2]);
    const int globCols = static_cast<int>(_datas[3]);
    const int locNnz = static_cast<int>(_datas[4]);
    int* globIndices = new int[locCols];
    for(int i=0;i<locCols;++i)
      globIndices[i] = static_cast<int>(_datas[5+i]);
    int* locRowPtr = new int[locRows+1];
    for(int i=0;i<locRows+1;++i)
      locRowPtr[i] = static_cast<int>(_datas[5+locCols+i]);

    int* locColPtr = new int[locNnz];
    for(int i=0;i<locNnz;++i)
      locColPtr[i] = static_cast<int>(_datas[5+locCols+locRows+1+i]);
    MatrixType* localMatrix = new MatrixType(locRows,locCols,locNnz,globRows,globCols,globIndices,locRowPtr,locColPtr,&_datas[5+locCols+locRows+1+locNnz]);
    delete[] globIndices;
    delete[] locRowPtr;
    delete[] locColPtr;
    return localMatrix;
	}

	/*! 
	 *  \brief Compute a local distributed vector
	 *
	 *  \details
	 *  Datas are unpacked from the buffer and the local distributed vector is then built.
	 *
	 *  Datas are unpacked in the following pattern [numberOfElements]:
	 *  - Number of local rows [1]
	 *  - Number of local cols [1]
	 *  - Number of own elements [1]
	 *  - Number of global rows [1]
	 *  - Number of global cols [1]
	 *  - Mapping LIDs/GIDs [nbLocalRows]
	 *  - Datas [nbLocalRows]
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _datas The buffer containing the packed datas
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
		VectorType* computeLocalVector(const typename VectorType::data_type* _datas) const
	{
		const int locRows = static_cast<int>(_datas[0]);
    const int locCols = static_cast<int>(_datas[1]);
		const int nbOwns = static_cast<int>(_datas[2]);
    const int globRows = static_cast<int>(_datas[3]);
    const int globCols = static_cast<int>(_datas[4]);
    int* globIndices = new int[locRows];
    for(int i=0;i<locRows;++i)
      globIndices[i] = static_cast<int>(_datas[5+i]);
    VectorType* localVec = new VectorType(locRows,locCols,nbOwns,globRows,globCols,globIndices,&_datas[5+locRows], m_synchronizer);
    delete[] globIndices;
    return localVec;
	}
};
YALLA_END_NAMESPACE
#endif
#endif
