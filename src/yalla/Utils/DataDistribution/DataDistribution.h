#pragma once
#ifndef yalla_data_distribution_h
#define yalla_data_distribution_h

/*!
 *  \file DataDistribution.h
 *  \brief Scheme to distribute matrices and vectors
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToDist.h"
#include "yalla/Utils/TypeTraits/PromoteToSeq.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class DataDistribution
 *  \brief Scheme to distribute matrices and vectors
 * 
 *  \details 
 *  This class provides an interface to distribute matrices and vectors following a policy based system.\n
 *  The template parameter allow to specify the policy used to distribute the datas
 *
 *  \tparam DistributionStrategyType The distribution strategy
 */
template<typename DistributionStrategyType>
class DataDistribution
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief The distribution strategy
	typedef DistributionStrategyType dist_strat_type;
	//! \brief Type of this class
	typedef DataDistribution<dist_strat_type> this_type;
	//@}
 private:
	//! \brief Pointer on the distribution strategy
	dist_strat_type* m_distribution_strategy;
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DataDistribution
	 */
	//@{

	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Create pointer to the distribution strategy implementation.
	 */
 public:
	explicit DataDistribution()
	{
    m_distribution_strategy = new DistributionStrategyType();
  }
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DataDistribution
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class, i.e. the distribution strategy. \n
	 */
	virtual ~DataDistribution()
	{
    delete m_distribution_strategy;
  }
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class DataDistribution. \n
	 *  These methods give access in read-only mode to the datas/information of the class DataDistribution
	 */
	//@{
	/*! 
	 *  \brief Return a handler on the distribution policy
	 *
	 *  \details
	 *
	 *  \return The distribution policy
	 */
	dist_strat_type* getPartitioner()
	{
		return m_distribution_strategy;
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class DataDistribution. \n
	 *  These methods give access to various distribution utilities.
	 */
	//@{

	/*! 
	 *  \brief Distribute a matrix following the specified policy
	 *
	 *  \details
	 *  Call the method DistributeMatrix of the chosen policy. \n
	 *
	 *  \param[in] _globalMatrix The sequential matrix
	 *
	 *  \return The distributed matrix
	 */
	template<typename MatrixType>
    typename PromoteToDist<MatrixType, typename MatrixType::data_type>::data_type* DistributeMatrix(const MatrixType& _globalMatrix)
	{
		return m_distribution_strategy->DistributeMatrix(_globalMatrix);
	}

	/*! 
	 *  \brief Distribute a vector following the specified policy
	 *
	 *  \details
	 *  Call the method DistributeVector of the chosen policy. \n
	 *
	 *  \param[in] _globalVector The sequential vector
	 *
	 *  \return The distributed vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type* DistributeVector(const VectorType& _globalVector)
	{
		return m_distribution_strategy->DistributeVector(_globalVector);
	}
	//@}
};
YALLA_END_NAMESPACE

#endif
