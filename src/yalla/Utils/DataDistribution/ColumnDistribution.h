#pragma once
#ifndef yalla_column_distribution_h
#define yalla_column_distribution_h

/*!
 *  \file ColumnDistribution.h
 *  \brief Block column distribution scheme
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToDist.h"
#include "yalla/Utils/TypeTraits/PromoteToSeq.h"
#include "yalla/Utils/ParUtils/ParUtils.h"

YALLA_USING_NAMESPACE(yalla)

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class ColumnDistribution
 *  \brief Distribution of matrices and vectors following a block column partition
 * 
 *  \details 
 *  Distribute matrices and vector following a block column partition. \n
 *  The partitionning scheme is computed with the first matrix given. \n
 *  All other matrices are distributed following the same partitionning scheme. \n
 *  Vectors are distributed according to this same partitionning scheme.
 */
class ColumnDistribution
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef ColumnDistribution this_type;
	//@}
 public:
	//! \brief Partition
	int* m_partition;

 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class ColumnDistribution
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Set the partition to zero
	 */
	explicit ColumnDistribution();
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class ColumnDistribution
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class, i.e. the m_partition pointer.
	 */
	virtual ~ColumnDistribution();
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class ColumnDistribution. \n
	 */
	//@{

	/*! 
	 *  \brief Distribute a matrix in a block column fashion
	 *
	 *  \details
	 *  The master proc is in charge of distributing the matrix following a block column scheme. \n
	 *  If the partition has not been computed yet, the partition is computed. Then the master proc packs and send the datas to all other procs. \n
	 *  Then all procs build their local matrix.
	 *
	 *  Datas are packed in the following pattern [numberOfElements]:
	 *  - number of local rows [1]
	 *  - number of local cols [1]
	 *  - number of global rows [1]
	 *  - number of global cols [1]
	 *  - mapping [nbLocalCols]
	 *  - datas [nbLocalRows*nbLocalCols]
	 *
	 *  \tparam MatrixType The type of the matrix
	 *
	 *  \param[in] _globalMatrix The sequential dense matrix
	 *
	 *  \return The distributed local matrix
	 */
	template<typename MatrixType>
    typename PromoteToDist<MatrixType, typename MatrixType::data_type>::data_type* DistributeMatrix(const MatrixType& _globalMatrix)
	{
		typedef typename MatrixType::data_type data_type;
    typedef typename PromoteToDist<MatrixType, typename MatrixType::data_type>::data_type distMatrixType;
    if(ParUtils::iAmMaster())
    {
      // Compute the distribution
      const int nbRows = _globalMatrix.getNbRows();
      const int nbCols = _globalMatrix.getNbCols();
      const int numPartitions = ParUtils::getNumProc();

      if(!m_partition)
				YALLA_ERROR_CHECK(this->ComputePartition(nbRows,nbCols));

      for(int i=numPartitions-1;i>=0;--i)
      {
        const int nbLocalCol = m_partition[i+1]-m_partition[i];

        int sizeMessage = 4+nbLocalCol+nbLocalCol*nbRows;

        data_type* dataToSendToProcI = new data_type[sizeMessage];
        dataToSendToProcI[0] = nbRows;
        dataToSendToProcI[1] = nbLocalCol;
        dataToSendToProcI[2] = nbRows;
        dataToSendToProcI[3] = nbCols;

        int counter = 4;

        for(int j=m_partition[i];j<m_partition[i+1];++j)
        {
          dataToSendToProcI[counter] = j;
          ++counter;
        }

        for(int j=0;j<nbRows;++j)
        {
          for(int k=m_partition[i];k<m_partition[i+1];++k)
          {
            dataToSendToProcI[counter] = _globalMatrix(j,k);
            ++counter;
          }
        }
        if(i!=0)
        {
          YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));
          delete[] dataToSendToProcI;
        }
        else
        {
          distMatrixType* localMatrix = this->computeLocalMatrix<distMatrixType>(dataToSendToProcI);
          delete[] dataToSendToProcI;
          return localMatrix;
        }
      }
    }
    else
    {
      if(!m_partition)
        YALLA_ERROR_CHECK(this->ComputePartition(0,0));

      MPI_Status msg_status;
      int count = 0;
      ParUtils::ProbeFromMaster(&msg_status);
      ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &count);
      data_type* datas = new data_type[count];
      YALLA_ERROR_CHECK(ParUtils::RecvFromMaster(datas,count,PromoteToMPI<data_type>::data_type()));
      distMatrixType* localMatrix = this->computeLocalMatrix<distMatrixType>(datas);
      delete[] datas;
      return localMatrix;
    }
    return 0;
	}

	/*! 
	 *  \brief Distribute a vector in a block column fashion
	 *
	 *  \details
	 *  The master proc is in charge of distributing the vector following a block column scheme. \n
	 *  The partition is supposed to have been computed when calling the DistributeMatrix method. \n
	 *  The master proc packs and send the datas to all other procs. \n
	 *  Then all procs build their local vectors.
	 *
	 *  Datas are packed in the following pattern [numberOfElements]:
	 *  - number of local rows [1]
	 *  - number of local cols [1]
	 *  - number of global rows [1]
	 *  - number of global cols [1]
	 *  - mapping [nbLocalCols]
	 *  - datas [nbLocalRows*nbLocalCols]
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _globalVector The sequential vector
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType>
    typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type* DistributeVector(const VectorType& _globalVector)
	{
		typedef typename PromoteToDist<VectorType,typename VectorType::data_type>::data_type distVectorType;
		typedef typename distVectorType::data_type data_type;
		if(ParUtils::iAmMaster())
		{
			// Compute the distribution
			const int nbRows = _globalVector.getNbRows();
			const int nbCols = _globalVector.getNbCols();
			const int numPartitions = ParUtils::getNumProc();

			for(int i=numPartitions-1;i>=0;--i)
			{
				const int nbLocalRow = m_partition[i+1]-m_partition[i];
				int sizeMessage = 4+nbLocalRow+nbLocalRow;

				data_type* dataToSendToProcI = new data_type[sizeMessage];
				dataToSendToProcI[0] = nbLocalRow;
				dataToSendToProcI[1] = nbCols;
				dataToSendToProcI[2] = nbRows;
				dataToSendToProcI[3] = nbCols;

				int counter = 4;

				for(int j=m_partition[i];j<m_partition[i+1];++j)
				{
					dataToSendToProcI[counter] = j;
					dataToSendToProcI[nbLocalRow+counter] = _globalVector(j);
					++counter;
				}
				if(i!=0)
				{
					YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));
					delete[] dataToSendToProcI;
				}
				else
				{
					distVectorType* localRHS = this->computeLocalVector<distVectorType>(dataToSendToProcI);
					delete[] dataToSendToProcI;
					return localRHS;
				}
			}
		}
		else
		{
			MPI_Status msg_status;
			int count = 0;
			ParUtils::ProbeFromMaster(&msg_status);
			ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &count);
			data_type* datas = new data_type[count];

			YALLA_ERROR_CHECK(ParUtils::RecvFromMaster(datas,count,PromoteToMPI<data_type>::data_type()));
			distVectorType* localRHS = this->computeLocalVector<distVectorType>(datas);
			delete[] datas;
			return localRHS;
		}
		return 0;
	}
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class ColumnDistribution. \n
	 */
	//@{

 private:
	/*! 
	 *  \brief Compute the block column distribution
	 *
	 *  \details
	 *  Compute the block column distribution. \n
	 *  Just take the number of global column in the matrix and divide it by the number of procs. \n
	 *  The last proc take all remaining cols
	 *
	 *  \param[in] _rows The number of global rows
	 *  \param[in] _cols The number of global col
	 *
	 *  \return The error code
	 */
	int ComputePartition(const int _rows, const int _cols);

	/*! 
	 *  \brief Compute a local distributed dense matrix
	 *
	 *  \details
	 *  Datas are unpacked from the buffer and the local distributed dense matrix is then built.
	 *
	 *  Datas are unpacked in the following pattern [numberOfElements]:
	 *  - number of local rows [1]
	 *  - number of local cols [1]
	 *  - number of global rows [1]
	 *  - number of global cols [1]
	 *  - mapping [nbLocalCols]
	 *  - datas [nbLocalRows*nbLocalCols]
	 *
	 *  \tparam MatrixType The type of the matrix
	 *
	 *  \param[in] _datas The buffer containing the packed datas
	 *
	 *  \return The distributed local matrix
	 */
	template<typename MatrixType> MatrixType* computeLocalMatrix(const typename MatrixType::data_type* _datas) const
	{
		const int locRows = static_cast<int>(_datas[0]);
		const int locCols = static_cast<int>(_datas[1]);
		const int globRows = static_cast<int>(_datas[2]);
		const int globCols = static_cast<int>(_datas[3]);
		int* globIndices = new int[locCols];
		for(int i=0;i<locCols;++i)
			globIndices[i] = static_cast<int>(_datas[4+i]);
		MatrixType* localMatrix = new MatrixType(locRows,locCols,globRows,globCols,globIndices,&_datas[4+locCols]);
		delete[] globIndices;
		return localMatrix;
	}

	/*! 
	 *  \brief Compute a local distributed vector
	 *
	 *  \details
	 *  Datas are unpacked from the buffer and the local distributed vector is then built.
	 *
	 *  Datas are unpacked in the following pattern [numberOfElements]:
	 *  - number of local rows [1]
	 *  - number of local cols [1]
	 *  - number of global rows [1]
	 *  - number of global cols [1]
	 *  - mapping [nbLocalCols]
	 *  - datas [nbLocalRows*nbLocalCols]
	 *
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _datas The buffer containing the packed datas
	 *
	 *  \return The distributed local vector
	 */
	template<typename VectorType> VectorType* computeLocalVector(const typename VectorType::data_type* _datas) const
	{
		const int locRows = static_cast<int>(_datas[0]);
		const int locCols = static_cast<int>(_datas[1]);
		const int globRows = static_cast<int>(_datas[2]);
		const int globCols = static_cast<int>(_datas[3]);
		int* globIndices = new int[locRows];
		for(int i=0;i<locRows;++i)
			globIndices[i] = static_cast<int>(_datas[4+i]);
		VectorType* localVec = new VectorType(locRows,locCols,locRows,globRows,globCols,globIndices,&_datas[4+locRows],0);
		delete[] globIndices;
		return localVec;
	}
};
YALLA_END_NAMESPACE
#endif
