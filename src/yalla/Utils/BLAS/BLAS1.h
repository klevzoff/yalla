#pragma once
#ifndef yalla_blas_1_h
#define yalla_blas_1_h

/*!
 *  \file BLAS1.h
 *  \brief BLAS level 1 operations
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

/*!
 *  \brief AXPY for SeqVector
 *
 *  \details
 *  Compute y += a . x
 *
 *  \tparam DataType Data type of the vector
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in,out] _y The second vector
 */
template<typename DataType>
static void axpy(const DataType& _a, const SeqVector<DataType>& _x, SeqVector<DataType>& _y)
{
#if defined (WITH_ATLAS) || defined (_WITH_MKL)
	YALLA_LOG("Not BLAS AXPY");
#endif
	const int size = _x.getSize();
	for(int i=0;i<size;++i)
		_y(i) += _a * _x(i);
}

#ifdef WITH_MPI
/*!
 *  \brief AXPY for DistVector
 *
 *  \details
 *  Compute y += a . x
 *
 *  \tparam DataType Data type of the vector
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in,out] _y The second vector
 */
template<typename DataType>
static void axpy(const DataType& _a, const DistVector<DataType>& _x, DistVector<DataType>& _y)
{
#if defined (WITH_ATLAS) || defined (_WITH_MKL)
	YALLA_LOG("Not BLAS AXPY");
#endif
	const int size = _x.getOwnSize();
	for(int i=0;i<size;++i)
		_y(i) += _a * _x(i);
}
#endif

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief AXPY for EpetraVector
 *
 *  \details
 *  Compute y += a . x
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in,out] _y The second vector
 */
static void axpy(const double& _a, const EpetraVector<Epetra_Vector>& _x, EpetraVector<Epetra_Vector>& _y)
{
	_y.getVector()->Update(_a, *_x.getVector(),1.);
}
#endif

#if defined (WITH_ATLAS) || defined (_WITH_MKL)
/*!
 *  \brief sAXPY for SeqVector with BLAS
 *
 *  \details
 *  Compute y += a . x
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in,out] _y The second vector
 */
static void axpy(const float& _a, const SeqVector<float>& _x, SeqVector<float>& _y)
{
	const int size = _x.getSize();
	cblas_saxpy(size,_a,_x.getPointeur(),1,&_y(0),1);
}

/*!
 *  \brief dAXPY for SeqVector with BLAS
 *
 *  \details
 *  Compute y += a . x
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in,out] _y The second vector
 */
static void axpy(const double& _a, const SeqVector<double>& _x, SeqVector<double>& _y)
{
	const int size = _x.getSize();
	cblas_daxpy(size,_a,_x.getPointeur(),1,&_y(0),1);
}

#ifdef WITH_MPI
/*!
 *  \brief sAXPY for DistVector with BLAS
 *
 *  \details
 *  Compute y += a . x
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in,out] _y The second vector
 */
static void axpy(const float& _a, const DistVector<float>& _x, DistVector<float>& _y)
{
	const int size = _x.getOwnSize();
	cblas_saxpy(size,_a,_x.getPointeur(),1,&_y(0),1);
}

/*!
 *  \brief dAXPY for DistVector with BLAS
 *
 *  \details
 *  Compute y += a . x
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in,out] _y The second vector
 */
static void axpy(const double& _a, const DistVector<double>& _x, DistVector<double>& _y)
{
	const int size = _x.getOwnSize();
	cblas_daxpy(size,_a,_x.getPointeur(),1,&_y(0),1);
}
#endif

#endif

/*!
 *  \brief Dot for SeqVector
 *
 *  \details
 *  Compute the inner product of two vectors
 *
 *  \tparam DataType Data type of the vector
 *
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *
 *  \return The result of the dot product
 */
template<class DataType>
	static DataType dot(const SeqVector<DataType>& _x, const SeqVector<DataType>& _y)
{
#if defined (WITH_ATLAS) || defined (_WITH_MKL)
	YALLA_LOG("Not BLAS DOT");
#endif
	const int size = _x.getSize();
	DataType sum = 0;
	for(int i=0;i<size;++i)
		sum += _x(i) * _y(i);
	return sum;
}

#ifdef WITH_MPI
/*!
 *  \brief Dot for DistVector
 *
 *  \details
 *  Compute the inner product of two vectors
 *
 *  \tparam DataType Data type of the vector
 *
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *
 *  \return The result of the dot product
 */
template<class DataType>
static DataType dot(const DistVector<DataType>& _x, const DistVector<DataType>& _y)
{
#if defined (WITH_ATLAS) || defined (_WITH_MKL)
	YALLA_LOG("Not BLAS DOT");
#endif
	const int size = _x.getOwnSize();
	DataType sum = 0;
	for(int i=0;i<size;++i)
		sum += _x(i) * _y(i);
	DataType otherSum = sum;
	YALLA_ERROR_CHECK(ParUtils::Allreduce(&sum,&otherSum, 1, PromoteToMPI<DataType>::data_type(),MPI_SUM));
	return otherSum;
}
#endif

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief Dot for EpetraVector
 *
 *  \details
 *  Compute the inner product of two vectors
 *
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *
 *  \return The result of the dot product
 */
static double dot(const EpetraVector<Epetra_Vector>& _x, const EpetraVector<Epetra_Vector>& _y)
{
	double result;
	_x.getVector()->Dot(*_y.getVector(),&result);
	return result;
}
#endif

#if defined (WITH_ATLAS) || defined (_WITH_MKL)
/*!
 *  \brief sDot for SeqVector with BLAS
 *
 *  \details
 *  Compute the inner product of two vectors
 *
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *
 *  \return The result of the dot product
 */
static float dot(const SeqVector<float>& _x, const SeqVector<float>& _y)
{
	const int size = _x.getSize();
	return cblas_sdot(size, _x.getPointeur(), 1, _y.getPointeur(), 1);
}

/*!
 *  \brief dDot for SeqVector with BLAS
 *
 *  \details
 *  Compute the inner product of two vectors
 *
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *
 *  \return The result of the dot product
 */
static double dot(const SeqVector<double>& _x, const SeqVector<double>& _y)
{
	const int size = _x.getSize();
	return cblas_ddot(size, _x.getPointeur(), 1, _y.getPointeur(), 1);
}

#ifdef WITH_MPI
/*!
 *  \brief sDot for DistVector with BLAS
 *
 *  \details
 *  Compute the inner product of two vectors
 *
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *
 *  \return The result of the dot product
 */
static float dot(const DistVector<float>& _x, const DistVector<float>& _y)
{
	const int size = _x.getOwnSize();
	float sum = cblas_sdot(size, _x.getPointeur(), 1, _y.getPointeur(), 1);
	float otherSum = sum;
	ParUtils::Allreduce(&sum,&otherSum, 1, PromoteToMPI<float>::data_type(),MPI_SUM);
	return otherSum;
}

/*!
 *  \brief dDot for DistVector with BLAS
 *
 *  \details
 *  Compute the inner product of two vectors
 *
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *
 *  \return The result of the dot product
 */
static double dot(const DistVector<double>& _x, const DistVector<double>& _y)
{
	const int size = _x.getOwnSize();
	double sum = cblas_ddot(size, _x.getPointeur(), 1, _y.getPointeur(), 1);
	double otherSum = sum;
	ParUtils::Allreduce(&sum,&otherSum, 1, PromoteToMPI<double>::data_type(),MPI_SUM);
	return otherSum;
}
#endif // WITH_MPI

#endif // WITH_ATLAS

/*!
 *  \brief nrm2
 *
 *  \details
 *  Compute the L2 norm of a vector
 *
 *  \tparam DataType Data type of the vector
*
 *  \param[in] _x The vector
 *
 *  \return The L2 norm of the vector
 */
template<typename VectorType>
static typename VectorType::data_type nrm2(const VectorType& _x)
{
#if defined (WITH_ATLAS) || defined (_WITH_MKL)
	YALLA_LOG("Not BLAS NRM2");
#endif
	return std::sqrt(BLAS::dot(_x,_x));
}

#if defined (WITH_ATLAS) || defined (_WITH_MKL)
/*!
 *  \brief snrm2 for SeqVector
 *
 *  \details
 *  Compute the L2 norm of a vector
 *
 *  \param[in] _x The vector
 *
 *  \return The L2 norm of the vector
 */
static float nrm2(const SeqVector<float>& _x)
{
	const int size = _x.getSize();
	return cblas_snrm2(size,_x.getPointeur(),1);
}

/*!
 *  \brief dnrm2 for SeqVector
 *
 *  \details
 *  Compute the L2 norm of a vector
 *
 *  \param[in] _x The vector
 *
 *  \return The L2 norm of the vector
 */
static double nrm2(const SeqVector<double>& _x)
{
	const int size = _x.getSize();
	return cblas_dnrm2(size,_x.getPointeur(),1);
}

#ifdef WITH_MPI
/*!
 *  \brief snrm2 for DistVector
 *
 *  \details
 *  Compute the L2 norm of a vector
 *
 *  \param[in] _x The vector
 *
 *  \return The L2 norm of the vector
 */
static float nrm2(const DistVector<float>& _x)
{
	return std::sqrt(BLAS::dot(_x,_x));
}

/*!
 *  \brief dnrm2 for DistVector
 *
 *  \details
 *  Compute the L2 norm of a vector
 *
 *  \param[in] _x The vector
 *
 *  \return The L2 norm of the vector
 */
static double nrm2(const DistVector<double>& _x)
{
	return std::sqrt(BLAS::dot(_x,_x));
}
#endif

#endif

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief snrm2 for EpetraVector
 *
 *  \details
 *  Compute the L2 norm of a vector
 *
 *  \param[in] _x The vector
 *
 *  \return The L2 norm of the vector
 */
static double nrm2(const EpetraVector<Epetra_Vector>& _x)
{
	double result;
	_x.getVector()->Norm2(&result);
	return result;
}
#endif

/*!
 *  \brief Copy
 *
 *  \details
 *  Copy of a vector
 *
 *  \tparam DataType The data type of the vector
 *  \tparam VectorType The type of the vector
 *
 *  \param[in] _src The vector source
 *  \param[in] _dest The vector destination
 */
template<class DataType, template<class VectorDataType> class VectorType>
	static void copy(const VectorType<DataType>& _src, VectorType<DataType>& _dest)
{
#if defined (WITH_ATLAS) || defined (_WITH_MKL)
	YALLA_LOG("NOT BLAS Copy");
#endif
	_dest.fill(_src.getPointeur());
}

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief Copy for EpetraVector
 *
 *  \details
 *  Copy of a vector
 *
 *  \tparam DataType The data type of the vector
 *  \tparam VectorType The type of the vector
 *
 *  \param[in] _src The vector source
 *  \param[in] _dest The vector destination
 */
static void copy(const EpetraVector<Epetra_Vector>& _src, EpetraVector<Epetra_Vector>& _dest)
{
	_dest = _src;
}
#endif

#if defined (WITH_ATLAS) || defined (_WITH_MKL)
/*!
 *  \brief sCopy for SeqVector with BLAS
 *
 *  \details
 *  Copy of a vector
 *
 *  \param[in] _src The vector source
 *  \param[in] _dest The vector destination
 */
static void copy(const SeqVector<float>& _src, SeqVector<float>& _dest)
{
	const int size = _src.getSize();
	cblas_scopy(size, _src.getPointeur(), 1, &_dest(0), 1);
}

/*!
 *  \brief dCopy for SeqVector with BLAS
 *
 *  \details
 *  Copy of a vector
 *
 *  \param[in] _src The vector source
 *  \param[in] _dest The vector destination
 */
static void copy(const SeqVector<double>& _src, SeqVector<double>& _dest)
{
	const int size = _src.getSize();
	cblas_dcopy(size, _src.getPointeur(), 1, &_dest(0), 1);
}

#ifdef WITH_MPI
/*!
 *  \brief sCopy for DistVector with BLAS
 *
 *  \details
 *  Copy of a vector
 *
 *  \param[in] _src The vector source
 *  \param[in] _dest The vector destination
 */
static void copy(const DistVector<float>& _src, DistVector<float>& _dest)
{
	const int size = _src.getOwnSize();
	cblas_scopy(size, _src.getPointeur(), 1, &_dest(0), 1);
}

/*!
 *  \brief dCopy for DistVector with BLAS
 *
 *  \details
 *  Copy of a vector
 *
 *  \param[in] _src The vector source
 *  \param[in] _dest The vector destination
 */
static void copy(const DistVector<double>& _src, DistVector<double>& _dest)
{
	const int size = _src.getOwnSize();
	cblas_dcopy(size, _src.getPointeur(), 1, &_dest(0), 1);
}
#endif // WITH_MPI

#endif // WITH_ATLAS

/*!
 *  \brief Scal for a SeqVector
 *
 *  \details
 *  Scale a vector
 *
 *  \tparam DataType The data type of the vector
 *
 *  \param[in] _val The scale factor
 *  \param[in] _vect The vector to scale
 */
template<class DataType>
static void scal(const DataType& _val, SeqVector<DataType>& _vect)
{
#if defined (WITH_ATLAS) || defined (_WITH_MKL)
	YALLA_LOG("Not BLAS scal");
#endif
	const int size = _vect.getSize();
	for(int i=0;i<size;++i)
		_vect(i) *= _val;
}

#ifdef WITH_MPI
/*!
 *  \brief Scal for a DistVector
 *
 *  \details
 *  Scale a vector
 *
 *  \tparam DataType The data type of the vector
 *
 *  \param[in] _val The scale factor
 *  \param[in] _vect The vector to scale
 */
template<class DataType>
static void scal(const DataType& _val, DistVector<DataType>& _vect)
{
#if defined (WITH_ATLAS) || defined (_WITH_MKL)
	YALLA_LOG("Not BLAS scal");
#endif
	const int size = _vect.getOwnSize();
	for(int i=0;i<size;++i)
		_vect(i) *= _val;
}
#endif

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief Scal for a EpetraVector
 *
 *  \details
 *  Scale a vector
 *
 *  \tparam DataType The data type of the vector
 *
 *  \param[in] _val The scale factor
 *  \param[in] _vect The vector to scale
 */
static void scal(const double& _val, EpetraVector<Epetra_Vector>& _vect)
{
	_vect.getVector()->Scale(_val);
}
#endif

#if defined (WITH_ATLAS) || defined (_WITH_MKL)
/*!
 *  \brief sScal for a SeqVector with BLAS
 *
 *  \details
 *  Scale a vector
 *
 *  \tparam DataType The data type of the vector
 *
 *  \param[in] _val The scale factor
 *  \param[in] _vect The vector to scale
 */
static void scal(const float& _val, SeqVector<float>& _vect)
{
	const int size = _vect.getSize();
	cblas_sscal(size, _val, &_vect(0), 1);
}

/*!
 *  \brief dScal for a SeqVector with BLAS
 *
 *  \details
 *  Scale a vector
 *
 *  \tparam DataType The data type of the vector
 *
 *  \param[in] _val The scale factor
 *  \param[in] _vect The vector to scale
 */
static void scal(const double& _val, SeqVector<double>& _vect)
{
	const int size = _vect.getSize();
	cblas_dscal(size, _val, &_vect(0), 1);
}

#ifdef WITH_MPI
/*!
 *  \brief sScal for a DistVector with BLAS
 *
 *  \details
 *  Scale a vector
 *
 *  \tparam DataType The data type of the vector
 *
 *  \param[in] _val The scale factor
 *  \param[in] _vect The vector to scale
 */
static void scal(const float& _val, DistVector<float>& _vect)
{
	const int size = _vect.getOwnSize();
	cblas_sscal(size, _val, &_vect(0), 1);
}

/*!
 *  \brief dScal for a DistVector with BLAS
 *
 *  \details
 *  Scale a vector
 *
 *  \tparam DataType The data type of the vector
 *
 *  \param[in] _val The scale factor
 *  \param[in] _vect The vector to scale
 */
static void scal(const double& _val, DistVector<double>& _vect)
{
	const int size = _vect.getOwnSize();
	cblas_dscal(size, _val, &_vect(0), 1);
}
#endif // WITH_MPI

#endif // WITH_ATLAS

#endif
