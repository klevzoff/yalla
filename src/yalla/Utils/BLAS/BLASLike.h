#pragma once
#ifndef yalla_blas_like_h
#define yalla_blas_like_h

/*!
 *  \file BLASLike.h
 *  \brief BLAS-like operations
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

/*!
 *  \brief axpyz
 *
 *  \details
 *  Compute z = a . x + y
 *
 *  \tparam DataType Data type of the vector
 *  \tparam VectorType The type of the vector
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *  \param[in,out] _z The resulting vector
 */
template<typename DataType, typename VectorType>
	static void axpyz(const DataType& _a, const VectorType& _x, const VectorType& _y, VectorType& _z)
{
	const DataType zero = 0.;
	const DataType one = 1;
	BLAS::scal(zero,_z);
	BLAS::axpy(_a,_x,_z);
	BLAS::axpy(one,_y,_z);
}

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief axpyz for EpetraVector
 *
 *  \details
 *  Compute z = a . x + y
 *
 *  \param[in] _a The scalar to multiply the first vector with
 *  \param[in] _x The first vector
 *  \param[in] _y The second vector
 *  \param[in,out] _z The resulting vector
 */
static void axpyz(const double& _a, const EpetraVector<Epetra_Vector>& _x, const EpetraVector<Epetra_Vector>& _y, EpetraVector<Epetra_Vector>& _z)
{
	_z.fill(0.);
	_z.getVector()->Update(_a, *_x.getVector(), 1., *_y.getVector(), 1.);
}
#endif

/*!
 *  \brief Compute the residual and return its norm
 *
 *  \details
 *  Compute nrm2(b-Ax)
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixType The type of the matrix
 *  \tparam MatrixImpl The actual matrix implementation
 *  \tparam VectorType The type of the vectors
 *
 *  \param[in] _A The matrix
 *  \param[in] _b The rhs
 *  \param[in] _x The lhs
 *
 *  \return The L2 norm of the residual
 */
template<class DataType, template<class MatrixDataType, class MatrixImplType> class MatrixType, template<class MatrixDataType> class MatrixImpl, template<class VectorDataType> class VectorType>
	static DataType computeNormOfResidual(const MatrixType<DataType, MatrixImpl<DataType> >& _A, const VectorType<DataType>& _b, const VectorType<DataType>& _x)
{
	DataType one = 1.;
	DataType minusOne = -1.;
	VectorType<DataType> tmpVector(_x);
	BLAS::scal(0.,tmpVector);
	BLAS::gemv(1.,_A,_x,1.,tmpVector);
	BLAS::scal(minusOne, tmpVector);
	BLAS::axpy(one,_b,tmpVector);
	return BLAS::nrm2(tmpVector);
}

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief Compute the residual and return its norm for Epetra elements
 *
 *  \details
 *  Compute nrm2(b-Ax)
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixType The type of the matrix
 *  \tparam MatrixImpl The actual matrix implementation
 *  \tparam VectorType The type of the vectors
 *
 *  \param[in] _A The matrix
 *  \param[in] _b The rhs
 *  \param[in] _x The lhs
 *
 *  \return The L2 norm of the residual
 */
static double computeNormOfResidual(const DistSparseMatrix<double,EpetraMatrix<Epetra_CrsMatrix> >& _A, const EpetraVector<Epetra_Vector>& _b, const EpetraVector<Epetra_Vector>& _x)
{
	EpetraVector<Epetra_Vector> tmpVector(_x);
	tmpVector.fill(0.);
	BLAS::gemv(1.,_A,_x,1.,tmpVector);
	const int size = tmpVector.getSize();
	for(int i=0;i<size;++i)
	{
		tmpVector(i) *= -1;
		tmpVector(i) += _b(i);
	}
	return BLAS::nrm2(tmpVector);
}
#endif

/*!
 *  \brief Compute the residual and its norm
 *
 *  \details
 *  Compute r = b-Ax and nrm2(r)
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixType The type of the matrix
 *  \tparam MatrixImpl The actual matrix implementation
 *  \tparam VectorType The type of the vectors
 *
 *  \param[in] _A The matrix
 *  \param[in] _b The rhs
 *  \param[in] _x The lhs
 *  \param[in,out] _r The residual
 *
 *  \return The L2 norm of the residual
 */
template<class DataType, template<class MatrixDataType, class MatrixImplType> class MatrixType, template<class MatrixDataType> class MatrixImpl, template<class VectorDataType> class VectorType>
	static DataType computeNormAndResidual(const MatrixType<DataType, MatrixImpl<DataType> >& _A, const VectorType<DataType>& _b, const VectorType<DataType>& _x, VectorType<DataType>& _r)
{
	const DataType one = 1.;
	const DataType minusOne = -1.;
	BLAS::gemv(1.,_A,_x,1.,_r);
	BLAS::scal(minusOne,_r);
	BLAS::axpy(one,_b,_r);
	return BLAS::nrm2(_r);
}

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief Compute the residual and its norm for Epetra elements
 *
 *  \details
 *  Compute r = b-Ax and nrm2(r)
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixType The type of the matrix
 *  \tparam MatrixImpl The actual matrix implementation
 *  \tparam VectorType The type of the vectors
 *
 *  \param[in] _A The matrix
 *  \param[in] _b The rhs
 *  \param[in] _x The lhs
 *  \param[in,out] _r The residual
 *
 *  \return The L2 norm of the residual
 */
static double computeNormAndResidual(const DistSparseMatrix<double,EpetraMatrix<Epetra_CrsMatrix> >& _A, const EpetraVector<Epetra_Vector>& _b, const EpetraVector<Epetra_Vector>& _x, EpetraVector<Epetra_Vector>& _r)
{
	BLAS::gemv(.,_A,_x,1.,_r);
	const int size = _r.getSize();
	for(int i=0;i<size;++i)
	{
		_r(i) *= -1;
		_r(i) += _b(i);
	}
	return BLAS::nrm2(_r);
}
#endif

/*!
 *  \brief Copy a (transposed) matrix
 *
 *  \details
 *  Copy A or At
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixType The type of the matrix
 *  \tparam MatrixImpl The actual matrix implementation
 *
 *  \param[in] _trans Flag for matrix transposition
 *  \param[in] _src The matrix to copy
 *  \param[in,out] _dest The (transposed) matrix copy
 */
template<class DataType, template<class MatrixDataType, class MatrixImplType> class MatrixType, template<class MatrixDataType> class MatrixImpl>
	static void copy(const Transpose _trans, const MatrixType<DataType, MatrixImpl<DataType> > & _src, MatrixType<DataType, MatrixImpl<DataType> > & _dest)
{
	if(_trans==NoTrans)
		_dest = _src;
	else
		BLAS::transpose(_src,_dest);
}

/*!
 *  \brief Transpose a sequential dense matrix 
 *
 *  \details
 *  Cmpute At from A
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixImpl The actual matrix implementation
 *
 *  \param[in] _src The matrix to copy
 *  \param[in,out] _dest The transposed matrix copy
 */
template<class DataType, template<class MatrixDataType> class MatrixImpl>
	static void transpose(const SeqDenseMatrix<DataType, MatrixImpl<DataType> > & _src, SeqDenseMatrix<DataType, MatrixImpl<DataType> > & _dest)
{
	const int nbRows = _src.getNbRows();
	const int nbCols = _src.getNbCols();
	_dest.initialize(nbCols,nbRows);
	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
		{
			_dest(j,i) = _src(i,j);
		}
	}
}

/*!
 *  \brief Transpose a sequential sparse matrix 
 *
 *  \details
 *  Cmpute At from A
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixImpl The actual matrix implementation
 *
 *  \param[in] _src The matrix to copy
 *  \param[in,out] _dest The transposed matrix copy
 */
template<class DataType, template<class MatrixDataType> class MatrixImpl>
	static void transpose(const SeqSparseMatrix<DataType, MatrixImpl<DataType> > & _src, SeqSparseMatrix<DataType, MatrixImpl<DataType> > & _dest)
{
	typedef DataType data_type;
	const int nbRows = _src.getNbRows();
	const int nbCols = _src.getNbCols();
	const int nbNnz = _src.getNnz();
	const data_type* data = _src.getPointeur();
	const typename SeqSparseMatrix<DataType, MatrixImpl<DataType> >::impl_type::graph_type& srcGraph = _src.getMatrixGraph();

	int* tmpRow = new int[nbCols+1];
	int* tmpCol = new int[nbNnz];
	data_type* tmpDatas = new data_type[nbNnz];
	
	for(int i=0;i<nbCols+1;++i)
		tmpRow[i] = 0;

	for(int i=0;i<nbNnz;++i)
	{
		tmpCol[i] = nbRows+1;
		tmpDatas[i] = 0;
	}

	for(int i=0;i<nbNnz;++i)
		++tmpRow[srcGraph.getColPtr(i)+1];

	for(int i=1;i<nbCols+1;++i)
		tmpRow[i] += tmpRow[i-1];

	for(int i=0;i<nbRows;++i)
	{
		for(int j=srcGraph.getRowPtr(i);j<srcGraph.getRowPtr(i+1);++j)
		{
			const int formerCol = srcGraph.getColPtr(j);
			int offset = 0;
			while(tmpCol[tmpRow[formerCol]+offset]!=nbRows+1)
				++offset;
			tmpCol[tmpRow[formerCol]+offset] = i;
			tmpDatas[tmpRow[formerCol]+offset] = data[j];
		}
	}

	_dest.initialize(nbCols,nbRows,nbNnz,tmpRow,tmpCol);
	_dest.fill(tmpDatas);

	delete[] tmpRow;
	delete[] tmpCol;
	delete[] tmpDatas;
}

#ifdef WITH_MPI
/*!
 *  \brief Transpose a distributed dense matrix 
 *
 *  \details
 *  Cmpute At from A
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixImpl The actual matrix implementation
 *
 *  \param[in] _src The matrix to copy
 *  \param[in,out] _dest The transposed matrix copy
 */
template<class DataType, template<class MatrixDataType> class MatrixImpl>
	static void transpose(const DistDenseMatrix<DataType, MatrixImpl<DataType> > & _src, DistDenseMatrix<DataType, MatrixImpl<DataType> > & _dest)
{
	typedef DataType data_type;
	const int nbGlobalRows = _src.getGlobalNbRows();
	const int nbGlobalCols = _src.getGlobalNbCols();
	const int nbLocRows = _src.getNbRows();
	const int nbLocCols = _src.getNbCols();

	const int numProc = ParUtils::getNumProc();
	const int procRank = ParUtils::getProcRank();

	const int newAverageColsByProcs = nbGlobalRows/numProc;
	int newLocCols = newAverageColsByProcs;
	if(procRank==numProc-1)
		newLocCols = nbGlobalRows-(numProc-1)*newAverageColsByProcs;

	int* newMapping = new int[newLocCols];
	for(int i=0;i<newLocCols;++i)
		newMapping[i] = newAverageColsByProcs*procRank+i;

	_dest.initialize(nbGlobalCols,newLocCols,nbGlobalCols,nbGlobalRows,newMapping);

	delete[] newMapping;

	// Packing and sending datas to the relevant procs.
	for(int i=0;i<numProc;++i)
	{
		if(i!=procRank)
		{
			int startingRow = i*newAverageColsByProcs;
			int endingRow = (i+1)*newAverageColsByProcs;
			if(i==numProc-1)
				endingRow = nbLocRows;
			const int sizeMessage = 4 + nbLocCols + (endingRow-startingRow)*nbLocCols;
			data_type* dataToSendToProcI = new data_type[sizeMessage];

			dataToSendToProcI[0] = endingRow-startingRow;
      dataToSendToProcI[1] = nbLocCols;
      dataToSendToProcI[2] = nbGlobalRows;
      dataToSendToProcI[3] = nbGlobalCols;

			int counter = 4;
			for(int j=0;j<nbLocCols;++j)
			{
				dataToSendToProcI[counter] = _src.getMapping(j);
				++counter;
			}

			for(int j=startingRow;j<endingRow;++j)
			{
				for(int k=0;k<nbLocCols;++k)
				{
					dataToSendToProcI[counter] = _src(j,k);
					++counter;
				}
			}

			YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), i));

			delete[] dataToSendToProcI;
		}
	}
	
	// Local transpose
	const int startingRow = procRank*newAverageColsByProcs;
	for(int i=0;i<newLocCols;++i)
	{
		for(int j=0;j<nbLocCols;++j)
		{
			_dest(_src.getMapping(j),i) = _src(startingRow+i,j);
		}
	}

	// Global transpose
	for(int i=0;i<numProc-1;++i)
	{
		int sizeMessage;
		MPI_Status msg_status;
		YALLA_ERROR_CHECK(ParUtils::ProbeFrom(MPI_ANY_SOURCE, &msg_status));
		YALLA_ERROR_CHECK(ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &sizeMessage));
		data_type* dataToReceiveFromProcI = new data_type[sizeMessage];

		YALLA_ERROR_CHECK(ParUtils::RecvFrom(dataToReceiveFromProcI,sizeMessage,PromoteToMPI<data_type>::data_type(),msg_status.MPI_SOURCE));

		const int receivedRow = static_cast<int>(dataToReceiveFromProcI[0]);
		const int receivedCol = static_cast<int>(dataToReceiveFromProcI[1]);
		for(int j=0;j<receivedRow;++j)
		{
			for(int k=0;k<receivedCol;++k)
			{
				_dest(static_cast<int>(dataToReceiveFromProcI[4+k]),j) = dataToReceiveFromProcI[4+receivedCol+j*receivedCol+k];
			}
		}
		delete[] dataToReceiveFromProcI;
	}
}

/*!
 *  \brief Transpose a distributed sparse matrix 
 *
 *  \details
 *  Cmpute At from A
 *
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixImpl The actual matrix implementation
 *
 *  \param[in] _src The matrix to copy
 *  \param[in,out] _dest The transposed matrix copy
 *
 *  \warning Not sure if this method is still working correctly ...
 */
template<class DataType, template<class MatrixDataType> class MatrixImpl>
	static void transpose(const DistSparseMatrix<DataType, MatrixImpl<DataType> > & _src, DistSparseMatrix<DataType, MatrixImpl<DataType> > & _dest)
{
	typedef DataType data_type;
	const int nbGlobalRows = _src.getGlobalNbRows();
	const int nbGlobalCols = _src.getGlobalNbCols();
	const int nbLocRows = _src.getNbRows();
	const int nbLocCols = _src.getNbCols();
	const typename DistSparseMatrix<DataType, MatrixImpl<DataType> >::impl_type::graph_type& srcGraph = _src.getMatrixGraph();

	const int numProc = ParUtils::getNumProc();
	const int procRank = ParUtils::getProcRank();

	const int newAverageRowsByProcs = nbGlobalCols/numProc;
	int newLocRows = newAverageRowsByProcs;
	if(procRank==numProc-1)
		newLocRows = nbGlobalCols-(numProc-1)*newAverageRowsByProcs;

	int* newMapping = new int[newLocRows];
	std::map<int,int> globalToLocalMapping;
	for(int i=0;i<newLocRows;++i)
	{
		newMapping[i] = newAverageRowsByProcs*procRank+i;
		globalToLocalMapping[newMapping[i]] = i;
	}
	const int newLowerGlobalRowIndex = newMapping[0];
	const int newUpperGlobalRowIndex = newMapping[newLocRows-1];

	int* numMessToSend = new int[numProc];
	int* scatterPattern = new int[numProc];
	int numOfMessagesToReceive = 0;
	for(int i=0;i<numProc;++i)
	{
		numMessToSend[i] = 0;
		scatterPattern[i] = 1;
	}
	
	std::map<int, int> procToIndexMap;
	std::map<int, int> newNnzOnProc;
	std::map<int, int> lowerBounds;
	std::map<int, int> upperBounds;
	typedef std::map<int, int>::iterator myMapIterator;
	int nnzStayingOnThisProc = 0;
	int index = 0;
	for(int i=0;i<nbLocRows;++i)
	{
		for(int j=srcGraph.getRowPtr(i);j<srcGraph.getRowPtr(i+1);++j)
		{
			if(srcGraph.getColPtr(j)<newLowerGlobalRowIndex || srcGraph.getColPtr(j)>newUpperGlobalRowIndex)
			{
				int colJWillBeOnProc = (srcGraph.getColPtr(j) - (srcGraph.getColPtr(j)%newAverageRowsByProcs))/newAverageRowsByProcs;
				if(colJWillBeOnProc>=numProc)
					colJWillBeOnProc=ParUtils::getNumProc()-1;
				numMessToSend[colJWillBeOnProc] = 1;
				++newNnzOnProc[colJWillBeOnProc];
				myMapIterator it = lowerBounds.find(colJWillBeOnProc);
				if(it==lowerBounds.end() || lowerBounds[colJWillBeOnProc]>srcGraph.getColPtr(j))
					lowerBounds[colJWillBeOnProc] = srcGraph.getColPtr(j);
				it = upperBounds.find(colJWillBeOnProc);
				if(it==upperBounds.end() || upperBounds[colJWillBeOnProc]<srcGraph.getColPtr(j))
					upperBounds[colJWillBeOnProc] = srcGraph.getColPtr(j);
				it = procToIndexMap.find(colJWillBeOnProc);
				if(it==procToIndexMap.end())
				{
					procToIndexMap[colJWillBeOnProc] = index;
					++index;
				}
			}
			else
				++nnzStayingOnThisProc;
		}
	}

	YALLA_ERROR_CHECK(ParUtils::reduceScatter(numMessToSend, &numOfMessagesToReceive, scatterPattern, PromoteToMPI<int>::data_type(), MPI_SUM));
	delete[] numMessToSend;
	numMessToSend = 0;
	
	data_type** dataToSendToProc = new data_type*[newNnzOnProc.size()];
	for(myMapIterator it=newNnzOnProc.begin(); it!=newNnzOnProc.end();++it)
	{
		const int index = procToIndexMap[it->first];
		dataToSendToProc[index] = new data_type[5+nbLocRows+nbLocRows+1+it->second+it->second];
		dataToSendToProc[index][0] = nbLocRows;
		dataToSendToProc[index][1] = nbLocCols;
		dataToSendToProc[index][2] = nbGlobalRows;
		dataToSendToProc[index][3] = nbGlobalCols;
		dataToSendToProc[index][4] = it->second;
		int counter = 5;
		for(int i=0;i<nbLocRows;++i)
		{
			dataToSendToProc[index][counter] = _src.getMapping(i);
			++counter;
		}

		// To initialize the first value of rowPtr to 0
		dataToSendToProc[index][counter] = 0;
	}
	
	// Packing the datas
	std::map<int,int> indexCounterMap;
	for(int i=0;i<nbLocRows;++i)
	{
		for(int j=srcGraph.getRowPtr(i);j<srcGraph.getRowPtr(i+1);++j)
		{
			if(srcGraph.getColPtr(j)<newLowerGlobalRowIndex || srcGraph.getColPtr(j)>newUpperGlobalRowIndex)
			{
				int colJWillBeOnProc = (srcGraph.getColPtr(j) - (srcGraph.getColPtr(j)%newAverageRowsByProcs))/newAverageRowsByProcs;
				if(colJWillBeOnProc>=ParUtils::getNumProc())
					colJWillBeOnProc=ParUtils::getNumProc()-1;
				
				const int index = procToIndexMap[colJWillBeOnProc];
				++dataToSendToProc[index][5+nbLocRows+1+i];
				dataToSendToProc[index][5+nbLocRows+nbLocRows+1+indexCounterMap[index]] = srcGraph.getColPtr(j);
				dataToSendToProc[index][5+nbLocRows+nbLocRows+1+newNnzOnProc[colJWillBeOnProc]+indexCounterMap[index]] = _src[j];
				++indexCounterMap[index];
			}
		}
	}

	for(int i=0;i<procToIndexMap.size();++i)
	{
		for(int j=0;j<nbLocRows;++j)
		{
			dataToSendToProc[i][5+nbLocRows+1+j] += dataToSendToProc[i][5+nbLocRows+j];
		}
	}

	// Sending the datas
	for(myMapIterator it=newNnzOnProc.begin(); it!=newNnzOnProc.end();++it)
	{
		const int size = 5+nbLocRows+nbLocRows+1+it->second+it->second;
		const int index = procToIndexMap[it->first];
		const int dest = it->first;
		YALLA_ERROR_CHECK(ParUtils::SendTo(dataToSendToProc[index], size, PromoteToMPI<data_type>::data_type(), dest));
	}

	data_type** receivedDatas = new data_type*[numOfMessagesToReceive];
	// Receiving the datas
	for(int i=0;i<numOfMessagesToReceive;++i)
	{
		int sizeMessage;
		MPI_Status msg_status;
		YALLA_ERROR_CHECK(ParUtils::ProbeFrom(MPI_ANY_SOURCE, &msg_status));
		YALLA_ERROR_CHECK(ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &sizeMessage));
		receivedDatas[i] = new data_type[sizeMessage];
		
		YALLA_ERROR_CHECK(ParUtils::RecvFrom(receivedDatas[i],sizeMessage,PromoteToMPI<data_type>::data_type(),msg_status.MPI_SOURCE));
	}
	
	int newNnz = nnzStayingOnThisProc;
	for(int i=0;i<numOfMessagesToReceive;++i)
	{
		newNnz += static_cast<int>(receivedDatas[i][4]);
	}
	
	int* tmpRowPtr = new int[newLocRows+1];
	int* tmpColPtr = new int[newNnz];
	data_type* tmpDatas = new data_type[newNnz];

	// Building the row pointer
	for(int i=0;i<newLocRows+1;++i)
		tmpRowPtr[i] = 0;

	// Global
	for(int i=0;i<numOfMessagesToReceive;++i)
	{
		for(int j=0;j<receivedDatas[i][4];++j)
		{
			const int col = static_cast<int>(receivedDatas[i][5+2*static_cast<int>(receivedDatas[i][0])+1+j]);
			++tmpRowPtr[globalToLocalMapping[col]+1];
		}
	}

	// Local
	for(int i=0;i<nbLocRows;++i)
	{
		for(int j=srcGraph.getRowPtr(i);j<srcGraph.getRowPtr(i+1);++j)
		{
			if(srcGraph.getColPtr(j)>=newLowerGlobalRowIndex && srcGraph.getColPtr(j)<=newUpperGlobalRowIndex)
			{
				++tmpRowPtr[globalToLocalMapping[srcGraph.getColPtr(j)]+1];
			}
		}
	}

	// Sum
	for(int i=1;i<newLocRows;++i)
		tmpRowPtr[i+1] += tmpRowPtr[i];

	// Building the col and data pointeur
	for(int i=0;i<newNnz;++i)
	{
		tmpColPtr[i] = nbGlobalRows+1;
		tmpDatas[i] = 0;
	}

	// Global
	for(int i=0;i<numOfMessagesToReceive;++i) // message loop
	{
		const int nbReceivedRows = static_cast<int>(receivedDatas[i][0]);
		for(int j=0;j<nbReceivedRows;++j) // row loop
		{
			const int nbReceivedNnz = static_cast<int>(receivedDatas[i][4]);
			const int startingRowIndex = static_cast<int>(receivedDatas[i][5+nbReceivedRows+j]);
			const int endingRowIndex = static_cast<int>(receivedDatas[i][5+nbReceivedRows+j+1]);
			for(int k=startingRowIndex;k<endingRowIndex;++k) // col loop
			{
				const int formerCol = static_cast<int>(receivedDatas[i][5+nbReceivedRows+nbReceivedRows+1+k]);
				const int newCol = static_cast<int>(receivedDatas[i][5+j]);
				const data_type newData = static_cast<int>(receivedDatas[i][5+nbReceivedRows+nbReceivedRows+1+nbReceivedNnz+k]);
				int offset = 0;
				while(tmpColPtr[tmpRowPtr[globalToLocalMapping[formerCol]]+offset]!=nbGlobalRows+1)
					++offset;
				tmpColPtr[tmpRowPtr[globalToLocalMapping[formerCol]]+offset] = newCol;
				tmpDatas[tmpRowPtr[globalToLocalMapping[formerCol]]+offset] = newData;
			}
		}
	}

	// Local
	for(int i=0;i<nbLocRows;++i)
	{
		for(int j=srcGraph.getRowPtr(i);j<srcGraph.getRowPtr(i+1);++j)
		{
			if(srcGraph.getColPtr(j)>=newLowerGlobalRowIndex && srcGraph.getColPtr(j)<=newUpperGlobalRowIndex)
			{
				const int formerCol = srcGraph.getColPtr(j);
				const int newCol = _src.getMapping(i);
				const data_type newData = _src[j];
				int offset = 0;
				while(tmpColPtr[tmpRowPtr[globalToLocalMapping[formerCol]]+offset]!=nbGlobalRows+1)
					++offset;
				tmpColPtr[tmpRowPtr[globalToLocalMapping[formerCol]]+offset] = newCol;
				tmpDatas[tmpRowPtr[globalToLocalMapping[formerCol]]+offset] = newData;
			}
		}
	}

	// Sorting cols and datas accordingly by row
	for(int i=0;i<newLocRows;++i)
		DoubleQuickSort<int,data_type>::sort(tmpColPtr,tmpDatas,tmpRowPtr[i],tmpRowPtr[i+1]-1);

	// Build the matrix
	_dest.initialize(newLocRows,nbGlobalRows,newNnz,nbGlobalCols,nbGlobalRows,newMapping,tmpRowPtr,tmpColPtr);
	_dest.fill(tmpDatas);

	for(int i=0;i<newNnzOnProc.size();++i)
	{
		delete[] dataToSendToProc[i];
		dataToSendToProc[i] = 0;
	}

	for(int i=0;i<numOfMessagesToReceive;++i)
	{
		delete[] receivedDatas[i];
		receivedDatas[i] = 0;
	}
	delete[] receivedDatas;
	receivedDatas = 0;
	
	delete[] dataToSendToProc;
	dataToSendToProc = 0;
	delete[] newMapping;
	newMapping = 0;

	delete[] tmpRowPtr;
	tmpRowPtr = 0;
	delete[] tmpColPtr;
	tmpColPtr = 0;
	delete[] tmpDatas;
	tmpDatas = 0;
}

#endif // WITH_MPI

#endif
