#pragma once
#ifndef yalla_blas_3_h
#define yalla_blas_3_h

/*!
 *  \file BLAS3.h
 *  \brief BLAS level 3 operations
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 *
 *  \warning gemm for distributed sparse matrices is broken
 *  \todo Probably all the sparse matrices gemm should be re-implemented from scratch. One should implement the Gustavson algorithm for sparse gemm (http://dl.acm.org/citation.cfm?id=355796), for effiency reasons.
 */

/*!
 *  \brief gemm
 *
 *  \details
 *  Compute C += A * B
 *
 *  \tparam DataType The data type of the matrices
 *  \tparam MatrixType The type of the matrices
 *  \tparam MatrixImpl The actual matrices implementation
 *
 *  \param[in] _matLHS The first matrix
 *  \param[in] _matRHS The second matrix
 *  \param[in,out] _matRes The resulting matrix
 */
template<class DataType, template<class MatrixDataType, class MatrixImplType> class MatrixType, template<class MatrixDataType> class MatrixImpl>
	static void gemm(const MatrixType<DataType, MatrixImpl<DataType> >& _matLHS, const MatrixType<DataType, MatrixImpl<DataType> >& _matRHS, MatrixType<DataType, MatrixImpl<DataType> >& _matRes)
{
	const int lNbRows = _matLHS.getNbRows();
	const int lNbCols = _matLHS.getNbCols();
	const int rNbCols = _matRHS.getNbCols();
	for(int i=0;i<lNbRows;++i)
	{
		for(int j=0;j<rNbCols;++j)
		{
			for(int k=0;k<lNbCols;++k)
			{
				_matRes(i,j) += _matLHS(i,k) * _matRHS(k,j);
			}
		}
	}
}

#ifdef WITH_ATLAS
/*!
 *  \brief sgemm for sequential dense matrices with BLAS
 *
 *  \details
 *  Compute C += A * B
 *
 *  \param[in] _matLHS The first matrix
 *  \param[in] _matRHS The second matrix
 *  \param[in,out] _matRes The resulting matrix
 */
template<template<class MatrixDataType> class MatrixImpl>
static void gemm(const SeqDenseMatrix<float, MatrixImpl<float> > & _A, const SeqDenseMatrix<float, MatrixImpl<float> >& _B, SeqDenseMatrix<float, MatrixImpl<float> >& _C)
{
	cblas_sgemm(PromoteToBLASOrder<SeqDenseMatrix<float, MatrixImpl<float> > >::data_type(), CblasNoTrans, CblasNoTrans, _A.getNbRows(), _B.getNbCols(), _A.getNbCols(), 1, _A.getPointeur(), _A.getNbCols(), _B.getPointeur(), _B.getNbCols(), 1, &_C(0,0), _C.getNbCols());
}

/*!
 *  \brief dgemm for sequential dense matrices with BLAS
 *
 *  \details
 *  Compute C += A * B
 *
 *  \param[in] _matLHS The first matrix
 *  \param[in] _matRHS The second matrix
 *  \param[in,out] _matRes The resulting matrix
 */
template<template<class MatrixDataType> class MatrixImpl>
static void gemm(const SeqDenseMatrix<double, MatrixImpl<double> > & _A, const SeqDenseMatrix<double, MatrixImpl<double> >& _B, SeqDenseMatrix<double, MatrixImpl<double> >& _C)
{
	cblas_dgemm(PromoteToBLASOrder<SeqDenseMatrix<double, MatrixImpl<double> > >::data_type(), CblasNoTrans, CblasNoTrans, _A.getNbRows(), _B.getNbCols(), _A.getNbCols(), 1, _A.getPointeur(), _A.getNbCols(), _B.getPointeur(), _B.getNbCols(), 1, &_C(0,0), _C.getNbCols());
}
#endif // WITH_ATLAS

#ifdef WITH_MPI
/*!
 *  \brief gemm for distributed dense matrices
 *
 *  \details
 *  Compute C += A * B
 *
 *  \tparam DataType The data type of the matrices
 *  \tparam MatrixImpl The actual matrices implementation
 *
 *  \param[in] _matLHS The first matrix
 *  \param[in] _matRHS The second matrix
 *  \param[in,out] _matRes The resulting matrix
 */
template<class DataType, template<class MatrixDataType> class MatrixImpl>
	static void gemm(const DistDenseMatrix<DataType, MatrixImpl<DataType> >& _matLHS, const DistDenseMatrix<DataType, MatrixImpl<DataType> >& _matRHS, DistDenseMatrix<DataType, MatrixImpl<DataType> >& _matRes)
{
	const int numProc = ParUtils::getNumProc();
	const int procRank = ParUtils::getProcRank();
	typedef DataType data_type;
	// Local computations
	const int lNbRows = _matLHS.getNbRows();
	const int rNbCols = _matRHS.getNbCols();
	const int lNbCols = _matLHS.getNbCols();
	for(int i=0;i<lNbRows;++i)
	{
		for(int j=0;j<rNbCols;++j)
		{
			for(int k=0;k<lNbCols;++k)
			{
				const int GlobalLine = _matRHS.getMapping(k);
				_matRes(i,j) += _matLHS(i,k) * _matRHS(GlobalLine,j);
			}
		}
	}
	// Global computations
	// Packing local matrix A
	int sizeMessage = 4 + lNbCols + lNbRows*lNbCols;
	data_type* dataToSendToProcI = new data_type[sizeMessage];
	dataToSendToProcI[0] = lNbRows;
	dataToSendToProcI[1] = lNbCols;
	dataToSendToProcI[2] = _matLHS.getGlobalNbRows();
	dataToSendToProcI[3] = _matLHS.getGlobalNbCols();

	int counter = 4;
	for(int i=0;i<lNbCols;++i)
	{
		dataToSendToProcI[counter] = _matLHS.getMapping(i);
		++counter;
	}

	for(int i=0;i<lNbRows;++i)
	{
		for(int j=0;j<lNbCols;++j)
		{
			dataToSendToProcI[counter] = _matLHS(i,j);
			++counter;
		}
	}

	for(int i=0;i<numProc-1;++i)
	{
		YALLA_ERROR_CHECK(ParUtils::IsendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), Math::modulo((procRank+1),numProc)));
				
		MPI_Status msg_status;
		YALLA_ERROR_CHECK(ParUtils::ProbeFrom(Math::modulo((procRank-1),numProc),&msg_status));
		YALLA_ERROR_CHECK(ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &sizeMessage));
		data_type* dataToReceiveFromProcI = new data_type[sizeMessage];
				
		YALLA_ERROR_CHECK(ParUtils::RecvFrom(dataToReceiveFromProcI,sizeMessage,PromoteToMPI<data_type>::data_type(),Math::modulo((procRank-1),numProc))); // data now contains A from proc-1
		// Global computations
		const int receivedNbCols = static_cast<int>(dataToReceiveFromProcI[1]);
		for(int j=0;j<lNbRows;++j)
		{
			for(int k=0;k<rNbCols;++k)
			{
				for(int l=0;l<receivedNbCols;++l)
				{
					const int GlobalLine = static_cast<int>(dataToReceiveFromProcI[4+l]);
					_matRes(j,k) += dataToReceiveFromProcI[4+receivedNbCols+j*receivedNbCols+l] * _matRHS(GlobalLine,k);
				}
			}
		}
		delete[] dataToSendToProcI;
		dataToSendToProcI = dataToReceiveFromProcI;
	}
	delete[] dataToSendToProcI;
}

/*!
 *  \brief gemm for distributed sparse matrices
 *
 *  \details
 *  Compute C += A * B
 *
 *  \tparam DataType The data type of the matrices
 *  \tparam MatrixImpl The actual matrices implementation
 *
 *  \param[in] _matLHS The first matrix
 *  \param[in] _matRHS The second matrix
 *  \param[in,out] _matRes The resulting matrix
 */
template<class DataType, template<class MatrixDataType> class MatrixImpl>
	static void gemm(const DistSparseMatrix<DataType, MatrixImpl<DataType> >& _matLHS, const DistSparseMatrix<DataType, MatrixImpl<DataType> >& _matRHS, DistSparseMatrix<DataType, MatrixImpl<DataType> >& _matRes)
{
	typedef DataType data_type;
	const int myRank = ParUtils::getProcRank();

	// Find the list of all non local needed rows
	std::set<int> nonLocalNeededRows;
	const typename DistSparseMatrix<DataType, MatrixImpl<DataType> >::impl_type::graph_type matLHSGraph = _matLHS.getMatrixGraph();
	const int nnz = _matLHS.getNnz();
	for(int i=0;i<nnz;++i)
	{
		if(matLHSGraph.getColPtr(i)<_matLHS.getMapping(0) || matLHSGraph.getColPtr(i)>_matLHS.getMapping(_matLHS.getNbRows()-1))
			nonLocalNeededRows.insert(matLHSGraph.getColPtr(i));
	}

	// Computing the number of messages to send by each processor
	int* localDatasToReceiveFrom = new int[ParUtils::getNumProc()];
	int* scatterPattern = new int[ParUtils::getNumProc()];
	int numberOfMessagesToSend = 0;
	for(int i=0;i<ParUtils::getNumProc();++i)
	{
		localDatasToReceiveFrom[i] = 0;
		scatterPattern[i] = 1;
	}

	const int averageRowsByProc = _matRHS.getGlobalNbRows()/ParUtils::getNumProc();
	typedef std::set<int>::iterator setIterator;
	int numberOfProcsWhichHoldsDatas = 0;
	for(setIterator it = nonLocalNeededRows.begin(); it !=nonLocalNeededRows.end(); ++it)
	{
		int procWhichHoldsDatas = (*it-((*it)%averageRowsByProc))/averageRowsByProc;
		if(procWhichHoldsDatas==ParUtils::getNumProc())
			--procWhichHoldsDatas;
		if(!localDatasToReceiveFrom[procWhichHoldsDatas])
		{
			++localDatasToReceiveFrom[procWhichHoldsDatas];
			++numberOfProcsWhichHoldsDatas;
		}
	}

	YALLA_ERROR_CHECK(ParUtils::reduceScatter(localDatasToReceiveFrom, &numberOfMessagesToSend, scatterPattern, PromoteToMPI<int>::data_type(), MPI_SUM));

	delete[] localDatasToReceiveFrom;
	delete[] scatterPattern;

	// Computing for each processor the following information:
	// Which processor holds the datas -> dataSetNeeded[4*i]
	// Which processor needs the datas -> dataSetNeeded[4*i+1]
	// Which first row is needed on this processor -> dataSetNeeded[4*i+2]
	// Which last row is needed on this processor -> dataSetNeeded[4*i+3]
	int* dataSetNeeded = new int[4*numberOfProcsWhichHoldsDatas];
	for(int i=0;i<numberOfProcsWhichHoldsDatas;++i)
	{
		dataSetNeeded[4*i] = -1;
		dataSetNeeded[4*i+1] = myRank;
		dataSetNeeded[4*i+2] = _matLHS.getGlobalNbCols();
		dataSetNeeded[4*i+3] = 0;
	}

	int counter = 0;
	for(setIterator it = nonLocalNeededRows.begin(); it !=nonLocalNeededRows.end(); ++it)
	{
		int procWhichHoldsDatas = (*it-((*it)%averageRowsByProc))/averageRowsByProc;
		if(procWhichHoldsDatas==ParUtils::getNumProc())
			--procWhichHoldsDatas;
		if(dataSetNeeded[counter]==-1)
			dataSetNeeded[counter] = procWhichHoldsDatas;
		else if(dataSetNeeded[counter]!=procWhichHoldsDatas)
		{
			counter +=4;
			dataSetNeeded[counter] = procWhichHoldsDatas;
		}
		if(dataSetNeeded[counter+2]>*it)
			dataSetNeeded[counter+2] = *it;
		if(dataSetNeeded[counter+3]<*it)
			dataSetNeeded[counter+3] = *it;
	}

	// Send the needed information to the relevant processors
	for(int i=0;i<numberOfProcsWhichHoldsDatas;++i)
		YALLA_ERROR_CHECK(ParUtils::IsendTo(&dataSetNeeded[4*i+1], 3, PromoteToMPI<int>::data_type(), dataSetNeeded[4*i]));

	// Receive all the needed information
	// To which processor I should send the datas -> whichRowSendToWhichProc[3*i]
	// Starting with which row -> whichRowSendToWhichProc[3*i+1]
	// Ending with which row -> whichRowSendToWhichProc[3*i+2]
	int* whichRowSendToWhichProc = new int[3*numberOfMessagesToSend];
	for(int i=0;i<numberOfMessagesToSend;++i)
		YALLA_ERROR_CHECK(ParUtils::IRecvFrom(&whichRowSendToWhichProc[3*i], 3, PromoteToMPI<int>::data_type(), MPI_ANY_SOURCE));

	YALLA_ERROR_CHECK(MPI_Barrier(ParUtils::ThisComm));

	// Packing and sending the datas
	const typename DistSparseMatrix<DataType, MatrixImpl<DataType> >::impl_type::graph_type matRHSGraph = _matRHS.getMatrixGraph();
	const int* rowPtr = matRHSGraph.getRowPtr();
	const int* colPtr = matRHSGraph.getColPtr();
	for(int i=0;i<numberOfMessagesToSend;++i)
	{
		const int numberOfRowsToSend = whichRowSendToWhichProc[3*i+2] - whichRowSendToWhichProc[3*i+1] + 1;
		const int localStartingIndex = whichRowSendToWhichProc[3*i+1] - _matRHS.getMapping(0);
        
		int removedNnz = 0;
		for(int j=0;j<localStartingIndex;++j)
			removedNnz += _matRHS.getRowNnz(j);

		for(int j=localStartingIndex+numberOfRowsToSend;j<_matRHS.getNbRows();++j)
			removedNnz += _matRHS.getRowNnz(j);

		int nbNnz = 0;
		for(int j=localStartingIndex;j<localStartingIndex+numberOfRowsToSend;++j)
			nbNnz += _matRHS.getRowNnz(j);

		const int sizeMessage = 5 + 2*numberOfRowsToSend + 1 + 2*nbNnz;
		data_type* datasToSendToProcI = new data_type[sizeMessage];
		datasToSendToProcI[0] = numberOfRowsToSend;
		datasToSendToProcI[1] = _matRHS.getNbCols();
		datasToSendToProcI[2] = _matRHS.getGlobalNbRows();
		datasToSendToProcI[3] = _matRHS.getGlobalNbCols();
		datasToSendToProcI[4] = nbNnz;

		counter = 5;
		for(int j=localStartingIndex;j<localStartingIndex+numberOfRowsToSend;++j)
		{
			datasToSendToProcI[counter] = _matRHS.getMapping(j);
			++counter;
		}

		datasToSendToProcI[counter] = 0;
		++counter;
		for(int j=localStartingIndex+1;j<=localStartingIndex+numberOfRowsToSend;++j)
		{
			datasToSendToProcI[counter] = datasToSendToProcI[counter-1] + rowPtr[j] - rowPtr[j-1];
			++counter;
		}

		for(int j=localStartingIndex;j<localStartingIndex+numberOfRowsToSend;++j)
		{
			for(int k=rowPtr[j];k<rowPtr[j+1];++k)
			{
				datasToSendToProcI[counter] = colPtr[k];
				datasToSendToProcI[counter+nbNnz] = _matRHS[k];
				++counter;
			}
		}

		YALLA_ERROR_CHECK(ParUtils::SendTo(datasToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), whichRowSendToWhichProc[3*i]));
		delete[] datasToSendToProcI;
	}

	delete[] dataSetNeeded;
	delete[] whichRowSendToWhichProc;

	// Receive needed datas
	data_type** receivedDatas = new data_type*[numberOfProcsWhichHoldsDatas];
	for(int i=0;i<numberOfProcsWhichHoldsDatas;++i)
	{
		int count = 0;
		MPI_Status status;
		YALLA_ERROR_CHECK(ParUtils::ProbeFrom(MPI_ANY_SOURCE,&status));
		const int source = status.MPI_SOURCE;
		YALLA_ERROR_CHECK(ParUtils::getCount(&status, PromoteToMPI<data_type>::data_type(), &count));

		receivedDatas[i] = new data_type[count];

		YALLA_ERROR_CHECK(ParUtils::RecvFrom(receivedDatas[i],count,PromoteToMPI<data_type>::data_type(),source));
	}

	// Symbolic factorization
	const int lNbRows = _matLHS.getNbRows();
	const int rNbCols = _matRHS.getNbCols();
	const int rNbRows = _matRHS.getNbRows();
	std::set<std::pair<int,int> > countedEntries;
	// Local nnz elements
	for(int i=0;i<lNbRows;++i)
	{
		for(int j=0;j<rNbCols;++j)
		{
			for(int k=0;k<rNbRows;++k)
			{
				if(_matLHS(i,_matLHS.getMapping(k)) * _matRHS(k,j))
				{
					countedEntries.insert(std::make_pair(i,j));
					break;
				}
			}
		}
	}
      
	for(int l=0;l<numberOfProcsWhichHoldsDatas;++l)
	{
		const int nbReceivedDatasRows = static_cast<int>(receivedDatas[l][0]);
		// Global nnz elements
		for(int k=0;k<lNbRows;++k)
		{
			for(int i=0;i<nbReceivedDatasRows;++i)
			{
				for(int j=static_cast<int>(receivedDatas[l][5+nbReceivedDatasRows+i]);j<static_cast<int>(receivedDatas[l][5+nbReceivedDatasRows+i+1]);++j)
				{
					if(_matLHS(k,static_cast<int>(receivedDatas[l][5+i])))
						countedEntries.insert(std::make_pair(k,static_cast<int>(receivedDatas[l][5+nbReceivedDatasRows+nbReceivedDatasRows+1+j])));
				}
			}
		}
	}

	// nnz == countedEntries.size()
	const int resultingNnz = countedEntries.size();
	int* resultingRowPtr = new int[lNbRows+1];
	int* resultingColPtr = new int[resultingNnz];

	for(int i=0;i<lNbRows+1;++i)
		resultingRowPtr[i] = 0;

	std::set<std::pair<int,int> >::iterator countedEntriesIterator;
	counter = 0;
	for(countedEntriesIterator = countedEntries.begin(); countedEntriesIterator != countedEntries.end();++countedEntriesIterator)
	{
		++resultingRowPtr[countedEntriesIterator->first+1];
		resultingColPtr[counter] = countedEntriesIterator->second;
		++counter;
	}

	for(int i=0;i<lNbRows;++i)
		resultingRowPtr[i+1] += resultingRowPtr[i];

	_matRes = DistSparseMatrix<DataType, MatrixImpl<DataType> >(lNbRows,rNbCols,resultingNnz,_matLHS.getGlobalNbRows(), _matRHS.getGlobalNbCols(), _matLHS.getMapping(), resultingRowPtr, resultingColPtr);

	// Numeric factorization
	// Local computations
	for(int i=0;i<lNbRows;++i)
	{
		for(int j=0;j<rNbCols;++j)
		{
			for(int k=0;k<rNbRows;++k)
			{
				_matRes(i,j) += _matLHS(i,_matLHS.getMapping(k)) * _matRHS(k,j);
			}
		}
	}

	// Global computations
	for(int l=0;l<numberOfProcsWhichHoldsDatas;++l)
	{
		const int nbReceivedDatasRows = static_cast<int>(receivedDatas[l][0]);
		for(int k=0;k<lNbRows;++k)
		{
			for(int i=0;i<nbReceivedDatasRows;++i)
			{
				for(int j=static_cast<int>(receivedDatas[l][5+nbReceivedDatasRows+i]);j<static_cast<int>(receivedDatas[l][5+nbReceivedDatasRows+i+1]);++j)
				{
					_matRes(k,static_cast<int>(receivedDatas[l][5+nbReceivedDatasRows+nbReceivedDatasRows+1+j])) += _matLHS(k,static_cast<int>(receivedDatas[l][5+i])) * receivedDatas[l][5+nbReceivedDatasRows+nbReceivedDatasRows+1+static_cast<int>(receivedDatas[l][4])+j];
				}
			}
		}
	}

	for(int i=0;i<numberOfProcsWhichHoldsDatas;++i)
		delete[] receivedDatas[i];
	delete[] receivedDatas;
}

#ifdef WITH_ATLAS
/*!
 *  \brief sgemm for distributed dense matrices with BLAS
 *
 *  \details
 *  Compute C += A * B
 *
 *  \param[in] _matLHS The first matrix
 *  \param[in] _matRHS The second matrix
 *  \param[in,out] _matRes The resulting matrix
 */
template<template<class MatrixDataType> class MatrixImpl>
static void gemm(const DistDenseMatrix<float, MatrixImpl<float> > & _A, const DistDenseMatrix<float, MatrixImpl<float> >& _B, DistDenseMatrix<float, MatrixImpl<float> >& _C)
{
	const int numProc = ParUtils::getNumProc();
	const int procRank = ParUtils::getProcRank();
	typedef float data_type;
	// Local computations
	const int lNbRows = _A.getNbRows();
	const int rNbCols = _B.getNbCols();
	const int lNbCols = _A.getNbCols();
	// Need to un-const _B because we need to use the non-const operator() to pass to BLAS
	DistDenseMatrix<float, MatrixImpl<float> >& nonConstB = const_cast<DistDenseMatrix<float, MatrixImpl<float> >&>(_B);
	cblas_sgemm(PromoteToBLASOrder<SeqDenseMatrix<data_type, MatrixImpl<data_type> > >::data_type(), CblasNoTrans, CblasNoTrans, lNbRows, rNbCols,lNbCols, 1, _A.getPointeur(), lNbCols, &nonConstB(nonConstB.getMapping(0),0), rNbCols, 1, &_C(0,0), rNbCols);

	// Global computations
	// Packing local matrix A
	int sizeMessage = 4 + lNbCols + lNbRows*lNbCols;
	data_type* dataToSendToProcI = new data_type[sizeMessage];
	dataToSendToProcI[0] = lNbRows;
	dataToSendToProcI[1] = lNbCols;
	dataToSendToProcI[2] = _A.getGlobalNbRows();
	dataToSendToProcI[3] = _A.getGlobalNbCols();

	int counter = 4;
	for(int i=0;i<lNbCols;++i)
	{
		dataToSendToProcI[counter] = _A.getMapping(i);
		++counter;
	}

	for(int i=0;i<lNbRows;++i)
	{
		for(int j=0;j<lNbCols;++j)
		{
			dataToSendToProcI[counter] = _A(i,j);
			++counter;
		}
	}

	for(int i=0;i<numProc-1;++i)
	{
		YALLA_ERROR_CHECK(ParUtils::IsendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), Math::modulo((procRank+1),numProc)));
				
		MPI_Status msg_status;
		YALLA_ERROR_CHECK(ParUtils::ProbeFrom(Math::modulo((procRank-1),numProc),&msg_status));
		YALLA_ERROR_CHECK(ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &sizeMessage));
		data_type* dataToReceiveFromProcI = new data_type[sizeMessage];
				
		YALLA_ERROR_CHECK(ParUtils::RecvFrom(dataToReceiveFromProcI,sizeMessage,PromoteToMPI<data_type>::data_type(),Math::modulo((procRank-1),numProc))); // data now contains A from proc-1
		// Global computations
		const int receivedNbCols = static_cast<int>(dataToReceiveFromProcI[1]);
				
		cblas_sgemm(PromoteToBLASOrder<SeqDenseMatrix<data_type, MatrixImpl<data_type> > >::data_type(), CblasNoTrans, CblasNoTrans, lNbRows, rNbCols, receivedNbCols, 1, &dataToReceiveFromProcI[4+receivedNbCols], receivedNbCols, &nonConstB(static_cast<int>(dataToReceiveFromProcI[4]),0), rNbCols, 1, &_C(0,0), rNbCols);
				
		delete[] dataToSendToProcI;
		dataToSendToProcI = dataToReceiveFromProcI;
	}
	delete[] dataToSendToProcI;
}

/*!
 *  \brief dgemm for distributed dense matrices with BLAS
 *
 *  \details
 *  Compute C += A * B
 *
 *  \param[in] _matLHS The first matrix
 *  \param[in] _matRHS The second matrix
 *  \param[in,out] _matRes The resulting matrix
 */
template<template<class MatrixDataType> class MatrixImpl>
static void gemm(const DistDenseMatrix<double, MatrixImpl<double> > & _A, const DistDenseMatrix<double, MatrixImpl<double> >& _B, DistDenseMatrix<double, MatrixImpl<double> >& _C)
{
	const int numProc = ParUtils::getNumProc();
	const int procRank = ParUtils::getProcRank();
	typedef double data_type;
	// Local computations
	const int lNbRows = _A.getNbRows();
	const int rNbCols = _B.getNbCols();
	const int lNbCols = _A.getNbCols();
	// Need to un-const _B because we need to use the non-const operator() to pass to BLAS
	DistDenseMatrix<double, MatrixImpl<double> >& nonConstB = const_cast<DistDenseMatrix<double, MatrixImpl<double> >&>(_B);
	cblas_dgemm(PromoteToBLASOrder<SeqDenseMatrix<data_type, MatrixImpl<data_type> > >::data_type(), CblasNoTrans, CblasNoTrans, lNbRows, rNbCols,lNbCols, 1, _A.getPointeur(), lNbCols, &nonConstB(nonConstB.getMapping(0),0), rNbCols, 1, &_C(0,0), rNbCols);

	// Global computations
	// Packing local matrix A
	int sizeMessage = 4 + lNbCols + lNbRows*lNbCols;
	data_type* dataToSendToProcI = new data_type[sizeMessage];
	dataToSendToProcI[0] = lNbRows;
	dataToSendToProcI[1] = lNbCols;
	dataToSendToProcI[2] = _A.getGlobalNbRows();
	dataToSendToProcI[3] = _A.getGlobalNbCols();

	int counter = 4;
	for(int i=0;i<lNbCols;++i)
	{
		dataToSendToProcI[counter] = _A.getMapping(i);
		++counter;
	}

	for(int i=0;i<lNbRows;++i)
	{
		for(int j=0;j<lNbCols;++j)
		{
			dataToSendToProcI[counter] = _A(i,j);
			++counter;
		}
	}

	for(int i=0;i<numProc-1;++i)
	{
		YALLA_ERROR_CHECK(ParUtils::IsendTo(dataToSendToProcI, sizeMessage, PromoteToMPI<data_type>::data_type(), Math::modulo((procRank+1),numProc)));
				
		MPI_Status msg_status;
		YALLA_ERROR_CHECK(ParUtils::ProbeFrom(Math::modulo((procRank-1),numProc),&msg_status));
		YALLA_ERROR_CHECK(ParUtils::getCount(&msg_status, PromoteToMPI<data_type>::data_type(), &sizeMessage));
		data_type* dataToReceiveFromProcI = new data_type[sizeMessage];
				
		YALLA_ERROR_CHECK(ParUtils::RecvFrom(dataToReceiveFromProcI,sizeMessage,PromoteToMPI<data_type>::data_type(),Math::modulo((procRank-1),numProc))); // data now contains A from proc-1
		// Global computations
		const int receivedNbCols = static_cast<int>(dataToReceiveFromProcI[1]);
				
		cblas_dgemm(PromoteToBLASOrder<SeqDenseMatrix<data_type, MatrixImpl<data_type> > >::data_type(), CblasNoTrans, CblasNoTrans, lNbRows, rNbCols, receivedNbCols, 1, &dataToReceiveFromProcI[4+receivedNbCols], receivedNbCols, &nonConstB(static_cast<int>(dataToReceiveFromProcI[4]),0), rNbCols, 1, &_C(0,0), rNbCols);
				
		delete[] dataToSendToProcI;
		dataToSendToProcI = dataToReceiveFromProcI;
	}
	delete[] dataToSendToProcI;
}
#endif // WITH_ATLAS

#endif // WITH_MPI

#endif
