#pragma once
#ifndef yalla_blas_h
#define yalla_blas_h

/*!
 *  \file BLAS.h
 *  \brief Basic Linear Algebra Subroutines
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <cmath>
#include <set>
#include <map>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToMPI.h"
#include "yalla/Utils/ParUtils/ParUtils.h"
#include "yalla/Utils/Math/Math.h"
#include "yalla/Utils/Algorithms/DoubleQuickSort.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMKLMatrix.h"
#ifdef WITH_MPI
#include "yalla/BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMKLMatrix.h"
#include "mpi.h"
#endif
#ifdef WITH_ATLAS
#include "yalla/Utils/TypeTraits/PromoteToBLASOrder.h"
#ifdef __cplusplus
extern "C"
{
#endif
#include "atlas/cblas.h"
#ifdef __cplusplus
}
#endif
#endif
#ifdef WITH_MKL
#include "yalla/Utils/TypeTraits/PromoteToBLASOrder.h"
#include "mkl.h"
#endif

#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#ifdef WITH_MPI
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"
#endif

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
#include "Epetra_Vector.h"
#include "BasicTypes/Vector/Dist/EpetraVector.h"
#include "Epetra_CrsMatrix.h"
#include "BasicTypes/Matrix/Implementation/Dist/EpetraMatrix.h"
#include "BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#endif

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace BLAS
 *  \brief Basic Linear Algebra Subroutines
 *
 *  Define some classic BLAS operations and convenient BLAS based operations.h
 */
class BLAS
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef BLAS this_class;
	// BLAS1 operations
#include "yalla/Utils/BLAS/BLAS1.h"
	// BLAS2 operations
#include "yalla/Utils/BLAS/BLAS2.h"
	// BLAS3 operations
#include "yalla/Utils/BLAS/BLAS3.h"
	// BLASLike operations
#include "yalla/Utils/BLAS/BLASLike.h"
};
YALLA_END_NAMESPACE
#endif
