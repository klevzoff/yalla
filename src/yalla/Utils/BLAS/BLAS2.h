#pragma once
#ifndef yalla_blas_2_h
#define yalla_blas_2_h

/*!
 *  \file BLAS2.h
 *  \brief BLAS level 2 operations
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

/*!
 *  \brief Sequential gemv
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixType The type of the matrix
 *
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
template<class DataType, class MatrixType>
	static void gemv(const DataType _alpha, const MatrixType& _A, const SeqVector<DataType>& _x, const DataType _beta, SeqVector<DataType>& _y)
{
#ifdef WITH_MKL
	YALLA_LOG("Not BLAS GEMV");
#endif
	typedef typename MatrixType::impl_type::const_row_iterator_type rowIt;
	typedef typename MatrixType::impl_type::const_col_iterator_type colIt;

	for(rowIt row=_A.rowBegin(); row != _A.rowEnd();++row)
	{
		const int rowIndex = row.index();
		_y(rowIndex) *= _beta;
		for(colIt col=_A.colBegin(row);col!=_A.colEnd(row);++col)
			_y(rowIndex) += _alpha * (*col) * _x(col.index());
	}
}

#ifdef WITH_MKL
/*!
 *  \brief Sequential sgemv for CSR matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
static void gemv(const float _alpha, const SeqSparseMatrix<float, SeqCSRMatrix<float> >& _A, const SeqVector<float>& _x, const float _beta, SeqVector<float>& _y)
{
	int nbRows = _A.getNbRows();
	int nbCols = _A.getNbCols();
	float* val = const_cast<float*>(_A.getPointeur());
	char trans = 'N';
	const SeqCSRMatrix<float>::graph_type& aGraph = _A.getMatrixGraph();
	int* indx = const_cast<int*>(aGraph.getColPtr());
	float* x = const_cast<float*>(_x.getPointeur());
	int* pointerB =  const_cast<int*>(aGraph.getRowPtr());
	int* pointerE =  const_cast<int*>(aGraph.getRowPtr()+1);
	float* alpha = const_cast<float*>(&_alpha);
	float* beta = const_cast<float*>(&_beta);
	char matdescra[6];
	matdescra[0] = 'G'; // General
	matdescra[1] = 'L'; // Ignored
	matdescra[2] = 'N'; // Ignored
	matdescra[3] = 'C'; // 0 based
	matdescra[4] = 'D'; // Dummy
	matdescra[5] = 'D'; // Dummy
	
	mkl_scsrmv( &trans, &nbRows, &nbCols, alpha, matdescra, val, indx, pointerB, pointerE, x, beta, &_y(0) );
}

/*!
 *  \brief Sequential sgemv for CSRMKL matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
static void gemv(const float _alpha, const SeqSparseMatrix<float, SeqCSRMKLMatrix<float> >& _A, const SeqVector<float>& _x, const float _beta, SeqVector<float>& _y)
{
	int nbRows = _A.getNbRows();
	int nbCols = _A.getNbCols();
	float* val = const_cast<float*>(_A.getPointeur());
	char trans = 'N';
	const SeqCSRMKLMatrix<float>::graph_type& aGraph = _A.getMatrixGraph();
	int* indx = const_cast<int*>(aGraph.getColPtr());
	float* x = const_cast<float*>(_x.getPointeur());
	int* pointerB =  const_cast<int*>(aGraph.getRowPtr());
	int* pointerE =  const_cast<int*>(aGraph.getRowPtr()+1);
	float* alpha = const_cast<float*>(&_alpha);
	float* beta = const_cast<float*>(&_beta);
	char matdescra[6];
	matdescra[0] = 'G'; // General
	matdescra[1] = 'L'; // Ignored
	matdescra[2] = 'N'; // Ignored
	matdescra[3] = 'C'; // 0 based
	matdescra[4] = 'D'; // Dummy
	matdescra[5] = 'D'; // Dummy
	
	mkl_scsrmv( &trans, &nbRows, &nbCols, alpha, matdescra, val, indx, pointerB, pointerE, x, beta, &_y(0) );
}

/*!
 *  \brief Sequential dgemv for CSR matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
static void gemv(const double _alpha, const SeqSparseMatrix<double, SeqCSRMatrix<double> >& _A, const SeqVector<double>& _x, const double _beta, SeqVector<double>& _y)
{
	int nbRows = _A.getNbRows();
	int nbCols = _A.getNbCols();
	double* val = const_cast<double*>(_A.getPointeur());
	char trans = 'N';
	const SeqCSRMatrix<double>::graph_type& aGraph = _A.getMatrixGraph();
	int* indx = const_cast<int*>(aGraph.getColPtr());
	double* x = const_cast<double*>(_x.getPointeur());
	int* pointerB = const_cast<int*>(aGraph.getRowPtr());
	int* pointerE = const_cast<int*>(aGraph.getRowPtr()+1);
	double* alpha = const_cast<double*>(&_alpha);
	double* beta = const_cast<double*>(&_beta);
	char matdescra[6];
	matdescra[0] = 'G'; // General
	matdescra[1] = 'L'; // Ignored
	matdescra[2] = 'N'; // Ignored
	matdescra[3] = 'C'; // 0 based
	matdescra[4] = 'D'; // Dummy
	matdescra[5] = 'D'; // Dummy
	
	mkl_dcsrmv( &trans, &nbRows, &nbCols, alpha, matdescra, val, indx, pointerB, pointerE, x, beta, &_y(0) );
}

/*!
 *  \brief Sequential dgemv for CSRMKL matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
static void gemv(const double _alpha, const SeqSparseMatrix<double, SeqCSRMKLMatrix<double> >& _A, const SeqVector<double>& _x, const double _beta, SeqVector<double>& _y)
{
	int nbRows = _A.getNbRows();
	int nbCols = _A.getNbCols();
	double* val = const_cast<double*>(_A.getPointeur());
	char trans = 'N';
	const SeqCSRMKLMatrix<double>::graph_type& aGraph = _A.getMatrixGraph();
	int* indx = const_cast<int*>(aGraph.getColPtr());
	double* x = const_cast<double*>(_x.getPointeur());
	int* pointerB =  const_cast<int*>(aGraph.getRowPtr());
	int* pointerE =  const_cast<int*>(aGraph.getRowPtr()+1);
	double* alpha = const_cast<double*>(&_alpha);
	double* beta = const_cast<double*>(&_beta);
	char matdescra[6];
	matdescra[0] = 'G'; // General
	matdescra[1] = 'L'; // Ignored
	matdescra[2] = 'N'; // Ignored
	matdescra[3] = 'C'; // 0 based
	matdescra[4] = 'D'; // Dummy
	matdescra[5] = 'D'; // Dummy
	
	mkl_dcsrmv( &trans, &nbRows, &nbCols, alpha, matdescra, val, indx, pointerB, pointerE, x, beta, &_y(0) );
}
#endif

#if defined (WITH_ATLAS) || defined (_WITH_MKL)
/*!
 *  \brief Sequential sgemv for dense matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \tparam MatrixImpl The actual matrix implementation
 *
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
template<template<class MatrixDataType> class MatrixImpl>
static void gemv(const float _alpha, const SeqDenseMatrix<float, MatrixImpl<float> >& _A, const SeqVector<float>& _x, const float _beta, SeqVector<float>& _y)
{
	cblas_sgemv( PromoteToBLASOrder<SeqDenseMatrix<float, MatrixImpl<float> > >::data_type(), CblasNoTrans, _A.getNbRows(), _A.getNbCols(), _alpha, _A.getPointeur(), _A.getNbCols(), _x.getPointeur(), 1, _beta, &_y(0), 1);
}

/*!
 *  \brief Sequential dgemv for dense matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \tparam MatrixImpl The actual matrix implementation
 *
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
template<template<class MatrixDataType> class MatrixImpl>
static void gemv(const double _alpha, const SeqDenseMatrix<double, MatrixImpl<double> >& _A, const SeqVector<double>& _x, const double _beta, SeqVector<double>& _y)
{
	cblas_dgemv( PromoteToBLASOrder<SeqDenseMatrix<double, MatrixImpl<double> > >::data_type(), CblasNoTrans, _A.getNbRows(), _A.getNbCols(), _alpha, _A.getPointeur(), _A.getNbCols(), _x.getPointeur(), 1, _beta, &_y(0), 1);
}
#endif // WITH_ATLAS

#ifdef WITH_MPI
/*!
 *  \brief Distributed gemv for dense matrices
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixImpl The actual matrix implementation
 *  \tparam VectorType The type of the vector
 *
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
template<class DataType, template<class MatrixDataType> class MatrixImpl, template<class VectorDataType> class VectorType>
	static void gemv(const DataType _alpha, const DistDenseMatrix<DataType, MatrixImpl<DataType> >& _A, const VectorType<DataType>& _x, const DataType _beta, VectorType<DataType>& _y)
{
	const int nbRows = _A.getNbRows();
	const int size = _x.getSize();
	const int nbPartitions = ParUtils::getNumProc();
	DataType* tmpVal = new DataType[nbRows];

	// Local computations
	for(int i=0;i<nbRows;++i)
	{
		tmpVal[i] = 0;
		for(int j=0;j<size;++j)
		{
			tmpVal[i] += _alpha * _A(i,j) * _x(j);
		}
	}

	// Global computations
	int* partition = new int[nbPartitions];

	const int globalCols =  _A.getGlobalNbCols();
	int averageColsByThread = globalCols/ParUtils::getNumProc();
	for(int i=0;i<nbPartitions-1;++i)
		partition[i] = averageColsByThread;

	partition[nbPartitions-1] = globalCols-averageColsByThread*(nbPartitions-1);

	ParUtils::reduceScatter(tmpVal, &_y(0), partition, PromoteToMPI<DataType>::data_type(),MPI_SUM);

	for(int i=0;i<nbRows;++i)
		_y[i] *= _beta;

	delete[] partition;
	delete[] tmpVal;
}

/*!
 *  \brief Distributed gemv for sparse matrices
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \tparam DataType The data type of the matrix/vectors
 *  \tparam MatrixImpl The actual matrix implementation
 *  \tparam VectorType The type of the vector
 *
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
template<class DataType, template<class MatrixDataType> class MatrixImpl, template<class VectorDataType> class VectorType>
	static void gemv(const DataType _alpha, const DistSparseMatrix<DataType, MatrixImpl<DataType> >& _A, const VectorType<DataType>& _x, const DataType _beta, VectorType<DataType>& _y)
{
	typedef typename MatrixImpl<DataType>::const_loc_row_iterator_type locRowIt;
	typedef typename MatrixImpl<DataType>::const_loc_col_iterator_type locColIt;
	typedef typename MatrixImpl<DataType>::const_glob_row_iterator_type globRowIt;
	typedef typename MatrixImpl<DataType>::const_glob_col_iterator_type globColIt;

	_x.synchronize();

	for(locRowIt row=_A.ownRowBegin(); row != _A.ownRowEnd();++row)
	{
		const int rowIndex = row.index();
		for(locColIt col=_A.ownColBegin(row);col!=_A.ownColEnd(row);++col)
		{
			const int colIndex = col.index();
			_y(rowIndex) *= _beta;
			_y(rowIndex) += _alpha * (*col) * _x(colIndex);
		}
	}

	_x.getSync()->isSynchronized();

	for(globRowIt row=_A.rowBegin(); row != _A.rowEnd();++row)
	{
		const int rowIndex = row.index();
		for(globColIt col=_A.colBegin(row);col!=_A.colEnd(row);++col)
		{
			const int colIndex = col.index();
			_y(rowIndex) += _alpha * (*col) * _x(colIndex);
		}
	}
}

#ifdef WITH_MKL
/*!
 *  \brief Distributed sgemv for CSR matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
static void gemv(const float _alpha, const DistSparseMatrix<float, DistCSRMKLMatrix<float> >& _A, const DistVector<float>& _x, const float _beta, DistVector<float>& _y)
{
	_x.synchronize();

	const DistCSRMKLMatrix<float>* impl = _A.getImpl();

	int diagNbRows = impl->getDiagNbRows();
	int diagNbCols = impl->getDiagNbCols();
	int ndiagNbRows = impl->getNDiagNbRows();
	int ndiagNbCols = impl->getNDiagNbCols();

	float* diagVal = const_cast<float*>(impl->getDiagPointeur());
	float* ndiagVal = const_cast<float*>(impl->getNDiagPointeur());

	char trans = 'N';
	int* diagIndx = const_cast<int*>(impl->getDiagColPtr());
	int* ndiagIndx = const_cast<int*>(impl->getNDiagColPtr());

	float* x = const_cast<float*>(_x.getPointeur());
	float* tmpY = new float[ndiagNbRows];
	for(int i=0;i<ndiagNbRows;++i)
		tmpY[i] = 0;

	int* diagPointerB = const_cast<int*>(impl->getDiagRowPtr());
	int* diagPointerE = const_cast<int*>(impl->getDiagRowPtr()+1);
	int* ndiagPointerB = const_cast<int*>(impl->getNDiagRowPtr());
	int* ndiagPointerE = const_cast<int*>(impl->getNDiagRowPtr()+1);

	float* alpha = const_cast<float*>(&_alpha);
	float* beta = const_cast<float*>(&_beta);
	float one = 1.;

	char matdescra[6];
	matdescra[0] = 'G'; // General
	matdescra[1] = 'L'; // Ignored
	matdescra[2] = 'N'; // Ignored
	matdescra[3] = 'C'; // 0 based
	matdescra[4] = 'D'; // Dummy
	matdescra[5] = 'D'; // Dummy

	mkl_scsrmv( &trans, &diagNbRows, &diagNbCols, alpha, matdescra, diagVal, diagIndx, diagPointerB, diagPointerE, x, beta, &_y(0) );

	_x.getSync()->isSynchronized();

	mkl_scsrmv( &trans, &ndiagNbRows, &ndiagNbCols, alpha, matdescra, ndiagVal, ndiagIndx, ndiagPointerB, ndiagPointerE, x, &one, &tmpY[0] );
	
	for(int i=0;i<ndiagNbRows;++i)
		_y[impl->getNDiagMapping(i)] += tmpY[i];
	
	delete[] tmpY;
}

/*!
 *  \brief Distributed dgemv for CSR matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
static void gemv(const double _alpha, const DistSparseMatrix<double, DistCSRMKLMatrix<double> >& _A, const DistVector<double>& _x, const double _beta, DistVector<double>& _y)
{
	_x.synchronize();
	
	const DistCSRMKLMatrix<double>* impl = _A.getImpl();

	int diagNbRows = impl->getDiagNbRows();
	int diagNbCols = impl->getDiagNbCols();
	int ndiagNbRows = impl->getNDiagNbRows();
	int ndiagNbCols = impl->getNDiagNbCols();

	double* diagVal = const_cast<double*>(impl->getDiagPointeur());
	double* ndiagVal = const_cast<double*>(impl->getNDiagPointeur());

	char trans = 'N';
	int* diagIndx = const_cast<int*>(impl->getDiagColPtr());
	int* ndiagIndx = const_cast<int*>(impl->getNDiagColPtr());

	double* x = const_cast<double*>(_x.getPointeur());
	double* tmpY = new double[ndiagNbRows];
	for(int i=0;i<ndiagNbRows;++i)
		tmpY[i] = 0;

	const int* ndiagMapping = impl->getNDiagMapping();
	int* diagPointerB = const_cast<int*>(impl->getDiagRowPtr());
	int* diagPointerE = const_cast<int*>(impl->getDiagRowPtr()+1);
	int* ndiagPointerB = const_cast<int*>(impl->getNDiagRowPtr());
	int* ndiagPointerE = const_cast<int*>(impl->getNDiagRowPtr()+1);

	double* alpha = const_cast<double*>(&_alpha);
	double* beta = const_cast<double*>(&_beta);
	double one = 1.;

	char matdescra[6];
	matdescra[0] = 'G'; // General
	matdescra[1] = 'L'; // Ignored
	matdescra[2] = 'N'; // Ignored
	matdescra[3] = 'C'; // 0 based
	matdescra[4] = 'D'; // Dummy
	matdescra[5] = 'D'; // Dummy

	mkl_dcsrmv( &trans, &diagNbRows, &diagNbCols, alpha, matdescra, diagVal, diagIndx, diagPointerB, diagPointerE, x, beta, &_y(0) );

	_x.getSync()->isSynchronized();

	mkl_dcsrmv( &trans, &ndiagNbRows, &ndiagNbCols, alpha, matdescra, ndiagVal, ndiagIndx, ndiagPointerB, ndiagPointerE, x, &one, &tmpY[0] );

	for(int i=0;i<ndiagNbRows;++i)
		_y[ndiagMapping[i]] += tmpY[i];

	delete[] tmpY;
}
#endif

#if defined (WITH_TRILINOS) && defined (_WITH_TRILINOS_BLAS)
/*!
 *  \brief gemv for EpetraMatrix
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
static void gemv(const DistSparseMatrix<double,EpetraMatrix<Epetra_CrsMatrix> >& _A, const EpetraVector<Epetra_Vector>& _x, EpetraVector<Epetra_Vector>& _y)
{
	_A.getImpl()->getMatrix()->Multiply(false, *_x.getVector(), *_y.getVector());
}
#endif

#ifdef WITH_ATLAS
/*!
 *  \brief Distributed sgemv for dense matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \tparam MatrixImpl The actual type of the matrix
 *  \tparam VectorType The type of the vector
 *
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
template<template<class MatrixDataType> class MatrixImpl, template<class VectorDataType> class VectorType>
	static void gemv(const float _alpha, const DistDenseMatrix<float, MatrixImpl<float> >& _A, const VectorType<float>& _x, const float _beta, VectorType<float>& _y)
{
	typedef float data_type;
	const int nbRows = _A.getNbRows();
	const int size = _x.getSize();
	const int nbPartitions = ParUtils::getNumProc();
	data_type* tmpVal = new data_type[nbRows];

	cblas_sscal(nbRows,0,tmpVal,1);
	cblas_sgemv( PromoteToBLASOrder<DistDenseMatrix<data_type, MatrixImpl<data_type> > >::data_type(), CblasNoTrans, nbRows, size, _alpha, _A.getPointeur(), size, _x.getPointeur(), 1, 1, tmpVal, 1);

	int* partition = new int[nbPartitions];
	const int globalCols =  _A.getGlobalNbCols();
	int averageColsByThread = globalCols/ParUtils::getNumProc();
	for(int i=0;i<nbPartitions-1;++i)
		partition[i] = averageColsByThread;

	partition[nbPartitions-1] = globalCols-averageColsByThread*(nbPartitions-1);

	ParUtils::reduceScatter(tmpVal, &_y(0), partition, PromoteToMPI<data_type>::data_type(),MPI_SUM);

	for(int i=0;i<nbRows;++i)
		_y[i] *= _beta;

	delete[] partition;
	delete[] tmpVal;
}

/*!
 *  \brief Distributed dgemv for dense matrices with BLAS
 *
 *  \details
 *  Compute beta * y += alpha * A * x
 *  
 *  \tparam MatrixImpl The actual type of the matrix
 *  \tparam VectorType The type of the vector
 *
 *  \param[in] _alpha The first scaling factor
 *  \param[in] _A The matrix
 *  \param[in] _x The input vector
 *  \param[in] _beta The second scaling factor
 *  \param[in,out] _y The output vector
 */
template<template<class MatrixDataType> class MatrixImpl, template<class VectorDataType> class VectorType>
	static void gemv(const double _alpha, const DistDenseMatrix<double, MatrixImpl<double> >& _A, const VectorType<double>& _x, const double _beta, VectorType<double>& _y)
{
	typedef double data_type;
	const int nbRows = _A.getNbRows();
	const int size = _x.getSize();
	const int nbPartitions = ParUtils::getNumProc();
	data_type* tmpVal = new data_type[nbRows];

	cblas_dscal(nbRows,0,tmpVal,1);
	cblas_dgemv( PromoteToBLASOrder<DistDenseMatrix<data_type, MatrixImpl<data_type> > >::data_type(), CblasNoTrans, nbRows, size, _alpha, _A.getPointeur(), size, _x.getPointeur(), 1, 1, tmpVal, 1);

	int* partition = new int[nbPartitions];
	const int globalCols =  _A.getGlobalNbCols();
	int averageColsByThread = globalCols/ParUtils::getNumProc();
	for(int i=0;i<nbPartitions-1;++i)
		partition[i] = averageColsByThread;

	partition[nbPartitions-1] = globalCols-averageColsByThread*(nbPartitions-1);

	ParUtils::reduceScatter(tmpVal, &_y(0), partition, PromoteToMPI<data_type>::data_type(),MPI_SUM);

	for(int i=0;i<nbRows;++i)
		_y[i] *= _beta;

	delete[] partition;
	delete[] tmpVal;
}
#endif // WITH_ATLAS

#endif // WITH_MPI

#endif
