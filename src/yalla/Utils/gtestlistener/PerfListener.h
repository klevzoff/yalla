#pragma once
#ifndef yalla_perf_listener_h
#define yalla_perf_listener_h

/*!
 *  \file PerfListener.h
 *  \brief Extension of the gtest listener for performance benchmark
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#ifdef WITH_GTEST

#include <iostream>
#include <fstream>
#if WITH_CPP11
#include <chrono>
#else
#include "yalla/Utils/Profiler/Profiler.h"
#endif
#include <string>
#include <limits>
#include <sstream>
#include <ctime>
#include <map>
#include <vector>

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/IO/console/log.h"
#include "gtest/gtest.h"
#include "yalla/Utils/ParUtils/ParUtils.h"

// Fix for Windows
#ifdef max
#undef max
#endif

using ::testing::EmptyTestEventListener;
using ::testing::InitGoogleTest;
using ::testing::Test;
using ::testing::TestCase;
using ::testing::TestEventListeners;
using ::testing::TestInfo;
using ::testing::TestPartResult;
using ::testing::UnitTest;

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class PerfTestFramework
 *  \brief Performance benchmark logger
 *
 *  The google test framework is mandatory for this class to be used. \n
 *  This class logs various execution statistics on the execution of benchmark tests.
 */
class PerfTestFramework
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef PerfTestFramework this_class;
	//@}
 private:
	/*!
	 *  \class PerfListener
	 *  \brief Extension of the gtest listener for performance benchmark
	 *
	 *  The google test framework is mandatory for this class to be used. \n
	 *  This class logs various execution statistics on the execution of benchmark tests. \n
	 *  All the read/write methods are terrible. It might be usefull to plug a xml reader/writer library to make those simpler.
	 */
	class PerfListener
		: public EmptyTestEventListener
	{
	public:
		//! \name Public Typedefs
		//@{
		//! \brief Type of this class
		typedef PerfListener this_class;
		//! \brief Type of the super class
		typedef EmptyTestEventListener super_class;
		//@}
	private:
		/*!
		 *  \class PerfLogger
		 *  \brief Structure to store the statistics execution
		 */
		struct PerfLogger
		{
			//! \brief Execution time of the current test
			std::vector<double> m_test_time;
			//! \brief Test status (success, stall or fail)
			std::vector<std::string> m_test_status;
			//! \brief Date of the test
			std::vector<std::string> m_test_date;
			//! \brief Message describing the test
			std::vector<std::string> m_test_message;
			//! \brief Best recorded time for this test
			double m_best_time;
			//! \brief Index of the best recorded test
			int m_best_test;
			//! \brief Date of the best recorded test
			std::string m_best_test_date;
			//! \brief Name of the test
			std::string m_test_name;
#ifdef WITH_MPI
			//! \brief Number of procs
			int m_num_proc;
			//! \brief Min time over all procs
			std::vector<double> m_min_time;
			//! \brief Average time
			std::vector<double> m_average_time;
			//! \brief Time for this proc
			std::vector<int> m_time_proc;
			//! \brief Proc with the min time
			std::vector<int> m_min_time_proc;
#endif
		};
#if WITH_CPP11
		//! \brief Start chrono
		std::chrono::time_point<std::chrono::high_resolution_clock> m_start;
		//! \brief Stop chrono
		std::chrono::time_point<std::chrono::high_resolution_clock> m_end;
#else
    Profiler profiler;
#endif
		//! \brief File to log the test results
		std::string m_test_file;
		//! \brief Directory of the log file 
		std::string m_base_dir;
		//! \brief Number of improved tests 
		int m_number_improved_perfs;
		//! \brief Number of stalled tests 
		int m_number_stalling_perfs;
		//! \brief Number of worst tests 
		int m_number_worst_perfs;
		//! \brief Tolerance to define a stall test
		double m_tolerance;
		//! \brief Flag for storing or not test information
		bool m_no_log;
		//! \brief Structure to store the different results of the tests
		std::map<std::string, std::vector<PerfLogger> > m_results;
		//! \brief Message describing the test
		std::string m_default_msg;
	public:
		/*! \name Public Constructors
		 *
		 *  Public constructors of the class PerfListener
		 */
		//@{

		/*! 
		 *  \brief Default constructor.
		 *
		 *  \details
		 *  Initialize all attributes
		 */
		PerfListener()
		{
			m_default_msg = "New Test";
			m_tolerance = 0.05;
			m_number_improved_perfs = 0;
			m_number_stalling_perfs = 0;
			m_number_worst_perfs = 0;
			m_base_dir = "../perflogs/";
			m_no_log = false;
		}
		//@}

		/*! \name Public Destructor
		 *
		 *  Public destructor of the class PerfListener
		 */
		//@{

		/*! 
		 *  \brief Destructor.
		 *
		 *  \details
		 *  Do nothing (no memory allocated by this class)
		 */
		virtual ~PerfListener()
		{
			;
		}
		//@}

		/*! \name Public Getter
		 *
		 *  Public Getter of the class PerfListener. \n
		 *  These methods give access in read-only mode to the datas/information of the class PerfListener
		 */
		//@{

		/*!
		 *  \brief Return the class name. 
		 *
		 *  \return "PerfListener" The name of the class
		 */
		const char* getClassName() const
		{
			return "PerfListener";
		}
		//@}

		/*! \name Public Setter
		 *
		 *  Public Setter of the class PerfTestFramework. \n
		 *  These methods give access in read/write mode to the datas/information of the class PerfListener
		 */
		//@{
		/*!
		 *  \brief Set the test message
		 *
		 *  \param _msg The test message
		 */
		void setMsg(const std::string _msg)
		{
			m_default_msg=_msg;
		}

		/*!
		 *  \brief Set the tolerance for stall tests
		 *
		 *  \param _tol The tolerance
		 */
		void setTol(const double _tol)
		{
			m_tolerance = _tol;
		}

		//! \brief Set the file log to false
		void noLog()
		{
			m_no_log = true;
		}
		//@}

	private:
		/*!
		 *  \brief Triggered when the test program starts
		 *
		 *  \param[in] unit_test The unit test
		 */
		virtual void OnTestProgramStart(const UnitTest& _unitTest)
		{
			;
		}

		/*!
		 *  \brief Triggered when the test program ends
		 *
		 *  Display a summary of the test results. \n
		 *  Write details on the execution in the log file.
		 *
		 *  \param[in] unit_test The unit test
		 */
		virtual void OnTestProgramEnd(const UnitTest& unit_test) 
		{
			int totalTests = m_number_improved_perfs + m_number_stalling_perfs + m_number_worst_perfs;
			std::string fileName = "summary_test";
#ifdef WITH_MPI
			fileName.append("_np");
			std::stringstream ss;
			ss << m_results.begin()->second[0].m_num_proc;
			fileName.append(ss.str());
#endif
			fileName.append(".xml");
			YALLA_LOG("Perf test summary:");
			YALLA_SUCCEED("Number of improved tests: " << m_number_improved_perfs << " (" << static_cast<double>(m_number_improved_perfs)/totalTests*100 << "%)");
			YALLA_STALLED("Number of stalling tests: " << m_number_stalling_perfs << " (" << static_cast<double>(m_number_stalling_perfs)/totalTests*100 << "%)");
			YALLA_FAILED("Number of worst tests: " << m_number_worst_perfs << " (" << static_cast<double>(m_number_worst_perfs)/totalTests*100 << "%)");
			if(!m_no_log)
			{
				YALLA_LOG("Writing summary to file: " << fileName);
				this->writeSummaryToFile(fileName.c_str());
			}
		}

		/*!
		 *  \brief Triggered before a test starts
		 *
		 *  Read previous logged results for comparison.
		 *
		 *  \param[in] test_info The test information
		 */
		virtual void OnTestStart(const TestInfo& test_info) 
		{
			PerfLogger perfLog;
			YALLA_LOG(test_info.name() << " (" << test_info.test_case_name() << ") started");
			m_test_file = test_info.test_case_name();
			m_test_file.append("_");
			m_test_file.append(test_info.name());
#ifdef WITH_MPI
			perfLog.m_num_proc = ParUtils::getNumProc();
			std::stringstream ss;
			ss << perfLog.m_num_proc;
			m_test_file.append("_np");
			m_test_file.append(ss.str());
#endif
			m_test_file.append(".xml");
#ifndef WITH_MPI
			this->readSeqTests(m_test_file.c_str(),perfLog);
#else
			this->readDistTests(m_test_file.c_str(),perfLog);
#endif
			perfLog.m_test_name = test_info.name();
			m_results[test_info.test_case_name()].push_back(perfLog);
#if WITH_CPP11
			m_start = std::chrono::system_clock::now();
#else
      profiler.start("test");
#endif
		}

		/*!
		 *  \brief Triggered on partial result
		 *
		 *  This methods is called after a failed assertion or a SUCCEED() invocation.
		 *
		 *  \param[in] test_part_result The partial result
		 */
		virtual void OnTestPartResult(const TestPartResult& test_part_result) 
		{
			YALLA_LOG("Test: " << test_part_result.summary() << (test_part_result.failed() ? "*** Failure" : "Success"));
		}

		/*!
		 *  \brief Triggered after a test ends
		 *
		 *  Collect execution statistics and write the details in the log files. \n
		 *  Display a summary on the standard output. \n
		 *
		 *  \param[in] test_info The test information
		 */
		virtual void OnTestEnd(const TestInfo& test_info) 
		{
			YALLA_LOG(test_info.name() << " (" << test_info.test_case_name() << ") ended");
#if WITH_CPP11
			m_end = std::chrono::system_clock::now();
			double elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
#else
      profiler.stop("test");
      double elapsed_seconds = profiler.getTime("test");
#endif
			int lastEntry = m_results[test_info.test_case_name()].size()-1;
#ifdef WITH_MPI
			struct
			{
				double m_val;
				int m_rank;
			} loc, min, max;
			double avg;
			loc.m_val = elapsed_seconds;
			loc.m_rank = ParUtils::getProcRank();
			ParUtils::Reduce(&loc,&max,1,MPI_DOUBLE_INT,MPI_MAXLOC,0,MPI_COMM_WORLD);
			ParUtils::Reduce(&loc,&min,1,MPI_DOUBLE_INT,MPI_MINLOC,0,MPI_COMM_WORLD);
			ParUtils::Reduce(&loc.m_val,&avg,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
			avg /= ParUtils::getNumProc();
			elapsed_seconds = max.m_val;
#endif
			char buffer [80];
			std::time_t result = std::time(NULL);
			strftime (buffer,80,"%F_%H:%M:%S",std::localtime(&result));
			m_results[test_info.test_case_name()][lastEntry].m_test_date.push_back(buffer);
			m_results[test_info.test_case_name()][lastEntry].m_test_message.push_back(m_default_msg);
			m_results[test_info.test_case_name()][lastEntry].m_test_time.push_back(elapsed_seconds);
#ifdef WITH_MPI
			m_results[test_info.test_case_name()][lastEntry].m_time_proc.push_back(max.m_rank);
			m_results[test_info.test_case_name()][lastEntry].m_min_time.push_back(min.m_val);
			m_results[test_info.test_case_name()][lastEntry].m_min_time_proc.push_back(min.m_rank);
			m_results[test_info.test_case_name()][lastEntry].m_average_time.push_back(avg);
			if(ParUtils::iAmMaster())
			{
#endif
				if(elapsed_seconds<m_results[test_info.test_case_name()][lastEntry].m_best_time)
				{
					m_results[test_info.test_case_name()][lastEntry].m_test_status.push_back("passed");
					YALLA_SUCCEED(elapsed_seconds << " vs. " << m_results[test_info.test_case_name()][lastEntry].m_best_time << " (s)");
					++m_number_improved_perfs;
					m_results[test_info.test_case_name()][lastEntry].m_best_time = elapsed_seconds;
					m_results[test_info.test_case_name()][lastEntry].m_best_test = m_results[test_info.test_case_name()][lastEntry].m_test_time.size()-1;
					m_results[test_info.test_case_name()][lastEntry].m_best_test_date = buffer;
				}
				else if(elapsed_seconds<m_results[test_info.test_case_name()][lastEntry].m_best_time*(1+m_tolerance))
				{
					m_results[test_info.test_case_name()][lastEntry].m_test_status.push_back("stalled");
					++m_number_stalling_perfs;
					YALLA_STALLED(elapsed_seconds << " vs. " << m_results[test_info.test_case_name()][lastEntry].m_best_time << " (s)");
				}
				else
				{
					m_results[test_info.test_case_name()][lastEntry].m_test_status.push_back("failed");
					++m_number_worst_perfs;
					YALLA_FAILED(elapsed_seconds << " vs. " << m_results[test_info.test_case_name()][lastEntry].m_best_time << " (s)");
				}
				if(!m_no_log)
				{
#ifndef WITH_MPI
					this->writeSeqTests(m_test_file.c_str(),test_info.name(),test_info.test_case_name(),lastEntry);
#else
					this->writeDistTests(m_test_file.c_str(),test_info.name(),test_info.test_case_name(),lastEntry);
#endif
				}
#ifdef WITH_MPI
			}
#endif
		}

		/*!
		 *  \brief Write execution summary to a file
		 *
		 *  \param[in] _fileName File to log the execution summary
		 */
		void writeSummaryToFile(const char* _fileName)
		{
			std::string file = m_base_dir + _fileName;
			std::ofstream outFile(file);
			if(outFile.is_open())
			{
				typedef std::map<std::string, std::vector<PerfLogger> >::const_iterator it_type;
				outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
				outFile << "<testsuites tests=\"";
				int numTest = 0;
				for(it_type it = m_results.begin();it!=m_results.end();++it)
					numTest+= it->second.size();
				outFile << numTest << "\" name=\"AllTests\">\n";
				for(it_type it = m_results.begin();it!=m_results.end();++it)
				{
					outFile << "  <testsuite name=\"" << it->first << "\" tests=\"" << it->second.size() << "\">\n";
					for(unsigned int i=0;i<it->second.size();++i)
					{
						outFile << "    <testcase name=\"" << it->second[i].m_test_name << "\" besttime=\"" << it->second[i].m_best_time << "\" rundate=\"" << it->second[i].m_best_test_date << "\" />\n";
					}
					outFile << "  </testsuite>\n";
				}
				outFile << "</testsuites>\n";
				outFile.close();
			}
		}

		/*!
		 *  \brief Read previously logged results for sequential execution
		 *
		 *  \param[in] _fileName File where the log have beed stored
		 *  \param[in] _perfLog Add previously stored result in the current framework
		 */
		void readSeqTests(const char* _fileName, PerfLogger& _perfLog)
		{
			std::string file = m_base_dir + _fileName;
			std::ifstream inFile(file);
			if(inFile.is_open() && inFile.peek()!=std::ifstream::traits_type::eof())
			{
				YALLA_LOG("Looking for previous results...: Found");
				std::string line;
				// Skipping the xml head line
				getline(inFile,line);
				// Getting the testsuite line
				getline(inFile,line);
				std::string beginAttr = "\"";
				std::string endAttr = "\" ";
				std::string testAttr = "tests=";
				std::string statusAttr = "status=";
				std::string timeAttr = "time=";
				std::string dateAttr = "date=";
				std::string classnameAttr = "classname=";
				std::string bestTestAttr = "besttest=";
				std::string bestTestTimeAttr = "besttime=";
				std::string bestTestDateAttr = "bestdate=";
				std::string msgAttr = "msg=";
				std::string lineEndAttr = "\" />";

				unsigned int testAttrBegin = line.find(testAttr);
				unsigned int bestTestAttrBegin = line.find(bestTestAttr);
				unsigned int bestTestTimeAttrBegin = line.find(bestTestTimeAttr);
				unsigned int bestTestDateAttrBegin = line.find(bestTestDateAttr);
				unsigned int classnameAttrBegin = line.find(classnameAttr);

				std::string tmpString = line.substr(testAttrBegin+testAttr.length()+beginAttr.length(),bestTestAttrBegin-testAttrBegin-testAttr.length()-beginAttr.length()-endAttr.length());

				std::istringstream os(tmpString);
				int numTests = 0;
				os >> numTests;

				tmpString = line.substr(bestTestAttrBegin + bestTestAttr.length() + beginAttr.length(), bestTestTimeAttrBegin - bestTestAttrBegin - bestTestAttr.length() - beginAttr.length()-endAttr.length());
				os.clear();
				os.str(tmpString);
				os >> _perfLog.m_best_test;
				
				tmpString = line.substr(bestTestTimeAttrBegin+bestTestTimeAttr.length()+beginAttr.length(),bestTestDateAttrBegin - bestTestTimeAttrBegin - bestTestTimeAttr.length() - beginAttr.length() - endAttr.length());

				double time = 0;
				os.clear();
				os.str(tmpString);
				os >> time;

				_perfLog.m_best_time = time;

				tmpString = line.substr(bestTestDateAttrBegin + bestTestDateAttr.length() + beginAttr.length(),classnameAttrBegin - bestTestDateAttrBegin - bestTestDateAttr.length() - beginAttr.length() - endAttr.length());
				_perfLog.m_best_test_date = tmpString;

				for(int i=0;i<numTests;++i)
				{
					getline(inFile,line);
					unsigned int statusAttrBegin = line.find(statusAttr);
					unsigned int timeAttrBegin = line.find(timeAttr);
					unsigned int dateAttrBegin = line.find(dateAttr);
					unsigned int classnameAttrBegin = line.find(classnameAttr);
					unsigned int msgAttrBegin = line.find(msgAttr);
					unsigned int lineEndAttrBegin = line.find(lineEndAttr);

					std::string tmpString = line.substr(statusAttrBegin + statusAttr.length() + beginAttr.length(), timeAttrBegin - statusAttrBegin - statusAttr.length() - beginAttr.length()-endAttr.length());
					_perfLog.m_test_status.push_back(tmpString);
				
					tmpString = line.substr(timeAttrBegin+timeAttr.length()+beginAttr.length(), dateAttrBegin - timeAttrBegin - timeAttr.length() - beginAttr.length() - endAttr.length());
					double time = 0;
					os.clear();
					os.str(tmpString);
					os >> time;

					_perfLog.m_test_time.push_back(time);
					
					tmpString = line.substr(dateAttrBegin + dateAttr.length() + beginAttr.length(), classnameAttrBegin - dateAttrBegin - dateAttr.length() - beginAttr.length() - endAttr.length());
					_perfLog.m_test_date.push_back(tmpString);

					tmpString = line.substr(msgAttrBegin + msgAttr.length() + beginAttr.length(), lineEndAttrBegin - msgAttrBegin - msgAttr.length() - beginAttr.length());
					_perfLog.m_test_message.push_back(tmpString);
				}
			}
			else
			{
				_perfLog.m_best_time = std::numeric_limits<double>::max();
				YALLA_LOG("Looking for previous results...: Not found");
			}
		}

#ifdef WITH_MPI
		/*!
		 *  \brief Read previously logged results for distributed  execution
		 *
		 *  \param[in] _fileName File where the log have beed stored
		 *  \param[in] _perfLog Add previously stored result in the current framework
		 */
		void readDistTests(const char* _fileName, PerfLogger& _perfLog)
		{
			std::string file = m_base_dir + _fileName;
			std::ifstream inFile(file);
			if(inFile.is_open() && inFile.peek()!=std::ifstream::traits_type::eof())
			{
				YALLA_LOG("Looking for previous results...: Found");
				std::string line;
				// Skipping the xml head line
				getline(inFile,line);
				// Getting the testsuite line
				getline(inFile,line);
				std::string beginAttr = "\"";
				std::string endAttr = "\" ";
				std::string testAttr = "tests=";
				std::string statusAttr = "status=";
				std::string maxTimeAttr = "maxtime=";
				std::string minTimeAttr = "mintime=";
				std::string avgTimeAttr = "avgtime=";
				std::string procMaxTimeAttr = "procmaxtime=";
				std::string procMinTimeAttr = "procmintime=";
				std::string dateAttr = "date=";
				std::string classnameAttr = "classname=";
				std::string bestTestAttr = "besttest=";
				std::string bestTestTimeAttr = "besttime=";
				std::string bestTestDateAttr = "bestdate=";
				std::string msgAttr = "msg=";
				std::string lineEndAttr = "\" />";

				unsigned int testAttrBegin = line.find(testAttr);
				unsigned int bestTestAttrBegin = line.find(bestTestAttr);
				unsigned int bestTestTimeAttrBegin = line.find(bestTestTimeAttr);
				unsigned int bestTestDateAttrBegin = line.find(bestTestDateAttr);
				unsigned int classnameAttrBegin = line.find(classnameAttr);

				std::string tmpString = line.substr(testAttrBegin+testAttr.length()+beginAttr.length(),bestTestAttrBegin-testAttrBegin-testAttr.length()-beginAttr.length()-endAttr.length());

				std::istringstream os(tmpString);
				int numTests = 0;
				os >> numTests;

				tmpString = line.substr(bestTestAttrBegin + bestTestAttr.length() + beginAttr.length(), bestTestTimeAttrBegin - bestTestAttrBegin - bestTestAttr.length() - beginAttr.length()-endAttr.length());
				os.clear();
				os.str(tmpString);
				os >> _perfLog.m_best_test;
				
				tmpString = line.substr(bestTestTimeAttrBegin+bestTestTimeAttr.length()+beginAttr.length(),bestTestDateAttrBegin - bestTestTimeAttrBegin - bestTestTimeAttr.length() - beginAttr.length() - endAttr.length());

				double time = 0;
				os.clear();
				os.str(tmpString);
				os >> time;

				_perfLog.m_best_time = time;

				tmpString = line.substr(bestTestDateAttrBegin + bestTestDateAttr.length() + beginAttr.length(),classnameAttrBegin - bestTestDateAttrBegin - bestTestDateAttr.length() - beginAttr.length() - endAttr.length());
				_perfLog.m_best_test_date = tmpString;

				for(int i=0;i<numTests;++i)
				{
					getline(inFile,line);
					unsigned int statusAttrBegin = line.find(statusAttr);
					unsigned int maxTimeAttrBegin = line.find(maxTimeAttr);
					unsigned int procMaxTimeAttrBegin = line.find(procMaxTimeAttr);
					unsigned int minTimeAttrBegin = line.find(minTimeAttr);
					unsigned int procMinTimeAttrBegin = line.find(procMinTimeAttr);
					unsigned int avgTimeAttrBegin = line.find(avgTimeAttr);
					unsigned int dateAttrBegin = line.find(dateAttr);
					unsigned int classnameAttrBegin = line.find(classnameAttr);
					unsigned int msgAttrBegin = line.find(msgAttr);
					unsigned int lineEndAttrBegin = line.find(lineEndAttr);
						
					std::string tmpString = line.substr(statusAttrBegin + statusAttr.length() + beginAttr.length(), maxTimeAttrBegin - statusAttrBegin - statusAttr.length() - beginAttr.length()-endAttr.length());
					_perfLog.m_test_status.push_back(tmpString);
				
					tmpString = line.substr(maxTimeAttrBegin+maxTimeAttr.length()+beginAttr.length(), procMaxTimeAttrBegin - maxTimeAttrBegin - maxTimeAttr.length() - beginAttr.length() - endAttr.length());
					double maxTime = 0;
					os.clear();
					os.str(tmpString);
					os >> maxTime;

					_perfLog.m_test_time.push_back(maxTime);
					
					tmpString = line.substr(procMaxTimeAttrBegin+procMaxTimeAttr.length()+beginAttr.length(), minTimeAttrBegin - procMaxTimeAttrBegin - procMaxTimeAttr.length() - beginAttr.length() - endAttr.length());
					int procMaxTime = 0;
					os.clear();
					os.str(tmpString);
					os >> procMaxTime;

					_perfLog.m_time_proc.push_back(procMaxTime);

					tmpString = line.substr(minTimeAttrBegin+minTimeAttr.length()+beginAttr.length(), procMinTimeAttrBegin - minTimeAttrBegin - minTimeAttr.length() - beginAttr.length() - endAttr.length());
					double minTime = 0;
					os.clear();
					os.str(tmpString);
					os >> minTime;

					_perfLog.m_min_time.push_back(minTime);
					
					tmpString = line.substr(procMinTimeAttrBegin+procMinTimeAttr.length()+beginAttr.length(), avgTimeAttrBegin - procMinTimeAttrBegin - procMinTimeAttr.length() - beginAttr.length() - endAttr.length());
					int procMinTime = 0;
					os.clear();
					os.str(tmpString);
					os >> procMinTime;

					_perfLog.m_min_time_proc.push_back(procMaxTime);

					tmpString = line.substr(avgTimeAttrBegin+avgTimeAttr.length()+beginAttr.length(), dateAttrBegin - avgTimeAttrBegin - avgTimeAttr.length() - beginAttr.length() - endAttr.length());
					double avgTime = 0;
					os.clear();
					os.str(tmpString);
					os >> avgTime;

					_perfLog.m_average_time.push_back(avgTime);

					tmpString = line.substr(dateAttrBegin + dateAttr.length() + beginAttr.length(), classnameAttrBegin - dateAttrBegin - dateAttr.length() - beginAttr.length() - endAttr.length());
					_perfLog.m_test_date.push_back(tmpString);

					tmpString = line.substr(msgAttrBegin + msgAttr.length() + beginAttr.length(), lineEndAttrBegin - msgAttrBegin - msgAttr.length() - beginAttr.length());
					_perfLog.m_test_message.push_back(tmpString);
				}
			}
			else
			{
				_perfLog.m_best_time = std::numeric_limits<double>::max();
				YALLA_LOG("Looking for previous results...: Not found");
			}
		}
#endif

		/*!
		 *  \brief Write detailed sequential execution results in a log file
		 *
		 *  \param[in] _fileName File to log the results
		 *  \param[in] _testName The name of the test
		 *  \param[in] _testCaseName The name of the test case
		 *  \param[in] _lastEntry The index of the last stored entry
		 */
		void writeSeqTests(const char* _outFile, const char* _testName, const char* _testCaseName, const int _lastEntry)
		{
			std::string file = m_base_dir + m_test_file;
			std::ofstream outFile(file);
			if(outFile.is_open())
			{
				outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
				outFile << "<testsuite name=\"" << _testName << "\" tests=\"" << m_results[_testCaseName][_lastEntry].m_test_time.size() << "\"";
				outFile << " besttest=\"" << m_results[_testCaseName][_lastEntry].m_best_test << "\" besttime=\"" << m_results[_testCaseName][_lastEntry].m_best_time << "\" bestdate=\"" << m_results[_testCaseName][_lastEntry].m_best_test_date << "\" classname=\"" << _testCaseName << "\">\n";
				for(unsigned int i=0;i<m_results[_testCaseName][_lastEntry].m_test_time.size();++i)
				{
					outFile << "  <testcase name=\"" << _testName << "\" test=\"" << i << "\"";
					outFile << " status=\"" << m_results[_testCaseName][_lastEntry].m_test_status[i] << "\" time=\"" << m_results[_testCaseName][_lastEntry].m_test_time[i] << "\" date=\"" << m_results[_testCaseName][_lastEntry].m_test_date[i] << "\" classname=\"" << _testCaseName << "\" msg=\"" << m_results[_testCaseName][_lastEntry].m_test_message[i] << "\" />\n";
				}
				outFile << "</testsuite>\n";
				outFile.close();
			}
		}

#ifdef WITH_MPI
		/*!
		 *  \brief Write detailed distributed execution results in a log file
		 *
		 *  \param[in] _fileName File to log the results
		 *  \param[in] _testName The name of the test
		 *  \param[in] _testCaseName The name of the test case
		 *  \param[in] _lastEntry The index of the last stored entry
		 */
		void writeDistTests(const char* _outFile, const char* _testName, const char* _testCaseName, const int _lastEntry)
		{
			std::string file = m_base_dir + m_test_file;
			std::ofstream outFile(file);
			if(outFile.is_open())
			{
				outFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
				outFile << "<testsuite name=\"" << _testName << "\" tests=\"" << m_results[_testCaseName][_lastEntry].m_test_time.size() << "\"";
				outFile << " besttest=\"" << m_results[_testCaseName][_lastEntry].m_best_test << "\" besttime=\"" << m_results[_testCaseName][_lastEntry].m_best_time << "\" bestdate=\"" << m_results[_testCaseName][_lastEntry].m_best_test_date << "\" classname=\"" << _testCaseName << "\">\n";
#ifndef WITH_MPI
				for(unsigned int i=0;i<m_results[_testCaseName][_lastEntry].m_test_time.size();++i)
				{
					outFile << "  <testcase name=\"" << _testName << "\" test=\"" << i << "\"";
					outFile << " status=\"" << m_results[_testCaseName][_lastEntry].m_test_status[i] << "\" time=\"" << m_results[_testCaseName][_lastEntry].m_test_time[i] << "\" date=\"" << m_results[_testCaseName][_lastEntry].m_test_date[i] << "\" classname=\"" << _testCaseName << "\" msg=\"" << m_results[_testCaseName][_lastEntry].m_test_message[i] << "\" />\n";
				}
#else
				for(unsigned int i=0;i<m_results[_testCaseName][_lastEntry].m_test_time.size();++i)
				{
					outFile << "  <testcase name=\"" << _testName << "\" test=\"" << i << "\"";
					outFile << " status=\"" << m_results[_testCaseName][_lastEntry].m_test_status[i] << "\" maxtime=\"" << m_results[_testCaseName][_lastEntry].m_test_time[i] << "\" procmaxtime=\"" << m_results[_testCaseName][_lastEntry].m_time_proc[i] << "\" mintime=\"" << m_results[_testCaseName][_lastEntry].m_min_time[i] << "\" procmintime=\"" << m_results[_testCaseName][_lastEntry].m_min_time_proc[i] << "\" avgtime=\"" << m_results[_testCaseName][_lastEntry].m_average_time[i] << "\" date=\"" << m_results[_testCaseName][_lastEntry].m_test_date[i] << "\" classname=\"" << _testCaseName << "\" msg=\"" << m_results[_testCaseName][_lastEntry].m_test_message[i] << "\" />\n";
				}
#endif
				outFile << "</testsuite>\n";
				outFile.close();
			}
		}
#endif
	}; // class PerfListener
	//! \brief The listener
	PerfListener* m_listener;
	//! \brief The command line arguments
	char** m_cmd_line;
	//! \brief The number of command line arguments
	int m_num_args;

 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class PerfTestFramework
	 */
	//@{

	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Process the command line and set the relevant values of the PerfTestFramework
	 */
	explicit PerfTestFramework(int _argc, char** _argv)
	{
		m_listener = new PerfListener();
		m_num_args = _argc;
		m_cmd_line = _argv;
		if(_argc > 1)
		{
			for(int i=1;i<_argc;++i)
			{
				std::string arg = _argv[i];
				if(arg.find("--msg=")!=std::string::npos)
				{
					unsigned pos = arg.find("=");
					std::string msg = arg.substr(pos+1);
					m_listener->setMsg(msg);
				}
				else if(arg.find("--tol=")!=std::string::npos)
				{
					unsigned pos = arg.find("=");
					std::string tolString = arg.substr(pos+1);
					std::istringstream os(tolString);
					double tol = 0;
					os >> tol;
					m_listener->setTol(tol);
				}
				else if(arg.find("--no-log")!=std::string::npos)
				{
					m_listener->noLog();
				}
				else
				{
					unsigned int pos = arg.find("--");
					std::string argument = arg.substr(pos+2,5);
					if(!(argument=="gtest"))
					{
						YALLA_LOG("Argument not understood: " << arg << "\nExit now");
						YALLA_ABORT(-1);
					}
				}
			}
		}
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class PerfTestFramework
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated by this class)
	 */
	virtual ~PerfTestFramework()
	{
		;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class PerfTestFramework. \n
	 *  These methods give access in read-only mode to the datas/information of the class PerfTestFramework
	 */
	//@{

	/*!
	 *  \brief Return the a handler on the listener
	 *
	 *  \return The listener
	 */
	PerfListener* getListener()
	{
		return m_listener;
	}

	/*!
	 *  \brief Return the command line arguments
	 *
	 *  \return The the command line arguments
	 */
	char** getCmdLineArgument()
	{
		return m_cmd_line;
	}

	/*!
	 *  \brief Return the number of command line arguments
	 *
	 *  \return The the number of command line arguments
	 */
	int* getNumArgument()
	{
		return &m_num_args;
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "PerfTestFramework" The name of the class
	 */
	const char* getClassName() const
	{
		return "PerfTestFramework";
	}
	//@}
}; // class PerfFramework
	
YALLA_END_NAMESPACE

#endif
#endif
