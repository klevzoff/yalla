#pragma once
#ifndef yalla_mpi_listener_h
#define yalla_mpi_listener_h

/*!
 *  \file MPIListener.h
 *  \brief Extension of the gtest listener for distributed environment
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#ifdef WITH_GTEST

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/IO/console/log.h"
#include "yalla/Utils/ParUtils/ParUtils.h"

using ::testing::EmptyTestEventListener;
using ::testing::InitGoogleTest;
using ::testing::Test;
using ::testing::TestCase;
using ::testing::TestEventListeners;
using ::testing::TestInfo;
using ::testing::TestPartResult;
using ::testing::UnitTest;

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*! \brief Success message
 *
 *  \param info The message
 */
#define yalla_succeed(info) std::cout << "\E[0m\E[32m\E[40m" << info << "\E[0m"
/*! \brief Faile message
 *
 *  \param info The message
 */
#define yalla_failed(info) std::cout << "\E[0m\E[31m\E[40m" << info << "\E[0m"

/*!
 *  \class MPIGTestListener
 *  \brief Extension of the gtest listener for distributed environment
 *
 *  The google test framework is mandatory for this class to be used
 */
class MPIGTestListener
: public EmptyTestEventListener
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef MPIGTestListener this_class;
	//! \brief Type of the super class
	typedef EmptyTestEventListener super_class;
	//@}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "MPIGTestListener" The name of the class
	 */
	const char* getClassName() const
	{
		return "MPIGTestListener";
	}
 private:
	/*!
	 *  \brief Triggered when the test program starts
	 *
	 *  \param[in] unit_test The unit test
	 */
	virtual void OnTestProgramStart(const UnitTest& unit_test)
	{
		if(ParUtils::iAmMaster())
		{
			yalla_succeed("[==========]");
			std::cout << " Running " << unit_test.total_test_count() << " tests from " << unit_test.total_test_case_count() << " test cases on " << ParUtils::getNumProc() << " procs.\n" << std::flush;
		}
	}

	/*!
	 *  \brief Triggered when the environment is set up
	 *
	 *  \param[in] unit_test The unit test
	 */
	virtual void OnEnvironmentsSetUpStart(const UnitTest& unit_test)
	{
		if(ParUtils::iAmMaster())
		{
			yalla_succeed("[----------]");
			std::cout << " Global test environment set-up. \n" << std::flush;
		}
	}

	/*!
	 *  \brief Triggered when the environment is tear down
	 *
	 *  \param[in] unit_test The unit test
	 */
	virtual void OnEnvironmentsTearDownStart(const UnitTest& unit_test)
	{
		if(ParUtils::iAmMaster())
		{
			yalla_succeed("[----------]");
			std::cout << " Global test environment tear-down. \n" << std::flush;
		}
	}

	/*!
	 *  \brief Triggered when the test program ends
	 *
	 *  \param[in] unit_test The unit test
	 */
	virtual void OnTestProgramEnd(const UnitTest& unit_test)
	{
		if(ParUtils::iAmMaster())
		{
			yalla_succeed("[==========]");
			std::cout << " " << unit_test.total_test_count() << " tests from " << unit_test.total_test_case_count() << " test cases ran. (" << unit_test.elapsed_time() << " ms total)\n" << std::flush;
			yalla_succeed("[  PASSED  ]");
			std::cout << " " << unit_test.successful_test_count() << " tests.\n" << std::flush;
			if(unit_test.Failed())
			{
				yalla_failed("[  FAILED  ]");
				std::cout << " " << unit_test.failed_test_case_count() << " tests, listed below: \n" << std::flush;
				for(int i=0;i<unit_test.total_test_case_count();++i)
				{
					const TestCase* test = unit_test.GetTestCase(i);
					if(test->Failed())
					{
						for(int j=0;j<test->total_test_count();++j)
						{
							const TestInfo* info = test->GetTestInfo(j);
							if(info->result()->Failed())
							{
								yalla_failed("[  FAILED  ]");
								std::cout << " " << info->name() << "." << info->test_case_name() << "\n" << std::flush;
							}
						}
					}
				}
			}
			if(unit_test.Failed())
				std::cout << "\n " << unit_test.failed_test_case_count() << " FAILED TEST\n" << std::flush;
		}
	}

	/*!
	 *  \brief Triggered when the test case starts
	 *
	 *  \param[in] test_case The test case
	 */
	virtual void OnTestCaseStart(const TestCase& test_case)
	{
		if(ParUtils::iAmMaster())
		{
			yalla_succeed("[----------]");
			std::cout << " " << test_case.total_test_count() << " test from " << test_case.name() << "\n" << std::flush;
		}
	}

	/*!
	 *  \brief Triggered when the test case ends
	 *
	 *  \param[in] test_case The test case
	 */
	virtual void OnTestCaseEnd(const TestCase& test_case)
	{
		if(ParUtils::iAmMaster())
		{
			yalla_succeed("[----------]");
			std::cout << " " << test_case.total_test_count() << " test from " << test_case.name() << " (" << test_case.elapsed_time() << "ms total) \n\n" << std::flush;
		}
	}

	/*!
	 *  \brief Triggered before a test starts
	 *
	 *  \param[in] test_info The test information
	 */
	virtual void OnTestStart(const TestInfo& test_info) 
	{
		if(ParUtils::iAmMaster())
		{
			yalla_succeed("[ RUN      ]");
			std::cout << " " << test_info.test_case_name() << "." << test_info.name() << "\n" << std::flush;
		}
	}

	/*!
	 *  \brief Triggered on partial result
	 *
	 *  This methods is called after a failed assertion or a SUCCEED() invocation.
	 *
	 *  \param[in] test_part_result The partial result
	 */
	virtual void OnTestPartResult(const TestPartResult& test_part_result) 
	{
		if(test_part_result.failed())
		{
			yalla_failed("Failure in: " << test_part_result.file_name() << ":" << test_part_result.line_number() << " on proc: " << ParUtils::getProcRank() << "\n" << test_part_result.summary() << "\n");
		}
	}

	/*!
	 *  \brief Triggered after a test ends
	 *
	 *  \param[in] test_info The test information
	 */
	virtual void OnTestEnd(const TestInfo& test_info) 
	{
		if(ParUtils::iAmMaster())
		{
			if(test_info.result()->Failed())
				yalla_failed("[  FAILED  ]");
			else
				yalla_succeed("[       OK ]");
			std::cout << " " << test_info.test_case_name() << "." << test_info.name() << " (" << test_info.result()->elapsed_time() << "ms) \n" << std::flush;
		}
	}
};
YALLA_END_NAMESPACE

#endif
#endif
