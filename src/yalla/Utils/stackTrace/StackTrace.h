#pragma once
#ifndef yalla_stack_trace_h
#define yalla_stack_trace_h

/*!
 *  \file StackTrace.h
 *  \brief Return the call stack
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <string>
#include <sstream>
#ifdef _WIN32
#include <Windows.h>
#include <DbgHelp.h>

//! Link de la library Dbghelp.lib
#pragma comment(lib, "Dbghelp.lib")
#else
#include <execinfo.h>
#include <cxxabi.h>
#include <iostream>
#include <string.h>
#include <cstdlib>
#endif

#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class StackTrace
 *  \brief Return the call stack
 * 
 *  \details
 *  Return the call stack for windows and unix. \n
 *  
 *  \warning Seems to not working properly on a virtual machine
 */
class StackTrace
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef StackTrace this_type;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class StackTrace
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Do nothing
	 */
	explicit StackTrace();
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class StackTrace
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated by this class)
	 */
	virtual ~StackTrace();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class StackTrace. \n
	 *  These methods give access in read-only mode to the datas/information of the class StackTrace
	 */
	//@{

	/*!
	 *  \brief Get and return the call stack
	 *  
	 *  \return The call stack
	 */
	static std::string printStack();

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "StackTrace" The name of the class
	 */
	const char* getClassName() const
	{
		return "StackTrace";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
