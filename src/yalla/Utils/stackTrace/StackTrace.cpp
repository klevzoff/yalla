#include "StackTrace.h"

/*!
 *  \file StackTrace.cpp
 *  \brief StackTrace implementation
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_USING_NAMESPACE(yalla)

StackTrace::StackTrace()
{
  ;
}

StackTrace::~StackTrace()
{
  ;
}

std::string StackTrace::printStack()
{
  std::string stackToString = "";
#ifdef _WIN32
  unsigned int i;
  void* stack[100];
  unsigned short frames;
  SYMBOL_INFO * symbol;
  HANDLE process;

  process = GetCurrentProcess();

  SymInitialize(process,NULL,TRUE);
  unsigned short int ushortmax = 65535;
  frames = CaptureStackBackTrace(1,ushortmax,stack,NULL);
  symbol = (SYMBOL_INFO*)calloc(sizeof(SYMBOL_INFO) + 256 * sizeof(char),1);
  symbol->MaxNameLen = 255;
  symbol->SizeOfStruct = sizeof(SYMBOL_INFO);

  for(i=0;i<frames;i++)
  {
    SymFromAddr(process,(DWORD64)(stack[i]),0,symbol);
    std::ostringstream oss;
    oss << frames - i - 1;
    stackToString.append(oss.str());
    stackToString.append(": ");
    stackToString.append(symbol->Name);
    stackToString.append(" - 0x");
    oss << std::hex << symbol->Address;
    stackToString.append(oss.str());
    stackToString.append("\n");
  }
  free( symbol );
#else
	const int max_depth = 100;
	void *stack_addrs[max_depth];
    
	const int stack_depth = backtrace(stack_addrs,max_depth);
	char **stack_strings = backtrace_symbols(stack_addrs,stack_depth);
    
	stackToString.append("Call stack from ");
	std::ostringstream oss;
	oss << __FILE__;
	oss << ":";
	oss << __LINE__;
	stackToString.append(oss.str());
	stackToString.append("\n");
	
	for (size_t i=1;i< static_cast<size_t>(stack_depth);i++)
	{
		size_t sz = 200;
		char *function = static_cast<char*>(malloc(sz));
		char *begin = 0, *end = 0;
        
		for (char *j=stack_strings[i];*j; ++j) 
		{
			if (( *j == '_' ) && ( *(j-1) == ' ' ))
				begin = j-1;
			else if (*j == '+') 
			{
				end = j-1;
			}
		}
		if (begin && end) 
		{
			*begin++ = '\0';
			*end++ = '\0';
            
			int status;
			char *ret = abi::__cxa_demangle(begin,function,&sz,&status);
			if (ret) 
			{
				function = ret;
			}
			else 
			{
				strncpy(function,begin,sz);
				strncat(function,"()",sz);
				function[sz-1] = '\0';
			}
			stackToString.append("* ");
			stackToString.append(stack_strings[i]);
			stackToString.append(" ");
			stackToString.append(function);
			stackToString.append("\n");
		}
		else 
		{
			stackToString.append("* ");
			stackToString.append(stack_strings[i]);
			stackToString.append("\n");
		}    
		free(function);
	}
	free(stack_strings);
#endif
  return stackToString;
}
