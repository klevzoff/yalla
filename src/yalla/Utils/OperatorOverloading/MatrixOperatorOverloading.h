#pragma once
#ifndef yalla_matrix_operator_overloading_h
#define yalla_matrix_operator_overloading_h

/*!
 *  \file MatrixOperatorOverloading.h
 *  \brief Operator overloading for matrices
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/Promote.h"
#include "yalla/Utils/Exceptions/MatrixDimensionsCompatibles.h"
#include "yalla/Utils/OperatorOverloading/MatrixGraphOperatorOverloading.h"
#include "yalla/Utils/BLAS/BLAS.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class MatrixOperationsChecking
 *  \brief Check matrices operations consistency
 */
class MatrixOperationsChecking
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef MatrixOperationsChecking this_type;
	//@}

/*! \brief Check if two matrices dimensions are compatibles
 *  
 *  \param[in] _dimLHS LHS dimension
 *  \param[in] _dimRHS RHS dimension
 *
 *  \exception MatrixDimensionsIncompatibles Matrices dimensions are incompatibles
 */
	static void checkCompatibleDimensions(const int _dimLHS, const int _dimRHS)
	{
#ifdef _DEBUG
		try
		{
			if(!MatrixDimensionsCompatibles::Check(_dimLHS,_dimRHS))
				throw MatrixDimensionsIncompatibles(__FILE__,__LINE__);
		}
		catch (MatrixDimensionsIncompatibles &matrixDimIncompatibles)
		{
			YALLA_ERR(matrixDimIncompatibles.what());
				}
#endif
	}

/*! \brief Check if two matrices mapping are compatibles
 *  
 *  \param[in] _sizeLHS Size of the LHS mapping
 *  \param[in] _sizeRHS Size of the RHS mapping
 *  \param[in] _mappingLHS LHS mapping
 *  \param[in] _mappingRHS RHS mapping
 */
	static void checkCompatibleMapping(const int _sizeLHS, const int _sizeRHS, const int* _mappingLHS, const int* _mappingRHS)
	{
#ifdef _DEBUG
		if(_sizeLHS != _sizeRHS)
			YALLA_ERR("Error, differents mappings");
		const int size = _sizeLHS;
		for(int i=0;i<size;++i)
		{
			if(_mappingLHS[i]!=_mappingRHS[i])
			{
				YALLA_ERR("Error, differents mappings");
			}
		}
#endif
	}
};

/*! \name Public Matrix-Matrix operator overloading
 *
 *  Public Matrix-Matrix operator overloading
 */
//@{

/*!
 *  \brief Operator overloading for addition or soustraction operations between two sequential matrices
 *  
 *  Possible operations between two matrices are: \n
 *  - Addition: element wise addition between two matrices
 *  - Soustraction: element wise soustraction between two matrices
 *
 *  \param MATRIXTYPE Type of the matrix
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Matrix containing the resulting operation
 */
#define SEQ_MATRIX_ADD_MINUS_OVERLOADING(MATRIXTYPE,OPERATOR,OPERATION) \
  template<typename DataTypeLHS, typename MatrixImplLHS, typename DataTypeRHS, typename MatrixImplRHS> \
		MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type,MatrixImplLHS> OPERATOR(const MATRIXTYPE<DataTypeLHS,MatrixImplLHS>& _matrixLHS, const MATRIXTYPE<DataTypeRHS,MatrixImplRHS>& _matrixRHS) \
  {																																			\
    const int lNbRows = _matrixLHS.getNbRows();								\
    const int lNbCols = _matrixLHS.getNbCols();								\
    const int rNbRows = _matrixRHS.getNbRows();								\
    const int rNbCols = _matrixRHS.getNbCols();								\
    MatrixOperationsChecking::checkCompatibleDimensions(lNbRows,rNbRows); \
    MatrixOperationsChecking::checkCompatibleDimensions(lNbCols,rNbCols); \
    MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> tmpMatrix(_matrixLHS.getMatrixGraph()+_matrixRHS.getMatrixGraph()); \
    for(int i=0;i<lNbRows;++i)																	\
    {																																		\
			for(int j=0;j<lNbCols;++j)																\
			{																																	\
				tmpMatrix(i,j) = _matrixLHS(i,j) OPERATION _matrixRHS(i,j);			\
			}																																	\
    }																																		\
    return tmpMatrix;																										\
	}																																			\

SEQ_MATRIX_ADD_MINUS_OVERLOADING(SeqDenseMatrix,operator+,+)
SEQ_MATRIX_ADD_MINUS_OVERLOADING(SeqDenseMatrix,operator-,-)

SEQ_MATRIX_ADD_MINUS_OVERLOADING(SeqSparseMatrix,operator+,+)
SEQ_MATRIX_ADD_MINUS_OVERLOADING(SeqSparseMatrix,operator-,-)
#undef SEQ_MATRIX_ADD_MINUS_OVERLOADING

/*!
 *  \brief Operator overloading for multiplication operations between two sequential matrices
 *  
 *  \param MATRIXTYPE Type of the matrix
 *  \param OPERATOR Operator
 *
 *  \return Matrix containing the resulting operation
 */
#define SEQ_MATRIX_TIMES_OVERLOADING(MATRIXTYPE,OPERATOR)								\
	template <typename DataTypeLHS, typename MatrixImplLHS, typename DataTypeRHS, typename MatrixImplRHS> \
		static MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> OPERATOR(const MATRIXTYPE<DataTypeLHS,MatrixImplLHS> & _matrixLHS, const MATRIXTYPE<DataTypeRHS,MatrixImplRHS> & _matrixRHS) \
	{																																			\
		const int lNbCols = _matrixLHS.getNbCols();								\
		const int rNbRows = _matrixRHS.getNbRows();								\
		MatrixOperationsChecking::checkCompatibleDimensions(lNbCols,rNbRows); \
		MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> tmpMatrix(_matrixLHS.getMatrixGraph()*_matrixRHS.getMatrixGraph()); \
		BLAS::gemm(_matrixLHS,_matrixRHS,tmpMatrix);												\
		return tmpMatrix;																										\
	}																																			\
		
SEQ_MATRIX_TIMES_OVERLOADING(SeqDenseMatrix,operator*)
SEQ_MATRIX_TIMES_OVERLOADING(SeqSparseMatrix,operator*)
#undef SEQ_MATRIX_TIMES_OVERLOADING

/*!
 *  \brief Operator overloading for addition or soustraction operations between two dsitributed matrices
 *  
 *  Possible operations between two matrices are: \n
 *  - Addition: element wise addition between two matrices
 *  - Soustraction: element wise soustraction between two matrices
 *
 *  \param MATRIXTYPE Type of the matrix
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Matrix containing the resulting operation
 */
#define DIST_MATRIX_ADD_MINUS_OVERLOADING(MATRIXTYPE,OPERATOR,OPERATION) \
	template <typename DataTypeLHS, typename MatrixImplLHS, typename DataTypeRHS, typename MatrixImplRHS> \
		static MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> OPERATOR(const MATRIXTYPE<DataTypeLHS,MatrixImplLHS> & _matrixLHS, const MATRIXTYPE<DataTypeRHS,MatrixImplRHS> & _matrixRHS) \
	{																																			\
		const int lNbRows = _matrixLHS.getNbRows();								\
		const int lNbCols = _matrixLHS.getNbCols();								\
		const int rNbRows = _matrixRHS.getNbRows();								\
		const int rNbCols = _matrixRHS.getNbCols();								\
		MatrixOperationsChecking::checkCompatibleDimensions(lNbRows,rNbRows); \
		MatrixOperationsChecking::checkCompatibleDimensions(lNbCols,rNbCols); \
		const int lGlobalNbRows = _matrixLHS.getGlobalNbRows();		\
		const int lGlobalNbCols = _matrixLHS.getGlobalNbCols();		\
		const int rGlobalNbRows = _matrixRHS.getGlobalNbRows();		\
		const int rGlobalNbCols = _matrixRHS.getGlobalNbCols();		\
		MatrixOperationsChecking::checkCompatibleDimensions(lGlobalNbRows,rGlobalNbRows); \
		MatrixOperationsChecking::checkCompatibleDimensions(lGlobalNbCols,rGlobalNbCols); \
		const int* lMapping = _matrixLHS.getMapping();							\
		const int* rMapping = _matrixRHS.getMapping();							\
		MatrixOperationsChecking::checkCompatibleMapping(lNbRows,lNbCols,lMapping,rMapping); \
		MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> tmpMatrix(_matrixLHS.getMatrixGraph()+_matrixRHS.getMatrixGraph()); \
		for(int i=0;i<lNbRows;++i)																	\
		{																																		\
			for(int j=0;j<lNbCols;++j)																\
			{																																	\
				tmpMatrix(i,j) = _matrixLHS(i,j) OPERATION _matrixRHS(i,j);			\
			}																																	\
		}																																		\
		return tmpMatrix;																										\
	}																																			\

DIST_MATRIX_ADD_MINUS_OVERLOADING(DistDenseMatrix,operator+,+)
DIST_MATRIX_ADD_MINUS_OVERLOADING(DistDenseMatrix,operator-,-)

DIST_MATRIX_ADD_MINUS_OVERLOADING(DistSparseMatrix,operator+,+)
DIST_MATRIX_ADD_MINUS_OVERLOADING(DistSparseMatrix,operator-,-)
#undef DIST_MATRIX_ADD_MINUS_OVERLOADING

/*!
 *  \brief Operator overloading for multiplication operations between two dense distributed matrices
 *  
 *  \param MATRIXTYPE Type of the matrix
 *  \param OPERATOR Operator
 *
 *  \return Matrix containing the resulting operation
 */
#define DIST_MATRIX_TIMES_OVERLOADING(MATRIXTYPE,OPERATOR)							\
	template <typename DataTypeLHS, typename MatrixImplLHS, typename DataTypeRHS, typename MatrixImplRHS> \
		static MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> OPERATOR(const MATRIXTYPE<DataTypeLHS,MatrixImplLHS> & _matrixLHS, const MATRIXTYPE<DataTypeRHS,MatrixImplRHS> & _matrixRHS) \
	{																																			\
		const int lNbCols = _matrixLHS.getGlobalNbCols();					\
		const int rNbRows = _matrixRHS.getGlobalNbRows();					\
		MatrixOperationsChecking::checkCompatibleDimensions(lNbCols,rNbRows); \
		MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> tmpMatrix(_matrixLHS.getMatrixGraph()*_matrixRHS.getMatrixGraph()); \
		BLAS::gemm(_matrixLHS,_matrixRHS,tmpMatrix);												\
		return tmpMatrix;																										\
	}																																			\
				
DIST_MATRIX_TIMES_OVERLOADING(DistDenseMatrix,operator*)
#undef DIST_MATRIX_TIMES_OVERLOADING

/*!
 *  \brief Operator overloading for multiplication operations between two sparse distributed matrices
 *  
 *  \param MATRIXTYPE Type of the matrix
 *  \param OPERATOR Operator
 *
 *  \return Matrix containing the resulting operation
 */
#define DIST_SPARSEMATRIX_TIMES_OVERLOADING(MATRIXTYPE,OPERATOR)				\
	template <typename DataTypeLHS, typename MatrixImplLHS, typename DataTypeRHS, typename MatrixImplRHS> \
		static MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> OPERATOR(const MATRIXTYPE<DataTypeLHS,MatrixImplLHS> & _matrixLHS, const MATRIXTYPE<DataTypeRHS,MatrixImplRHS> & _matrixRHS) \
	{																																			\
		const int lNbCols = _matrixLHS.getGlobalNbCols();					\
		const int rNbRows = _matrixRHS.getGlobalNbRows();					\
		MatrixOperationsChecking::checkCompatibleDimensions(lNbCols,rNbRows); \
		MATRIXTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type, MatrixImplLHS> tmpMatrix; \
		BLAS::gemm(_matrixLHS,_matrixRHS,tmpMatrix);												\
		return tmpMatrix;																										\
	}																																			\
				
DIST_SPARSEMATRIX_TIMES_OVERLOADING(DistSparseMatrix,operator*)
#undef DIST_SPARSEMATRIX_TIMES_OVERLOADING
//@}

/*! \name Public Matrix-Scalar operator overloading
 *
 *  Public Matrix-Scalar operator overloading
 */
//@{

/*!
 *  \brief Operator overloading for operations between a matrix and a scalar
 *  
 *  Possible operations between a matrix and a scalar are: \n
 *  - Multiplication: element wise mulitplication of all elements of a matrix by a scalar
 *  - Division: element wise division of all elements of a matrix by a scalar
 *
 *  \param MATRIXTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Matrix containing the resulting operation
 */
#define MATRIX_SCALAR_OPERATOR_OVERLOADING(MATRIXTYPE,OPERATOR,OPERATION) \
	template<typename DataTypeLHS, typename MatrixImplLHS, typename DataTypeRHS> \
		MATRIXTYPE<DataTypeLHS,MatrixImplLHS> OPERATOR(const MATRIXTYPE<DataTypeLHS,MatrixImplLHS> & _matrixLHS, const DataTypeRHS _scalarRHS) \
	{																																			\
		const int lNbRows = _matrixLHS.getNbRows();								\
		const int lNbCols = _matrixLHS.getNbCols();								\
		MATRIXTYPE<DataTypeLHS, MatrixImplLHS> tmpMatrix(_matrixLHS);				\
		for(int i=0;i<lNbRows;++i)																	\
		{																																		\
			for(int j=0;j<lNbCols;++j)																\
			{																																	\
				tmpMatrix(i,j) OPERATION _scalarRHS;														\
			}																																	\
		}																																		\
		return tmpMatrix;																										\
	}																																			\

MATRIX_SCALAR_OPERATOR_OVERLOADING(SeqDenseMatrix,operator*,*=)
MATRIX_SCALAR_OPERATOR_OVERLOADING(SeqDenseMatrix,operator/,/=)

MATRIX_SCALAR_OPERATOR_OVERLOADING(SeqSparseMatrix,operator*,*=)
MATRIX_SCALAR_OPERATOR_OVERLOADING(SeqSparseMatrix,operator/,/=)

MATRIX_SCALAR_OPERATOR_OVERLOADING(DistDenseMatrix,operator*,*=)
MATRIX_SCALAR_OPERATOR_OVERLOADING(DistDenseMatrix,operator/,/=)

MATRIX_SCALAR_OPERATOR_OVERLOADING(DistSparseMatrix,operator*,*=)
MATRIX_SCALAR_OPERATOR_OVERLOADING(DistSparseMatrix,operator/,/=)
#undef MATRIX_SCALAR_OPERATOR_OVERLOADING

/*!
 *  \brief Operator overloading for operations between a scalar and a matrix
 *  
 *  Possible operations between a matrix and a scalar are: \n
 *  - Multiplication: element wise mulitplication of all elements of a matrix by a scalar
 *  - Division: element wise division of all elements of a matrix by a scalar
 *
 *  \param MATRIXTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Matrix containing the resulting operation
 */
#define SCALAR_MATRIX_OPERATOR_OVERLOADING(MATRIXTYPE,OPERATOR,OPERATION) \
	template<typename DataTypeLHS, typename DataTypeRHS, typename MatrixImplRHS> \
		MATRIXTYPE<DataTypeRHS,MatrixImplRHS> OPERATOR(const DataTypeLHS _scalarLHS, const MATRIXTYPE<DataTypeRHS,MatrixImplRHS> & _matrixRHS) \
	{																																			\
		const int rNbRows = _matrixRHS.getNbRows();								\
		const int rNbCols = _matrixRHS.getNbCols();								\
		MATRIXTYPE<DataTypeRHS, MatrixImplRHS> tmpMatrix(_matrixRHS);				\
		for(int i=0;i<rNbRows;++i)																	\
		{																																		\
			for(int j=0;j<rNbCols;++j)																\
			{																																	\
				tmpMatrix(i,j) OPERATION _scalarLHS;														\
			}																																	\
		}																																		\
		return tmpMatrix;																										\
	}																																			\

SCALAR_MATRIX_OPERATOR_OVERLOADING(SeqDenseMatrix,operator*,*=)

SCALAR_MATRIX_OPERATOR_OVERLOADING(SeqSparseMatrix,operator*,*=)

SCALAR_MATRIX_OPERATOR_OVERLOADING(DistDenseMatrix,operator*,*=)

SCALAR_MATRIX_OPERATOR_OVERLOADING(DistSparseMatrix,operator*,*=)
#undef SCALAR_MATRIX_OPERATOR_OVERLOADING
//@}

/*! \name Public Matrix-Matrix comparator overloading
 *
 *  Public Matrix-Matrix comparator overloading
 */
//@{
/*!
 *  \brief Operator overloading for comparison between two matrices
 *
 *  \tparam DataTypeLHS Data type of the LHS matrix
 *  \tparam MatrixTypeLHS Type of the LHS matrix
 *  \tparam MatrixImplLHS The actual type of the LHS matrix
 *  \tparam DataTypeRHS Data type of the RHS matrix
 *  \tparam MatrixTypeRHS Type of the RHS matrix
 *  \tparam MatrixImplRHS The actual type of the RHS matrix
 *
 *  \param[in] _matrixLHS First matrix
 *  \param[in] _matrixRHS Second matrix
 *
 *  \return 1 if the matrices are equals, 0 otherwise
 */
template<class DataTypeLHS, template<class MatrixDataTypeLHS, class MatrixImplTypeLHS> class MatrixTypeLHS, template<class MatrixDataTypeLHS> class MatrixImplLHS, class DataTypeRHS, template<class MatrixDataTypeRHS, class MatrixImplTypeRHS> class MatrixTypeRHS, template<class MatrixDataTypeRHS> class MatrixImplRHS>
	int operator==(const MatrixTypeLHS<DataTypeLHS, MatrixImplLHS<DataTypeLHS> >& _matrixLHS, const MatrixTypeRHS<DataTypeRHS, MatrixImplRHS<DataTypeRHS> >& _matrixRHS)
{
	const int nbRowsLHS = _matrixLHS.getNbRows();
	const int nbColsLHS = _matrixLHS.getNbCols();
	const int nbRowsRHS = _matrixRHS.getNbRows();
	const int nbColsRHS = _matrixRHS.getNbCols();
	if(nbRowsLHS!=nbRowsRHS || nbColsLHS!=nbColsRHS)
		return 0;
	for(int i=0;i<nbRowsLHS;++i)
	{
		for(int j=0;j<nbColsLHS;++j)
		{
			if(_matrixLHS(i,j)!=_matrixRHS(i,j))
				return 0;
		}
	}
	return 1;
}
//@}
YALLA_END_NAMESPACE
#endif
