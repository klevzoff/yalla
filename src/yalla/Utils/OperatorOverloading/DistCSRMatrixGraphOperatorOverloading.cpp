#include "MatrixGraphOperatorOverloading.h"

/*!
 *  \file DistCSRMatrixGraphOperatorOverloading.cpp
 *  \brief Implementation of operator overloading for distributed CSR matrix graph
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_BEGIN_NAMESPACE(yalla)
/*! \brief Build the graph of a distributed CSR matrix for the addition of two distributed CSR matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
DistCSRMatrixGraph operator+(const DistCSRMatrixGraph& _graphLHS, const DistCSRMatrixGraph& _graphRHS)
{
	const int nbRowsLHS = _graphLHS.getNbRows();
	const int nbColsLHS = _graphLHS.getNbCols();
	const int nbNnzLHS = _graphLHS.getNnz();
	const int nbNnzRHS = _graphRHS.getNnz();
	const int nbGlobalRows = _graphLHS.getGlobalNbRows();
	const int nbGlobalCols = _graphLHS.getGlobalNbCols();
	const int* mapping = _graphLHS.getMapping();
	std::vector<int> rowPtr;
	std::vector<int> colPtr;
	rowPtr.reserve(nbNnzLHS+nbNnzRHS);
	colPtr.reserve(nbNnzLHS+nbNnzRHS);
	int nbNnz = 0;
	for(int i=0;i<nbRowsLHS;++i)
	{
		for(int j=0;j<nbColsLHS;++j)
		{
			if(_graphLHS(i,j) || _graphRHS(i,j))
			{
				++nbNnz;
				rowPtr.push_back(i);
				colPtr.push_back(j);
			}
		}
	}
	return DistCSRMatrixGraph(nbRowsLHS, nbColsLHS, nbNnz, nbGlobalRows, nbGlobalCols, mapping, rowPtr, colPtr);
}

/*! \brief Build the graph of a distributed CSR matrix for the soustraction of two distributed CSR matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
DistCSRMatrixGraph operator-(const DistCSRMatrixGraph& _graphLHS, const DistCSRMatrixGraph& _graphRHS)
{
	const int nbRowsLHS = _graphLHS.getNbRows();
	const int nbColsLHS = _graphLHS.getNbCols();
	const int nbNnzLHS = _graphLHS.getNnz();
	const int nbNnzRHS = _graphRHS.getNnz();
	const int nbGlobalRows = _graphLHS.getGlobalNbRows();
	const int nbGlobalCols = _graphLHS.getGlobalNbCols();
	const int* mapping = _graphLHS.getMapping();
	std::vector<int> rowPtr;
	std::vector<int> colPtr;
	rowPtr.reserve(nbNnzLHS+nbNnzRHS);
	colPtr.reserve(nbNnzLHS+nbNnzRHS);
	int nbNnz = 0;
	for(int i=0;i<nbRowsLHS;++i)
	{
		for(int j=0;j<nbColsLHS;++j)
		{
			if(_graphLHS(i,j) || _graphRHS(i,j))
			{
				++nbNnz;
				rowPtr.push_back(i);
				colPtr.push_back(j);
			}
		}
	}
	return DistCSRMatrixGraph(nbRowsLHS, nbColsLHS, nbNnz, nbGlobalRows, nbGlobalCols, mapping, rowPtr, colPtr);
}

YALLA_END_NAMESPACE
