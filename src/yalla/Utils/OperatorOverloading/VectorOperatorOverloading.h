#pragma once
#ifndef yalla_vector_operator_overloading_h
#define yalla_vector_operator_overloading_h

/*!
 *  \file VectorOperatorOverloading.h
 *  \brief Operator overloading for vectors
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/Promote.h"
#include "yalla/Utils/Exceptions/MatrixDimensionsCompatibles.h"
#include "yalla/Utils/BLAS/BLAS.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class VectorOperationsChecking
 *  \brief Check vector operations consistency
 */
class VectorOperationsChecking
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef VectorOperationsChecking this_type;
	//@}
/*! \brief Check if two vectors dimensions are compatibles
 *  
 *  \param[in] _dimLHS LHS dimension
 *  \param[in] _dimRHS RHS dimension
 *
 *  \exception MatrixDimensionsIncompatibles Matrices dimensions are incompatibles
 */
  static void checkCompatibleDimensions(const int _dimLHS, const int _dimRHS)
	{
#ifdef _DEBUG
		try
		{
			if(!MatrixDimensionsCompatibles::Check(_dimLHS,_dimRHS))
				throw MatrixDimensionsIncompatibles(__FILE__,__LINE__);
		}
		catch (MatrixDimensionsIncompatibles &matrixDimIncompatibles)
		{
			YALLA_ERR(matrixDimIncompatibles.what());
		}
#endif
	}

/*! \brief Check if two vectors mapping are compatibles
 *  
 *  \param[in] _size Size of the mapping
 *  \param[in] _mappingLHS LHS mapping
 *  \param[in] _mappingRHS RHS mapping
 */
	static void checkCompatibleMapping(const int _size, const int* _mappingLHS, const int* _mappingRHS)
	{
#ifdef _DEBUG
		for(int i=0;i<_size;++i)
		{
			if(_mappingLHS[i]!=_mappingRHS[i])
			{
				YALLA_ERR("Error, differents mappings");
			}
		}
#endif
	}
};

/*! \name Public Vector-Vector operator overloading
 *
 *  Public Vector-Vector operator overloading
 */
//@{

/*!
 *  \brief Operator overloading for addition or soustraction operations between two sequential vectors
 *  
 *  Possible operations between two vectors are: \n
 *  - Addition: element wise addition between two vectors
 *  - Soustraction: element wise soustraction between two vectors
 *
 *  \param VECTORTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *  \param ALPHA Scale factor
 *
 *  \return Vector containing the resulting operation
 */
#define SEQ_VECTOR_VECTOR_ADD_MINUS_OPERATOR_OVERLOADING(VECTORTYPE,OPERATOR,OPERATION,ALPHA) \
  template<typename DataTypeLHS, typename DataTypeRHS>									\
		VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> OPERATOR(const VECTORTYPE<DataTypeLHS>& _vectorLHS, const VECTORTYPE<DataTypeRHS> & _vectorRHS) \
  {																																			\
    const int lNbRows = _vectorLHS.getNbRows();								\
    const int lNbCols = _vectorLHS.getNbCols();								\
    const int rNbRows = _vectorRHS.getNbRows();								\
    const int rNbCols = _vectorRHS.getNbCols();								\
    VectorOperationsChecking::checkCompatibleDimensions(lNbRows,rNbRows); \
    VectorOperationsChecking::checkCompatibleDimensions(lNbCols,rNbCols); \
    VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(lNbRows,lNbCols); \
    BLAS::copy(_vectorLHS,tmpVector);																		\
		BLAS::axpy(static_cast<typename Promote<DataTypeLHS,DataTypeRHS>::data_type>(ALPHA),_vectorRHS,tmpVector); \
    return tmpVector;																										\
	}																																			\

SEQ_VECTOR_VECTOR_ADD_MINUS_OPERATOR_OVERLOADING(SeqVector,operator+,+,1)
SEQ_VECTOR_VECTOR_ADD_MINUS_OPERATOR_OVERLOADING(SeqVector,operator-,-,-1)
#undef SEQ_VECTOR_VECTOR_ADD_MINUS_OPERATOR_OVERLOADING

/*!
 *  \brief Operator overloading for element wise multiplication or division operations between two sequential vectors
 *  
 *  \param VECTORTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Vector containing the resulting operation
 */
#define SEQ_VECTOR_VECTOR_OPERATOR_OVERLOADING(VECTORTYPE,OPERATOR,OPERATION) \
  template<typename DataTypeLHS, typename DataTypeRHS>									\
		VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> OPERATOR(const VECTORTYPE<DataTypeLHS>& _vectorLHS, const VECTORTYPE<DataTypeRHS> & _vectorRHS) \
  {																																			\
    const int lNbRows = _vectorLHS.getNbRows();								\
    const int lNbCols = _vectorLHS.getNbCols();								\
    const int rNbRows = _vectorRHS.getNbRows();								\
    const int rNbCols = _vectorRHS.getNbCols();								\
    VectorOperationsChecking::checkCompatibleDimensions(lNbRows,rNbRows); \
    VectorOperationsChecking::checkCompatibleDimensions(lNbCols,rNbCols); \
    const int relevantDimension = _vectorLHS.getSize();				\
    VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(lNbRows,lNbCols); \
    for(int i=0;i<relevantDimension;++i)												\
    {																																		\
			tmpVector(i) = _vectorLHS(i) OPERATION _vectorRHS(i);							\
    }																																		\
    return tmpVector;																										\
	}																																			\

SEQ_VECTOR_VECTOR_OPERATOR_OVERLOADING(SeqVector,operator*,*)
SEQ_VECTOR_VECTOR_OPERATOR_OVERLOADING(SeqVector,operator/,/)
#undef SEQ_VECTOR_VECTOR_OPERATOR_OVERLOADING

/*!
 *  \brief Operator overloading for addition or soustraction operations between two distributed vectors
 *  
 *  Possible operations between two vectors are: \n
 *  - Addition: element wise addition between two vectors
 *  - Soustraction: element wise soustraction between two vectors
 *
 *  \param VECTORTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *  \param ALPHA Scale factor
 *
 *  \return Vector containing the resulting operation
 */
#define DIST_VECTOR_VECTOR_ADD_MINUS_OPERATOR_OVERLOADING(VECTORTYPE,OPERATOR,OPERATION,ALPHA) \
  template<typename DataTypeLHS, typename DataTypeRHS>									\
		static VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> OPERATOR(const VECTORTYPE<DataTypeLHS> & _vectorLHS, const VECTORTYPE<DataTypeRHS> & _vectorRHS) \
	{																																			\
    const int lNbRows = _vectorLHS.getNbRows();								\
    const int lNbCols = _vectorLHS.getNbCols();								\
    const int rNbRows = _vectorRHS.getNbRows();								\
    const int rNbCols = _vectorRHS.getNbCols();								\
    VectorOperationsChecking::checkCompatibleDimensions(lNbRows,rNbRows); \
    VectorOperationsChecking::checkCompatibleDimensions(lNbCols,rNbCols); \
    const int lGlobalNbRows = _vectorLHS.getGlobalNbRows();		\
    const int lGlobalNbCols = _vectorLHS.getGlobalNbCols();		\
    const int rGlobalNbRows = _vectorRHS.getGlobalNbRows();		\
    const int rGlobalNbCols = _vectorRHS.getGlobalNbCols();		\
    VectorOperationsChecking::checkCompatibleDimensions(lGlobalNbRows,rGlobalNbRows); \
    VectorOperationsChecking::checkCompatibleDimensions(lGlobalNbCols,rGlobalNbCols); \
    const int relevantDimension = _vectorLHS.getSize();				\
    const int* lMapping = _vectorLHS.getMapping();							\
    const int* rMapping = _vectorRHS.getMapping();							\
    VectorOperationsChecking::checkCompatibleMapping(relevantDimension,lMapping,rMapping); \
		const int lOwnSize = _vectorLHS.getOwnSize();							\
		const int rOwnSize = _vectorRHS.getOwnSize();							\
    VectorOperationsChecking::checkCompatibleDimensions(lOwnSize,rOwnSize);	\
    VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(lNbRows,lNbCols,lOwnSize,lGlobalNbRows,lGlobalNbCols,lMapping,_vectorLHS.getSync()); \
		BLAS::copy(_vectorLHS,tmpVector);																		\
		BLAS::axpy(static_cast<typename Promote<DataTypeLHS,DataTypeRHS>::data_type>(ALPHA),_vectorRHS,tmpVector); \
    return tmpVector;																										\
	}																																			\

DIST_VECTOR_VECTOR_ADD_MINUS_OPERATOR_OVERLOADING(DistVector,operator+,+,1)
DIST_VECTOR_VECTOR_ADD_MINUS_OPERATOR_OVERLOADING(DistVector,operator-,-,-1)
#undef DIST_VECTOR_VECTOR_ADD_MINUS_OPERATOR_OVERLOADING

/*!
 *  \brief Operator overloading for element wise multiplication or division operations between two distributed vectors
 *  
 *  \param VECTORTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Vector containing the resulting operation
 */
#define DIST_VECTOR_VECTOR_OPERATOR_OVERLOADING(VECTORTYPE,OPERATOR,OPERATION) \
  template<typename DataTypeLHS, typename DataTypeRHS>									\
		static VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> OPERATOR(const VECTORTYPE<DataTypeLHS> & _vectorLHS, const VECTORTYPE<DataTypeRHS> & _vectorRHS) \
	{																																			\
    const int lNbRows = _vectorLHS.getNbRows();								\
    const int lNbCols = _vectorLHS.getNbCols();								\
    const int rNbRows = _vectorRHS.getNbRows();								\
    const int rNbCols = _vectorRHS.getNbCols();								\
    VectorOperationsChecking::checkCompatibleDimensions(lNbRows,rNbRows); \
    VectorOperationsChecking::checkCompatibleDimensions(lNbCols,rNbCols); \
    const int lGlobalNbRows = _vectorLHS.getGlobalNbRows();		\
    const int lGlobalNbCols = _vectorLHS.getGlobalNbCols();		\
    const int rGlobalNbRows = _vectorRHS.getGlobalNbRows();		\
    const int rGlobalNbCols = _vectorRHS.getGlobalNbCols();		\
    VectorOperationsChecking::checkCompatibleDimensions(lGlobalNbRows,rGlobalNbRows); \
    VectorOperationsChecking::checkCompatibleDimensions(lGlobalNbCols,rGlobalNbCols); \
    const int relevantDimension = _vectorLHS.getSize();				\
    const int* lMapping = _vectorLHS.getMapping();							\
    const int* rMapping = _vectorRHS.getMapping();							\
    VectorOperationsChecking::checkCompatibleMapping(relevantDimension,lMapping,rMapping); \
		const int lOwnSize = _vectorLHS.getOwnSize();							\
		const int rOwnSize = _vectorRHS.getOwnSize();							\
    VectorOperationsChecking::checkCompatibleDimensions(lOwnSize,rOwnSize);	\
    VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(lNbRows,lNbCols,lOwnSize,lGlobalNbRows,lGlobalNbCols,lMapping,_vectorLHS.getSync()); \
    for(int i=0;i<relevantDimension;++i)												\
    {																																		\
			tmpVector(i) = _vectorLHS(i) OPERATION _vectorRHS(i);							\
    }																																		\
    return tmpVector;																										\
	}																																			\

DIST_VECTOR_VECTOR_OPERATOR_OVERLOADING(DistVector,operator*,*)
DIST_VECTOR_VECTOR_OPERATOR_OVERLOADING(DistVector,operator/,/)
#undef DIST_VECTOR_VECTOR_OPERATOR_OVERLOADING
//@}

/*! \name Public Vector-Scalar operator overloading
 *
 *  Public Vector-Scalar operator overloading
 */
//@{

/*!
 *  \brief Operator overloading for operations between a vector and a scalar
 *  
 *  Possible operations between a vector and a scalar are: \n
 *  - Multiplication: element wise multiplication of all elements of a vector by a scalar
 *
 *  \param VECTORTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Vector containing the resulting operation
 */
#define VECTOR_TIMES_SCALAR_OPERATOR_OVERLOADING(VECTORTYPE,OPERATOR,OPERATION) \
  template<typename DataTypeLHS, typename DataTypeRHS>									\
		VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> OPERATOR(const VECTORTYPE<DataTypeLHS>& _vectorLHS, const DataTypeRHS & _scalarRHS) \
  {																																			\
    VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(_vectorLHS); \
		BLAS::scal(_scalarRHS,tmpVector);																		\
    return tmpVector;																										\
  } ;

VECTOR_TIMES_SCALAR_OPERATOR_OVERLOADING(SeqVector,operator*,*=)

VECTOR_TIMES_SCALAR_OPERATOR_OVERLOADING(DistVector,operator*,*=)
#undef VECTOR_TIMES_SCALAR_OPERATOR_OVERLOADING

/*!
 *  \brief Operator overloading for operations between a vector and a scalar
 *  
 *  Possible operations between a vector and a scalar are: \n
 *  - Division: element wise division of all elements of a vector by a scalar
 *
 *  \param VECTORTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Vector containing the resulting operation
 */
#define VECTOR_DIV_SCALAR_OPERATOR_OVERLOADING(VECTORTYPE,OPERATOR,OPERATION) \
  template<typename DataTypeLHS, typename DataTypeRHS>									\
		VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> OPERATOR(const VECTORTYPE<DataTypeLHS>& _vectorLHS, const DataTypeRHS & _scalarRHS) \
  {																																			\
    VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(_vectorLHS); \
		BLAS::scal(1./_scalarRHS,tmpVector);																\
    return tmpVector;																										\
  } ;

VECTOR_DIV_SCALAR_OPERATOR_OVERLOADING(SeqVector,operator/,/=)

VECTOR_DIV_SCALAR_OPERATOR_OVERLOADING(DistVector,operator/,/=)
#undef VECTOR_DIV_SCALAR_OPERATOR_OVERLOADING

/*!
 *  \brief Operator overloading for operations between a scalar and a vector
 *  
 *  Possible operations between a vector and a scalar are: \n
 *  - Multiplication: element wise multiplication of all elements of a vector by a scalar
 *
 *  \param VECTORTYPE Type of the vector
 *  \param OPERATOR Operator
 *  \param OPERATION Function associated with the operator
 *
 *  \return Vector containing the resulting operation
 */
#define SCALAR_TIMES_VECTOR_OPERATOR_OVERLOADING(VECTORTYPE,OPERATOR,OPERATION) \
  template<typename DataTypeLHS, typename DataTypeRHS>									\
		VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> OPERATOR(const DataTypeLHS &_scalarLHS, const VECTORTYPE<DataTypeRHS>& _vectorRHS) \
  {																																			\
		VECTORTYPE<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(_vectorRHS); \
		BLAS::scal(_scalarLHS,tmpVector);																		\
    return tmpVector;																										\
	}																																			\

SCALAR_TIMES_VECTOR_OPERATOR_OVERLOADING(SeqVector,operator*,*=)

SCALAR_TIMES_VECTOR_OPERATOR_OVERLOADING(DistVector,operator*,*=)
#undef SCALAR_TIMES_VECTOR_OPERATOR_OVERLOADING
//@}

/*! \name Public Vector-Matrix operator overloading
 *
 *  Public Vector-Matrix operator overloading
 */
//@{
/*!
 *  \brief Operator overloading for operations between a sequential matrix and a sequential vector
 *  
 *  Possible operations between a sequential matrix and a sequential vector are: \n
 *  - Multiplication: mulitplication of a vector and a matrix
 *
 *  \param MATRIXTYPE Type of the matrix
 *
 *  \return Vector containing the resulting operation
 */
#define MATRIX_VECTOR_OPERATOR_OVERLOADING(MATRIXTYPE)									\
  template<typename DataTypeLHS, typename MatrixImplLHS, typename DataTypeRHS> \
		SeqVector<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> operator*(const MATRIXTYPE<DataTypeLHS, MatrixImplLHS> & _matrixLHS, const SeqVector<DataTypeRHS>& _vectorRHS) \
  {																																			\
    const int lNbRows = _matrixLHS.getNbRows();								\
    const int lNbCols = _matrixLHS.getNbCols();								\
    const int rNbRows =_vectorRHS.getNbRows();									\
    const int rNbCols =_vectorRHS.getNbCols();									\
    VectorOperationsChecking::checkCompatibleDimensions(lNbCols,rNbRows); \
    SeqVector<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(lNbRows,rNbCols); \
		BLAS::gemv(1.,_matrixLHS,_vectorRHS,1.,tmpVector);									\
    return tmpVector;																										\
	}																																			\

MATRIX_VECTOR_OPERATOR_OVERLOADING(SeqDenseMatrix)
MATRIX_VECTOR_OPERATOR_OVERLOADING(SeqSparseMatrix)
#undef MATRIX_VECTOR_OPERATOR_OVERLOADING

/*!
 *  \brief Operator overloading for operations between a sequential vector and a sequential matrix
 *  
 *  Possible operations between a sequential vector and a sequential matrix are: \n
 *  - Multiplication: mulitplication of a vector and a matrix
 *
 *  \param MATRIXTYPE Type of the matrix
 *
 *  \return Vector containing the resulting operation
 */
#define VECTOR_MATRIX_OPERATOR_OVERLOADING(MATRIXTYPE)									\
  template<typename DataTypeLHS, typename DataTypeRHS, typename MatrixImplRHS> \
		SeqVector<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> operator*(const SeqVector<DataTypeLHS>& _vectorLHS, const MATRIXTYPE<DataTypeRHS, MatrixImplRHS> & _matrixRHS) \
  {																																			\
    return _matrixRHS * _vectorLHS;																			\
  }																																			\

VECTOR_MATRIX_OPERATOR_OVERLOADING(SeqDenseMatrix)
VECTOR_MATRIX_OPERATOR_OVERLOADING(SeqSparseMatrix)
#undef VECTOR_MATRIX_OPERATOR_OVERLOADING

/*!
 *  \brief Operator overloading for operations between a distributed matrix and a distributed vector
 *  
 *  Possible operations between a distributed matrix and a distributed vector are: \n
 *  - Multiplication: mulitplication of a vector and a matrix
 *
 *  \param MATRIXTYPE Type of the matrix
 *
 *  \return Vector containing the resulting operation
 */
#define DIST_MATRIX_VECTOR_OPERATOR_OVERLOADING(MATRIXTYPE)							\
  template<typename DataTypeLHS, typename MatrixImplLHS, typename DataTypeRHS> \
		DistVector<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> operator*(const MATRIXTYPE<DataTypeLHS, MatrixImplLHS> & _matrixLHS, const DistVector<DataTypeRHS>& _vectorRHS) \
  {																																			\
    const int lGlobalNbCols = _matrixLHS.getGlobalNbCols();		\
    const int rNbRows = _vectorRHS.getNbRows();								\
    const int rNbCols = _vectorRHS.getNbCols();								\
		const int rGlobalSize = _vectorRHS.getGlobalSize();				\
    VectorOperationsChecking::checkCompatibleDimensions(lGlobalNbCols,rGlobalSize); \
		const int ownSize = _vectorRHS.getOwnSize();								\
    DistVector<typename Promote<DataTypeLHS,DataTypeRHS>::data_type> tmpVector(rNbRows,rNbCols,ownSize,_vectorRHS.getGlobalNbRows(),_vectorRHS.getGlobalNbCols(),_vectorRHS.getMapping(),_vectorRHS.getSync()); \
		BLAS::gemv(1.,_matrixLHS,_vectorRHS,1.,tmpVector);									\
    return tmpVector;																										\
	}																																			\

DIST_MATRIX_VECTOR_OPERATOR_OVERLOADING(DistDenseMatrix)
DIST_MATRIX_VECTOR_OPERATOR_OVERLOADING(DistSparseMatrix)
#undef DIST_MATRIX_VECTOR_OPERATOR_OVERLOADING
//@}

/*! \name Public Vector-Vector comparator overloading
 *
 *  Public Vector-Vector comparator overloading
 */
//@{
/*!
 *  \brief Operator overloading for comparison between two vectors
 *
 *  \tparam DataTypeLHS Data type of the LHS vector
 *  \tparam VectorTypeLHS Type of the LHS vector
 *  \tparam DataTypeRHS Data type of the RHS vector
 *  \tparam VectorTypeRHS Type of the RHS vector
 *
 *  \param[in] _vectorLHS First vector
 *  \param[in] _vectorRHS Second vector
 *
 *  \return 1 if the vectors are equals, 0 otherwise
 */
template<class DataTypeLHS, template<class VectorDataTypeLHS> class VectorTypeLHS, class DataTypeRHS, template<class VectorDataTypeRHS> class VectorTypeRHS>
int operator==(const VectorTypeLHS<DataTypeLHS>& _vectorLHS, const VectorTypeRHS<DataTypeRHS>& _vectorRHS)
{
	const int nbRowsLHS = _vectorLHS.getNbRows();
	const int nbColsLHS = _vectorLHS.getNbCols();
	const int nbRowsRHS = _vectorRHS.getNbRows();
	const int nbColsRHS = _vectorRHS.getNbCols();
	if(nbRowsLHS!=nbRowsRHS || nbColsLHS!=nbColsRHS)
		return 0;
	for(int i=0;i<nbRowsLHS;++i)
	{
		if(_vectorLHS(i)!=_vectorRHS(i))
			return 0;
	}
	return 1;
}
//@}
YALLA_END_NAMESPACE
#endif
