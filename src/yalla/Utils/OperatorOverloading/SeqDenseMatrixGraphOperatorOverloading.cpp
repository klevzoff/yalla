#include "MatrixGraphOperatorOverloading.h"

/*!
 *  \file SeqDenseMatrixGraphOperatorOverloading.cpp
 *  \brief Implementation of operator overloading for sequential dense matrix graph
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_BEGIN_NAMESPACE(yalla)
/*! \brief Build the graph of a sequential dense matrix for the addition of two sequential dense matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
SeqDenseMatrixGraph operator+(const SeqDenseMatrixGraph& _graphLHS, const SeqDenseMatrixGraph& _graphRHS)
{
	return SeqDenseMatrixGraph(_graphLHS);
}

/*! \brief Build the graph of a sequential dense matrix for the soustraction of two sequential dense matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
SeqDenseMatrixGraph operator-(const SeqDenseMatrixGraph& _graphLHS, const SeqDenseMatrixGraph& _graphRHS)
{
	return SeqDenseMatrixGraph(_graphLHS);
}

/*! \brief Build the graph of a sequential dense matrix for the division of two sequential dense matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
SeqDenseMatrixGraph operator*(const SeqDenseMatrixGraph& _graphLHS, const SeqDenseMatrixGraph& _graphRHS)
{
	return SeqDenseMatrixGraph(_graphLHS.getNbRows(),_graphRHS.getNbCols(),_graphLHS.getNnz());
}
YALLA_END_NAMESPACE
