#include "MatrixGraphOperatorOverloading.h"

/*!
 *  \file DistDenseMatrixGraphOperatorOverloading.cpp
 *  \brief Implementation of operator overloading for distributed dense matrix graph
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_BEGIN_NAMESPACE(yalla)
/*! \brief Build the graph of a distributed dense matrix for the addition of two distributed dense matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
DistDenseMatrixGraph operator+(const DistDenseMatrixGraph& _graphLHS, const DistDenseMatrixGraph& _graphRHS)
{
	return DistDenseMatrixGraph(_graphLHS);
}

/*! \brief Build the graph of a distributed dense matrix for the soustraction of two distributed dense matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
DistDenseMatrixGraph operator-(const DistDenseMatrixGraph& _graphLHS, const DistDenseMatrixGraph& _graphRHS)
{
	return DistDenseMatrixGraph(_graphLHS);
}

/*! \brief Build the graph of a distributed dense matrix for the multiplication of two distributed dense matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
DistDenseMatrixGraph operator*(const DistDenseMatrixGraph& _graphLHS, const DistDenseMatrixGraph& _graphRHS)
{
	return DistDenseMatrixGraph(_graphLHS.getNbRows(), _graphRHS.getNbCols(), _graphLHS.getNnz(), _graphLHS.getGlobalNbRows(), _graphRHS.getGlobalNbCols(), const_cast<int*>(_graphRHS.getMapping()));
}
YALLA_END_NAMESPACE
