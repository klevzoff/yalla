#pragma once
#ifndef yalla_matrix_graph_operator_overloading_h
#define yalla_matrix_graph_operator_overloading_h

/*!
*  \file MatrixGraphOperatorOverloading.h
*  \brief Operator overloading for matrices graph
*  \date 12/27/2013
*  \author Xavier TUNC
*  \version 1.0
*/

#include <iostream>
#include "yalla/Utils/MatrixGraphs/SeqDenseMatrixGraph.h"
#include "yalla/Utils/MatrixGraphs/SeqCSRMatrixGraph.h"
#include "yalla/Utils/MatrixGraphs/DistDenseMatrixGraph.h"
#include "yalla/Utils/MatrixGraphs/DistCSRMatrixGraph.h"
/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
  /*!
  *  \brief Operator overloading for matrices graph
  *  
  *  \param MATRIXGRAPHTYPE Type of the matrix
  *  \param OPERATOR Operator
  *
  *  \return The new matrix graph
  */
#define MATRIX_GRAPH_OPERATOR_OVERLOADING(MATRIXGRAPHTYPE, OPERATOR) \
  MATRIXGRAPHTYPE OPERATOR(const MATRIXGRAPHTYPE& _graphLHS, const MATRIXGRAPHTYPE& _graphRHS);

  MATRIX_GRAPH_OPERATOR_OVERLOADING(SeqDenseMatrixGraph, operator+)
    MATRIX_GRAPH_OPERATOR_OVERLOADING(SeqDenseMatrixGraph, operator-)
    MATRIX_GRAPH_OPERATOR_OVERLOADING(SeqDenseMatrixGraph, operator*)

    MATRIX_GRAPH_OPERATOR_OVERLOADING(SeqCSRMatrixGraph, operator+)
    MATRIX_GRAPH_OPERATOR_OVERLOADING(SeqCSRMatrixGraph, operator-)
    MATRIX_GRAPH_OPERATOR_OVERLOADING(SeqCSRMatrixGraph, operator*)

    MATRIX_GRAPH_OPERATOR_OVERLOADING(DistDenseMatrixGraph, operator+)
    MATRIX_GRAPH_OPERATOR_OVERLOADING(DistDenseMatrixGraph, operator-)
    MATRIX_GRAPH_OPERATOR_OVERLOADING(DistDenseMatrixGraph, operator*)

		MATRIX_GRAPH_OPERATOR_OVERLOADING(DistCSRMatrixGraph, operator+)
    MATRIX_GRAPH_OPERATOR_OVERLOADING(DistCSRMatrixGraph, operator-)
#undef MATRIX_GRAPH_OPERATOR_OVERLOADING
YALLA_END_NAMESPACE
#endif
