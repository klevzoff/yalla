#include "MatrixGraphOperatorOverloading.h"

/*!
 *  \file SeqCSRMatrixGraphOperatorOverloading.cpp
 *  \brief Implementation of operator overloading for sequential CSR matrix graph
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

YALLA_BEGIN_NAMESPACE(yalla)
/*! \brief Build the graph of a sequential CSR matrix for the addition of two sequential CSR matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
SeqCSRMatrixGraph operator+(const SeqCSRMatrixGraph& _graphLHS, const SeqCSRMatrixGraph& _graphRHS)
{
	const int nbRowsLHS = _graphLHS.getNbRows();
	const int nbColsLHS = _graphLHS.getNbCols();
	const int nbNnzLHS = _graphLHS.getNnz();
	const int nbNnzRHS = _graphRHS.getNnz();
	std::vector<int> rowPtr;
	std::vector<int> colPtr;
	rowPtr.reserve(nbNnzLHS+nbNnzRHS);
	colPtr.reserve(nbNnzLHS+nbNnzRHS);
	int nbNnz = 0;
	for(int i=0;i<nbRowsLHS;++i)
	{
		for(int j=0;j<nbColsLHS;++j)
		{
			if(_graphLHS(i,j) || _graphRHS(i,j))
			{
				++nbNnz;
				rowPtr.push_back(i);
				colPtr.push_back(j);
			}
		}
	}
	return SeqCSRMatrixGraph(nbRowsLHS, nbColsLHS, nbNnz, rowPtr, colPtr);
}

/*! \brief Build the graph of a sequential CSR matrix for the soustraction of two sequential CSR matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
SeqCSRMatrixGraph operator-(const SeqCSRMatrixGraph& _graphLHS, const SeqCSRMatrixGraph& _graphRHS)
{
	const int nbRowsLHS = _graphLHS.getNbRows();
	const int nbColsLHS = _graphLHS.getNbCols();
	const int nbNnzLHS = _graphLHS.getNnz();
	const int nbNnzRHS = _graphRHS.getNnz();
	std::vector<int> rowPtr;
	std::vector<int> colPtr;
	rowPtr.reserve(nbNnzLHS+nbNnzRHS);
	colPtr.reserve(nbNnzLHS+nbNnzRHS);
	int nbNnz = 0;
	for(int i=0;i<nbRowsLHS;++i)
	{
		for(int j=0;j<nbColsLHS;++j)
		{
			if(_graphLHS(i,j) || _graphRHS(i,j))
			{
				++nbNnz;
				rowPtr.push_back(i);
				colPtr.push_back(j);
			}
		}
	}
	return SeqCSRMatrixGraph(nbRowsLHS, nbColsLHS, nbNnz, rowPtr, colPtr);
}

/*! \brief Build the graph of a sequential CSR matrix for the multiplication of two sequential CSR matrices
 *  
 *  \param[in] _graphLHS The LHS graph
 *  \param[in] _graphRHS The RHS graph
 *
 *  \return The resulting new matrix graph
 */
SeqCSRMatrixGraph operator*(const SeqCSRMatrixGraph& _graphLHS, const SeqCSRMatrixGraph& _graphRHS)
{
	const int nbRowsLHS = _graphLHS.getNbRows();
	const int nbColsLHS = _graphLHS.getNbCols();
	const int nbNnzLHS = _graphLHS.getNnz();
	const int nbColsRHS = _graphRHS.getNbCols();
	const int nbNnzRHS = _graphRHS.getNnz();
	std::vector<int> rowPtr;
	std::vector<int> colPtr;
	rowPtr.reserve(nbNnzLHS+nbNnzRHS);
	colPtr.reserve(nbNnzLHS+nbNnzRHS);
	int nbNnz = 0;

	for(int i=0;i<nbRowsLHS;++i)
	{
		for(int j=0;j<nbColsRHS;++j)
		{
			for(int k=0;k<nbColsLHS;++k)
			{
				if(_graphLHS(i,k) * _graphRHS(k,j))
				{
					++nbNnz;
					rowPtr.push_back(i);
					colPtr.push_back(j);
					break;
				}
			}
		}
	}
	return SeqCSRMatrixGraph(nbRowsLHS, nbColsRHS, nbNnz, rowPtr, colPtr);
}
YALLA_END_NAMESPACE
