#pragma once
#ifndef yalla_singleton_dynamic_h
#define yalla_singleton_dynamic_h

/*!
 *  \file SingletonDynamic.h
 *  \brief Dynamic singleton
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "DefaultSingletonConcurrencyPolicy.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SingletonDynamic
 *  \brief Dynamic singleton
 * 
 *  \details
 *  This class implement a dynamic singleton following a policy-based design:
 *  - \tparam T The type of the object in the singleton
 *  - \tparam ConcurrencyPolicy The concurrency policy
 *
 *  From: http://stackoverflow.com/questions/3444445/c-singleton-template-class
 */
template< typename T, typename ConcurrencyPolicy = DefaultSingletonConcurrencyPolicy >
	class SingletonDynamic : public ConcurrencyPolicy
{
 public:
 //! \name Public Typedefs
 //@{
 //! \brief Type of the object in the singleton
 typedef T data_type;
 //! \brief Type of the concurrency policy
 typedef ConcurrencyPolicy concurrency_policy_type;
 //! \brief Type of this class
 typedef DefaultSingletonConcurrencyPolicy this_class;
 //@}
 private:
 //! \brief Unique instance of the object
 static T *pInstance_;
 //! \brief Flag indicating if the singleton object has been created
 static volatile bool flag_;
 protected:
 /*! \name Protected Constructors
	*
	*  Protected constructors of the class SingletonDynamic
	*/
 //@{

 /*! 
	*  \brief Default constructor.
	*
	*  \details
	*  Do nothing
	*/
 SingletonDynamic()
 {
	 ;
 }
 //@}

 private:
 /*! \name Private Constructors
	*
	*  Private constructors of the class SingletonDynamic
	*/
 //@{

 /*! 
	*  \brief Copy constructor
	*
	*  \details
	*  Disabled
	*
	*  \param[in] _singleton The singleton to copy
	*/
 SingletonDynamic(const SingletonDynamic& _singleton);
 //@}

 /*! \name Private assignment operator
	*
	*  Private assignment operator of the class SingletonDynamic
	*/
 //@{

 /*! 
	*  \brief Assignment operator
	*
	*  \details
	*  Disabled
	*
	*  \param[in] _singleton The singleton to copy
	*/
 SingletonDynamic &operator=(const SingletonDynamic& _singleton);
 //@}

 /*! \name Protected Destructor
	*
	*  Protected destructor of the class SingletonDynamic
	*/
 //@{

 protected:
 /*! 
	*  \brief Destructor.
	*
	*  \details
	*  Delete pointer the instance
	*/
 virtual ~SingletonDynamic() 
 {
	 delete SingletonDynamic< T, ConcurrencyPolicy >::pInstance_;
 }
 //@}

 /*! \name Public getter
	*
	*  Public getter of the class SingletonDynamic
	*/
 //@{
 public:
 /*! \brief Return the pointer instance
	*
	*  Call the protected method get_instance in charge of the pointer instance
	*
	*  \tparam The pointer type
	*/
 static T& instance()
 {
	 return *SingletonDynamic< T, ConcurrencyPolicy >::get_instance();
 }

 /*! \brief Return the const pointer instance
	*
	*  Call the protected method get_instance in charge of the pointer instance
	*
	*  \tparam The pointer type
	*/
 static const T& const_instance()
 {
	 return *SingletonDynamic< T, ConcurrencyPolicy >::get_instance();
 }
 //@}

 /*! \name Private getter
	*
	*  Private getter of the class SingletonDynamic
	*/
 //@{
 private:
 /*! \brief Creates or return the pointer instance
	*
	*  If the singleton doesn't exist yet, call the lock_mutex method of the concurrency policy specified, then creates the instance if necessary. \n
	*  The mutex is then unlocked, the initialization flag is set to true and the instance is then returned. \n
	*  If the singleton already exist, the instance is returned.
	*
	*  \tparam The pointer type
	*/
 static T *get_instance()
 {
	 if( SingletonDynamic< T, ConcurrencyPolicy >::flag_ == false )
	 {
		 ConcurrencyPolicy::lock_mutex();
		 
		 if( SingletonDynamic< T, ConcurrencyPolicy >::pInstance_ == NULL )
		 {
			 pInstance_ = new T();
		 }
		 
		 ConcurrencyPolicy::unlock_mutex();
		 
		 ConcurrencyPolicy::memory_barrier();
		 
		 SingletonDynamic< T, ConcurrencyPolicy >::flag_ = true;
		 
		 return SingletonDynamic< T, ConcurrencyPolicy >::pInstance_;
	 }
	 else
	 {
		 ConcurrencyPolicy::memory_barrier();

		 return SingletonDynamic< T, ConcurrencyPolicy >::pInstance_;
	 }
 }
};

//! \brief Initialize the singleton instance pointer
template< typename T, typename ConcurrencyPolicy >
	T *SingletonDynamic< T , ConcurrencyPolicy >::pInstance_ = 0;

//! \brief Initialize the singleton flag
template< typename T, typename ConcurrencyPolicy >
	volatile bool SingletonDynamic< T , ConcurrencyPolicy >::flag_ = false;

YALLA_END_NAMESPACE

#endif
