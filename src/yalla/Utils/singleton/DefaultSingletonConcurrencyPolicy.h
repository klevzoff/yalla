#pragma once
#ifndef yalla_default_singleton_concurrency_policy_h
#define yalla_default_singleton_concurrency_policy_h

/*!
 *  \file DefaultSingletonConcurrencyPolicy.h
 *  \brief Default concurrency policy for the generic singleton
 *  \date 12/27/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class DefaultSingletonConcurrencyPolicy
 *  \brief Default concurrency policy for a generic singleton
 * 
 *  \details 
 *  This class implement the default concurrency policy for a generic singleton. \n
 *  From: http://stackoverflow.com/questions/3444445/c-singleton-template-class
 */
struct DefaultSingletonConcurrencyPolicy
{
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef DefaultSingletonConcurrencyPolicy this_class;
	//@}
	/*! \brief Lock the mutex
	 *
	 *  Placeholder function for locking a mutex, thereby preventing access to other threads. This default implementation does not perform any function, the derived class must provide an implementation if this functionality is desired.
	 */
	static void lock_mutex() 
	{ 
		return;
	}
		
	/*! \brief Unlock the mutex
	 *
	 *  Placeholder function for unlocking a mutex, thereby allowing access to other threads. This default implementation does not perform any function, the derived class must provide an implementation if this functionality is desired.
	 */
	static void unlock_mutex()
	{
		return;
	}

	/*! \brief Memory barrier
	 *
   *  Placeholder function for executing a memory barrier instruction, thereby preventing the compiler from reordering read and writes across this boundary. This default implementation does not perform any function, the derived class must provide an implementation if this functionality is desired.
   */
  inline static void memory_barrier()
  {
    return;
  }
};
YALLA_END_NAMESPACE
#endif

