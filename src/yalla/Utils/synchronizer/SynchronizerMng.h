#pragma once
#ifndef yalla_synchronizer_mng_h
#define yalla_synchronizer_mng_h

/*!
 *  \file SynchronizerMng.h
 *  \brief Synchronizer manager
 *  \date 12/30/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <map>
#include <tuple>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/singleton/SingletonDynamic.h"
#include "yalla/Utils/ParUtils/GeneralSynchronizer.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
#if WITH_CPP11
/*!
 *  \class SynchronizerMng
 *  \brief Synchronizer manager using variadic templates
 * 
 *  \details
 *  This class allows to register and get back different synchronizers. \n
 *  These synchronizer are stored in a map where the key is defined by:
 *  - an Integer specifying the operation
 *  - a variadic templates tuple storing the operands
 *
 *  This way, one class only is used to store different communication maps for different operations
 *
 *  \tparam ...T The variadic templates
 */
template<typename...T>
class SynchronizerMng
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the key
	typedef std::pair<int, std::tuple<T...> > key_type;
	//! \brief Type of the synchronizer
	typedef GeneralSynchronizer* data_type;
	//! \brief Type of this class
	typedef SynchronizerMng<T...> this_type;
 private:
	//! \brief Map storing the key and the synchronizer
	std::map<key_type, const GeneralSynchronizer*> m_synchronizer;
 public:
	/*!
	 *  \brief Register a synchronizer for an operation
	 *  
	 *  \param[in] _key The key to register the synchronizer
	 *  \param[in] _sync The synchronizer
	 */
	void setValue(const key_type& _key, const GeneralSynchronizer* _sync)
	{
		m_synchronizer[_key] = _sync;
	}

	/*!
	 *  \brief Return the synchronizer associated with the key
	 *  
	 *  \param[in] _key The key of the synchronizer
	 *
	 *  \return The synchronizer
	 */
	const GeneralSynchronizer* getValue(const key_type& _key) const 
	{
		typename std::map<key_type, const GeneralSynchronizer*>::const_iterator it = m_synchronizer.find(_key);
		if(it!=m_synchronizer.end())
			return it->second;
		return 0;
	}
};

/*!
 *  \class SynchronizerSingleton
 *  \brief Singleton for synchronizer manager
 * 
 *  \details
 *  This class guarantee that one and only one instance of the SynchronizerMng exist in the code
 */
class SynchronizerSingleton
: public SingletonDynamic<SynchronizerSingleton>
{
	//! \brief Friend class
	friend class SingletonDynamic<SynchronizerSingleton>;
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the synchronizer for 1 operand operations
	typedef SynchronizerMng<void*> synchronizer_mng_1_type;
	//! \brief Type of the synchronizer for 2 operand operations
	typedef SynchronizerMng<void*,void*> synchronizer_mng_2_type;

	//! \brief Enum for the different operations
	enum CommMap
	{
		//! \brief Column sum operation
		col_sum_map = 0,
		//! \brief SPMM operation
		spmm_map = 1
	};
 private:
	//! \brief Synchronizer for 1 operand operations
	synchronizer_mng_1_type m_1_arguments;
	//! \brief Synchronizer for 2 operand operations
	synchronizer_mng_2_type m_2_arguments;
 private:
	/*! \name Private Constructors
	 *
	 *  Private constructors of the class SynchronizerSingleton
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Disabled
	 */
	SynchronizerSingleton() {}
	//@}

	/*! \name Private Destructor
	 *
	 *  Private destructor of the class SynchronizerSingleton
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Disabled
	 */
	~SynchronizerSingleton() {}

 public:
	/*!
	 *  \brief Register a synchronizer for a one operand operation
	 *  
	 *  \param[in] _key The key to register the synchronizer
	 *  \param[in] _sync The synchronizer
	 */
	void setValue( const synchronizer_mng_1_type::key_type& _key, const GeneralSynchronizer* _sync)
	{
		m_1_arguments.setValue(_key, _sync);
	}

	/*!
	 *  \brief Return the synchronizer associated with the key for one operand operation
	 *  
	 *  \param[in] _key The key of the synchronizer
	 *
	 *  \return The synchronizer
	 */
	const GeneralSynchronizer* getValue( const synchronizer_mng_1_type::key_type& _key) const
	{
		return m_1_arguments.getValue(_key);
	}

	/*!
	 *  \brief Register a synchronizer for a two operand operation
	 *  
	 *  \param[in] _key The key to register the synchronizer
	 *  \param[in] _sync The synchronizer
	 */
	void setValue( const synchronizer_mng_2_type::key_type& _key, const GeneralSynchronizer* _sync)
	{
		m_2_arguments.setValue(_key, _sync);
	}

	/*!
	 *  \brief Return the synchronizer associated with the key for two operand operation
	 *  
	 *  \param[in] _key The key of the synchronizer
	 *
	 *  \return The synchronizer
	 */
	const GeneralSynchronizer* getValue( const synchronizer_mng_2_type::key_type& _key) const
	{
		return m_2_arguments.getValue(_key);
	}
};
#else // We don't have variadic templates, so will have to duplicate class
/*!
 *  \class SynchronizerMng
 *  \brief Synchronizer manager for one operand
 * 
 *  \details
 *  This class allows to register and get back different synchronizers. \n
 *  These synchronizer are stored in a map where the key is defined by:
 *  - an Integer specifying the operation
 *  - a tuple storing the operands
 *
 *  \tparam T type of the operand
 */
template<typename T>
class SynchronizerMng1
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the key
	typedef std::pair<int, std::tuple<T> > key_type;
	//! \brief Type of the synchronizer
	typedef GeneralSynchronizer* data_type;
	//! \brief Type of this class
	typedef SynchronizerMng1<T> this_type;
 private:
	//! \brief Map storing the key and the synchronizer
	std::map<key_type, const GeneralSynchronizer*> m_synchronizer;
 public:
	/*!
	 *  \brief Register a synchronizer for an operation
	 *  
	 *  \param[in] _key The key to register the synchronizer
	 *  \param[in] _sync The synchronizer
	 */
	void setValue(const key_type& _key, const GeneralSynchronizer* _sync)
	{
		m_synchronizer[_key] = _sync;
	}

	/*!
	 *  \brief Return the synchronizer associated with the key
	 *  
	 *  \param[in] _key The key of the synchronizer
	 *
	 *  \return The synchronizer
	 */
	const GeneralSynchronizer* getValue(const key_type& _key) const 
	{
		typename std::map<key_type, const GeneralSynchronizer*>::const_iterator it = m_synchronizer.find(_key);
		if(it!=m_synchronizer.end())
			return it->second;
		return 0;
	}
};
/*!
 *  \class SynchronizerMng
 *  \brief Synchronizer manager for two operands
 * 
 *  \details
 *  This class allows to register and get back different synchronizers. \n
 *  These synchronizer are stored in a map where the key is defined by:
 *  - an Integer specifying the operation
 *  - a tuple storing the operands
 *
 *  \tparam T1 type of the first operand
 *  \tparam T2 type of the second operand
 */
template<typename T1, typename T2>
class SynchronizerMng2
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the key
	typedef std::pair<int, std::tuple<T1, T2> > key_type;
	//! \brief Type of the synchronizer
	typedef GeneralSynchronizer* data_type;
	//! \brief Type of this class
	typedef SynchronizerMng2<T1, T2> this_type;
 private:
	//! \brief Map storing the key and the synchronizer
	std::map<key_type, const GeneralSynchronizer*> m_synchronizer;
 public:
	/*!
	 *  \brief Register a synchronizer for an operation
	 *  
	 *  \param[in] _key The key to register the synchronizer
	 *  \param[in] _sync The synchronizer
	 */
	void setValue(const key_type& _key, const GeneralSynchronizer* _sync)
	{
		m_synchronizer[_key] = _sync;
	}

	/*!
	 *  \brief Return the synchronizer associated with the key
	 *  
	 *  \param[in] _key The key of the synchronizer
	 *
	 *  \return The synchronizer
	 */
	const GeneralSynchronizer* getValue(const key_type& _key) const 
	{
		typename std::map<key_type, const GeneralSynchronizer*>::const_iterator it = m_synchronizer.find(_key);
		if(it!=m_synchronizer.end())
			return it->second;
		return 0;
	}
};

/*!
 *  \class SynchronizerSingleton
 *  \brief Singleton for synchronizer manager
 * 
 *  \details
 *  This class guarantee that one and only one instance of the SynchronizerMng exist in the code
 */
class SynchronizerSingleton
: public SingletonDynamic<SynchronizerSingleton>
{
	//! \brief Friend class
	friend class SingletonDynamic<SynchronizerSingleton>;
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the synchronizer for 1 operand operations
	typedef SynchronizerMng1<void*> synchronizer_mng_1_type;
	//! \brief Type of the synchronizer for 2 operand operations
	typedef SynchronizerMng2<void*,void*> synchronizer_mng_2_type;

	//! \brief Enum for the different operations
	enum CommMap
	{
		//! \brief Column sum operation
		col_sum_map = 0,
		//! \brief SPMM operation
		spmm_map = 1
	};
 private:
	//! \brief Synchronizer for 1 operand operations
	synchronizer_mng_1_type m_1_arguments;
	//! \brief Synchronizer for 2 operand operations
	synchronizer_mng_2_type m_2_arguments;
 private:
	/*! \name Private Constructors
	 *
	 *  Private constructors of the class SynchronizerSingleton
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  Disabled
	 */
	SynchronizerSingleton() {}
	//@}

	/*! \name Private Destructor
	 *
	 *  Private destructor of the class SynchronizerSingleton
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Disabled
	 */
	~SynchronizerSingleton() {}

 public:
	/*!
	 *  \brief Register a synchronizer for a one operand operation
	 *  
	 *  \param[in] _key The key to register the synchronizer
	 *  \param[in] _sync The synchronizer
	 */
	void setValue( const synchronizer_mng_1_type::key_type& _key, const GeneralSynchronizer* _sync)
	{
		m_1_arguments.setValue(_key, _sync);
	}

	/*!
	 *  \brief Return the synchronizer associated with the key for one operand operation
	 *  
	 *  \param[in] _key The key of the synchronizer
	 *
	 *  \return The synchronizer
	 */
	const GeneralSynchronizer* getValue( const synchronizer_mng_1_type::key_type& _key) const
	{
		return m_1_arguments.getValue(_key);
	}

	/*!
	 *  \brief Register a synchronizer for a two operand operation
	 *  
	 *  \param[in] _key The key to register the synchronizer
	 *  \param[in] _sync The synchronizer
	 */
	void setValue( const synchronizer_mng_2_type::key_type& _key, const GeneralSynchronizer* _sync)
	{
		m_2_arguments.setValue(_key, _sync);
	}

	/*!
	 *  \brief Return the synchronizer associated with the key for two operand operation
	 *  
	 *  \param[in] _key The key of the synchronizer
	 *
	 *  \return The synchronizer
	 */
	const GeneralSynchronizer* getValue( const synchronizer_mng_2_type::key_type& _key) const
	{
		return m_2_arguments.getValue(_key);
	}
};
#endif
	
YALLA_END_NAMESPACE

#endif // _SYNCHRONIZERMNG_
