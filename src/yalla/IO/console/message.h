#pragma once
#ifndef yalla_message_h
#define yalla_message_h

/*!
 *  \file message.h
 *  \brief Classes for I/O on console
 *  \date 13/12/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include <sstream>
#include "yalla/Utils/stackTrace/StackTrace.h"
#include "yalla/Utils/ParUtils/ParUtils.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*!
 *  \class Message
 *  \brief Display a message on the standard output
 */
class Message
{
 private:
	//! \name Private class attributes
	//@{
	//! \brief Label of the message
	const std::string m_label;
	//! \brief Buffer for the message
	std::ostringstream m_buffer;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class Message
	 */
	//@{
	/*! 
	 *  \brief Constructor.
	 *
	 *  \details
	 *  Sets the label of the message
	 *
	 *  \param[in] _label The message label
	 */
 Message(std::string _label = "info")
	 : m_label(_label)
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class Message
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Display the message by calling the method print
	 */
	virtual ~Message()
	{
		print();
	}
	//@}

	//! \name Public Operators overloading of the class Message
	//@{
	/*! 
	 *  \brief Operator << overloading for char*
	 *
	 *  \details
	 *  Append the message passed in argument to the buffer
	 *
	 *  \param[in] _c The message to append
	 */
	Message& operator<<(const char* _c)
	{
		m_buffer << _c;
		return *this;
	}

	/*! 
	 *  \brief Generic operator << overloading
	 *
	 *  \details
	 *  Append the message passed in argument to the buffer
	 *
	 *  \tparam T The type of the message to append
	 *
	 *  \param[in] _i The message to append
	 */
	template<typename T>
    Message& operator<<(const T& _i)
	{
		m_buffer << _i;
		return *this;
	}

	/*! 
	 *  \brief Operator << overloading for std::endl
	 *
	 *  \details
	 *  Append the message passed in argument to the buffer
	 *
	 *  \param[in] pf The ostream
	 */
	Message& operator<<(std::ostream& (*pf)(std::ostream&))
	{
		pf(m_buffer);
		return *this;
	}
	//@}

 protected:
	/*! \name Protected Utils
	 *
	 *  Protect Utils of the class Message. \n
	 */
	//@{
	//!  \brief Print the buffer on the standard output
	void print() const
	{
#ifdef WITH_MPI
		if(ParUtils::iAmMaster())
#endif
			std::cout << "[ " << m_label << " ] " << m_buffer.rdbuf()->str() << std::endl << std::flush;
	}
	//@}
};

/*!
 *  \class Error
 *  \brief Display an error message on the standard output
 */

class Error : public Message
{
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class Error
	 */
	//@{
	/*! 
	 *  \brief Constructor.
	 *
	 *  \details
	 *  Sets the label of the message
	 *
	 *  \param[in] _label The message label
	 */
 Error(std::string _label = "error")
	 : Message(_label)
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class Error
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Display the message by calling the method print then abort.
	 */
	virtual ~Error()
	{
		print();
		YALLA_ABORT(-1);
	}
	//@}
};

/*!
 *  \class Print
 *  \brief Print an object on the standard output
 */
class Print
{
 private:
	//! \name Private class attributes
	//@{
	//! \brief Label of the message
	const std::string m_label;
	//! \brief Buffer for the message
	std::ostringstream m_buffer;
	//! \brief The proc from which one want to print
	int m_proc;
	//@}
	
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class Print
	 */
	//@{
	/*! 
	 *  \brief Constructor.
	 *
	 *  \details
	 *  Sets the label of the message. \n
	 *  Sets the proc from which one want to print.
	 *
	 *  \param[in] _label The message label
	 *  \param[in] _proc The proc to print
	 */
 Print(std::string _label = "print", const int _proc = -1)
	 : m_label(_label), m_proc(_proc)
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class Print
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Display the message by calling the method print
	 */
	virtual ~Print()
	{
		print();
	}
	//@}

	//! \name Public Operators overloading of the class Print
	//@{
	/*! 
	 *  \brief Operator << overloading for char*
	 *
	 *  \details
	 *  Append the message passed in argument to the buffer
	 *
	 *  \param[in] _c The message to append
	 */
	Print& operator<<(const char* _c)
	{
		m_buffer << _c;
		return *this;
	}

	/*! 
	 *  \brief Generic operator << overloading
	 *
	 *  \details
	 *  Append the message passed in argument to the buffer
	 *
	 *  \tparam T The type of the message to append
	 *
	 *  \param[in] _i The message to append
	 */
	template<typename T>
    Print& operator<<(const T& _i)
	{
		m_buffer << _i;
		return *this;
	}

	/*! 
	 *  \brief Operator << overloading for std::endl
	 *
	 *  \details
	 *  Append the message passed in argument to the buffer
	 *
	 *  \param[in] pf The ostream
	 */
	Print& operator<<(std::ostream& (*pf)(std::ostream&))
	{
		pf(m_buffer);
		return *this;
	}

 protected:
	//@}

 protected:
	/*! \name Protected Utils
	 *
	 *  Protect Utils of the class Print. \n
	 */
	//@{
	//!  \brief Print the buffer on the standard output
	void print() const
	{
#ifdef WITH_MPI
		if((m_proc==-1 && ParUtils::iAmMaster()) || (m_proc!=-1 && ParUtils::getProcRank()==m_proc))
#endif
			std::cout << "[ " << m_label << " ]" << std::endl << std::flush;
#ifndef WITH_MPI
		std::cout << m_buffer.rdbuf()->str() << std::endl << std::flush;
#else
		int rank = 0;
		int myRank = ParUtils::getProcRank();
		int numProc = ParUtils::getNumProc();
		if(m_proc==-1)
		{
			while (rank < numProc) 
			{
				if (myRank == rank)
					std::cout << m_buffer.rdbuf()->str() << std::endl << std::flush;
				MPI_Barrier(ParUtils::ThisComm);
				++rank;
			}
		}
		else
		{
			if(myRank==m_proc)
				std::cout << m_buffer.rdbuf()->str() << std::endl << std::flush;
		}
#endif
	}
	//@}
};

YALLA_END_NAMESPACE
        
#endif
