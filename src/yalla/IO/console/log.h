#pragma once
#ifndef yalla_log_h
#define yalla_log_h

/*!
 *  \file log.h
 *  \brief Macros for logging different informations
 *  \date 13/12/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "message.h"
#include "yalla/Utils/stackTrace/StackTrace.h"

/*!
 *  \brief Display an error message and abort
 *  
 *  \param info The message to display
 */
// Windows console does not support color escape sequences
#ifndef _WIN32
#define YALLA_ERR(info) yalla::Error("\E[0m\E[31m\E[40merr\E[0m") << info << "\n" << StackTrace::printStack();
#else
#define YALLA_ERR(info) yalla::Error("err") << info << "\n" << StackTrace::printStack();
#endif
/*!
 *  \brief Display a message
 *  
 *  \param info The message to display
 */
#define YALLA_LOG(info) yalla::Message("log") << info;
/*!
 *  \brief Display a succesfull event (used in the perf test framework)
 *  
 *  \param info The message to display
 */
#ifndef _WIN32
#define YALLA_SUCCEED(info) YALLA_LOG("\E[0m\E[32m\E[40m" << info << "\E[0m")
#else
#define YALLA_SUCCEED(info) YALLA_LOG(info)
#endif
/*!
 *  \brief Display a stalled event (used in the perf test framework)
 *  
 *  \param info The message to display
 */
#ifndef _WIN32
#define YALLA_STALLED(info) YALLA_LOG("\E[0m\E[33m\E[40m" << info << "\E[0m")
#else
#define YALLA_STALLED(info) YALLA_LOG(info)
#endif
/*!
 *  \brief Display a failed event (used in the perf test framework)
 *  
 *  \param info The message to display
 */
#ifndef _WIN32
#define YALLA_FAILED(info) YALLA_LOG("\E[0m\E[31m\E[40m" << info << "\E[0m")
#else
#define YALLA_FAILED(info) YALLA_LOG(info)
#endif
#ifdef _DEBUG
/*!
 *  \brief Display a message
 *  
 *  \param info The message to display
 */
#define YALLA_SILENT_LOG(info) yalla::Message("log") << info;
/*!
 *  \brief Print an object (needs the <<operator)
 *  
 *  \param info The message to display
 */
#define YALLA_PRINT(info) yalla::Print("print",-1) << info;
/*!
 *  \brief Print an object only on the specified proc (needs the <<operator)
 *  
 *  \param info The message to display
 *  \param proc The processor that will display its information
 */
#define YALLA_PRINT_PROC(info,proc) yalla::Print("print",proc) << info;
#else
/*!
 *  \brief Display a message (do nothing in release mode)
 *  
 *  \param info The message to display
 */
#define YALLA_SILENT_LOG(info) ((void)0)
/*!
 *  \brief Print an object (do nothing in release mode)
 *  
 *  \param info The message to display
 */
#define YALLA_PRINT(info) ((void)0)
/*!
 *  \brief Print an object only on the specified proc (do nothing in release mode)
 *  
 *  \param info The message to display
 *  \param proc The processor that will display its information
 */
#define YALLA_PRINT_PROC(info,proc) ((void)0)
#endif

#endif
