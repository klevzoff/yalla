#pragma once
#ifndef yalla_sparse_matrix_writer_mm_format_h
#define yalla_sparse_matrix_writer_mm_format_h

/*!
 *  \file SparseMatrixWriterMMFormat.h
 *  \brief Writes a matrix in format MM in a file
 *  \date 12/24/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "yalla/Utils/Exceptions/NoFileName.h"

/*
	#include "yalla/Utils/Types/TypesAndDef.h"
	#include "yalla/Utils/Exceptions/NoFileName.h"
	#include "yalla/IO/console/log.h"
*/

/*!
 *  \namespace yalla
 *  \brief Global namespace du projet
 *
 *  Global namespace du projet
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*!
 *  \class SparseMatrixWriterMMFormat
 *  \brief Writes a matrix in format MM in a file
 *
 *  This class works only with a CSR matrix. It has to be extended for other type of matrix implementation
 *
 *  \tparam Matrix The matrix to write
 */
template<typename Matrix>
class SparseMatrixWriterMMFormat
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the matrix
	typedef Matrix data_type;
	//! \brief Type of this class
	typedef SparseMatrixWriterMMFormat<data_type> this_type;
	//@}
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SparseMatrixWriterMMFormat
	 */
	//@{
	/*! 
	 *  \brief Default constructor.
	 */
	explicit SparseMatrixWriterMMFormat()
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SparseMatrixWriterMMFormat
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~SparseMatrixWriterMMFormat()
	{
		;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SparseMatrixWriterMMFormat. \n
	 *  These methods give access in read-only mode to the datas/information of the class SparseMatrixWriterMMFormat
	 */

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SparseMatrixWriterMMFormat" The name of the class
	 */
	const char* getClassName() const
	{
		return "SparseMatrixWriterMMFormat";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SparseMatrixWriterMMFormat. \n
	 *  These methods provide various utilities to the class SparseMatrixWriterMMFormat
	 */
	//@{

	/*!
	 *  \brief Write a matrix based on the value given as parameter
	 *
	 *  \details
	 *  \exception NoFileNameSpecified No filename was specified
	 *
	 *  \param[in] _inMatrix The matrix to write
	 *  \param[in] _file The file to write the matrix
	 */
	void Write(const data_type& _inMatrix, const char * _file)
	{
		try
		{
			if(NoFileName::Check(_file))
				throw NoFileNameSpecified(__FILE__,__LINE__);
		}
		catch ( NoFileNameSpecified &noFileName )
		{
			YALLA_ERR(noFileName.what());
		}

		const int nbRows = _inMatrix.getNbRows();
		const int nbCols = _inMatrix.getNbCols();
		const int nnz = _inMatrix.getNnz();
		const typename data_type::impl_type::graph_type& graph = _inMatrix.getMatrixGraph();

		std::ofstream file(_file, std::ios::out);
		
		file << "% Writing MM Matrix \n% Nb Rows : " << nbRows << " ; Nb Cols : " << nbCols << " Nb Nnz: " << nnz << "\n";
		file << nbRows << " " << nbCols << " " << nnz << "\n";
		file.precision(15);
		file.setf(std::ios::scientific,std::ios::floatfield);

		for(int i=0;i<nbRows;++i)
		{
			for(int j=graph.getRowPtr(i);j<graph.getRowPtr(i+1);++j)
			{
				file << i+1 << " " << graph.getColPtr(j)+1 << " " << _inMatrix[j] << "\n";
			}
		}
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
