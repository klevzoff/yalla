#pragma once
#ifndef yalla_full_matrix_writer_h
#define yalla_full_matrix_writer_h

/*!
 *  \file DenseMatrixWriter.h
 *  \brief Writes dense matrix in a file
 *  \date 12/24/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#define TOL 1e-16

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "yalla/Utils/Exceptions/NoFileName.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace du projet
 *
 *  Global namespace du projet
 */
YALLA_BEGIN_NAMESPACE(yalla)
 
/*!
 *  \class DenseMatrixWriter
 *  \brief Writes dense matrix in a file
 *
 *  This class should work with any kind of matrix.
 *
 *  \tparam Matrix The matrix to write
 *
 *  \warning A parameter TOL is used to specify the tolerance. If a value in the matrix is less that this tolerance, then 0 is written. 
 */
template<typename Matrix>
class DenseMatrixWriter
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the matrix
	typedef Matrix data_type;
	//! \brief Type of this class
	typedef DenseMatrixWriter<Matrix> this_type;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DenseMatrixWriter
	 */
	//@{
	/*! 
	 *  \brief Default constructor.
	 */
	explicit DenseMatrixWriter()
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DenseMatrixWriter
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~DenseMatrixWriter()
	{
		;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class DenseMatrixWriter. \n
	 *  These methods give access in read-only mode to the datas/information of the class DenseMatrixWriter
	 */

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DenseMatrixWriter" The name of the class
	 */
	const char* getClassName() const
	{
		return "DenseMatrixWriter";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class DenseMatrixWriter. \n
	 *  These methods provide various utilities to the class DenseMatrixWriter
	 */
	//@{

	/*!
	 *  \brief Write a matrix based on the value given as parameter
	 *
	 *  \details
	 *  \exception NoFileNameSpecified No filename was specified
	 *
	 *  \param[in] _inMatrix The matrix to write
	 *  \param[in] _file The file to write the matrix
	 */
	void Write(const data_type& _inMatrix, const char * _file)
	{
		try
		{
			if(NoFileName::Check(_file))
				throw NoFileNameSpecified(__FILE__,__LINE__);
		}
		catch ( NoFileNameSpecified &noFileName )
		{
			YALLA_ERR(noFileName.what())
				}

		int nbRows = _inMatrix.getNbRows();
		int nbCols = _inMatrix.getNbCols();

		std::ofstream file(_file, std::ios::out);
		try
		{
			if(FileNotFound::Check(file))
				throw SpecifiedFileNotFound(__FILE__,__LINE__);
		}
		catch ( SpecifiedFileNotFound &fileNotFound )
		{
			YALLA_ERR(fileNotFound.what())
				}

		file << "% Writing Dense Matrix \n% Nb Rows : " << nbRows << " ; Nb Cols : " << nbCols << "\n";
		file << "% Tol set to : " << TOL << "\n";
		file << nbRows << " " << nbCols << " " << nbRows*nbCols << "\n";
		for(int i=0;i<nbRows-1;++i)
		{
			for(int j=0;j<nbCols-1;++j)
			{
				if(std::abs(_inMatrix(i,j))<TOL)
					file << 0 << " ";
				else
					file << _inMatrix(i,j) << " " ;
			}
			if(std::abs(_inMatrix(i,nbCols-1))<TOL)
				file << 0 << "\n";
			else
				file << _inMatrix(i,nbCols-1) << "\n";
		}
		for(int j=0;j<nbCols-1;++j)
			if(std::abs(_inMatrix(nbRows-1,j))<TOL)
				file << 0 << " ";
			else
				file << _inMatrix(nbRows-1,j) << " ";
		if(std::abs(_inMatrix(nbRows-1,nbCols-1))<TOL)
			file << 0;
		else
			file << _inMatrix(nbRows-1,nbCols-1);
		file.close();
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
