#pragma once
#ifndef yalla_full_matrix_reader_h
#define yalla_full_matrix_reader_h

/*!
 *  \file DenseMatrixReader.h
 *  \brief Dense matrix reader
 *  \date 12/24/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "yalla/Utils/Exceptions/NoFileName.h"
#include "yalla/Utils/Exceptions/FileNotFound.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace du projet
 *
 *  Global namespace du projet
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*!
 *  \class DenseMatrixReader
 *  \brief Dense matrix reader
 * 
 *  \details
 *  Creates a dense matrix read from a file
 *
 *  \tparam MatrixImpl The actual implementation of the matrix \n
 */
template<typename MatrixImpl>
class DenseMatrixReader
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the implementation of the matrix
	typedef MatrixImpl impl_type;
	//! \brief Type of this class
	typedef DenseMatrixReader<impl_type> this_type;
	//@}
 private:
	//! \name Private class attributes
	//@{
	//! \brief File to read the matrix from
	std::string m_filename;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DenseMatrixReader
	 */
	//@{
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Set the filename to "". \n
	 *  If the class is built that way, the method Read(const std::string) has to be used.
	 */
	explicit DenseMatrixReader()
	{
		m_filename="";
	}

	/*! 
	 *  \brief Constructor with filename
	 *
	 *  \details
	 *  Set the filename to _file. \n
	 *  If the class is built that way, the method Read() has to be used.
	 *
	 *  \param[in] _file The file where the matrix is stored
	 */
	explicit DenseMatrixReader(const char * _file)
	{
		m_filename=_file;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DenseMatrixReader
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~DenseMatrixReader()
	{
		;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class DenseMatrixReader. \n
	 *  These methods give access in read-only mode to the datas/information of the class DenseMatrixReader
	 */

	//@{
	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DenseMatrixReader" The name of the class
	 */
	const char* getClassName() const
	{
		return "DenseMatrixReader";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class DenseMatrixReader. \n
	 *  These methods provide various utilities to the class DenseMatrixReader
	 */
	//@{

	/*!
	 *  \brief Read a matrix based on the value in m_filename
	 *
	 *  \details
	 *  \exception NoFileNameSpecified No filename was specified
	 *  \exception SpecifiedFileNotFound The file was not found
	 */
	impl_type * Read()
	{
		// Assert that a filename is provided and can be found
		try
		{
			if(NoFileName::Check(m_filename))
				throw NoFileNameSpecified(__FILE__,__LINE__);
		}
		catch ( NoFileNameSpecified &noFileName )
		{
			YALLA_ERR(noFileName.what())
				}
		std::ifstream file(m_filename, std::ios::in);
		try
		{
			if(FileNotFound::Check(file))
				throw SpecifiedFileNotFound(__FILE__,__LINE__);
		}
		catch ( SpecifiedFileNotFound &fileNotFound )
		{
			YALLA_ERR(fileNotFound.what())
				}

		// Skipping all the comments
		std::string line;
		while(std::getline(file,line))
		{
			if(line.at(0)=='%')
				continue;
			else
				break;
		}

		// Getting information about the matrix size and the number of entries
		int row, col, entries;
		std::istringstream input(line);
		input >> row >> col >> entries;

		impl_type* tmpMatrix = new impl_type(row,col);

		// Getting the values and filling the matrix
		std::string tmpString;
		int i = 0;
		int j = 0;
		dataType val;
		while(!file.eof())
		{
			file >> tmpString;
			std::istringstream stringStream(tmpString);
			stringStream >> val;
			(*tmpMatrix)(i,j) = val;
			++j;
			if(j==col)
			{
				++i;
				j=0;
			}
		}
		file.close();
		return tmpMatrix;
	}

	/*!
	 *  \brief Read a matrix based on the value given as parameter
	 *
	 *  \details
	 *  \exception NoFileNameSpecified No filename was specified
	 *  \exception SpecifiedFileNotFound The file was not found
	 *
	 *  \param[in] _file The file to read the matrix from
	 */
	impl_type * Read(const char* _file)
	{
		// Assert that a filename is provided and can be found
		try
		{
			if(NoFileName::Check(m_filename))
				throw NoFileNameSpecified(__FILE__,__LINE__);
		}
		catch ( NoFileNameSpecified &noFileName )
		{
			YALLA_ERR(noFileName.what())
				}
		std::ifstream file(m_filename, std::ios::in);
		try
		{
			if(FileNotFound::Check(file))
				throw SpecifiedFileNotFound(__FILE__,__LINE__);
		}
		catch ( SpecifiedFileNotFound &fileNotFound )
		{
			YALLA_ERR(fileNotFound.what())
				}

		// Skipping all the informations
		std::string line;
		while(std::getline(file,line))
		{
			if(line.at(0)=='%')
				continue;
			else
				break;
		}


		// Getting information about the matrix size and the number of entries
		int row, col, entries;
		std::istringstream input(line);
		input >> row >> col >> entries;

		impl_type* tmpMatrix = new impl_type(row,col);

		// Getting the values and filling the matrix
		std::string tmpString;
		int i = 0;
		int j = 0;
		dataType val;
		while(!file.eof())
		{
			file >> tmpString;
			std::istringstream stringStream(tmpString);
			stringStream >> val;
			(*tmpMatrix)(i,j) = val;
			++j;
			if(j==col)
			{
				++i;
				j=0;
			}
		}
		file.close();
		return tmpMatrix;
	}
};
YALLA_END_NAMESPACE
#endif
