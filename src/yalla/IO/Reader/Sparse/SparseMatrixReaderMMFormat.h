#pragma once
#ifndef yalla_sparse_matrix_reader_mm_format_h
#define yalla_sparse_matrix_reader_mm_format_h

/*!
 *  \file SparseMatrixReaderMMFormat.h
 *  \brief Matrix Market  matrix reader
 *  \date 12/24/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include "yalla/Utils/Exceptions/NoFileName.h"
#include "yalla/Utils/Exceptions/FileNotFound.h"
#include "yalla/Utils/Algorithms/DoubleQuickSort.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace du projet
 *
 *  Global namespace du projet
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*!
 *  \class SparseMatrixReaderMMFormat 
 *  \brief Read a matrix from a file in MM format
 *
 *  Creates a sparse matrix from a file stored in MM format. \n
 *  Now, only CSR matrices can be built from this class. However, adding a new buildMatrix method specialized for a new type of matrix is straightforward. The matrix will always be read in CSR format, but then using the information are easy to build any new kind of matrix.
 *
 *  \tparam MatrixImpl The actual implementation of the matrix \n
 *
 *  \todo Add an option to specify if the matrix is 0 or 1 based index in the file (now files are expected to be 1 based)
 *  \details 
 */
template<typename MatrixImpl>
class SparseMatrixReaderMMFormat
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the implementation of the matrix
	typedef MatrixImpl impl_type;
	//! \brief Data type of the matrix
	typedef typename impl_type::data_type data_type;
	//! \brief Type of this class
	typedef SparseMatrixReaderMMFormat<impl_type> this_type;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SparseMatrixReaderMMFormat
	 */
	//@{
	/*! 
	 *  \brief Default constructor.
	 */
	explicit SparseMatrixReaderMMFormat()
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SparseMatrixReaderMMFormat
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing (no memory allocated)
	 */
	virtual ~SparseMatrixReaderMMFormat()
	{
		;
	}
	//@}

	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SparseMatrixReaderMMFormat. \n
	 *  These methods give access in read-only mode to the datas/information of the class SparseMatrixReaderMMFormat
	 */

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SparseMatrixReaderMMFormat" The name of the class
	 */
	const char* getClassName() const
	{
		return "SparseMatrixReaderMMFormat";
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SparseMatrixReaderMMFormat. \n
	 *  These methods provide various utilities to the class SparseMatrixReaderMMFormat
	 */
	//@{

	/*!
	 *  \brief Read a matrix based on the value given as parameter
	 *
	 *  \details
	 *  \exception NoFileNameSpecified No filename was specified
	 *  \exception SpecifiedFileNotFound The file was not found
	 *
	 *  \param[in] _file The file to read the matrix from
	 */
	impl_type* Read(const char* _file)
	{
		// Assert that a filename is provided and can be found
		try
		{
			if(NoFileName::Check(_file))
				throw NoFileNameSpecified(__FILE__,__LINE__);
		}
		catch ( NoFileNameSpecified &noFileName )
		{
			YALLA_ERR(noFileName.what())
				}
		std::ifstream file(_file, std::ios::in);
		try
		{
			if(FileNotFound::Check(file))
				throw SpecifiedFileNotFound(__FILE__,__LINE__);
		}
		catch ( SpecifiedFileNotFound &fileNotFound )
		{
			YALLA_ERR(fileNotFound.what())
				}

		// Skipping all the informations
		std::string line;
		while(std::getline(file,line))
		{
			if(line.at(0)=='%')
				continue;
			else
				break;
		}

		// Getting information about the matrix size and the number of entries
		int row, col, entries;
		std::istringstream input(line);
		input >> row >> col >> entries;

		int* rowPtr = new int[entries];
		int* colPtr = new int[entries];
		data_type* valPtr = new data_type[entries];

		// Getting the values and filling the matrix
		int counter = 0;
		while(std::getline(file,line))
		{
			std::istringstream input(line);
			input >> rowPtr[counter] >> colPtr[counter] >> valPtr[counter];
			++counter;
		}
			
		for(int i=0;i<entries;++i)
		{
			--rowPtr[i];
			--colPtr[i];
		}

		file.close();

		impl_type* tmpMatrix = new impl_type();
		this->buildMatrix(row,col,entries,rowPtr,colPtr,valPtr,*tmpMatrix);
			
		delete[] rowPtr;
		delete[] colPtr;
		delete[] valPtr;

		return tmpMatrix;
	}
	//@}
 private:
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class SparseMatrixReaderMMFormat. \n
	 *  These methods provide various utilities to the class y
	 */
	//@{
	/*!
	 *  \brief Build a CSR matrix
	 *
	 *  \details
	 *  This method format the datas read from the file to build a SeqSparseMatrix<data_type, SeqCSRMatrix<data_type> > 
	 *
	 *  \param[in] _row The number of rows
	 *  \param[in] _col The number of cols
	 *  \param[in] _entries The number of entries
	 *  \param[in] _rowPtr The row pointer
	 *  \param[in] _colPtr The col pointer
	 *  \param[in] _valPtr The data pointer
	 *  \param[in] _matrix The matrix to be built
	 */
	void buildMatrix(const int _row, const int _col, const int _entries, int* _rowPtr, int* _colPtr, data_type* _valPtr, SeqSparseMatrix<data_type, SeqCSRMatrix<data_type> >& _matrix)
	{
		int* rowPtr = new int[_row+1];

		for(int i=0;i<_row+1;++i)
			rowPtr[i] = 0;

		for(int i=0;i<_entries;++i)
			++rowPtr[_rowPtr[i]+1];

		for(int i=1;i<_row+1;++i)
			rowPtr[i] += rowPtr[i-1];

		for(int i=0;i<_row;++i)
			DoubleQuickSort<int,data_type>::sort(_colPtr,_valPtr,rowPtr[i],rowPtr[i+1]-1);

		_matrix.initialize(_row,_col,_entries,rowPtr,_colPtr);
		_matrix.fill(_valPtr);
		delete[] rowPtr;
	}

	/*!
	 *  \brief Build a CSR matrix
	 *
	 *  \details
	 *  This method format the datas read from the file to build a SeqSparseMatrix<data_type, SeqCSRMKLatrix<data_type> > 
	 *
	 *  \param[in] _row The number of rows
	 *  \param[in] _col The number of cols
	 *  \param[in] _entries The number of entries
	 *  \param[in] _rowPtr The row pointer
	 *  \param[in] _colPtr The col pointer
	 *  \param[in] _valPtr The data pointer
	 *  \param[in] _matrix The matrix to be built
	 */
	void buildMatrix(const int _row, const int _col, const int _entries, int* _rowPtr, int* _colPtr, data_type* _valPtr, SeqSparseMatrix<data_type, SeqCSRMKLMatrix<data_type> >& _matrix)
	{
		int* rowPtr = new int[_row+1];

		for(int i=0;i<_row+1;++i)
			rowPtr[i] = 0;

		for(int i=0;i<_entries;++i)
			++rowPtr[_rowPtr[i]+1];

		for(int i=1;i<_row+1;++i)
			rowPtr[i] += rowPtr[i-1];

		for(int i=0;i<_row;++i)
			DoubleQuickSort<int,data_type>::sort(_colPtr,_valPtr,rowPtr[i],rowPtr[i+1]-1);

		_matrix.initialize(_row,_col,_entries,rowPtr,_colPtr);
		_matrix.fill(_valPtr);
		delete[] rowPtr;
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
