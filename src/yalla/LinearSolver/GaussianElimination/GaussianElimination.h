#pragma once
#ifndef yalla_gaussian_elimination_h
#define yalla_gaussian_elimination_h

/*!
 *  \file GaussianElimination.h
 *  \brief Gaussian elimination solver
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <vector>
#include <algorithm>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/LinearSolver/GaussianElimination/GaussianEliminationInfo.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class GaussianElimination
 *  \brief Gaussian elimination solver
 * 
 *  \details
 *  The algorithm is decomposed in three steps:
 *  - LU decomposition of the matrix A
 *  - Ly = b is solved
 *  - Ux = y is solved
 *
 *  In this implementation:
 *  - The first two steps are performed simultanuously
 *  - The matrix A is not modified
 *
 *  This class is sequential only and has no other purpose that being "handy" if no direct solver is available.
 *
 *  \todo Check that A is squared
 *  \todo Check that there is no null pivot
 */
class GaussianElimination
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef GaussianElimination this_type;
	//@}
 private:
	//! \brief Verbosity level
	int m_verbose_level;
	//! \brief Information on the GaussianElimination solver execution
	GaussianEliminationInfo * m_ge_info;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class GaussianElimination
	 */
	//@{
		
	//! \brief Default constructor.
	explicit GaussianElimination()
	{
		m_verbose_level = 0;
		m_ge_info = 0;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class GaussianElimination
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the information data stucture
	 */
	virtual ~GaussianElimination()
	{
		delete m_ge_info;
		m_ge_info= 0;
	}

	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class GaussianElimination. \n
	 *  These methods give access in read-only mode to the datas/information of the class GaussianElimination
	 */
	//@{

	/*! 
	 *  \brief Return the verbosity level
	 *
	 *  \details
	 *
	 *  \return The verbosity level
	 */
	int getVerboseLvl() const
	{
		return m_verbose_level;
	}

	/*! \brief Return the class name. 
	 *
	 *  \return "GaussianElimination" The name of the class
	 */
	const char* getClassName() const
	{
		return "GaussianElimination";
	}

	/*! 
	 *  \brief Return the structure storing execution information
	 *
	 *  \details
	 *
	 *  \return The execution information
	 */
	const GaussianEliminationInfo* getInfos() const
	{
		return m_ge_info;
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class GaussianElimination. \n
	 *  These methods give access in read/write mode to the datas/information of the class GaussianElimination
	 */
	//@{
	/*! \brief Set the verbosity level
	 *
	 *  \param[in] _lvl The verbosity level
	 */
	void setVerbose(const int _lvl)
	{
		m_verbose_level = _lvl;
		if(m_verbose_level && !m_ge_info)
			m_ge_info = new GaussianEliminationInfo(m_verbose_level);
	}
	//@}

	/*! \name Public setup
	 *
	 *  Setup method of the GaussianElimination solver. \n
	 */
	//@{
	/*! \brief Apply the setup phase of the GaussianElimination solver
	 *
	 *  Do nothing (no memory to be allocated here). \n
	 *  Eventually, the symbolic factorization might be done here.
	 *
	 *  \tparam Matrix The type of the matrix
	 *  \tparam Vector The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[out] _lhs The lhs
	 */
	template<typename Matrix, typename Vector>
		void setup(const Matrix& _A, const Vector& _rhs, Vector& _lhs)
	{
		;
	}
	//@}


	/*! \brief Solve method
	 *  
	 *  The solve is decomposed in two parts:
	 *  - First we perform the symbolic factorization if the matrix is sparse. If the matrix is dense, we just copy it. This is done in the allocate method. \n
	 *  - Then the 3 steps of the Gaussian Elimination algorithm are done
	 *
	 *  \tparam Matrix The type of the matrix
	 *  \tparam Vector The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[out] _lhs The lhs
	 */
	template<typename MatrixType, typename Vector>
    void solve(const MatrixType& _A, const Vector& _rhs, Vector& _lhs)
	{
		if(m_verbose_level)
		{
			m_ge_info->m_profiler->start("GESolve");
			m_ge_info->m_memory_used->start("GESolve");
		}
		const int n = _A.getNbRows();
		typedef typename MatrixType::data_type data_type;
		MatrixType tmpL;
		// Symbolic factorization
		this->allocate(_A,tmpL);
		Vector tmpb(_rhs);
		// First two steps (A = LU && Ly = b)
		for(int k=0;k<n-1;++k)
		{
			for(int i=k+1;i<n;++i)
			{
				const data_type l_ik = tmpL(i,k)/tmpL(k,k);
				for(int j=k;j<n;++j)
					tmpL(i,j) -= l_ik * tmpL(k,j);
				tmpb(i) -= l_ik * tmpb(k);
			}
		}
		// Third step (Ux = y)
		_lhs(n-1) = tmpb(n-1)/tmpL(n-1,n-1);
		for(int k=n-2;k>=0;--k)
		{
			_lhs(k) = tmpb(k);
			for(int i=k+1;i<n;++i)
			{
				_lhs(k) -= tmpL(k,i)*_lhs(i);
			}
			_lhs(k) /= tmpL(k,k);
		}
		if(m_verbose_level)
		{
			m_ge_info->m_profiler->stop("GESolve");
			m_ge_info->m_memory_used->stop("GESolve");
		}
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class GaussianElimination. \n
	 *  These methods provide various utilities to the class GaussianElimination
	 */
	//@{

	/*!
	 *  \brief Display the information related to a GaussianElimination solver on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _solver The solver to print
	 */
	friend std::ostream& operator<<(std::ostream& _os, const this_type& _solver)
	{
		const GaussianEliminationInfo* infos = _solver.getInfos();
		if(infos==0)
			return _os;

		std::string infoMessage = "";
		std::ostringstream oss;
		oss.setf(std::ios::scientific);
		oss.precision(6);
		if(_solver.getVerboseLvl()>0) // Cv rate, residual and scaled residual norm
		{
			infoMessage.append("=====================================================\n");
			infoMessage.append("              GaussianElimination Infos              \n");
			infoMessage.append("=====================================================\n");
			oss.unsetf(std::ios::scientific);
			oss.setf(std::ios::fixed);
			oss.precision(6);
			infoMessage.append("=====================================================\n");
			infoMessage.append(" Time -   Solve               \n");
			oss.str("");
			infoMessage.append(" [s]    ");
			const double timeGESolve = infos->m_profiler->getTime("GESolve");
			oss.str("");
			oss << timeGESolve;
			infoMessage.append(oss.str());
			infoMessage.append("\n");
			infoMessage.append("=====================================================\n");
			infoMessage.append(" Mem. -   Solve                \n");
			const double memoryGESolve = infos->m_memory_used->getMem("GESolve");
			oss.precision(6);
			oss.str("");
			infoMessage.append(" [Mb]   ");
			oss.str("");
			oss << memoryGESolve;
			infoMessage.append(oss.str());
			infoMessage.append("\n");
			infoMessage.append("=====================================================\n");
		}
		_os << infoMessage;
		return _os;
	}
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class GaussianElimination. \n
	 *  These methods provide various utilities to the class GaussianElimination
	 */
	//@{

	/*! \brief Build the matrix required for the Gaussian Elimination solver for a dense matrix
	 *
	 *  For a dense matrix, there is no special requirements to apply the Gaussian Elimination. \n
	 *  Therefore the matrix A is just copied.
	 *
	 *  \tparam DataType The data type of the matrix
	 *  \tparam MatrixImpl The actual implementation of the matrix
	 *
	 *  \param[in] _matLHS The matrix to inverse
	 *  \param[in,out] _matCopy The temporary matrix for Gaussian Elimination
	 */
	template<typename DataType, typename MatrixImpl>
    void allocate(const SeqDenseMatrix<DataType, MatrixImpl>& _matLHS, SeqDenseMatrix<DataType, MatrixImpl>& _matCopy)
	{
		_matCopy = _matLHS;
	}

	/*! \brief Build the matrix required for the Gaussian Elimination solver for a sparse matrix
	 *
	 *  The requirement here is to create a new matrix containing the additionnal fillin occuring while applying the Gaussian Elimination algorithm.
	 *
	 *  Here is how we proceed:
	 *  - First, the number of elements that will have to be added are counted. To do so, we pick up the first pivot, look through all rows of the matrix where an entry will have to be eliminated and count the number of elements for each of these rows that will have to be added. Meanwhile, we store all these indices. 
	 *  - Each element that have been added in the previous step need to be taken into account for the remaining part of the algorithm, as they can create additionnal fillin.
	 *  - Once we have counted and stored the indices for all those new elements, the former matrix is copied while a null entry is added for each of those elements. This is done in several steps:
	 *    - First, the row pointer is copied and all new entries are added
	 *    - Then all unchanged rows, cols and datas are copied
	 *    - Finally, the remaining part of the matrix is copied while the new elements are inserted in the column and data pointer
	 *  
	 *  The new matrix with the required additionnal fillin is now ready to use to apply the Gaussian Elimination algorithm
	 *
	 *  \tparam DataType The data type of the matrix
	 *  \tparam MatrixImpl The actual implementation of the matrix
	 *
	 *  \param[in] _matLHS The matrix to inverse
	 *  \param[in,out] _matCopy The temporary matrix for Gaussian Elimination
	 */
	template<typename DataType, typename MatrixImpl>
    void allocate(const SeqSparseMatrix<DataType, MatrixImpl>& _matLHS, SeqSparseMatrix<DataType, MatrixImpl>& _matCopy)
	{
		typedef SeqSparseMatrix<DataType, MatrixImpl> MatrixType;
		typedef typename MatrixType::data_type data_type;
		int nnz = _matLHS.getNnz();
		const int nbRows = _matLHS.getNbRows();
		const int nbCols = _matLHS.getNbCols();
		std::vector<std::pair<int,int> > tmpIndices;
		for(int i=0;i<nbRows;++i)
		{
			for(int j=i+1;j<nbRows;++j)
			{
				if(_matLHS(j,i) || this->findInVector(tmpIndices, j, i))
				{
					for(int k=i;k<nbCols;++k)
					{
						if((_matLHS(i,k) && !_matLHS(j,k)) || (this->findInVector(tmpIndices, i, k) && !_matLHS(j,k)))
						{
							if(!(this->findInVector(tmpIndices, j, k)))
							{
								++nnz;
								std::pair<int,int> indices(j,k);
								tmpIndices.push_back(indices);
							}
						}
					}
				}
			}
		}
		std::sort(tmpIndices.begin(),tmpIndices.end());
		int * rows = new int[nbRows+1];
		int * cols = new int[nnz];
		data_type * val = new data_type[nnz];
		typedef typename MatrixImpl::graph_type MatrixGraph;
		const MatrixGraph& AGraph = _matLHS.getMatrixGraph();
		for(int i=0;i<nbRows+1;++i)
			rows[i] = AGraph.getRowPtr(i);

		for(unsigned int i=0;i<tmpIndices.size();++i)
		{
			for(int j=tmpIndices[i].first;j<nbRows;++j)
				++rows[j+1];
		}

		int lastRow = 0;
		int lastColEntry = 0;
		for(;lastRow<nbRows;++lastRow)
		{
			if(rows[lastRow+1] == AGraph.getRowPtr(lastRow+1))
			{
				for(lastColEntry=rows[lastRow];lastColEntry<rows[lastRow+1];++lastColEntry)
				{
					cols[lastColEntry] = AGraph.getColPtr(lastColEntry);
					val[lastColEntry] = _matLHS(lastRow,AGraph.getColPtr(lastColEntry));
				}
			}
			else
				break;
		}
		unsigned int addedValue = 0;
		bool stop = false;
		for(;lastRow<nbRows;++lastRow)
		{
			for(lastColEntry=rows[lastRow];lastColEntry<rows[lastRow+1];++lastColEntry)
			{
				if(lastRow < tmpIndices[addedValue].first)
				{
					cols[lastColEntry] = AGraph.getColPtr(lastColEntry-addedValue);
					val[lastColEntry] = _matLHS(lastRow,AGraph.getColPtr(lastColEntry-addedValue));
				}
				else
				{
					if(AGraph.getColPtr(lastColEntry-addedValue) < tmpIndices[addedValue].second)
					{
						cols[lastColEntry] = AGraph.getColPtr(lastColEntry-addedValue);
						val[lastColEntry] = _matLHS(lastRow,AGraph.getColPtr(lastColEntry-addedValue));
					}
					cols[lastColEntry] = tmpIndices[addedValue].second;
					val[lastColEntry] = 0;
					++addedValue;
					if(addedValue==tmpIndices.size())
					{
						stop = true;
						break;
					}
				}
			}
			if(stop)
				break;
		}

		if(lastRow<nbRows)
		{
			++lastColEntry;
			for(;lastColEntry<rows[lastRow+1];++lastColEntry)
			{
				cols[lastColEntry] = AGraph.getColPtr(lastColEntry-addedValue);
				val[lastColEntry] = _matLHS(lastRow,AGraph.getColPtr(lastColEntry-addedValue));
			}

			++lastRow;
			for(;lastRow<nbRows;++lastRow)
			{
				for(lastColEntry=rows[lastRow];lastColEntry<rows[lastRow+1];++lastColEntry)
				{
					cols[lastColEntry] = AGraph.getColPtr(lastColEntry-addedValue);
					val[lastColEntry] = _matLHS(lastRow,AGraph.getColPtr(lastColEntry-addedValue));
				} 
			}
		}
		_matCopy = MatrixType(nbRows,nbCols,nnz,rows,cols,val);
		delete[] rows;
		delete[] cols;
		delete[] val;
	}

	/*! \brief Look if an entry exist in a vector of indices
	 *
	 *  Check if the (_row,_col) exist in the vector (containing the indices of added elements while computing the additionnal fillin of the matrix to be inverted)
	 *
	 *  \param[in] _vec The vector containing the indices of the additionnal fillin
	 *  \param[in] _row The row index we are looking
	 *  \param[in] _col The col index we are looking
	 *
	 *  \return True if the index exists, false otherwise
	 */
	bool findInVector(const std::vector<std::pair<int, int> > _vec, const int _row, const int _col)
	{
		const int size = _vec.size();
		for(int i=0;i<size;++i)
		{
			if(_vec[i].first == _row && _vec[i].second == _col)
				return true;
		}
		return false;
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
