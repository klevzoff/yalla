#pragma once
#ifndef yalla_gaussian_elimination_info_h
#define yalla_gaussian_elimination_info_h

/*!
 *  \file GaussianEliminationInfo.h
 *  \brief Structure to store Gaussian Elimination information
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Profiler/Profiler.h"
#include "yalla/Utils/MemoryCheck/MemoryCheck.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
//! \brief Structure to store GaussianEliminationInfo execution information
struct GaussianEliminationInfo
{
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef GaussianEliminationInfo this_type;
	//@}
	//! \brief Memory consumption of the BiCGStab solver
	MemoryCheck * m_memory_used;
	//! \brief Profiler for the different steps of the AMG solver
	Profiler * m_profiler;

	/*! \name Public Constructors
	 *
	 *  Public constructors of the class GaussianEliminationInfo
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  If the verbosity level is greater than 0, allocates the memory and profiling logger
	 *
	 *  \param[in] _verboseLvl Verbosity level
	 */
	explicit GaussianEliminationInfo(const int _verboseLvl)
	{
		m_profiler = 0;
		m_memory_used = 0;
		if(_verboseLvl>0)
		{
			m_profiler = new Profiler();
			m_memory_used = new MemoryCheck();
		}
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class GaussianEliminationInfo
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the memory and profiling pointers
	 */
	virtual ~GaussianEliminationInfo()
	{
		delete m_profiler;
		m_profiler = 0;
		delete m_memory_used;
		m_memory_used = 0;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class GaussianEliminationInfo. \n
	 *  These methods give access in read-only mode to the datas/information of the class GaussianEliminationInfo
	 */

	//@{
	/*! \brief  Return the class name. 
	 *
	 *  \return "GaussianEliminationInfo" The name of the class
	 */
	const char* getClassName() const
	{
		return "GaussianEliminationInfo";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
