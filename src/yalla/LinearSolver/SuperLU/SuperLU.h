#pragma once
#ifndef yalla_super_lu_h
#define yalla_super_lu_h

/*!
 *  \file SuperLU.h
 *  \brief Wrapper around the SuperLU library
 *  \date 12/26/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */
#ifdef WITH_MPI
#include "superludist/superlu_ddefs.h"
#else
#include "superlu/slu_ddefs.h"
#endif
#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*!
 *  \class SuperLU
 *  \brief Wrapper around the SuperLU library
 * 
 *  \details
 *  This class is a wrapper around the SuperLU(Dist) library. \n
 *  The SuperLuDist grid is computed in the constructor. \n
 *  The setup method prepare the SuperLU(Dist) library and the solve method call the SuperLU(Dist) solver. \n
 *  One should read the SuperLU(Dist) documentation to figure out the role of the different parameters/methods. \n
 *  If WITH_MPI is defined, this class wraps around SuperLUDist, otherwise single-threaded SuperLU. \n
 *  The interfaces of those two libraries differ in significant ways.
 *  
 *  \todo Add support for multi-threaded SuperLU (SuperLU_MT)
 *  \warning To link SuperLUDist while using the MKL, one might need to edit the file superlu_defs.h at line 554 and remove the slamch and dlamch declaration from the extern "C"
 */
class SuperLU
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef SuperLU this_class;
	//@}
 private:
	//! \brief SuperLU options
	superlu_options_t m_options;
	//! \brief SuperLU stats
	SuperLUStat_t m_stat;
	//! \brief SuperLU matrix A
	SuperMatrix m_a;
#ifdef WITH_MPI
  //! \brief SuperLU matrix permutation structure
	ScalePermstruct_t m_scale_perm_struct;
	//! \brief SuperLU L and U factors storage structure
	LUstruct_t m_lu_struct;
	//! \brief SuperLU solution structure
	SOLVEstruct_t m_solve_struct;
	//! \brief SuperLU computational grid
	gridinfo_t m_grid;
  //! \brief SuperLU attribute
	int m_nprow;
	//! \brief SuperLU attribute
	int m_npcol;
#else
  //! \brief Row permutation data
  int * m_perm_row;
  //! \brief Column permutation data
  int * m_perm_col;
  //! \brief L factor matrix
  SuperMatrix m_L;
  //! \brief U factor matrix
  SuperMatrix m_U;
  //! \brief Right-hand side / solution matrix B
  SuperMatrix m_B;
#endif
	//! \brief SuperLU attribute
	int m_m;
	//! \brief SuperLU attribute
	int m_n;
	//! \brief SuperLU attribute
	double m_residual[1];
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SuperLU
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Compute the SuperLUDist grid
	 */
	explicit SuperLU()
	{
#ifndef WITH_SUPERLU
		YALLA_ERR("Error, SuperLU not available")
#endif
#ifdef WITH_SUPERLU
#ifdef WITH_MPI
	  this->computeNumRowAndCols();
		superlu_gridinit(ParUtils::ThisComm, m_nprow, m_npcol, &m_grid);
#endif
#endif
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SuperLU
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class
	 */
	virtual ~SuperLU()
	{
#ifdef WITH_SUPERLU
#ifdef WITH_MPI
    superlu_gridexit(&m_grid);
#endif
#endif
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SuperLU. \n
	 *  These methods give access in read-only mode to the datas/information of the class SuperLU
	 */
	//@{
	const char* getClassName() const
	{
		return "SuperLU";
	}
	//@}

	/*! \name Public setup
	 *
	 *  Setup method of the SuperLU solver. \n
	 */
	//@{
	/*! \brief Apply the setup phase of the SuperLU solver
	 *
	 *  Prepare the SuperLU solver
	 *
	 *  \tparam MatrixType The type of the matrix
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[out] _lhs The lhs
	 */
	template<typename MatrixType, typename VectorType>
    void setup(const MatrixType& _A, const VectorType& _rhs, VectorType& _lhs)
	{
#ifdef WITH_SUPERLU
#ifdef WITH_MPI
		int iam = m_grid.iam;
		if(iam >= m_nprow * m_npcol)
			return;
#endif

		const typename MatrixType::impl_type::graph_type& matrixGraph = _A.getMatrixGraph();
#ifdef WITH_MPI
		m_m = _A.getGlobalNbRows();
		m_n = _A.getGlobalNbCols();
    const int startingRow = _A.getMapping(0);
		const int locNbRows = _A.getNbRows();
#else
    m_m = _A.getNbRows();
    m_n = _A.getNbCols();
#endif
    const int nnz = _A.getNnz();
		double* dataPtr = const_cast<double*>(_A.getPointeur());
		int* colPtr = const_cast<int*>(matrixGraph.getColPtr());
		int* rowPtr = const_cast<int*>(matrixGraph.getRowPtr());
#ifdef WITH_MPI
		dCreate_CompRowLoc_Matrix_dist(&m_a, m_m, m_n, nnz, locNbRows, startingRow, dataPtr, colPtr, rowPtr, SLU_NR_loc, SLU_D, SLU_GE);
#else
    dCreate_CompCol_Matrix(&m_a, m_m, m_n, nnz, dataPtr, colPtr, rowPtr, SLU_NR, SLU_D, SLU_GE);
#endif
#endif
	}
	//@}

	/*! \name Public solve
	 *
	 *  Solve method of the SuperLU solver. \n
	 */
	//@{

	/*! \brief Solve method of the SuperLU solver
	 *  
	 *  Solve the linear system with the SuperLU library
	 *
	 *  \tparam MatrixType The type of the matrix
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[out] _lhs The lhs
	 */
	template<typename MatrixType, typename VectorType>
    int solve(const MatrixType& _A, const VectorType& _rhs, VectorType& _lhs)
	{
#ifdef WITH_SUPERLU
#ifdef WITH_MPI
		int iam = m_grid.iam;
		if(iam >= m_nprow * m_npcol)
			return YALLA_SUCCESS;
#endif

		int info = 0;
		int nbRhs = 1;
		_lhs = _rhs;
#ifdef WITH_MPI
    /* Set defaul options */
		set_default_options_dist(&m_options);
    /* Initialize the statistics variables. */
    PStatInit(&m_stat);
#else
    /* Set defaul options */
    set_default_options(&m_options);
    m_options.Trans = TRANS;
    /* Initialize the statistics variables. */
    StatInit(&m_stat);
#endif
		m_options.PrintStat = NO;
    m_options.ColPerm = COLAMD;
    m_options.IterRefine = SLU_DOUBLE;

#ifdef WITH_MPI
    /* Initialize ScalePermstruct and LUstruct. */
		ScalePermstructInit(m_m, m_n, &m_scale_perm_struct);
		LUstructInit(m_m, m_n, &m_lu_struct);
    /* Call the linear equation solver. */
		pdgssvx(&m_options, &m_a, &m_scale_perm_struct, &_lhs(0), m_n, nbRhs, &m_grid, &m_lu_struct, &m_solve_struct, m_residual, &m_stat, &info);
    /* Free the memory */
    PStatFree(&m_stat);
    ScalePermstructFree(&m_scale_perm_struct);
		Destroy_LU(m_n, &m_grid, &m_lu_struct);
		LUstructFree(&m_lu_struct);
		if ( m_options.SolveInitialized ) 
		{
			dSolveFinalize(&m_options, &m_solve_struct);
		}
#else
    /* Allocate and initialize RHS and permutation vectors */
    dCreate_Dense_Matrix(&m_B, m_m, nbRhs, &_lhs(0), m_m, SLU_DN, SLU_D, SLU_GE);
    m_perm_row = new int[m_m];
    m_perm_col = new int[m_n];
    /* Call the linear equation solver. */
    dgssv(&m_options, &m_a, m_perm_col, m_perm_row, &m_L, &m_U, &m_B, &m_stat, &info);
    /* Free the memory */
    delete[] m_perm_row;
    delete[] m_perm_col;
    StatFree(&m_stat);
    Destroy_SuperMatrix_Store(&m_B);
    Destroy_SuperNode_Matrix(&m_L);
    Destroy_CompCol_Matrix(&m_U);
#endif
    if(!info)
			return YALLA_SUCCESS;
		else
			return YALLA_ERROR;
#endif
		return YALLA_ERROR;
	}
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class SuperLU. \n
	 *  These methods provide various utilities to the class SuperLU
	 */
	//@{
 private:
	//! \brief Compute the SuperLU grid
#ifdef WITH_MPI
	void computeNumRowAndCols()
	{
		int counter;
		const int NumProcs = ParUtils::getNumProc();
		for ( counter = 1; counter*counter <= NumProcs; ++counter ) 
			;
		bool done = false ;
		for ( m_nprow = counter-1 ; done == false ; ) 
		{
			m_npcol = NumProcs / m_nprow;
			if ( m_nprow * m_npcol == NumProcs ) 
				done = true; 
			else 
				m_nprow-- ; 
		}
		if( m_nprow <1)
			m_nprow = 1;
		m_npcol = NumProcs/m_nprow;
		if( m_nprow <= 0 || m_npcol <= 0)
			YALLA_ERR("Problem computing 2D grid map for SuperLUDist")
  }
#endif
	//@}
};
	
YALLA_END_NAMESPACE
#endif
