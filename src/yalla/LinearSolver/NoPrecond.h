#pragma once
#ifndef yalla_no_precond_h
#define yalla_no_precond_h

/*!
 *  \file NoPrecond.h
 *  \brief No preconditionner "implementation"
 *  \date 12/26/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class NoPrecond
 *  \brief No preconditionner implementation
 *
 *  This class is an empty shell when one wants to use a solver without a preconditionner. \n
 *  It implements the "required" methods, but they actually do nothing.
 */
class NoPrecond
{
	//! \name Public Typedefs
	//@{
	typedef NoPrecond this_type;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class NoPrecond
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Do nothing
	 */
	explicit NoPrecond()
	{

	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class NoPrecond
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing
	 */
	virtual ~NoPrecond()
	{

	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class NoPrecond. \n
	 *  These methods give access in read-only mode to the datas/information of the class NoPrecond
	 */
	//@{
	/*! \brief Return the class name. 
	 *
	 *  \return "NoPrecond" The name of the class
	 */
	const char* getClassName() const
	{
		return "NoPrecond";
	}
	//@}

	/*! \name Public setup
	 *
	 *  Setup method of the NoPrecond class. \n
	 */
	//@{
	/*! \brief Setup method of the NoPrecond class
	 *
	 *  Do nothing
	 *
	 *  \tparam Matrix The type of the matrix
	 *  \tparam Vector The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[out] _lhs The lhs
	 */
	template<typename Matrix, typename Vector>
    void setup(const Matrix& _A, const Vector& _rhs, Vector& _lhs)
	{

	}
	//@}

	/*! \name Public solve
	 *
	 *  Solve method of the NoPrecond. \n
	 */
	//@{
	/*! \brief Solve method of the NoPrecond class
	 *
	 *  Just copy the rhs in the lhs
	 *
	 *  \tparam Matrix The type of the matrix
	 *  \tparam Vector The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[out] _lhs The lhs
	 */
	template<typename Matrix, typename Vector>
    void solve(const Matrix& _A, const Vector& _rhs, Vector& _lhs)
	{
		BLAS::copy(_rhs,_lhs);
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
