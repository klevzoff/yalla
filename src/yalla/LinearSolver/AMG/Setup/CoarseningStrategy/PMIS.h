#pragma once
#ifndef yalla_pmis_h
#define yalla_pmis_h

/*!
 *  \file PMIS.h
 *  \brief PMIS coarsening strategy for the AMG solver
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <limits>
#include <stdlib.h>
#include <time.h>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToType.h"
#include "yalla/Utils/TypeTraits/PromoteToVector.h"
#include "yalla/Utils/Math/Math.h"
#include "yalla/Utils/synchronizer/SynchronizerMng.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
//! Forward declaration
class NoPrecond;
	
/*!
 *  \class PMIS
 *  \brief PMIS coarsening strategy for the AMG solver
 * 
 *  \details
 *  This algorithm mimics a sequential PMIS coarsening in a distributed environment.
 *
 *  This coarsening strategy has two main steps:
 *  - During the first step, the connection strength matrix is built, to determine which unknown will be fine or coarse
 *  - During the second step, the splitting between fine and coarse unknowns is done
 *
 *  The strength of connection matrix is computed following the PMIS algorithm
 *
 *  The splitting is done in one step:
 *  - When the connection matrix is processed, a random number is added for each node measure, then making each measure unique. The node with the max measure is then selected and made a coarse node. All nodes influenced by this node are then made fine.
 *
 *  \tparam MatrixType The type of the matrix
 *
 *  \warning This class was implemented for distributed execution only
 *  \warning This class is most probably broken. It was implemented before the last "big changes" in the data structure (when column indices were stored with their GIDs and not LIDs), therefore the implementation has to be adapted.
 *
 *  \todo Fix this class
 *  \todo The fact that we exactly mimic the sequential algorithm implies plenty of "useless" communications, therefore making this algorithm not scalable. A possibility would be to treat first only the boundary nodes (implying communications) taking into account only the mesure of those nodes and then the interior nodes (that would become embarassingly parallel). However, this approach would probably degrade the grid and therefore the convergence rate, but for an increased scalability ... Both approach should probably be compared
 */
template<typename MatrixType>
class PMIS
{
 public:
//! \name Public Typedefs
	//@{
	//! \brief Type of the matrix
	typedef MatrixType matrix_type;
	//! \brief Data type of the matrix
	typedef typename matrix_type::data_type data_type;
	//! \brief Consistent vector type
	typedef typename PromoteToVector<matrix_type, data_type>::data_type vector_type;
	//! \brief Consistent integer matrix
	typedef typename PromoteToType<matrix_type, int>::data_type IntegerMatrix;
	//! \brief Consistent integer vector
	typedef typename PromoteToVector<matrix_type, int>::data_type IntegerVector;
	//! Type of this class
	typedef PMIS<matrix_type> this_class;
	//@}
 private:
	//! \brief Pointer on the connection matrix hierarchy
	IntegerMatrix* m_connection_matrix;
	//! \brief Pointer on the unknown splitting hierarchy
	IntegerVector* m_splitting;
	//! \brief Threshold to determine strong connection
	double m_theta;
	//! \brief Max number of level in the grid hierarchy
	int m_max_level;
	//! \brief Enum for the different state of unknowns
	enum nodeStatus
	{
		//! \brief Fine node
		F_NODE = 0,
		//! \brief Coarsed node
		C_NODE = 1,
		//! \brief Unprocessed node
		U_NODE = 2
	};
	//! \brief Enum to label nodes
	enum nodeLocation
	{
		//! \brief Interior node
		INT_NODE = 1,
		//! \brief Boundary node
		BND_NODE = 0
	};
 public:
/*! \name Public Constructors
	 *
	 *  Public constructors of the class PMIS
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Calls the method setDefaults
	 */
	PMIS()
	{
		this->setDefaults();
	}
//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class PMIS
	 */
	//@{

/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the connection matrix and the splitting
	 */
	virtual ~PMIS()
	{
		this->deallocate();
	}
	//@}

/*! \name Public Getter
	 *
	 *  Public Getter of the class PMIS. \n
	 *  These methods give access in read-only mode to the datas/information of the class PMIS
	 */

	/*! \brief  Return the class name. 
	 *
	 *  \return "PMIS" The name of the class
	 */
	const char* getClassName() const
	{
		return "PMIS";
	}
	//@}

/*! \name Public Setter
	 *
	 *  Public Setter of the class PMIS. \n
	 *  These methods give access in read/write mode to the datas/information of the class PMIS
	 */
	//@{

/*! \brief Set the maximum number of grids used
	 *
	 *  \param[in] _maxLvl The max number of grids used
	 */
	void setMaxLevel(const int _maxLvl)
	{
		m_max_level =_maxLvl;
	}
	
/*! \brief Set the strength connection threshold
	 *
	 *  Set the strength connection threshold to establish the strong connections while building the grid hierarchy
	 *
	 *  \param[in] _strength Strength threshold
	 */
	void setStrengthThreshold(const double _strength)
	{
		m_theta = _strength;
	}
	//@}

/*! \name Public setup
	 *
	 *  Setup method of the PMIS strategy. \n
	 */
	//@{

	/*! \brief Allocate the memory required for the class
	 *
	 *
	 *  Allocate the memory for the connection matrix and the splitting hierarchy
	 */
	void setup()
	{
		this->deallocate();
		m_connection_matrix = new IntegerMatrix[m_max_level];
		m_splitting = new IntegerVector[m_max_level];
	}
//@}

	/*! \name Public apply
	 *
	 *  Setup method of the PMIS strategy. \n
	 */
	//@{

	/*! \brief Build the connection matrix and the splitting for the current level
	 *
	 *  Apply the PMIS coarsening strategy and the splitting. \n
	 *  Call the method computeStrengthMatrix to build the connection matrix for the current level. \n
	 *  Call the method split to separate the coarse and fine unknowns at the current level . \n
	 *
	 *  \param[in] _matrix The matrix at the current level
	 *  \param[in] _rhs The rhs at the current level
	 *  \param[out] _lhs The lhs at the current level
	 *  \param[in] _currentLevel The current level
	 */
	void apply(const matrix_type& _matrix, const vector_type& _rhs, vector_type& _lhs, const int _currentLevel)
	{
		this->computeStrengthMatrix(_matrix,_currentLevel);
		this->split(_lhs,_currentLevel);
	}
	//@}

 private:
/*! \name Private Utils
	 *
	 *  Private Utils of the class PMIS strategy. \n
	 *  These methods provide various utilities to the class PMIS strategy
	 */
	//@{
	
/*! \brief Apply the PMIS coarsening strategy for a DistDenseMatrix
	 *
	 *  \tparam DataType The data type of the working matrix
	 *  \tparam MatrixImpl The actual matrix implementation
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 *  \param[in] _currentLevel The current level
	 *
	 *  \todo Implement this method
	 */
	template<typename DataType, typename MatrixImpl>
		void computeStrengthMatrix(const DistDenseMatrix<DataType,MatrixImpl>& _matrix, const int _currentLevel)
	{
		YALLA_ERR("Not implemented yet");
	}
	
/*! \brief Apply the PMIS coarsening strategy for a DistSparseMatrix
	 *
	 *  The algorithm is the following:
	 *  - For each row, the maximum absolute value is located and used to compare connection strength wrt to all other values (this is done in the method maxNegOffDiagonal)
	 *  - Then for each entry in the matrix row, if A(i,j) >= threshold * maxAbs, the connection is made strong and the value of the connection matrix at the index (i,j) is set to one.
	 *  - Otherwise, the value of the connection matrix at the index (i,j) is set to zero.
	 *
	 *  \tparam DataType The data type of the working matrix
	 *  \tparam MatrixImpl The actual matrix implementation
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 *  \param[in] _currentLevel The current level
	 *
	 *  \warning This method will actually work only for a CSR matrix as the pointers built are in CSR format. It is possible to make this method generi by simply copying the working matrix and properly setting all the values. This will be done at the cost of some extra useless storage
	 */
	template<typename DataType, typename MatrixImpl>
		void computeStrengthMatrix(const DistSparseMatrix<DataType,MatrixImpl>& _matrix, const int _currentLevel)
	{
		typedef typename MatrixImpl::const_row_iterator_type rowIteratorType;
		typedef typename MatrixImpl::const_col_iterator_type colIteratorType;
		
		const int nbRows = _matrix.getNbRows();
		const int nbCols = _matrix.getNbCols();
		const int nbNnz = this->countStrengthMatrixNnz(_matrix);
		const int nbGlobalRows = _matrix.getGlobalNbRows();
		const int nbGlobalCols = _matrix.getGlobalNbCols();
		const int* mapping = _matrix.getMapping();
		int* rowPtr = new int[nbRows+1];
		int* colPtr = new int[nbNnz];
		rowPtr[0] = 0;

		for(rowIteratorType rowIt = _matrix.rowBegin(); rowIt != _matrix.rowEnd();++rowIt)
		{
			rowPtr[rowIt.index()+1] = rowPtr[rowIt.index()];
			const data_type max = this->maxNegOffDiagonal(_matrix,rowIt);
			for(colIteratorType colIt = _matrix.colBegin(rowIt); colIt !=_matrix.diagonalElement(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
				{
					colPtr[rowPtr[rowIt.index()+1]] = colIt.index();
					++rowPtr[rowIt.index()+1];
				}
			}
			for(colIteratorType colIt = _matrix.diagonalElement(rowIt).next(); colIt != _matrix.colEnd(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
				{
					colPtr[rowPtr[rowIt.index()+1]] = colIt.index();
					++rowPtr[rowIt.index()+1];
				}
			}
		}
		m_connection_matrix[_currentLevel].initialize(nbRows,nbCols,nbNnz,nbGlobalRows,nbGlobalCols,mapping,rowPtr,colPtr);
		m_connection_matrix[_currentLevel].fill(static_cast<data_type>(1));
		delete[] rowPtr;
		delete[] colPtr;
	}

	// ColSumMap are computed in the following way:
	// - We need to compute the map for a col sum on the strength matrix. The strength matrix is not the fine/coarse matrix but a subset of this one.
	// - Therefore we have two possibilities: recomputing all informations, which would imply global communication, or using the SPMV informations of the fine/coarse matrix
	// - I have chosen to use the SPMV information. The main drawback is that we could send more information than necessary (in case a column is not present any more on a proc), and eventually, a proc might have no more information at all to send to one of its neighbour. In the later case, we will do a completely useless communication.
	// Still have to do: change the synchronize method of the generalsync to pass as a parameter a sendBuf and a recvBuffer

/*! \brief Compute the splitting between coarse and fine unknowns
	 *
	 *  First of all, I strongly believe that this method is broken due to the changes in the data structures since I implemented it. One has to change all exchanges information based on GIDs for columns. In addition, it seens that I implemented it for a blo row distribution and not in a more generic way. So all computations done using the averageNumOfRowsByProc attribute need to be fixed.
	 *
	 *  Nevertheless, here is the algorithm implemented.
	 *
	 *  Initialisation phase:
	 *  - The number of nodes is equal to the number of rows in the connection matrix (used to allocate the splitting array).
	 *  - The strength measure array lambda is then initialised with the connection matrix (each node is attributed a value depending on how many nodes it strongly influence: lambda[i] = sum m_connection_matrix(i,j)). To compute this array, we need to compute the local contributions to the lambda array (columns located on the same proc) but also to do a column sum of the matrix (as some column entries might not be on the same proc) and here come the problems, as the matrix is distributed by rows. Therefore, the method computeColSumMap compute the communication map for the current matrix and store it in the singleton synchronizer manager syncMng (to be able to reuse it later without having to recompute it). The communication map is a subset of the SPMV map that has already computed. Therefore, this map is used despite the fact that it might imply additional/useless communications. Once we have all required information, we can compute the lambda array.
	 *  - The pool of nodes is initialized to unprocessed
	 *  - Nodes are labelled as interior or boundaries
	 *  - Then a random number is added to the weight array, to make each measure unique.
	 *  - All nodes that then have a measure inferior to one are set to fine nodes and removed from the pool
	 *  
	 *  Splitting phase (single pass):
	 *  - The node with the highest measure is located.
	 *  - If the node is interior (if(nodePosition[NodeWithMaxLambda]==INT_NODE)):
	 *    - The node become a coarse node, all neighbours a fine node 
	 *    - The weight vector (is updated)
	 *  - If the node is boundary:
	 *    - We look at the weight of the neighbouring nodes on the other procs:
	 *      - If the previously selected node still has the biggest weight (if(lambdaIsStillMax)), wemake it a coarse node and set all neighbouring nodes (including those on neighbouring nodes) as fine nodes
	 *      - Otherwise, we do nothing (or it won't mimic exactly the sequential version of the PMIS algorithm)
	 *
	 *  Several problems lie with this approach:
	 *  - If a node picked up is interior, we still have to open the communication channels, just in case one of the neighbouring proc has picked up a boundary node (even if it's not the case). Therefore we impose a lot of (eventually) useless synchronization that will destroy the scalability
	 *  - Even if a proc is done processing its nodes, we can't stop the communications, as its neighbouring procs might still need to exchange information. And again a lot of (eventually) useless synchronization that will destroy the scalability
	 *
	 *  \param[in] _vector Vector from which we will extract the SPMV communication map
	 *  \param[in] _currentLevel The current level
	 */
	void split(const vector_type& _vector, const int _currentLevel)
	{
		srand(time(NULL));
		typedef typename IntegerMatrix::impl_type::const_row_iterator_type rowIt;
		typedef typename IntegerMatrix::impl_type::const_col_iterator_type colIt;
		const int nbLocalNodes = m_connection_matrix[_currentLevel].getNbRows();
		const int numProc = ParUtils::getNumProc();
		const int offsetGIDToLid = m_connection_matrix[_currentLevel].rowBegin().globalIndex();
		// This will work only for a bloc row distribution
		const int averageNumOfRowsByProc = m_connection_matrix[_currentLevel].getGlobalNbRows()/numProc;

		// Weight vector
		float* lambda, *lambdaDup;
		MPI_Alloc_mem(sizeof(float)*nbLocalNodes, MPI_INFO_NULL, &lambda);
		int* NodePoolIndices = new int[nbLocalNodes];
		int* nodePosition = new int[nbLocalNodes];

		// Initialisation phase
		for(int i=0;i<nbLocalNodes;++i)
		{
			NodePoolIndices[i] = U_NODE;
			lambda[i] = 0.;
			nodePosition[i] = INT_NODE;
		}

		// We start by counting how many information need to be sent to another proc using the SPMV communication map (as the col sum map is always a subset of this one)
		SynchronizerSingleton& syncMng = SynchronizerSingleton::instance();
		typedef typename SynchronizerSingleton::synchronizer_mng_1_type::key_type key_type;
		key_type this_key = key_type(SynchronizerSingleton::CommMap::col_sum_map, std::make_tuple(&m_connection_matrix[_currentLevel]));

		if(!syncMng.getValue(this_key))
			this->computeColSumMap(m_connection_matrix[_currentLevel], _vector);

		auto sync = syncMng.getValue(this_key);
		// We build the send/recv buffers
		const int nbGhosts = sync->getGeneralGhosts().getNbElements();
		const int nbShared = sync->getGeneralShared().getNbElements();

		std::map<int, int> gidToSharedLids;

		int* sendBuffer = new int[nbShared];
		int* recvBuffer = new int[nbGhosts];
		const int* sharedGIDs = sync->getGeneralShared().getGID();
		// Worst case, the node will have nbShared neighbours (this is the max "theoritical" size of the array)
		MPI_Alloc_mem(sizeof(float)*nbShared, MPI_INFO_NULL, &lambdaDup);
		for(int i=0;i<nbShared;++i)
		{
			sendBuffer[i] = 0;
			lambdaDup[i] = -1;
			gidToSharedLids[sharedGIDs[i]] = i;
		}

		for(int i=0;i<nbGhosts;++i)
			recvBuffer[i] = 0;

		// Put relevant information in the send buffer (those which need to be communicated to other procs)
		for(rowIt row = m_connection_matrix[_currentLevel].rowBegin(); row != m_connection_matrix[_currentLevel].rowEnd(); ++row)
		{
			// Only values on the non-diagonal blocks need to be sent
			for(colIt col = m_connection_matrix[_currentLevel].colBegin(row);col != m_connection_matrix[_currentLevel].ownColBegin(row);++col)
				sendBuffer[gidToSharedLids[col.index()]] += *col;
			// Only values on the non-diagonal blocks need to be sent
			for(colIt col = m_connection_matrix[_currentLevel].ownColEnd(row);col != m_connection_matrix[_currentLevel].colEnd(row);++col)
				sendBuffer[gidToSharedLids[col.index()]] += *col;
		}

		// Start the communication process
		sync->synchronize(recvBuffer,sendBuffer);

		// Preparing one sided-comm for splitting
		MPI_Group wholeGroup, neighbourGroup;
		MPI_Win lambdaWin;
		// Shared infos
		const int sizeNeighbourGroup = sync->getGeneralShared().getNbReceiver();
		const int* sharedReceiverList = sync->getGeneralShared().getReceiverList();
		int* toRank = new int[sizeNeighbourGroup];
		for(int i=0;i<sizeNeighbourGroup;++i)
			toRank[i] = sharedReceiverList[i];

		// Whole comm group
		MPI_Comm_group(ParUtils::ThisComm, &wholeGroup);
		// Reading comm group
		MPI_Group_incl(wholeGroup, sizeNeighbourGroup, toRank, &neighbourGroup);

		MPI_Win_create(lambda, nbLocalNodes*sizeof(float), sizeof(float), MPI_INFO_NULL, ParUtils::ThisComm, &lambdaWin);

		// Compute local contributions of the weight vector (elements in the diagonal block)
		for(rowIt row = m_connection_matrix[_currentLevel].rowBegin(); row != m_connection_matrix[_currentLevel].rowEnd(); ++row)
		{
			for(colIt col = m_connection_matrix[_currentLevel].ownColBegin(row);col != m_connection_matrix[_currentLevel].ownColEnd(row);++col)
				lambda[col.index()-offsetGIDToLid] += *col;
		}

		sync->isSynchronized();

		// Buffers are now synchronized, therefore we can add global contrinution to the weight array
		const int* ghostGIDs = sync->getGeneralGhosts().getGIDs();
		for(int i=0;i<nbGhosts;++i)
		{
			lambda[ghostGIDs[i]-offsetGIDToLid] += recvBuffer[i];
			nodePosition[ghostGIDs[i]-offsetGIDToLid] = BND_NODE;
		}

		// Add the random number to the weight vector and remove weaks connection
		for(int i=0;i<nbLocalNodes;++i)
		{
			lambda[i] += (static_cast<float>( rand() ) / RAND_MAX);
			if(lambda[i] <= 1)
			{
				NodePoolIndices[i] = F_NODE;
				lambda[i] = -1;
			}
		}

		delete[] sendBuffer;
		delete[] recvBuffer;

		// Splitting phase
		int everybodysNotDone = 0;
		int iAmNotDone = 0;

		MPI_Win doneWin;
		MPI_Win_create(&everybodysNotDone, sizeof(int), sizeof(int), MPI_INFO_NULL, ParUtils::ThisComm, &doneWin);

		while(everybodysNotDone!=numProc) // As long as every proc hasn't proceed all its nodes
		{
			MPI_Win_post(neighbourGroup, 0, lambdaWin);
			MPI_Win_start(neighbourGroup, 0, lambdaWin);

			for(int i=0;i<nbShared;++i)
				lambdaDup[i] = -1;

			everybodysNotDone = 0;

			if(!iAmNotDone)
			{
				int NodeWithMaxLambda = 0;
				float MaxLambda = lambda[0];
				for(int i=1;i<nbLocalNodes;++i)
				{
					if(lambda[i] > MaxLambda)
					{
						NodeWithMaxLambda = i;
						MaxLambda = lambda[i];
					}
				}
				if(nodePosition[NodeWithMaxLambda]==INT_NODE) // The node with max lambda is interior ...
				{
					NodePoolIndices[NodeWithMaxLambda] = C_NODE;
					lambda[NodeWithMaxLambda] = 0;
					int neighbourNodes = -1;
					rowIt rowOfMaxLambda = m_connection_matrix[_currentLevel].rowIterator(NodeWithMaxLambda);
					// ... so are its neighbours
					for(colIt col = m_connection_matrix[_currentLevel].colBegin(rowOfMaxLambda); col!=m_connection_matrix[_currentLevel].colEnd(rowOfMaxLambda);++col)
					{
						neighbourNodes = col.globalIndex()-offsetGIDToLid;
						NodePoolIndices[neighbourNodes] = F_NODE;
						lambda[neighbourNodes] = -1;
					}
					MPI_Win_complete(lambdaWin);
					MPI_Win_wait(lambdaWin);
					MPI_Win_post(neighbourGroup, 0, lambdaWin);
					MPI_Win_start(neighbourGroup, 0, lambdaWin);
				}
				else // The node is boundary
				{
					// Look for the weight of all neighbouring nodes (including on other procs)
					int neighbourNodes = -1;
					rowIt rowOfMaxLambda = m_connection_matrix[_currentLevel].rowIterator(NodeWithMaxLambda);
					int counter = 0;
					for(colIt col = m_connection_matrix[_currentLevel].colBegin(rowOfMaxLambda); col!=m_connection_matrix[_currentLevel].ownColBegin(rowOfMaxLambda);++col)
					{
						int relevantProc = -1;
						int found = 0;
						const GeneralShared& shared = sync->getGeneralShared();
						for(int i=0;i<sizeNeighbourGroup && !found;++i)
						{
							const int* gids = shared.getGIDsForProc(i);
							for(int j=0;j<shared.getNbElementsByProc(i);++j)
							{
								if(col.globalIndex()==gids[j])
								{
									relevantProc = sharedReceiverList[i];
									found = 1;
									break;
								}
							}
						}

						MPI_Get(&lambdaDup[counter], 1, MPI_FLOAT, relevantProc, col.globalIndex()-averageNumOfRowsByProc*relevantProc, 1, MPI_FLOAT, lambdaWin);
						++counter;
					}
					for(colIt col = m_connection_matrix[_currentLevel].ownColEnd(rowOfMaxLambda); col!=m_connection_matrix[_currentLevel].colEnd(rowOfMaxLambda);++col)
					{
						int relevantProc = -1;
						int found = 0;
						const GeneralShared& shared = sync->getGeneralShared();
						for(int i=0;i<sizeNeighbourGroup && !found;++i)
						{
							const int* gids = shared.getGIDsForProc(i);
							for(int j=0;j<shared.getNbElementsByProc(i);++j)
							{
								if(col.globalIndex()==gids[j])
								{
									relevantProc = sharedReceiverList[i];
									found = 1;
									break;
								}
							}
						}
						MPI_Get(&lambdaDup[counter], 1, MPI_FLOAT, relevantProc, col.globalIndex()-averageNumOfRowsByProc*relevantProc, 1, MPI_FLOAT, lambdaWin);
						++counter;
					}

					MPI_Win_complete(lambdaWin);
					MPI_Win_wait(lambdaWin);
					MPI_Win_post(neighbourGroup, 0, lambdaWin);
					MPI_Win_start(neighbourGroup, 0, lambdaWin);

					// Check all the neighbouring lambda values
					bool lambdaIsStillMax = true;
					for(int i=0;i<nbShared;++i)
					{
						if(MaxLambda<lambdaDup[i])
						{
							lambdaIsStillMax = false;
							break;
						}
					}

					if(lambdaIsStillMax) // The node picked has the biggest weight
					{
						NodePoolIndices[NodeWithMaxLambda] = C_NODE;
						lambda[NodeWithMaxLambda] = 0;
						// Make all local neighbouring nodes fine nodes and update weight vector
						for(colIt col = m_connection_matrix[_currentLevel].ownColBegin(rowOfMaxLambda); col!=m_connection_matrix[_currentLevel].ownColEnd(rowOfMaxLambda);++col)
						{
							neighbourNodes = col.globalIndex()-offsetGIDToLid;
							NodePoolIndices[neighbourNodes] = F_NODE;
							lambda[neighbourNodes] = -1;
						}
						// Make all global (on other procs) neighbouring nodes fine nodes and update weight vector
						counter = 0;
						for(colIt col = m_connection_matrix[_currentLevel].colBegin(rowOfMaxLambda); col!=m_connection_matrix[_currentLevel].ownColBegin(rowOfMaxLambda);++col)
						{
							int relevantProc = -1;
							int found = 0;
							const GeneralShared& shared = sync->getGeneralShared();
							for(int i=0;i<sizeNeighbourGroup && !found;++i)
							{
								const int* gids = shared.getGIDsForProc(i);
								for(int j=0;j<shared.getNbElementsByProc(i);++j)
								{
									if(col.globalIndex()==gids[j])
									{
										relevantProc = sharedReceiverList[i];
										found = 1;
										break;
									}
								}
							}
							
							if(lambdaDup[counter]!=0)
								MPI_Put(&lambda[neighbourNodes], 1, MPI_FLOAT, relevantProc, col.globalIndex()-averageNumOfRowsByProc*relevantProc, 1, MPI_FLOAT, lambdaWin);
							++counter;
						}
						for(colIt col = m_connection_matrix[_currentLevel].ownColEnd(rowOfMaxLambda); col!=m_connection_matrix[_currentLevel].colEnd(rowOfMaxLambda);++col)
						{
							int relevantProc = -1;
							int found = 0;
							const GeneralShared& shared = sync->getGeneralShared();
							for(int i=0;i<sizeNeighbourGroup && !found;++i)
							{
								const int* gids = shared.getGIDsForProc(i);
								for(int j=0;j<shared.getNbElementsByProc(i);++j)
								{
									if(col.globalIndex()==gids[j])
									{
										relevantProc = sharedReceiverList[i];
										found = 1;
										break;
									}
								}
							}

							if(lambdaDup[counter]!=0)
								MPI_Put(&lambda[neighbourNodes], 1, MPI_FLOAT, relevantProc, col.globalIndex()-averageNumOfRowsByProc*relevantProc, 1, MPI_FLOAT, lambdaWin);
							++counter;
						}
					}
					else // The picked up node wasn't the one with the biggest lambda in its neighbourhood
					{
						; // Do nothing
					}
				}
			}
			else // The proc is done processing its nodes but we still need communication windows for the neighbouring procs that are not done
			{
				MPI_Win_complete(lambdaWin);
				MPI_Win_wait(lambdaWin);
				MPI_Win_post(neighbourGroup, 0, lambdaWin);
				MPI_Win_start(neighbourGroup, 0, lambdaWin);
			}

			MPI_Win_complete(lambdaWin);
			MPI_Win_wait(lambdaWin);

			if(Math::myMax(lambda,nbLocalNodes)<=1)
				iAmNotDone = 1;
	
			MPI_Win_fence((MPI_MODE_NOPUT | MPI_MODE_NOPRECEDE), doneWin);
			for(int i=0;i<numProc;++i)
				MPI_Accumulate(&iAmNotDone, 1, MPI_INT, i, 0, 1, MPI_INT, MPI_SUM, doneWin);
			MPI_Win_fence(MPI_MODE_NOSUCCEED, doneWin);
		} // we are done processing all the nodes :)

		for(int i=0;i<nbLocalNodes;++i)
		{
			if(lambda[i]==-1)
				NodePoolIndices[i] = F_NODE;
		}

		MPI_Win_free(&lambdaWin);
		MPI_Win_free(&doneWin);
		MPI_Free_mem(lambda);
		MPI_Free_mem(lambdaDup);
		MPI_Group_free(&neighbourGroup);
		delete[] NodePoolIndices;
		delete[] toRank;
		delete[] nodePosition;
	}

//! \brief Set default values for the PMIS strategy
void setDefaults()
	{
		m_max_level = 1;
		m_theta = 0.25;
		m_connection_matrix = 0;
		m_splitting = 0;
	}
	
//! \brief Free the memory allocated for the connection matrix and splitting hierarchy
	void deallocate()
	{
		delete[] m_connection_matrix;
		m_connection_matrix = 0;
		delete[] m_splitting;
		m_splitting = 0;
	}

/*! \brief Count the number of nnz to build the connection matrix
	 *
	 *  The algorithm is the following:
	 *  - For each row, the maximum absolute value is located and used to compare connection strength wrt to all other values (this is done in the method maxNegOffDiagonal)
	 *  - Then for each entry in the matrix row, if abs(A(i,j)) >= threshold * maxAbs, then the nnz counter is increased by one.
	 *
	 *  \tparam DataType The data type of the working matrix
	 *  \tparam MatrixImpl The actual matrix implementation
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 */
	int countStrengthMatrixNnz(const matrix_type& _matrix)
	{
		int nnz = 0;
		typedef typename matrix_type::impl_type::const_row_iterator_type rowIteratorType;
		typedef typename matrix_type::impl_type::const_col_iterator_type colIteratorType;
		
		for(rowIteratorType rowIt = _matrix.rowBegin(); rowIt != _matrix.rowEnd();++rowIt)
		{
			const data_type max = this->maxNegOffDiagonal(_matrix,rowIt);
			for(colIteratorType colIt = _matrix.colBegin(rowIt); colIt !=_matrix.diagonalElement(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
					++nnz;
			}
			for(colIteratorType colIt = _matrix.diagonalElement(rowIt).next(); colIt != _matrix.colEnd(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
					++nnz;
			}
		}
		return nnz;
	}
	
	/*! \brief Search for the maximum absolute value in a row
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 *  \param[in] _row The row we're looking the maximum absolute value
	 *
	 *  \return The maximum absolute value of the specified row
	 */
	data_type maxNegOffDiagonal(const matrix_type& _matrix, const typename matrix_type::impl_type::const_row_iterator_type& _row)
	{
		typedef typename matrix_type::impl_type::const_col_iterator_type colIteratorType;
		data_type max = std::numeric_limits<data_type>::min();
		
		for(colIteratorType colIt = _matrix.colBegin(_row); colIt!=_matrix.diagonalElement(_row); ++colIt)
		{
			if(max < -*colIt)
				max = -*colIt;
		}
		for(colIteratorType colIt = _matrix.diagonalElement(_row).next(); colIt != _matrix.colEnd(_row); ++colIt)
		{
			if(max < -*colIt)
				max = -*colIt;
		}
		return max;
	}

	/*! \brief Compute and register the communication map for a row distributed matrix col sum
	 *
	 *  Extract the communication map required for a matrix column sum from the vector passed as parameter. \n
	 *  The col sum map is always a subset of the SPMV map, except that "roles" are inversed. \n
	 *  The ghost elements for a SPMV are the shared elements for a col sum, and vice versa.
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 *  \param[in] _vector Vector from which we will extract the SPMV communication map
	 */
	void computeColSumMap(const IntegerMatrix& _matrix, const vector_type& _vector)
	{
		const SPMVShared& shared = _vector.getSync()->getSPMVShared();
		const SPMVGhosts& ghost = _vector.getSync()->getSPMVGhosts();

		const int AverageRowsByPartition = _matrix.getGlobalNbRows()/ParUtils::getNumProc();
			
		// Computing ghosts infos
		const int nbSender = shared.getNbReceiver();
		const int* senderList = shared.getReceiverList();
		const int nbElementsToReceive = shared.getNbElements();
		const int* nbElementsByProcList = shared.getNbElementsByProcList();
		// Need to transform these LIDs in GIDs
		const int* LIDsGhosts = shared.getLID();
		int* GIDsGhosts = new int[nbElementsToReceive];
		for(int i=0;i<nbElementsToReceive;++i)
			GIDsGhosts[i] = LIDsGhosts[i] + AverageRowsByPartition*ParUtils::getProcRank();

		// Computing shared infos
		const int nbReceiver = ghost.getNbSender();
		const int* receiverList = ghost.getSenderList();
		const int nbElementsToSend = ghost.getNbElements();
		const int* nbElementsForProcList = ghost.getNbElementsForProcList();
		// No need to go from GIDs to LIDs here coz we need to access matrix col, not matrix rows; therefore GID == LID
		const int* GIDsShared = ghost.getGIDs();
			
		SynchronizerSingleton& sync = SynchronizerSingleton::instance();
		typedef typename SynchronizerSingleton::synchronizer_mng_1_type::key_type key_type;
		key_type this_key = key_type(SynchronizerSingleton::CommMap::col_sum_map, std::make_tuple(&_matrix));

		sync.setValue(this_key, new GeneralSynchronizer(new GeneralGhosts(nbElementsToReceive, nbSender, senderList, nbElementsByProcList, GIDsGhosts), new GeneralShared(nbElementsToSend, nbReceiver, receiverList, nbElementsForProcList, GIDsShared)));

		delete[] GIDsGhosts;
	}
	//@}
};
YALLA_END_NAMESPACE

#endif
