#pragma once
#ifndef yalla_standard_coarsening_h
#define yalla_standard_coarsening_h

/*!
 *  \file StandardCoarsening.h
 *  \brief Standard coarsening strategy for the AMG solver
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <limits>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToType.h"
#include "yalla/Utils/TypeTraits/PromoteToVector.h"
#include "yalla/Utils/Math/Math.h"
#include "yalla/IO/console/log.h"

// Windows bug (feature) workaround. See http://support.microsoft.com/kb/143208
#ifdef min
#undef min
#endif

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class StandardCoarsening
 *  \brief Standard coarsening strategy for the AMG solver
 * 
 *  \details
 *  This coarsening strategy has two main steps:
 *  - During the first step, the connection strength matrix is built, to determine which unknown will be fine or coarse
 *  - During the second step, the splitting between fine and coarse unknowns is done
 *
 *  The strength of connection matrix is computed following the RS algorithm
 *
 *  The splitting is done in two steps:
 *  - First, coarse and fine nodes are determined following the strength of connection matrix
 *  - This first step doesn't guarantee that all nodes j influencing a node i is either a coarse node or influenced by a coarse node. Therefore, this second step look all fine-fine connections and add coarse nodes if necessary
 *
 *  The class can throw one type of exception, only in debug mode:
 *  - isOutOfRange exception. If someone try to access an element in the hierarchy that is greater than the number of max level or less than 0
 *
 *  \tparam MatrixType The type of the matrix
 *
 *  \warning This class was implemented for sequential execution only
 */
template<typename MatrixType>
class StandardCoarsening
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the matrix
	typedef MatrixType matrix_type;
	//! \brief Data type of the matrix
	typedef typename matrix_type::data_type data_type;
	//! \brief Consistent vector type
	typedef typename PromoteToVector<matrix_type, data_type>::data_type vector_type;
	//! \brief Consistent integer matrix
	typedef typename PromoteToType<matrix_type, int>::data_type IntegerMatrix;
	//! \brief Consistent integer vector
	typedef typename PromoteToVector<matrix_type, int>::data_type IntegerVector;
	//! Type of this class
	typedef StandardCoarsening<matrix_type> this_type;
	//@}
 private:
	//! \brief Pointer on the connection matrix hierarchy
	IntegerMatrix** m_connection_matrix;
	//! \brief Pointer on the unknown splitting hierarchy
	IntegerVector** m_splitting;
	//! \brief Threshold to determine strong connection
	double m_theta;
	//! \brief Max number of level in the grid hierarchy
	int m_max_level;
	//! \brief Enum for the different state of unknowns
	enum nodeStatus
	{
		//! \brief Fine node
		F_NODE = 0,
		//! \brief Coarse node
		C_NODE = 1,
		//! \brief Unprocessed node
		U_NODE = 2
	};
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class StandardCoarsening
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Calls the method setDefaults
	 */
	explicit StandardCoarsening()
	{
		this->setDefaults();
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class StandardCoarsening
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the connection matrix and the splitting
	 */
	virtual ~StandardCoarsening()
	{
		this->deallocate();
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class StandardCoarsening. \n
	 *  These methods give access in read-only mode to the datas/information of the class StandardCoarsening
	 */

	//@{
	/*! \brief Return the connection matrix at a level i
	 *
	 *  \param[in] _lvl Level for which one wants the connection matrix
	 *
	 *  \return The connection matrix at the specified level
	 *
	 *  \exception isOutOfRange _lvl is out of range
	 */
	const IntegerMatrix& getStrengthMatrix(const int _lvl) const
	{
#ifdef _DEBUG
		try
		{
			if(CheckBounds::Check(_lvl,0,m_max_level))
				throw isOutOfRange(__FILE__,__LINE__);
		}
		catch ( isOutOfRange &isOut )
		{
			YALLA_ERR(isOut.what())
				} 
#endif
		return *m_connection_matrix[_lvl];
	}

	/*! \brief Return the splitting at a level i
	 *
	 *  \param[in] _lvl Level for which one wants the splitting
	 *
	 *  \return The connection matrix at the specified level
	 *
	 *  \exception isOutOfRange _lvl is out of range
	 */
	const IntegerVector& getSplitting(const int _lvl) const
	{
#ifdef _DEBUG
		try
		{
			if(CheckBounds::Check(_lvl,0,m_max_level))
				throw isOutOfRange(__FILE__,__LINE__);
		}
		catch ( isOutOfRange &isOut )
		{
			YALLA_ERR(isOut.what())
				} 
#endif
		return *m_splitting[_lvl];
	}

	/*! \brief  Return the class name. 
	 *
	 *  \return "StandardCoarsening" The name of the class
	 */
	const char* getClassName() const
	{
		return "StandardCoarsening";
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class StandardCoarsening. \n
	 *  These methods give access in read/write mode to the datas/information of the class StandardCoarsening
	 */
	//@{

	/*! \brief Set the maximum number of grids used
	 *
	 *  \param[in] _maxLvl The max number of grids used
	 */
	void setMaxLevel(const int _maxLvl)
	{
		m_max_level =_maxLvl;
	}

	/*! \brief Set the strength connection threshold
	 *
	 *  Set the strength connection threshold to establish the strong connections while building the grid hierarchy
	 *
	 *  \param[in] _strength Strength threshold
	 */
	void setStrengthThreshold(const double _strength)
	{
		m_theta = _strength;
	}
	//@}

	/*! \name Public setup
	 *
	 *  Setup method of the StandardCoarsening strategy. \n
	 */
	//@{

	/*! \brief Allocate the memory required for the class
	 *
	 *
	 *  Allocate the memory for the connection matrix and the splitting hierarchy
	 */
	void setup()
	{
		this->deallocate();
		m_connection_matrix = new IntegerMatrix*[m_max_level];
		m_splitting = new IntegerVector*[m_max_level];
	}
	//@}

	/*! \name Public apply
	 *
	 *  Setup method of the StandardCoarsening strategy. \n
	 */
	//@{

	/*! \brief Build the connection matrix and the splitting for the current level
	 *
	 *  Apply the standard coarsening strategy and the splitting. \n
	 *  Call the method computeStrengthMatrix to build the connection matrix for the current level. \n
	 *  Call the method split to separate the coarse and fine unknowns at the current level . \n
	 *
	 *  \param[in] _matrix The matrix at the current level
	 *  \param[in] _rhs The rhs at the current level
	 *  \param[out] _lhs The lhs at the current level
	 *  \param[in] _currentLevel The current level
	 */
	void apply(const matrix_type& _matrix, const vector_type& _rhs, vector_type& _lhs, const int _currentLevel)
	{
		this->computeStrengthMatrix(_matrix,_currentLevel);
		this->split(_currentLevel);
	}
	//@}

 private:

	/*! \name Private Utils
	 *
	 *  Private Utils of the class StandardCoarsening strategy. \n
	 *  These methods provide various utilities to the class StandardCoarsening strategy
	 */
	//@{

	/*! \brief Apply the standard coarsening strategy for a SeqDenseMatrix
	 *
	 *  The algorithm is the following:
	 *  - For each row, the maximum absolute value is located and used to compare connection strength wrt to all other values (this is done in the method maxNegOffDiagonal)
	 *  - Then for each entry in the matrix row, if abs(A(i,j)) >= threshold * maxAbs, the connection is made strong and the value of the connection matrix at the index (i,j) is set to one.
	 *  - Otherwise, the value of the connection matrix at the index (i,j) is set to zero.
	 *
	 *  \tparam DataType The data type of the working matrix
	 *  \tparam MatrixImpl The actual matrix implementation
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 *  \param[in] _currentLevel The current level
	 */
	template<typename DataType, typename MatrixImpl>
		void computeStrengthMatrix(const SeqDenseMatrix<DataType,MatrixImpl>& _matrix, const int _currentLevel)
	{
		typedef typename matrix_type::impl_type::const_row_iterator_type rowIteratorType;
		typedef typename matrix_type::impl_type::const_col_iterator_type colIteratorType;
		const int nbRows = _matrix.getNbRows();
		const int nbCols = _matrix.getNbCols();
		m_connection_matrix[_currentLevel] = new IntegerMatrix(nbRows,nbCols);
		for(rowIteratorType rowIt = _matrix.rowBegin(); rowIt != _matrix.rowEnd();++rowIt)
		{
			const data_type max = this->maxNegOffDiagonal(_matrix,rowIt);
			for(colIteratorType colIt = _matrix.colBegin(rowIt); colIt !=_matrix.diagonalElement(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
					m_connection_matrix[_currentLevel]->setValue(rowIt.index(),colIt.index(),1);
			}
			for(colIteratorType colIt = _matrix.diagonalElement(rowIt).next(); colIt != _matrix.colEnd(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
					m_connection_matrix[_currentLevel]->setValue(rowIt.index(),colIt.index(),1);
			}
		}
	}
		
	/*! \brief Apply the standard coarsening strategy for a SeqSparseMatrix
	 *
	 *  The algorithm is the following:
	 *  - For each row, the maximum absolute value is located and used to compare connection strength wrt to all other values (this is done in the method maxNegOffDiagonal)
	 *  - Then for each entry in the matrix row, if -A(i,j) >= threshold * maxAbs, the connection is made strong and the value of the connection matrix at the index (i,j) is set to one.
	 *  - Otherwise, the value of the connection matrix at the index (i,j) is set to zero.
	 *
	 *  \tparam DataType The data type of the working matrix
	 *  \tparam MatrixImpl The actual matrix implementation
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 *  \param[in] _currentLevel The current level
	 *
	 *  \warning This method will actually work only for a CSR matrix as the pointers built are in CSR format. It is possible to make this method generi by simply copying the working matrix and properly setting all the values. This will be done at the cost of some extra useless storage
	 */
	template<typename DataType, typename MatrixImpl>
		void computeStrengthMatrix(const SeqSparseMatrix<DataType,MatrixImpl>& _matrix, const int _currentLevel)
	{
		typedef typename MatrixImpl::const_row_iterator_type rowIteratorType;
		typedef typename MatrixImpl::const_col_iterator_type colIteratorType;

		const int nbRows = _matrix.getNbRows();
		const int nbCols = _matrix.getNbCols();
		const int nbNnz = this->countStrengthMatrixNnz(_matrix);
		int* rowPtr = new int[nbRows+1];
		int* colPtr = new int[nbNnz];
		rowPtr[0] = 0;

		for(rowIteratorType rowIt = _matrix.rowBegin(); rowIt != _matrix.rowEnd();++rowIt)
		{
			rowPtr[rowIt.index()+1] = rowPtr[rowIt.index()];
			const data_type max = this->maxNegOffDiagonal(_matrix,rowIt);
			for(colIteratorType colIt = _matrix.colBegin(rowIt); colIt !=_matrix.diagonalElement(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
				{
					colPtr[rowPtr[rowIt.index()+1]] = colIt.index();
					++rowPtr[rowIt.index()+1];
				}
			}
			for(colIteratorType colIt = _matrix.diagonalElement(rowIt).next(); colIt != _matrix.colEnd(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
				{
					colPtr[rowPtr[rowIt.index()+1]] = colIt.index();
					++rowPtr[rowIt.index()+1];
				}
			}
		}
		m_connection_matrix[_currentLevel] = new IntegerMatrix(nbRows,nbCols,nbNnz,rowPtr,colPtr);
		m_connection_matrix[_currentLevel]->fill(static_cast<data_type>(1));
		delete[] rowPtr;
		delete[] colPtr;
	}

	/*! \brief Compute the splitting between coarse and fine unknowns
	 *
	 *  Initialisation phase:
	 *  - The number of nodes is equal to the number of rows in the connection matrix (used to allocate the splitting array).
	 *  - The strength measure array lambda is then initialised with the connection matrix (each node is attributed a value depending on how many nodes it strongly influence: lambda[i] = sum m_connection_matrix(i,j)).
	 *  - Finally, the pool of nodes is initialized to unprocessed
	 *  
	 *  Splitting phase:
	 *  - First pass (while all nodes haven't beed processed):
	 *    - The node with the highest measure is located.
	 *    - This node become a coarse node (NodePoolIndices[NodeWithMaxLambda] = C_NODE).
	 *    - The measure of this node is then set to zero (lambda[NodeWithMaxLambda] = 0).
	 *    - All neighbouring nodes become fine nodes (NodePoolIndices[j] = F_NODE).
	 *    - The measure of all those nodes are then set to zero (lambda[j] = 0).
	 *    - The measure of all new fine neighbouring nodes is increased by one.
	 *  
	 *  After this first pass, all nodes are either coarse of fine. Now we still have to check that eah strong connection is between a coarse and a fine node or that each strong connection between two fine nodes are influenced by a same coarse node.
	 * 
	 *  - Second pass (all strong connections are checked):
	 *    - For each strong connection, we identify the involved nodes.
	 *    - If one of this node is coarse, we continue
	 *    - If both nodes are fine, we loog for all strong connection involving those two nodes:
	 *      - If a coarse node is strongly connected to both fine nodes, we continue
	 *      - Otherwise, the first fine node is labelled as a coarse node
	 *
	 *  \warning The second pass has never been tested on an actual case
	 *
	 *  \param[in] _currentLevel The current level
	 */
	void split(const int _currentLevel)
	{
		typedef typename IntegerMatrix::impl_type::const_row_iterator_type rowIt;
		typedef typename IntegerMatrix::impl_type::const_col_iterator_type colIt;

		const int nbNodes = m_connection_matrix[_currentLevel]->getNbCols();
		int* lambda = new int[nbNodes];
		int* NodePoolIndices = new int[nbNodes];
		// Initialisation phase
		for(int i=0;i<nbNodes;++i)
		{
			NodePoolIndices[i] = U_NODE;
			lambda[i] = 0;
		}

		for(rowIt row = m_connection_matrix[_currentLevel]->rowBegin(); row != m_connection_matrix[_currentLevel]->rowEnd(); ++row)
		{
			for(colIt col = m_connection_matrix[_currentLevel]->colBegin(row); col != m_connection_matrix[_currentLevel]->colEnd(row);++col)
			{
				lambda[col.index()] += *col;
			}
		}

    // Nodes that have no connections must become F-nodes
    int rowsum;
    for (rowIt row = m_connection_matrix[_currentLevel]->rowBegin(); row != m_connection_matrix[_currentLevel]->rowEnd(); ++row)
    {
      if (!lambda[row.index()])
      {
        rowsum = 0;
        for(colIt col = m_connection_matrix[_currentLevel]->colBegin(row); col != m_connection_matrix[_currentLevel]->colEnd(row);++col)
          rowsum += *col;
        if (rowsum == 0)
          NodePoolIndices[row.index()] = F_NODE;
      }
    }

		// First pass
		while(Math::myMax(lambda,nbNodes)>0) // Until all points are either C_NODE or F_NODE
		{
			int NodeWithMaxLambda = Math::PosOfMax(lambda,nbNodes);
			NodePoolIndices[NodeWithMaxLambda] = C_NODE;
			lambda[NodeWithMaxLambda] = 0;
			for(int j=0;j<nbNodes;++j)
			{
				// All nodes j that are strongly influenced by the node with highest lambda
				if((*m_connection_matrix[_currentLevel])(j,NodeWithMaxLambda) && NodePoolIndices[j]==U_NODE)
				{
					NodePoolIndices[j] = F_NODE;
					lambda[j] = 0;
					for(int i=0;i<nbNodes;++i)
					{
						// All nodes i that strongly influence the node j
						if((*m_connection_matrix[_currentLevel])(i,j) && NodePoolIndices[i]==U_NODE)
							++lambda[i];
					}
				}
			}
		}
		// Second pass
		for(int i=0;i<nbNodes;++i)
		{
			for(int j=0;j<nbNodes;++j)
			{
				if((*m_connection_matrix[_currentLevel])(i,j))
				{
					const int node1 = i;
					const int node2 = j;
					if(NodePoolIndices[node1]==C_NODE || NodePoolIndices[node2]==C_NODE)
						continue;
					else if(NodePoolIndices[node1]==F_NODE && NodePoolIndices[node2]==F_NODE)
					{
						int foundCommonCoarsedPoint = 0;
						for(int k=0;k<nbNodes;++k)
						{
							if((*m_connection_matrix[_currentLevel])(k,node1) && NodePoolIndices[k]==C_NODE)
							{
								if((*m_connection_matrix[_currentLevel])(k,node2))
								{
									foundCommonCoarsedPoint = 1;
									break;
								}
							}
						}
						if(!foundCommonCoarsedPoint)
						{
							NodePoolIndices[node1]=C_NODE;
						}
					}
					else
					{
						YALLA_ERR("Problem with the standard coarsening strategy.");
					}
				}
			}
		}
		m_splitting[_currentLevel] = new IntegerVector(nbNodes, 1, NodePoolIndices);
		delete[] lambda;
		delete[] NodePoolIndices;
	}

	//! \brief Set default values for the StandardCoarsening strategy
	void setDefaults()
	{
		m_max_level = 1;
		m_theta = 0.25;
		m_connection_matrix = 0;
		m_splitting = 0;
	}

	//! \brief Free the memory allocated for the connection matrix and splitting hierarchy
	void deallocate()
	{
		if(m_connection_matrix)
		{
			for(int i=0;i<m_max_level;++i)
			{
				delete m_connection_matrix[i];
				delete m_splitting[i];
			}
			delete[] m_connection_matrix;
			m_connection_matrix = 0;
			delete[] m_splitting;
			m_splitting = 0;
		}
	}

	/*! \brief Count the number of nnz to build the connection matrix
	 *
	 *  The algorithm is the following:
	 *  - For each row, the maximum absolute value is located and used to compare connection strength wrt to all other values (this is done in the method maxNegOffDiagonal)
	 *  - Then for each entry in the matrix row, if abs(A(i,j)) >= threshold * maxAbs, then the nnz counter is increased by one.
	 *
	 *  \tparam DataType The data type of the working matrix
	 *  \tparam MatrixImpl The actual matrix implementation
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 */
	template<typename DataType, typename MatrixImpl>
		int countStrengthMatrixNnz(const SeqSparseMatrix<DataType,MatrixImpl>& _matrix)
	{
		int nnz = 0;
		typedef typename matrix_type::impl_type::const_row_iterator_type rowIteratorType;
		typedef typename matrix_type::impl_type::const_col_iterator_type colIteratorType;

		for(rowIteratorType rowIt = _matrix.rowBegin(); rowIt != _matrix.rowEnd();++rowIt)
		{
			const data_type max = this->maxNegOffDiagonal(_matrix,rowIt);
			for(colIteratorType colIt = _matrix.colBegin(rowIt); colIt !=_matrix.diagonalElement(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
					++nnz;
			}
			for(colIteratorType colIt = _matrix.diagonalElement(rowIt).next(); colIt != _matrix.colEnd(rowIt); ++colIt)
			{
				if(-*colIt >= m_theta*max)
					++nnz;
			}
		}
		return nnz;
	}
		
	/*! \brief Search for the maximum absolute value in a row
	 *
	 *  \param[in] _matrix The matrix for which the connection matrix will be computed
	 *  \param[in] _row The row we're looking the maximum absolute value
	 *
	 *  \return The maximum absolute value of the specified row
	 */
	data_type maxNegOffDiagonal(const matrix_type& _matrix, const typename matrix_type::impl_type::const_row_iterator_type& _row)
	{
		typedef typename matrix_type::impl_type::const_col_iterator_type colIteratorType;
		data_type max = std::numeric_limits<data_type>::min();
			
		for(colIteratorType colIt = _matrix.colBegin(_row); colIt!=_matrix.diagonalElement(_row); ++colIt)
		{
			if(max < -*colIt)
				max = -*colIt;
		}
		for(colIteratorType colIt = _matrix.diagonalElement(_row).next(); colIt != _matrix.colEnd(_row); ++colIt)
		{
			if(max < -*colIt)
				max = -*colIt;
		}
		return max;
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
