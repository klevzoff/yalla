#pragma once
#ifndef yalla_standard_interpolation_h
#define yalla_standard_interpolation_h

/*!
 *  \file DirectInterpolation.h
 *  \brief Build the interpolation and restriction operator following a direct interpolation algorithm
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/LinearSolver/AMG/AMGInfo.h"

YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class DirectInterpolation
 *  \brief Direct interpolation for the AMG method
 * 
 *  \details
 *  Build the interpolation and restriction operator following a direct interpolation algorithm
 *
 *  \warning This class was implemented for sequential execution only
 *  \todo Add the iterators to this implementation
 */
template<typename MatrixType>
class DirectInterpolation
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of the matrix
	typedef MatrixType matrix_type;
	//! \brief Data type of the matrix
	typedef typename matrix_type::data_type data_type;
	//! \brief Consistent vector type
	typedef typename PromoteToVector<matrix_type, data_type>::data_type vector_type;
	//! \brief Consistent integer vector
	typedef typename PromoteToVector<matrix_type, int>::data_type IntegerVector;
	//! Type of this class
	typedef DirectInterpolation<matrix_type> this_type;
	//@}
 private:
	//! \brief Pointer on the interpolation matrix hierarchy
	matrix_type** m_interpolation_matrix;
	//! \brief Pointer on the restriction matrix hierarchy
	matrix_type** m_restriction_matrix;
	//! \brief Pointer on the interpolation variable index hierarchy
	IntegerVector* m_interpolation_variables_indices;
	//! \brief Max number of level in the grid hierarchy
	int m_max_level;
	//! \brief Verbosity level
	int m_verbose_level;
	//! \brief Information on the AMG solver execution
	AMGInfo* m_amg_info;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DirectInterpolation
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Calls the method setDefaults
	 */
	explicit DirectInterpolation()
	{
		this->setDefaults();
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DirectInterpolation
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the interpolation and restriction matrix hierarchy
	 */
	virtual ~DirectInterpolation()
	{
		this->deallocate();
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class DirectInterpolation. \n
	 *  These methods give access in read-only mode to the datas/information of the class DirectInterpolation
	 */
	//@{
		
	/*! \brief Return the interpolation matrix hierarchy pointer
	 *
	 *  \return The interpolation matrix hierarchy pointer
	 */
	matrix_type** getInterpolationMatrices() const
	{
		return m_interpolation_matrix;
	}

	/*! \brief Return the restriction matrix hierarchy pointer
	 *
	 *  \return The restriction matrix hierarchy pointer
	 */
	matrix_type** getRestrictionMatrices() const
	{
		return m_restriction_matrix;
	}

	/*! \brief Return the interpolation matrix at level i
	 *
	 *  \param[in] _level The required level for the interpolation matrix
	 *
	 *  \return The interpolation matrix hierarchy pointerat level i
	 */
	matrix_type& getInterpolationMatrix(const int _level) const
	{
		return *m_interpolation_matrix[_level];
	}

	/*! \brief Return the restriction matrix at level i
	 *
	 *  \param[in] _level The required level for the restriction matrix
	 *
	 *  \return The restriction matrix hierarchy pointerat level i
	 */
	matrix_type& getRestrictionMatrix(const int _level) const
	{
		return *m_restriction_matrix[_level];
	}

	/*! \brief  Return the class name. 
	 *
	 *  \return "DirectInterpolation" The name of the class
	 */
	const char* getClassName() const
	{
		return "DirectInterpolation";
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class DirectInterpolation. \n
	 *  These methods give access in read/write mode to the datas/information of the class DirectInterpolation
	 */
	//@{

	/*! \brief Set the verbosity level
	 *
	 *  \param[in] _lvl The verbosity level
	 *  \param[in] _amgInfo Structure to stored AMG information
	 */
	void setVerbose(const int _lvl, AMGInfo* _amgInfo)
	{
		m_verbose_level =_lvl;
		m_amg_info = _amgInfo;
	}

	/*! \brief Set the maximum number of grids used
	 *
	 *  \param[in] _maxLvl The max number of grids used
	 */
	void setMaxLevel(const int _maxLvl)
	{
		m_max_level =_maxLvl;
	}
	//@}

	/*! \name Public setup
	 *
	 *  Setup method of the DirectInterpolation class. \n
	 */

	/*! \brief Allocate the memory required for the class
	 *
	 *  Allocate the memory for the interpolation and restriction matrix hierarchy
	 */
	void setup()
	{
		this->deallocate();
		m_interpolation_matrix = new matrix_type*[m_max_level];
		m_restriction_matrix = new matrix_type*[m_max_level];
		for(int i=0;i<m_max_level;++i)
		{
			m_interpolation_matrix[i] = 0;
			m_restriction_matrix[i] = 0;
		}
		m_interpolation_variables_indices = new IntegerVector[m_max_level];
	}
	//@}

	/*! \name Public apply
	 *
	 *  Setup method of the DirectInterpolation strategy. \n
	 */
	//@{

	/*! \brief Build the interpolation and restriction matrix for the current level
	 *
	 *  Call the method allocate to allocate matrices at the current level. \n
	 *  Call the method interpolate to build to interpolation and restrition matrices at the current level.
	 *  
	 *  \param[in] _matrix The working matrix
	 *  \param[in] _rhs The rhs at the current level
	 *  \param[out] _lhs The lhs at the current level
	 *  \param[in] _coarsedVariables Vector of interpolation variables
	 *  \param[in] _currentLevel 
	 */
	void apply(const matrix_type& _matrix, const vector_type& _rhs, vector_type& _lhs, const IntegerVector& _coarsedVariables, const int _currentLevel)
	{
		this->allocate(_matrix,_coarsedVariables,_currentLevel);
		this->interpolate(_matrix,_coarsedVariables,_currentLevel);
	}
	//@}
 private:

	/*! \name Private Utils
	 *
	 *  Private Utils of the class DirectInterpolation strategy. \n
	 *  These methods provide various utilities to the class DirectInterpolation strategy
	 */
	//@{

	/*! \brief Allocate the interpolation and restriction matrices for the current level
	 *
	 *  Start by counting the number of coarse nodes to define the dimensions of the interpolation and restriction matrices. \n
	 *  Fill the array containing the index of the coarse nodes used in the interpolation. 
	 *  
	 *  \param[in] _matrix The working matrix
	 *  \param[in] _coarsedVariables The splitting vector
	 *  \param[in] _currentLevel _currentLevel The current level
	 */
	void allocate(const matrix_type& _matrix, const IntegerVector& _coarsedVariables, const int _currentLevel)
	{
		int coarsedVariableCounter = 0;
		const int nbRows = _coarsedVariables.getNbRows();
		for(int i=0;i<nbRows;++i)
		{
			if(_coarsedVariables(i))
				++coarsedVariableCounter;
		}
		if(m_verbose_level>1)
		{
			m_amg_info->m_coarsening_rate.push_back(static_cast<double>(coarsedVariableCounter)/static_cast<double>(nbRows));
		}
		m_interpolation_variables_indices[_currentLevel] = IntegerVector(coarsedVariableCounter,1);
		coarsedVariableCounter=0;
		for(int i=0;i<nbRows;++i)
		{
			if(_coarsedVariables(i))
			{
				m_interpolation_variables_indices[_currentLevel](coarsedVariableCounter) = i;
				++coarsedVariableCounter;
			}
		}
		int nnz = this->countInterpolationMatrixNnz(_matrix,_coarsedVariables,coarsedVariableCounter,_currentLevel);
		m_interpolation_matrix[_currentLevel] = new matrix_type(nbRows,coarsedVariableCounter,nnz);
	}

	/*! \brief Build the interpolation and restriction matrices following the direct interpolation algorithm
	 *
	 *  The algorithm is the following:
	 *  - For each row, the sum of all positive and all negative off diagonal terms are computed
	 *  - For each row, the sum of all positive and all negative interpolation nodes are computed
	 *  - Then the interpolation weight is computed. Negative terms should always be present (elliptic matrix). If there is no positif term in the strong connection, all positive terms are added in the diagonal coefficient and the interpolation weight is set to zero. Otherwise positive terms are treated exactly the same way than the negative ones.
	 *  - Finally, the interpolation coefficients are computed to give the interpolation matrix
	 *
	 *  The restriction matrix is then built by transposing the interpolation matrix.
	 *
	 *  \param[in] _matrix The working matrix
	 *  \param[in] _coarsedVariables The splitting vector
	 *  \param[in] _currentLevel The current level
	 */
	void interpolate(const matrix_type& _matrix, const IntegerVector& _coarsedVariables, const int _currentLevel)
	{
		const int nbRows = _matrix.getNbRows();
		const int nbCols = _matrix.getNbCols();
		for(int i=0;i<nbRows;++i)
		{
			if(_coarsedVariables(i))
			{
				const int nbCoarsedUnknowns = m_interpolation_matrix[_currentLevel]->getNbCols();
				for(int j=0;j<nbCoarsedUnknowns;++j)
				{
					if(m_interpolation_variables_indices[_currentLevel](j)==i)
					{
						m_interpolation_matrix[_currentLevel]->setValue(i,j,1);
						break;
					}
				}
			}
			else
			{
				data_type sum_all_neg = 0;
				data_type sum_all_pos = 0;
				data_type sum_strong_neg = 0;
				data_type sum_strong_pos = 0;
				data_type diag = 0;
				for(int j=0;j<nbCols;++j)
				{
					if(i!=j)
					{
						if(_matrix(i,j)<0)
							sum_all_neg += _matrix(i,j);
						else
							sum_all_pos += _matrix(i,j);
					}
					else
						diag += _matrix(i,j);
				}
				const int nbCoarsedUnknowns = m_interpolation_matrix[_currentLevel]->getNbCols();
				for(int j=0;j<nbCoarsedUnknowns;++j)
				{
					if(_matrix(i,m_interpolation_variables_indices[_currentLevel](j))<0)
						sum_strong_neg += _matrix(i,m_interpolation_variables_indices[_currentLevel](j));
					else
						sum_strong_pos += _matrix(i,m_interpolation_variables_indices[_currentLevel](j));
				}

				data_type alpha = sum_all_neg/sum_strong_neg;
				data_type beta;
				if(sum_strong_pos==0)
				{
					diag += sum_all_pos;
					beta = 0;
				}
				else
					beta = sum_all_pos/sum_strong_pos;

				data_type neg_coeff = -alpha/diag;
				data_type pos_coeff = -beta/diag;
				for(int k=0;k<nbCoarsedUnknowns;++k)
				{
					if(_matrix(i,m_interpolation_variables_indices[_currentLevel](k))<0)
						m_interpolation_matrix[_currentLevel]->setValue(i,k, neg_coeff * _matrix(i,m_interpolation_variables_indices[_currentLevel](k)));
					else if(_matrix(i,m_interpolation_variables_indices[_currentLevel](k))>0)
						m_interpolation_matrix[_currentLevel]->setValue(i,k, pos_coeff * _matrix(i,m_interpolation_variables_indices[_currentLevel](k)));
				}
			}
		}
		this->finalize(m_interpolation_matrix[_currentLevel]);
		m_restriction_matrix[_currentLevel] = new matrix_type();
		BLAS::copy(Trans,m_interpolation_matrix[_currentLevel][0], m_restriction_matrix[_currentLevel][0]);
	}

	//! \brief Set default values for the DirectInterpolation strategy
	void setDefaults()
	{
		m_max_level = 1;
		m_verbose_level = 0;
		m_interpolation_matrix = 0;
		m_restriction_matrix = 0;
		m_interpolation_variables_indices = 0;
	}

	//! \brief Free the memory allocated for the interpolation and the restriction matrix hierarchy
	void deallocate()
	{
		if(m_interpolation_matrix)
		{
			for(int i=0;i<m_max_level;++i)
			{
				delete m_interpolation_matrix[i];
				delete m_restriction_matrix[i];
			}
			delete[] m_interpolation_matrix;
			m_interpolation_matrix = 0;
			delete[] m_restriction_matrix;
			m_restriction_matrix = 0;
			delete[] m_interpolation_variables_indices;
			m_interpolation_variables_indices = 0;
		}
	}

	/*! \brief Count the number of nnz to build the interpolation matrix for a SeqDenseMatrix
	 *
	 *  For the case of a dense matrix, there is nothing to be counted.
	 *
	 *  \tparam DataType The data type of the working matrix
	 *  \tparam MatrixImpl The actual matrix implementation
	 *
	 *  \param[in] _matrix The matrix for which the interpolation matrix will be computed
	 *  \param[in] _coarsedVariables The splitting vector
	 *  \param[in] _nbCoarsedVariable The number of coarse variables
	 *  \param[in] _currentlevel The current level
	 */
	template<typename DataType, typename MatrixImpl>
    int countInterpolationMatrixNnz(const SeqDenseMatrix<DataType,MatrixImpl>& _matrix, const IntegerVector& _coarsedVariables, const int _nbCoarsedVariable, const int _currentlevel)
	{
		return 0;
	}

	/*! \brief Count the number of nnz to build the interpolation matrix for a SeqSparseMatrix
	 *
	 *  Just count the non null entries in the coarse variable array
	 *
	 *  \tparam DataType The data type of the working matrix
	 *  \tparam MatrixImpl The actual matrix implementation
	 *
	 *  \param[in] _matrix The matrix for which the interpolation matrix will be computed
	 *  \param[in] _coarsedVariables The splitting vector
	 *  \param[in] _nbCoarsedVariable The number of coarse variables
	 *  \param[in] _currentLevel The current level
	 */
	template<typename DataType, typename MatrixImpl>
    int countInterpolationMatrixNnz(const SeqSparseMatrix<DataType,MatrixImpl>& _matrix, const IntegerVector& _coarsedVariables, const int _nbCoarsedVariable, const int _currentLevel)
	{
		int nnz = 0;
		const int nbRows = _matrix.getNbRows();
		for(int i=0;i<nbRows;++i)
		{
			if(_coarsedVariables(i))
				++nnz;
			else
			{
				for(int j=0;j<_nbCoarsedVariable;++j)
				{
					if(_matrix(i,m_interpolation_variables_indices[_currentLevel](j)))
						++nnz;
				}
			}
		}
		return nnz;
	}

	/*! 
	 *  \brief Finalize a SeqDenseMatrix
	 *  
	 *  This method actually do nothing as dense matrices don't need to be finalized.
	 *
	 *  \tparam DataType Data type of the matrix
	 *  \tparam MatrixImpl Data structure implementation
	 *
	 *  \param[in] _matrix The matrix to be returned
	 */
	template<typename DataType, typename MatrixImpl>
    void finalize(SeqDenseMatrix<DataType,MatrixImpl>* _matrix)
	{
		;
	}

	/*! 
	 *  \brief Finalize a SeqSparseMatrix
	 *  
	 *  This method calls the finalize() method of the matrix. \n
	 *  The finalize() method of the matrix ensure proper construction and storage of the matrix.
	 *
	 *  \tparam DataType Data type of the matrix
	 *  \tparam MatrixImpl Data structure implementation
	 *
	 *  \param[in] _matrix The matrix to be returned
	 */
	template<typename DataType, typename MatrixImpl>
    void finalize(SeqSparseMatrix<DataType,MatrixImpl>* _matrix)
	{
		_matrix->finalize();
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
