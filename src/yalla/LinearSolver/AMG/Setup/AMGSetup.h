#pragma once
#ifndef yalla_amg_setup_h
#define yalla_amg_setup_h

/*!
 *  \file AMGSetup.h
 *  \brief Setup phase of the AMG solver
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/TypeTraits/PromoteToVector.h"
#include "yalla/LinearSolver/Relaxation/NoSmoother.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
//! Forward declaration
class NoSmoother;

/*!
 *  \class AMGSetup
 *  \brief Setup phase of the AMG solver
 * 
 *  \details
 *  This class follow a policy-based design and therefore has four template parameters:
 *  - \tparam MatrixType The type of the matrix
 *  - \tparam CoarseningStrategy The coarsening strategy used in the AMG solver
 *  - \tparam Interpolation The interpolation used in the AMG solver
 *  - \tparam Smoother The smoother used in the AMG solver
 *
 *  This class essentially build the grids hierarchy by calling the relevant methods of the coarsening strategy and the interpolation. If a smoother is provided, it is then used. However, by default no smoother is used.
 */
template<typename MatrixType, typename CoarseningStrategy, typename Interpolation, typename Smoother = NoSmoother>
	class AMGSetup
{
 public:
 //! \name Public Typedefs
 //@{
 //! \brief Type of the matrix
 typedef MatrixType matrix_type;
 //! \brief Consistent vector type
 typedef typename PromoteToVector<matrix_type, typename matrix_type::data_type>::data_type vector_type;
 //! \brief Data type of the matrix
 typedef typename matrix_type::data_type data_type;
 //! \brief Coarsening strategy type
 typedef CoarseningStrategy coarsening_strategy_type;
 //! \brief Interpolation type
 typedef Interpolation interpolation_type;
 //! \brief Smoother type
 typedef Smoother smoother_type;
 //! \brief Type of this class
 typedef AMGSetup<matrix_type, coarsening_strategy_type, interpolation_type, smoother_type> this_type;
 //@}
 private:
 //! \brief Pointer on the coarsening strategy
 coarsening_strategy_type* m_coarsening_strategy;
 //! \brief Pointer on the interpolation strategy
 interpolation_type* m_interpolation;
 //! \brief Pointer on the smoother strategy
 smoother_type* m_smoother;
 //! \brief Max number of level in the grid hierarchy
 int m_max_level;
 //! \brief Pointer on the grids hierarchy
 matrix_type* m_matrix_hierarchy;
 //! \brief Pointer on the rhs hierarchy
 vector_type* m_rhs_hierarchy;
 //! \brief Pointer on the lhs hierarchy
 vector_type* m_lhs_hierarchy;
 //! \brief Verbosity level
 int m_verbose_level;
 //! \brief Information on the AMG solver execution
 AMGInfo* m_amg_info;
 public:
 /*! \name Public Constructors
	*
	*  Public constructors of the class AMGSetup
	*/
 //@{
		
 /*! 
	*  \brief Default constructor.
	*
	*  \details
	*  Calls the method setDefaults
	*/
 explicit AMGSetup()
 {
	 this->setDefaults();
 }
 //@}

 /*! \name Public Destructor
	*
	*  Public destructor of the class AMGSetup
	*/
 //@{

 /*! 
	*  \brief Destructor.
	*
	*  \details
	*  Free the memory allocated by the class i.e. the linear systems hierarchy, the coarsening strategy, the interpolation strategy and the smoother. \n
	*/
 virtual ~AMGSetup()
 {
	 this->deallocate();
	 delete m_coarsening_strategy;
	 delete m_interpolation;
	 delete m_smoother;
 }
 //@}

 /*! \name Public Getter
	*
	*  Public Getter of the class AMGSetup. \n
	*  These methods give access in read-only mode to the datas/information of the class AMGSetup
	*/

 //@{
 /*! \brief Return the interpolation matrices hierarchy
	*  
	*  \return The interpolation matrices hierarchy
	*/
 matrix_type** getInterpolationMatrices() const
 {
	 return m_interpolation->getInterpolationMatrices();
 }

 /*! \brief Return the restriction matrices hierarchy
	*  
	*  \return The restriction matrices hierarchy
	*/
 matrix_type** getRestrictionMatrices() const
 {
	 return m_interpolation->getRestrictionMatrices();
 }

 /*! \brief Return the matrices hierarchy
	*  
	*  \return The matrices hierarchy
	*/
 matrix_type* getMatrixHierarchy() const
 {
	 return m_matrix_hierarchy;
 }

 /*! \brief Return the rhs hierarchy
	*  
	*  \return The rhs hierarchy
	*/
 vector_type* getRHSHierarchy() const
 {
	 return m_rhs_hierarchy;
 }

 /*! \brief Return the lhs hierarchy
	*  
	*  \return The lhs hierarchy
	*/
 vector_type* getLHSHierarchy() const
 {
	 return m_lhs_hierarchy;
 }

 /*! \brief Return a handler on the coarsening strategy
	*  
	*  \return The coarsening strategy
	*/
 coarsening_strategy_type* getCoarseningStrategyHandler() const
 {
	 return m_coarsening_strategy;
 }

 /*! \brief Return a handler on the interpolation strategy
	*  
	*  \return The interpolation strategy
	*/
 interpolation_type* getInterpolationHandler() const
 {
	 return m_interpolation;
 }

 /*! \brief Return a handler on the smoother
	*  
	*  \return The smoother
	*/
 smoother_type* getSmootherHandler() const
 {
	 return m_smoother;
 }

 /*! \brief  Return the class name. 
	*
	*  \return "AMGSetup" The name of the class
	*/
 const char* getClassName() const
 {
	 return "AMGSetup";
 }
 //@}

 /*! \name Public Setter
	*
	*  Public Setter of the class AMGSetup. \n
	*  These methods give access in read/write mode to the datas/information of the class AMGSetup
	*/
 //@{

 /*! \brief Set the verbosity level
	*
	*  \param[in] _lvl The verbosity level
	*  \param[in] _amgInfo The structure to store the solver execution information
	*/
 void setVerbose(const int _lvl, AMGInfo* _amgInfo)
 {
	 m_verbose_level =_lvl;
	 m_amg_info = _amgInfo;
	 m_interpolation->setVerbose(_lvl,_amgInfo);
 }

 /*! \brief Set the maximum number of grids used
	*
	*  \param[in] _maxLvl The max number of grids used
	*/
 void setMaxLevel(const int _maxLvl)
 {
	 m_max_level =_maxLvl;
	 m_coarsening_strategy->setMaxLevel(_maxLvl);
	 m_interpolation->setMaxLevel(_maxLvl);
 }

 /*! \brief Set the strength connection threshold
	*
	*  Set the strength connection threshold to establish the strong connections while building the grid hierarchy
	*
	*  \param[in] _strength Strength threshold
	*/
 void setStrengthThreshold(const double _strength)
 {
	 m_coarsening_strategy->setStrengthThreshold(_strength);
 }
 //@}

 /*! \name Public setup
	*
	*  Setup method of the AMGSetup solver. \n
	*/
 //@{

 /*! \brief Apply the setup phase of the AMGSetup
	*
	*  Allocates space to compute the linear systems hierarchy. \n
	*  Setup the smoother, the coarsening and interpolation strategy. Setup methods are in charge of allocating memory in the different strategies. \n
	*  Build the grids hierarchy by calling the coarsen, interpolate and smooth methods. \n
	*
	*  Separating the memory allocation (in setup methods) and the execution of the strategies is done on purpose. The idea is to be able to cheaply recompute the grids herarchy without having to reallocate everything if the sparsity pattern of the linear systems remain unchanged, while the coefficients change. This should probably be improved by implementing a setup method and an apply method in the AMGSetup. The setup method would call the setup methods of the different policies while the apply method would apply the different strategies.
	*  
	*  \param[in] _A The fine matrix
	*  \param[in] _rhs The fine rhs
	*  \param[out] _lhs The fine lhs
	*/
 void setup(const matrix_type& _A, const vector_type& _rhs, vector_type& _lhs)
 {
	 this->deallocate();
	 m_matrix_hierarchy = new matrix_type[m_max_level+1];
	 m_matrix_hierarchy[0] = _A;
	 m_rhs_hierarchy = new vector_type[m_max_level+1];
	 m_rhs_hierarchy[0] = _rhs;
	 m_lhs_hierarchy = new vector_type[m_max_level+1];
	 m_lhs_hierarchy[0] = _lhs;
	 if(m_verbose_level>1)
		 m_amg_info->m_coarsening_rate.push_back(1);

	 m_coarsening_strategy->setup();
	 m_interpolation->setup();
	 m_smoother->setup();

	 for(int i=0;i<m_max_level;++i)
	 {
		 this->coarsen(m_matrix_hierarchy[i], m_rhs_hierarchy[i], m_lhs_hierarchy[i], i);
		 this->interpolate(m_matrix_hierarchy[i], m_rhs_hierarchy[i], m_lhs_hierarchy[i], i);
		 this->smooth(m_matrix_hierarchy[i], m_rhs_hierarchy[i], m_lhs_hierarchy[i], i);
		 m_matrix_hierarchy[i+1] = m_interpolation->getRestrictionMatrix(i) * m_matrix_hierarchy[i] * m_interpolation->getInterpolationMatrix(i);
	 }
	 if(m_verbose_level>1)
	 {
		 for(int i=0;i<m_max_level+1;++i)
		 {
			 m_amg_info->m_operator_complexity.push_back(m_matrix_hierarchy[i].getNnz());
		 }
	 }
 }
 //@}

 /*! \name Private Utils
	*
	*  Private Utils of the class AMGSetup. \n
	*  These methods provide various utilities to the class AMGSetup
	*/
 //@{
 private:
 /*! \brief Apply the coarsening strategy for a level i
	*
	*  Call the apply method of the coarsening strategy for the current i level
	*
	*  \param[in] _inMatrix The matrix at level i
	*  \param[in] _inRHS The rhs at level i
	*  \param[in] _inLHS The lhs at level i
	*  \param[in] _levelCounter The current level
	*/
 void coarsen(const matrix_type& _inMatrix, const vector_type& _inRHS, vector_type& _inLHS, const int _levelCounter)
 {
	 m_coarsening_strategy->apply(_inMatrix, _inRHS, _inLHS, _levelCounter);
 }

 /*! \brief Apply the interpolation strategy for a level i
	*
	*  Call the apply method of the interpolation strategy for the current i level
	*
	*  \param[in] _inMatrix The matrix at level i
	*  \param[in] _inRHS The rhs at level i
	*  \param[in] _inLHS The lhs at level i
	*  \param[in] _levelCounter The current level
	*/
 void interpolate(const matrix_type& _inMatrix, const vector_type& _inRHS, vector_type& _inLHS, const int _levelCounter)
 {
	 m_interpolation->apply(_inMatrix, _inRHS, _inLHS, m_coarsening_strategy->getSplitting(_levelCounter), _levelCounter);
 }

 /*! \brief Apply the smoother for a level i
	*
	*  Call the solve method of the smoother for the current i level
	*
	*  \param[in] _inMatrix The matrix at level i
	*  \param[in] _inRHS The rhs at level i
	*  \param[in,out] _inLHS The lhs at level i
	*  \param[in] _levelCounter The current level
	*/
 void smooth(const matrix_type& _inMatrix, const vector_type& _inRHS, vector_type& _inLHS, const int _levelCounter)
 {
	 m_smoother->solve(_inMatrix,_inRHS,_inLHS);
 }

 //! \brief Set default values for the AMG solver
 void setDefaults()
 {
	 m_max_level = 1;
	 m_verbose_level = 0;
	 m_matrix_hierarchy = 0;
	 m_rhs_hierarchy = 0;
	 m_lhs_hierarchy = 0;
	 m_coarsening_strategy = new coarsening_strategy_type();
	 m_interpolation = new interpolation_type();
	 m_smoother = new smoother_type();
 }

 //! \brief Free the memory allocated for the linear systems hierarchy
 void deallocate()
 {
	 delete[] m_matrix_hierarchy;
	 m_matrix_hierarchy = 0;
	 delete[] m_rhs_hierarchy;
	 m_rhs_hierarchy = 0;
	 delete[] m_lhs_hierarchy;
	 m_lhs_hierarchy = 0;
 }
 //@}
};
YALLA_END_NAMESPACE
#endif
