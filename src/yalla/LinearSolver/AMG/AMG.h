#pragma once
#ifndef yalla_amg_h
#define yalla_amg_h

/*!
 *  \file AMG.h
 *  \brief AMG solver
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/LinearSolver/AMG/AMGInfo.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class AMG
 *  \brief Implementation of an AMG solver
 * 
 *  \details
 *  This class implements an AMG solver consisting in two phases:
 *  - The setup phase, during which the grid, the interpolation and the restriction hierarchy is built
 *  - The solve phase, during which the linear systems are solved
 * 
 *  This class follow a policy-based design and therefore has two template parameters:
 *  - \tparam SetupPhase The policies used in the setup phase
 *  - \tparam SolvePhase The policies used in the solve phase
 */
template<typename SetupPhase, typename SolvePhase>
	class AMG
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Setup phase type
	typedef SetupPhase setup_phase_type;
	//! \brief Solve phase type
	typedef SolvePhase solve_phase_type;
	//! \brief Type of this class
	typedef AMG<setup_phase_type, solve_phase_type> this_type;
	//@}
 private:
	//! \brief Max number of level in the grid hierarchy
	int m_max_level;
	//! \brief Max number of cycle
	int m_max_cycles;
	//! \brief Solver tolerance
	double m_tolerance;
	//! \brief Pointer on the setup phase
	setup_phase_type* m_setup;
	//! \brief Pointer on the solve phase
	solve_phase_type* m_solve;
	//! \brief Verbosity level
	int m_verbose_level;
	//! \brief Information on the AMG solver execution
	AMGInfo * m_amg_info;
	//! \brief Initialization flag
	bool m_initialised;
	//! \brief Change in max grid level after the setup call
	bool m_max_level_has_changed;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class AMG
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Calls the method setDefaults
	 */
	explicit AMG()
	{
		this->setDefaults();
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class AMG
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the setup and solve phase pointers
	 */
	virtual ~AMG()
	{
		delete m_amg_info;
		m_amg_info = 0;
		delete m_setup;
		m_setup = 0;
		delete m_solve;
		m_solve = 0;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class AMG. \n
	 *  These methods give access in read-only mode to the datas/information of the class AMG
	 */
	//@{

	/*! 
	 *  \brief Return the max number of level used in the solver
	 *
	 *  \details
	 *
	 *  \return The max number of level
	 */
	int getMaxLevel() const
	{
		return m_max_level;
	}

	/*! 
	 *  \brief Return the verbosity level
	 *
	 *  \details
	 *
	 *  \return The verbosity level
	 */
	int getVerboseLvl() const
	{
		return m_verbose_level;
	}

	/*! 
	 *  \brief Return the structure storing execution information
	 *
	 *  \details
	 *
	 *  \return The execution information
	 */
	const AMGInfo* getInfos() const
	{
		return m_amg_info;
	}

	/*! \brief Return the class name. 
	 *
	 *  \return "AMG" The name of the class
	 */
	const char* getClassName() const
	{
		return "AMG";
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class AMG. \n
	 *  These methods give access in read/write mode to the datas/information of the class AMG
	 */
	//@{
	/*! \brief Set the max number of cycle used
	 *
	 *  \param[in] _maxCycles The max number of cycles
	 */
	void setMaxIterations(const int _maxCycles)
	{
		m_max_cycles =_maxCycles;
		m_solve->setMaxCycles(_maxCycles);
	}

	/*! \brief Set the solver tolerance
	 *
	 *  \param[in] _tol The tolerance
	 */
	void setTolerance(const double _tol)
	{
		m_tolerance =_tol;
		m_solve->setTolerance(_tol);
	}

	/*! \brief Set the maximum number of grids used
	 *
	 *  \param[in] _maxLvl The max number of grids used
	 */
	void setMaxLevel(const int _maxLvl)
	{
		m_max_level =_maxLvl;
		m_setup->setMaxLevel(_maxLvl);
		m_solve->setMaxLevel(_maxLvl);
		m_max_level_has_changed = true;
	}

	/*! \brief Set the strength connection threshold
	 *
	 *  Set the strength connection threshold to establish the strong connections while building the grid hierarchy
	 *
	 *  \param[in] _strength Strength threshold
	 */
	void setStrengthThreshold(const double _strength)
	{
		m_setup->setStrengthThreshold(_strength);
	}

	/*! \brief Set the verbosity level
	 *
	 *  \param[in] _lvl The verbosity level
	 */
	void setVerboseLvl(const int _lvl)
	{
		m_verbose_level =_lvl;
		if(_lvl && !m_amg_info)
			m_amg_info = new AMGInfo(m_verbose_level);
		m_setup->setVerbose(_lvl,m_amg_info);
		m_solve->setVerbose(_lvl,m_amg_info);
	}
	//@}

	/*! \name Public setup
	 *
	 *  Setup method of the AMG solver. \n
	 */
	//@{
	/*! \brief Apply the setup phase of the AMG solver
	 *
	 *  Build the grids hierarchy by calling the setup method of the setup phase. \n
	 *  Set the initialization flag to true and the max level change flag to false.
	 *
	 *  \tparam Matrix The fine matrix type
	 *  \tparam Vector The fine vectors type
	 *
	 *  \param[in] _A The fine matrix
	 *  \param[in] _rhs The fine rhs
	 *  \param[out] _lhs The fine lhs
	 */
	template<typename Matrix, typename Vector>
		void setup(const Matrix& _A, const Vector& _rhs, Vector& _lhs)
	{
		if(m_verbose_level>2)
		{
			m_amg_info->m_memory_used->start("AMGSetup");
			m_amg_info->m_profiler->start("AMGSetup");
		}
		m_setup->setup(_A,_rhs,_lhs);
		m_initialised = true;
		m_max_level_has_changed = false;
		if(m_verbose_level>2)
		{
			m_amg_info->m_profiler->stop("AMGSetup");
			m_amg_info->m_memory_used->stop("AMGSetup");
		}
	}
	//@}

	/*! \name Public solve
	 *
	 *  Solve method of the AMG solver. \n
	 */
	//@{
	/*! \brief Apply the solve phase of the AMG solver
	 *
	 *  Solve the different linear system by calling the solve method of the solve phase. \n
	 *  If the setup method hasn't been called previously, the code abort. \n
	 *  If the number of max level has changed between the call to the setup and the solve method, the code abort.
	 *
	 *  \tparam Matrix The fine matrix type
	 *  \tparam Vector The fine vectors type
	 *
	 *  \param[in] _A The fine matrix
	 *  \param[in] _rhs The fine rhs
	 *  \param[out] _lhs The fine lhs
	 */
	template<typename Matrix, typename Vector>
    void solve(const Matrix& _A, const Vector& _rhs, Vector& _lhs)
	{
		if(!m_initialised)
			YALLA_ERR("Error, the class " << this->getClassName() << " is not initialised. \nYou should probably run the setup method before the solve method.")
				if(m_max_level_has_changed)
					YALLA_ERR("Error, the class " << this->getClassName() << " is not properly initialised. \nThe number of grids used in the method has changed between the last setup phase and this solve phase. \nYou should probably run the setup method before the solve method.")
						if(m_verbose_level>2)
						{
							m_amg_info->m_memory_used->start("AMGSolve");
							m_amg_info->m_profiler->start("AMGSolve");
						}
		Matrix* A = m_setup->getMatrixHierarchy();
		Vector* rhs = m_setup->getRHSHierarchy();
		Vector* lhs = m_setup->getLHSHierarchy();
		Matrix** Interpolation = m_setup->getInterpolationMatrices();
		Matrix** Restriction = m_setup->getRestrictionMatrices();
		m_solve->solve(A, Interpolation, Restriction, rhs, lhs, 0);
		_lhs = lhs[0];
		if(m_verbose_level>2)
		{
			m_amg_info->m_profiler->stop("AMGSolve");
			m_amg_info->m_memory_used->stop("AMGSolve");
		}
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class AMG. \n
	 *  These methods provide various utilities to the class AMG
	 */
	//@{

	/*!
	 *  \brief Display the information related to an AMG solver on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _solver The solver to print
	 */
	friend std::ostream& operator<<(std::ostream& _os, const this_type& _solver)
	{
		const AMGInfo* infos = _solver.getInfos();
		if(infos==0)
			return _os;
		std::string infoMessage = "";
		std::ostringstream oss;
		oss.setf(std::ios::scientific);
		oss.precision(6);
		if(_solver.getVerboseLvl()>0) // Cv rate, residual and scaled residual norm
		{
			infoMessage.append("=============================================\n");
			infoMessage.append("                      AMG Infos                      \n");
			infoMessage.append("=====================================================\n");
			infoMessage.append("Cycle -   Conv. Rate  - Residual Norm - Scaled R Norm\n");
			infoMessage.append("=====================================================\n");
			for(unsigned int i=0;i<infos->m_residual_norm.size();++i)
			{
				oss.str("");
				infoMessage.append("  ");
				oss << i;
				infoMessage.append(oss.str());
				infoMessage.append("   - ");
				oss.str("");
				oss << infos->m_convergence_rate[i];
				infoMessage.append(oss.str());
				infoMessage.append(" - ");
				oss.str("");
				oss << infos->m_residual_norm[i];
				infoMessage.append(oss.str());
				infoMessage.append(" - ");
				oss.str("");
				oss << infos->m_scaled_residual_norm[i];
				infoMessage.append(oss.str());
				infoMessage.append("\n");
			}
			if(_solver.getVerboseLvl()>1) // coarsening rate and operator complexity
			{
				oss.unsetf(std::ios::scientific);
				oss.setf(std::ios::fixed);
				oss.precision(6);
				infoMessage.append("=====================================================\n");
				infoMessage.append(" LVL - Coarsening R. - Op Complexity                \n");
				double sum = 0;
				for(int i=0;i<_solver.getMaxLevel()+1;++i)
				{
					oss.str("");
					infoMessage.append("  ");
					oss << i;
					infoMessage.append(oss.str());
					infoMessage.append("  -    ");
					oss.str("");
					oss << infos->m_coarsening_rate[i];
					infoMessage.append(oss.str());
					infoMessage.append("   -   ");
					oss.str("");
					sum += infos->m_operator_complexity[i];
					oss << sum/infos->m_operator_complexity[0];
					infoMessage.append(oss.str());
					infoMessage.append("\n");
				}
			}
			if(_solver.getVerboseLvl()>2) // Time and memory
			{
				oss.unsetf(std::ios::scientific);
				oss.setf(std::ios::fixed);
				oss.precision(6);
				infoMessage.append("=====================================================\n");
				infoMessage.append(" Time -   Total  -   Setup  -   Solve               \n");
				oss.str("");
				infoMessage.append(" [s]    ");
				const double timeAMGSetup = infos->m_profiler->getTime("AMGSetup");
				const double timeAMGSolve = infos->m_profiler->getTime("AMGSolve");
				const double timeTotal = timeAMGSetup + timeAMGSolve;
				oss << timeTotal;
				infoMessage.append(oss.str());
				infoMessage.append(" - ");
				oss.str("");
				oss << timeAMGSetup;
				infoMessage.append(oss.str());
				infoMessage.append(" - ");
				oss.str("");
				oss << timeAMGSolve;
				infoMessage.append(oss.str());
				infoMessage.append("\n");
				oss.precision(2);
				oss.str("");
				infoMessage.append(" [%]      ");
				oss << timeTotal/timeTotal*100;
				infoMessage.append(oss.str());
				infoMessage.append(" -    ");
				oss.str("");
				oss << timeAMGSetup/timeTotal*100;
				infoMessage.append(oss.str());
				infoMessage.append(" -    ");
				oss.str("");
				oss << timeAMGSolve/timeTotal*100;
				infoMessage.append(oss.str());
				infoMessage.append("\n");
				infoMessage.append("=====================================================\n");
				infoMessage.append(" Mem. -   Total  -   Setup  -   Solve                \n");
				const double memoryAMGSetup = infos->m_memory_used->getMem("AMGSetup");
				const double memoryAMGSolve = infos->m_memory_used->getMem("AMGSolve");
				const double memoryTotal = memoryAMGSetup + memoryAMGSolve;
				oss.precision(6);
				oss.str("");
				infoMessage.append(" [Mb]   ");
				oss << memoryTotal;
				infoMessage.append(oss.str());
				infoMessage.append(" - ");
				oss.str("");
				oss << memoryAMGSetup;
				infoMessage.append(oss.str());
				infoMessage.append(" - ");
				oss.str("");
				oss << memoryAMGSolve;
				infoMessage.append(oss.str());
				infoMessage.append("\n");
				oss.precision(2);
				oss.str("");
				infoMessage.append(" [%]      ");
				oss << memoryTotal/memoryTotal*100;
				infoMessage.append(oss.str());
				infoMessage.append(" -    ");
				oss.str("");
				oss << memoryAMGSetup/memoryTotal*100;
				infoMessage.append(oss.str());
				infoMessage.append(" -    ");
				oss.str("");
				oss << memoryAMGSolve/memoryTotal*100;
				infoMessage.append(oss.str());
				infoMessage.append("\n");
			}
			infoMessage.append("=====================================================\n");
		}
		_os << infoMessage;
		return _os;
	}
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class AMG. \n
	 *  These methods provide various utilities to the class AMG
	 */
	//@{
 private:
	//! \brief Set default values for the AMG solver
	void setDefaults()
	{
		m_max_level = 1;
		m_verbose_level = 0;
		m_max_cycles = 1;
		m_tolerance = 1e-7;
		m_amg_info = 0;
		m_initialised = false;
		m_max_level_has_changed = false;
		m_setup = new setup_phase_type();
		m_solve = new solve_phase_type();
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
