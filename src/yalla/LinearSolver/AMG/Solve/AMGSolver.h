#pragma once
#ifndef yalla_amg_solver_h
#define yalla_amg_solver_h

/*!
 *  \file AMGSolver.h
 *  \brief Solve phase of the AMG solver
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/LinearSolver/AMG/AMGInfo.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
//! \brief Forward declaration
class NoSmoother;

/*!
 *  \class AMGSolver
 *  \brief Solve phase of the AMG solver
 * 
 *  \details
 *  This class follow a policy-based design and therefore has three template parameters:
 *  - \tparam CoarseSolver The solver used at the coarse level
 *  - \tparam Smoother The smoother used between grids transfers
 *  - \tparam Cycle The cycle used to solve the linear systems
 *
 *  This class essentially calls the cycle and the smoother which are in charge of solving the linear systems. \n
 *  By default, no smoother is used and a V-cycle is used to solve the problem
 *
 *  \todo To add a different pre/post smoother, one just has to modify the Smoother template to make it the pre or post smoother and add a template parameter for the other smoother.
 *  \todo To use different pre/post smoother on the different levels, one has to implement a "composite" class, containing the different smoother, and the number of level that he wants to apply the smoother on
 */
template<typename CoarseSolver, typename Smoother = NoSmoother, typename Cycle = VCycle>
  class AMGSolver
{
 public:
 //! \name Public Typedefs
 //@{
 //! \brief Type of the coarse solver used
 typedef CoarseSolver coarse_solver_type;
 //! \brief Type of the smoother used for the pre/post smoothing sweeps
 typedef Smoother smoother_type;
 //! \brief Type of the cycle used to solve the problem
 typedef Cycle cycle_type;
 //! \brief Type of this class
 typedef AMGSolver<coarse_solver_type, smoother_type, cycle_type> this_type;
 //@}
 private:
 //! \brief Max number of level in the grid hierarchy
 int m_max_level;
 //! \brief Max number of cycle
 int m_max_cycles;
 //! \brief Solver tolerance
 double m_tolerance;
 //! \brief Pointer on the coarse solver
 coarse_solver_type* m_solver;
 //! \brief Pointer on the smoother
 smoother_type* m_smoother;
 //! \brief Pointer on the cycle
 cycle_type* m_cycle;
 //! \brief Verbosity level
 int m_verbose_level;
 //! \brief Information on the AMG solver execution
 AMGInfo* m_amg_info;
 public:
 /*! \name Public Constructors
	*
	*  Public constructors of the class AMGSolver
	*/
 //@{
		
 /*! 
	*  \brief Default constructor.
	*
	*  \details
	*  Calls the method setDefaults
	*/
 explicit AMGSolver()
 {
	 this->setDefaults();
 }
 //@}
 /*! \name Public Destructor
	*
	*  Public destructor of the class AMGSolver
	*/
 //@{

 /*! 
	*  \brief Destructor.
	*
	*  \details
	*  Free the memory allocated by the class i.e. the coarse solver, the smoother and the cycle pointers
	*/
 virtual ~AMGSolver()
 {
	 delete m_solver;
	 delete m_smoother;
	 delete m_cycle;
 }
 //@}

 /*! \name Public Getter
	*
	*  Public Getter of the class AMGSolver. \n
	*  These methods give access in read-only mode to the datas/information of the class AMGSolver
	*/
 //@{

 /*! \brief  Return the class name. 
	*
	*  \return "AMGSolver" The name of the class
	*/
 const char* getClassName() const
 {
	 return "AMGSolver";
 }
 //@}

 /*! \name Public Setter
	*
	*  Public Setter of the class AMGSolver. \n
	*  These methods give access in read/write mode to the datas/information of the class AMGSolver
	*/
 //@{
 /*! \brief Define the number of pre-smoothing sweeps
	*
	*  Call the method setPreSmoothSweeps of the cycle
	*  
	*  \param[in] _nbSweepsPreSmooth Number of pre-smoothing sweeps
	*/
 void setPreSmoothSweeps(const int _nbSweepsPreSmooth)
 {
	 m_cycle->setPreSmoothSweeps(_nbSweepsPreSmooth);
 }

 /*! \brief Define the number of post-smoothing sweeps
	*
	*  Call the method setPostSmoothSweeps of the cycle
	*  
	*  \param[in] _nbSweepsPostSmooth Number of pre-smoothing sweepss
	*/
 void setPostSmoothSweeps(const int _nbSweepsPostSmooth)
 {
	 m_cycle->setPostSmoothSweeps(_nbSweepsPostSmooth);
 }

 /*! \brief Set the verbosity level
	*
	*  \param[in] _lvl The verbosity level
	*  \param[in] _amgInfo The structure to store the solver execution information
	*/
 void setVerbose(const int _lvl, AMGInfo* _amgInfo)
 {
	 m_verbose_level =_lvl;
	 m_amg_info = _amgInfo;
	 m_cycle->setVerbose(_lvl,m_amg_info);
 }

 /*! \brief Set the max number of cycle used
	*
	*  \param[in] _maxCycles The max number of cycles
	*/
 void setMaxCycles(const int _maxCycles)
 {
	 m_max_cycles =_maxCycles;
 }

 /*! \brief Set the solver tolerance
	*
	*  \param[in] _tol The tolerance
	*/
 void setTolerance(const double _tol)
 {
	 m_tolerance =_tol;
 }

 /*! \brief Set the maximum number of grids used
	*
	*  \param[in] _maxLvl The max number of grids used
	*/
 void setMaxLevel(const int _maxLvl)
 {
	 m_max_level =_maxLvl;
	 m_cycle->setMaxLevel(_maxLvl);
 }
 //@}

 /*! \name Public solve
	*
	*  Solve method of the AMGSolver class. \n
	*/
 //@{
 /*! \brief Apply the solve phase of the AMG solver
	*
	*  Call the solve method of the cycle.
	*
	*  \tparam Matrix The matrix hierarchy type
	*  \tparam pMatrix The interpolation/restriction matrix hierarchy type
	*  \tparam Vector The lhs/rhs hierarchy type
	*
	*  \param[in] _A The coarse matrices hierarchy
	*  \param[in] _Interpolation The interpolation matrices hierarchy
	*  \param[in] _Restriction The restriction matrices hierarchy
	*  \param[in] _rhs The rhs hierarchy
	*  \param[in,out] _lhs The lhs hierarchy
	*  \param[in] _currentLevel The current level
	*/
 template<typename Matrix, typename pMatrix, typename Vector>
 void solve(const Matrix* _A, const pMatrix* _Interpolation, const pMatrix* _Restriction, Vector* _rhs, Vector* _lhs, int _currentLevel)
 {
	 const double initRes = this->computeResidual(_A[0], _rhs[0], _lhs[0]);
	 const double rhsNorm = Math::normL2(_rhs[0]);
	 if(m_verbose_level)
	 {
		 m_amg_info->m_residual_norm.push_back(initRes);
		 m_amg_info->m_scaled_residual_norm.push_back(initRes/rhsNorm);
		 m_amg_info->m_convergence_rate.push_back(1);
	 }

	 double err = initRes;
	 int nbCycle = 0;

	 while(nbCycle<m_max_cycles && err>m_tolerance)
	 {
		 m_cycle->solve(_A, _Interpolation, _Restriction, _rhs, _lhs, _currentLevel, m_solver, m_smoother);
		 ++nbCycle;
		 double newResidual = this->computeResidual(_A[0], _rhs[0], _lhs[0]);
		 err = newResidual/initRes;
		 if(m_verbose_level)
		 {
			 m_amg_info->m_residual_norm.push_back(newResidual);
			 m_amg_info->m_scaled_residual_norm.push_back(newResidual/rhsNorm);
			 m_amg_info->m_convergence_rate.push_back(err);
		 }
	 }
 }
 //@}

 private:
 /*! \name Private Utils
	*
	*  Private Utils of the class AMGSolver. \n
	*  These methods provide various utilities to the class AMGSolver
	*/
 //@{

 /*! \brief Compute the residual
	*
	*  \param[in] _A La matrice
	*  \param[in] _rhs Le second membre
	*  \param[in] _lhs La solution
	*
	*  \return The residual norm
	*
	*  \todo This method has to disappear and replaced by the one implemented in BLASLike operations (duplicated code and this method is probably less efficient)
	*/
 template<typename Matrix, typename Vector>
 double computeResidual(const Matrix& _A, const Vector& _rhs, const Vector& _lhs)
 {
	 Vector tmpVector = Vector(_rhs.getNbRows(),1);
	 for(int k=0;k<_A.getNbRows();++k)
	 {
		 for(int l=0;l<_A.getNbCols();++l)
		 {
			 tmpVector(k) -=  _A(k,l) * _lhs(l);
		 }
		 tmpVector(k) +=_rhs(k);
	 }
	 return Math::normL2(tmpVector);
 }

 //! \brief Set default values for the AMGSolver class
 void setDefaults()
 {
	 m_max_level = 1;
	 m_verbose_level = 0;
	 m_max_cycles = 1;
	 m_tolerance = 1e-7;
	 m_solver = new coarse_solver_type();
	 m_smoother = new smoother_type();
	 m_cycle = new cycle_type();
 }
 //@}
};
YALLA_END_NAMESPACE
#endif
