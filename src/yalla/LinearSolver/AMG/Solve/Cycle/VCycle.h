#pragma once
#ifndef yalla_v_cycle_h
#define yalla_v_cycle_h

/*!
 *  \file VCycle.h
 *  \brief V-cycle for the AMG solver
 *  \date 27/05/2012
 *  \author Xavier TUNC
 *  \version 0.1
 */

#include "yalla/LinearSolver/AMG/AMGInfo.h"


YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class VCycle
 *  \brief V-cycle for the AMG solver
 * 
 *  \details
 *  The V-cycle is a cycle where the coarsest level is visited only one by cycle. \n
 *  At the coarsest level, the linear system is solved using the coarse solver specified as parameter. 
 */
class VCycle
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef VCycle this_type;
	//@}
 private:
	//! \brief Max number of level in the grid hierarchy
	int m_max_level;
	//! \brief The number of pre-smoothing sweeps
	int m_pre_smooth_steps;
	//! \brief The number of post-smoothing sweeps
	int m_post_smooth_steps;
	//! \brief Verbosity level
	int m_verbose_level;
	//! \brief Information on the AMG solver execution
	AMGInfo* m_amg_info;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class VCycle
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Calls the method setDefaults
	 */
	explicit VCycle()
	{
		this->setDefaults();
	}
	//@}
	/*! \name Public Destructor
	 *
	 *  Public destructor of the class VCycle
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. nothing (no memory allocated by this class).
	 */
	virtual ~VCycle()
	{
		;
	}
	//@}
	/*! \name Public Getter
	 *
	 *  Public Getter of the class VCycle. \n
	 *  These methods give access in read-only mode to the datas/information of the class VCycle
	 */
	//@{
	/*! \brief  Return the class name. 
	 *
	 *  \return "VCycle" The name of the class
	 */
	const char* getClassName() const
	{
		return "VCycle";
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class VCycle. \n
	 *  These methods give access in read/write mode to the datas/information of the class VCycle
	 */
	//@{
	/*! \brief Define the number of pre-smoothing sweeps
	 *
	 *  Call the method setPreSmoothSweeps of the cycle
	 *  
	 *  \param[in] _nbSweepsPreSmooth Number of pre-smoothing sweeps
	 */
	void setPreSmoothSweeps(const int _nbSweepsPreSmooth)
	{
		m_pre_smooth_steps = _nbSweepsPreSmooth;
	}

	/*! \brief Define the number of post-smoothing sweeps
	 *
	 *  Call the method setPostSmoothSweeps of the cycle
	 *  
	 *  \param[in] _nbSweepsPostSmooth Number of pre-smoothing sweepss
	 */
	void setPostSmoothSweeps(const int _nbSweepsPostSmooth)
	{
		m_post_smooth_steps = _nbSweepsPostSmooth;
	}

	/*! \brief Set the verbosity level
	 *
	 *  \param[in] _lvl The verbosity level
	 *  \param[in] _amgInfo The structure to store the solver execution information
	 */
	void setVerbose(const int _lvl, AMGInfo* _amgInfo)
	{
		m_verbose_level =_lvl;
		m_amg_info = _amgInfo;
	}

	/*! \brief Set the maximum number of grids used
	 *
	 *  \param[in] _maxLvl The max number of grids used
	 */
	void setMaxLevel(const int _maxLvl)
	{
		m_max_level =_maxLvl;
	}
	//@}

	/*! \name Public solve
	 *
	 *  Solve method of the AMGSolver class. \n
	 */
	//@{
	/*! \brief Solve the linear systems following a V-cycle
	 *
	 *  If the current level is not the max level:
	 *  - m_pre_smooth_steps of the smoother are applied (1)
	 *  - The residual is updated (2)
	 *  - Steps (1) and (2) are repeated while the current level is incremented by one
	 * 
	 *  If the current level is the max level (i.e. the coarsest level):
	 *  - The coarse linear system is solved with the specified coarse solver
	 *  - The solution is then propagated to the "upper" levels (finer)
	 * 
	 *  While the solution is propagated to the finest level:
	 *  - The solution at the "upper" level is updated through the interpolator (finer)
	 *  - m_post_smooth_steps of the smoother are applied
	 *
	 *  \tparam MatrixType The matrix hierarchy type
	 *  \tparam pMatrix The interpolation/restriction matrix hierarchy type
	 *  \tparam VectorType The lhs/rhs hierarchy type
	 *  \tparam Solver The coarse solver type
	 *  \tparam Smoother The smoother type
	 *
	 *  \param[in] _A The coarse matrix hierarchy
	 *  \param[in] _Interpolation The interpolation matrix hierarchy
	 *  \param[in] _Restriction The restriction matrix hierarchy
	 *  \param[in] _rhs The rhs/residual hierarchy
	 *  \param[in,out] _lhs The lhs/interpolation error hierarchy
	 *  \param[in] _currentLevel The current level
	 *  \param[in] _solver The coarse solver
	 *  \param[in] _smoother The pre/post smoother
	 */
	template<typename MatrixType, typename pMatrixType, typename VectorType, typename Solver, typename Smoother>
    void solve(const MatrixType* _A, const pMatrixType* _Interpolation, const pMatrixType* _Restriction, VectorType* _rhs, VectorType* _lhs, int _currentLevel, Solver* _solver, Smoother* _smoother)
	{
		if(_currentLevel!=m_max_level)
		{
			_smoother->setMaxIterations(m_pre_smooth_steps);
			_smoother->solve(_A[_currentLevel],_rhs[_currentLevel],_lhs[_currentLevel]);
			this->updateResidual(_A[_currentLevel], _Restriction[_currentLevel][0], _rhs[_currentLevel], _lhs[_currentLevel], _rhs[_currentLevel+1]);
			_lhs[_currentLevel+1] = VectorType(_rhs[_currentLevel+1].getNbRows(), _rhs[_currentLevel+1].getNbCols());
			this->solve(_A, _Interpolation, _Restriction, _rhs, _lhs, ++_currentLevel, _solver, _smoother);
			--_currentLevel;
			this->updateSolution(_Interpolation[_currentLevel][0], _lhs[_currentLevel+1], _lhs[_currentLevel]);
			_smoother->setMaxIterations(m_post_smooth_steps);
			_smoother->solve(_A[_currentLevel],_rhs[_currentLevel],_lhs[_currentLevel]);
		}
		else
		{
			_solver->solve(_A[_currentLevel],_rhs[_currentLevel],_lhs[_currentLevel]);
			VectorType tmpVector = VectorType(_rhs[_currentLevel].getNbRows(), _rhs[_currentLevel].getNbCols());
			for(int k=0;k<_A[_currentLevel].getNbRows();++k)
			{
				for(int l=0;l<_A[_currentLevel].getNbCols();++l)
				{
					tmpVector(k) -=  _A[_currentLevel](k,l) * _lhs[_currentLevel](l);
				}
				tmpVector(k) += _rhs[_currentLevel](k);
			}
		}
	}
	//@}

 private:
	/*! \name Private Utils
	 *
	 *  Private Utils of the class VCycle. \n
	 *  These methods provide various utilities to the class VCycle
	 */
	//@{

	/*! \brief Update the residual and restrict it on the lower (coarser) grid
	 *
	 *  This happens in two steps:
	 *  - Compute the residual: tmp = b^k - A^k x^k
	 *  - Propagate the residual to the coarser level: b^(k+1) = Restriction * tmp
	 *  
	 *  \tparam MatrixType The type of the matrices
	 *  \tparam VectorType The type of the vectors
	 *
	 *  \param[in] _A The working matrix
	 *  \param[in] _Restriction The restriction operator
	 *  \param[in] _Residual The rhs/residual
	 *  \param[in] _solution The lhs/interpolation error
	 *  \param[in,out] _newResidual The new residual
	 *
	 *  \return residualNorm The L2 residual norm
	 */
	template<typename MatrixType, typename VectorType>
    typename MatrixType::data_type updateResidual(const MatrixType& _A, const MatrixType& _Restriction, const VectorType& _Residual, const VectorType& _solution, VectorType& _newResidual)
	{
		const int nbRows = _Restriction.getNbRows();
		const int nbCols = _Restriction.getNbCols();
		const int ANbRows = _A.getNbRows();
		const int ANbCols = _A.getNbCols();

		_newResidual = VectorType(nbRows,_solution.getNbCols());
		VectorType tmpVector = VectorType(_Residual.getNbRows(), _solution.getNbCols());
		for(int k=0;k<ANbRows;++k)
		{
			for(int l=0;l<ANbCols;++l)
			{
				tmpVector(k) -=  _A(k,l) * _solution(l);
			}
			tmpVector(k) +=_Residual(k);
		}
		for(int i=0;i<nbRows;++i)
		{
			for(int j=0;j<nbCols;++j)
			{
				_newResidual(i) += _Restriction(i,j) * tmpVector(j);
			}
		}
		return Math::normL2(tmpVector);
	}

	/*! \brief Update the solution and interpolate it on the upper (finer) grid
	 *
	 *  This happens in one step:
	 *  - _newSolution += _Interpolation * _previousSolution
	 *  
	 *  \tparam MatrixType The type of the matrices
	 *  \tparam VectorType The type of the vectors
	 *
	 *  \param[in] _Interpolation The interpolation operator
	 *  \param[in] _previousSolution The lower level solution
	 *  \param[in,out] _newSolution The new solution
	 */
	template<typename MatrixType, typename VectorType>
    void updateSolution(const MatrixType& _Interpolation, const VectorType& _previousSolution, VectorType& _newSolution)
	{
		typedef typename MatrixType::data_type data_type;
		const int nbRows = _Interpolation.getNbRows();
		const int nbCols = _Interpolation.getNbCols();

		for(int i=0;i<nbRows;++i)
		{
			data_type tmp = 0;
			for(int j=0;j<nbCols;++j)
				tmp += _Interpolation(i,j) * _previousSolution(j);
			_newSolution(i) += tmp;
		}
	}

	//! \brief Set default values for the VCycle class
	void setDefaults()
	{
		m_max_level = 1;
		m_pre_smooth_steps = 1;
		m_post_smooth_steps = 1;
		m_verbose_level = 0;
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
