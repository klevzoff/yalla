#pragma once
#ifndef yalla_amg_info_h
#define yalla_amg_info_h

/*!
 *  \file AMGInfo.h
 *  \brief Structure to store AMG information
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <vector>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Profiler/Profiler.h"
#include "yalla/Utils/MemoryCheck/MemoryCheck.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
//! \brief Structure to store AMG execution information
struct AMGInfo
{
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef AMGInfo this_type;
	//@}
	//! \brief Vector to store the convergence rate
	std::vector<double> m_convergence_rate;
	//! \brief Vector to store the L2 residual norm
	std::vector<double> m_residual_norm;
	//! \brief Vector to store the L2 scaled residual norm
	std::vector<double> m_scaled_residual_norm;
	//! \brief Vector to store the coarsening rate
	std::vector<double> m_coarsening_rate;
	//! \brief Vector to store the operator complexity
	std::vector<double> m_operator_complexity;
	//! \brief Memory consumption of the AMG solver
	MemoryCheck * m_memory_used;
	//! \brief Profiler for the different steps of the AMG solver
	Profiler * m_profiler;

	/*! \name Public Constructors
	 *
	 *  Public constructors of the class AMGInfo
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  If the verbosity level is greater than 2, allocates the memory and profiling logger
	 *
	 *  \param[in] _verboseLvl Verbosity level
	 */
	explicit AMGInfo(const int _verboseLvl)
	{
		m_profiler = 0;
		m_memory_used = 0;
		if(_verboseLvl>2)
		{
			m_profiler = new Profiler();
			m_memory_used = new MemoryCheck();
		}
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class AMGInfo
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the memory and profiling pointers
	 */
	virtual ~AMGInfo()
	{
		delete m_profiler;
		m_profiler = 0;
		delete m_memory_used;
		m_memory_used = 0;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class AMGInfo. \n
	 *  These methods give access in read-only mode to the datas/information of the class AMGInfo
	 */

	//@{
	/*! \brief  Return the class name. 
	 *
	 *  \return "AMGInfo" The name of the class
	 */
	const char* getClassName() const
	{
		return "AMGInfo";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
