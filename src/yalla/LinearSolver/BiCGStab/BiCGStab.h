#pragma once
#ifndef yalla_bicgstab_h
#define yalla_bicgstab_h

/*!
 *  \file BiCGStab.h
 *  \brief Biconjugate Gradient Stabilized solver
 *  \date 12/25/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToVector.h"
#include "yalla/Utils/BLAS/BLAS.h"
#include "yalla/LinearSolver/BiCGStab/BiCGStabInfo.h"
#include "yalla/LinearSolver/NoPrecond.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
//! Forward declaration
class NoPrecond;

/*!
 *  \class BiCGStab
 *  \brief Biconjugate Gradient Stabilized solver
 * 
 *  \details
 *  As usual all allocations are done in the setup method. \n
 *  This implementation can be improved in several ways (at least):
 *  - In the setup method, it would be mre efficient to copy only the structure of the vectors and not the values. Otherwise they have to be zeroed after that (as in the actual implementation)
 *  - In the solve method, the first iteration could be removed of the loop. This would save a test for each iteration
 *  - Kind of a general remark for all solvers. All tests done to log information have an impact on the performance. However, I have no idea how big is the impact. Anyway, there should be a smarter/more efficient way to do that
 *
 *  The two class template parameters are:
 *  - \tparam MatrixType The type of the matrix
 *  - \tparam Precond The type of the preconditioner
 *
 *  This implementation works for both sequential and distributed cases. \n
 *  The nomenclature is the one used here: http://www.netlib.org/templates/templates.pdf
 */
template<typename MatrixType, typename Precond = NoPrecond>
  class BiCGStab
{
 public:
 //! \name Public Typedefs
 //@{
 //! \brief Type of the matrix
 typedef MatrixType matrix_type;
 //! \brief Data type of the matrix
 typedef typename MatrixType::data_type data_type;
 //! \brief Consistent vector type
 typedef typename PromoteToVector<matrix_type, typename matrix_type::data_type>::data_type vector_type;
 //! \brief Preconditionner type
 typedef Precond preconditionner_type;
 //! \brief Type of this class
 typedef BiCGStab<matrix_type, preconditionner_type> this_type;
 //@}
 private:
 //! \brief Solver tolerance
 double m_tolerance;
 //! \brief Max number of iteration
 int m_max_iterations;
 //! \brief Verbosity level
 int m_verbose_level;
 //! \brief Initialization flag
 bool m_initialised;
 //! \brief Preconditionner
 preconditionner_type * m_precond;
 //! \brief Information on the BiCGStab solver execution
 BiCGStabInfo * m_bicgstab_info;
 //! \brief Vector r
 vector_type * m_r;
 //! \brief Vector rtilde
 vector_type * m_rtilde;
 //! \brief Vector p
 vector_type * m_p;
 //! \brief Vector v
 vector_type * m_v;
 //! \brief Vector pHat
 vector_type * m_pHat;
 //! \brief Vector s
 vector_type * m_s;
 //! \brief Vector sHat
 vector_type * m_sHat;
 //! \brief Vector t
 vector_type * m_t;
 public:
 /*! \name Public Constructors
	*
	*  Public constructors of the class BiCGStab
	*/
 //@{
		
 /*! 
	*  \brief Default constructor.
	*
	*  \details
	*  Calls the method setDefaults
	*/
 explicit BiCGStab()
 {
	 this->setDefaults();
 }
 //@}

 /*! \name Public Destructor
	*
	*  Public destructor of the class BiCGStab
	*/
 //@{

 /*! 
	*  \brief Destructor.
	*
	*  \details
	*  Free the memory allocated by the class i.e. the different vectors, the preconditionner and the information data stucture
	*/
 virtual ~BiCGStab()
 {
	 this->deallocate();
	 delete m_precond;
	 m_precond = 0;
	 delete m_bicgstab_info;
	 m_bicgstab_info = 0;
 }
 //@}

 /*! \name Public Getter
	*
	*  Public Getter of the class BiCGStab. \n
	*  These methods give access in read-only mode to the datas/information of the class BiCGStab
	*/
 //@{

 /*! 
	*  \brief Return the verbosity level
	*
	*  \details
	*
	*  \return The verbosity level
	*/
 int getVerboseLvl() const
 {
	 return m_verbose_level;
 }

 /*! \brief Return the class name. 
	*
	*  \return "BiCGStab" The name of the class
	*/
 const char* getClassName() const
 {
	 return "BiCGStab";
 }

 /*! \brief Return a handler on the preconditionner
	*
	*  \return The preconditionner
	*/
 preconditionner_type* getPrecondHandler() const
 {
	 return m_precond;
 }

 /*! 
	*  \brief Return the structure storing execution information
	*
	*  \details
	*
	*  \return The execution information
	*/
 const BiCGStabInfo* getInfos() const
 {
	 return m_bicgstab_info;
 }
 //@}

 /*! \name Public Setter
	*
	*  Public Setter of the class BiCGStab. \n
	*  These methods give access in read/write mode to the datas/information of the class BiCGStab
	*/
 //@{

 /*! \brief Set the verbosity level
	*
	*  \param[in] _lvl The verbosity level
	*/
 void setVerbose(const int _lvl)
 {
	 m_verbose_level = _lvl;
	 if(m_verbose_level)
		 m_bicgstab_info = new BiCGStabInfo(_lvl);
 }

 /*! \brief Set the max number of iterations
	*
	*  \param[in] _iterMax The max number of iterations
	*/
 void setMaxIterations(const int _iterMax)
 {
	 m_max_iterations = _iterMax;
 }

 /*! \brief Set the solver tolerance
	*
	*  \param[in] _tol The tolerance
	*/
 void setTolerance(const double _tol)
 {
	 m_tolerance =_tol;
 }
 //@}

 /*! \name Public setup
	*
	*  Setup method of the BiCGStab solver. \n
	*/
 //@{
 /*! \brief Apply the setup phase of the BiCGStab solver
	*
	*  Allocate necessary vectors and set up the preconditionner.
	*
	*  \param[in] _A The matrix
	*  \param[in] _rhs The rhs
	*  \param[out] _lhs The lhs
	*/
 void setup(const matrix_type& _A, const vector_type& _rhs, vector_type& _lhs)
 {
	 if(m_verbose_level>1)
	 {
		 m_bicgstab_info->m_profiler->start("BiCGStabSetup");
		 m_bicgstab_info->m_memory_used->start("BiCGStabSetup");
	 }
	 this->deallocate();
	 m_r = new vector_type(_lhs);
	 m_r->fill(data_type());
	 m_rtilde = new vector_type(_lhs);
	 m_rtilde->fill(data_type());
	 m_p = new vector_type(_lhs);
	 m_p->fill(data_type());
	 m_v = new vector_type(_lhs);
	 m_v->fill(data_type());
	 m_pHat = new vector_type(_lhs);
	 m_pHat->fill(data_type());
	 m_s = new vector_type(_lhs);
	 m_s->fill(data_type());
	 m_sHat = new vector_type(_lhs);
	 m_sHat->fill(data_type());
	 m_t = new vector_type(_lhs);
	 m_t->fill(data_type());
	 m_precond->setup(_A,_rhs,_lhs);
	 if(m_verbose_level>1)
	 {
		 m_bicgstab_info->m_profiler->stop("BiCGStabSetup");
		 m_bicgstab_info->m_memory_used->stop("BiCGStabSetup");
	 }
	 m_initialised = true;
 }
 //@}

 /*! \name Public solve
	*
	*  Solve method of the BiCGStab solver. \n
	*/
 //@{

 /*! \brief Solve method of the BiCGStab solver
	*  
	*  Solve the linear system following the BiCGStab algorithm
	*
	*  \param[in] _A The matrix
	*  \param[in] _rhs The rhs
	*  \param[out] _lhs The lhs
	*/
 void solve(const matrix_type& _A, const vector_type& _rhs, vector_type& _lhs)
 {
	 if(!m_initialised)
		 YALLA_ERR("Error, the class " << this->getClassName() << " is not initialised.\nYou should probably run the setup method before the solve method.")
			 if(m_verbose_level>1)
			 {
				 m_bicgstab_info->m_profiler->start("BiCGStabSolve");
				 m_bicgstab_info->m_memory_used->start("BiCGStabSolve");
			 }
	 const data_type initRes = BLAS::computeNormAndResidual(_A, _rhs, _lhs, *m_r);
	 const data_type rhsNorm = BLAS::nrm2(_rhs);
	 BLAS::copy(*m_r, *m_rtilde);
	 data_type rho0 = 1.;
	 data_type omega = 1.;
	 data_type alpha = 0.;
	 data_type err = initRes;
	 int nbIter = 0;

	 if(m_verbose_level)
	 {
		 m_bicgstab_info->m_residual_norm.push_back(initRes);
		 m_bicgstab_info->m_scaled_residual_norm.push_back(initRes/rhsNorm);
		 m_bicgstab_info->m_convergence_rate.push_back(1);
	 }

	 while(nbIter<m_max_iterations && err>m_tolerance)
	 {
		 ++nbIter;

		 data_type rho1 = BLAS::dot(*m_rtilde,*m_r);
		 if(rho1==0)
			 YALLA_ERR("rho1 null abort");
		 if(nbIter!=1)
		 {
			 data_type beta = (rho1/rho0)*(alpha/omega);
			 BLAS::scal(beta, *m_p);
			 BLAS::axpy(1., *m_r, *m_p);
			 BLAS::axpy(- beta*omega, *m_v, *m_p);
		 }
		 else
			 BLAS::copy(*m_r,*m_p);
		 m_precond->solve(_A,*m_p,*m_pHat);
		 BLAS::scal(data_type(),*m_v);
		 BLAS::gemv(1.,_A,*m_pHat,1.,*m_v);
		 alpha = rho1/BLAS::dot(*m_rtilde, *m_v);
		 BLAS::axpyz(- alpha, *m_v, *m_r, *m_s);

		 if(BLAS::nrm2(*m_s)<m_tolerance)
		 {
			 BLAS::axpy(alpha, *m_pHat, _lhs);

			 data_type newRes = BLAS::computeNormOfResidual(_A,_rhs, _lhs);
			 err = newRes/initRes;
			 if(m_verbose_level)
			 {
				 m_bicgstab_info->m_residual_norm.push_back(newRes);
				 m_bicgstab_info->m_scaled_residual_norm.push_back(newRes/rhsNorm);
				 m_bicgstab_info->m_convergence_rate.push_back(err);
			 }
			 break;
		 }
		 m_precond->solve(_A,*m_s,*m_sHat);
		 BLAS::scal(data_type(),*m_t);
		 BLAS::gemv(1.,_A, *m_sHat, 1., *m_t);
		 omega = BLAS::dot(*m_t, *m_s) / BLAS::dot(*m_t, *m_t);
		 BLAS::axpy(alpha, *m_pHat, _lhs);
		 BLAS::axpy(omega, *m_sHat, _lhs);
		 BLAS::axpyz(- omega, *m_t, *m_s, *m_r);
		 rho0 = rho1;

		 data_type newRes = BLAS::computeNormOfResidual(_A,_rhs, _lhs);

		 err = newRes/initRes;
		 if(m_verbose_level)
		 {
			 m_bicgstab_info->m_residual_norm.push_back(newRes);
			 m_bicgstab_info->m_scaled_residual_norm.push_back(newRes/rhsNorm);
			 m_bicgstab_info->m_convergence_rate.push_back(err);
		 }
	 }
	 if(m_verbose_level>1)
	 {
		 m_bicgstab_info->m_profiler->stop("BiCGStabSolve");
		 m_bicgstab_info->m_memory_used->stop("BiCGStabSolve");
	 }
 }
 //@}

 /*! \name Public Utils
	*
	*  Public Utils of the class BiCGStab. \n
	*  These methods provide various utilities to the class BiCGStab
	*/
 //@{

 /*!
	*  \brief Display the information related to a BiCGStab solver on the standard output
	*
	*  \param[in] _os The output stream
	*  \param[in] _solver The solver to print
	*/
 friend std::ostream& operator<<(std::ostream& _os, const this_type& _solver)
 {
	 const BiCGStabInfo* infos = _solver.getInfos();
	 if(infos==0)
		 return _os;

	 std::string infoMessage = "";
	 std::ostringstream oss;
	 oss.setf(std::ios::scientific);
	 oss.precision(6);
	 if(_solver.getVerboseLvl()>0) // Cv rate, residual and scaled residual norm
	 {
		 infoMessage.append("=====================================================\n");
		 infoMessage.append("                   BiCGStab Infos                   \n");
		 infoMessage.append("=====================================================\n");
		 infoMessage.append("Iter. -   Conv. Rate  - Residual Norm - Scaled R Norm\n");
		 infoMessage.append("=====================================================\n");
		 for(unsigned int i=0;i<infos->m_residual_norm.size();++i)
		 {
			 oss.str("");
			 infoMessage.append("  ");
			 oss << i;
			 infoMessage.append(oss.str());
			 infoMessage.append("   - ");
			 oss.str("");
			 oss << infos->m_convergence_rate[i];
			 infoMessage.append(oss.str());
			 infoMessage.append(" - ");
			 oss.str("");
			 oss << infos->m_residual_norm[i];
			 infoMessage.append(oss.str());
			 infoMessage.append(" - ");
			 oss.str("");
			 oss << infos->m_scaled_residual_norm[i];
			 infoMessage.append(oss.str());
			 infoMessage.append("\n");
		 }
		 if(_solver.getVerboseLvl()>1) // Time and memory
		 {
			 oss.unsetf(std::ios::scientific);
			 oss.setf(std::ios::fixed);
			 oss.precision(6);
			 infoMessage.append("=====================================================\n");
			 infoMessage.append(" Time -   Total  -   Setup  -   Solve               \n");
			 oss.str("");
			 infoMessage.append(" [s]    ");
			 const double timeBICGSTABSetup = infos->m_profiler->getTime("BiCGStabSetup");
			 const double timeBICGSTABSolve = infos->m_profiler->getTime("BiCGStabSolve");
			 const double timeTotal = timeBICGSTABSetup + timeBICGSTABSolve;
			 oss << timeTotal;
			 infoMessage.append(oss.str());
			 infoMessage.append(" - ");
			 oss.str("");
			 oss << timeBICGSTABSetup;
			 infoMessage.append(oss.str());
			 infoMessage.append(" - ");
			 oss.str("");
			 oss << timeBICGSTABSolve;
			 infoMessage.append(oss.str());
			 infoMessage.append("\n");
			 oss.precision(2);
			 oss.str("");
			 infoMessage.append(" [%]      ");
			 oss << timeTotal/timeTotal*100;
			 infoMessage.append(oss.str());
			 infoMessage.append(" -    ");
			 oss.str("");
			 oss << timeBICGSTABSetup/timeTotal*100;
			 infoMessage.append(oss.str());
			 infoMessage.append(" -    ");
			 oss.str("");
			 oss << timeBICGSTABSolve/timeTotal*100;
			 infoMessage.append(oss.str());
			 infoMessage.append("\n");
			 infoMessage.append("=====================================================\n");
			 infoMessage.append(" Mem. -   Total  -   Setup  -   Solve                \n");
			 const double memoryBICGSTABSetup = infos->m_memory_used->getMem("BiCGStabSetup");
			 const double memoryBICGSTABSolve = infos->m_memory_used->getMem("BiCGStabSolve");
			 const double memoryTotal = memoryBICGSTABSetup + memoryBICGSTABSolve;
			 oss.precision(6);
			 oss.str("");
			 infoMessage.append(" [Mb]   ");
			 oss << memoryTotal;
			 infoMessage.append(oss.str());
			 infoMessage.append(" - ");
			 oss.str("");
			 oss << memoryBICGSTABSetup;
			 infoMessage.append(oss.str());
			 infoMessage.append(" - ");
			 oss.str("");
			 oss << memoryBICGSTABSolve;
			 infoMessage.append(oss.str());
			 infoMessage.append("\n");
			 oss.precision(2);
			 oss.str("");
			 infoMessage.append(" [%]      ");
			 oss << memoryTotal/memoryTotal*100;
			 infoMessage.append(oss.str());
			 infoMessage.append(" -    ");
			 oss.str("");
			 oss << memoryBICGSTABSetup/memoryTotal*100;
			 infoMessage.append(oss.str());
			 infoMessage.append(" -    ");
			 oss.str("");
			 oss << memoryBICGSTABSolve/memoryTotal*100;
			 infoMessage.append(oss.str());
			 infoMessage.append("\n");
		 }
		 infoMessage.append("=====================================================\n");
	 }
	 _os << infoMessage;
	 return _os;
 }
 //@}

 /*! \name Private Utils
	*
	*  Private Utils of the class BiCGStab. \n
	*  These methods provide various utilities to the class BiCGStab
	*/
 //@{
 private:
 //! \brief Free the memory allocated for the different vectors
 void deallocate()
 {
	 delete m_r;
	 m_r = 0;
	 delete m_rtilde;
	 m_rtilde = 0;
	 delete m_p;
	 m_p = 0;
	 delete m_v;
	 m_v = 0;
	 delete m_pHat;
	 m_pHat = 0;
	 delete m_s;
	 m_s = 0;
	 delete m_sHat;
	 m_sHat = 0;
	 delete m_t;
	 m_t = 0;
 }

 //! \brief Set default values for the BiCGStab solver
 void setDefaults()
 {
	 m_tolerance = 1e-7;
	 m_max_iterations = 10;
	 m_verbose_level = 0;
	 m_initialised = false;
	 m_r = 0;
	 m_rtilde = 0;
	 m_p = 0;
	 m_v = 0;
	 m_pHat = 0;
	 m_s = 0;
	 m_sHat = 0;
	 m_t = 0;
	 m_bicgstab_info = 0;
	 m_precond = new preconditionner_type();
 }
 //@}
};
YALLA_END_NAMESPACE
#endif
