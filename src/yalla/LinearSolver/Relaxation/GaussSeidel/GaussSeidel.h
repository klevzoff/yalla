#pragma once
#ifndef yalla_gauss_seidel_h
#define yalla_gauss_seidel_h

/*!
 *  \file GaussSeidel.h
 *  \brief Gauss-Seidel method
 *  \date 12/26/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <stdlib.h>
#include <sstream>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/LinearSolver/Relaxation/GaussSeidel/GaussSeidelInfo.h"
#include "yalla/Utils/BLAS/BLAS.h"
#include "yalla/IO/console/log.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class GaussSeidel
 *  \brief Sequential and parallel implementation of the Gauss-Seidel method.
 * 
 *  \details 
 *  The sequential implementation of this algorithm is very classic.
 *
 *  The parallel implementation needs to be refined. \n
 *  Right now, here is how it is implemented:
 *  - First, the unknown are colored in red and black. Even unknowns are labeled red, odds are labeled black.
 *  - Then all unknows of the same color are updated in the same time
 *
 *  However, the coloring is very very coarse and needs to be improved. This can probably be done in the setup method. For a two colors coloring, the PMIS can be used, one color would be the coarse unknowns, the other one the fine unknowns. How to iterate efficiently over the different colors still need to be defined. \n
 *  A more general approach could be implemented for more than two colors coloring, but that would need to implement/use a proper coloring algorithm.
 *
 *  \todo Implement the sequential and parallel version using iterators
 *  \todo Properly implement the coloring for parallel execution
 */
class GaussSeidel
{
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef GaussSeidel this_type;
	//@}
 private:
	//! \brief Max number of iteration
	int m_max_iterations;
	//! \brief Solver tolerance
	double m_tolerance;
	//! \brief Verbosity level
	int m_verbose_level;
	//! \brief Information on the BiCGStab solver execution
	GaussSeidelInfo * m_gs_info;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class GaussSeidel
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Calls the method setDefaults
	 */
	explicit GaussSeidel();
	//@}
    
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class GaussSeidel
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the information data stucture
	 */
	virtual ~GaussSeidel();
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class GaussSeidel. \n
	 *  These methods give access in read-only mode to the datas/information of the class GaussSeidel
	 */
	//@{

	/*! 
	 *  \brief Return the verbosity level
	 *
	 *  \details
	 *
	 *  \return The verbosity level
	 */
	int getVerboseLvl() const;

	/*! \brief Return the class name. 
	 *
	 *  \return "GaussSeidel" The name of the class
	 */
	const char* getClassName() const;

	/*! 
	 *  \brief Return the structure storing execution information
	 *
	 *  \details
	 *
	 *  \return The execution information
	 */
	const GaussSeidelInfo* getInfos() const;
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class GaussSeidel. \n
	 *  These methods give access in read/write mode to the datas/information of the class GaussSeidel
	 */
	//@{
	/*! \brief Set the verbosity level
	 *
	 *  \param[in] _lvl The verbosity level
	 */
	void setVerbose(const int _lvl);

	/*! \brief Set the max number of iterations
	 *
	 *  \param[in] _maxIterations The max number of iterations
	 */
	void setMaxIterations(const int _maxIterations);

	/*! \brief Set the solver tolerance
	 *
	 *  \param[in] _tol The tolerance
	 */
	void setTolerance(const double _tol);
	//@}

	/*! \name Public setup
	 *
	 *  Setup method of the GaussSeidel solver. \n
	 */
	//@{
	/*! \brief Apply the setup phase of the GaussSeidel
	 *
	 *  Nothing is done here. Probably the coloring could be done here for a distributed implementation.
	 *
	 *  \tparam MatrixType The type of the matrix
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[in,out] _lhs The lhs
	 */
	template<typename MatrixType, typename VectorType>
    void setup(const MatrixType& _A, const VectorType& _rhs, VectorType& _lhs)
	{
		;
	}
	//@}

	/*! \name Public solve
	 *
	 *  Solve method of the GaussSeidel solver. \n
	 */
	//@{

	/*! \brief Solve method of the GaussSeidel solver
	 *  
	 *  Solve the linear system following the GaussSeidel algorithm
	 *
	 *  \tparam MatrixType The type of the matrix
	 *  \tparam VectorType The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[out] _lhs The lhs
	 */
	template<typename MatrixType, typename VectorType>
    int solve(const MatrixType& _A, const VectorType& _rhs, VectorType& _lhs)
	{
#ifndef WITH_MPI
		if(m_verbose_level>1)
		{
			m_gs_info->m_profiler->start("GSSolve");
			m_gs_info->m_memory_used->start("GSSolve");
		}

		typedef typename MatrixType::data_type data_type;
		int converged = 0;
		data_type err = 0;
		const data_type initRes = BLAS::computeNormOfResidual(_A,_rhs,_lhs);
		const data_type rhsNorm = BLAS::nrm2(_rhs);
		if(m_verbose_level)
		{
			m_gs_info->m_residual_norm.push_back(initRes);
			m_gs_info->m_scaled_residual_norm.push_back(initRes/rhsNorm);
			m_gs_info->m_convergence_rate.push_back(1);
		}

		for(int nbIterations=0;nbIterations<m_max_iterations;++nbIterations)
		{
			const int nbRows = _A.getNbRows();
			const int nbCols = _A.getNbCols();

			for(int i=0;i<nbRows;++i)
			{
				data_type sigma = 0;
				for(int j=0;j<i;++j)
					sigma += _A(i,j) * _lhs(j);
				for(int j=i+1;j<nbCols;++j)
					sigma += _A(i,j) * _lhs(j);

				_lhs(i) = (_rhs(i) - sigma) / _A(i,i);
			}
			const double res = BLAS::computeNormOfResidual(_A,_rhs,_lhs);
			err = res/initRes;
			if(m_verbose_level)
			{
				m_gs_info->m_residual_norm.push_back(res);
				m_gs_info->m_scaled_residual_norm.push_back(res/rhsNorm);
				m_gs_info->m_convergence_rate.push_back(err);
			}
			if(err < m_tolerance)
			{
				converged = 1;
				if(m_verbose_level>1)
				{
					m_gs_info->m_profiler->stop("GSSolve");
					m_gs_info->m_memory_used->stop("GSSolve");
				}
				return converged;
			}
		}
		if(m_verbose_level>1)
		{
			m_gs_info->m_profiler->stop("GSSolve");
			m_gs_info->m_memory_used->stop("GSSolve");
		}
		return converged;
#else
		if(m_verbose_level>1)
		{
			m_gs_info->m_profiler->start("GSSolve");
			m_gs_info->m_memory_used->start("GSSolve");
		}

		typedef typename MatrixType::data_type data_type;
		int converged = 0;
		data_type err = 0;
		const data_type initRes = BLAS::computeNormOfResidual(_A,_rhs,_lhs);
		const data_type rhsNorm = BLAS::nrm2(_rhs);
		if(m_verbose_level)
		{
			m_gs_info->m_residual_norm.push_back(initRes);
			m_gs_info->m_scaled_residual_norm.push_back(initRes/rhsNorm);
			m_gs_info->m_convergence_rate.push_back(1);
		}

		const int nbRows = _A.getNbRows();
		const int nbCols = _A.getNbCols();

		data_type* locAii = new data_type[nbRows];
		data_type* globAii = new data_type[nbRows];
		data_type* locSigma = new data_type[nbRows];
		data_type* globSigma = new data_type[nbRows];

		for(int i=0;i<nbRows;++i)
		{
			locAii[i] = 0;
			globAii[i] = 0;
		}

		for(int j=0;j<nbCols;++j)
			locAii[_lhs.getMapping(j)] = _A(_lhs.getMapping(j),j);

		MPI_Allreduce(locAii, globAii, nbRows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

		delete[] locAii;

		int blackStartingIndex, redStartingIndex;
		if(_lhs.getMapping(0)%2==0)
		{
			redStartingIndex = 0;
			blackStartingIndex = 1;
		}
		else
		{
			redStartingIndex = 1;
			blackStartingIndex = 0;
		}

		for(int nbIterations=0;nbIterations<m_max_iterations;++nbIterations)
		{
			// Local computations
			for(int i=0;i<nbRows;++i)
			{
				locSigma[i] = 0;
				globSigma[i] = 0;
				for(int j=blackStartingIndex;j<nbCols;j+=2)
				{
					if(_lhs.getMapping(j)!=i)
						locSigma[i] += _A(i,j) * _lhs(j);
				}
			}

			MPI_Allreduce(locSigma, globSigma, nbRows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

			for(int i=redStartingIndex;i<nbCols;i+=2)
				_lhs(i) = (_rhs(i) - globSigma[_lhs.getMapping(i)]) / globAii[_lhs.getMapping(i)];

			for(int i=0;i<nbRows;++i)
			{
				for(int j=redStartingIndex;j<nbCols;j+=2)
				{
					if(_lhs.getMapping(j)!=i)
						locSigma[i] += _A(i,j) * _lhs(j);
				}
			}

			MPI_Allreduce(locSigma, globSigma, nbRows, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

			for(int i=blackStartingIndex;i<nbCols;i+=2)
				_lhs(i) = (_rhs(i) - globSigma[_lhs.getMapping(i)]) / globAii[_lhs.getMapping(i)];

			const double res = BLAS::computeNormOfResidual(_A,_rhs,_lhs);
			err = res/initRes;
			if(m_verbose_level)
			{
				m_gs_info->m_residual_norm.push_back(res);
				m_gs_info->m_scaled_residual_norm.push_back(res/rhsNorm);
				m_gs_info->m_convergence_rate.push_back(err);
			}
			if(err < m_tolerance)
			{
				converged = 1;
				if(m_verbose_level>1)
				{
					m_gs_info->m_profiler->stop("GSSolve");
					m_gs_info->m_memory_used->stop("GSSolve");
				}
				delete[] globAii;
				delete[] locSigma;
				delete[] globSigma;
				return converged;
			}
		}
		if(m_verbose_level>1)
		{
			m_gs_info->m_profiler->stop("GSSolve");
			m_gs_info->m_memory_used->stop("GSSolve");
		}
		delete[] globAii;
		delete[] locSigma;
		delete[] globSigma;
		return converged;
#endif
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class GaussSeidel. \n
	 *  These methods provide various utilities to the class GaussSeidel
	 */
	//@{

	/*!
	 *  \brief Display the information related to a GaussSeidel solver on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _solver The solver to print
	 */
	friend std::ostream& operator<<(std::ostream& _os, const this_type& _solver)
	{
		const GaussSeidelInfo* infos = _solver.getInfos();
		if(infos==0)
			return _os
				;
		std::string infoMessage = "";
		std::ostringstream oss;
		oss.setf(std::ios::scientific);
		oss.precision(6);
		if(_solver.getVerboseLvl()>0) // Cv rate, residual and scaled residual norm
		{
			infoMessage.append("=====================================================\n");
			infoMessage.append("                 Gauss-Seidel Infos                  \n");
			infoMessage.append("=====================================================\n");
			infoMessage.append("Iter. -   Conv. Rate  - Residual Norm - Scaled R Norm\n");
			infoMessage.append("=====================================================\n");
			for(unsigned int i=0;i<infos->m_residual_norm.size();++i)
			{
				oss.str("");
				infoMessage.append("  ");
				oss << i;
				infoMessage.append(oss.str());
				infoMessage.append("   - ");
				oss.str("");
				oss << infos->m_convergence_rate[i];
				infoMessage.append(oss.str());
				infoMessage.append(" - ");
				oss.str("");
				oss << infos->m_residual_norm[i];
				infoMessage.append(oss.str());
				infoMessage.append(" - ");
				oss.str("");
				oss << infos->m_scaled_residual_norm[i];
				infoMessage.append(oss.str());
				infoMessage.append("\n");
			}
			if(_solver.getVerboseLvl()>1) // Time and memory
			{
				oss.unsetf(std::ios::scientific);
				oss.setf(std::ios::fixed);
				oss.precision(6);
				infoMessage.append("=====================================================\n");
				infoMessage.append(" Time -   Solve               \n");
				oss.str("");
				infoMessage.append(" [s]    ");
				const double timeGSSolve = infos->m_profiler->getTime("GSSolve");
				oss.str("");
				oss << timeGSSolve;
				infoMessage.append(oss.str());
				infoMessage.append("\n");
				infoMessage.append("=====================================================\n");
				infoMessage.append(" Mem. -   Solve                \n");
				const double memoryGSSolve = infos->m_memory_used->getMem("GSSolve");
				oss.precision(6);
				oss.str("");
				infoMessage.append(" [Mb]   ");
				oss.str("");
				oss << memoryGSSolve;
				infoMessage.append(oss.str());
				infoMessage.append("\n");
			}
			infoMessage.append("=====================================================\n");
		}
		_os << infoMessage;
		return _os;
	}
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class GaussSeidel. \n
	 *  These methods provide various utilities to the class GaussSeidel
	 */
	//@{

 private:
	//! \brief Set default values for the BiCGStab solver
	void setDefaults();
	//@}
};
YALLA_END_NAMESPACE
#endif
