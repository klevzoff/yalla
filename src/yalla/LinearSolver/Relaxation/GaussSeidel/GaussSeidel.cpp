#include "GaussSeidel.h"

/*!
*  \file GaussSeidel.cpp
*  \brief Implementation of the Gauss Seidel method
*  \date 27/05/2012
*  \author Xavier TUNC
*  \version 0.1
*/

YALLA_USING_NAMESPACE(yalla)

GaussSeidel::
  GaussSeidel()
{
  this->setDefaults();
}

GaussSeidel::
  ~GaussSeidel()
{
  delete m_gs_info;
  m_gs_info = 0;
}

int
GaussSeidel::
getVerboseLvl() const
{
	return m_verbose_level;
}

const char* 
  GaussSeidel::
  getClassName() const
{
  return "GaussSeidel";
}

const GaussSeidelInfo*
GaussSeidel::
getInfos() const
{
	return m_gs_info;
}

void 
  GaussSeidel::
  setVerbose(const int _lvl)
{
  m_verbose_level = _lvl;
  if(m_verbose_level && !m_gs_info)
    m_gs_info = new GaussSeidelInfo(m_verbose_level);
}

void 
  GaussSeidel::
  setMaxIterations(const int _maxIterations)
{
  m_max_iterations =_maxIterations;
}

void 
  GaussSeidel::
  setTolerance(const double _tol)
{
  m_tolerance =_tol;
}

void 
  GaussSeidel::
  setDefaults()
{
  m_max_iterations = 10;
  m_tolerance = 1e-7;
  m_verbose_level = 0;
  m_gs_info = 0;
}
