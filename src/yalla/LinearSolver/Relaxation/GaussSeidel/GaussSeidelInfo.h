#pragma once
#ifndef yalla_gauss_seidel_info_h
#define yalla_gauss_seidel_info_h

/*!
 *  \file GaussSeidelInfo.h
 *  \brief Structure to store GaussSeidel information
 *  \date 12/26/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <vector>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Profiler/Profiler.h"
#include "yalla/Utils/MemoryCheck/MemoryCheck.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
//! \brief Structure to store GaussSeidel execution information
struct GaussSeidelInfo
{
	//! \name Public Typedefs
	//@{
	//! \brief Type of this class
	typedef GaussSeidelInfo this_type;
	//@}
	//! \brief Vector to store the convergence rate
	std::vector<double> m_convergence_rate;
	//! \brief Vector to store the L2 residual norm
	std::vector<double> m_residual_norm;
	//! \brief Vector to store the L2 scaled residual norm
	std::vector<double> m_scaled_residual_norm;
	//! \brief Memory consumption of the GaussSeidel solver
	MemoryCheck * m_memory_used;
	//! \brief Profiler for the different steps of the GaussSeidel solver
	Profiler * m_profiler;

	/*! \name Public Constructors
	 *
	 *  Public constructors of the class GaussSeidelInfo
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  If the verbosity level is greater than 1, allocates the memory and profiling logger
	 *
	 *  \param[in] _verboseLvl Verbosity level
	 */
	explicit GaussSeidelInfo(const int _verboseLvl)
	{
		m_profiler = 0;
		m_memory_used = 0;
		if(_verboseLvl>1)
		{
			m_profiler = new Profiler();
			m_memory_used = new MemoryCheck();
		}
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class GaussSeidelInfo
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the memory and profiling pointers
	 */
	virtual ~GaussSeidelInfo()
	{
		if(m_profiler)
		{
			delete m_profiler;
			m_profiler = 0;
			delete m_memory_used;
			m_memory_used = 0;
		}
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class GaussSeidelInfo. \n
	 *  These methods give access in read-only mode to the datas/information of the class GaussSeidelInfo
	 */
	//@{

	/*! \brief  Return the class name. 
	 *
	 *  \return "GaussSeidelInfo" The name of the class
	 */
	const char* getClassName() const
	{
		return "GaussSeidelInfo";
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
