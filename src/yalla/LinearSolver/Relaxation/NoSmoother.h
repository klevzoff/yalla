#pragma once
#ifndef yalla_no_smoother_h
#define yalla_no_smoother_h

/*!
 *  \file NoSmoother.h
 *  \brief No smoother "implementation" for the AMG solver
 *  \date 12/26/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class NoSmoother
 *  \brief No smoother implementation
 * 
 *  \details
 *  This class is an empty shell when one wants to use an AMG solver without a smoother. \n
 *  It implements the "required" methods, but they actually do nothing.
 */
class NoSmoother
{
 public:
	//! \name Public Typedefs
	//@{
	typedef NoSmoother this_type;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class NoSmoother
	 */
	//@{
		
	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Do nothing
	 */
	explicit NoSmoother()
	{
		;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class NoSmoother
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Do nothing
	 */
	virtual ~NoSmoother()
	{
		;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class NoSmoother. \n
	 *  These methods give access in read-only mode to the datas/information of the class NoSmoother
	 */
	//@{
	/*! \brief Return the class name. 
	 *
	 *  \return "NoSmoother" The name of the class
	 */
	const char* getClassName() const
	{
		return "NoSmoother";
	}
	//@}

 /*! \name Public Setter
	*
	*  Public Setter of the class NoSmoother. \n
	*  These methods give access in read/write mode to the datas/information of the class NoSmoother
	*/
 //@{
 /*! \brief Set the max number of iterations
	*
	*  \param[in] _maxIterations The max number of iterations
	*/
	void setMaxIterations(const int _maxIterations)
	{
		;
	}
	//@}

	/*! \name Public setup
	 *
	 *  Setup method of the NoSmoother class. \n
	 */
	//@{
	/*! \brief Setup method of the NoSmoother class
	 *
	 *  Do nothing
	 */
	void setup()
	{
		;
	}
	//@}

	/*! \name Public Solve
	 *
	 *  Solve method of the NoSmoother. \n
	 */
	//@{
	/*! \brief Solve method of the NoSmoother class
	 *
	 *  Do nothing
	 *
	 *  \tparam Matrix The type of the matrix
	 *  \tparam Vector The type of the vector
	 *
	 *  \param[in] _A The matrix
	 *  \param[in] _rhs The rhs
	 *  \param[out] _lhs The lhs
	 */
	template<typename Matrix, typename Vector>
    int solve(const Matrix& _A, const Vector& _rhs, Vector& _lhs)
	{
		return 0;
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
