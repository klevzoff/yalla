#pragma once
#ifndef yalla_dist_vector_h
#define yalla_dist_vector_h

/*!
 *  \file DistVector.h
 *  \brief Distributed vector
 *  \date 12/20/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Exceptions/CheckBounds.h"
#include "yalla/Utils/Exceptions/PointerNotAllocated.h"
#include "yalla/Utils/Exceptions/FailedAllocation.h"
#include "yalla/Utils/Exceptions/VectorBidimensionnalNotAllowed.h"
#include "yalla/Utils/ParUtils/SPMVSynchronization.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class DistVector
 *  \brief Distributed vector
 *
 *  \details
 *  The class stores the two local dimensions of the vector in the attributes m_nb_rows and m_nb_cols. For now, only one dimension is used, the idea is to be able to extend this class to a multi-vector class, to solve a linear system with several RHS. \n
 *  Datas are stored in m_data, a unidimensionnal array. \n
 *
 *  For the distributed elements, the class stores four additionnal attributes:
 *  - m_global_nb_rows: The global number of rows in the matrix
 *  - m_global_nb_cols: The global number of cols in the matrix
 *  - m_mapping: The mapping between the local and global ID of rows/cols
 *  - m_own_size: The size of the local elements of the vector
 *
 *  The m_own_size first elements are local elements (for a SPMV). The remaining elements are ghosts elements (global for a SPMV) that need to be updated through the m_synchronizer to complete the SPMV.
 *
 *  The class can throw four types of exception, only in debug mode:
 *  - isOutOfRange exception. If someones try to access an element at index (row,col), such that m_nb_rows < row < 0 or m_nb_cols < col < 0. This exception can be thrown in almost all setter/getter functions.
 *  - AllocationFailed exception. If an allocation failed, the function throws this exception. This exception can be thrown in all functions that allocates memory
 *  - PointerNotAllocated exception. If the user tries to access a non allocated pointer. This exception can be thrown in almost all setter/getter functions
 *  - VectorBidimensionnalNotAllowed. If a vector a both dimensions superior to one (as multivector is not supported yet)
 *
 *  \tparam DataType The data type of the matrix
 *
 *  \warning No alias test is implemented right now.
 */
template<typename DataType>
class DistVector
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the vector
	typedef DataType data_type;
	//! \brief Type of this class
	typedef DistVector<data_type> this_type;
	//@}
 private:
	//! \brief Datas of the vector
	data_type* m_data;
	//! \brief Total number of rows (local + ghosts)
	int m_nb_rows;
	//! \brief Total number of columns (local + ghosts)
	int m_nb_cols;
	//! \brief Number of local elements
	int m_own_size;
	//! \brief Global number of rows
	int m_global_nb_rows;
	//! \brief Global number of cols
	int m_global_nb_cols;
	//! \brief Mapping between the local and global ID of rows/cols
	int* m_mapping;
	//! \brief Symchronizer to update ghosts elements (elements between m_own_size and m_nb_rows or m_nb_cols)
	const SPMVSynchronization<data_type>* m_synchronizer;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DistVector
	 */
	//@{

	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Set all attributes to zero
	 */
	explicit DistVector()
	{
    m_nb_rows = 0;
    m_nb_cols = 0;
    m_data = 0;
    m_global_nb_rows = 0;
    m_global_nb_cols = 0;
    m_mapping = 0;
    m_own_size = 0;
    m_synchronizer = 0;
  }

	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Call the method allocate and fill to copy the vector structure and the datas. \n
	 *
	 *  \exception AllocationFailed The allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _vector Vector to copy
	 */
	DistVector(const DistVector& _vector)
	{
    this->allocate(_vector);
    this->fill(_vector);
  }

	/*!
	 *  \brief Dimension and mapping constructor
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _ownSize, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_own_size, m_global_nb_rows and m_global_nb_cols. \n
	 *  Call the method allocateDatas and allocateMapping to allocate the different pointers of the data structure. \n
	 *  Fill the data pointer with zeros. \n
	 *  Copy the values of_mapping in m_mapping. \n
	 *  Copy _sync in m_synchronizer. 
	 *
	 *  \exception AllocationFailed The allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _rows Number of rows in the vector
	 *  \param[in] _cols Number of cols in the vector
	 *  \param[in] _ownSize Number of local elements in the vector
	 *  \param[in] _globalRows Number of global rows in the vector
	 *  \param[in] _globalCols Number of global cols in the vector
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 *  \param[in] _sync SPMV synchronizer
	 */
	explicit DistVector(const int _rows, const int _cols, const int _ownSize, const int _globalRows, const int _globalCols, const int* _mapping, const SPMVSynchronization<data_type>* _sync)
	{
		m_nb_rows = _rows;
		m_nb_cols = _cols;
    m_own_size = _ownSize;
    m_global_nb_rows = _globalRows;
    m_global_nb_cols = _globalCols;
    this->allocateDatas(_rows,_cols);
    this->fill(data_type());
		this->allocateMapping(this->getSize());
    for(int i=0;i<this->getSize();++i)
      m_mapping[i] = _mapping[i];
    m_synchronizer = _sync;
  }

	/*!
	 *  \brief Dimension and mapping constructor with datas
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _ownSize, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_own_size, m_global_nb_rows and m_global_nb_cols. \n
	 *  Call the method allocateDatas and allocateMapping to allocate the different pointers of the data structure. \n
	 *  Copy the values of _dataPtr in m_data. \n
	 *  Copy the values of_mapping in m_mapping. \n
	 *  Copy _sync in m_synchronizer. 
	 *
	 *  \exception AllocationFailed The allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _rows Number of rows in the vector
	 *  \param[in] _cols Number of cols in the vector
	 *  \param[in] _ownSize Number of local elements in the vector
	 *  \param[in] _globalRows Number of global rows in the vector
	 *  \param[in] _globalCols Number of global cols in the vector
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 *  \param[in] _dataPtr Data pointer
	 *  \param[in] _sync SPMV synchronizer
	 */
	explicit DistVector(const int _rows, const int _cols, const int _ownSize, const int _globalRows, const int _globalCols, const int* _mapping, const data_type* _dataPtr, const SPMVSynchronization<data_type>* _sync)
	{
		m_nb_rows = _rows;
		m_nb_cols = _cols;
    m_own_size = _ownSize;
    m_global_nb_rows = _globalRows;
    m_global_nb_cols = _globalCols;
    this->allocateDatas(_rows,_cols);
    this->fill(_dataPtr);
		this->allocateMapping(this->getSize());
    for(int i=0;i<this->getSize();++i)
      m_mapping[i] = _mapping[i];
    m_synchronizer = _sync;
  }
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DistVector
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class
	 */
	virtual ~DistVector()
	{
		delete[] m_data;
		m_data = 0;
		delete[] m_mapping;
		m_mapping = 0;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class DistVector. \n
	 *  These methods give access in read-only mode to the datas/information of the class DistVector
	 */

	//@{
	/*! 
	 *  \brief Return the number of rows in the vector
	 *
	 *  \details
	 *
	 *  \return The number of rows in the vector
	 */
	int getNbRows() const
	{
		return m_nb_rows;
	}

	/*! 
	 *  \brief Return the number of global rows in the vector
	 *
	 *  \details
	 *
	 *  \return The number of global rows in the vector
	 */
	int getGlobalNbRows() const
	{
		return m_global_nb_rows;
	}

	/*! 
	 *  \brief Return the number of cols in the vector
	 *
	 *  \details
	 *
	 *  \return The number of cols in the vector
	 */
	int getNbCols() const
	{
		return m_nb_cols;
	}

	/*! 
	 *  \brief Return the number of global cols in the vector
	 *
	 *  \details
	 *
	 *  \return The number of global cols in the vector
	 */
	int getGlobalNbCols() const
	{
		return m_global_nb_cols;
	}

	/*! 
	 *  \brief Return the size of the vector
	 *
	 *  \details
	 *
	 *  \return The size of the vector
	 */
	int getSize() const
	{
		return (m_nb_rows > m_nb_cols) ? m_nb_rows : m_nb_cols;
	}

	/*! 
	 *  \brief Return the global size of the vector
	 *
	 *  \details
	 *
	 *  \return The global size of the vector
	 */
	int getGlobalSize() const
	{
		return (m_global_nb_rows > m_global_nb_cols) ? m_global_nb_rows : m_global_nb_cols;
	}

	/*! 
	 *  \brief Return the number of owned elements in the vector
	 *
	 *  \details
	 *
	 *  \return The number of owned elements in the vector
	 */
	int getOwnSize() const
	{
		return m_own_size;
	}

	/*! 
	 *  \brief Return the global ID of a row/column
	 *
	 *  \details
	 *
	 *  \param[in] _index The local ID of the row/col
	 *
	 *  \return The global ID of the local row/col
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	int getMapping(const int _index) const
	{
#ifdef _DEBUG
		this->checkBounds(_index,0,this->getSize());
		this->checkAllocated(m_mapping);
#endif
		return m_mapping[_index];
	}

	/*! 
	 *  \brief Return the global mapping.
	 *
	 *  \details
	 *
	 *  \return The local/global ID mapping array
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 */
	const int* getMapping() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_mapping);
#endif
		return m_mapping;
	}

	/*! 
	 *  \brief Return a pointeur on the datas of the vector.
	 *
	 *  \details
	 *
	 *  \return The data pointer of the vector
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 */
	const data_type* getPointeur() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		return m_data;
	}

	/*! 
	 *  \brief Return the SPMV synchronizer
	 *
	 *  \details
	 *
	 *  \return The SPMV synchronizer of the vector
	 *
	 *  \exception PtrNotAllocated m_sync is not allocated
	 */
	const SPMVSynchronization<data_type>* getSync() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_synchronizer);
#endif
		return m_synchronizer;
	}

	/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type operator()(const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
		this->checkBounds(_index,0,this->getSize());
#endif
		return m_data[_index];
	}

	/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type operator[](const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
		this->checkBounds(_index,0,this->getSize());
#endif
		return m_data[_index];
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DistVector" The name of the class
	 */
	const char* getClassName() const
	{
		return "DistVector";
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class DistVector. \n
	 *  These methods give access in read/write mode to the datas/information of the class DistVector
	 */
	//@{

	/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type& operator()(const int _index)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
		this->checkBounds(_index,0,this->getSize());
#endif
		return m_data[_index];
	}

	/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type&  operator[](const int _index)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
		this->checkBounds(_index,0,this->getSize());
#endif
		return m_data[_index];
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class DistVector. \n
	 *  These methods provide various utilities to the class DistVector
	 */
	//@{

	/*!
	 *  \brief Fill the vector with a value.
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _val The value to copy in the matrix
	 */
	void fill(const data_type _val)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		for(int i=0;i<this->getSize();++i)
			m_data[i] = _val;
	}

	/*!
	 *  \brief Fill a vector by copying the pointer passed in parameter.
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _src The datas to copy
	 */
	void fill(const data_type* _src)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		for(int i=0;i<this->getSize();++i)
			m_data[i] = _src[i];
	}

	/*!
	 *  \brief Display a distributed vector on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _vec The matrix to print
	 */
	friend std::ostream& operator<<(std::ostream& _os, const DistVector& _vec)
	{
		_os << _vec.getClassName() << " - ";
		_os << "Size: " << _vec.getSize() << " - Global Size: " << _vec.getGlobalSize() << "\n";
		if(_vec.getPointeur()==0)
			return _os;
		_os << std::setw(4) << "CPU" << " - " << std::setw(4) << "GID" << " - " << std::setw(4) << "LID" << " - " << std::setw(4) << "VAL \n";
		const int size = _vec.getOwnSize();
		for(int i=0;i<size;++i)
		{
			_os << std::right << std::setw(4) << ParUtils::getProcRank() << " - " << std::setw(4) << _vec.getMapping(i) << " - " << std::setw(4) << i << " : " << std::setw(4) << _vec(i) << "\n";
		}
		return _os;
	}

	/*!
	 *  \brief Synchronize a vector
	 *
	 *  \details
	 *  Call the method synchronize of the m_synchronizer. \n
	 *  Synchronize the vector by sending shared elements and receiving ghost ones.
	 */
	void synchronize() const
	{
		m_synchronizer->synchronize(&m_data[0],m_own_size);
	}
	// @}

	/*! \name Public Assignement Operators
	 *
	 *  Public Assignement Operators of the class DistVector. \n
	 *  These methods provide various utilities to the class DistVector
	 */
	//@{
   
	/*!
	 *  \brief Copy a vector through the assignment operator
	 *
	 *  \details
	 *  Call the method allocate and fill to copy the data structure and fill it. \n
	 *
	 *  \exception AllocationFailed The allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _vector The vector to copy
	 */
	void operator=(const DistVector & _vector)
		{
			delete[] m_data;
			delete[] m_mapping;
			this->allocate(_vector);
			this->fill(_vector);
		}
	//@}
 private:
	/*! \name Private Utils
	 *
	 *  Private Utils of the class DistVector. \n
	 *  These internal methods provides various utilities to the class DistVector
	 */
	//@{

	/*!
	 *  \brief Fill an allocated vector with another vector
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _src The vector to copy
	 */
	void fill(const DistVector& _src)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		const int nbElements = _src.getSize();
		for(int i=0;i<nbElements;++i)
			m_data[i] = _src(i);
	}
	//@}

	/*! \name Private Allocator
	 *
	 *  Private Allocator of the class DistVector. \n
	 *  These internal methods allow to allocate a DistVector in different cases
	 */
	//@{

	/*!
	 *  \brief Allocate the data pointer
	 *
	 *  \details
	 *
	 *  \param[in] _rows Number of rows in the vector
	 *  \param[in] _cols Number of cols in the vector
	 *
	 *  \exception AllocationFailed Allocation of m_data failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 */
	void allocateDatas(const int _rows, const int _cols)
	{
#ifdef _DEBUG
		this->checkBidimVector(_rows,_cols);
#endif
		m_data = new data_type[this->getSize()];
#ifdef _DEBUG
		this->checkAllocation(m_data);
#endif
	}

	/*!
	 *  \brief Allocate the mapping pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the mapping pointer (should be this->getSize)
	 *
	 *  \exception AllocationFailed Allocation of m_mapping failed
	 */
	void allocateMapping(const int _size)
	{
		if(_size==0)
			m_mapping = 0;
		else
			m_mapping = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocation(m_mapping);
#endif
	}

	/*!
	 *  \brief Allocate a vector from another vector
	 *
	 *  \details
	 *
	 *  \param[in] _vector The vector to copy
	 *
	 *  \exception AllocationFailed Allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 */
	void allocate(const DistVector& _vector)
	{
		m_nb_rows = _vector.getNbRows();
		m_nb_cols = _vector.getNbCols();
		m_own_size = _vector.getOwnSize();
		m_global_nb_rows = _vector.getGlobalNbRows();
		m_global_nb_cols = _vector.getGlobalNbCols();
		this->allocateDatas(m_nb_rows,m_nb_cols);
		this->fill(data_type());
		this->allocateMapping(this->getSize());
		for(int i=0;i<this->getSize();++i)
			m_mapping[i] = _vector.getMapping(i);
		m_synchronizer = _vector.getSync();
	}
	//@}

	/*! \name Private Checkeur
	 *
	 *  Private Checkeur of the class DistVector. \n
	 *  These internal methods provides various check utilities for the class DistVector
	 */
	//@{

	/*!
	 *  \brief Check if an allocation was successful
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception AllocationFailed _datas was not properly allocated
	 */
	void checkAllocation(const void* _datas) const
	{
		try
		{
			if(FailedAllocation::Check(_datas))
				throw AllocationFailed(__FILE__,__LINE__);
		}
		catch ( AllocationFailed &ptrFailed )
		{
			YALLA_ERR(ptrFailed.what());
		} 
	}

	/*!
	 *  \brief Check if a pointer was allocated
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception PtrNotAllocated _datas is not allocated
	 */
	void checkAllocated(const void* _datas) const
	{
		try
		{
			if(PointerNotAllocated::Check(_datas))
				throw AllocationFailed(__FILE__,__LINE__);
		}
		catch ( PtrNotAllocated &ptrFailed )
		{
			YALLA_ERR(ptrFailed.what());
		}
	}

	/*!
	 *  \brief Check if both dimensions of the vector are superior to one
	 *
	 *  \details
	 *
	 *  \param[in] _rows The number of rows
	 *  \param[in] _cols The number of cols
	 *
	 *  \exception VectorBidimensionnalNotAllowed Both dimensions of the vector are superior to one
	 */
	void checkBidimVector(const int _rows, const int _cols) const
	{
		try
		{
			if(isVectorBidimensionnal::Check(_rows,_cols))
				throw VectorBidimensionnalNotAllowed(__FILE__,__LINE__);
		}
		catch (VectorBidimensionnalNotAllowed &bidimensionnalVector)
		{
			YALLA_ERR(bidimensionnalVector.what());
		}
	}

	/*!
	 *  \brief Check if an index is between specified bounds
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to  check
	 *  \param[in] _lowerBound The lower bound
	 *  \param[in] _upperBound The upper bound
	 *
	 *  \exception isOutOfRange _index is not between lower/upper bound
	 */
	void checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
	{
		try
		{
			if(CheckBounds::Check(_index,_lowerBound,_upperBound))
				throw isOutOfRange(__FILE__,__LINE__);
		}
		catch ( isOutOfRange &isOut )
		{
			YALLA_ERR(isOut.what());
		} 
	}
	//@}
};

YALLA_END_NAMESPACE
#endif
