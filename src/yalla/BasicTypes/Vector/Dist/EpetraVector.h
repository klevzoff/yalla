#pragma once
#ifndef yalla_epetra_vector_h
#define yalla_epetra_vector_h

/*!
 *  \file EpetraVector.h
 *  \brief Wrapper around the Trilinos Epetra vector
 *  \date 12/20/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/TypesAndDef.h"
#ifdef WITH_TRILINOS
#include "Epetra_Vector.h"
#include "Epetra_BlockMap.h"
#else
class Epetra_BlockMap
{

};
#endif

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)

/*!
 *  \class EpetraVector
 *  \brief Wrapper around a Trilinos Epetra vector
 *
 *  \details
 *  This class is a first draft of a wrapper around the Trilinos Epetra vector. \n
 *  It implements some of the necessary methods to be used in the code. \n
 *
 *  On the class itself, not much needs to be said. The Trilinos vector is stored in the template attribute m_vector and the m_own_vector flag is used to define the ownership of the matrix. \n
 *  All the methods are then a wrapper around the Trilinos API to provide the information required. \n
 *  Not all necessary methods are implemented.
 *
 *  \tparam VectorType The type of the vector
 */
template<typename VectorType>
class EpetraVector
{
 public:
//! \name Public Typedefs
	//@{
	//! \brief Data type of the vector
	typedef double data_type;
	//! \brief Type of the vector
	typedef VectorType impl_type;
	//! \brief Type of this class
	typedef EpetraVector<impl_type> this_type;
	//! \brief Type of the graph
	typedef Epetra_BlockMap graph_type;
	//@}
 private:
//! \brief Pointer on the vector implementation
	impl_type* m_vector;
	//! \brief Ownership flag
	bool m_own_vector;
 public:
/*! \name Public Constructors
	 *
	 *  Public constructors of the class EpetraVector
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Set all attributes to zero/false.
	 */
	explicit EpetraVector()
	{
		m_vector = 0;
		m_own_vector = true;
	}

	/*!
	 *  \brief Epetra constructor
	 *
	 *  \details
	 *  Copy the address of the implementation and set the ownership flag to false. \n
	 *  The vector has to be built inside Trilinos. Therefore the matrix is not deep copied. \n
	 *
	 *  \param[in] _vector Vector to copy
	 */
	explicit EpetraVector(impl_type* _vector)
	{
		m_vector = _vector;
		m_own_vector = false;
	}

	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Deep copy of the implementation and set the ownership flag to true. \n
	 *  Call the copy constructor of the relevant Trilinos vector class.
	 *
	 *  \param[in] _vector Vector to copy
	 */
	explicit EpetraVector(const EpetraVector& _vector)
	{
		m_vector = new impl_type(*_vector.getVector());
		m_own_vector = true;
	}

/*!
	 *  \brief Graph constructor
	 *
	 *  \details
	 *  Call the graph constructor of the relevant Trilinos vector class and set the ownership flag to true.
	 *  
	 *  \param[in] _graph The Vector graph
	 */
	explicit EpetraVector(const graph_type& _graph)
	{
		m_vector = new impl_type(_graph);
		m_own_vector = true;
	}
//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class EpetraVector
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class
	 */
	virtual ~EpetraVector()
	{
		if(m_own_vector)
		{
			delete m_vector;
			m_vector = 0;
		}
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class EpetraVector. \n
	 *  These methods give access in read-only mode to the datas/information of the class EpetraVector
	 */

	//@{
	/*! 
	 *  \brief Return the number of local rows in the vector.
	 *
	 *  \details
	 *
	 *  \return The local number of rows in the vector
	 */
	unsigned int getNbRows() const
	{
		return m_vector->MyLength();
	}

/*! 
	 *  \brief Return the number of global rows in the vector.
	 *
	 *  \details
	 *
	 *  \return The number of global rows in the vector
	 */
	unsigned int getGlobalNbRows() const
	{
		return m_vector->GlobalLength();
	}

/*! 
	 *  \brief Return the number of local cols in the vector.
	 *
	 *  \details
	 *
	 *  \return 1 
	 */
	unsigned int getNbCols() const
	{
		return 1;
	}

/*! 
	 *  \brief Return the number of global cols in the vector.
	 *
	 *  \details
	 *
	 *  \return 1
	 */
	unsigned int getGlobalNbCols() const
	{
		return 1;
	}

	/*! 
	 *  \brief Return the size of the vector
	 *
	 *  \details
	 *
	 *  \return The size of the vector
	 */
	unsigned int getSize() const
	{
		return m_vector->MyLength();
	}

	/*! 
	 *  \brief Return the global ID of a row/col
	 *
	 *  \details
	 *
	 *  \param[in] _index The local ID of the row/col
	 *
	 *  \return The global ID of the local row/col
	 */
	unsigned int getMapping(const unsigned int _index) const
	{
		return (m_vector->Map().MyGlobalElements())[_index];
	}

/*! 
	 *  \brief Return the global mapping.
	 *
	 *  \details
	 *
	 *  \return The local/global ID mapping array
	 */
	const unsigned int* getMapping() const
	{
		return reinterpret_cast<unsigned int*>(m_vector->Map().MyGlobalElements());
	}

/*! 
	 *  \brief Return a pointeur on the datas of the vector.
	 *
	 *  \details
	 *
	 *  \return The data pointer of the vector
	 */
	const data_type* getPointeur() const
	{
		return &(*m_vector)[0];
	}

/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 */
	data_type operator()(const unsigned int _index) const
	{
		return (*m_vector)[_index];
	}

/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 */
	data_type operator[](const unsigned int _index) const
	{
		return (*m_vector)[_index];
	}

/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DistCSRMatrix" The name of the class
	 */
	const char* getClassName() const
	{
		return "EpetraVector";
	}

/*! 
	 *  \brief Return a handler on the vector implementation
	 *
	 *  \details
	 *
	 *  \return Return a handler on the vector implementation
	 */
	impl_type* getVector() const
	{
		return m_vector;
	}

//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class EpetraVector. \n
	 *  These methods give access in read/write mode to the datas/information of the class EpetraVector
	 */
	//@{

/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 */
	data_type& operator()(const unsigned int _index)
	{
		return (*m_vector)[_index];
	}

/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 */
	data_type& operator[](const unsigned int _index)
	{
		return (*m_vector)[_index];
	}
//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class EpetraVector. \n
	 *  These methods provide various utilities to the class EpetraVector
	 */
	//@{

	/*!
	 *  \brief Fill the vector with a value.
	 *
	 *  \details
	 *
	 *  \param[in] _val The value to copy in the vector
	 */
	void fill(const data_type _val)
	{
		m_vector->PutScalar(_val);
	}

/*!
	 *  \brief Fill a vector by copying the pointer passed in parameter.
	 *
	 *  \details
	 *
	 *  \param[in] _src The datas to copy
	 */
	void fill(const data_type* _src)
	{
		const int size = m_vector->MyLength();
		for(int i=0;i<size;++i)
			(*m_vector)[i] = _src[i];
	}

/*!
	 *  \brief Display a vector on the standard output
	 */
	void print() const
	{
		std::cout << *m_vector;
		std::cout << std::flush;
	}
//@}
};

YALLA_END_NAMESPACE
#endif
