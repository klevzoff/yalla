#pragma once
#ifndef yalla_seq_vector_h
#define yalla_seq_vector_h

/*!
 *  \file SeqVector.h
 *  \brief Dense sequential vector
 *  \date 24/12/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Exceptions/CheckBounds.h"
#include "yalla/Utils/Exceptions/PointerNotAllocated.h"
#include "yalla/Utils/Exceptions/FailedAllocation.h"
#include "yalla/Utils/Exceptions/VectorBidimensionnalNotAllowed.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SeqVector SeqVector.h
 *  \brief Sequential vector
 *
 *  \details
 *  The class stores the two dimensions of the vector in the attributes m_nb_rows and m_nb_cols. For now, only one dimension is used, the idea is to be able to extend this class to a multi-vector class, to solve a linear system with several RHS. \n
 *  Datas are stored in m_data, a unidimensionnal array. \n
 *
 *  The class can throw four types of exception, only in debug mode:
 *  - isOutOfRange exception. If someones try to access an element at index (row,col), such that m_nb_rows < row < 0 or m_nb_cols < col < 0. This exception can be thrown in almost all setter/getter functions.
 *  - AllocationFailed exception. If an allocation failed, the function throws this exception. This exception can be thrown in all functions that allocates memory
 *  - PointerNotAllocated exception. If the user tries to access a non allocated pointer. This exception can be thrown in almost all setter/getter functions
 *  - VectorBidimensionnalNotAllowed. If a vector a both dimensions superior to one (as multivector is not supported yet)
 *
 *  \tparam DataType The data type of the matrix
 *
 *  \warning No alias test is implemented right now.
 */
template<typename DataType>
class SeqVector
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the vector
	typedef DataType data_type;
	//! \brief Type of this class
	typedef SeqVector<data_type> this_type;
	//@}
 private:
	//! \brief Datas of the vector
	data_type* m_data;
	//! \brief Number of rows
	int m_nb_rows;
	//! \brief Number of columns
	int m_nb_cols;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SeqVector
	 */
	//@{

	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Set all attributes to zero
	 */
	explicit SeqVector()
	{
		m_nb_rows = 0;
		m_nb_cols = 0;
		m_data = 0;
	}

	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Call the method allocate and fill to copy the vector structure and the datas. \n
	 *
	 *  \exception AllocationFailed The allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _vector Vector to copy
	 */
	SeqVector(const SeqVector& _vector)
	{
		this->allocate(_vector);
		this->fill(_vector);
	}

	/*!
	 *  \brief Dimension constructor
	 *
	 *  \details
	 *  Copy the value of _rows and _cols in m_nb_rows and m_nb_cols. \n
	 *  Call the method allocateDatas to allocate the pointer of the data structure. \n
	 *  Fill the data pointer with zeros. \n
	 *
	 *  \exception AllocationFailed The allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _rows Number of rows in the vector
	 *  \param[in] _cols Number of cols in the vector
	 */
	explicit SeqVector(const int _rows, const int _cols)
	{
		this->allocateDatas(_rows,_cols);
		this->fill(data_type());
	}

	/*!
	 *  \brief Dimension and mapping constructor with datas
	 *
	 *  \details
	 *  Copy the value of _rows and _cols in m_nb_rows and m_nb_cols.n
	 *  Call the method allocateDatas to allocate the pointer of the data structure. \n
	 *  Copy the values of _dataPtr in m_data. \n
	 *
	 *  \exception AllocationFailed The allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _rows Number of rows in the vector
	 *  \param[in] _cols Number of cols in the vector
	 *  \param[in] _dataPtr Data pointer
	 */
	explicit SeqVector(const int _rows, const int _cols, const data_type* _dataPtr)
	{
		this->allocateDatas(_rows,_cols);
		this->fill(_dataPtr);
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SeqVector
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the data pointer
	 */
	virtual ~SeqVector()
	{
		delete[] m_data;
		m_data = 0;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SeqVector. \n
	 *  These methods give access in read-only mode to the datas/information of the class SeqVector
	 */

	//@{
	/*! 
	 *  \brief Return the number of rows in the vector
	 *
	 *  \details
	 *
	 *  \return The number of rows in the vector
	 */
	int getNbRows() const
	{
		return m_nb_rows;
	}

	/*! 
	 *  \brief Return the number of cols in the vector
	 *
	 *  \details
	 *
	 *  \return The number of cols in the vector
	 */
	int getNbCols() const
	{
		return m_nb_cols;
	}

	/*! 
	 *  \brief Return the size of the vector
	 *
	 *  \details
	 *
	 *  \return The size of the vector
	 */
	int getSize() const
	{
		return (m_nb_rows > m_nb_cols) ? m_nb_rows : m_nb_cols;
	}

	/*! 
	 *  \brief Return a pointeur on the datas of the vector.
	 *
	 *  \details
	 *
	 *  \return The data pointer of the vector
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 */
	const data_type* getPointeur() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		return m_data;
	}

	/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type  operator()(const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
		this->checkBounds(_index,0,this->getSize());
#endif
		return m_data[_index];
	}

	/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type  operator[](const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
		this->checkBounds(_index,0,this->getSize());
#endif
		return m_data[_index];
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SeqVector" The name of the class
	 */
	const char* getClassName() const
	{
		return "SeqVector";
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class SeqVector. \n
	 *  These methods give access in read/write mode to the datas/information of the class SeqVector
	 */
	//@{
	/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type& operator()(const int _index)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
		this->checkBounds(_index,0,this->getSize());
#endif
		return m_data[_index];
	}

	/*! 
	 *  \brief Return the value in the vector located at the index (_index)
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the vector located at the index (_index)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type&  operator[](const int _index)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
		this->checkBounds(_index,0,this->getSize());
#endif
		return m_data[_index];
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SeqVector. \n
	 *  These methods provide various utilities to the class SeqVector
	 */
	//@{
	/*!
	 *  \brief Fill the vector with a value.
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _val The value to copy in the matrix
	 */
	void fill(const data_type _val)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		for(int i=0;i<this->getSize();++i)
			m_data[i] = _val;
	}

	/*!
	 *  \brief Fill a vector by copying the pointer passed in parameter.
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _src The datas to copy
	 */
	void fill(const data_type* _src)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		for(int i=0;i<this->getSize();++i)
			m_data[i] = _src[i];
	}

	/*!
	 *  \brief Display a distributed vector on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _vec The matrix to print
	 */
	friend std::ostream& operator<<(std::ostream& _os, const SeqVector& _vec)
	{
		_os << _vec.getClassName() << " - ";
		_os << "Size: " << _vec.getSize() << "\n";
		if(_vec.getPointeur()==0)
			return _os;
		_os << std::setw(4) << "GID" << " - " << std::setw(4) << "VAL \n";
		const int size = _vec.getSize();
		for(int i=0;i<size;++i)
		{
			_os << std::right << std::setw(4) << i << " : " << std::setw(4) << _vec(i) << "\n";
		}
		return _os;
	}
	// @}

	/*! \name Public Assignement Operators
	 *
	 *  Public Assignement Operators of the class SeqDVector. \n
	 *  These methods provide various utilities to the class SeqDVector
	 */
	//@{
	/*!
	 *  \brief Copy a vector through the assignment operator
	 *
	 *  \details
	 *  Call the method allocate and fill to copy the data structure and fill it. \n
	 *
	 *  \exception AllocationFailed The allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _vector The vector to copy
	 */
	void operator=(const SeqVector& _vector)
		{
			delete[] m_data;
			this->allocate(_vector);
			this->fill(_vector);
		}
	//@}

 private:
	/*! \name Private Utils
	 *
	 *  Private Utils of the class SeqVector. \n
	 *  These internal methods provides various utilities to the class SeqVector
	 */
	//@{
	/*!
	 *  \brief Fill an allocated vector with another vector
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _src The vector to copy
	 */
	void fill(const SeqVector& _src)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		const int nbElements = _src.getSize();
		for(int i=0;i<nbElements;++i)
			m_data[i] = _src(i);
	}
	//@}

	/*! \name Private Allocator
	 *
	 *  Private Allocator of the class SeqVector. \n
	 *  These internal methods allow to allocate a SeqVector in different cases
	 */
	//@{
	/*!
	 *  \brief Allocate the data pointer
	 *
	 *  \details
	 *
	 *  \param[in] _rows Number of rows in the vector
	 *  \param[in] _cols Number of cols in the vector
	 *
	 *  \exception AllocationFailed Allocation of m_data failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 */
	void allocateDatas(const int _rows, const int _cols)
	{
    m_nb_rows = _rows;
    m_nb_cols = _cols;
#ifdef _DEBUG
    this->checkBidimVector(m_nb_rows,m_nb_cols);
#endif
    const int nbElements = this->getSize();
    m_data = new DataType[nbElements];
#ifdef _DEBUG
    this->checkAllocation(m_data);
#endif
  }
  
	/*!
	 *  \brief Allocate a vector from another vector
	 *
	 *  \details
	 *
	 *  \param[in] _vector The vector to copy
	 *
	 *  \exception AllocationFailed Allocation of m_data or m_mapping failed
	 *  \exception VectorBidimensionnalNotAllowedBoth dimensions of the vector are superior to one
	 */
	void allocate(const SeqVector& _vector)
	{
		m_nb_rows = _vector.getNbRows();
		m_nb_cols = _vector.getNbCols();
		this->allocateDatas(m_nb_rows,m_nb_cols);
		this->fill(data_type());
	}
	//@}

	/*! \name Private Checkeur
	 *
	 *  Private Checkeur of the class SeqVector. \n
	 *  These internal methods provides various check utilities for the class SeqVector
	 */
	//@{
	/*!
	 *  \brief Check if an allocation was successful
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception AllocationFailed _datas was not properly allocated
	 */
	void checkAllocation(const void* _datas) const
	{
		try
		{
			if(FailedAllocation::Check(_datas))
				throw AllocationFailed(__FILE__,__LINE__);
		}
		catch ( AllocationFailed &ptrFailed )
		{
			YALLA_ERR(ptrFailed.what());
		} 
	}

	/*!
	 *  \brief Check if a pointer was allocated
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception PtrNotAllocated _datas is not allocated
	 */
	void checkAllocated(const void* _datas) const
	{
		try
		{
			if(PointerNotAllocated::Check(_datas))
				throw AllocationFailed(__FILE__,__LINE__);
		}
		catch ( PtrNotAllocated &ptrFailed )
		{
			YALLA_ERR(ptrFailed.what());
		}
	}

	/*!
	 *  \brief Check if both dimensions of the vector are superior to one
	 *
	 *  \details
	 *
	 *  \param[in] _rows The number of rows
	 *  \param[in] _cols The number of cols
	 *
	 *  \exception VectorBidimensionnalNotAllowed Both dimensions of the vector are superior to one
	 */
	void checkBidimVector(const int _rows, const int _cols) const
	{
		try
		{
			if(isVectorBidimensionnal::Check(_rows,_cols))
				throw VectorBidimensionnalNotAllowed(__FILE__,__LINE__);
		}
		catch (VectorBidimensionnalNotAllowed &bidimensionnalVector)
		{
			YALLA_ERR(bidimensionnalVector.what());
		}
	}

	/*!
	 *  \brief Check if an index is between specified bounds
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to  check
	 *  \param[in] _lowerBound The lower bound
	 *  \param[in] _upperBound The upper bound
	 *
	 *  \exception isOutOfRange _index is not between lower/upper bound
	 */
	void checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
	{
		try
		{
			if(CheckBounds::Check(_index,_lowerBound,_upperBound))
				throw isOutOfRange(__FILE__,__LINE__);
		}
		catch ( isOutOfRange &isOut )
		{
			YALLA_ERR(isOut.what());
		} 
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
