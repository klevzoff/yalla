#pragma once
#ifndef yalla_seq_sparse_matrix_h
#define yalla_seq_sparse_matrix_h

/*!
 *  \file SeqSparseMatrix.h
 *  \brief Sequential sparse matrix encapsulating the different policies
 *  \date 12/16/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include <iomanip>
#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SeqSparseMatrix
 *  \brief Sequential sparse matrix encapsulating the different policies
 * 
 *  \details 
 *  This class provides an interface to build and use different types of sparse matrices following a policy based system.\n
 *  The different templates parameters allow to specify: \n
 *  - \tparam DataType: The data type of the matrix \n
 *  - \tparam MatrixImpl: The actual implementation of the matrix \n
 *
 *  \warning No alias test is implemented right now.
 */
template <typename DataType, typename MatrixImpl>
	class SeqSparseMatrix 
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the matrix
	typedef DataType data_type;
	//! \brief Type of the implementation of the matrix
	typedef MatrixImpl impl_type;
	//! \brief Type of this class
	typedef SeqSparseMatrix<data_type,impl_type> this_type;
	//@}
 private:
	//! \name Private class attributes
	//@{
	//! \brief Pointer on the matrix implementation
	impl_type* m_matrix_impl;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SeqSparseMatrix
	 */
	//@{

	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Calls the default constructor of the chosen matrix implementation
	 */
	explicit SeqSparseMatrix()
	{
		m_matrix_impl = new impl_type();
	}

	/*! 
	 *  \brief Copy constructor.
	 *
	 *  \details
	 *  Call the graph constructor of the chosen matrix implementation. \n
	 *  Call the fill method of the chosen implementation. \n
	 *
	 *  \param[in] _matrix Matrix to copy
	 */
	SeqSparseMatrix(const SeqSparseMatrix& _matrix)
	{
		m_matrix_impl = new impl_type(_matrix.getMatrixGraph());
    m_matrix_impl->fill(_matrix);
  }

	/*!
	 *  \brief Dimension constructor
	 *
	 *  \details
	 *  Call the relevant constructor of the chosen matrix implementation. \n
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 */
	explicit SeqSparseMatrix(const int _rows, const int _cols)
	{
    m_matrix_impl = new impl_type(_rows, _cols);
  }

	/*!
	 *  \brief Dimension constructor with number of non zero elements
	 *
	 *  \details
	 *  Call the relevant constructor of the chosen matrix implementation. \n
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 */
	explicit SeqSparseMatrix(const int _rows, const int _cols, const int _nbNnz)
	{
    m_matrix_impl = new impl_type(_rows, _cols,_nbNnz);
  }

	/*! 
	 *  \brief Dimension constructor with data structure.
	 *
	 *  \details
	 *  Call the relevant constructor of the chosen matrix implementation. \n
	 *
	 *  \param[in] _rows Number of rows in the matrix
	 *  \param[in] _cols  Number of cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Row pointer in the desired format
	 *  \param[in] _colPtr Col pointer in the desired format
	 */
	explicit SeqSparseMatrix(const int _rows, const int _cols, const int _nbNnz, const int* _rowPtr, const int* _colPtr)
	{
    m_matrix_impl = new impl_type(_rows,_cols,_nbNnz,_rowPtr,_colPtr);
  }

	/*! 
	 *  \brief Dimension constructor with data structure.
	 *
	 *  \details
	 *  Call the relevant constructor of the chosen matrix implementation. \n
	 *
	 *  \param[in] _rows Number of rows in the matrix
	 *  \param[in] _cols  Number of cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Pointer describing the row structure of the matrix
	 *  \param[in] _colPtr Pointer describing the col structure of the matrix
	 *  \param[in] _dataPtr Datas pointer
	 */
	explicit SeqSparseMatrix(const int _rows, const int _cols, const int _nbNnz, const int* _rowPtr, const int* _colPtr, const data_type* _dataPtr)
	{
    m_matrix_impl = new impl_type(_rows,_cols,_nbNnz,_rowPtr,_colPtr,_dataPtr);
  }

	/*! 
	 *  \brief Graph constructor
	 *
	 *  \details
	 *  Call the graph constructor of the chosen implementation. \n
	 *  This constructor allow to create an empty matrix by copying only the structure of the matrix (no datas copied).
	 *
	 *  \param[in] _matrixGraph The graph of the matrix to copy
	 */
	explicit SeqSparseMatrix(const typename MatrixImpl::graph_type& _matrixGraph)
	{
    m_matrix_impl = new impl_type(_matrixGraph);
  }
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SeqSparseMatrix
	 */
	//@{

	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the implementation pointer
	 */
	virtual ~SeqSparseMatrix()
	{
    delete m_matrix_impl;
    m_matrix_impl = 0;
  }

	//@}




	/*! \name Public Getter
	 *
	 *  Public Getter of the class SeqSparseMatrix. \n
	 *  These methods give access in read-only mode to the datas/information of the class SeqSparseMatrix
	 */

	//@{
	/*! 
	 *  \brief Return the number of local rows in the matrix.
	 *
	 *  \details
	 *  Call the method getNbRows of the chosen implementation. \n
	 *
	 *  \return The local number of rows in the matrix
	 */
	int getNbRows() const
	{
    return m_matrix_impl->getNbRows();
  }

	/*! 
	 *  \brief Return the number of local cols in the matrix.
	 *
	 *  \details
	 *  Call the method getNbCols of the chosen implementation. \n
	 *
	 *  \return The number of local cols in the matrix
	 */
	int getNbCols() const
	{
    return m_matrix_impl->getNbCols();
  }

	/*! 
	 *  \brief Return the number of non zero elements in the matrix.
	 *
	 *  \details
	 *  Call the method getNnz of the chosen implementation. \n
	 *
	 *  \return The number of non zero elements in the matrix
	 */	
	int getNnz() const
	{
    return m_matrix_impl->getNnz();
  }

	/*! 
	 *  \brief Return the number of non zero elements on a row
	 *
	 *  \details
	 *  Call the method getRowNnz of the chosen implementation. \n
	 *
	 *  \param[in] _row The row we want to know the nnz
	 *
	 *  \return The number of non zero elements on the specified row
	 */	
	int getRowNnz(const int _row) const
	{
    return m_matrix_impl->getRowNnz(_row);
  }

	/*! 
	 *  \brief Return a pointeur on the datas of the matrix.
	 *
	 *  \details
	 *  Call the method getPointeur of the chosen implementation. \n
	 *
	 *  \return The data pointer of the matrix
	 */
	const data_type* getPointeur() const
	{
    return m_matrix_impl->getPointeur();
  }

	/*! 
	 *  \brief Return a handler on the matrix implementation
	 *
	 *  \return The matrix implementation
	 */
	const impl_type* getImpl() const
	{
		return m_matrix_impl;
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *  Call the method operator() of the chosen implementation. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 */
	data_type operator()(const int _row, const int _col) const
	{
    return (*const_cast<const MatrixImpl*>(m_matrix_impl))(_row,_col);
  }

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  Call the method operator[] of the chosen implementation. \n
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 */
	data_type operator[](const int _index) const
	{
    return (*const_cast<const MatrixImpl*>(m_matrix_impl))[_index];
  }

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SeqSparseMatrix" The name of the class
	 */
	const char* getClassName() const
	{
    return "SeqSparseMatrix";
  }

	/*!
	 *  \brief Return the graph of the matrix. 
	 *
	 *  \details
	 *  Call the method getMatrixGraph of the chosen implementation.
	 *
	 *  \return The graph of the matrix
	 */
	const typename MatrixImpl::graph_type& getMatrixGraph() const
	{
    return m_matrix_impl->getMatrixGraph();
  }

	/*! 
	 *  \brief Check if the entry exists in the matrix
	 *
	 *  \details
	 *  Return 1 if the entry exists i.e. there is a nnz at this index, 0 otherwise. \n
	 *  Call the method getEntry of the chosen implementation.
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return 1 if an entry exists in the matrix at the index (_row,_col) , 0 otherwise
	 */
	int getEntry(const int _row, const int _col) const
	{
    return m_matrix_impl->getEntry(_row,_col);
  }

	/*! 
	 *  \brief Check if the matrix is initialized
	 *  
	 *  \details 
	 *  Return true if the matrix is initialized, false otherwise. \n
	 *  Call the method isInitialized of the chosen implementation.
	 *
	 *  \return The state of the initialization flag
	 */
	bool isInitialized() const
	{
    return m_matrix_impl->isInitialized();
  }

	/*! 
	 *  \brief Check if the matrix is allocated
	 *  
	 *  \details 
	 *  Return true if the matrix is allocated, false otherwise. \n
	 *  Call the method isAllocated of the chosen implementation.
	 *
	 *  \return The state of the allocation flag
	 */
	bool isAllocated() const
	{
    return m_matrix_impl->isAllocated();
  }
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class SeqSparseMatrix. \n
	 *  These methods give access in read/write mode to the datas/information of the class SeqSparseMatrix
	 */
	//@{

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *  Call the method operator() of the chosen implementation. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 */
	data_type& operator()(const int _row, const int _col)
	{
    return (*m_matrix_impl)(_row,_col);
  }

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  Call the method operator[] of the chosen implementation. \n
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 */
	data_type& operator[](const int _index)
	{
    return (*m_matrix_impl)[_index];
  }

	/*!
	 *  \brief Set the value in the matrix located at the index (_row,_col) to _val
	 *
	 *  \details
	 *  Call the method setValue of the chosen implementation. \n
	 *  In the case of a dense matrix, this method is strictly similar to the operator()
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *  \param[in] _val The value to set at the index (_row,_col)
	 */
	void setValue(const int _row, const int _col, const data_type _val)
	{
    m_matrix_impl->setValue(_row,_col,_val);
  }
	//@}

	/*! \name Public Iterators
	 *
	 *  Public iterators of the class SeqSparseMatrix. \n
	 *  These methods returns differents iterators on the matrix elements
	 */

	/*!
	 *  \brief Return a const iterator on the first row of the matrix
	 *
	 *  \details
	 *  Call the method rowBegin of the chosen implementation.
	 *
	 *  \return The first element on the first row of the matrix
	 */
	typename MatrixImpl::const_row_iterator_type rowBegin() const
	{
    return const_cast<const MatrixImpl*>(m_matrix_impl)->rowBegin();
  }

	/*!
	 *  \brief Return an iterator on the first row of the matrix
	 *
	 *  \details
	 *  Call the method rowBegin of the chosen implementation.
	 *
	 *  \return The first element on the first row of the matrix
	 */
	typename MatrixImpl::row_iterator_type rowBegin()
	{
    return m_matrix_impl->rowBegin();
	}

	/*!
	 *  \brief Return a const iterator on the last row of the matrix
	 *
	 *  \details
	 *  Call the method rowEnd of the chosen implementation.
	 *
	 *  \return The first element on the last row of the matrix
	 */
	typename MatrixImpl::const_row_iterator_type rowEnd() const
	{
    return const_cast<const MatrixImpl*>(m_matrix_impl)->rowEnd();
  }

	/*!
	 *  \brief Return an iterator on the last row of the matrix
	 *
	 *  \details
	 *  Call the method rowEnd of the chosen implementation.
	 *
	 *  \return The first element on the last row of the matrix
	 */
	typename MatrixImpl::row_iterator_type rowEnd()
	{
    return m_matrix_impl->rowEnd();
  }

	/*!
	 *  \brief Return a const iterator on the first col of a row 
	 *
	 *  \details
	 *  Call the method colBegin of the chosen implementation. \n
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The first element on the first row of the matrix
	 */
	typename MatrixImpl::const_col_iterator_type colBegin(const typename MatrixImpl::const_row_iterator_type& _rowIterator) const
	{
    return const_cast<const MatrixImpl*>(m_matrix_impl)->colBegin(_rowIterator);
  }

	/*!
	 *  \brief Return an iterator on the first col of a row 
	 *
	 *  \details
	 *  Call the method colBegin of the chosen implementation. \n
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \return The first element on the first row of the matrix
	 */
	typename MatrixImpl::col_iterator_type colBegin(const typename MatrixImpl::row_iterator_type& _rowIterator)
	{
    return m_matrix_impl->colBegin(_rowIterator);
  }

	/*!
	 *  \brief Return a const iterator on the last col of a row 
	 *
	 *  \details
	 *  Call the method colEnd of the chosen implementation. \n
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the first row of the matrix
	 */
	typename MatrixImpl::const_col_iterator_type colEnd(const typename MatrixImpl::const_row_iterator_type& _rowIterator) const
	{
    return const_cast<const MatrixImpl*>(m_matrix_impl)->colEnd(_rowIterator);
  }

	/*!
	 *  \brief Return an iterator on the last col of a row 
	 *
	 *  \details
	 *  Call the method colEnd of the chosen implementation. \n
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \return The last element on the first row of the matrix
	 */
	typename MatrixImpl::col_iterator_type colEnd(const typename MatrixImpl::row_iterator_type& _rowIterator)
	{
    return m_matrix_impl->colEnd(_rowIterator);
  }

	/*!
	 *  \brief Return a const iterator on the diagonal element of a row
	 *
	 *  \details
	 *  Call the method diagonalElement of the chosen implementation. \n
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the diagonal element
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	typename MatrixImpl::const_col_iterator_type diagonalElement(const typename MatrixImpl::const_row_iterator_type& _rowIterator) const
	{
    return const_cast<const MatrixImpl*>(m_matrix_impl)->diagonalElement(_rowIterator);
  }

	/*!
	 *  \brief Return an iterator on the diagonal element of a row
	 *
	 *  \details
	 *  Call the method diagonalElement of the chosen implementation. \n
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	typename MatrixImpl::col_iterator_type diagonalElement(const typename MatrixImpl::row_iterator_type& _rowIterator)
	{
    return m_matrix_impl->diagonalElement(_rowIterator);
  }
	//@}

	
	/*! \name Public Utils
	 *
	 *  Public Utils of the class SeqSparseMatrix. \n
	 *  These methods provide various utilities to the class SeqSparseMatrix
	 */
	//@{

	/*!
	 *  \brief Initialize a matrix with the parameters given as arguments
	 *
	 *  \details
	 *  Call the method initialize of the chosen implementation. \n
	 *  This is useful if a matrix is first created with the default constructor, then need to be initialized.
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Pointer describing the row structure of the matrix
	 *  \param[in] _colPtr Pointer describing the col structure of the matrix
	 */
	void initialize(const int _rows, const int _cols, const int _nbNnz, const int* _rowPtr, const int* _colPtr)
	{
    m_matrix_impl->initialize(_rows,_cols,_nbNnz,_rowPtr,_colPtr);
  }

	/*!
	 *  \brief Fill the matrix with a value.
	 *
	 *  \details
	 *  Call the method fill of the chosen implementation.
	 *
	 *  \param[in] _val The value to copy in the matrix
	 */
	void fill(const data_type _val)
	{
    m_matrix_impl->fill(_val);
  }

	/*!
	 *  \brief Fill a matrix by copying the pointeur passed in parameter.
	 *
	 *  \details
	 *  Call the method fill of the chosen implementation.
	 *
	 *  \param[in] _val The datas to copy
	 */
	void fill(const data_type* _val)
	{
    m_matrix_impl->fill(_val);
  }

	/*!
	 *  \brief Finalize the matrix
	 *  
	 *  \details
	 *  Call the method finalize of the chosen implementation.
	 */
	void finalize()
	{
    m_matrix_impl->finalize();
  }

	/*!
	 *  \brief Display a sparse matrix on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _mat The matrix to print
	 */
	friend std::ostream& operator<<(std::ostream& _os, const SeqSparseMatrix& _mat)
	{
		typedef typename MatrixImpl::const_row_iterator_type rowIt;
		typedef typename MatrixImpl::const_col_iterator_type colIt;
			
		_os << _mat.getClassName() << "<" << _mat.getImpl()->getClassName() << "> - ";
		_os << "Size: " << _mat.getNbRows() << "x" << _mat.getNbCols() << "\n";
		if(_mat.getPointeur()==0)
			return _os;
			
		_os << std::setw(9) << "GID" << " - " << std::setw(4) << "VAL \n";
		for(rowIt row = _mat.rowBegin(); row!= _mat.rowEnd();++row)
		{
			for(colIt col = _mat.colBegin(row);col!=_mat.colEnd(row);++col)
			{
				_os << "(" << std::setw(3) << row.index() << "," << std::left << std::setw(3) << col.index() << std::right << std::right << ") - " << std::setw(4) << *col << "\n";
			}
		}
		return _os;
	}
	//@}

	/*! \name Public Assignement Operators
	 *
	 *  Public Assignement Operators of the class SeqSparseMatrix. \n
	 */
	//@{

	/*!
	 *  \brief Copy a matrix through the assignment operator
	 *
	 *  \details
	 *  Call the method allocateAndFill of the chosen implementation. \n
	 *
	 *  \param[in] _matrix The matrix to copy
	 */
	void operator=(const SeqSparseMatrix& _matrix)
		{
			m_matrix_impl->allocateAndFill(_matrix);
		}
	//@}
};

YALLA_END_NAMESPACE
#endif
