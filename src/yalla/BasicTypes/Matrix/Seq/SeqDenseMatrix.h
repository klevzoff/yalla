#pragma once
#ifndef yalla_seq_dense_matrix_h
#define yalla_seq_dense_matrix_h

/*!
 *  \file SeqDenseMatrix.h
 *  \brief Sequential dense matrix encapsulating the different policies
 *  \date 12/13/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include <iomanip>
#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SeqDenseMatrix
 *  \brief Sequential dense matrix encapsulating the different policies
 * 
 *  \details 
 *  This class provides an interface to build and use different types of dense matrices following a policy based system.\n
 *  The different templates parameters allow to specify: \n
 *  - \tparam DataType: The data type of the matrix \n
 *  - \tparam MatrixImpl: The actual implementation of the matrix \n
 *
 *  \warning No alias test is implemented right now.
 */
template <typename DataType, typename MatrixImpl>
	class SeqDenseMatrix
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the matrix
	typedef DataType data_type;
	//! \brief Type of the implementation of the matrix
	typedef MatrixImpl impl_type;
	//! \brief Type of this class
	typedef SeqDenseMatrix<data_type,impl_type> this_type;
	//@}
 private:
	//! \name Private class attributes
	//@{
	//! \brief Pointer on the matrix implementation
	impl_type* m_matrix_impl;
	//@}
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SeqDenseMatrix
	 */
	//@{

	/*! 
	 *  \brief Default constructor.
	 *
	 *  \details
	 *  Calls the default constructor of the chosen matrix implementation
	 */
	explicit SeqDenseMatrix()
	{
		m_matrix_impl = new impl_type();
	}

	/*! 
	 *  \brief Copy constructor.
	 *
	 *  \details
	 *  Call the graph constructor of the chosen matrix implementation. \n
	 *  Call the fill method of the chosen implementation. \n
	 *
	 *  \param[in] _matrix Matrix to copy
	 */
	SeqDenseMatrix(const SeqDenseMatrix& _matrix)
	{
		m_matrix_impl = new impl_type(_matrix.getMatrixGraph());
		m_matrix_impl->fill(_matrix);
	}

	/*!
	 *  \brief Dimension constructor
	 *
	 *  \details
	 *  Call the relevant constructor of the chosen matrix implementation. \n
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 */
	explicit SeqDenseMatrix(const int _rows, const int _cols)
	{
		m_matrix_impl = new impl_type(_rows, _cols);
	}

	/*!
	 *  \brief Dimension constructor with number of non zero elements
	 *
	 *  \details
	 *  Call the relevant constructor of the chosen matrix implementation. \n
	 *  In the case of a dense matrix, the nnz elements is useless. This constructor exists only to have a common constructor with sparse matrices.
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 */
	explicit SeqDenseMatrix(const int _rows, const int _cols, const int _nbNnz)
	{
		m_matrix_impl = new impl_type(_rows, _cols,_nbNnz);
	}

	/*!
	 *  \brief Dimension with mapping and data pointer constructor
	 *
	 *  \details
	 *  Call the relevant constructor of the chosen matrix implementation. \n
	 *
	 *  \param[in] _rows Number of rows in the matrix
	 *  \param[in] _cols Number of cols in the matrix
	 *  \param[in] _dataPtr Datas to copy
	 */
	explicit SeqDenseMatrix(const int _rows, const int _cols, const data_type* _dataPtr)
	{
		m_matrix_impl = new impl_type(_rows,_cols,_dataPtr);
	}

	/*! 
	 *  \brief Graph constructor
	 *
	 *  \details
	 *  Call the graph constructor of the chosen implementation. \n
	 *  This constructor allow to create an empty matrix by copying only the structure of the matrix (no datas copied).
	 *
	 *  \param[in] _matrixGraph The graph of the matrix to copy
	 */
	explicit SeqDenseMatrix(const typename impl_type::graph_type& _matrixGraph)
	{
		m_matrix_impl = new impl_type(_matrixGraph);
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SeqDenseMatrix
	 */
	//@{

	/*! 
	 *  \brief Destructor
	 *
	 *  \details
	 *  Free the memory allocated by the class i.e. the implementation pointer
	 */
	virtual ~SeqDenseMatrix()
	{
		delete m_matrix_impl;
		m_matrix_impl = 0;
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SeqDenseMatrix. \n
	 *  These methods give access in read-only mode to the datas/information of the class SeqDenseMatrix
	 */

	//@{
	/*! 
	 *  \brief Return the number of rows in the matrix.
	 *
	 *  \details
	 *  Call the method getNbRows of the chosen implementation. \n
	 *
	 *  \return The number of rows in the matrix
	 */
	int getNbRows() const
	{
		return m_matrix_impl->getNbRows();
	}

	/*! 
	 *  \brief Return the number of cols in the matrix.
	 *
	 *  \details
	 *  Call the method getNbCols of the chosen implementation. \n
	 *
	 *  \return The number of cols in the matrix
	 */
	int getNbCols() const
	{
		return m_matrix_impl->getNbCols();
	}

	/*! 
	 *  \brief Return the number of non zero elements in the matrix.
	 *
	 *  \details
	 *  Call the method getNnz of the chosen implementation. \n
	 *
	 *  \return The number of non zero elements in the matrix
	 */
	int getNnz() const
	{
		return m_matrix_impl->getNnz();
	}

	/*! 
	 *  \brief Return a pointer on the datas of the matrix.
	 *
	 *  \details
	 *  Call the method getPointeur of the chosen implementation. \n
	 *
	 *  \return The data pointer of the matrix
	 */
	const data_type* getPointeur() const
	{
		return m_matrix_impl->getPointeur();
	}

	/*! 
	 *  \brief Return a handler on the matrix implementation
	 *
	 *  \return The matrix implementation
	 */
	const impl_type* getImpl() const
	{
		return m_matrix_impl;
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *  Call the method operator() of the chosen implementation. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 */
	data_type operator()(const int _row, const int _col) const
	{
		return (*const_cast<MatrixImpl*>(m_matrix_impl))(_row,_col);
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  Call the method operator[] of the chosen implementation. \n
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 */
	data_type operator[](const int _index) const
	{
		return (*const_cast<MatrixImpl*>(m_matrix_impl))[_index];
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SeqDenseMatrix" The name of the class
	 */
	const char* getClassName() const
	{
		return "SeqDenseMatrix";
	}

	/*!
	 *  \brief Return the graph of the matrix. 
	 *
	 *  \details
	 *  Call the method getMatrixGraph of the chosen implementation.
	 *
	 *  \return The graph of the matrix
	 */
	const typename MatrixImpl::graph_type & getMatrixGraph() const
	{
		return m_matrix_impl->getMatrixGraph();
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class SeqDenseMatrix. \n
	 *  These methods give access in read/write mode to the datas/information of the class DistDenseMatrix
	 */
	//@{

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *  Call the method operator() of the chosen implementation. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 */
	data_type& operator()(const int _row, const int _col)
	{
		return (*m_matrix_impl)(_row,_col);
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  Call the method operator[] of the chosen implementation. \n
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 */
	data_type& operator[](const int _index)
	{
		return (*m_matrix_impl)[_index];
	}

	/*!
	 *  \brief Set the value in the matrix located at the index (_row,_col) to _val
	 *
	 *  \details
	 *  Call the method setValue of the chosen implementation. \n
	 *  In the case of a dense matrix, this method is strictly similar to the operator()
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *  \param[in] _val The value to set at the index (_row,_col)
	 */
	void setValue(const int _row, const int _col, const data_type _val)
	{
		m_matrix_impl->setValue(_row,_col,_val);
	}
	//@}

	/*! \name Public Iterators
	 *
	 *  Public iterators of the class SeqDenseMatrix. \n
	 *  These methods returns differents iterators on the matrix elements
	 */

	//@{

	/*!
	 *  \brief Return a const iterator on the first row of the matrix
	 *
	 *  \details
	 *  Call the method rowBegin of the chosen implementation.
	 *
	 *  \return The first element on the first row of the matrix
	 */
	typename MatrixImpl::const_row_iterator_type rowBegin() const
	{
		return const_cast<MatrixImpl*>(m_matrix_impl)->rowBegin();
	}

	/*!
	 *  \brief Return an iterator on the first row of the matrix
	 *
	 *  \details
	 *  Call the method rowBegin of the chosen implementation.
	 *
	 *  \return The first element on the first row of the matrix
	 */
	typename MatrixImpl::row_iterator_type rowBegin()
	{
		return m_matrix_impl->rowBegin();
	}

	/*!
	 *  \brief Return a const iterator on the last row of the matrix
	 *
	 *  \details
	 *  Call the method rowEnd of the chosen implementation.
	 *
	 *  \return The first element on the last row of the matrix
	 */
	typename MatrixImpl::const_row_iterator_type rowEnd() const
	{
		return const_cast<MatrixImpl*>(m_matrix_impl)->rowEnd();
	}

	/*!
	 *  \brief Return an iterator on the last row of the matrix
	 *
	 *  \details
	 *  Call the method rowEnd of the chosen implementation.
	 *
	 *  \return The first element on the last row of the matrix
	 */
	typename MatrixImpl::row_iterator_type rowEnd()
	{
		return m_matrix_impl->rowEnd();
	}

	/*!
	 *  \brief Return a const iterator on the first col of a row 
	 *
	 *  \details
	 *  Call the method colBegin of the chosen implementation. \n
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The first element on the first row of the matrix
	 */
	typename MatrixImpl::const_col_iterator_type colBegin(const typename MatrixImpl::const_row_iterator_type& _rowIterator) const
	{
		return const_cast<MatrixImpl*>(m_matrix_impl)->colBegin(_rowIterator);
	}

	/*!
	 *  \brief Return an iterator on the first col of a row 
	 *
	 *  \details
	 *  Call the method colBegin of the chosen implementation. \n
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \return The first element on the first row of the matrix
	 */
	typename MatrixImpl::col_iterator_type colBegin(const typename MatrixImpl::row_iterator_type& _rowIterator)
	{
		return m_matrix_impl->colBegin(_rowIterator);
	}

	/*!
	 *  \brief Return a const iterator on the last col of a row 
	 *
	 *  \details
	 *  Call the method colEnd of the chosen implementation. \n
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the first row of the matrix
	 */
	typename MatrixImpl::const_col_iterator_type colEnd(const typename MatrixImpl::const_row_iterator_type& _rowIterator) const
	{
		return const_cast<MatrixImpl*>(m_matrix_impl)->colEnd(_rowIterator);
	}

	/*!
	 *  \brief Return an iterator on the last col of a row 
	 *
	 *  \details
	 *  Call the method colEnd of the chosen implementation. \n
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \return The last element on the first row of the matrix
	 */
	typename MatrixImpl::col_iterator_type colEnd(const typename MatrixImpl::row_iterator_type& _rowIterator)
	{
		return m_matrix_impl->colEnd(_rowIterator);
	}

	/*!
	 *  \brief Return a const iterator on the diagonal element of a row
	 *
	 *  \details
	 *  Call the method diagonalElement of the chosen implementation. \n
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the diagonal element
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	typename MatrixImpl::const_col_iterator_type diagonalElement(const typename MatrixImpl::const_row_iterator_type& _rowIterator) const
	{
		return const_cast<MatrixImpl*>(m_matrix_impl)->diagonalElement(_rowIterator);
	}

	/*!
	 *  \brief Return an iterator on the diagonal element of a row
	 *
	 *  \details
	 *  Call the method diagonalElement of the chosen implementation. \n
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	typename MatrixImpl::col_iterator_type diagonalElement(const typename MatrixImpl::row_iterator_type& _rowIterator)
	{
		return m_matrix_impl->diagonalElement(_rowIterator);
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class SeqDenseMatrix. \n
	 *  These methods provide various utilities to the class SeqDenseMatrix
	 */
	//@{

	/*!
	 *  \brief Initialize a matrix with the parameters given as arguments
	 *
	 *  \details
	 *  Call the method initialize of the chosen implementation. \n
	 *  This is useful if a matrix is first created with the default constructor, then need to be initialized.
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 */
	void initialize(const int _rows, const int _cols, const int _nbNnz = 0)
	{
		m_matrix_impl->initialize(_rows,_cols,_nbNnz);
	}

	/*!
	 *  \brief Fill the matrix with a value.
	 *
	 *  \details
	 *  Call the method fill of the chosen implementation.
	 *
	 *  \param[in] _val The value to copy in the matrix
	 */
	void fill(const data_type _val)
	{
		m_matrix_impl->fill(_val);
  }

	/*!
	 *  \brief Fill a matrix by copying the pointeur passed in parameter.
	 *
	 *  \details
	 *  Call the method fill of the chosen implementation.
	 *
	 *  \param[in] _val The datas to copy
	 */
	void fill(const data_type* _val)
	{
    m_matrix_impl->fill(_val);
  }

	/*!
	 *  \brief Display a dense matrix on the standard output
	 *
	 *  \param[in] _os The output stream
	 *  \param[in] _mat The matrix to print
	 */
	friend std::ostream& operator<<(std::ostream& _os, const SeqDenseMatrix& _mat)
	{
		typedef typename MatrixImpl::const_row_iterator_type rowIt;
		typedef typename MatrixImpl::const_col_iterator_type colIt;
			
		_os << _mat.getClassName() << "<" << _mat.getImpl()->getClassName() << "> - ";
		_os << "Size: " << _mat.getNbRows() << "x" << _mat.getNbCols() << "\n";
		if(_mat.getPointeur()==0)
			return _os;
			
		_os << std::setw(9) << "GID" << " - " << std::setw(4) << "VAL \n";
		for(rowIt row = _mat.rowBegin(); row!= _mat.rowEnd();++row)
		{
			for(colIt col = _mat.colBegin(row);col!=_mat.colEnd(row);++col)
			{
				_os << "(" << std::setw(3) << row.index() << "," << std::left << std::setw(3) << col.index() << std::right << std::right << ") - " << std::setw(4) << *col << "\n";
			}
		}
		return _os;
	}

	//@}

	/*! \name Public Assignement Operators
	 *
	 *  Public Assignement Operators of the class SeqDenseMatrix. \n
	 */
	//@{

	/*!
	 *  \brief Copy a matrix through the assignment operator
	 *
	 *  \details
	 *  Call the method allocate of the chosen implementation. \n
	 *  Call the method fill of the chosen implementation.
	 *
	 *  \param[in] _matrix The matrix to copy
	 */
	void operator=(const SeqDenseMatrix& _matrix)
		{
			m_matrix_impl->allocate(_matrix);
			m_matrix_impl->fill(_matrix);
		}
	//@}
};
YALLA_END_NAMESPACE
#endif
