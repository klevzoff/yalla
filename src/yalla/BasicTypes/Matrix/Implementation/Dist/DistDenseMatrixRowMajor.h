#pragma once
#ifndef yalla_dist_dense_matrix_row_major_h
#define yalla_dist_dense_matrix_row_major_h

/*!
 *  \file DistDenseMatrixRowMajor.h
 *  \brief Distributed row major dense matrix
 *  \date 12/17/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/MatrixGraphs/DistDenseMatrixGraph.h"
#include "yalla/Utils/Exceptions/CheckBounds.h"
#include "yalla/Utils/Exceptions/PointerNotAllocated.h"
#include "yalla/Utils/Exceptions/FailedAllocation.h"
#include "yalla/Utils/Iterators/RowIteratorDistDenseRowMajorMatrix.h"
#include "yalla/Utils/Iterators/ColIteratorDistDenseRowMajorMatrix.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class DistDenseMatrixRowMajor
 *  \brief Distributed row major dense matrix
 * 
 *  \details
 *  The class stores the two local dimensions of the matrix in the attributes m_nb_rows and m_nb_cols. \n
 *  Datas are stocked in m_data, a unidimensionnal array. \n
 *  In addition, the class stores the number of non zero elements in the attribut m_nb_nnz, which, in the case of dense matrices is the number of rows times the number of cols. This is just to be consistent with sparse matrices. \n
 *  The graph of the matrix is also stored in the attribute m_matrix_graph. \n
 *
 *  For the distributed elements, the class stores three additionnal attributes:
 *  - m_global_nb_rows: The global number of rows in the matrix
 *  - m_global_nb_cols: The global number of cols in the matrix
 *  - m_col_mapping: The mapping between the local and global ID of cols
 *
 *  4 types of iterators are used here. One for iterating over the local rows, one for iterating over the local cols. They exist in both their const and non const version.
 *
 *  The class can throw three types of exception, only in debug mode:
 *  - isOutOfRange exception. If someones try to access an element at index (row,col), such that m_nb_rows < row < 0 or m_nb_cols < col < 0. This exception can be thrown in almost all setter/getter functions.
 *  - AllocationFailed exception. If an allocation failed, the function throws this exception. If an allocation failed, the function throws this exception. This exception can be thrown in all functions that allocates memory
 *  - PointerNotAllocated exception. If the user tries to access a non allocated pointer. This exception can be thrown in almost all setter/getter functions
 *
 *  \tparam DataType The data type of the matrix
 *
 *  \warning No alias test is implemented right now.
 *  \todo rowIterator that return an iterator on a specific row are not implemented
 */
template<typename DataType>
class DistDenseMatrixRowMajor
{
 public:
	//! \name Friend Class
	//@{
	//! Friend encapsulating class
	template<typename DataType2, typename MatrixImpl>
    friend class DistDenseMatrix;
	//@}

	//! \name Public Typedefs
	//@{
	//! \brief Data type of the matrix
	typedef DataType data_type;
	//! \brief Type of the graph
	typedef DistDenseMatrixGraph graph_type;
	//! \brief Type of this class
	typedef DistDenseMatrixRowMajor<data_type> this_type;
	//! \brief Type of the constant local rows iterator
	typedef RowIteratorDistDenseRowMajorMatrix<this_type,true> const_row_iterator_type;
	//! \brief Type of the local rows iterator
	typedef RowIteratorDistDenseRowMajorMatrix<this_type> row_iterator_type;
	//! \brief Type of the constant local cols iterator
	typedef ColIteratorDistDenseRowMajorMatrix<this_type,true> const_col_iterator_type;
	//! \brief Type of the local cols iterator
	typedef ColIteratorDistDenseRowMajorMatrix<this_type> col_iterator_type;
	//@}
 private:
	//! \brief Datas of the matrix
	data_type* m_data;
	//! \brief Number of local rows
	int m_nb_rows;
	//! \brief Global number of rows
	int m_global_nb_rows;
	//! \brief Number of local columns
	int m_nb_cols;
	//! \brief Global number of cols
	int m_global_nb_cols;
	//! \brief Number of local non zero elements
	int m_nb_nnz;
	//! \brief Mapping local/global ID of cols
	int* m_col_mapping;
	//! \brief Graph of the matrix
	graph_type* m_matrix_graph;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DistDenseMatrixRowMajor
	 */
	//@{
    
	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Set all attributes to zero
	 */
	explicit DistDenseMatrixRowMajor()
	{
    m_data = 0;
    m_nb_rows = 0;
    m_nb_cols = 0;
    m_nb_nnz = 0;
    m_global_nb_rows = 0;
    m_global_nb_cols = 0;
    m_col_mapping = 0;
    m_matrix_graph = 0;
  }

	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Call the method allocate to copy the matrix structure. \n
	 *  Call the method fill  to copy the datas. \n
	 *  Create a new graph for the matrix. \n
	 *
	 *  \exception AllocationFailed The allocation of m_data, m_cols_mapping or m_matrix_graph failed
	 *
	 *  \param[in] _matrix Matrix to copy
	 */
	explicit DistDenseMatrixRowMajor(const DistDenseMatrixRowMajor& _matrix)
	{
    this->allocate(_matrix);
    this->fill(_matrix);
  }

	/*!
	 *  \brief Dimension and mapping constructor
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_global_nb_rows and m_global_nb_cols. \n
	 *  Also set the (useless) parameter m_nb_nnz to m_nb_rows*m_nb_cols. \n
	 *  Call the method allocateDatas and allocateMapping to allocate the different pointers of the data structure. \n
	 *  Copy the values of _mapping in m_col_mapping and set up the matrix graph. \n
	 *  Fill the data pointer with zeros. 
	 *
	 *  \exception AllocationFailed The allocation of m_data, m_col_mapping or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 */
	explicit DistDenseMatrixRowMajor(const int _rows, const int _cols, const int _globalRows, const int _globalCols, const int* _mapping)
	{
    m_nb_rows = _rows;
    m_nb_cols = _cols;
    m_nb_nnz = m_nb_rows*m_nb_cols;
    m_global_nb_rows = _globalRows;
    m_global_nb_cols = _globalCols;
    this->allocateDatas(m_nb_nnz);
    this->allocateMapping(m_nb_cols);
    for(int i=0;i<m_nb_cols;++i)
      m_col_mapping[i] = _mapping[i];
    this->fill(data_type());
    m_matrix_graph = new graph_type(m_nb_rows,m_nb_cols,m_nb_nnz,m_global_nb_rows,m_global_nb_cols,m_col_mapping);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
  }

	/*!
	 *  \brief Dimension and mapping constructor with nnz
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_global_nb_rows and m_global_nb_cols. \n
	 *  Also set the (useless) parameter m_nb_nnz to m_nb_rows*m_nb_cols. \n
	 *  Call the method allocateDatas and allocateMapping to allocate the different pointers of the data structure. \n
	 *  Copy the values of _mapping in m_col_mapping and set up the matrix graph. \n
	 *  Fill the data pointer with zeros. 
	 *
	 *  \exception AllocationFailed The allocation of m_data, m_col_mapping or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix (not used)
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 */
	explicit DistDenseMatrixRowMajor(const int _rows, const int _cols, const int _nbNnz, const int _globalRows, const int _globalCols, const int* _mapping)
	{
    m_nb_rows = _rows;
    m_nb_cols = _cols;
    m_nb_nnz = m_nb_rows*m_nb_cols;
    m_global_nb_rows = _globalRows;
    m_global_nb_cols = _globalCols;
    this->allocateDatas(m_nb_nnz);
    this->allocateMapping(m_nb_cols);
    for(int i=0;i<m_nb_cols;++i)
      m_col_mapping[i] = _mapping[i];
    this->fill(data_type());
    m_matrix_graph = new graph_type(m_nb_rows,m_nb_cols,m_nb_nnz,m_global_nb_rows,m_global_nb_cols,m_col_mapping);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
  }

	/*!
	 *  \brief Dimension and mapping constructor with datas
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_global_nb_rows and m_global_nb_cols. \n
	 *  Also set the (useless) parameter m_nb_nnz to m_nb_rows*m_nb_cols. \n
	 *  Call the method allocateDatas and allocateMapping to allocate the different pointers of the data structure. \n
	 *  Copy the values of _dataPtr and _mapping in m_data and m_col_mapping and set up the matrix graph. \n
	 *
	 *  \exception AllocationFailed The allocation of m_data, m_col_mapping or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 *  \param[in] _dataPtr Data pointer
	 */
	explicit DistDenseMatrixRowMajor(const int _rows, const int _cols, const int _globalRows, const int _globalCols, const int* _mapping, const data_type * _dataPtr)
	{
    m_nb_rows = _rows;
    m_nb_cols = _cols;
    m_nb_nnz = m_nb_rows*m_nb_cols;
    m_global_nb_rows = _globalRows;
    m_global_nb_cols = _globalCols;
    this->allocateDatas(m_nb_nnz);
    this->allocateMapping(m_nb_cols);
    for(int i=0;i<m_nb_cols;++i)
      m_col_mapping[i] = _mapping[i];
    this->fill(_dataPtr);
    m_matrix_graph = new graph_type(m_nb_rows,m_nb_cols,m_nb_nnz,m_global_nb_rows,m_global_nb_cols,m_col_mapping);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
  }

	/*!
	 *  \brief Graph constructor
	 *
	 *  \details
	 *  Copy the value of matrixGraph.getNbRows(), _matrixGraph.getNbCols(), _matrixGraph.getGlobalNbRows() and _matrixGraph.getGlobalNbCols() in m_nb_rows, m_nb_cols, m_global_nb_rows and m_global_nb_cols. \n
	 *  Also set the (useless) parameter m_nb_nnz to m_nb_rows*m_nb_cols. \n
	 *  Call the method allocateDatas and allocateMapping to allocate the different pointers of the data structure. \n
	 *  Copy the values of _mapping in m_col_mapping and set up the matrix graph. \n
	 *  Fill the data pointer with zeros. 
	 *
	 *  \exception AllocationFailed The allocation of m_data, m_col_mapping or m_matrix_graph failed
	 *
	 *  \param[in] _matrixGraph The matrix graph
	 */
	explicit DistDenseMatrixRowMajor(const graph_type& _matrixGraph)
	{
    m_nb_rows = _matrixGraph.getNbRows();
    m_nb_cols = _matrixGraph.getNbCols();
    m_nb_nnz = m_nb_rows*m_nb_cols;
    m_global_nb_rows = _matrixGraph.getGlobalNbRows();
    m_global_nb_cols = _matrixGraph.getGlobalNbCols();
    this->allocateDatas(m_nb_nnz);
		this->allocateMapping(m_nb_cols);
    for(int i=0;i<m_nb_cols;++i)
      m_col_mapping[i] = _matrixGraph.getMapping(i);
    this->fill(data_type());
    m_matrix_graph = new graph_type(m_nb_rows,m_nb_cols,m_nb_nnz,m_global_nb_rows,m_global_nb_cols,m_col_mapping);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
  }
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DistDenseMatrixRowMajor
	 */
	//@{
    
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class
	 */
	virtual ~DistDenseMatrixRowMajor()
	{
    delete[] m_data;
    m_data = 0;
    delete [] m_col_mapping;
    m_col_mapping = 0;
    delete m_matrix_graph;
    m_matrix_graph = 0;
  }

	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class DistDenseMatrixRowMajor. \n
	 *  These methods give access in read-only mode to the datas/information of the class DistDenseMatrixRowMajor
	 */

	//@{
    
	/*! 
	 *  \brief Return the number of local rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of rows in the matrix
	 */
	int getNbRows() const
	{
    return m_nb_rows;
  }

	/*! 
	 *  \brief Return the number of global rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of global rows in the matrix
	 */
	int getGlobalNbRows() const
	{
    return m_global_nb_rows;
  }

	/*! 
	 *  \brief Return the number of local cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of local cols in the matrix
	 */
	int getNbCols() const
	{
    return m_nb_cols;
  }

	/*! 
	 *  \brief Return the number of global cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of global cols in the matrix
	 */
	int getGlobalNbCols() const
	{
    return m_global_nb_cols;
  }

	/*! 
	 *  \brief Return the number of local non zero elements in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of local non zero elements in the matrix
	 */	
	int getNnz() const
	{
    return m_nb_nnz;
  }

	/*! 
	 *  \brief Return the global ID of a column
	 *
	 *  \details
	 *
	 *  \param[in] _index The local ID of the col
	 *
	 *  \return The global ID of the local col
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	int getMapping(const int _index) const
	{
#ifdef _DEBUG
    this->checkBounds(_index,0,m_nb_cols);
    this->checkAllocated(m_col_mapping);
#endif
    return m_col_mapping[_index];
  }

	/*! 
	 *  \brief Return the global mapping.
	 *
	 *  \details
	 *
	 *  \return The local/global ID mapping array
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 */
	const int* getMapping() const
	{
#ifdef _DEBUG
    this->checkAllocated(m_col_mapping);
#endif
    return m_col_mapping;
  }

	/*! 
	 *  \brief Return a pointeur on the datas of the matrix.
	 *
	 *  \details
	 *
	 *  \return The data pointer of the matrix
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 */
	const data_type* getPointeur() const
	{
#ifdef _DEBUG
    this->checkAllocated(m_data);
#endif
    return m_data;
  }

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	data_type operator()(const int _row, const int _col) const
	{
#ifdef _DEBUG
    this->checkBounds(_row,0,m_nb_rows);
    this->checkBounds(_col,0,m_nb_cols);
		this->checkAllocated(m_data);
#endif
    return m_data[_row*m_nb_cols + _col];
  }

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type operator[](const int _index) const
	{
#ifdef _DEBUG
    this->checkBounds(_index,0,m_nb_nnz);
		this->checkAllocated(m_data);
#endif
    return m_data[_index];
  }

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DistDenseMatrixRowMajor" The name of the class
	 */
	const char* getClassName() const
	{
    return "DistDenseMatrixRowMajor";
  }

	/*!
	 *  \brief Return the graph of the matrix. 
	 *
	 *  \details
	 *
	 *  \return The graph of the matrix
	 *
	 *  \exception PtrNotAllocated m_matrix_graph is not allocated
	 */
	const graph_type& getMatrixGraph() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_matrix_graph);
#endif
    return *m_matrix_graph;
  }

	/*! \name Public Setter
	 *
	 *  Public Setter of the class DistDenseMatrixRowMajor. \n
	 *  These methods give access in read/write mode to the datas/information of the class DistDenseMatrixRowMajor
	 */
	//@{

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 *
	 *  \exception PtrNotAllocated m_datas is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	data_type& operator()(const int _row, const int _col)
  {
#ifdef _DEBUG
    this->checkBounds(_row,0,m_nb_rows);
    this->checkBounds(_col,0,m_nb_cols);
		this->checkAllocated(m_data);
#endif
    return m_data[_row*m_nb_cols + _col];
  }

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type& operator[](const int _index)
	{
#ifdef _DEBUG
    this->checkBounds(_index,0,m_nb_nnz);
		this->checkAllocated(m_data);
#endif
    return m_data[_index];
  }

	/*!
	 *  \brief Set the value in the matrix located at the index (_row,_col) to _val
	 *
	 *  \details
	 *  This method is strictly similar to the operator().
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *  \param[in] _val The value to set at the index (_row,_col)
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	void setValue(const int _row, const int _col, const data_type _val)
	{
#ifdef _DEBUG
    this->checkBounds(_row,0,m_nb_rows);
    this->checkBounds(_col,0,m_nb_cols);
		this->checkAllocated(m_data);
#endif
    m_data[_row*m_nb_cols + _col] = _val;
  }
	//@}

	/*! \name Public Iterators
	 *
	 *  Public iterators of the class DistDenseMatrixRowMajor. \n
	 *  These methods returns differents iterators on the matrix elements
	 */

	/*!
	 *  \brief Return a const iterator on the first local row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the first local row of the matrix
	 */
	const_row_iterator_type rowBegin() const
	{
    return const_row_iterator_type(&m_data[0], *this);
  }

	/*!
	 *  \brief Return an iterator on the first local row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the first local row of the matrix
	 */
	row_iterator_type rowBegin()
	{
    return row_iterator_type(&m_data[0], *this);
  }

	/*!
	 *  \brief Return a const iterator on the last local row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the last local row of the matrix
	 */
	const_row_iterator_type rowEnd() const
	{
    return const_row_iterator_type(&m_data[m_nb_rows*m_nb_cols], *this);
  }

	/*!
	 *  \brief Return an iterator on the last local row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the last local row of the matrix
	 */
	row_iterator_type rowEnd()
	{
    return row_iterator_type(&m_data[m_nb_rows*m_nb_cols], *this);
  }

	/*!
	 *  \brief Return a const iterator on the first local col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The first element on the specified local row of the matrix
	 */
	const_col_iterator_type colBegin(const const_row_iterator_type& _rowIterator) const
	{
    return const_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
  }

	/*!
	 *  \brief Return an iterator on the first local col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \return The first element on the specified local row of the matrix
	 */
	col_iterator_type colBegin(const row_iterator_type& _rowIterator)
	{
    return col_iterator_type(_rowIterator.getPosition(), _rowIterator);
  }

	/*!
	 *  \brief Return a const iterator on the last local col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the specified local row of the matrix
	 */
	const_col_iterator_type colEnd(const const_row_iterator_type& _rowIterator) const
	{
    return const_col_iterator_type(_rowIterator.getPosition()+m_nb_cols, _rowIterator);
  }

	/*!
	 *  \brief Return an iterator on the last local col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the specified local row of the matrix
	 */
	col_iterator_type colEnd(const row_iterator_type& _rowIterator)
	{
    return col_iterator_type(_rowIterator.getPosition()+m_nb_cols, _rowIterator);
  }

	/*!
	 *  \brief Return a const iterator on the diagonal element of a row
	 *
	 *  \details
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the diagonal element
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	col_iterator_type diagonalElement(const row_iterator_type& _rowIterator)
	{
    col_iterator_type colIt =  col_iterator_type(_rowIterator.getPosition(), _rowIterator);
		if(colIt.globalIndex()==_rowIterator.index())
			return colIt;
		else
		{
			while(colIt.globalIndex()<_rowIterator.index() && colIt!=this->colEnd(_rowIterator))
				++colIt;
		}
		return colIt;
  }

	/*!
	 *  \brief Return an iterator on the diagonal element of a row
	 *
	 *  \details
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	const_col_iterator_type diagonalElement(const const_row_iterator_type& _rowIterator) const
	{
    const_col_iterator_type colIt =  const_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
		if(colIt.globalIndex()==_rowIterator.index())
			return colIt;
		else
		{
			while(colIt.globalIndex()<_rowIterator.index() && colIt!=this->colEndConst(_rowIterator))
				++colIt;
		}
		return colIt;
  }
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class DistDenseMatrixRowMajor. \n
	 *  These methods provide various utilities to the class DistDenseMatrixRowMajor
	 */
	//@{

	/*!
	 *  \brief Initialize a matrix with the parameters given as arguments
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_global_nb_rows and m_global_nb_cols. \n
	 *  Also set the (useless) parameter m_nb_nnz to m_nb_rows*m_nb_cols. \n
	 *  Call the method allocateDatas and allocateMapping to allocate the different pointers of the data structure. \n
	 *  Copy the values of _mapping in m_col_mapping and set up the matrix graph. \n
	 *  Fill the data pointer with zeros. 
	 *
	 *  \exception AllocationFailed The allocation of m_data, m_col_mapping or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 */
	void initialize(const int _rows, const int _cols, const int _globalRows, const int _globalCols, const int* _mapping)
	{
		delete[] m_data;
		delete[] m_col_mapping;
		delete m_matrix_graph;
		m_nb_rows = _rows;
		m_nb_cols = _cols;
		m_nb_nnz = m_nb_rows*m_nb_cols;
		m_global_nb_rows = _globalRows;
		m_global_nb_cols = _globalCols;
		this->allocateDatas(m_nb_nnz);
    this->allocateMapping(m_nb_cols);
		for(int i=0;i<m_nb_cols;++i)
			m_col_mapping[i] = _mapping[i];
		m_matrix_graph = new graph_type(m_nb_rows,m_nb_cols,m_nb_nnz,m_global_nb_rows,m_global_nb_cols,m_col_mapping);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
	}
	
	/*!
	 *  \brief Initialize a matrix with the parameters given as arguments
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_global_nb_rows and m_global_nb_cols. \n
	 *  Also set the (useless) parameter m_nb_nnz to m_nb_rows*m_nb_cols. \n
	 *  Call the method allocateDatas and allocateMapping to allocate the different pointers of the data structure. \n
	 *  Copy the values of _mapping in m_col_mapping and set up the matrix graph. \n
	 *  Fill the data pointer with zeros. 
	 *
	 *  \exception AllocationFailed The allocation of m_data, m_col_mapping or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix (not used)
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 */
	void initialize(const int _rows, const int _cols, const int _nbNnz, const int _globalRows, const int _globalCols, const int* _mapping)
	{
		delete[] m_data;
		delete[] m_col_mapping;
		delete m_matrix_graph;
		m_nb_rows = _rows;
		m_nb_cols = _cols;
		m_nb_nnz = m_nb_rows*m_nb_cols;
		m_global_nb_rows = _globalRows;
		m_global_nb_cols = _globalCols;
		this->allocateDatas(m_nb_nnz);
    this->allocateMapping(m_nb_cols);
		for(int i=0;i<m_nb_cols;++i)
			m_col_mapping[i] = _mapping[i];
		m_matrix_graph = new graph_type(m_nb_rows,m_nb_cols,m_nb_nnz,m_global_nb_rows,m_global_nb_cols,m_col_mapping);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
	}

	/*!
	 *  \brief Fill the matrix with a value.
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _val The value to copy in the matrix
	 */
	void fill(const data_type _val)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
    for(int i=0;i<m_nb_nnz;++i)
      m_data[i] = _val;
  }

	/*!
	 *  \brief Fill a matrix by copying the pointer passed in parameter.
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _src The datas to copy
	 */
	void fill(const data_type* _src)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
    for(int i=0;i<m_nb_nnz;++i)
      m_data[i] = _src[i];
  }
	// @}
 private:
	/*! \name Private Utils
	 *
	 *  Private Utils of the class DistDenseMatrixRowMajor. \n
	 *  These internal methods provides various utilities to the class DistDenseMatrixRowMajor
	 */
	//@{

	/*!
	 *  \brief Fill an allocated matrix with another matrix
	 *
	 *  \details
	 *
	 *  \tparam MatrixType The matrix type used to fill the data pointer
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *
	 *  \param[in] _src The matrix to copy
	 */
	template<typename MatrixType>
    void fill(const MatrixType& _src)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
		for(int i=0;i<m_nb_rows;++i)
		{
			for(int j=0;j<m_nb_cols;++j)
			{
				m_data[i*m_nb_cols + j] = _src(i,j);
			}
		}
	}
	//@}

	/*! \name Private Allocator
	 *
	 *  Private Allocator of the class DistDenseMatrixRowMajor. \n
	 *  These internal methods allow to allocate a DistDenseMatrixRowMajor in different cases
	 */
	//@{

	/*!
	 *  \brief Allocate the data pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the data pointer (should be m_nb_nnz)
	 *
	 *  \exception AllocationFailed Allocation of m_data failed
	 */
	void allocateDatas(const int _size)
	{
    if(_size==0)
      m_data = 0;
    else
      m_data = new data_type[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_data);
#endif
  }

	/*!
	 *  \brief Allocate the mapping pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the mapping pointer (should be m_nb_cols)
	 *
	 *  \exception AllocationFailed Allocation of m_mapping failed
	 */
	void allocateMapping(const int _size)
	{
    if(_size==0)
      m_col_mapping = 0;
    else
      m_col_mapping = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_col_mapping);
#endif
  }

	/*!
	 *  \brief Allocate a matrix from another matrix
	 *
	 *  \details
	 *
	 *  \tparam MatrixType The matrix type used to allocate the matrix
	 *
	 *  \param[in] _matrix The matrix to copy
	 *
	 *  \exception AllocationFailed Allocation of m_data, m_mapping or m_matrix_graph failed
	 */
	template<typename MatrixType>
    void allocate(const MatrixType& _matrix)
	{
		delete[] m_data;
		delete[] m_col_mapping;
		m_nb_rows = _matrix.getNbRows();
		m_nb_cols = _matrix.getNbCols();
		m_nb_nnz = m_nb_rows*m_nb_cols;
		m_global_nb_rows = _matrix.getGlobalNbRows();
		m_global_nb_cols = _matrix.getGlobalNbCols();
		this->allocateDatas(m_nb_nnz);
		this->allocateMapping(m_nb_cols);
		const int* rhsMapping = _matrix.getMapping();
		for(int i=0;i<m_nb_cols;++i)
			m_col_mapping[i] = rhsMapping[i];
		delete m_matrix_graph;
		m_matrix_graph = new graph_type(m_nb_rows,m_nb_cols,m_nb_nnz,m_global_nb_rows,m_global_nb_cols,m_col_mapping);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
	}
	//@}


	/*! \name Private Checkeur
	 *
	 *  Private Checkeur of the class DistDenseMatrixRowMajor. \n
	 *  These internal methods provides various check utilities for the class DistDenseMatrixRowMajor
	 */
	//@{

	/*!
	 *  \brief Check if an allocation was successful
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception AllocationFailed _datas was not properly allocated
	 */    
	void checkAllocation(const void* _datas) const
	{
    try
    {
      if(FailedAllocation::Check(_datas))
        throw AllocationFailed(__FILE__,__LINE__);
    }
    catch ( AllocationFailed &ptrFailed )
    {
      YALLA_ERR(ptrFailed.what());
    }
  }

	/*!
	 *  \brief Check if a pointer was allocated
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception PtrNotAllocated _datas is not allocated
	 */
	void checkAllocated(const void* _datas) const
	{
		try
		{
			if(PointerNotAllocated::Check(_datas))
				throw AllocationFailed(__FILE__,__LINE__);
		}
		catch ( PtrNotAllocated &ptrFailed )
		{
			YALLA_ERR(ptrFailed.what());
		}
	}

	/*!
	 *  \brief Check if an index is between specified bounds
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to  check
	 *  \param[in] _lowerBound The lower bound
	 *  \param[in] _upperBound The upper bound
	 *
	 *  \exception isOutOfRange _index is not between lower/upper bound
	 */
	void checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
	{
    try
    {
      if(CheckBounds::Check(_index,_lowerBound,_upperBound))
        throw isOutOfRange(__FILE__,__LINE__);
    }
    catch ( isOutOfRange &isOut )
    {
      YALLA_ERR(isOut.what());
    }
  }
	//@}
};

YALLA_END_NAMESPACE
#endif
