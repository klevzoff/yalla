#pragma once
#ifndef _yalla_epetra_matrix_h
#define _yalla_epetra_matrix_h

/*!
 *  \file EpetraMatrix.h
 *  \brief Wrapper around the Trilinos Epetra matrix
 *  \date 12/16/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <iostream>
#include "yalla/Utils/Types/myTypes.h"
#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#ifdef WITH_TRILINOS
#include "Epetra_CrsMatrix.h"
#include "Epetra_BlockMap.h"
#endif

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class EpetraMatrix
 *  \brief Wrapper around the Trilinos Epetra matrix
 * 
 *  \details
 *  This class is a first draft of a wrapper around the Trilinos Epetra matrix. \n
 *  It implements some of the necessary methods to be used in the code. \n
 *  However, iterators have not been included and therefore must be implemented for a full integration of the Epetra matrix class in the yalla code. \n
 *  This should not be a big problem, as most of the elements required are already in place. \n
 *  The Epetra matrix class is a classical CSR representation of a matrix in the standard 3 array variation, so very close to the DistCSRMatrix. \n
 *  The extension of this class could probably be done easily providing someone has the time to do it, and is ready to take a look a the Trilinos Epetra class. Indeed, probably all the required elements are already implemented for the DistCSRMatrix. \n
 *  Indeed, to implement the iterators, one will probably have to use the internal structure of the Trilinos Epetra matrix and not only the API.
 *
 *  On the class itself, not much needs to be said. The Trilinos matrix is stored in the template attribute m_matrix and the m_own_matrix flag is used to define the ownership of the matrix. \n
 *  All the methods are then a wrapper around the Trilinos API to provide the information required. \n
 *  Not all necessary methods are implemented.
 *
 *  \tparam MatrixType The type of the matrix
 */
template<typename MatrixType>
class EpetraMatrix
{
 public:
	//! \name Public Typedefs
	//@{
	//! \brief Data type of the matrix
	typedef double data_type;
	//! \brief Type of the matrix
	typedef MatrixType impl_type;
	//! \brief Type of this class
	typedef EpetraMatrix<impl_type> this_type;
	//! \brief Type of the graph
	typedef Epetra_BlockMap graph_type;
	//@}
 private:
	//! \brief Pointer on the matrix implementation
	impl_type* m_matrix;
	//! \brief Ownership flag
	bool m_own_matrix;
	//! \brief Attribute always valued to zero
	data_type m_zero;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class EpetraMatrix
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Set all attributes to zero/false.
	 */
	explicit EpetraMatrix()
	{
		m_matrix = 0;
		m_zero = 0;
		m_own_matrix = true;
	}

	/*!
	 *  \brief Epetra constructor
	 *
	 *  \details
	 *  Copy the address of the implementation and set the ownership flag to false. \n
	 *  The matrix has to be built inside Trilinos. Therefore the matrix is not deep copied. \n
	 *
	 *  \param[in] _matrix Matrix to copy
	 */
	explicit EpetraMatrix(impl_type* _matrix)
	{
		m_matrix = _matrix;
		m_zero = 0;
		m_own_matrix = false;
	}

	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Deep copy of the implementation and set the ownership flag to true. \n
	 *  Call the copy constructor of the relevant Trilinos matrix class.
	 *
	 *  \param[in] _matrix Matrix to copy
	 */
	explicit EpetraMatrix(const EpetraMatrix& _matrix)
	{
		m_matrix = new impl_type(*_matrix.getMatrix());
		m_zero = 0;
		m_own_matrix = true;
	}

	/*!
	 *  \brief Graph constructor
	 *
	 *  \details
	 *  Call the graph constructor of the relevant Trilinos matrix class and set the ownership flag to true.
	 *  
	 *  \param[in] _matrixGraph The matrix graph
	 */
	explicit EpetraMatrix(const graph_type& _matrixGraph)
	{
		m_matrix = new impl_type(_matrixGraph);
		m_zero = 0;
		m_own_matrix = true;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class EpetraMatrix
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class
	 */
	virtual ~EpetraMatrix()
	{
		if(m_own_matrix)
		{
			delete m_matrix;
			m_matrix = 0;
		}
	}
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class EpetraMatrix. \n
	 *  These methods give access in read-only mode to the datas/information of the class EpetraMatrix
	 */

	//@{
	/*! 
	 *  \brief Return the number of local rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of rows in the matrix
	 */
	int getNbRows() const
	{
		return m_matrix->NumMyRows();
	}

	/*! 
	 *  \brief Return the number of global rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of global rows in the matrix
	 */
	int getGlobalNbRows() const
	{
		return m_matrix->NumGlobalRows();
	}

	/*! 
	 *  \brief Return the number of local cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of local cols in the matrix
	 */
	int getNbCols() const
	{
		return m_matrix->NumMyCols();
	}

	/*! 
	 *  \brief Return the number of global cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of global cols in the matrix
	 */
	int getGlobalNbCols() const
	{
		return m_matrix->NumGlobalCols();
	}

	/*! 
	 *  \brief Return the number of local non zero elements in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of local non zero elements in the matrix
	 */	
	int getNnz() const
	{
		return m_matrix->NumMyNonzeros();
	}

	/*! 
	 *  \brief Return the global ID of a row
	 *
	 *  \details
	 *
	 *  \param[in] _index The local ID of the row/col
	 *
	 *  \return The global ID of the local row/col
	 */
	int getMapping(const int _index) const
	{
		return (m_matrix->Map().MyGlobalElements())[_index];
	}

	/*! 
	 *  \brief Return the global mapping.
	 *
	 *  \details
	 *
	 *  \return The local/global ID mapping array
	 */
	const int* getMapping() const
	{
		return reinterpret_cast<int*>(m_matrix->Map().MyGlobalElements());
	}

	/*! 
	 *  \brief Return a pointeur on the datas of the matrix.
	 *
	 *  \details
	 *
	 *  \return The data pointer of the matrix
	 */
	const data_type* getPointeur() const
	{
		return (*m_matrix)[0];
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 */
	data_type operator()(const int _row, const int _col) const
	{
		int size, *cols;
		double* tmpVals;
		m_matrix->ExtractMyRowView(_row, size, tmpVals, cols);
		for(int i=0;i<size;++i)
		{
			if(_col==cols[i])
				return tmpVals[i];
		}
		return data_type();
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 */
	data_type operator[](const int _index) const
	{
		return ((*m_matrix)[0])[_index];
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DistCSRMatrix" The name of the class
	 */
	const char* getClassName() const
	{
		return "EpetraMatrix";
	}

	/*!
	 *  \brief Return the graph of the matrix. 
	 *
	 *  \details
	 *
	 *  \return The graph of the matrix
	 *
	 *  \exception PtrNotAllocated m_matrix_graph is not allocated
	 */
	const graph_type& getMatrixGraph() const
	{
		return m_matrix->Map();
	}

	/*! 
	 *  \brief Return a handler on the matrix implementation
	 *
	 *  \details
	 *
	 *  \return Return a handler on the matrix implementation
	 */
	impl_type* getMatrix() const
	{
		return m_matrix;
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class EpetraMatrix. \n
	 *  These methods give access in read/write mode to the datas/information of the class EpetraMatrix
	 */
	//@{

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *  If there is no value located at the index (_row,_col), this methods returns the attribute m_zero, set to zero. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 */
	data_type& operator()(const int _row, const int _col)
	{
		data_type* tmp = (*m_matrix)[0];
		int size, *cols;
		double* tmpVals;
		m_matrix->ExtractMyRowView(_row, size, tmpVals, cols);
		for(int i=0;i<size;++i)
		{
			if(static_cast<int>(_col)==cols[i])
				return tmp[i];
		}
		m_zero = 0;
		return m_zero;
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 */
	data_type& operator[](const int _index)
	{
		return ((*m_matrix)[0])[_index];
	}

	/*! 
	 *  \brief Set the value in the matrix located at the index (_row,_col) to _val
	 *
	 *  \details
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *  \param[in] _val The value to set
	 */
	void setValue(const int _row, const int _col, const data_type _val)
	{
		m_matrix->ReplaceMyValues(_row, 1, &_val, reinterpret_cast<const int*>(&_col));
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class EpetraMatrix. \n
	 *  These methods provide various utilities to the class EpetraMatrix
	 */
	//@{

	/*!
	 *  \brief Fill the matrix with a value.
	 *
	 *  \details
	 *
	 *  \param[in] _val The value to copy in the matrix
	 */
	void fill(const data_type _val)
	{
		m_matrix->PutScalar(_val);
	}

	/*!
	 *  \brief Fill a matrix by copying the pointer passed in parameter.
	 *
	 *  \details
	 *
	 *  \param[in] _src The datas to copy
	 */
	void fill(const data_type* _src)
	{
		data_type* tmp = (*m_matrix)[0];
		for(int i=0;i<this->getNnz();++i)
			tmp[i] = _src[i];
	}

	/*!
	 *  \brief Display a sparse matrix on the standard output
	 */
	void print() const
	{
		std::cout << *m_matrix;
		std::cout << std::flush;
	}
	//@}

	/*! \name Public Allocator
	 *
	 *  Public Allocator of the class EpetraMatrix. \n
	 *  These internal methods allow to allocate pointers of the class EpetraMatrix
	 */
	//@{

	/*!
	 *  \brief Allocate and fill a matrix from another matrix
	 *
	 *  \details
	 *
	 *  \tparam MatrixType The matrix type used to allocate and fill the matrix
	 *
	 *  \param[in] _matrix The matrix to copy
	 */
	template<typename NewMatrixType>
    void allocateAndFill(const NewMatrixType& _matrix)
	{
		if(m_own_matrix)
			delete m_matrix;
		m_matrix = new impl_type(*(_matrix.getImpl()->getMatrix()));
	}
	//@}
};
YALLA_END_NAMESPACE
#endif
