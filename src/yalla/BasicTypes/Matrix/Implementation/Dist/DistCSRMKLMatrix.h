#pragma once
#ifndef yalla_dist_csr_mkl_matrix_h
#define yalla_dist_csr_mkl_matrix_h

/*!
 *  \file DistCSRMKLMatrix.h
 *  \brief Distributed CSR matrix in "block" format
 *  \date 12/16/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include <unordered_set>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/MatrixGraphs/DistCSRMatrixGraph.h"
#include "yalla/Utils/Exceptions/CheckBounds.h"
#include "yalla/Utils/Exceptions/PointerNotAllocated.h"
#include "yalla/Utils/Exceptions/FailedAllocation.h"
#include "yalla/Utils/Iterators/RowIteratorDistCSRMatrix.h"
#include "yalla/Utils/Iterators/ColIteratorDistCSRMatrix.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class DistCSRMKLMatrix
 *  \brief Distributed CSR matrix that can be used with the Intel MKL
 * 
 *  Compared to the DistCSRMatrix, this implementation separate the "diagonal" block of the matrix (the one with only diagonal elements) and the "non diagonal" block of the matrix. \n
 *  This approach allows to:
 *  - Be more efficient to compute a SPMV
 *  - Be able to use the multithreaded Intel MKL in a distributed environment.
 *
 *  Therefore, all attributes are duplicated to match this representation. Refer to the technical documentation for more details.
 *
 *  The class stores the two local dimensions of the matrix in the attributes m_nb_rows and m_nb_cols. \n
 *  In addition, local dimensions of the (non) diagonal block are stored in the attributes m_(n)diag_nb_rows and m_(n)diag_nb_cols. \n
 *  Datas are stored in m_(n)diag_data, a unidimensionnal array. \n
 *  In addition, the class store the local number of non zero elements in the attribute m_nb_nnz. \n
 *  The number of non zero elements is stored in the attribute m_nb_(n)diag_nnz for the (non) diagonal block. \n
 *  The graph of the matrix is also stored in the attribute m_matrix_graph. The graph representation is the same than the DistCSRMatrix. \n
 *
 *  The data structure of the matrix is stored in the classical 3 array variation of a CSR matrix in the attributes m_(n)diag_rows_ptr and m_(n)diag_cols_ptr for the (non) diagonal block. \n
 *
 *  For the distributed elements, the class stores three additionnal attributes:
 *  - m_global_nb_rows: The global number of rows in the matrix
 *  - m_global_nb_cols: The global number of cols in the matrix
 *  - m_mapping: The mapping between the local and global ID of rows/cols
 *  The mapping is shared between the columns and the rows, this is why the m_mapping pointer has the dimension of m_diag_nb_cols. The m_ndiag_mapping maps the local elements of the non diagonal block to the diagonal block. Therefore, the global ID of a local element i is obtained by m_mapping[i]. The global ID of a non diagonal elements i is obtained by m_mapping[m_ndiag_mapping[i]]. Refer to the technical documentation for more details.
 *
 *  The matrix is stored in the following way:
 *  - The diagonal block is stored in all m_diag_XXX elements, the diagonal element of each row first.
 *  - The diagonal block is stored in all m_ndiag_XXX elements, row by row then by ascending column.
 *  This is ensured by calling countElements then fillPointers method. \n
 *
 *  8 types of iterators are used here. One for iterating over the diagonal rows, one for iterating over the non-diagonal rows, one for iterating over the diagonals cols, one for iterating over the non-diagonal cols. They exist in both their const and non const version. In this implementation, the diagonal and non-diagonal iterators are not the same. \n
 *  Diagonal iterators iterate over the diagonal block, whereas non-diagonal iterators iterate over the non diagonal block. In addition, the way of computing the index and non-diagonal of an element is not the same. Please refer to the technical documentation for more details.
 *
 *  The class can throw three types of exception, only in debug mode:
 *  - isOutOfRange exception. If someones try to access an element at index (row,col), such that m_nb_rows < row < 0 or m_nb_cols < col < 0. This exception can be thrown in almost all setter/getter functions.
 *  - AllocationFailed exception. If an allocation failed, the function throws this exception. This exception can be thrown in all functions that allocates memory
 *  - PointerNotAllocated exception. If the user tries to access a non allocated pointer. This exception can be thrown in almost all setter/getter functions
 *
 *  \tparam DataType The data type of the matrix
 *
 *  \warning No alias test is implemented right now.
 *  \warning In this implementation, the differents flags representing the current state of the object (m_is_allocated, m_is_init and m_is_ready) has not be tested and should be used with caution (probably not working at all)
 *
 *  \todo Some methods in this class are not properly/fully implemented.
 *  \todo One possible approach when datas are not passed as the matrix is built would be to store the datas in a standard CSR 3 array variation, and call the countEntries and fillPointers only at the end, when finalizing the matrix. \n For the constructor with no datas, that would allow the user to not have to handle the sorting himself. \n For the differents fill methods it would also be simpler to copy the datas. \n 
 */
template<typename DataType>
class DistCSRMKLMatrix
{
 public:
	//! \name Friend Class
	//@{
	//! Friend encapsulating class
	template<typename DataType2, typename MatrixImpl>
    friend class DistSparseMatrix;
	//@}

	//! \name Public Typedefs
	//@{
	//! \brief Data type of the matrix
	typedef DataType data_type;
	//! \brief Type of the graph
	typedef DistCSRMatrixGraph graph_type;
	//! \brief Type of this class
	typedef DistCSRMKLMatrix<data_type> this_type;
	//! \brief Type of the constant diagonal rows iterator
	typedef RowIteratorDistCSRMatrix<this_type, true> const_loc_row_iterator_type;
	//! \brief Type of the diagonal rows iterator
	typedef RowIteratorDistCSRMatrix<this_type> loc_row_iterator_type;
	//! \brief Type of the constant diagonal cols iterator
	typedef ColIteratorDistCSRMatrix<this_type, const_loc_row_iterator_type, true> const_loc_col_iterator_type;
	//! \brief Type of the diagonal cols iterator
	typedef ColIteratorDistCSRMatrix<this_type, loc_row_iterator_type> loc_col_iterator_type;
	//! \brief Type of the constant non-diagonal rows iterator
	typedef RowIteratorDistCSRMatrix<this_type, true, true> const_glob_row_iterator_type;
	//! \brief Type of the non-diagonal rows iterator
	typedef RowIteratorDistCSRMatrix<this_type, false, true> glob_row_iterator_type;
	//! \brief Type of the constant non-diagonal cols iterator
	typedef ColIteratorDistCSRMatrix<this_type, const_glob_row_iterator_type, true> const_glob_col_iterator_type;
	//! \brief Type of the non-diagonal cols iterator
	typedef ColIteratorDistCSRMatrix<this_type, glob_row_iterator_type> glob_col_iterator_type;
	//@}
 private:
	//! \brief Diagonal datas of the matrix
	data_type* m_diag_data;
	//! \brief Non diagonal datas of the matrix
	data_type* m_ndiag_data;
	//! \brief Total number of local rows
	int m_nb_rows;
	//! \brief Number of local diagonal rows
	int m_diag_nb_rows;
	//! \brief Number of local non diagonal rows
	int m_ndiag_nb_rows;
	//! \brief Global number of rows
	int m_global_nb_rows;
	//! \brief Total number of local columns
	int m_nb_cols;
	//! \brief Number of local diagonal cols
	int m_diag_nb_cols;
	//! \brief Number of local non diagonal cols
	int m_ndiag_nb_cols;
	//! \brief Global number of cols
	int m_global_nb_cols;
	//! \brief Total number of local non zero elements
	int m_nb_nnz;
	//! \brief Number of local diagonal non zero elements
	int m_nb_diag_nnz;
	//! \brief Number of local non diagonal non zero elements
	int m_nb_ndiag_nnz;
	//! \brief Diagonal row pointer
	int* m_diag_rows_ptr;
	//! \brief Non diagonal row pointer
	int* m_ndiag_rows_ptr;
	//! \brief Diagonal col pointer
	int* m_diag_cols_ptr;
	//! \brief Non diagonal col pointer
	int* m_ndiag_cols_ptr;
	//! \brief Mapping local/global ID of rows/cols
	int* m_mapping;
	//! \brief Mapping local non diagonal/local ID of rows/cols
	int* m_ndiag_mapping;
	//! \brief Graph of the matrix
	graph_type* m_matrix_graph;
	//! \brief Attribute always valued to zero
	data_type m_zero;
	//! \brief Flag indicating if the row and col pointers are allocated but not filled
	bool m_is_allocated;
	//! \brief Flag indicating if the row and col pointers are allocated and filled
	bool m_is_init;
	//! \brief Flag indicating if the matrix is ready to be used (data sorted)
	bool m_is_ready;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class DistCSRMKLMatrix
	 */
	//@{
	
	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Set all attributes to zero/false.
	 */
	explicit DistCSRMKLMatrix()
	{
		m_diag_data = 0;
		m_ndiag_data = 0;
		m_diag_nb_rows = 0;
		m_diag_nb_cols = 0;
		m_ndiag_nb_rows = 0;
		m_ndiag_nb_cols = 0;
		m_nb_nnz = 0;
		m_diag_rows_ptr = 0;
		m_ndiag_rows_ptr = 0;
		m_diag_cols_ptr = 0;
		m_ndiag_cols_ptr = 0;
		m_global_nb_rows = 0;
		m_global_nb_cols = 0;
		m_mapping = 0;
		m_matrix_graph = 0;
		m_is_init = false;
		m_is_allocated = false;
		m_is_ready = false;
		m_zero = 0;
	}

	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  \param[in] _matrix Matrix to copy
	 *  
	 *  \todo Implement this constructor
	 */
	explicit DistCSRMKLMatrix(const DistCSRMKLMatrix& _matrix)
	{
		m_diag_data = 0;
		m_ndiag_data = 0;
		m_diag_nb_rows = 0;
		m_diag_nb_cols = 0;
		m_ndiag_nb_rows = 0;
		m_ndiag_nb_cols = 0;
		m_nb_nnz = 0;
		m_diag_rows_ptr = 0;
		m_ndiag_rows_ptr = 0;
		m_diag_cols_ptr = 0;
		m_ndiag_cols_ptr = 0;
		m_global_nb_rows = 0;
		m_global_nb_cols = 0;
		m_mapping = 0;
		m_matrix_graph = 0;
		m_is_init = false;
		m_is_allocated = false;
		m_is_ready = false;
		m_zero = 0;
	}

	/*!
	 *  \brief Dimension and mapping constructor
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _nbNnz, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows and m_global_nb_cols. \n
	 *  Copy the values of_mapping in m_mapping and set all other pointers value and the graph to zero. \n
	 *  Set the m_is_allocated to true (pointers are allocated), m_is_init to false (pointers not filled) and m_is_ready to false (datas not sorted).
	 *
	 *  \exception AllocationFailed The allocation of m_mapping failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 */
	explicit DistCSRMKLMatrix(const int _rows, const int _cols, const int _nbNnz, const int _globalRows, const int _globalCols, const int* _mapping)
	{
		m_nb_rows = _rows;
		m_nb_cols = _cols;
		m_nb_nnz = _nbNnz;
		m_global_nb_rows = _globalRows;
		m_global_nb_cols = _globalCols;
		this->allocateMapping(m_nb_cols);

		for(int i=0;i<m_nb_cols;++i)
			m_mapping[i] = _mapping[i];

		m_diag_rows_ptr = 0;
		m_ndiag_rows_ptr = 0;
		m_diag_cols_ptr = 0;
		m_ndiag_cols_ptr = 0;
		m_diag_data = 0;
		m_ndiag_data = 0;
		m_ndiag_mapping = 0;
		m_matrix_graph = 0;
		m_is_allocated = true;
		m_is_init = false;
		m_is_ready = false;
		m_zero = 0;
	}

	/*!
	 *  \brief Dimension and mapping constructor with data structure
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _nbNnz, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows and m_global_nb_cols. \n
	 *  Call the method countElements to count and compute the elements of the different blocks. \n
	 *  Call the method allocateDiagRowPtr, allocateNDiagRowPtr, allocateDiagColPtr, allocateNDiagColPtr, allocateDiagDatas, allocateNDiagDatas, allocateMapping and allocateNDiagMapping to allocate the different pointers of the data structure. \n
	 *
	 *  Copy the values of_mapping in m_mapping, \n
	 *
	 *  Call the method fillPointer to fill m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr and m_ndiag_cols_ptr.
	 *
	 *  Allocate the data pointer m_diag_data and m_ndiag_data and fill them with zeroes. \n
	 *
	 *  The matrix graph is created and stored in m_matrix_graph (same format than DistCSRMatrix). \n
	 *  Set the m_is_allocated to true (pointers are allocated), m_is_init to true (pointers filled) and m_is_ready to true (matrix sorted). It is the responsibility of the user to give the data pointers in a consistentmanner.
	 *
	 *  \exception AllocationFailed The allocation of m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr, m_ndiag_cols_ptr, m_diag_data, m_ndiag_data or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 */
	explicit DistCSRMKLMatrix(const int _rows, const int _cols, const int _nbNnz, const int _globalRows, const int _globalCols, const int* _mapping, const int* _rowPtr, const int* _colPtr)
	{
		m_nb_rows = _rows;
		m_nb_cols = _cols;
		m_nb_nnz = _nbNnz;
		m_global_nb_rows = _globalRows;
		m_global_nb_cols = _globalCols;
		// This method counts and set the values of m_n/diag_nb_rows, m_n/diag_nb_cols, m_nb_n/diag_nnz
		this->countElements(_rows, _cols, _nbNnz, _rowPtr,_colPtr);
		// We can now allocate all pointers
		this->allocateDiagRowPtr(m_diag_nb_rows+1);
		this->allocateNDiagRowPtr(m_ndiag_nb_rows+1);
		this->allocateDiagColPtr(m_nb_diag_nnz);
		this->allocateNDiagColPtr(m_nb_ndiag_nnz);
		this->allocateDiagDatas(m_nb_diag_nnz);
		this->allocateNDiagDatas(m_nb_ndiag_nnz);
		this->allocateMapping(m_nb_cols);
		this->allocateNDiagMapping(m_ndiag_nb_rows);

		for(int i=0;i<m_nb_cols;++i)
			m_mapping[i] = _mapping[i];

		for(int i=0;i<m_ndiag_nb_rows;++i)
			m_ndiag_mapping[i] = 0;

		for(int i=0;i<m_diag_nb_rows+1;++i)
			m_diag_rows_ptr[i] = 0;

		for(int i=0;i<m_ndiag_nb_rows+1;++i)
			m_ndiag_rows_ptr[i] = 0;

		for(int i=0;i<m_nb_diag_nnz;++i)
		{
			m_diag_data[i] = 0;
			m_diag_cols_ptr[i] = 0;
		}
		for(int i=0;i<m_nb_ndiag_nnz;++i)
		{
			m_ndiag_data[i] = 0;
			m_ndiag_cols_ptr[i] = 0;
		}

		// Time to fill the pointers (the zero passed as the last parameter means that no datas are to be copied).
		this->fillPointers(_rowPtr, _colPtr, 0);

		m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows, m_global_nb_cols, m_mapping, const_cast<int*>(_rowPtr), const_cast<int*>(_colPtr), true);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
		m_is_allocated = true;
		m_is_init = true;
		m_is_ready = true;
		m_zero = 0;
	}

	/*!
	 *  \brief Dimension and mapping constructor with data structure and data pointer
	 *
	 *  \details
	 *  Copy the value of _rows, _cols, _nbNnz, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows and m_global_nb_cols. \n
	 *  Call the method countElements to count and compute the elements of the different blocks. \n
	 *  Call the method allocateDiagRowPtr, allocateNDiagRowPtr, allocateDiagColPtr, allocateNDiagColPtr, allocateDiagDatas, allocateNDiagDatas, allocateMapping and allocateNDiagMapping to allocate the different pointers of the data structure. \n
	 *
	 *  Copy the values of_mapping in m_mapping, \n
	 *
	 *  Call the method fillPointer to fill m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr, m_ndiag_cols_ptr, m_diag_data and m_ndiag_data. 
	 *
	 *  The matrix graph is created and stored in m_matrix_graph (same format than DistCSRMatrix). \n
	 *  Set the m_is_allocated to true (pointers are allocated), m_is_init to true (pointers filled) and m_is_ready to true (matrix sorted).
	 *
	 *  \exception AllocationFailed The allocation of m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr, m_ndiag_cols_ptr, m_diag_data, m_ndiag_data or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 *  \param[in] _dataPtr Data pointer
	 */
	explicit DistCSRMKLMatrix(const int _rows, const int _cols, const int _nbNnz, const int _globalRows, const int _globalCols, const int* _mapping, const int* _rowPtr, const int* _colPtr, const data_type * _dataPtr)
	{
		m_nb_rows = _rows;
		m_nb_cols = _cols;
		m_nb_nnz = _nbNnz;
		// This method counts and set the values of m_n/diag_nb_rows, m_n/diag_nb_cols, m_nb_n/diag_nnz
		this->countElements(_rows, _cols, _nbNnz, _rowPtr,_colPtr);
		m_global_nb_rows = _globalRows;
		m_global_nb_cols = _globalCols;
		// We can now allocate all pointers
		this->allocateDiagRowPtr(m_diag_nb_rows+1);
		this->allocateNDiagRowPtr(m_ndiag_nb_rows+1);
		this->allocateDiagColPtr(m_nb_diag_nnz);
		this->allocateNDiagColPtr(m_nb_ndiag_nnz);
		this->allocateDiagDatas(m_nb_diag_nnz);
		this->allocateNDiagDatas(m_nb_ndiag_nnz);
		this->allocateMapping(m_nb_cols);
		this->allocateNDiagMapping(m_ndiag_nb_rows);

		for(int i=0;i<m_nb_cols;++i)
			m_mapping[i] = _mapping[i];

		for(int i=0;i<m_ndiag_nb_rows;++i)
			m_ndiag_mapping[i] = 0;

		for(int i=0;i<m_diag_nb_rows+1;++i)
			m_diag_rows_ptr[i] = 0;

		for(int i=0;i<m_ndiag_nb_rows+1;++i)
			m_ndiag_rows_ptr[i] = 0;

		for(int i=0;i<m_nb_diag_nnz;++i)
		{
			m_diag_data[i] = 0;
			m_diag_cols_ptr[i] = 0;
		}

		for(int i=0;i<m_nb_ndiag_nnz;++i)
		{
			m_ndiag_data[i] = 0;
			m_ndiag_cols_ptr[i] = 0;
		}

		// Time to fill the pointers
		this->fillPointers(_rowPtr, _colPtr, _dataPtr);

		m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows, m_global_nb_cols, m_mapping, const_cast<int*>(_rowPtr), const_cast<int*>(_colPtr), true);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
		m_is_allocated = true;
		m_is_ready = true;
		m_is_init = true;
		m_zero = 0;
	}

	/*!
	 *  \brief Graph constructor
	 *
	 *  \details
	 *  Copy the value of matrixGraph.getNbRows(), _matrixGraph.getNbCols(), _matrixGraph.getNnz(), _matrixGraph.getGlobalNbRows() and _matrixGraph.getGlobalNbCols() in m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows and m_global_nb_cols. \n
	 *  Call the method countElements to count and compute the elements of the different blocks. \n
	 *  Call the method allocateDiagRowPtr, allocateNDiagRowPtr, allocateDiagColPtr, allocateNDiagColPtr, allocateDiagDatas, allocateNDiagDatas, allocateMapping and allocateNDiagMapping to allocate the different pointers of the data structure. \n
	 *
	 *  Copy the values of_mapping in m_mapping, \n
	 *
	 *  Call the method fillPointer to fill m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr and m_ndiag_cols_ptr.
	 *
	 *  Allocate the data pointer m_diag_data and m_ndiag_data and fill them with zeroes. \n
	 *
	 *  The matrix graph is created and stored in m_matrix_graph (same format than DistCSRMatrix). \n
	 *  Set the m_is_allocated to true (pointers are allocated), m_is_init to true (pointers filled) and m_is_ready to true (matrix sorted). It is the responsibility of the user to give the data pointers in a consistentmanner.
	 *
	 *  \exception AllocationFailed The allocation of m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr, m_ndiag_cols_ptr, m_diag_data, m_ndiag_data or m_matrix_graph failed
	 *
	 *  \param[in] _matrixGraph The matrix graph
	 */
	explicit DistCSRMKLMatrix(const graph_type& _matrixGraph)
	{
		m_nb_rows = _matrixGraph.getNbRows();
		m_nb_cols = _matrixGraph.getNbCols();
		m_nb_nnz = _matrixGraph.getNnz();
		m_global_nb_rows = _matrixGraph.getGlobalNbRows();
		m_global_nb_cols = _matrixGraph.getGlobalNbCols();
		// This method counts and set the values of m_n/diag_nb_rows, m_n/diag_nb_cols, m_nb_n/diag_nnz
		this->countElements(m_nb_rows, m_nb_cols, m_nb_nnz, _matrixGraph.getRowPtr(), _matrixGraph.getColPtr());
		// We can now allocate all pointers
		this->allocateDiagRowPtr(m_diag_nb_rows+1);
		this->allocateNDiagRowPtr(m_ndiag_nb_rows+1);
		this->allocateDiagColPtr(m_nb_diag_nnz);
		this->allocateNDiagColPtr(m_nb_ndiag_nnz);
		this->allocateDiagDatas(m_nb_diag_nnz);
		this->allocateNDiagDatas(m_nb_ndiag_nnz);
		this->allocateMapping(m_nb_cols);
		this->allocateNDiagMapping(m_ndiag_nb_rows);

		for(int i=0;i<m_nb_cols;++i)
			m_mapping[i] = _matrixGraph.getMapping(i);

		for(int i=0;i<m_ndiag_nb_rows;++i)
			m_ndiag_mapping[i] = 0;

		for(int i=0;i<m_diag_nb_rows+1;++i)
			m_diag_rows_ptr[i] = 0;

		for(int i=0;i<m_ndiag_nb_rows+1;++i)
			m_ndiag_rows_ptr[i] = 0;

		for(int i=0;i<m_nb_diag_nnz;++i)
		{
			m_diag_data[i] = 0;
			m_diag_cols_ptr[i] = 0;
		}
		for(int i=0;i<m_nb_ndiag_nnz;++i)
		{
			m_ndiag_data[i] = 0;
			m_ndiag_cols_ptr[i] = 0;
		}

		// Time to fill the pointers
		this->fillPointers(_matrixGraph.getRowPtr(), _matrixGraph.getColPtr(), 0);

		m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows, m_global_nb_cols, m_mapping, const_cast<int*>(_matrixGraph.getRowPtr()), const_cast<int*>(_matrixGraph.getColPtr()), true);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
		m_is_allocated = true;
		m_is_ready = true;
		m_is_init = true;
		m_zero = 0;
	}
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class DistCSRMKLMatrix
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Call the method deallocate that free the memory allocated by the class
	 */
	virtual ~DistCSRMKLMatrix()
	{
		this->deallocate();
	}
	//@}


	/*! \name Public Getter
	 *
	 *  Public Getter of the class DistCSRMKLMatrix. \n
	 *  These methods give access in read-only mode to the datas/information of the class DistCSRMKLMatrix
	 */

	//@{
	/*! 
	 *  \brief Return the number of local rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of rows in the matrix
	 */
	int getNbRows() const
	{
		return m_nb_rows;
	}

	/*! 
	 *  \brief Return the number of local rows in the diagonal part of the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of rows in the diagonal part of the matrix
	 */
	int getDiagNbRows() const
	{
		return m_diag_nb_rows;
	}

	/*! 
	 *  \brief Return the number of local rows in the non diagonal part of the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of rows in the non diagonal part of the matrix
	 */
	int getNDiagNbRows() const
	{
		return m_ndiag_nb_rows;
	}

	/*! 
	 *  \brief Return the number of global rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of global rows in the matrix
	 */
	int getGlobalNbRows() const
	{
		return m_global_nb_rows;
	}

	/*! 
	 *  \brief Return the number of local cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of local cols in the matrix
	 */
	int getNbCols() const
	{
		return m_nb_cols;
	}

	/*! 
	 *  \brief Return the number of local cols in the diagonal part of the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of cols in the diagonal part of the matrix
	 */
	int getDiagNbCols() const
	{
		return m_diag_nb_cols;
	}

	/*! 
	 *  \brief Return the number of local cols in the non diagonal part of the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of cols in the non diagonal part of the matrix
	 */
	int getNDiagNbCols() const
	{
		return m_ndiag_nb_cols;
	}

	/*! 
	 *  \brief Return the number of global cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of global cols in the matrix
	 */
	int getGlobalNbCols() const
	{
		return m_global_nb_cols;
	}

	/*! 
	 *  \brief Return the number of local non zero elements in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of local non zero elements in the matrix
	 */	
	int getNnz() const
	{
		return m_nb_nnz;
	}

	/*! 
	 *  \brief Return the number of non zero elements in the diagonal part of the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of non zero elements in the diagonal part of the matrix
	 */	
	int getDiagNnz() const
	{
		return m_nb_diag_nnz;
	}

	/*! 
	 *  \brief Return the number of non zero elements in the non diagonal part of the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of non zero elements in the non diagonal part of the matrix
	 */	
	int getNDiagNnz() const
	{
		return m_nb_ndiag_nnz;
	}

	/*! 
	 *  \brief Return the number of non zero elements on a diagonal row
	 *
	 *  \details
	 *
	 *  \param[in] _row The row we want to know the nnz
	 *
	 *  \return The number of non zero elements on the specified diagonal row
	 *
	 *  \exception PtrNotAllocated m_rows_ptr is not allocated
	 *  \exception isOutOfRange _row is out of range
	 */	
	int getDiagRowNnz(const int _row) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_diag_rows_ptr);
		this->checkBounds(_row,0,m_diag_nb_rows);
#endif
		return m_diag_rows_ptr[_row+1] - m_diag_rows_ptr[_row];
	}

	/*! 
	 *  \brief Return the number of non zero elements on a non diagonal row
	 *
	 *  \details
	 *
	 *  \param[in] _row The row we want to know the nnz
	 *
	 *  \return The number of non zero elements on the specified non diagonal row
	 *
	 *  \exception PtrNotAllocated m_rows_ptr is not allocated
	 *  \exception isOutOfRange _row is out of range
	 */	
	int getNDiagRowNnz(const int _row) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_ndiag_rows_ptr);
		this->checkBounds(_row,0,m_ndiag_nb_rows+1);
#endif
		return m_ndiag_rows_ptr[_row+1] - m_ndiag_rows_ptr[_row];
	}

	/*! 
	 *  \brief Return the global ID of a diagonal row/column
	 *
	 *  \details
	 *
	 *  \param[in] _index The local ID of the diagonal row/col
	 *
	 *  \return The global ID of the local diagonal row/col
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	int getMapping(const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_mapping);
		this->checkBounds(_index,0,m_nb_cols);
#endif
		return m_mapping[_index];
	}

	/*! 
	 *  \brief Return the local ID of a non diagonal row/column
	 *
	 *  \details
	 *
	 *  \param[in] _index The local ID (in the non diagonal block) of the non diagonal row/col
	 *
	 *  \return The local ID (in the diagonal block) of the local non diagonal row/col
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	int getNDiagMapping(const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_ndiag_mapping);
		this->checkBounds(_index,0,m_ndiag_nb_rows);
#endif
		return m_ndiag_mapping[_index];
	}

	/*! 
	 *  \brief Return the global diagonal mapping.
	 *
	 *  \details
	 *
	 *  \return The local/global ID mapping array
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 */
	const int* getMapping() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_mapping);
#endif
		return m_mapping;
	}

	/*! 
	 *  \brief Return the local non diagonal mapping.
	 *
	 *  \details
	 *
	 *  \return The non diagona local/diagonal local ID mapping array
	 *
	 *  \exception PtrNotAllocated m_mapping is not allocated
	 */
	const int* getNDiagMapping() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_ndiag_mapping);
#endif
		return m_ndiag_mapping;
	}

	/*! 
	 *  \brief Return a pointeur on the diagonal datas of the matrix.
	 *
	 *  \details
	 *
	 *  \return The diagonal data pointer of the matrix
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 */
	const data_type* getDiagPointeur() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_diag_data);
#endif
		return m_diag_data;
	}

	/*! 
	 *  \brief Return a pointeur on the non diagonal datas of the matrix.
	 *
	 *  \details
	 *
	 *  \return The non diagonal data pointer of the matrix
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 */
	const data_type* getNDiagPointeur() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_ndiag_data);
#endif
		return m_ndiag_data;
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *  Check if the data lies on the diagonal or non diagonal block. It the data is found, then returns the value, otherwise returns zero.
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 *
	 *  \exception PtrNotAllocated m_diag_rows_ptr, m_diag_cols_ptr, m_ndiag_rows_ptr, m_ndiag_cols_ptr, m_diag_data or m_ndiag_data is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	data_type operator()(const int _row, const int _col) const
	{
#ifdef _DEBUG
		this->checkBounds(_row,0,m_nb_rows);
		this->checkBounds(_col,0,m_nb_cols);
		this->checkAllocated(m_diag_rows_ptr);
		this->checkAllocated(m_diag_cols_ptr);
		this->checkAllocated(m_ndiag_rows_ptr);
		this->checkAllocated(m_ndiag_cols_ptr);
		this->checkAllocated(m_diag_data);
		this->checkAllocated(m_ndiag_data);
#endif
		if(_col<m_nb_rows)
		{
			for(int i=m_diag_rows_ptr[_row];i<m_diag_rows_ptr[_row+1];++i)
			{
				if(m_diag_cols_ptr[i]==_col)
					return m_diag_data[i];
			}
		}
		else
		{
			for(int i=0;i<m_ndiag_nb_rows;++i)
			{
				if(_row==m_ndiag_mapping[i])
				{
					for(int j=m_ndiag_rows_ptr[i];j<m_ndiag_rows_ptr[i+1];++j)
					{
						if(m_ndiag_cols_ptr[j]==_col)
							return m_ndiag_data[i];
					}
				}
			}
		}
		return 0;
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *  It is assumed here that diagonal datas are stored first, then on diagonal datas. Therefore, if the _index is higher that the nnz elements in the diagonal block, the datas lies on the non diagonal block.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 *
	 *  \exception PtrNotAllocated m_diag_data or m_ndiag_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type operator[](const int _index) const
	{
#ifdef _DEBUG
		this->checkBounds(_index,0,m_nb_nnz);
		this->checkAllocated(m_diag_data);
		this->checkAllocated(m_ndiag_data);
#endif
		if(_index<m_nb_diag_nnz)
			return m_diag_data[_index];
		else
			return m_ndiag_data[_index-m_nb_diag_nnz];
	}

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "DistCSRMKLMatrix" The name of the class
	 */
	const char* getClassName() const
	{
		return "DistCSRMKLMatrix";
	}

	/*!
	 *  \brief Return the graph of the matrix. 
	 *
	 *  \details
	 *
	 *  \return The graph of the matrix
	 *
	 *  \exception PtrNotAllocated m_matrix_graph is not allocated
	 */
	const graph_type& getMatrixGraph() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_matrix_graph);
#endif
		return *m_matrix_graph;
	}

	/*! 
	 *  \brief Return the value of the diagonal row pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index we want to access the value
	 *
	 *  \return The value of the diagonal row pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_diag_rows_ptr_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getDiagRowPtr(const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_diag_rows_ptr);
		this->checkBounds(_index,0,m_diag_nb_rows+1);
#endif
		return m_diag_rows_ptr[_index];
	}

	/*! 
	 *  \brief Return the value of the non diagonal row pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index we want to access the value
	 *
	 *  \return The value of the non diagonal row pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_ndiag_rows_ptr_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getNDiagRowPtr(const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_ndiag_rows_ptr);
		this->checkBounds(_index,0,m_ndiag_nb_rows+1);
#endif
		return m_ndiag_rows_ptr[_index];
	}

	/*! 
	 *  \brief Return the diagonal row pointer
	 *
	 *  \details
	 *
	 *  \return The diagonal row pointer
	 *
	 *  \exception PtrNotAllocated m_diag_rows_ptr_ptr is not allocated
	 */	
	const int* getDiagRowPtr() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_diag_rows_ptr);
#endif
		return m_diag_rows_ptr;
	}

	/*! 
	 *  \brief Return the non diagonal row pointer
	 *
	 *  \details
	 *
	 *  \return The non diagonal row pointer
	 *
	 *  \exception PtrNotAllocated m_ndiag_rows_ptr_ptr is not allocated
	 */	
	const int* getNDiagRowPtr() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_ndiag_rows_ptr);
#endif
		return m_ndiag_rows_ptr;
	}

	/*! 
	 *  \brief Return the value of the diagonal col pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index the we want to access the value
	 *
	 *  \return The value of the diagonal col pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_diag_cols_ptr_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getDiagColPtr(const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_diag_cols_ptr);
		this->checkBounds(_index,0,m_nb_diag_nnz);
#endif
		return m_diag_cols_ptr[_index];
	}

	/*! 
	 *  \brief Return the value of the non diagonal col pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index the we want to access the value
	 *
	 *  \return The value of the non diagonal col pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_ndiag_cols_ptr_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getNDiagColPtr(const int _index) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_ndiag_cols_ptr);
		this->checkBounds(_index,0,m_nb_ndiag_nnz);
#endif
		return m_ndiag_cols_ptr[_index];
	}

	/*! 
	 *  \brief Return the diagonal col pointer
	 *
	 *  \details
	 *
	 *  \return The diagonal col pointer
	 *
	 *  \exception PtrNotAllocated m_diag_cols_ptr_ptr is not allocated
	 */	
	const int* getDiagColPtr() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_diag_cols_ptr);
#endif
		return m_diag_cols_ptr;
	}

	/*! 
	 *  \brief Return the non diagonal col pointer
	 *
	 *  \details
	 *
	 *  \return The non diagonal col pointer
	 *
	 *  \exception PtrNotAllocated m_ndiag_cols_ptr_ptr is not allocated
	 */	
	const int* getNDiagColPtr() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_ndiag_cols_ptr);
#endif
		return m_ndiag_cols_ptr;
	}

	/*! 
	 *  \brief Check if the entry exists in the matrix
	 *
	 *  \details
	 *  Return 1 if the entry exists i.e. there is a nnz at this index, 0 otherwise. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return 1 if an entry exists in the matrix at the index (_row,_col) , 0 otherwise
	 *
	 *  \exception PtrNotAllocated m_matrix_graph is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	int getEntry(const int _row, const int _col) const
	{
#ifdef _DEBUG
	  this->checkAllocated(m_matrix_graph);
	  this->checkBounds(_row,0,m_nb_rows);
	  this->checkBounds(_col,0,m_nb_cols);
#endif
	  return (*m_matrix_graph)(_row,_col);
	}

	/*! 
	 *  \brief Check if the matrix is initialized
	 *  
	 *  \details 
	 *  Return true if the matrix pointers are allocated and filled, false otherwise. \n
	 *
	 *  \return The state of the initialization flag
	 *
	 *  \warning Most probably not working so don't rely on it (unless you fix it)
	 */
	bool isInitialized() const
	{
		return m_is_init;
	}

	/*! 
	 *  \brief Check if the matrix is allocated
	 *  
	 *  \details 
	 *  Return true if the matrix pointers are allocated, false otherwise. \n
	 *
	 *  \return The state of the allocation flag
	 *
	 *  \warning Most probably not working so don't rely on it (unless you fix it)
	 */
	bool isAllocated() const
	{
		return m_is_allocated;
	}

	/*! 
	 *  \brief Check if the matrix is sorted
	 *  
	 *  \details 
	 *  Return true if the matrix is ready to use, false otherwise. \n
	 *
	 *  \return The state of the m_is_ready flag
	 *
	 *  \warning Most probably not working so don't rely on it (unless you fix it)
	 */
	bool isReady() const
	{
		return m_is_ready;
	}
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class DistCSRMKLMatrix. \n
	 *  These methods give access in read/write mode to the datas/information of the class DistCSRMKLMatrix
	 */
	//@{
	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *  Check if the data lies on the diagonal or non diagonal block. It the data is found, then returns the value, otherwise returns zero.
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 *
	 *  \exception PtrNotAllocated m_diag_rows_ptr, m_diag_cols_ptr, m_ndiag_rows_ptr, m_ndiag_cols_ptr m_diag_data or m_ndiag_data is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	data_type& operator()(const int _row, const int _col)
	{
#ifdef _DEBUG
		this->checkBounds(_row,0,m_diag_nb_rows);
		this->checkBounds(_col,0,m_diag_nb_cols);
		this->checkAllocated(m_diag_rows_ptr);
		this->checkAllocated(m_ndiag_rows_ptr);
		this->checkAllocated(m_diag_cols_ptr);
		this->checkAllocated(m_ndiag_cols_ptr);
		this->checkAllocated(m_diag_data);
		this->checkAllocated(m_ndiag_data);
		if(m_is_allocated && m_is_ready && m_is_init)
		{
#endif
			if(_col<m_nb_rows)
			{
				for(int i=m_diag_rows_ptr[_row];i<m_diag_rows_ptr[_row+1];++i)
				{
					if(m_diag_cols_ptr[i]==_col)
						return m_diag_data[i];
				}
			}
			else
			{
				for(int i=0;i<m_ndiag_nb_rows;++i)
				{
					if(_row==m_ndiag_mapping[i])
					{
						for(int j=m_ndiag_rows_ptr[i];j<m_ndiag_rows_ptr[i+1];++j)
						{
							if(m_ndiag_cols_ptr[j]==_col)
								return m_ndiag_data[i];
						}
					}
				}
			}
			m_zero = 0;
			return m_zero;
#ifdef _DEBUG
		}
#endif
	}

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *  It is assumed here that diagonal datas are stored first, then on diagonal datas. Therefore, if the _index is higher that the nnz elements in the diagonal block, the datas lies on the non diagonal block.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 *
	 *  \exception PtrNotAllocated m_diag_data or m_ndiag_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type& operator[](const int _index)
	{
#ifdef _DEBUG
		this->checkBounds(_index,0,m_nb_nnz);
		this->checkAllocated(m_diag_data);
		this->checkAllocated(m_ndiag_data);
#endif
		if(_index<m_nb_diag_nnz)
			return m_diag_data[_index];
		else
			return m_ndiag_data[_index-m_nb_diag_nnz];
	}

	/*!
	 *  \brief Set the value in the matrix located at the index (_row,_col) to _val while building the data structure pointers
	 *
	 *  \details
	 *  The purpose of this method is to allow the user to set up the data structure of a sparse matrix while filling it. \n
	 *  If the data structure pointers have been allocated but not filled, this method is used to build the data structure pointers while filling the matrix. \n
	 *  The matrix needs to be allocated, but not initialized.
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *  \param[in] _val The value to set at the index (_row,_col)
	 *
	 *  \warning This method might be convenient but is clearly inefficient
	 *
	 *  \exception PtrNotAllocated m_rows_ptr, m_cols_ptr or m_data is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 *
	 *  \todo Implement this method
	 */
	void setValue(const int _row, const int _col, const data_type _val)
	{
		// TODO
	}
	//@}

	/*! \name Public Iterators
	 *
	 *  Public iterators of the class DistCSRMKLMatrix. \n
	 *  These methods returns differents iterators on the matrix elements
	 */

	/*!
	 *  \brief Return a const iterator on the first non-diagonal row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the first non-diagonal row of the matrix
	 */
	const_glob_row_iterator_type rowBegin() const
	{
		return const_glob_row_iterator_type(&m_ndiag_data[0], 0, *this, m_ndiag_rows_ptr, m_ndiag_cols_ptr, m_ndiag_mapping, m_ndiag_data);
	}

	/*!
	 *  \brief Return an iterator on the first non-diagonal row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the first non-diagonal row of the matrix
	 */
	glob_row_iterator_type rowBegin()
	{
		return glob_row_iterator_type(&m_ndiag_data[0], 0, *this, m_ndiag_rows_ptr, m_ndiag_cols_ptr, m_ndiag_mapping, m_ndiag_data);
	}

	/*!
	 *  \brief Return a const iterator on the last non-diagonal row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the last non-diagonal row of the matrix
	 */
	const_glob_row_iterator_type rowEnd() const
	{
		return const_glob_row_iterator_type(&m_ndiag_data[m_ndiag_rows_ptr[m_ndiag_nb_rows]], m_ndiag_nb_rows-1, *this, m_ndiag_rows_ptr, m_ndiag_cols_ptr, m_ndiag_mapping, m_ndiag_data);
	}

	/*!
	 *  \brief Return an iterator on the last non-diagonal row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the last non-diagonal row of the matrix
	 */
	glob_row_iterator_type rowEnd()
	{
		return glob_row_iterator_type(&m_ndiag_data[m_ndiag_rows_ptr[m_ndiag_nb_rows]], m_ndiag_nb_rows-1, *this, m_ndiag_rows_ptr, m_ndiag_cols_ptr, m_ndiag_mapping, m_ndiag_data);
	}

	/*!
	 *  \brief Return a const iterator on the first non-diagonal col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The first element on the specified non-diagonal row of the matrix
	 */
	const_glob_col_iterator_type colBegin(const const_glob_row_iterator_type& _rowIterator) const
	{
		return const_glob_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
	}

	/*!
	 *  \brief Return an iterator on the first non-diagonal col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \return The first element on the specified non-diagonal row of the matrix
	 */
	glob_col_iterator_type colBegin(const glob_row_iterator_type& _rowIterator)
	{
		return glob_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
	}

	/*!
	 *  \brief Return a const iterator on the last non-diagonal col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the specified non-diagonal row of the matrix
	 */
	const_glob_col_iterator_type colEnd(const const_glob_row_iterator_type& _rowIterator) const
	{
		return const_glob_col_iterator_type(_rowIterator.getPosition()+this->getNDiagRowNnz(_rowIterator.realIndex()), _rowIterator);
	}

	/*!
	 *  \brief Return an iterator on the last non-diagonal col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the specified non-diagonal row of the matrix
	 */
	glob_col_iterator_type colEnd(const glob_row_iterator_type& _rowIterator)
	{
		return glob_col_iterator_type(_rowIterator.getPosition()+this->getNDiagRowNnz(_rowIterator.realIndex()), _rowIterator);
	}

	/*!
	 *  \brief Return a const iterator on the diagonal element of a row
	 *
	 *  \details
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the diagonal element
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	const_loc_col_iterator_type diagonalElement(const const_loc_row_iterator_type& _rowIterator) const
	{
		return const_loc_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
	}

	/*!
	 *  \brief Return an iterator on the diagonal element of a row
	 *
	 *  \details
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	loc_col_iterator_type diagonalElement(const loc_row_iterator_type& _rowIterator)
	{
		return loc_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
	}

	/*!
	 *  \brief Return a const iterator on the first diagonal row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the first diagonal row of the matrix
	 */
	const_loc_row_iterator_type ownRowBegin() const
	{
		return const_loc_row_iterator_type(&m_diag_data[0], 0, *this, m_diag_rows_ptr, m_diag_cols_ptr, m_mapping, m_diag_data);
	}

	/*!
	 *  \brief Return an iterator on the first diagonal row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the first diagonal row of the matrix
	 */
	loc_row_iterator_type ownRowBegin()
	{
		return loc_row_iterator_type(&m_diag_data[0], 0, *this, m_diag_rows_ptr, m_diag_cols_ptr, m_mapping, m_diag_data);
	}

	/*!
	 *  \brief Return a const iterator on the last diagonal row of the matrix
	 *
	 *  \details
	 *  Call the method ownRowEnd of the chosen implementation.
	 *
	 *  \return The first element on the last diagonal row of the matrix
	 */
	const_loc_row_iterator_type ownRowEnd() const
	{
		return const_loc_row_iterator_type(&m_diag_data[m_diag_rows_ptr[m_diag_nb_rows]], m_diag_nb_rows-1, *this, m_diag_rows_ptr, m_diag_cols_ptr, m_mapping, m_diag_data);
	}

	/*!
	 *  \brief Return an iterator on the last diagonal row of the matrix
	 *
	 *  \details
	 *  Call the method ownRowEnd of the chosen implementation.
	 *
	 *  \return The first element on the last diagonal row of the matrix
	 */
	loc_row_iterator_type ownRowEnd()
	{
		return loc_row_iterator_type(&m_diag_data[m_diag_rows_ptr[m_diag_nb_rows]], m_diag_nb_rows-1, *this, m_diag_rows_ptr, m_diag_cols_ptr, m_mapping, m_diag_data);
	}

	/*!
	 *  \brief Return a const iterator on the first diagonal col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the first diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The first element on the first diagonal row of the matrix
	 */
	const_loc_col_iterator_type ownColBegin(const const_loc_row_iterator_type& _rowIterator) const
	{
		return const_loc_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
	}

	/*!
	 *  \brief Return an iterator on the first diagonal col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the first diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The first element on the first diagonal row of the matrix
	 */
	loc_col_iterator_type ownColBegin(const loc_row_iterator_type& _rowIterator)
	{
		return loc_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
	}

	/*!
	 *  \brief Return a const iterator on the last diagonal col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the last diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the first diagonal row of the matrix
	 */
	const_loc_col_iterator_type ownColEnd(const const_loc_row_iterator_type& _rowIterator) const
	{
		return const_loc_col_iterator_type(_rowIterator.getPosition()+this->getDiagRowNnz(_rowIterator.index()), _rowIterator);
	}

	/*!
	 *  \brief Return an iterator on the last diagonal col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the last diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the first diagonal row of the matrix
	 */
	loc_col_iterator_type ownColEnd(const loc_row_iterator_type& _rowIterator)
	{
		return loc_col_iterator_type(_rowIterator.getPosition()+this->getDiagRowNnz(_rowIterator.index()), _rowIterator);
	}

	/*!
	 *  \brief Return a const iterator on the specified row
	 *
	 *  \details
	 *
	 *  \param[in] _row The row we want to access
	 *
	 *  \return The first element on the specified row of the matrix
	 *  
	 *  \warning This is clearly not working. Probably need to duplicate this method to take into account non-diagonal rows also. Maybe this iterator should not exist, and should not allow users to access a random row in the matrix
	 */
	const_loc_row_iterator_type rowIterator(const int _row) const
	{
		return const_loc_row_iterator_type(&m_diag_data[m_diag_rows_ptr[_row]], _row, *this, m_diag_rows_ptr, m_diag_rows_ptr, m_diag_cols_ptr, 0, m_diag_data);
	}

	/*!
	 *  \brief Return an iterator on the specified row
	 *
	 *  \details
	 *
	 *  \param[in] _row The row we want to access
	 *
	 *  \return The first element on the specified row of the matrix
	 *  
	 *  \warning This is clearly not working. Probably need to duplicate this method to take into account non-diagonal rows also. Maybe this iterator should not exist, and should not allow users to access a random row in the matrix
	 */
	loc_row_iterator_type rowIterator(const int _row)
	{
		return loc_row_iterator_type(&m_diag_data[m_diag_rows_ptr[_row]], _row, *this, m_diag_rows_ptr, m_diag_rows_ptr, m_diag_cols_ptr, 0, m_diag_data);
	}
	//@}

	/*! \name Public Utils
	 *
	 *  Public Utils of the class DistCSRMKLMatrix. \n
	 *  These methods provide various utilities to the class DistCSRMKLMatrix
	 */
	//@{

	/*!
	 *  \brief Initialize a matrix with the parameters given as arguments
	 *
	 *  \details
	 *  Free the memory eventually allocated so far. \n
	 *  Copy the value of _rows, _cols, _nbNnz, _globalRows and _globalCols in m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows and m_global_nb_cols. \n
	 *  Call the method countElements to count and compute the elements of the different blocks. \n
	 *  Call the method allocateDiagRowPtr, allocateNDiagRowPtr, allocateDiagColPtr, allocateNDiagColPtr, allocateDiagDatas, allocateNDiagDatas, allocateMapping and allocateNDiagMapping to allocate the different pointers of the data structure. \n
	 *
	 *  Copy the values of_mapping in m_mapping, \n
	 *
	 *  Call the method fillPointer to fill m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr and m_ndiag_cols_ptr.
	 *
	 *  Allocate the data pointer m_diag_data and m_ndiag_data and fill them with zeroes. \n
	 *
	 *  The matrix graph is created and stored in m_matrix_graph (same format than DistCSRMatrix). \n
	 *  Set the m_is_allocated to true (pointers are allocated), m_is_init to true (pointers filled) and m_is_ready to true (matrix sorted). It is the responsibility of the user to give the data pointers in a consistentmanner.
	 *
	 *  \exception AllocationFailed The allocation of m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr, m_ndiag_cols_ptr, m_diag_data, m_ndiag_data or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _globalRows Number of global rows in the matrix
	 *  \param[in] _globalCols Number of global cols in the matrix
	 *  \param[in] _mapping Mapping local/global ID of rows/cols
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 */
	void initialize(const int _rows, const int _cols, const int _nbNnz, const int _globalRows, const int _globalCols, const int* _mapping, const int* _rowPtr, const int* _colPtr)
	{
		this->deallocate();
		m_nb_rows = _rows;
		m_nb_cols = _cols;
		m_nb_nnz = _nbNnz;
		// This method counts and set the values of m_n/diag_nb_rows, m_n/diag_nb_cols, m_nb_n/diag_nnz
		this->countElements(_rows, _cols, _nbNnz, _rowPtr,_colPtr);
		m_global_nb_rows = _globalRows;
		m_global_nb_cols = _globalCols;
		// We can now allocate all pointers
		this->allocateDiagRowPtr(m_diag_nb_rows+1);
		this->allocateNDiagRowPtr(m_ndiag_nb_rows+1);
		this->allocateDiagColPtr(m_nb_diag_nnz);
		this->allocateNDiagColPtr(m_nb_ndiag_nnz);
		this->allocateDiagDatas(m_nb_diag_nnz);
		this->allocateNDiagDatas(m_nb_ndiag_nnz);
		this->allocateMapping(m_nb_cols);
		this->allocateNDiagMapping(m_ndiag_nb_rows);

		for(int i=0;i<m_nb_cols;++i)
			m_mapping[i] = _mapping[i];

		for(int i=0;i<m_ndiag_nb_rows;++i)
			m_ndiag_mapping[i] = 0;

		for(int i=0;i<m_diag_nb_rows+1;++i)
			m_diag_rows_ptr[i] = 0;

		for(int i=0;i<m_ndiag_nb_rows+1;++i)
			m_ndiag_rows_ptr[i] = 0;

		for(int i=0;i<m_nb_diag_nnz;++i)
		{
			m_diag_data[i] = 0;
			m_diag_cols_ptr[i] = 0;
		}
		for(int i=0;i<m_nb_ndiag_nnz;++i)
		{
			m_ndiag_data[i] = 0;
			m_ndiag_cols_ptr[i] = 0;
		}

		// Time to fill the pointers
		this->fillPointers(_rowPtr, _colPtr, 0);

		m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_global_nb_rows, m_global_nb_cols, m_mapping, const_cast<int*>(_rowPtr), const_cast<int*>(_colPtr), true);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
		m_is_allocated = true;
		m_is_ready = true;
		m_is_init = true;
		m_zero = 0;
	}

	/*!
	 *  \brief Fill the matrix with a value.
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_diag_data or m_ndiag_data is not allocated
	 *
	 *  \param[in] _val The value to copy in the matrix
	 */
	void fill(const data_type _val)
	{
#ifdef _DEBUG
		this->checkAllocated(m_diag_data);
		this->checkAllocated(m_ndiag_data);
#endif
		for(int i=0;i<m_nb_diag_nnz;++i)
			m_diag_data[i] = _val;
		for(int i=0;i<m_nb_ndiag_nnz;++i)
			m_ndiag_data[i] = _val;
	}

	/*!
	 *  \brief Fill a matrix by copying the pointer passed in parameter.
	 *
	 *  \details
	 *
	 *  \exception PtrNotAllocated m_diag_data or m_ndiag_data is not allocated
	 *
	 *  \param[in] _src The datas to copy
	 *
	 *  \warning It is assumed here that diagonal datas are stored first, then non diagonal datas. Therefore, if the _index is higher that the nnz elements in the diagonal block, the datas lies on the non diagonal block.
	 */
	void fill(const data_type* _src)
	{
#ifdef _DEBUG
		this->checkAllocated(m_diag_data);
		this->checkAllocated(m_ndiag_data);
#endif
		for(int i=0;i<m_nb_diag_nnz;++i)
			m_diag_data[i] = _src[i];
		for(int i=0;i<m_nb_ndiag_nnz;++i)
			m_ndiag_data[i] = _src[i];
	}

	/*!
	 *  \brief Finalize the matrix
	 *  
	 *  \details
	 *  
	 *  \todo Implement this method
	 */
	void finalize()
	{
		// TODO
	}
	// @}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class DistCSRMKLMatrix. \n
	 *  These internal methods provides various utilities to the class DistCSRMKLMatrix
	 */
	//@{

	/*!
	 *  \brief Fill an allocated matrix with another matrix
	 *
	 *  \details
	 *
	 *  \tparam MatrixType The matrix type used to fill the data pointer
	 *
	 *  \param[in] _src The matrix to copy
	 * 
	 *  \todo Implement this method
	 */
	template<typename MatrixType>
    void fill(const MatrixType& _src)
	{
		// TODO
	}

	/*!
	 *  \brief Counts the number differents informations for this particular data structure format
	 *
	 *  \details
	 *  This method counts and set the value of: \n
	 *  - m_diag_nb_rows
	 *  - m_diag_nb_cols
	 *  - m_ndiag_nb_rows
	 *  - m_ndiag_nb_cols
	 *  - m_nb_diag_nnz
	 *  - m_nb_ndiag_nnz
	 *
	 *  The algorithm is pretty simple. If the local index of a column is higher than the number of rows+1, that means the elements is global. Otherwise, local. \n
	 *  To get rid of duplicates, a unordered_set. The set don't have to be ordered because we are just counting, not storing.
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 */
	void countElements(const int _rows, const int _cols, const int _nbNnz, const int* _rowPtr, const int* _colPtr)
	{
		int diagNnz = 0;
		int ndiagNnz = 0;

		std::unordered_set<int> diagUniqueRowSet;
		std::unordered_set<int> ndiagUniqueRowSet;
		std::unordered_set<int> diagUniqueColSet;
		std::unordered_set<int> ndiagUniqueColSet;
		const int ownColEnd = m_nb_rows-1;
		for(int i=0;i<m_nb_rows;++i)
		{
			for(int j=_rowPtr[i];j<_rowPtr[i+1];++j)
			{
				if(_colPtr[j]<=ownColEnd)
				{
					diagUniqueRowSet.insert(i);
					diagUniqueColSet.insert(_colPtr[j]);
					++diagNnz;
				}
				else //if(m_cols_ptr[j]>ownColEnd)
				{
					ndiagUniqueRowSet.insert(i);
					ndiagUniqueColSet.insert(_colPtr[j]);
					++ndiagNnz;
				}
			}
		}
		m_diag_nb_rows = diagUniqueRowSet.size();
		m_diag_nb_cols = diagUniqueColSet.size();
		m_ndiag_nb_rows = ndiagUniqueRowSet.size();
		m_ndiag_nb_cols = ndiagUniqueColSet.size();
		m_nb_diag_nnz = diagNnz;
		m_nb_ndiag_nnz = ndiagNnz;
	}

	/*!
	 *  \brief Fills the different pointers of the data structure
	 *
	 *  \details
	 *  This method set the value of: \n
	 *  - m_diag_rows_ptr
	 *  - m_ndiag_rows_ptr
	 *  - m_diag_cols_ptr
	 *  - m_ndiag_cols_ptr
	 *  - m_ndiag_mapping
	 *
	 *  The algorithm is the following: \n
	 *  For each row, we test if the column is diagonal or not (less than m_nb_rows)
	 *    - If the element is diagonal, we copy the values in the diagonal pointers, the diagonal element being the first
	 *    - If the element is non-diagonal, we copy the values in the non-dagonal pointers
	 *
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 *  \param[in] _dataPtr The data pointer
	 */
	void fillPointers(const int* _rowPtr, const int* _colPtr, const data_type* _dataPtr)
	{
		const int ownColEnd = m_nb_rows-1;
		int globalRowDone = 0;
		int nbGlobal = 0;
		for(int i=0;i<m_nb_rows;++i)
		{
			const int diagOffset = m_diag_rows_ptr[i];
			int nbLocal = 1;
			bool gRowDone = false;
			for(int j=_rowPtr[i];j<_rowPtr[i+1];++j)
			{
				if(_colPtr[j]<=ownColEnd && _colPtr[j]!=i) // local
				{
					m_diag_cols_ptr[diagOffset+nbLocal] = _colPtr[j];
					if(_dataPtr)
						m_diag_data[diagOffset+nbLocal] = _dataPtr[j];
					++m_diag_rows_ptr[i+1];
					++nbLocal;
				}
				else if(_colPtr[j]>ownColEnd) // global
				{
					if(!gRowDone)
					{
						m_ndiag_mapping[globalRowDone] = i; //m_mapping[i];
						++globalRowDone;
						gRowDone = true;
					}
					++m_ndiag_rows_ptr[globalRowDone];
					m_ndiag_cols_ptr[nbGlobal] = _colPtr[j];
					if(_dataPtr)
						m_ndiag_data[nbGlobal] = _dataPtr[j];
					++nbGlobal;
				}
				else //(m_cols_ptr[j]==i)
				{
					m_diag_cols_ptr[diagOffset] = _colPtr[j];
					if(_dataPtr)
						m_diag_data[diagOffset] = _dataPtr[j];
					++m_diag_rows_ptr[i+1];
				}
			}
			m_diag_rows_ptr[i+1] += m_diag_rows_ptr[i];
		}
		for(int i=0;i<m_ndiag_nb_rows;++i)
			m_ndiag_rows_ptr[i+1] += m_ndiag_rows_ptr[i];
	}
	//@}

	/*! \name Private Allocator
	 *
	 *  Private Allocator of the class DistCSRMKLMatrix. \n
	 *  These internal methods allow to allocate a DistCSRMKLMatrix in different cases
	 */
	//@{

 private:
	/*! 
	 *  \brief Deallocate the memory
	 *
	 *  \details
	 *  Free the memory allocated by the class
	 */
	void deallocate()
	{
		delete[] m_diag_data;
		m_diag_data = 0;
		delete[] m_ndiag_data;
		m_ndiag_data = 0;
		delete[] m_diag_rows_ptr;
		m_diag_rows_ptr = 0;
		delete[] m_ndiag_rows_ptr;
		m_ndiag_rows_ptr = 0;
		delete[] m_diag_cols_ptr;
		m_diag_cols_ptr = 0;
		delete[] m_ndiag_cols_ptr;
		m_ndiag_cols_ptr = 0;
		delete[] m_mapping;
		m_mapping = 0;
		delete[] m_ndiag_mapping;
		m_ndiag_mapping = 0;
		delete m_matrix_graph;
		m_matrix_graph = 0;
	}

	/*!
	 *  \brief Allocate the diagonal row pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the diagonal row pointer (should be m_nb_diag_rows+1)
	 *
	 *  \exception AllocationFailed Allocation of m_diag_rows_ptr failed
	 */
	void allocateDiagRowPtr(const int _size)
	{
		if(_size==0)
		{
			m_diag_rows_ptr = 0;
			return;
		}
		else
			m_diag_rows_ptr = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_diag_rows_ptr);
#endif
	}

	/*!
	 *  \brief Allocate the non diagonal row pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the non diagonal row pointer (should be m_nb_ndiag_rows+1)
	 *
	 *  \exception AllocationFailed Allocation of m_diag_rows_ptr failed
	 */
	void allocateNDiagRowPtr(const int _size)
	{
		if(_size==0)
		{
			m_ndiag_rows_ptr = 0;
			return;
		}
		else
			m_ndiag_rows_ptr = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_ndiag_rows_ptr);
#endif
	}

	/*!
	 *  \brief Allocate the diagonal col pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the diagonal col pointer (should be m_nb_diag_nnz)
	 *
	 *  \exception AllocationFailed Allocation of m_diag_cols_ptr failed
	 */
	void allocateDiagColPtr(const int _size)
	{
		if(_size==0)
		{
			m_diag_cols_ptr = 0;
			return ;
		}
		else
			m_diag_cols_ptr = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_diag_cols_ptr);
#endif
	}

	/*!
	 *  \brief Allocate the non diagonal col pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the non diagonal col pointer (should be m_nb_ndiag_nnz)
	 *
	 *  \exception AllocationFailed Allocation of m_ndiag_cols_ptr failed
	 */
	void allocateNDiagColPtr(const int _size)
	{
		if(_size==0)
		{
			m_ndiag_cols_ptr = 0;
			return ;
		}
		else
			m_ndiag_cols_ptr = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_ndiag_cols_ptr);
#endif
	}

	/*!
	 *  \brief Allocate the diagonal data pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the diagonal data pointer (should be m_nb_diag_nnz)
	 *
	 *  \exception AllocationFailed Allocation of m_diag_data failed
	 */
	void allocateDiagDatas(const int _size)
	{
		if(_size==0)
		{
			m_diag_data = 0;
			return;
		}
		else
			m_diag_data = new data_type[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_diag_data);
#endif
	}

	/*!
	 *  \brief Allocate the non diagonal data pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the non diagonal data pointer (should be m_nb_diag_nnz)
	 *
	 *  \exception AllocationFailed Allocation of m_ndiag_data failed
	 */
	void allocateNDiagDatas(const int _size)
	{
		if(_size==0)
		{
			m_ndiag_data = 0;
			return;
		}
		else
			m_ndiag_data = new data_type[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_ndiag_data);
#endif
	}

	/*!
	 *  \brief Allocate the local/global ID mapping
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the local/global ID mapping (should be m_nb_diag_cols)
	 *
	 *  \exception AllocationFailed Allocation of m_mapping failed
	 */
	void allocateMapping(const int _size)
	{
		if(_size==0)
		{
			m_mapping = 0;
			return;
		}
		else
			m_mapping = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocation(m_mapping);
#endif
	}

	/*!
	 *  \brief Allocate the non diagonal/diagonal ID mapping
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the non diagonal/diagonal ID mapping (should be m_nb_ndiag_cols)
	 *
	 *  \exception AllocationFailed Allocation of m_mapping failed
	 */
	void allocateNDiagMapping(const int _size)
	{
		if(_size==0)
		{
			m_ndiag_mapping = 0;
			return;
		}
		else
			m_ndiag_mapping = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocated(m_ndiag_mapping);
#endif
	}

	/*!
	 *  \brief Allocate and fill a matrix from another matrix
	 *
	 *  \details
	 *
	 *  \tparam MatrixType The matrix type used to allocate and fill the matrix
	 *
	 *  \param[in] _matrix The matrix to copy
	 *
	 *  \exception AllocationFailed The allocation of m_ndiag_mapping, m_diag_rows_ptr, m_ndiag_rows_ptr, m_diag_cols_ptr, m_ndiag_cols_ptr, m_diag_data or m_ndiag_data failed
	 *
	 *  \todo Implement this method
	 */
	template<typename MatrixType>
    void allocateAndFill(const MatrixType& _matrix)
	{
		this->deallocate();
		// TODO
	}
	//@}

	/*! \name Private Checkeur
	 *
	 *  Private Checkeur of the class DistCSRMKLMatrix. \n
	 *  These internal methods provides various check utilities for the class DistCSRMKLMatrix
	 */
	//@{

	/*!
	 *  \brief Check if an allocation was successful
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception AllocationFailed _datas was not properly allocated
	 */
	void checkAllocation(const void* _datas) const
	{
		try
		{
			if(FailedAllocation::Check(_datas))
				throw AllocationFailed(__FILE__,__LINE__);
		}
		catch ( AllocationFailed &ptrFailed )
		{
			YALLA_ERR(ptrFailed.what());
		}
	}

	/*!
	 *  \brief Check if a pointer was allocated
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception PtrNotAllocated _datas is not allocated
	 */
	void checkAllocated(const void* _datas) const
	{
		try
		{
			if(PointerNotAllocated::Check(_datas))
				throw AllocationFailed(__FILE__,__LINE__);
		}
		catch ( PtrNotAllocated &ptrFailed )
		{
			YALLA_ERR(ptrFailed.what());
		}
	}

	/*!
	 *  \brief Check if an index is between specified bounds
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to  check
	 *  \param[in] _lowerBound The lower bound
	 *  \param[in] _upperBound The upper bound
	 *
	 *  \exception isOutOfRange _index is not between lower/upper bound
	 */
	void checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
	{
		try
		{
			if(CheckBounds::Check(_index,_lowerBound,_upperBound))
				throw isOutOfRange(__FILE__,__LINE__);
		}
		catch ( isOutOfRange &isOut )
		{
			YALLA_ERR(isOut.what());
		}
	}
	//@}
};

YALLA_END_NAMESPACE
#endif
