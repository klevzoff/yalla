#pragma once
#ifndef yalla_seq_csr_mkl_matrix_h
#define yalla_seq_csr_mkl_matrix_h

/*!
 *  \file SeqCSRMKLMatrix.h
 *  \brief Sequential CSR matrix
 *  \date 12/17/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/MatrixGraphs/SeqCSRMatrixGraph.h"
#include "yalla/Utils/Exceptions/CheckBounds.h"
#include "yalla/Utils/Exceptions/PointerNotAllocated.h"
#include "yalla/Utils/Exceptions/FailedAllocation.h"
#include "yalla/Utils/Iterators/RowIteratorSeqCSRMatrix.h"
#include "yalla/Utils/Iterators/ColIteratorSeqCSRMatrix.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 *
 *  Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \class SeqCSRMKLMatrix
 *  \brief Sequential CSR matrix
 *
 *  \details
 *  The class stores the two dimensions of the matrix in the attributes m_nb_rows and m_nb_cols. \n
 *  Datas are stored in m_data, a unidimensionnal array. \n
 *  In addition, the class store the local number of non zero elements in the attribute m_nb_nnz. \n
 *  The graph of the matrix is also stored in the attribute m_matrix_graph. \n
 *
 *  The data structure of the matrix is stored in the classical 3 array variation of a CSR matrix in the attributes m_rows_ptr and m_cols_ptr. \n
 *
 *  2 types of iterators are used here. One for iterating over the rows and one for iterating over the cols. They exist in both their const and non const version.
 *
 *  The class can throw three types of exception, only in debug mode:
 *  - isOutOfRange exception. If someones try to access an element at index (row,col), such that m_nb_rows < row < 0 or m_nb_cols < col < 0. This exception can be thrown in almost all setter/getter functions.
 *  - AllocationFailed exception. If an allocation failed, the function throws this exception. This exception can be thrown in all functions that allocates memory
 *  - PointerNotAllocated exception. If the user tries to access a non allocated pointer. This exception can be thrown in almost all setter/getter functions
 *
 *  \tparam DataType The data type of the matrix
 *
 *  \warning No alias test is implemented right now.
 *
 *  \todo It might be usefull to sort the matrix, to store the diagonal element first for each row. This might easily be done by adding a m_is_ready flag, the same way it's done for the DistCSRMKLMatrix.
 */
template<typename DataType>
class SeqCSRMKLMatrix
{
 public:
	//! \name Friend Class
	//@{
	//! Friend encapsulating class
	template<typename DataType2, typename MatrixImpl>
    friend class SeqSparseMatrix;
	//@}

	//! \name Public Typedefs
	//@{
	//! \brief Data type of the matrix
	typedef DataType data_type;
	//! \brief Type of the graph
	typedef SeqCSRMatrixGraph graph_type;
	//! \brief Type of this class
	typedef SeqCSRMKLMatrix<data_type> this_type;
	//! \brief Type of the constant rows iterator
	typedef RowIteratorSeqCSRMatrix<this_type,true> const_row_iterator_type;
	//! \brief Type of the rows iterator
	typedef RowIteratorSeqCSRMatrix<this_type> row_iterator_type;
	//! \brief Type of the constant cols iterator
	typedef ColIteratorSeqCSRMatrix<this_type,true> const_col_iterator_type;
	//! \brief Type of the cols iterator
	typedef ColIteratorSeqCSRMatrix<this_type> col_iterator_type;
	//@}
 private:
	//! \brief Datas of the matrix
	data_type* m_data;
	//! \brief Number of rows
	int m_nb_rows;
	//! \brief Number of columns
	int m_nb_cols;
	//! \brief Number of non zero elements
	int m_nb_nnz;
	//! \brief Row pointer
	int* m_rows_ptr;
	//! \brief Col pointer
	int* m_cols_ptr;
	//! \brief Graph of the matrix
	graph_type* m_matrix_graph;
	//! \brief Attribute always valued to zero
	data_type m_zero;
	//! \brief Flag indicating if the row and col pointers are allocated but not filled
	bool m_is_allocated;
	//! \brief Flag indicating if the row and col pointers are allocated and filled
	bool m_is_init;
 public:
	/*! \name Public Constructors
	 *
	 *  Public constructors of the class SeqCSRMKLMatrix
	 */
	//@{
	/*! 
	 *  \brief Default constructor
	 *
	 *  \details
	 *  Set all attributes to zero/false.
	 */
	explicit SeqCSRMKLMatrix()
	{
    m_nb_rows = 0;
    m_nb_cols = 0;
    m_nb_nnz = 0;
    m_rows_ptr = 0;
    m_cols_ptr = 0;
    m_data = 0;
    m_matrix_graph = 0;
    m_is_allocated = false;
    m_is_init = false;
    m_zero = 0;
  }

	/*!
	 *  \brief Copy constructor
	 *
	 *  \details
	 *  Call the method allocateAndFill to copy the matrix structure and the datas. \n
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr, m_data or m_matrix_graph failed
	 *
	 *  \param[in] _matrix Matrix to copy
	 */
	explicit SeqCSRMKLMatrix(const SeqCSRMKLMatrix& _matrix)
	{
		this->allocateAndFill(_matrix);
    m_zero = 0;
  }

	/*!
	 *  \brief Dimension constructor
	 *
	 *  \details
	 *  Copy the value of _rows and _cols in m_nb_rows and m_nb_cols. \n
	 *  Set all other values to zero. \n
	 *  Set the m_is_allocated to false (pointers are not allocated) and m_is_init to false (pointers not filled)
	 *
	 *  \param[in] _rows Number of rows in the matrix
	 *  \param[in] _cols Number of cols in the matrix
	 */
	explicit SeqCSRMKLMatrix(const int _rows, const int _cols)
	{
		m_nb_rows = _rows;
    m_nb_cols = _cols;
    m_nb_nnz = 0;
    m_rows_ptr = 0;
    m_cols_ptr = 0;
    m_data = 0;
    m_matrix_graph = 0;
    m_is_allocated = false;
    m_is_init = false;
    m_zero = 0;
  }

	/*!
	 *  \brief Dimension with nnz
	 *
	 *  \details
	 *  Copy the value of _rows, _cols and _nbNnz in m_nb_rows, m_nb_cols and m_nb_nnz. \n
	 *  Call the method allocateRowPtr, allocateColPtr and allocateDatas to allocate the different pointers of the data structure. \n
	 *  Set the m_is_allocated to true (pointers are allocated) and m_is_init to false (pointers not filled).
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr or m_data failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 */
	explicit SeqCSRMKLMatrix(const int _rows, const int _cols, const int _nbNnz)
	{
    m_nb_rows = _rows;
    m_nb_cols = _cols;
    m_nb_nnz = _nbNnz;
    this->allocateRowPtr(m_nb_rows+1);
    this->allocateColPtr(m_nb_nnz);
    this->allocateDatas(m_nb_nnz);
    for(int i=0;i<m_nb_rows+1;++i)
      m_rows_ptr[i] = 0;
    for(int i=0;i<m_nb_nnz;++i)
    {
      m_cols_ptr[i] = 0;
      m_data[i] = 0;
    }
    m_matrix_graph = 0;
    m_is_allocated = true;
    m_is_init = false;
    m_zero = 0;
  }

	/*!
	 *  \brief Dimension constructor with data structure
	 *
	 *  \details
	 *  Copy the value of _rows, _cols and _nbNnz in m_nb_rows, m_nb_cols and m_nb_nnz. \n
	 *  Call the method allocateRowPtr, allocateColPtr and allocateDatas to allocate the different pointers of the data structure. \n
	 *
	 *  Copy the values of _rowPtr and _colPtr in m_rows_ptr and m_cols_ptr. \n
	 *  Allocate the data pointer and fill it with zeroes. \n
	 *  The matrix graph is created and stored in m_matrix_graph. \n
	 *  Set the m_is_allocated to true (pointers are allocated) and m_is_init to true (pointers filled).
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr, m_data or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 */
	explicit SeqCSRMKLMatrix(const int _rows, const int _cols, const int _nbNnz, const int* _rowPtr, const int* _colPtr)
	{
    m_nb_rows = _rows;
    m_nb_cols = _cols;
    m_nb_nnz = _nbNnz;
    this->allocateRowPtr(m_nb_rows+1);
    this->allocateColPtr(m_nb_nnz);
    this->allocateDatas(m_nb_nnz);
    for(int i=0;i<m_nb_rows+1;++i)
    {
      m_rows_ptr[i] = _rowPtr[i];
    }
    for(int i=0;i<m_nb_nnz;++i)
    {
      m_cols_ptr[i] = _colPtr[i];
      m_data[i] = 0;
    }
    m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_rows_ptr, m_cols_ptr);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
    m_is_allocated = true;
    m_is_init = true;
    m_zero = 0;
  }

	/*!
	 *  \brief Dimension constructor with data structure and datas
	 *
	 *  \details
	 *  Copy the value of _rows, _cols and _nbNnz in m_nb_rows, m_nb_cols and m_nb_nnz. \n
	 *  Call the method allocateRowPtr, allocateColPtr and allocateDatas to allocate the different pointers of the data structure. \n
	 *
	 *  Copy the values of _rowPtr, _colPtr and _dataPtr in m_rows_ptr, m_cols_ptr and m_data. \n
	 *
	 *  The matrix graph is created and stored in m_matrix_graph. \n
	 *  Set the m_is_allocated to true (pointers are allocated) and m_is_init to true (pointers filled).
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr, m_data or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 *  \param[in] _dataPtr Data pointer
	 */
	explicit SeqCSRMKLMatrix(const int _rows, const int _cols, const int _nbNnz, const int* _rowPtr, const int* _colPtr, const data_type* _dataPtr)
	{
    m_nb_rows = _rows;
    m_nb_cols = _cols;
    m_nb_nnz = _nbNnz;
    this->allocateRowPtr(m_nb_rows+1);
    this->allocateColPtr(m_nb_nnz);
    this->allocateDatas(m_nb_nnz);
    for(int i=0;i<m_nb_rows+1;++i)
      m_rows_ptr[i] = _rowPtr[i];
    for(int i=0;i<m_nb_nnz;++i)
    {
      m_cols_ptr[i] = _colPtr[i];
      m_data[i] = _dataPtr[i];
    }
    m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_rows_ptr, m_cols_ptr);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
    m_is_allocated = true;
    m_is_init = true;
    m_zero = 0;
  }

	/*!
	 *  \brief Graph constructor
	 *
	 *  \details
	 *  Copy the value of matrixGraph.getNbRows(), _matrixGraph.getNbCols() and _matrixGraph.getNnz() in m_nb_rows, m_nb_cols and m_nb_nnz. \n
	 *  Call the method allocateRowPtr, allocateColPtr and allocateDatas to allocate the different pointers of the data structure. \n
	 *
	 *  Copy the values of _matrixGraph.getRowPtr and _matrixGraph.getColPtr in m_rows_ptr and m_cols_ptr. \n
	 *  Allocate the data pointer and fill it with zeroes. \n
	 *  The matrix graph is deep copied in m_matrix_graph. \n
	 *  Set the m_is_allocated to true (pointers are allocated) and m_is_init to true (pointers filled). 
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr, m_data or m_matrix_graph failed
	 *
	 *  \param[in] _matrixGraph The matrix graph
	 */
	explicit SeqCSRMKLMatrix(const graph_type& _matrixGraph)
	{
    m_nb_rows = _matrixGraph.getNbRows();
    m_nb_cols = _matrixGraph.getNbCols();
    m_nb_nnz = _matrixGraph.getNnz();
    this->allocateRowPtr(m_nb_rows+1);
    this->allocateColPtr(m_nb_nnz);
    this->allocateDatas(m_nb_nnz);
    for(int i=0;i<m_nb_rows+1;++i)
      m_rows_ptr[i] = _matrixGraph.getRowPtr(i);
    for(int i=0;i<m_nb_nnz;++i)
    {
      m_cols_ptr[i] = _matrixGraph.getColPtr(i);
      m_data[i] = 0;
    }
    m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_rows_ptr, m_cols_ptr);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
    m_is_allocated = true;
    m_is_init = true;
    m_zero = 0;
  }
	//@}

	/*! \name Public Destructor
	 *
	 *  Public destructor of the class SeqCSRMKLMatrix
	 */
	//@{
	/*! 
	 *  \brief Destructor.
	 *
	 *  \details
	 *  Free the memory allocated by the class
	 */
	virtual ~SeqCSRMKLMatrix()
	{
    delete[] m_rows_ptr;
    m_rows_ptr = 0;
    delete[] m_cols_ptr;
    m_cols_ptr = 0;
    delete[] m_data;
    m_data = 0;
    delete m_matrix_graph;
    m_matrix_graph = 0;
  }
	//@}

	/*! \name Public Getter
	 *
	 *  Public Getter of the class SeqCSRMKLMatrix. \n
	 *  These methods give access in read-only mode to the datas/information of the class SeqCSRMKLMatrix
	 */

	/*! 
	 *  \brief Return the number of rows in the matrix.
	 *
	 *  \details
	 *
	 *  \return The local number of rows in the matrix
	 */
	int getNbRows() const
	{
    return m_nb_rows;
  }

	/*! 
	 *  \brief Return the number of cols in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of cols in the matrix
	 */
	int getNbCols() const
	{
    return m_nb_cols;
  }

	/*! 
	 *  \brief Return the number of non zero elements in the matrix.
	 *
	 *  \details
	 *
	 *  \return The number of non zero elements in the matrix
	 */	
	int getNnz() const
	{
    return m_nb_nnz;
  }

	/*! 
	 *  \brief Return the number of non zero elements on a row
	 *
	 *  \details
	 *
	 *  \param[in] _row The row we want to know the nnz
	 *
	 *  \return The number of non zero elements on the specified row
	 *
	 *  \exception PtrNotAllocated m_rows_ptr is not allocated
	 *  \exception isOutOfRange _row is out of range
	 */	
	int getRowNnz(const int _row) const
	{
#ifdef _DEBUG
    this->checkAllocated(m_rows_ptr);
		this->checkBounds(_row,0,m_nb_rows);
#endif
    return m_rows_ptr[_row+1] - m_rows_ptr[_row];
  }
		
	/*! 
	 *  \brief Return a pointeur on the datas of the matrix.
	 *
	 *  \details
	 *
	 *  \return The data pointer of the matrix
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 */
	const data_type* getPointeur() const
	{
#ifdef _DEBUG
    this->checkAllocated(m_data);
#endif
    return m_data;
  }

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 *
	 *  \exception PtrNotAllocated m_rows_ptr, m_cols_ptr or m_data is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	data_type operator()(const int _row, const int _col) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_rows_ptr);
		this->checkAllocated(m_cols_ptr);
		this->checkAllocated(m_data);
    this->checkBounds(_row,0,m_nb_rows);
    this->checkBounds(_col,0,m_nb_cols);
#endif
    for(int i=m_rows_ptr[_row];i<m_rows_ptr[_row+1];++i)
    {
      if(m_cols_ptr[i]==_col)
        return m_data[i];
    }
    return 0;
  }

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type operator[](const int _index) const
	{
#ifdef _DEBUG
    this->checkBounds(_index,0,m_nb_nnz);
    this->checkAllocated(m_data);
#endif
    return m_data[_index];
  }

	/*!
	 *  \brief Return the class name. 
	 *
	 *  \return "SeqCSRMKLMatrix" The name of the class
	 */
	const char* getClassName() const
	{
    return "SeqCSRMKLMatrix";
  }

	/*!
	 *  \brief Return the graph of the matrix. 
	 *
	 *  \details
	 *
	 *  \return The graph of the matrix
	 *
	 *  \exception PtrNotAllocated m_matrix_graph is not allocated
	 */
	const graph_type& getMatrixGraph() const
	{
#ifdef _DEBUG
		this->checkAllocated(m_matrix_graph);
#endif
    return *m_matrix_graph;
  }
   
	/*! 
	 *  \brief Return the value of the row pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index of the we want to access the value
	 *
	 *  \return The value of the row pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_rows_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getRowPtr(const int _index) const
	{
#ifdef _DEBUG
    this->checkBounds(_index,0,m_nb_rows+1);
    this->checkAllocated(m_rows_ptr);
#endif
    return m_rows_ptr[_index];
  }

	/*! 
	 *  \brief Return the value of the col pointer at the desired index
	 *
	 *  \details
	 *
	 *  \param[in] _index The index of the we want to access the value
	 *
	 *  \return The value of the col pointer at the desired index
	 *
	 *  \exception PtrNotAllocated m_cols_ptr is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */	
	int getColPtr(const int _index) const
	{
#ifdef _DEBUG
    this->checkBounds(_index,0,m_nb_nnz);
    this->checkAllocated(m_cols_ptr);
#endif
    return m_cols_ptr[_index];
  }

	/*! 
	 *  \brief Check if the entry exists in the matrix
	 *
	 *  \details
	 *  Return 1 if the entry exists i.e. there is a nnz at this index, 0 otherwise. \n
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return 1 if an entry exists in the matrix at the index (_row,_col) , 0 otherwise
	 *
	 *  \exception PtrNotAllocated m_matrix_graph is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	int getEntry(const int _row, const int _col) const
	{
#ifdef _DEBUG
		this->checkAllocated(m_matrix_graph);
		this->checkBounds(_row,0,m_nb_rows);
		this->checkBounds(_col,0,m_nb_cols);
#endif
    return (*m_matrix_graph)(_row,_col);
  }
    
	/*! 
	 *  \brief Check if the matrix is initialized
	 *  
	 *  \details 
	 *  Return true if the matrix pointers are allocated and filled, false otherwise. \n
	 *
	 *  \return The state of the initialization flag
	 */
	bool isInitialized() const
	{
    return m_is_init;
  }

	/*! 
	 *  \brief Check if the matrix is allocated
	 *  
	 *  \details 
	 *  Return true if the matrix pointers are allocated, false otherwise. \n
	 *
	 *  \return The state of the allocation flag
	 */
	bool isAllocated() const
	{
    return m_is_allocated;
  }
	//@}

	/*! \name Public Setter
	 *
	 *  Public Setter of the class SeqCSRMKLMatrix. \n
	 *  These methods give access in read/write mode to the datas/information of the class SeqCSRMKLMatrix
	 */
	//@{

	/*! 
	 *  \brief Return the value in the matrix located at the index (_row,_col)
	 *
	 *  \details
	 *  If there is no value located at the index (_row,_col), this methods returns the attribute m_zero, set to zero. \n
	 *  If the matrix is uninitialized, the method always return m_zero, thus not actually doing anything.
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *
	 *  \return The value in the matrix located at the index (_row,_col)
	 *
	 *  \exception PtrNotAllocated m_rows_ptr, m_cols_ptr or m_data is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	data_type& operator()(const int _row, const int _col)
	{
#ifdef _DEBUG
		this->checkAllocated(m_rows_ptr);
		this->checkAllocated(m_cols_ptr);
		this->checkAllocated(m_data);
		this->checkBounds(_row,0,m_nb_rows);
		this->checkBounds(_col,0,m_nb_cols);
    if(m_is_allocated && m_is_init)
    {
#endif
      for(int i=m_rows_ptr[_row];i<m_rows_ptr[_row+1];++i)
      {
        if(m_cols_ptr[i]==_col)
          return m_data[i];
      }
			m_zero = 0;
      return m_zero;
#ifdef _DEBUG
    }
		m_zero = 0;
		return m_zero;
#endif
  }

	/*! 
	 *  \brief Return the value in the matrix located at the index (_index)
	 *
	 *  \details
	 *  This method allow to access an element in the matrix in a 1D fashion. \n
   *  The access is not done by row and col, just by the index of the element we want to access.
	 *
	 *  \param[in] _index The index to access
	 *
	 *  \return The value in the matrix located at the index _index
	 *
	 *  \exception PtrNotAllocated m_data is not allocated
	 *  \exception isOutOfRange _index is out of range
	 */
	data_type& operator[](const int _index)
	{
#ifdef _DEBUG
    this->checkBounds(_index,0,m_nb_nnz);
    this->checkAllocated(m_data);
#endif
    return m_data[_index];
  }

	/*!
	 *  \brief Set the value in the matrix located at the index (_row,_col) to _val while building the data structure pointers
	 *
	 *  \details
	 *  The purpose of this method is to allow the user to set up the data structure of a sparse matrix while filling it. \n
	 *  If the data structure pointers (i.e. m_rows_ptr and m_cols_ptr) have been allocated but not filled, this method is used to build the data structure pointers while filling the matrix. \n
	 *  The matrix needs to be allocated, but not initialized.
	 *
	 *  \param[in] _row The index of the row to access
	 *  \param[in] _col The index of the col to access
	 *  \param[in] _val The value to set at the index (_row,_col)
	 *
	 *  \warning This method might be convenient but is clearly inefficient
	 *
	 *  \exception PtrNotAllocated m_rows_ptr, m_cols_ptr or m_data is not allocated
	 *  \exception isOutOfRange _row or _col is out of range
	 */
	void setValue(const int _row, const int _col, const data_type _val)
	{
#ifdef _DEBUG
		this->checkAllocated(m_rows_ptr);
		this->checkAllocated(m_cols_ptr);
		this->checkAllocated(m_data);
		this->checkBounds(_row,0,m_nb_rows);
		this->checkBounds(_col,0,m_nb_cols);
		if(m_is_allocated && !m_is_init)
		{
#endif
			for(int i=_row;i<m_nb_rows;++i)
				++m_rows_ptr[i+1];
			m_cols_ptr[m_rows_ptr[_row+1]-1] = _col;
			m_data[m_rows_ptr[_row+1]-1] = _val;
#ifdef _DEBUG
		}
		else
			YALLA_ERR("ERROR: Matrix is not allocated or is already initialized . In the later, use the operator() to fill the matrix");
#endif
  }
	//@}

	/*! \name Public Iterators
	 *
	 *  Public iterators of the class SeqCSRMKLMatrix. \n
	 *  These methods returns differents iterators on the matrix elements
	 */
	//@{

	/*!
	 *  \brief Return a const iterator on the first row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the first row of the matrix
	 */
	const_row_iterator_type rowBegin() const
	{
    return const_row_iterator_type(&m_data[0], 0, *this);
  }

	/*!
	 *  \brief Return an iterator on the first row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the first row of the matrix
	 */
	row_iterator_type rowBegin()
	{
    return row_iterator_type(&m_data[0], 0, *this);
  }

	/*!
	 *  \brief Return a const iterator on the last row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the last row of the matrix
	 */
	const_row_iterator_type rowEnd() const
	{
    return const_row_iterator_type(&m_data[m_rows_ptr[m_nb_rows]], m_rows_ptr[m_nb_rows], *this);
  }

	/*!
	 *  \brief Return an iterator on the last row of the matrix
	 *
	 *  \details
	 *
	 *  \return The first element on the last row of the matrix
	 */
	row_iterator_type rowEnd()
	{
    return row_iterator_type(&m_data[m_rows_ptr[m_nb_rows]], m_rows_ptr[m_nb_rows], *this);
  }

	/*!
	 *  \brief Return a const iterator on the first col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The first element on the specified row of the matrix
	 */
	const_col_iterator_type colBegin(const const_row_iterator_type& _rowIterator) const
	{
    return const_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
  }

	/*!
	 *  \brief Return an iterator on the first col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the first element of the row associated with the row iterator.
	 *
	 *  \return The first element on the specified row of the matrix
	 */
	col_iterator_type colBegin(const row_iterator_type& _rowIterator)
	{
    return col_iterator_type(_rowIterator.getPosition(), _rowIterator);
  }

	/*!
	 *  \brief Return a const iterator on the last col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the specified row of the matrix
	 */
	const_col_iterator_type colEnd(const const_row_iterator_type& _rowIterator) const
	{
    return const_col_iterator_type(_rowIterator.getPosition()+this->getRowNnz(_rowIterator.index()), _rowIterator);
  }

	/*!
	 *  \brief Return an iterator on the last col of a row 
	 *
	 *  \details
	 *  This method will return an iterator on the last element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the col
	 *
	 *  \return The last element on the specified row of the matrix
	 */
	col_iterator_type colEnd(const row_iterator_type& _rowIterator)
	{
    return col_iterator_type(_rowIterator.getPosition()+this->getRowNnz(_rowIterator.index()), _rowIterator);
  }

	/*!
	 *  \brief Return a const iterator on the diagonal element of a row
	 *
	 *  \details
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \param[in] _rowIterator The row iterator we want to access the diagonal element
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	const_col_iterator_type diagonalElement(const const_row_iterator_type& _rowIterator) const
	{
		const_col_iterator_type colIt = const_col_iterator_type(_rowIterator.getPosition(), _rowIterator);
		if(colIt.index()==_rowIterator.index())
			return colIt;
		else
		{
			while(colIt.index()!=_rowIterator.index())
				++colIt;
		}		
    return colIt;
  }

	/*!
	 *  \brief Return an iterator on the diagonal element of a row
	 *
	 *  \details
	 *  This method will return an iterator on the diagonal element of the row associated with the row iterator.
	 *
	 *  \return The diagonal element of the row of the matrix
	 */
	col_iterator_type diagonalElement(const row_iterator_type& _rowIterator)
	{
		col_iterator_type colIt = col_iterator_type(_rowIterator.getPosition(), _rowIterator);
		if(colIt.index()==_rowIterator.index())
			return colIt;
		else
		{
			while(colIt.index()!=_rowIterator.index())
				++colIt;
		}		
    return colIt;
  }
	//@}
    
	/*! \name Public Utils
	 *
	 *  Public Utils of the class SeqCSRMKLMatrix. \n
	 *  These methods provide various utilities to the class SeqCSRMKLMatrix
	 */
	//@{

	/*!
	 *  \brief Initialize a matrix with the parameters given as arguments
	 *
	 *  \details
	 *  Free the memory eventually allocated so far. \n
	 *  Copy the value of _rows, _cols and _nbNnz in m_nb_rows, m_nb_cols and m_nb_nnz. \n
	 *  Call the method allocateRowPtr, allocateColPtr and allocateDatas to allocate the different pointers of the data structure. \n
	 *
	 *  Copy the values of _rowPtr and _colPtr in m_rows_ptr and m_cols_ptr. \n
	 *  Allocate the data pointer and fill it with zeroes. \n
	 *  The matrix graph is created and stored in m_matrix_graph. \n
	 *  Set the m_is_allocated to true (pointers are allocated) and m_is_init to true (pointers filled).
	 *
	 *  \exception AllocationFailed The allocation of m_rows_ptr, m_cols_ptr, m_data or m_matrix_graph failed
	 *
	 *  \param[in] _rows Number of local rows in the matrix
	 *  \param[in] _cols Number of local cols in the matrix
	 *  \param[in] _nbNnz Number of non zero elements in the matrix
	 *  \param[in] _rowPtr Row pointer in standard 3 array variation format
	 *  \param[in] _colPtr Col pointer in standard 3 array variation format
	 */
	void initialize(const int _rows, const int _cols, const int _nbNnz, const int* _rowPtr, const int* _colPtr)
	{
		delete[] m_rows_ptr;
		delete[] m_cols_ptr;
		delete[] m_data;
		m_nb_rows = _rows;
		m_nb_cols = _cols;
		m_nb_nnz = _nbNnz;
		this->allocateRowPtr(m_nb_rows+1);
		this->allocateColPtr(m_nb_nnz);
		this->allocateDatas(m_nb_nnz);
		for(int i=0;i<m_nb_rows+1;++i)
		{
			m_rows_ptr[i] = _rowPtr[i];
		}
		for(int i=0;i<m_nb_nnz;++i)
		{
			m_cols_ptr[i] = _colPtr[i];
			m_data[i] = 0;
		}
		m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_rows_ptr, m_cols_ptr);
#ifdef _DEBUG
		this->checkAllocation(m_matrix_graph);
#endif
		m_is_allocated = true;
		m_is_init = true;
		m_zero = 0;
	}

	/*!
	 *  \brief Fill the matrix with a value.
	 *
	 *  \details
	 *
	 *  \exception AllocationFailed The allocation of m_data failed
	 *
	 *  \param[in] _val The value to copy in the matrix
	 */
	void fill(const data_type _val)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
    for(int i=0;i<m_nb_nnz;++i)
      m_data[i] = _val;
  }

	/*!
	 *  \brief Fill a matrix by copying the pointer passed in parameter.
	 *
	 *  \details
	 *
	 *  \exception AllocationFailed The allocation of m_data failed
	 *
	 *  \param[in] _src The datas to copy
	 */
	void fill(const data_type* _src)
	{
#ifdef _DEBUG
		this->checkAllocated(m_data);
#endif
    for(int i=0;i<m_nb_nnz;++i)
      m_data[i] = _src[i];
  }

	/*!
	 *  \brief Finalize the matrix
	 *  
	 *  \details
	 *  If the matrix is initialized, this method do nothing. \n
	 *  If the matrix is allocated and not initialized, this method creates the graph and set the m_is_init flag to true. \n
	 *  This method is to be called once the matrix is completely set up (all pointers allocated and filled). 
	 *
	 *  \exception AllocationFailed The allocation of m_matrix_graph failed
	 */
	void finalize()
	{
		if(m_is_init && m_is_allocated)
			return;
    
		if(m_is_allocated)
    {
			m_is_init = true;
      delete m_matrix_graph;
      m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_rows_ptr, m_cols_ptr);
#ifdef _DEBUG
			this->checkAllocation(m_matrix_graph);
#endif
      return;
    }
  }
	//@}

	/*! \name Private Allocator
	 *
	 *  Private Allocator of the class SeqCSRMKLMatrix. \n
	 *  These internal methods allow to allocate and eventually fill a SeqCSRMKLMatrix in different cases
	 */
	//@{

 private:
	/*!
	 *  \brief Allocate the row pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the row pointer (should be m_nb_rows+1)
	 *
	 *  \exception AllocationFailed Allocation of m_rows_ptr failed
	 */
	void allocateRowPtr(const int _size)
	{
    if(_size==0)
      m_rows_ptr = 0;
    else
      m_rows_ptr = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocation(m_rows_ptr);
#endif
  }

	/*!
	 *  \brief Allocate the col pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the col pointer (should be m_nb_nnz)
	 *
	 *  \exception AllocationFailed Allocation of m_cols_ptr failed
	 */
	void allocateColPtr(const int _size)
	{
    if(_size==0)
      m_cols_ptr = 0;
    else
      m_cols_ptr = new int[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocation(m_cols_ptr);
#endif
  }

	/*!
	 *  \brief Allocate the data pointer
	 *
	 *  \details
	 *
	 *  \param[in] _size The size of the data pointer (should be m_nb_nnz)
	 *
	 *  \exception AllocationFailed Allocation of m_data failed
	 */
	void allocateDatas(const int _size)
	{
    if(_size==0)
      m_data = 0;
    else
      m_data = new data_type[_size];
#ifdef _DEBUG
		if(_size!=0)
			this->checkAllocation(m_data);
#endif
  }

	/*!
	 *  \brief Allocate and fill a matrix from another matrix
	 *
	 *  \details
	 *
	 *  \tparam MatrixType The matrix type used to allocate and fill the matrix
	 *
	 *  \param[in] _matrix The matrix to copy
	 *
	 *  \exception AllocationFailed Allocation of m_rows_ptr, m_cols_ptr, m_data or m_matrix_graph failed
	 */
	template<typename MatrixType>
    void allocateAndFill(const MatrixType& _matrix)
	{
		delete[] m_rows_ptr;
		delete[] m_cols_ptr;
		delete[] m_data;
		m_nb_rows = _matrix.getNbRows();
		m_nb_cols = _matrix.getNbCols();
		m_nb_nnz = _matrix.getNnz();
		this->allocateRowPtr(m_nb_rows+1);
		this->allocateColPtr(m_nb_nnz);
		this->allocateDatas(m_nb_nnz);
		const graph_type& matrixGraph = _matrix.getMatrixGraph();
		for(int i=0;i<m_nb_rows+1;++i)
			m_rows_ptr[i] = matrixGraph.getRowPtr(i);
		const data_type* tmpData = _matrix.getPointeur();
		for(int i=0;i<m_nb_nnz;++i)
		{
			m_cols_ptr[i] = matrixGraph.getColPtr(i);
			m_data[i] = tmpData[i];
		}
		delete m_matrix_graph;
		m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_rows_ptr, m_cols_ptr);
#ifdef _DEBUG
			this->checkAllocation(m_matrix_graph);
#endif
		m_is_allocated = true;
		m_is_init = true;
	}

	/*!
	 *  \brief Allocate a matrix from another matrix
	 *
	 *  \details
	 *
	 *  \tparam MatrixType The matrix type used to allocate the matrix
	 *
	 *  \param[in] _matrix The matrix to copy
	 *
	 *  \exception AllocationFailed Allocation of m_rows_ptr, m_cols_ptr, m_data or m_matrix_graph failed
	 */
	template<typename MatrixType>
    void allocate(const MatrixType& _matrix)
	{
		delete[] m_rows_ptr;
		delete[] m_cols_ptr;
		delete[] m_data;
		m_nb_rows = _matrix.getNbRows();
		m_nb_cols = _matrix.getNbCols();
		m_nb_nnz = _matrix.getNnz();
		this->allocateRowPtr(m_nb_rows+1);
		this->allocateColPtr(m_nb_nnz);
		this->allocateDatas(m_nb_nnz);
		const graph_type& matrixGraph = _matrix.getMatrixGraph();
		for(int i=0;i<m_nb_rows+1;++i)
			m_rows_ptr[i] = matrixGraph.getRowPtr(i);
		for(int i=0;i<m_nb_nnz;++i)
		{
			m_cols_ptr[i] = matrixGraph.getColPtr(i);
		}
		delete m_matrix_graph;
		m_matrix_graph = new graph_type(m_nb_rows, m_nb_cols, m_nb_nnz, m_rows_ptr, m_cols_ptr);
#ifdef _DEBUG
			this->checkAllocation(m_matrix_graph);
#endif
		m_is_allocated = true;
		m_is_init = true;
	}
	//@}

	/*! \name Private Utils
	 *
	 *  Private Utils of the class SeqCSRMKLMatrix. \n
	 *  These internal methods provides various utilities to the class SeqCSRMKLMatrix
	 */
	//@{

	/*!
	 *  \brief Fill an allocated matrix with another matrix
	 *
	 *  \details
	 *
	 *  \tparam MatrixType The matrix type used to fill the data pointer
	 *
	 *  \param[in] _src The matrix to copy
	 */
	template<typename MatrixType>
    void fill(const MatrixType& _src)
	{
#ifdef _DEBUG
			this->checkAllocated(m_data);
#endif
		for(int i=0;i<m_nb_nnz;++i)
		{
			m_data[i] = _src[i];
		}
	}
	//@}

	/*! \name Private Checkeur
	 *
	 *  Private Checkeur of the class SeqCSRMKLMatrix. \n
	 *  These internal methods provides various check utilities for the class SeqCSRMKLMatrix
	 */
	//@{

	/*!
	 *  \brief Check if an allocation was successful
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception AllocationFailed _datas was not properly allocated
	 */
	void checkAllocation(const void* _datas) const
	{
    try
    {
      if(FailedAllocation::Check(_datas))
        throw AllocationFailed(__FILE__,__LINE__);
    }
    catch ( AllocationFailed &ptrFailed )
    {
      YALLA_ERR(ptrFailed.what());
    }
  }

	/*!
	 *  \brief Check if a pointer was allocated
	 *
	 *  \details
	 *
	 *  \param[in] _datas The pointer to check
	 *
	 *  \exception PtrNotAllocated _datas is not allocated
	 */
	void checkAllocated(const void* _datas) const
	{
		try
		{
			if(PointerNotAllocated::Check(_datas))
				throw AllocationFailed(__FILE__,__LINE__);
		}
		catch ( PtrNotAllocated &ptrFailed )
		{
			YALLA_ERR(ptrFailed.what());
		}
	}
    
	/*!
	 *  \brief Check if an index is between specified bounds
	 *
	 *  \details
	 *
	 *  \param[in] _index The index to  check
	 *  \param[in] _lowerBound The lower bound
	 *  \param[in] _upperBound The upper bound
	 *
	 *  \exception isOutOfRange _index is not between lower/upper bound
	 */
	void checkBounds(const int _index, const int _lowerBound, const int _upperBound) const
	{
    try
    {
      if(CheckBounds::Check(_index,_lowerBound,_upperBound))
        throw isOutOfRange(__FILE__,__LINE__);
    }
    catch ( isOutOfRange &isOut )
    {
      YALLA_ERR(isOut.what());
    }
  }
	//@}
};

YALLA_END_NAMESPACE
#endif
