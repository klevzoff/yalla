#ifndef yalla_test_perf_seq_copy_12_mio_nnz_h
#define yalla_test_perf_seq_copy_12_mio_nnz_h

class SeqCopyTest12MioNnz
: public testing::Test
{
 public:
	typedef double data_type;
	typedef SeqVector<data_type> vector_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static vector_type *m_x, *m_y;
	static int m_size;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		m_size = 12e6;
		m_x = new SeqVector<data_type>(m_size,1);
		m_y = new SeqVector<data_type>(m_size,1);
		m_x->fill(1.);
		m_y->fill(0.);
		m_end = std::chrono::system_clock::now();

		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_x;
		delete m_y;
	}
};

SeqCopyTest12MioNnz::chrono_type SeqCopyTest12MioNnz::m_start;
SeqCopyTest12MioNnz::chrono_type SeqCopyTest12MioNnz::m_end;
int SeqCopyTest12MioNnz::m_size = 0;
SeqCopyTest12MioNnz::vector_type* SeqCopyTest12MioNnz::m_x = 0;
SeqCopyTest12MioNnz::vector_type* SeqCopyTest12MioNnz::m_y = 0;

TEST_F(SeqCopyTest12MioNnz, SeqCopy)
{
	BLAS::copy(*m_x,*m_y);
}

#endif
