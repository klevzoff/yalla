#ifndef yalla_test_perf_dist_dot_2_mio_nnz_h
#define yalla_test_perf_dist_dot_2_mio_nnz_h

class DistDotTest2MioNnz
: public testing::Test
{
 public:
	typedef double data_type;
	typedef DistVector<data_type> vector_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static vector_type *m_x, *m_y;
	static int m_size;
	static int m_own_size;
	static int* m_row_mapping;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		m_size = 2e6;
		m_own_size = m_size/ParUtils::getNumProc();
		m_row_mapping = new int[m_own_size];
		for(int i=0;i<m_own_size;++i)
			m_row_mapping[i] = 0;
		m_x = new vector_type(m_own_size,1,m_own_size,m_size,1,m_row_mapping,0);
		m_y = new vector_type(m_own_size,1,m_own_size,m_size,1,m_row_mapping,0);
		m_x->fill(1.);
		m_y->fill(1.);
		m_end = std::chrono::system_clock::now();

		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_x;
		delete m_y;
		delete[] m_row_mapping;
	}
};

DistDotTest2MioNnz::chrono_type DistDotTest2MioNnz::m_start;
DistDotTest2MioNnz::chrono_type DistDotTest2MioNnz::m_end;
int DistDotTest2MioNnz::m_size = 0;
int DistDotTest2MioNnz::m_own_size = 0;
int* DistDotTest2MioNnz::m_row_mapping = 0;
DistDotTest2MioNnz::vector_type* DistDotTest2MioNnz::m_y = 0;
DistDotTest2MioNnz::vector_type* DistDotTest2MioNnz::m_x = 0;

TEST_F(DistDotTest2MioNnz, DistDot)
{
	BLAS::dot(*m_x,*m_y);
}

#endif
