#ifndef yalla_test_dist_scal_h
#define yalla_test_dist_scal_h

#include <chrono>
#include <string>

#include "gtest/gtest.h"

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"
#include "yalla/Utils/BLAS/BLAS.h"

#include "TestPerfDistScal2MioNnz.h"
#include "TestPerfDistScal12MioNnz.h"

#endif
