#ifndef yalla_test_seq_copy_h
#define yalla_test_seq_copy_h

#include <chrono>
#include <string>

#include "gtest/gtest.h"

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#include "yalla/Utils/BLAS/BLAS.h"

#include "TestPerfSeqCopy2MioNnz.h"
#include "TestPerfSeqCopy12MioNnz.h"

#endif
