#ifndef yalla_test_perf_dist_copy_2_mio_nnz_h
#define yalla_test_perf_dist_copy_2_mio_nnz_h

class DistCopyTest2MioNnz
: public testing::Test
{
 public:
	typedef double data_type;
	typedef DistVector<data_type> vector_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static vector_type *m_x, *m_y;
	static int m_size;
	static int m_own_size;
	static int* m_row_mapping;
	static data_type m_a;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		m_size = 2e6;
		m_own_size = m_size/ParUtils::getNumProc();
		m_row_mapping = new int[m_own_size];
		for(int i=0;i<m_own_size;++i)
			m_row_mapping[i] = 0;
		m_x = new vector_type(m_own_size,1,m_own_size,m_size,1,m_row_mapping,0);
		m_y = new vector_type(m_own_size,1,m_own_size,m_size,1,m_row_mapping,0);
		m_x->fill(1.);
		m_y->fill(0.);
		m_end = std::chrono::system_clock::now();

		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_x;
		delete m_y;
		delete[] m_row_mapping;
	}
};

DistCopyTest2MioNnz::chrono_type DistCopyTest2MioNnz::m_start;
DistCopyTest2MioNnz::chrono_type DistCopyTest2MioNnz::m_end;
int DistCopyTest2MioNnz::m_size = 0;
int DistCopyTest2MioNnz::m_own_size = 0;
int* DistCopyTest2MioNnz::m_row_mapping = 0;
DistCopyTest2MioNnz::vector_type* DistCopyTest2MioNnz::m_x = 0;
DistCopyTest2MioNnz::vector_type* DistCopyTest2MioNnz::m_y = 0;

TEST_F(DistCopyTest2MioNnz, DistCopy)
{
	BLAS::copy(*m_x,*m_y);
}

#endif
