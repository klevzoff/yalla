#ifndef yalla_test_seq_gemv_h
#define yalla_test_seq_gemv_h

#include <chrono>
#include <string>

#include "gtest/gtest.h"

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMKLMatrix.h"
#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#include "yalla/IO/Reader/Sparse/SparseMatrixReaderMMFormat.h"
#include "yalla/Utils/BLAS/BLAS.h"

#include "TestPerfSeqGEMV2MioNnz.h"
#include "TestPerfSeqGEMV2MioNnzMKL.h"
#include "TestPerfSeqGEMV2MioNnzRawPointers.h"
#include "TestPerfSeqGEMV12MioNnz.h"
#include "TestPerfSeqGEMV12MioNnzMKL.h"
#include "TestPerfSeqGEMV12MioNnzRawPointers.h"


#endif
