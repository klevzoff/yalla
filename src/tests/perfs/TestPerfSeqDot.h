#ifndef yalla_test_seq_dot_h
#define yalla_test_seq_dot_h

#include <chrono>
#include <string>

#include "gtest/gtest.h"

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#include "yalla/Utils/BLAS/BLAS.h"

#include "TestPerfSeqDot2MioNnz.h"
#include "TestPerfSeqDot12MioNnz.h"

#endif
