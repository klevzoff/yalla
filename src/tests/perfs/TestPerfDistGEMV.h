#ifndef yalla_test_dist_gemv_h
#define yalla_test_dist_gemv_h

#include <chrono>
#include <string>
#include <memory>

#include "gtest/gtest.h"

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMKLMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMKLMatrix.h"

#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"

#include "yalla/IO/Reader/Sparse/SparseMatrixReaderMMFormat.h"
#include "yalla/Utils/BLAS/BLAS.h"
#include "yalla/Utils/TypeTraits/PromoteToDist.h"
#include "yalla/Utils/DataDistribution/DataDistribution.h"
#ifdef WITH_METIS
#include "yalla/Utils/DataDistribution/MetisDistribution.h"
#else
#include "yalla/Utils/DataDistribution/RowDistribution.h"
#endif

#include "TestPerfDistGEMV2MioNnz.h"
#include "TestPerfDistGEMV2MioNnzMKL.h"
#include "TestPerfDistGEMV2MioNnzRawPointers.h"
#include "TestPerfDistGEMV12MioNnz.h"
#include "TestPerfDistGEMV12MioNnzMKL.h"
#include "TestPerfDistGEMV12MioNnzRawPointers.h"

#endif
