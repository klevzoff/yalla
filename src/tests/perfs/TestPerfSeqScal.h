#ifndef yalla_test_scal_dot_h
#define yalla_test_scal_dot_h

#include <chrono>
#include <string>

#include "gtest/gtest.h"

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#include "yalla/Utils/BLAS/BLAS.h"

#include "TestPerfSeqScal2MioNnz.h"
#include "TestPerfSeqScal12MioNnz.h"

#endif
