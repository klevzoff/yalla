#ifndef yalla_test_perf_seq_dot_2_mio_nnz_h
#define yalla_test_perf_seq_dot_2_mio_nnz_h

class SeqDotTest2MioNnz
: public testing::Test
{
 public:
	typedef double data_type;
	typedef SeqVector<data_type> vector_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static vector_type *m_x, *m_y;
	static int m_size;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		m_size = 2e6;
		m_x = new SeqVector<data_type>(m_size,1);
		m_y = new SeqVector<data_type>(m_size,1);
		m_x->fill(2.);
		m_y->fill(1.);
		m_end = std::chrono::system_clock::now();

		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_x;
		delete m_y;
	}
};

SeqDotTest2MioNnz::chrono_type SeqDotTest2MioNnz::m_start;
SeqDotTest2MioNnz::chrono_type SeqDotTest2MioNnz::m_end;
int SeqDotTest2MioNnz::m_size = 0;
SeqDotTest2MioNnz::vector_type* SeqDotTest2MioNnz::m_y = 0;
SeqDotTest2MioNnz::vector_type* SeqDotTest2MioNnz::m_x = 0;

TEST_F(SeqDotTest2MioNnz, SeqDot)
{
	BLAS::dot(*m_x,*m_y);
}

#endif
