#ifndef yalla_test_perf_seq_gemv_12_mio_nnz_h
#define yalla_test_perf_seq_gemv_12_mio_nnz_h

class SeqGEMVTest12MioNnz
: public testing::Test
{
 public:
	typedef double data_type;
	typedef dCSRMatrix matrix_type;
	typedef SeqVector<data_type> vector_type;
	typedef SparseMatrixReaderMMFormat<matrix_type> reader_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
	typedef std::string filename_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static matrix_type *m_matrix;
	static vector_type *m_lhs, *m_rhs;
	static reader_type* m_reader;
	static filename_type m_file_name;
	
	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		m_file_name = "../src/tests/testcases/EXXON_CS_1.mtx";
		m_reader = new reader_type();

		m_matrix = m_reader->Read(m_file_name.c_str());

		m_lhs = new SeqVector<data_type>(m_matrix->getNbRows(),1);
		m_rhs = new SeqVector<data_type>(m_matrix->getNbRows(),1);
		m_lhs->fill(1.);
		m_rhs->fill(0.);
		m_end = std::chrono::system_clock::now();

		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_matrix;
		delete m_lhs;
		delete m_rhs;
		delete m_reader;
	}
};

SeqGEMVTest12MioNnz::chrono_type SeqGEMVTest12MioNnz::m_start;
SeqGEMVTest12MioNnz::chrono_type SeqGEMVTest12MioNnz::m_end;
SeqGEMVTest12MioNnz::matrix_type* SeqGEMVTest12MioNnz::m_matrix = 0;
SeqGEMVTest12MioNnz::vector_type* SeqGEMVTest12MioNnz::m_lhs = 0;
SeqGEMVTest12MioNnz::vector_type* SeqGEMVTest12MioNnz::m_rhs = 0;
SeqGEMVTest12MioNnz::reader_type* SeqGEMVTest12MioNnz::m_reader = 0;
SeqGEMVTest12MioNnz::filename_type SeqGEMVTest12MioNnz::m_file_name = "";

TEST_F(SeqGEMVTest12MioNnz, SeqGEMV)
{
	BLAS::gemv(1.,*m_matrix,*m_lhs,1.,*m_rhs);
}

#endif
