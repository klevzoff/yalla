#ifndef yalla_test_perf_seq_nrm_2_mio_nnz_h
#define yalla_test_perf_seq_nrm_2_mio_nnz_h

class SeqNrmTest2MioNnz
: public testing::Test
{
 public:
	typedef double data_type;
	typedef SeqVector<data_type> vector_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static vector_type *m_x;
	static int m_size;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		m_size = 2e6;
		m_x = new SeqVector<data_type>(m_size,1);
		m_x->fill(1.);
		m_end = std::chrono::system_clock::now();

		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_x;
	}
};

SeqNrmTest2MioNnz::chrono_type SeqNrmTest2MioNnz::m_start;
SeqNrmTest2MioNnz::chrono_type SeqNrmTest2MioNnz::m_end;
int SeqNrmTest2MioNnz::m_size = 0;
SeqNrmTest2MioNnz::vector_type* SeqNrmTest2MioNnz::m_x = 0;

TEST_F(SeqNrmTest2MioNnz, SeqNrm)
{
	BLAS::nrm2(*m_x);
}

#endif
