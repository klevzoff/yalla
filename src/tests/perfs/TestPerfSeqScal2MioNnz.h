#ifndef yalla_test_perf_seq_scal_2_mio_nnz_h
#define yalla_test_perf_seq_scal_2_mio_nnz_h

class SeqScalTest2MioNnz
: public testing::Test
{
 public:
	typedef double data_type;
	typedef SeqVector<data_type> vector_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static vector_type *m_x;
	static data_type m_a;
	static int m_size;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		m_size = 2e6;
		m_x = new SeqVector<data_type>(m_size,1);
		m_x->fill(1.);
		m_a = 2.;
		m_end = std::chrono::system_clock::now();

		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_x;
	}
};

SeqScalTest2MioNnz::chrono_type SeqScalTest2MioNnz::m_start;
SeqScalTest2MioNnz::chrono_type SeqScalTest2MioNnz::m_end;
int SeqScalTest2MioNnz::m_size = 0;
SeqScalTest2MioNnz::vector_type* SeqScalTest2MioNnz::m_x = 0;
SeqScalTest2MioNnz::data_type SeqScalTest2MioNnz::m_a = 0;

TEST_F(SeqScalTest2MioNnz, SeqScal)
{
	BLAS::scal(m_a,*m_x);
}

#endif
