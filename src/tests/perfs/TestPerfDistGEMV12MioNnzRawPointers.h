#ifndef yalla_test_perf_dist_gemv_12_mio_nnz_raw_pointers_h
#define yalla_test_perf_dist_gemv_12_mio_nnz_raw_pointers_h

class DistGEMVTest12MioNnzRawPointers
: public testing::Test
{
 public:
	typedef double data_type;
	typedef dCSRMatrix seq_matrix_type;
	typedef SeqVector<data_type> seq_vector_type;
	typedef PromoteToDist<seq_matrix_type,data_type>::data_type dist_matrix_type;
	typedef PromoteToDist<seq_vector_type, data_type>::data_type dist_vector_type;
	typedef SparseMatrixReaderMMFormat<seq_matrix_type> reader_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
	typedef std::string filename_type;
#ifdef WITH_METIS
	typedef DataDistribution<MetisDistribution<data_type> > distribution_type;
#else
	typedef DataDistribution<RowDistribution<data_type> > distribution_type;
#endif
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static seq_matrix_type *m_matrix;
	static seq_vector_type *m_lhs, *m_rhs;
	static dist_matrix_type* m_loc_matrix;
	static dist_vector_type *m_loc_lhs, *m_loc_rhs;
	static reader_type* m_reader;
	static filename_type m_file_name;
	static distribution_type* m_distribution;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		if(ParUtils::iAmMaster())
		{
			m_file_name = "../src/tests/testcases/EXXON_CS_1.mtx";
			m_reader = new reader_type();
			m_matrix = m_reader->Read(m_file_name.c_str());
			
			m_lhs = new SeqVector<data_type>(m_matrix->getNbRows(),1);
			m_rhs = new SeqVector<data_type>(m_matrix->getNbRows(),1);
			m_lhs->fill(1.);
			m_rhs->fill(0.);
			m_end = std::chrono::system_clock::now();
			
			double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
			YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
		}

		m_distribution = new distribution_type();
    m_loc_matrix = m_distribution->DistributeMatrix(*m_matrix);
    m_loc_lhs = m_distribution->DistributeVector(*m_lhs);
    m_loc_rhs = m_distribution->DistributeVector(*m_rhs);

		delete m_matrix;
		delete m_lhs;
		delete m_rhs;
		delete m_reader;

		m_end = std::chrono::system_clock::now();
			
		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_loc_matrix;
		delete m_loc_lhs;
		delete m_loc_rhs;
	}
};

DistGEMVTest12MioNnzRawPointers::chrono_type DistGEMVTest12MioNnzRawPointers::m_start;
DistGEMVTest12MioNnzRawPointers::chrono_type DistGEMVTest12MioNnzRawPointers::m_end;
DistGEMVTest12MioNnzRawPointers::seq_matrix_type* DistGEMVTest12MioNnzRawPointers::m_matrix = 0;
DistGEMVTest12MioNnzRawPointers::seq_vector_type* DistGEMVTest12MioNnzRawPointers::m_lhs = 0;
DistGEMVTest12MioNnzRawPointers::seq_vector_type* DistGEMVTest12MioNnzRawPointers::m_rhs = 0;
DistGEMVTest12MioNnzRawPointers::dist_matrix_type* DistGEMVTest12MioNnzRawPointers::m_loc_matrix = 0;
DistGEMVTest12MioNnzRawPointers::dist_vector_type* DistGEMVTest12MioNnzRawPointers::m_loc_lhs = 0;
DistGEMVTest12MioNnzRawPointers::dist_vector_type* DistGEMVTest12MioNnzRawPointers::m_loc_rhs = 0;
DistGEMVTest12MioNnzRawPointers::reader_type* DistGEMVTest12MioNnzRawPointers::m_reader = 0;
DistGEMVTest12MioNnzRawPointers::filename_type DistGEMVTest12MioNnzRawPointers::m_file_name = "";
DistGEMVTest12MioNnzRawPointers::distribution_type* DistGEMVTest12MioNnzRawPointers::m_distribution = 0;

TEST_F(DistGEMVTest12MioNnzRawPointers, DistGEMV)
{
	const int nbRows = m_loc_matrix->getNbRows();
	const typename dist_matrix_type::impl_type::graph_type graph = m_loc_matrix->getMatrixGraph();
	const int* rowPtr = graph.getRowPtr();
	const int* colPtr = graph.getColPtr();
	const int* ownEnd = m_loc_matrix->getImpl()->getOwnEnd();

	m_loc_lhs->synchronize();

	for(int i=0;i<nbRows;++i)
	{
		for(int j=rowPtr[i];j<ownEnd[i];++j)
		{
			(*m_loc_rhs)(i) += (*m_loc_matrix)[j] * (*m_loc_lhs)(colPtr[j]);
		}
	}

	m_loc_lhs->getSync()->isSynchronized();

	for(int i=0;i<nbRows;++i)
	{
		for(int j=ownEnd[i];j<rowPtr[i+1];++j)
		{
			(*m_loc_rhs)(i) += (*m_loc_matrix)[j] * (*m_loc_lhs)(colPtr[j]);
		}
	}
}

#endif
