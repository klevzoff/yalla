#ifndef yalla_test_perf_dist_bicgstab_2_mio_nnz_mkl_h
#define yalla_test_perf_dist_bicgstab_2_mio_nnz_mkl_h

class DistBiCGStabTest2MioNnzMKL
: public testing::Test
{
 public:
	typedef double data_type;
	typedef SeqSparseMatrix<double, SeqCSRMKLMatrix<double> >  seq_matrix_type;
	typedef SeqVector<data_type> seq_vector_type;
	typedef PromoteToDist<seq_matrix_type,data_type>::data_type dist_matrix_type;
	typedef PromoteToDist<seq_vector_type, data_type>::data_type dist_vector_type;
	typedef SparseMatrixReaderMMFormat<seq_matrix_type> reader_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
	typedef std::string filename_type;
#ifdef WITH_METIS
	typedef DataDistribution<MetisDistribution<data_type> > distribution_type;
#else
	typedef DataDistribution<RowDistribution<data_type> > distribution_type;
#endif
	typedef BiCGStab<dist_matrix_type> solver_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static seq_matrix_type *m_matrix;
	static seq_vector_type *m_lhs, *m_rhs;
	static dist_matrix_type* m_loc_matrix;
	static dist_vector_type *m_loc_lhs, *m_loc_rhs;
	static reader_type* m_reader;
	static filename_type m_file_name;
	static distribution_type* m_distribution;
	static solver_type* m_solver;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		if(ParUtils::iAmMaster())
		{
			m_file_name = "../src/tests/testcases/EXXON_MM_EN_1.mtx";
			m_reader = new reader_type();
			m_matrix = m_reader->Read(m_file_name.c_str());
			
			m_lhs = new SeqVector<data_type>(m_matrix->getNbRows(),1);
			m_rhs = new SeqVector<data_type>(m_matrix->getNbRows(),1);
			m_lhs->fill(1.);
			m_rhs->fill(0.);
			m_end = std::chrono::system_clock::now();
			
			double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
			YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
		}

		m_distribution = new distribution_type();
    m_loc_matrix = m_distribution->DistributeMatrix(*m_matrix);
    m_loc_lhs = m_distribution->DistributeVector(*m_lhs);
    m_loc_rhs = m_distribution->DistributeVector(*m_rhs);

		delete m_matrix;
		delete m_lhs;
		delete m_rhs;
		delete m_reader;

		m_solver = new solver_type();
		m_solver->setMaxIterations(500);
		m_solver->setup(*m_loc_matrix, *m_loc_lhs, *m_loc_rhs);

		m_end = std::chrono::system_clock::now();

		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_loc_matrix;
		delete m_loc_lhs;
		delete m_loc_rhs;
		delete m_solver;
		//delete m_distribution;
	}
};

DistBiCGStabTest2MioNnzMKL::chrono_type DistBiCGStabTest2MioNnzMKL::m_start;
DistBiCGStabTest2MioNnzMKL::chrono_type DistBiCGStabTest2MioNnzMKL::m_end;
DistBiCGStabTest2MioNnzMKL::seq_matrix_type* DistBiCGStabTest2MioNnzMKL::m_matrix = 0;
DistBiCGStabTest2MioNnzMKL::seq_vector_type* DistBiCGStabTest2MioNnzMKL::m_lhs = 0;
DistBiCGStabTest2MioNnzMKL::seq_vector_type* DistBiCGStabTest2MioNnzMKL::m_rhs = 0;
DistBiCGStabTest2MioNnzMKL::dist_matrix_type* DistBiCGStabTest2MioNnzMKL::m_loc_matrix = 0;
DistBiCGStabTest2MioNnzMKL::dist_vector_type* DistBiCGStabTest2MioNnzMKL::m_loc_lhs = 0;
DistBiCGStabTest2MioNnzMKL::dist_vector_type* DistBiCGStabTest2MioNnzMKL::m_loc_rhs = 0;
DistBiCGStabTest2MioNnzMKL::reader_type* DistBiCGStabTest2MioNnzMKL::m_reader = 0;
DistBiCGStabTest2MioNnzMKL::filename_type DistBiCGStabTest2MioNnzMKL::m_file_name = "";
DistBiCGStabTest2MioNnzMKL::distribution_type* DistBiCGStabTest2MioNnzMKL::m_distribution = 0;
DistBiCGStabTest2MioNnzMKL::solver_type* DistBiCGStabTest2MioNnzMKL::m_solver = 0;

TEST_F(DistBiCGStabTest2MioNnzMKL, DistBiCGStab)
{
	m_solver->solve(*m_loc_matrix, *m_loc_lhs, *m_loc_rhs);
}

#endif
