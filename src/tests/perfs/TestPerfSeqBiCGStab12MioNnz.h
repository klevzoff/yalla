#ifndef yalla_test_perf_seq_bicgstab_12_mio_nnz_h
#define yalla_test_perf_seq_bicgstab_12_mio_nnz_h

class SeqBiCGStabTest12MioNnz
: public testing::Test
{
 public:
	typedef double data_type;
	typedef dCSRMatrix matrix_type;
	typedef SeqVector<data_type> vector_type;
	typedef SparseMatrixReaderMMFormat<matrix_type> reader_type;
	typedef std::chrono::time_point<std::chrono::high_resolution_clock> chrono_type;
	typedef std::string filename_type;
	typedef BiCGStab<matrix_type> solver_type;
 protected:
	static chrono_type m_start;
	static chrono_type m_end;
	static matrix_type *m_matrix;
	static vector_type *m_lhs, *m_rhs;
	static reader_type* m_reader;
	static filename_type m_file_name;
	static solver_type* m_solver;

	static void SetUpTestCase()
	{
		m_start = std::chrono::system_clock::now();
		m_file_name = "../src/tests/testcases/EXXON_CS_1.mtx";
		m_reader = new reader_type();
		m_matrix = m_reader->Read(m_file_name.c_str());

		m_lhs = new SeqVector<data_type>(m_matrix->getNbRows(),1);
		m_rhs = new SeqVector<data_type>(m_matrix->getNbRows(),1);
		m_lhs->fill(1.);
		m_rhs->fill(0.);

		m_solver = new solver_type();
		m_solver->setMaxIterations(500);
		m_solver->setup(*m_matrix, *m_lhs, *m_rhs);

		m_end = std::chrono::system_clock::now();
		double elapsed_time = std::chrono::duration_cast<std::chrono::nanoseconds>(m_end-m_start).count()/1e9;
		YALLA_LOG("SetUp Time: " << elapsed_time << " (s)");
	}

	static void TearDownTestCase()
	{
		delete m_matrix;
		delete m_lhs;
		delete m_rhs;
		delete m_reader;
		delete m_solver;
	}
};

SeqBiCGStabTest12MioNnz::chrono_type SeqBiCGStabTest12MioNnz::m_start;
SeqBiCGStabTest12MioNnz::chrono_type SeqBiCGStabTest12MioNnz::m_end;
SeqBiCGStabTest12MioNnz::matrix_type* SeqBiCGStabTest12MioNnz::m_matrix = 0;
SeqBiCGStabTest12MioNnz::vector_type* SeqBiCGStabTest12MioNnz::m_lhs = 0;
SeqBiCGStabTest12MioNnz::vector_type* SeqBiCGStabTest12MioNnz::m_rhs = 0;
SeqBiCGStabTest12MioNnz::reader_type* SeqBiCGStabTest12MioNnz::m_reader = 0;
SeqBiCGStabTest12MioNnz::solver_type* SeqBiCGStabTest12MioNnz::m_solver = 0;
SeqBiCGStabTest12MioNnz::filename_type SeqBiCGStabTest12MioNnz::m_file_name = "";

TEST_F(SeqBiCGStabTest12MioNnz, SeqBiCGStab)
{
	m_solver->solve(*m_matrix, *m_lhs, *m_rhs);
}

#endif
