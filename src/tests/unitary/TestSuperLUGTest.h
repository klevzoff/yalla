#pragma once
#ifndef yalla_test_superlu_gtest_h
#define yalla_test_superlu_gtest_h

#ifdef WITH_SUPERLU

#include <assert.h>
#include "yalla/LinearSolver/SuperLU/SuperLU.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/Utils/OperatorOverloading/VectorOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "yalla/IO/console/log.h"
#include "gallery/Gallery.h"

#ifdef WITH_GTEST

YALLA_USING_NAMESPACE(yalla)

TEST(SeqLinearSolver, SeqSuperLUCSR)
{
#ifndef WITH_MPI
	const int size = 5;
  typedef dCSRMatrix CSRMatrix;
  typedef PromoteToVector<CSRMatrix, double>::data_type SeqVector;

	SuperLU mySuperLU;
	CSRMatrix* A = Gallery::create2DLaplace<CSRMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;

	mySuperLU.setup(*A,b,x);
	mySuperLU.solve(*A, b, x);

	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);

	delete A;
#else
	YALLA_ERR("Error, this test is supposed to be run in sequential");
#endif
}

#endif

#endif

#endif