#pragma once
#ifndef yalla_test_dist_vectors_gtest_h
#define yalla_test_dist_vectors_gtest_h

#include <cmath>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/Math/Math.h"
#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"
#include "yalla/Utils/OperatorOverloading/VectorOperatorOverloading.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Dist/DistDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMKLMatrix.h"
#include "yalla/Utils/DataDistribution/DataDistribution.h"
#include "yalla/Utils/DataDistribution/RowDistribution.h"
#include "yalla/Utils/DataDistribution/ColumnDistribution.h"
#include "gallery/Gallery.h"
#include "yalla/IO/console/log.h"
#ifdef WITH_GTEST
#include "gtest/gtest.h"

YALLA_USING_NAMESPACE(yalla)

TEST(DistVectorTest, DistVector)
{
#ifdef WITH_MPI
	const int numProc = ParUtils::getNumProc();
	if(numProc<2)
		YALLA_ERR("Error, this test is designed to run on at least 2 procs");
	typedef dDenseVector myVectorType;
	typedef dDistDenseVector myDistVectorType;
	typedef myVectorType::data_type data_type;
	typedef dDenseMatrixRowMajor denseMatrix;
	typedef dCSRMatrix CSRMatrix;
	typedef dDistCSRMatrix CSRDistMatrix;

	const int size = 5;
	const int procRank = ParUtils::getProcRank();
	const int actualSize = size*numProc;
	const int nbCols = 1;
	const data_type val = 2;

	int* mapping = new int[size];
	for(int i=0;i<size;++i)
		mapping[i] = i+size*procRank;

	data_type* datas = new data_type[size];
	for(int i=0;i<size;++i)
		datas[i] = i+size*procRank+1;

	data_type* controlDatas = new data_type[actualSize];
	for(int i=0;i<actualSize;++i)
		controlDatas[i] = i+1;

	myDistVectorType distVector;
	myDistVectorType distVector1(size,nbCols,size,actualSize,nbCols,mapping,0);
	myDistVectorType distVector2(size,nbCols,size,actualSize,nbCols,mapping,datas,0);
	myDistVectorType distVector3(distVector2);
	dDenseVector controlVector(actualSize,nbCols,controlDatas);

	//EXPECT_EQ(distVector2,distVector3);
	EXPECT_EQ(distVector1.getNbRows(),size);
	EXPECT_EQ(distVector1.getSize(),size);
	EXPECT_EQ(distVector1.getNbCols(),nbCols);
	EXPECT_EQ(distVector1.getGlobalNbRows(),actualSize);
	EXPECT_EQ(distVector1.getGlobalNbCols(),nbCols);

	for(int i=0;i<size;++i)
		EXPECT_EQ(distVector2(i),controlVector(distVector2.getMapping(i)));

	for(int i=0;i<size;++i)
		distVector1(i) = distVector2(i);

	for(int i=0;i<size;++i)
		EXPECT_EQ(distVector1(i),distVector2(i));

	distVector1.fill(val);

	for(int i=0;i<size;++i)
		EXPECT_EQ(distVector1(i),val);

	distVector1.fill(datas);

	for(int i=0;i<size;++i)
		EXPECT_EQ(distVector1(i),datas[i]);

	// Test operateurs
	// Operateur + et =
	distVector = distVector3 + distVector2;
	for(int i=0;i<distVector.getSize();++i)
		EXPECT_EQ(distVector(i),distVector3(i) + distVector2(i));

	distVector = distVector3 - distVector2;
	for(int i=0;i<distVector.getSize();++i)
		EXPECT_EQ(distVector(i),distVector3(i) - distVector2(i));

	distVector = distVector3 * distVector2;
	for(int i=0;i<distVector.getSize();++i)
		EXPECT_EQ(distVector(i),distVector3(i) * distVector2(i));

	distVector = distVector3 / distVector2;
	for(int i=0;i<distVector.getSize();++i)
		EXPECT_EQ(distVector(i),distVector3(i) / distVector2(i));

	// Operateurs scalaires
	// Operateur * et =
	distVector = distVector3 * val;
	for(int i=0;i<distVector.getSize();++i)
		EXPECT_EQ(distVector(i),distVector3(i) * val);

	distVector = val * distVector3;
	for(int i=0;i<distVector.getSize();++i)
		EXPECT_EQ(distVector(i),distVector3(i) * val);

	// Operateur / et =
	distVector = distVector3 / val;
	for(int i=0;i<distVector.getSize();++i)
		EXPECT_EQ(distVector(i),distVector3(i) / val);

	denseMatrix* myDenseMatrix = Gallery::create1DLaplace<denseMatrix>(actualSize);
	CSRMatrix* myCSRMatrix = Gallery::create1DLaplace<CSRMatrix>(actualSize);

	DataDistribution<RowDistribution<CSRDistMatrix::data_type> > rowDistrib;
	CSRDistMatrix* CSRDistribMatrix = rowDistrib.DistributeMatrix(*myCSRMatrix);
	myDistVectorType* distVector4 = rowDistrib.DistributeVector(controlVector);

	dDenseVector matVecProdControl = (*myDenseMatrix)*controlVector;
	myDistVectorType CSRMatVec = (*CSRDistribMatrix) * (*distVector4);

	for(int i=0;i<CSRMatVec.getOwnSize();++i)
		EXPECT_EQ(matVecProdControl(CSRMatVec.getMapping(i)),CSRMatVec(i));

	data_type dot = BLAS::dot(*distVector4,*distVector4);
	data_type dotControl = BLAS::dot(controlVector,controlVector);

	EXPECT_EQ(dot,dotControl);

	data_type nrm = BLAS::nrm2(*distVector4);
	data_type nrmControl = std::sqrt(dotControl);
	EXPECT_EQ(nrm,nrmControl);

	delete[] mapping;
	delete[] datas;
	delete[] controlDatas;
	delete myDenseMatrix;
	delete myCSRMatrix;
	delete CSRDistribMatrix;
	delete distVector4;

#else
	YALLA_ERR("Error, this test is designed to run in parallel");
#endif
}

#endif

#endif
