#pragma once
#ifndef yalla_test_matrices_gtest_h
#define yalla_test_matrices_gtest_h

#include <assert.h>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixGraphOperatorOverloading.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixColMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistDenseMatrixRowMajor.h"

#ifdef WITH_GTEST
#include "gtest/gtest.h"

YALLA_USING_NAMESPACE(yalla)

TEST(SeqMatrixTest, DenseMatrixRowMajor)
{
	typedef sDenseMatrixRowMajor denseMatrixRow;
	typedef denseMatrixRow::data_type data_type;
	
	const int nbRows = 5;
	const int nbCols = 5;
	const data_type val = 2;
	
	data_type* myData = new data_type[nbRows*nbCols];
	
	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
			myData[i*nbCols + j] = i*j+i;
	}
		
	// Appel de tous les constructeur
	denseMatrixRow myRowMatrix;
	denseMatrixRow myRowMatrix1(nbRows,nbRows);
	denseMatrixRow myRowMatrix2(nbRows,nbRows,10);
	denseMatrixRow myRowMatrix3(nbRows,nbCols,myData);
	denseMatrixRow myRowMatrix4(myRowMatrix3);
	denseMatrixRow myRowMatrix5;

	// Appel des methodes
	EXPECT_EQ(myRowMatrix3.getNbRows(),nbRows);
	EXPECT_EQ(myRowMatrix3.getNbCols(),nbCols);

	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
		{
			EXPECT_EQ((i*j+i),myRowMatrix3(i,j));
		}
	}

	myRowMatrix2.fill(val);
	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
		{
			EXPECT_EQ(val,myRowMatrix2(i,j));
		}
	}

	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
		{
			EXPECT_EQ((i*j+i),myRowMatrix3(i,j));
		}
	}

	myRowMatrix2(nbRows/2,nbCols/2)=1;
	EXPECT_EQ(myRowMatrix2(nbRows/2,nbCols/2),1);

	// Test des operateurs
	// Operateurs matriciels
	// operateur + et =
	myRowMatrix5 = myRowMatrix4 + myRowMatrix3;
	for(int i=0;i<myRowMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myRowMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myRowMatrix5(i,j),myRowMatrix4(i,j)+myRowMatrix3(i,j));
		}
	}
		
	// operateur - et =
	myRowMatrix5 = myRowMatrix4 - myRowMatrix3;
	for(int i=0;i<myRowMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myRowMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myRowMatrix5(i,j),myRowMatrix4(i,j)-myRowMatrix3(i,j));
		}
	}

	// operateur * et =
	myRowMatrix5 = myRowMatrix4 * myRowMatrix3;
	denseMatrixRow productResult(myRowMatrix4.getNbRows(),myRowMatrix3.getNbCols());
	for(int i=0;i<myRowMatrix4.getNbRows();++i)
	{
		for(int j=0;j<myRowMatrix3.getNbCols();++j)
		{
			for(int k=0;k<myRowMatrix4.getNbCols();++k)
			{
				productResult(i,j) += myRowMatrix4(i,k) * myRowMatrix3(k,j);
			}
		}
	}

	for(int i=0;i<myRowMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myRowMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myRowMatrix5(i,j),productResult(i,j));
		}
	}

	// Operateurs scalaires
	// Operateurs * et =
	myRowMatrix5 = 2.*myRowMatrix4;
	for(int i=0;i<myRowMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myRowMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myRowMatrix5(i,j),2*myRowMatrix4(i,j));
		}
	}

	myRowMatrix5 = myRowMatrix4*2.;
	for(int i=0;i<myRowMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myRowMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myRowMatrix5(i,j),2*myRowMatrix4(i,j));
		}
	}

	// Operateurs / et =
	myRowMatrix5 = myRowMatrix4/2.;
	for(int i=0;i<myRowMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myRowMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myRowMatrix5(i,j),myRowMatrix4(i,j)/2.);
		}
	}

	EXPECT_EQ(myRowMatrix4,myRowMatrix3);

	delete[] myData;
}

TEST(SeqMatrixTest, DenseMatrixColMajor)
{

	typedef sDenseMatrixColMajor denseMatrixCol;
	typedef denseMatrixCol::data_type data_type;

	const int nbRows = 5;
	const int nbCols = 5;
	const data_type val = 2;

	data_type* myData2 = new data_type[nbRows*nbCols];

	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
		{
			myData2[j*nbRows + i] = i*j+i;
		}
	}

	denseMatrixCol myColMatrix;
	denseMatrixCol myColMatrix1(nbRows,nbRows);
	denseMatrixCol myColMatrix2(nbRows,nbRows,10);
	denseMatrixCol myColMatrix3(nbRows,nbCols,myData2);
	denseMatrixCol myColMatrix4(myColMatrix3);
	denseMatrixCol myColMatrix5;

	// Appel des methodes
	EXPECT_EQ(myColMatrix3.getNbRows(),nbRows);
	EXPECT_EQ(myColMatrix3.getNbCols(),nbCols);

	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
		{
			EXPECT_EQ((i*j+i),myColMatrix3(i,j));
		}
	}

	myColMatrix2.fill(2);
	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
		{
			EXPECT_EQ(val,myColMatrix2(i,j));
		}
	}

	for(int i=0;i<nbRows;++i)
	{
		for(int j=0;j<nbCols;++j)
		{
			EXPECT_EQ((i*j+i),myColMatrix3(i,j));
		}
	}

	myColMatrix2(nbRows/2,nbCols/2)=1;
	EXPECT_EQ(myColMatrix2(nbRows/2,nbCols/2),1);

	// Test des operateurs
	// Operateurs matriciels
	// operateur + et =
	myColMatrix5 = myColMatrix4 + myColMatrix3;
	for(int i=0;i<myColMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myColMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myColMatrix5(i,j),myColMatrix4(i,j)+myColMatrix3(i,j));
		}
	}

	// operateur - et =
	myColMatrix5 = myColMatrix4 - myColMatrix3;
	for(int i=0;i<myColMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myColMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myColMatrix5(i,j),myColMatrix4(i,j)-myColMatrix3(i,j));
		}
	}

	// operateur * et =
	myColMatrix5 = myColMatrix4 * myColMatrix3;
	denseMatrixCol productResultCol(myColMatrix4.getNbRows(),myColMatrix3.getNbCols());
	for(int i=0;i<myColMatrix4.getNbRows();++i)
	{
		for(int j=0;j<myColMatrix3.getNbCols();++j)
		{
			for(int k=0;k<myColMatrix4.getNbCols();++k)
			{
				productResultCol(i,j) += myColMatrix4(i,k) * myColMatrix3(k,j);
			}
		}
	}

	for(int i=0;i<myColMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myColMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myColMatrix5(i,j),productResultCol(i,j));
		}
	}

	// Operateurs scalaires
	// Operateurs * et =
	myColMatrix5 = 2*myColMatrix4;
	for(int i=0;i<myColMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myColMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myColMatrix5(i,j),2*myColMatrix4(i,j));
		}
	}

	myColMatrix5 = myColMatrix4*2;
	for(int i=0;i<myColMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myColMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myColMatrix5(i,j),2*myColMatrix4(i,j));
		}
	}

	// Operateurs / et =
	myColMatrix5 = myColMatrix4/2;
	for(int i=0;i<myColMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myColMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(myColMatrix5(i,j),myColMatrix4(i,j)/2.);
		}
	}

	EXPECT_EQ(myColMatrix4,myColMatrix3);

	delete[] myData2;
}

TEST(SeqMatrixTest, CSRMatrix)
{
	typedef sCSRMatrix CSRMatrix;
	typedef sDenseMatrixRowMajor denseMatrixRow;
	typedef CSRMatrix::data_type data_type;

	const int CSRNbRows = 4;
	const int CSRNbCols = 4;
	const int nnz = 7;

	data_type* CSRData = new data_type[nnz];
	int* CSRRow = new int[CSRNbRows+1];
	int* CSRCols = new int[nnz];
	data_type* denseData = new data_type[CSRNbRows*CSRNbRows];

	for(int i=0;i<CSRNbRows*CSRNbRows;++i)
		denseData[i] = 0;

	CSRData[0] = 1;
	CSRData[1] = 2;
	CSRData[2] = 3;
	CSRData[3] = 4;
	CSRData[4] = 5;
	CSRData[5] = 6;
	CSRData[6] = 7;

	denseData[1] = 1;
	denseData[4] = 2;
	denseData[6] = 3;
	denseData[9] = 4;
	denseData[11] = 5;
	denseData[12] = 6;
	denseData[14] = 7;

	CSRRow[0] = 0;
	CSRRow[1] = 1;
	CSRRow[2] = 3;
	CSRRow[3] = 5;
	CSRRow[4] = 7;

	CSRCols[0] = 1;
	CSRCols[1] = 0;
	CSRCols[2] = 2;
	CSRCols[3] = 1;
	CSRCols[4] = 3;
	CSRCols[5] = 0;
	CSRCols[6] = 2;

	// Pour verifier
	denseMatrixRow myDenseMatrix(CSRNbRows,CSRNbCols,denseData);
	// Appel de tous les constructeurs et des methodes pour construire les matrices
	CSRMatrix myCSRMatrix;
	CSRMatrix myCSRMatrix1(CSRNbRows,CSRNbRows);
	CSRMatrix myCSRMatrix2(CSRNbRows,CSRNbCols);
	CSRMatrix myCSRMatrix3(CSRNbRows,CSRNbCols,nnz);
	CSRMatrix myCSRMatrix4(CSRNbRows,CSRNbCols,nnz,CSRRow,CSRCols);
	CSRMatrix myCSRMatrix5(CSRNbRows,CSRNbCols,nnz,CSRRow,CSRCols,CSRData);
	CSRMatrix myCSRMatrix6(myCSRMatrix5);
	CSRMatrix myCSRMatrix7;

	myCSRMatrix3.setValue(0,1,1);
	myCSRMatrix3.setValue(1,0,2);
	myCSRMatrix3.setValue(1,2,3);
	myCSRMatrix3.setValue(2,1,4);
	myCSRMatrix3.setValue(2,3,5);
	myCSRMatrix3.setValue(3,0,6);
	myCSRMatrix3.setValue(3,2,7);
	myCSRMatrix3.finalize();

	EXPECT_EQ(myCSRMatrix3,myDenseMatrix);

	myCSRMatrix4.fill(2);

	// Operateurs
	myCSRMatrix7 = myCSRMatrix5 + myCSRMatrix6;
	for(int i=0;i<myCSRMatrix7.getNbRows();++i)
	{
		for(int j=0;j<myCSRMatrix7.getNbCols();++j)
		{
			EXPECT_EQ(myCSRMatrix7(i,j),myCSRMatrix5(i,j)+myCSRMatrix6(i,j));
		}
	}

	myCSRMatrix7 = myCSRMatrix5 - myCSRMatrix6;
	for(int i=0;i<myCSRMatrix7.getNbRows();++i)
	{
		for(int j=0;j<myCSRMatrix7.getNbCols();++j)
		{
			EXPECT_EQ(myCSRMatrix7(i,j),myCSRMatrix5(i,j)-myCSRMatrix6(i,j));
		}
	}

	myCSRMatrix7 = myCSRMatrix5 * myCSRMatrix6;
	denseMatrixRow productResult(myCSRMatrix7.getNbRows(),myCSRMatrix7.getNbCols());
	for(int i=0;i<myCSRMatrix5.getNbRows();++i)
	{
		for(int j=0;j<myCSRMatrix6.getNbCols();++j)
		{
			for(int k=0;k<myCSRMatrix5.getNbCols();++k)
			{
				productResult(i,j) += myCSRMatrix5(i,k) * myCSRMatrix6(k,j);
			}
		}
	}

	for(int i=0;i<myCSRMatrix7.getNbRows();++i)
	{
		for(int j=0;j<myCSRMatrix7.getNbCols();++j)
		{
			EXPECT_EQ(myCSRMatrix7(i,j),productResult(i,j));
		}
	}

	myCSRMatrix7 = myCSRMatrix6*2;
	for(int i=0;i<myCSRMatrix7.getNbRows();++i)
	{
		for(int j=0;j<myCSRMatrix7.getNbCols();++j)
		{
			EXPECT_EQ(myCSRMatrix7(i,j),2*myCSRMatrix6(i,j));
		}
	}

	myCSRMatrix7 = 2*myCSRMatrix6;
	for(int i=0;i<myCSRMatrix7.getNbRows();++i)
	{
		for(int j=0;j<myCSRMatrix7.getNbCols();++j)
		{
			EXPECT_EQ(myCSRMatrix7(i,j),2*myCSRMatrix6(i,j));
		}
	}

	myCSRMatrix7 = myCSRMatrix6/2;
	for(int i=0;i<myCSRMatrix7.getNbRows();++i)
	{
		for(int j=0;j<myCSRMatrix7.getNbCols();++j)
		{
			EXPECT_EQ(myCSRMatrix7(i,j),myCSRMatrix6(i,j)/2.);
		}
	}

	delete[] CSRData;
	delete[] CSRRow;
	delete[] CSRCols;
	delete[] denseData;
}

#endif

#endif

