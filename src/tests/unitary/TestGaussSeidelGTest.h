#pragma once
#ifndef yalla_test_gauss_seidel_gtest_h
#define yalla_test_gauss_seidel_gtest_h

#include <assert.h>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToVector.h"
#include "yalla/Utils/TypeTraits/PromoteToDist.h"
#include "yalla/Utils/OperatorOverloading/VectorOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixColMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMatrix.h"
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"
#include "yalla/LinearSolver/Relaxation/GaussSeidel/GaussSeidel.h"
#ifdef WITH_MPI
#include "yalla/Utils/DataDistribution/DataDistribution.h"
#include "yalla/Utils/DataDistribution/RowDistribution.h"
#include "yalla/Utils/DataDistribution/ColumnDistribution.h"
#endif
#include "yalla/Utils/Math/Math.h"
#include "gallery/Gallery.h"
#include "yalla/IO/console/log.h"

#ifdef WITH_GTEST
#include "gtest/gtest.h"

YALLA_USING_NAMESPACE(yalla)

TEST(SeqLinearSolver, SeqGaussSeidelRowMajor)
{
#ifndef WITH_MPI
	const int size = 5;
  typedef dDenseMatrixRowMajor rowMajorMatrix;
  typedef PromoteToVector<rowMajorMatrix, double>::data_type SeqVector;

  GaussSeidel myGaussSeidel;

	rowMajorMatrix* A = Gallery::create2DLaplace<rowMajorMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;
	
	myGaussSeidel.setMaxIterations(1000);
	myGaussSeidel.setTolerance(1e-10);
	myGaussSeidel.solve(*A, b, x);
	
	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);

	delete A;
#else
	YALLA_ERR("Error, this test is supposed to be run in sequential");
#endif
}

TEST(SeqLinearSolver, SeqGaussSeidelColMajor)
{
#ifndef WITH_MPI
	const int size = 5;
  typedef dDenseMatrixRowMajor colMajorMatrix;
  typedef PromoteToVector<colMajorMatrix, double>::data_type SeqVector;

  GaussSeidel myGaussSeidel;

	colMajorMatrix* A = Gallery::create2DLaplace<colMajorMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;
	
	myGaussSeidel.setMaxIterations(1000);
	myGaussSeidel.setTolerance(1e-10);
	myGaussSeidel.solve(*A, b, x);
	
	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);

	delete A;
#else
	YALLA_ERR("Error, this test is supposed to be run in sequential");
#endif
}

TEST(SeqLinearSolver, SeqGaussSeidelCSR)
{
#ifndef WITH_MPI
	const int size = 5;
  typedef dCSRMatrix CSRMatrix;
  typedef PromoteToVector<CSRMatrix, double>::data_type SeqVector;

  GaussSeidel myGaussSeidel;

	CSRMatrix* A = Gallery::create2DLaplace<CSRMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;
	
	myGaussSeidel.setMaxIterations(1000);
	myGaussSeidel.setTolerance(1e-10);
	myGaussSeidel.solve(*A, b, x);
	
	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);

	delete A;
#else
	YALLA_ERR("Error, this test is supposed to be run in sequential");
#endif
}

#endif

#endif
