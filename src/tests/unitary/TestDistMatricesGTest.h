#pragma once
#ifndef yalla_test_dist_matrices_gtest_h
#define yalla_test_dist_matrices_gtest_h

#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixGraphOperatorOverloading.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMKLMatrix.h"
#include "yalla/Utils/DataDistribution/DataDistribution.h"
#include "yalla/Utils/DataDistribution/RowDistribution.h"
#include "yalla/Utils/DataDistribution/ColumnDistribution.h"
#include "gallery/Gallery.h"
#include "yalla/IO/console/log.h"

#ifdef WITH_GTEST
#include "gtest/gtest.h"

using namespace yalla;

TEST(DistMatrixTest, DenseMatrixRowMajor)
{
#ifdef WITH_MPI
	const int size = 5;
	typedef dDenseMatrixRowMajor denseMatrixRow;
	typedef dDistDenseMatrixRowMajor distDenseMatrixRow;
	typedef denseMatrixRow::data_type data_type;

	const int numProc = ParUtils::getNumProc();
	if(numProc<2)
		YALLA_ERR("Error, this test is designed to run on at least 2 procs");
	const int procRank = ParUtils::getProcRank();
	const int actualSize = size*numProc;
	const data_type val = 2;
	denseMatrixRow seqMatrix(actualSize,actualSize);
	denseMatrixRow seqMatrix2(actualSize,actualSize);
	seqMatrix2.fill(val);

	for(int i=0;i<actualSize;++i)
	{
		for(int j=0;j<actualSize;++j)
		{
			seqMatrix(i,j) = i*j+i;
		}
	}

	int* colMapping = 0;
	data_type* data = 0;
	const int numColsByProc = actualSize/numProc;

	colMapping = new int[numColsByProc];
	data = new data_type[numColsByProc*actualSize];
		
	for(int i=0;i<numColsByProc;++i)
		colMapping[i] = procRank*numColsByProc+i;
		
	for(int i=0;i<actualSize;++i)
	{
		for(int j=0;j<numColsByProc;++j)
		{
			data[i*numColsByProc+j] = i*colMapping[j]+i;
		}
	}

	distDenseMatrixRow distribMatrix(actualSize,numColsByProc,actualSize,actualSize,colMapping,data);
		
	DataDistribution<ColumnDistribution> distrib;
	distDenseMatrixRow* distribMatrixPrime = distrib.DistributeMatrix(seqMatrix);

	EXPECT_EQ(distribMatrix,*distribMatrixPrime);
		
	// Appel de tous les constructeur
	distDenseMatrixRow distribMatrix1(actualSize,numColsByProc,actualSize,actualSize,colMapping);
	distDenseMatrixRow distribMatrix2(distribMatrix);
	distDenseMatrixRow distribMatrix3(distribMatrix.getMatrixGraph());
	distDenseMatrixRow distribMatrix4;

	EXPECT_EQ(distribMatrix,distribMatrix2);
	EXPECT_EQ(distribMatrix1,distribMatrix3);

	// Appel des methodes
	EXPECT_EQ(distribMatrix.getNbRows(),actualSize);
	EXPECT_EQ(distribMatrix.getNbCols(),static_cast<int>(numColsByProc));
	EXPECT_EQ(distribMatrix.getGlobalNbRows(),actualSize);
	EXPECT_EQ(distribMatrix.getGlobalNbCols(),actualSize);

	for(int i=0;i<actualSize;++i)
	{
		for(int j=0;j<numColsByProc;++j)
		{
			EXPECT_EQ((i*colMapping[j]+i),distribMatrix2(i,j));
		}
	}

	for(int j=0;j<numColsByProc;++j)
	{
		EXPECT_EQ(colMapping[j],distribMatrix2.getMapping(j));
	}

	distribMatrix3.fill(val);
	for(int i=0;i<actualSize;++i)
	{
		for(int j=0;j<numColsByProc;++j)
		{
			EXPECT_EQ(val,distribMatrix3(i,j));
		}
	}

	distribMatrix2(actualSize/2,numColsByProc/2)=1;
	EXPECT_EQ(distribMatrix2(actualSize/2,numColsByProc/2),1);

	// Test des operateurs
	// Operateurs matriciels
	// operateur + et =
	distribMatrix4 = distribMatrix + distribMatrix3;
	for(int i=0;i<distribMatrix4.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix4.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix4(i,j),distribMatrix(i,j)+distribMatrix3(i,j));
			EXPECT_EQ(distribMatrix4(i,j),seqMatrix(i,distribMatrix4.getMapping(j))+val);
		}
	}
		
	// operateur - et =
	distribMatrix4 = distribMatrix - distribMatrix3;
	for(int i=0;i<distribMatrix4.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix4.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix4(i,j),distribMatrix(i,j)-distribMatrix3(i,j));
			EXPECT_EQ(distribMatrix4(i,j),seqMatrix(i,distribMatrix4.getMapping(j))-val);
		}
	}

	// operateur * et =
	distribMatrix4 = distribMatrix * distribMatrix3;
	denseMatrixRow productResult = seqMatrix*seqMatrix2;

	for(int i=0;i<distribMatrix4.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix4.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix4(i,j),productResult(i,distribMatrix4.getMapping(j)));
		}
	}

	// Operateurs scalaires
	// Operateurs * et =
	distribMatrix4 = 2.*distribMatrix;
	for(int i=0;i<distribMatrix4.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix4.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix4(i,j),2*distribMatrix(i,j));
		}
	}

	distribMatrix4 = distribMatrix*2.;
	for(int i=0;i<distribMatrix4.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix4.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix4(i,j),2*distribMatrix(i,j));
		}
	}

	// Operateurs / et =
	distribMatrix4 = distribMatrix/2.;
	for(int i=0;i<distribMatrix4.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix4.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix4(i,j),distribMatrix(i,j)/2.);
		}
	}

	delete distribMatrixPrime;
	delete[] data;
	delete[] colMapping;
#else
	YALLA_ERR("Error, this test is designed to run in parallel");
#endif
}

TEST(DistMatrixTest, CSRMatrix)
{
#ifdef WITH_MPI
	const int size = 5;
	typedef dCSRMatrix CSRMatrix;
	typedef dDistCSRMatrix distCSRMatrix;
	typedef distCSRMatrix::data_type data_type;

	const int numProc = ParUtils::getNumProc();
	if(numProc<2)
		YALLA_ERR("Error, this test is designed to run on at least 2 procs");
	const int procRank = ParUtils::getProcRank();
	const int actualSize = size*numProc;
	const data_type val = 2;
	CSRMatrix* seqMatrix = Gallery::create2DLaplace<CSRMatrix>(actualSize);
	CSRMatrix* seqMatrix2 = Gallery::create2DLaplace<CSRMatrix>(actualSize);
	seqMatrix2->fill(val);
		
	int* rowMapping = new int[size*size*numProc];
	for(int i=0;i<size*size*numProc;++i)
		rowMapping[i] = size*size*numProc*procRank+i;
	
	DataDistribution<RowDistribution<data_type> > distrib;
	distCSRMatrix* distribMatrix = distrib.DistributeMatrix(*seqMatrix);
	distCSRMatrix::impl_type::graph_type matrixGraph = distribMatrix->getMatrixGraph();
	
	// Appel de tous les constructeur
	distCSRMatrix distribMatrix1(size*size*numProc,distribMatrix->getNbCols(),distribMatrix->getNnz(),actualSize*actualSize,actualSize*actualSize,distribMatrix->getMapping(),matrixGraph.getRowPtr(), matrixGraph.getColPtr());
	distCSRMatrix distribMatrix2(size*size*numProc,distribMatrix->getNbCols(),distribMatrix->getNnz(),actualSize*actualSize,actualSize*actualSize,distribMatrix->getMapping(),matrixGraph.getRowPtr(), matrixGraph.getColPtr(),distribMatrix->getPointeur());
	distCSRMatrix distribMatrix3(*distribMatrix);
	distCSRMatrix distribMatrix4(distribMatrix->getMatrixGraph());
	distCSRMatrix distribMatrix5;
	
	EXPECT_EQ(*distribMatrix,distribMatrix2);
	EXPECT_EQ(*distribMatrix,distribMatrix3);
	EXPECT_EQ(distribMatrix1,distribMatrix4);
		
	// Appel des methodes
	EXPECT_EQ(distribMatrix->getNbRows(),static_cast<int>(size*size*numProc));
	//EXPECT_EQ(distribMatrix->getNbCols(),actualSize*actualSize);
	EXPECT_EQ(distribMatrix->getGlobalNbRows(),actualSize*actualSize);
	EXPECT_EQ(distribMatrix->getGlobalNbCols(),actualSize*actualSize);

	for(int i=0;i<distribMatrix->getNbRows();++i)
	{
		for(int j=0;j<distribMatrix->getNbCols();++j)
		{
			EXPECT_EQ((*distribMatrix)(i,j),(*seqMatrix)(distribMatrix->getMapping(i),distribMatrix->getMapping(j)));
		}
	}
	
	for(int i=0;i<size*size*numProc;++i)
	{
		EXPECT_EQ(rowMapping[i],distribMatrix2.getMapping(i));
	}
	
	distribMatrix1.fill(val);
	for(int i=0;i<distribMatrix1.getNbRows();++i)
	{
		for(int j=distribMatrix1.getMatrixGraph().getRowPtr(i);j<distribMatrix1.getMatrixGraph().getRowPtr(i+1);++j)
		{
			EXPECT_EQ(val,distribMatrix1(i,distribMatrix1.getMatrixGraph().getColPtr(j)));
		}
	}
	
	// Test des operateurs
	// Operateurs matriciels
	// operateur + et =
	distribMatrix5 = distribMatrix3 + distribMatrix1;
	for(int i=0;i<distribMatrix5.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix5(i,j),distribMatrix3(i,j)+distribMatrix1(i,j));
		}
	}
	
	// operateur - et =
	distribMatrix5 = distribMatrix3 - distribMatrix1;
	for(int i=0;i<distribMatrix5.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix5(i,j),distribMatrix3(i,j)-distribMatrix1(i,j));
		}
	}
	
	// operateur * et =
	YALLA_LOG("dist CSR SPMM not working any more due to changes in dist CSR matrices. To investigate!");
	/*
	distribMatrix5 = distribMatrix3 * distribMatrix1;
	CSRMatrix productResult = (*seqMatrix)*(*seqMatrix2);
	for(int i=0;i<distribMatrix5.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix5(i,j),productResult(distribMatrix5.getMapping(i),distribMatrix5.getMapping(j)));
		}
	}
	*/

	// Operateurs scalaires
	// Operateurs * et =
	distribMatrix5 = 2*distribMatrix3;
	for(int i=0;i<distribMatrix5.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix5(i,j),2*distribMatrix3(i,j));
		}
	}
	
	distribMatrix5 = distribMatrix3*2;
	for(int i=0;i<distribMatrix5.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix5(i,j),2*distribMatrix3(i,j));
		}
	}
	
	// Operateurs / et =
	distribMatrix5 = distribMatrix3/2;
	for(int i=0;i<distribMatrix5.getNbRows();++i)
	{
		for(int j=0;j<distribMatrix5.getNbCols();++j)
		{
			EXPECT_EQ(distribMatrix5(i,j),distribMatrix3(i,j)/2.);
		}
	}
	
	delete seqMatrix;
	delete seqMatrix2;
	delete distribMatrix;
	delete[] rowMapping;
#else
	YALLA_ERR("Error, this test is designed to run in parallel");
#endif
}
#endif

#endif
