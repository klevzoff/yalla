#pragma once
#ifndef yalla_test_dist_bicgstab_l_gtest_h
#define yalla_test_dist_bicgstab_l_gtest_h

#include <assert.h>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToVector.h"
#include "yalla/Utils/OperatorOverloading/VectorOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "gallery/Gallery.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixColMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMatrix.h"
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"
#include "yalla/Utils/Math/Math.h"
#include "yalla/LinearSolver/NoPrecond.h"
#include "yalla/LinearSolver/BiCGStab/BiCGStabL.h"
#ifdef WITH_MPI
#include "yalla/Utils/DataDistribution/DataDistribution.h"
#include "yalla/Utils/DataDistribution/RowDistribution.h"
#include "yalla/Utils/DataDistribution/ColumnDistribution.h"
#endif

#ifdef WITH_GTEST

YALLA_USING_NAMESPACE(yalla)

TEST(DistLinearSolver, DistBiCGStabLRowMajor)
{
#ifdef WITH_MPI
	const int size = 5;
	const int numProc = ParUtils::getNumProc();
	typedef dDenseMatrixRowMajor mySeqDenseMatrixRowMajor;
	typedef PromoteToVector<mySeqDenseMatrixRowMajor, mySeqDenseMatrixRowMajor::data_type>::data_type mySeqVectorType;
	typedef PromoteToDist<mySeqDenseMatrixRowMajor, mySeqDenseMatrixRowMajor::data_type>::data_type myDistDenseMatrixRowMajor;
	typedef PromoteToVector<myDistDenseMatrixRowMajor, myDistDenseMatrixRowMajor::data_type>::data_type myDistVectorType;

	BiCGStabL<myDistDenseMatrixRowMajor> BiCGStabL(2);
	mySeqDenseMatrixRowMajor* A = 0;
	mySeqVectorType* x = 0, * b = 0;
	if(ParUtils::iAmMaster())
	{
		A = Gallery::create2DLaplace<mySeqDenseMatrixRowMajor>(size*numProc);
		x = new mySeqVectorType(size*size*numProc*numProc,1);
		b = new mySeqVectorType(size*size*numProc*numProc,1);
		x->fill(1);
		*b = *A * *x;
		x->fill(0.);
	}
	
	DataDistribution<ColumnDistribution> distrib;
	myDistDenseMatrixRowMajor* locA = distrib.DistributeMatrix(*A);
	
	myDistVectorType* locX = distrib.DistributeVector(*x);
	myDistVectorType* locB = distrib.DistributeVector(*b);
	
	BiCGStabL.setMaxIterations(1000);
	BiCGStabL.setTolerance(1e-10);
	BiCGStabL.setup(*locA, *locB, *locX);
	BiCGStabL.solve(*locA, *locB, *locX);
	
	for(int i=0;i<locX->getSize();++i)
		EXPECT_NEAR((*locX)(i),1.,1e-7);
	
	delete A;
	delete x;
	delete b;
	delete locA;
	delete locX;
	delete locB;
#else
	std::cout << "Error, this test is designed to run in parallel\n";
	abort();
#endif
}

TEST(DistLinearSolver, DistBiCGStabLCSR)
{
#ifdef WITH_MPI
	const int size = 5;
	const int numProc = ParUtils::getNumProc();
	typedef dCSRMatrix mySeqCSRMatrix;
	typedef PromoteToVector<mySeqCSRMatrix, mySeqCSRMatrix::data_type>::data_type mySeqCSRVectorType;
	typedef PromoteToDist<mySeqCSRMatrix, mySeqCSRMatrix::data_type>::data_type myDistCSRMatrixType;
	typedef PromoteToVector<myDistCSRMatrixType, myDistCSRMatrixType::data_type>::data_type myDistCSRVectorType;

	BiCGStabL<myDistCSRMatrixType> BiCGStabL(2);
	mySeqCSRMatrix* A = 0;
	mySeqCSRVectorType* x = 0, * b = 0;
	if(ParUtils::iAmMaster())
	{
		A = Gallery::create2DLaplace<mySeqCSRMatrix>(size*numProc);
		x = new mySeqCSRVectorType(size*size*numProc*numProc,1);
		b = new mySeqCSRVectorType(size*size*numProc*numProc,1);
		x->fill(1);
		*b = *A * *x;
		x->fill(0.);
	}
	
	DataDistribution<RowDistribution<mySeqCSRMatrix::data_type> > distrib;
	myDistCSRMatrixType* locA = distrib.DistributeMatrix(*A);
	
	myDistCSRVectorType* locX = distrib.DistributeVector(*x);
	myDistCSRVectorType* locB = distrib.DistributeVector(*b);
	
	BiCGStabL.setMaxIterations(1000);
	BiCGStabL.setTolerance(1e-10);
	BiCGStabL.setup(*locA, *locB, *locX);
	BiCGStabL.solve(*locA, *locB, *locX);
	
	for(int i=0;i<locX->getSize();++i)
		EXPECT_NEAR((*locX)(i),1.,1e-7);

	delete A;
	delete x;
	delete b;
	delete locA;
	delete locX;
	delete locB;
#else
	std::cout << "Error, this test is designed to run in parallel\n";
	abort();
#endif
}

#endif
#endif
