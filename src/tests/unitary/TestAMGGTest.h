#pragma once
#ifndef yalla_test_amg_gtest_h
#define yalla_test_amg_gtest_h

#include <assert.h>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToVector.h"
#include "yalla/Utils/OperatorOverloading/VectorOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "gallery/Gallery.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixColMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/Utils/Math/Math.h"
#include "yalla/LinearSolver/Relaxation/NoSmoother.h"
#include "yalla/LinearSolver/AMG/Setup/CoarseningStrategy/StandardCoarsening.h"
#include "yalla/LinearSolver/AMG/Setup/Interpolation/DirectInterpolation.h"
#include "yalla/LinearSolver/AMG/Solve/Cycle/VCycle.h"
#include "yalla/LinearSolver/GaussianElimination/GaussianElimination.h"
#include "yalla/LinearSolver/Relaxation/GaussSeidel/GaussSeidel.h"
#include "yalla/LinearSolver/AMG/Setup/AMGSetup.h"
#include "yalla/LinearSolver/AMG/Solve/AMGSolver.h"
#include "yalla/LinearSolver/AMG/AMG.h"
#include "yalla/IO/console/log.h"

#ifdef WITH_GTEST
#include "gtest/gtest.h"

YALLA_USING_NAMESPACE(yalla)

TEST(SeqLinearSolver, SeqAMGRowMajor)
{
#ifndef WITH_MPI
  typedef dDenseMatrixRowMajor rowMajorMatrix;
  typedef PromoteToVector<rowMajorMatrix, double>::data_type SeqVector;
	const int size = 5;

	AMG<AMGSetup<rowMajorMatrix, StandardCoarsening<rowMajorMatrix>, DirectInterpolation<rowMajorMatrix> >, AMGSolver<GaussianElimination, GaussSeidel> > myAMG;
	rowMajorMatrix* A = Gallery::create2DLaplace<rowMajorMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;
	
	myAMG.setup(*A,b,x);
	myAMG.setMaxIterations(100);
	myAMG.setTolerance(1e-10);
	myAMG.solve(*A, b, x);
	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);
	
	delete A;
#else
	YALLA_ERR("Error, this test is supposed to be run in sequential");
#endif
}

TEST(SeqLinearSolver, SeqAMGColMajor)
{
#ifndef WITH_MPI
	typedef dDenseMatrixColMajor colMajorMatrix;
  typedef PromoteToVector<colMajorMatrix, double>::data_type SeqVector;
	const int size = 5;
	AMG<AMGSetup<colMajorMatrix, StandardCoarsening<colMajorMatrix>, DirectInterpolation<colMajorMatrix> >, AMGSolver<GaussianElimination, GaussSeidel> > myAMG;
	colMajorMatrix* A = Gallery::create2DLaplace<colMajorMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;
	
	myAMG.setup(*A,b,x);
	myAMG.setMaxIterations(100);
	myAMG.setTolerance(1e-10);
	myAMG.solve(*A, b, x);
	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);

    delete A;
#else
		YALLA_ERR("Error, this test is supposed to be run in sequential");		
#endif
}

TEST(SeqLinearSolver, SeqAMGCSR)
{
#ifndef WITH_MPI
  typedef dCSRMatrix CSRMatrix;
  typedef PromoteToVector<CSRMatrix, double>::data_type SeqVector;
	const int size = 5;
    AMG<AMGSetup<CSRMatrix, StandardCoarsening<CSRMatrix>, DirectInterpolation<CSRMatrix> >, AMGSolver<GaussianElimination, GaussSeidel> > myAMG;
    CSRMatrix* A = Gallery::create2DLaplace<CSRMatrix>(size);
    SeqVector x(size*size,1), b(size*size,1);
    x.fill(1);
    b = *A * x;
    for(int i=0;i<size*size;++i)
      x(i) = 0;
		
    myAMG.setup(*A,b,x);
    myAMG.setMaxIterations(100);
    myAMG.setTolerance(1e-10);
    myAMG.solve(*A, b, x);
    for(int i=0;i<size*size;++i)
      EXPECT_NEAR(x(i),1.,1e-7);

    delete A;
#else
		YALLA_ERR("Error, this test is supposed to be run in sequential");		
#endif
}


#endif

#endif
