#pragma once
#ifndef yalla_test_superlu_gtest_h
#define yalla_test_superlu_gtest_h

#ifdef WITH_SUPERLU

#include <assert.h>
#include "yalla/LinearSolver/SuperLU/SuperLU.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/Utils/OperatorOverloading/VectorOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "yalla/IO/console/log.h"
#include "gallery/Gallery.h"
#ifdef WITH_MPI
#include "yalla/Utils/DataDistribution/DataDistribution.h"
#include "yalla/Utils/DataDistribution/RowDistribution.h"
#include "yalla/Utils/DataDistribution/ColumnDistribution.h"
#endif

#ifdef WITH_GTEST

YALLA_USING_NAMESPACE(yalla)

TEST(DistLinearSolver, DistSuperLUCSR)
{
#ifdef WITH_MPI
	const int size = 5;
	const int numProc = ParUtils::getNumProc();
	typedef dCSRMatrix mySeqCSRMatrix;
	typedef PromoteToVector<mySeqCSRMatrix, mySeqCSRMatrix::data_type>::data_type mySeqCSRVectorType;
	typedef PromoteToDist<mySeqCSRMatrix, mySeqCSRMatrix::data_type>::data_type myDistCSRMatrixType;
	typedef PromoteToVector<myDistCSRMatrixType, myDistCSRMatrixType::data_type>::data_type myDistCSRVectorType;

	SuperLU mySuperLU;
	mySeqCSRMatrix* A = 0;
	mySeqCSRVectorType* x = 0, * b = 0;
	if(ParUtils::iAmMaster())
	{
		A = Gallery::create2DLaplace<mySeqCSRMatrix>(size*numProc);
		x = new mySeqCSRVectorType(size*size*numProc*numProc,1);
		b = new mySeqCSRVectorType(size*size*numProc*numProc,1);
		x->fill(1);
		*b = *A * *x;
		x->fill(0.);
	}
  
  DataDistribution<RowDistribution<myDistCSRMatrixType::data_type> > distrib;
	myDistCSRMatrixType* locA = distrib.DistributeMatrix(*A);
	
	myDistCSRVectorType* locX = distrib.DistributeVector(*x);
	myDistCSRVectorType* locB = distrib.DistributeVector(*b);

	mySuperLU.setup(*locA, *locB, *locX);
	mySuperLU.solve(*locA, *locB, *locX);

	for(int i=0;i<locX->getSize();++i)
		EXPECT_NEAR((*locX)(i),1.,1e-7);

	delete A;
  delete x;
	delete b;
	delete locA;
	delete locX;
	delete locB;
#else
	YALLA_ERR("Error, this test is supposed to be run in parallel");
#endif
}

#endif

#endif

#endif