#pragma once
#ifndef yalla_test_vectors_gtest_h
#define yalla_test_vectors_gtest_h

#include <assert.h>
#include <cmath>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/OperatorOverloading/VectorOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"
#include "yalla/BasicTypes/Vector/Dist/EpetraVector.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/Utils/Math/Math.h"
#ifdef WITH_GTEST
#include "gtest/gtest.h"
#endif

YALLA_USING_NAMESPACE(yalla)

#ifdef WITH_GTEST

TEST(SeqVectorTest, SeqVector)
{
  typedef dDenseVector myVectorType;
	typedef myVectorType::data_type data_type;
	typedef dDenseMatrixRowMajor denseMatrix;
	typedef dCSRMatrix CSRMatrix;

	const int size = 5;
	const int nnz = 10;
	const data_type val = 2;
  // Test regularFullVector
  {
    data_type* myData = new data_type[size];

    for(int i=0;i<size;++i)
      myData[i] = i+1;

    // Appel de tous les constructeurs
    myVectorType myVector;
    myVectorType myVector1(size,1);
    myVectorType myVector2(size,1,myData);
    myVectorType myVector3(myVector2);
    myVectorType myVector4;

		EXPECT_EQ(myVector2,myVector3);
		
		EXPECT_EQ(myVector1.getNbRows(),size);
		EXPECT_EQ(myVector1.getNbCols(),static_cast<int>(1));

    for(int i=0;i<size;++i)
      EXPECT_EQ(myVector2(i),i+1);

    for(int i=0;i<size;++i)
      myVector1(i) = myVector2(i);

    for(int i=0;i<size;++i)
      EXPECT_EQ(myVector1(i),i+1);

    myVector1.fill(val);

    for(int i=0;i<size;++i)
      EXPECT_EQ(myVector1(i),val);

    myVector1.fill(myData);

    for(int i=0;i<size;++i)
      EXPECT_EQ(myVector1(i),myData[i]);

    // Test operateurs
    // Operateur + et =
    myVector4 = myVector3 + myVector2;
		for(int i=0;i<myVector4.getSize();++i)
			EXPECT_EQ(myVector4(i),myVector3(i) + myVector2(i));

    // Operateur - et =
    myVector4 = myVector3 - myVector2;
		for(int i=0;i<myVector4.getSize();++i)
			EXPECT_EQ(myVector4(i),myVector3(i) - myVector2(i));

    // Operateur * et =
    myVector4 = myVector3 * myVector2;
		for(int i=0;i<myVector4.getSize();++i)
			EXPECT_EQ(myVector4(i),myVector3(i) * myVector2(i));

    // Operateur / et =
    myVector4 = myVector3 / myVector2;
		for(int i=0;i<myVector4.getSize();++i)
			EXPECT_EQ(myVector4(i),myVector3(i) / myVector2(i));

    // Operateurs scalaires
    // Operateur * et =
    myVector4 = myVector3 * val;
		for(int i=0;i<myVector4.getSize();++i)
			EXPECT_EQ(myVector4(i),myVector3(i) * val);

    myVector4 = val * myVector3;
		for(int i=0;i<myVector4.getSize();++i)
			EXPECT_EQ(myVector4(i),myVector3(i) * val);

    // Operateur / et =
    myVector4 = myVector3 / val;
		for(int i=0;i<myVector4.getSize();++i)
			EXPECT_EQ(myVector4(i),myVector3(i) / val);

    denseMatrix myDenseMatrix(size,size);
    CSRMatrix myCSRMatrix(size,size,nnz);

		myCSRMatrix.setValue(0,0,1);
		myCSRMatrix.setValue(0,3,1);
		myCSRMatrix.setValue(1,1,1);
		myCSRMatrix.setValue(1,4,1);
		myCSRMatrix.setValue(2,0,1);
		myCSRMatrix.setValue(2,2,1);
		myCSRMatrix.setValue(3,1,1);
		myCSRMatrix.setValue(3,3,1);
		myCSRMatrix.setValue(4,4,1);
		myCSRMatrix.setValue(4,0,1);
		myCSRMatrix.finalize();

		myDenseMatrix.setValue(0,0,1);
		myDenseMatrix.setValue(0,3,1);
		myDenseMatrix.setValue(1,1,1);
		myDenseMatrix.setValue(1,4,1);
		myDenseMatrix.setValue(2,0,1);
		myDenseMatrix.setValue(2,2,1);
		myDenseMatrix.setValue(3,1,1);
		myDenseMatrix.setValue(3,3,1);
		myDenseMatrix.setValue(4,4,1);
		myDenseMatrix.setValue(4,0,1);

		const int lNbRows = myDenseMatrix.getNbRows();
		const int lNbCols = myDenseMatrix.getNbCols();
    myVectorType result(lNbRows,1), myDenseVector, myDenseVector1;
		for(int i=0;i<lNbRows;++i)
		{
			for(int j=0;j<lNbCols;++j)
			{
				result(i) += myDenseMatrix(i,j) * myVector3(j);
			}
		}

    myDenseVector = myDenseMatrix*myVector3;
		for(int i=0;i<myDenseVector.getSize();++i)
			EXPECT_EQ(myDenseVector(i),result(i));

    myDenseVector1 = myCSRMatrix*myVector3;
		for(int i=0;i<myDenseVector.getSize();++i)
			EXPECT_EQ(myDenseVector1(i),result(i));

		data_type dot = BLAS::dot(result,result);
		data_type dotControl = 0;
		for(int i=0;i<result.getSize();++i)
			dotControl += result(i)*result(i);
		EXPECT_EQ(dot,dotControl);

		data_type nrm = BLAS::nrm2(result);
		data_type nrmControl = std::sqrt(dotControl);
		EXPECT_EQ(nrm,nrmControl);

    delete[] myData;
  }
}
#endif

#endif
