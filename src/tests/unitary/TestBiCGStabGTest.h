#pragma once
#ifndef yalla_test_bicgstab_gtest_h
#define yalla_test_bicgstab_gtest_h

#include <assert.h>
#include "yalla/Utils/Types/TypesAndDef.h"
#include "yalla/Utils/TypeTraits/PromoteToVector.h"
#include "yalla/Utils/OperatorOverloading/VectorOperatorOverloading.h"
#include "yalla/Utils/OperatorOverloading/MatrixOperatorOverloading.h"
#include "gallery/Gallery.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Seq/SeqSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqDenseMatrixColMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistDenseMatrix.h"
#include "yalla/BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistDenseMatrixRowMajor.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMatrix.h"
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"
#include "yalla/Utils/Math/Math.h"
#include "yalla/LinearSolver/NoPrecond.h"
#include "yalla/LinearSolver/BiCGStab/BiCGStab.h"
#include "yalla/IO/console/log.h"

#ifdef WITH_GTEST

YALLA_USING_NAMESPACE(yalla)

TEST(SeqLinearSolver, SeqBiCGStabRowMajor)
{
#ifndef WITH_MPI
	const int size = 5;
  typedef dDenseMatrixRowMajor rowMajorMatrix;
  typedef PromoteToVector<rowMajorMatrix, double>::data_type SeqVector;

	BiCGStab<rowMajorMatrix> myBiCGStab;
	rowMajorMatrix* A = Gallery::create2DLaplace<rowMajorMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;
	
	myBiCGStab.setup(*A,b,x);
	myBiCGStab.setMaxIterations(100);
	myBiCGStab.setTolerance(1e-7);
	myBiCGStab.solve(*A, b, x);
	
	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);
	
	delete A;
#else
	YALLA_ERR("Error, this test is supposed to be run in sequential");
#endif
}

TEST(SeqLinearSolver, SeqBiCGStabColMajor)
{
#ifndef WITH_MPI
	const int size = 5;
  typedef dDenseMatrixColMajor colMajorMatrix;
  typedef PromoteToVector<colMajorMatrix, double>::data_type SeqVector;

	BiCGStab<colMajorMatrix> myBiCGStab;
	colMajorMatrix* A = Gallery::create2DLaplace<colMajorMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;

	myBiCGStab.setup(*A,b,x);
	myBiCGStab.setMaxIterations(100);
	myBiCGStab.setTolerance(1e-7);
	myBiCGStab.solve(*A, b, x);

	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);

	delete A;
#else
	YALLA_ERR("Error, this test is supposed to be run in sequential");
#endif
}

TEST(SeqLinearSolver, SeqBiCGStabCSR)
{
#ifndef WITH_MPI
	const int size = 5;
  typedef dCSRMatrix CSRMatrix;
  typedef PromoteToVector<CSRMatrix, double>::data_type SeqVector;

	BiCGStab<CSRMatrix> myBiCGStab;
	CSRMatrix* A = Gallery::create2DLaplace<CSRMatrix>(size);
	SeqVector x(size*size,1), b(size*size,1);
	x.fill(1);
	b = *A * x;
	for(int i=0;i<size*size;++i)
		x(i) = 0;

	myBiCGStab.setup(*A,b,x);
	myBiCGStab.setMaxIterations(100);
	myBiCGStab.setTolerance(1e-7);
	myBiCGStab.solve(*A, b, x);

	for(int i=0;i<size*size;++i)
		EXPECT_NEAR(x(i),1.,1e-7);

	delete A;
#else
	YALLA_ERR("Error, this test is supposed to be run in sequential");
#endif
}


#endif

#endif
