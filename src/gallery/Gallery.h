#pragma once
#ifndef yalla_gallery_h
#define yalla_gallery_h

/*!
 *  \file Gallery.h
 *  \brief Matrix gallery
 *  \date 12/12/2013
 *  \author Xavier TUNC
 *  \version 1.0
 */

#include "yalla/Utils/Types/TypesAndDef.h"

/*!
 *  \namespace yalla
 *  \brief Global namespace of the project
 */
YALLA_BEGIN_NAMESPACE(yalla)
/*!
 *  \namespace Gallery
 *  \brief Matrix gallery
 * 
 *  \details This class allows to create sequential usual matrices. \n
 *  Marices implemented (v1.0):
 *  - 1D Laplacian
 *  - 2D Laplacian
 *  - Identity
 *  
 *  \todo Implement 3D Laplacian
 *  \todo Improve efficiency of the 1-2D Laplacian (remove call to setValue method)
 *  \todo Extend this to build directly distributed matrices
 */
YALLA_BEGIN_NAMESPACE(Gallery)

/*! 
 *  \brief Returns a sequential dense matrix
 *  
 *  \tparam DataType Data type of the matrix
 *  \tparam MatrixImpl Data structure implementation
 *  \param _mat The matrix to be returned
 *
 *  \return _mat The matrix to be returned
 */
template<typename DataType, typename MatrixImpl>
	SeqDenseMatrix<DataType,MatrixImpl>* returnMatrix(SeqDenseMatrix<DataType,MatrixImpl>* _mat)
{
	return _mat;
}

/*! 
 *  \brief Returns a sequential sparse matrix
 *  
 *  This method calls the finalize() method of the matrix before returning it. 
 *  The finalize() method of the matrix ensure proper construction and storage of the matrix.
 *
 *  \tparam DataType Data type of the matrix
 *  \tparam MatrixImpl Data structure implementation
 *
 *  \param _mat The matrix to be returned
 *
 *  \return _mat The matrix to be returned
 */
template<typename DataType, typename MatrixImpl>
	SeqSparseMatrix<DataType,MatrixImpl>* returnMatrix(SeqSparseMatrix<DataType,MatrixImpl>* _mat)
{
	_mat->finalize();
	return _mat;
}

/*! 
 *  \brief Create a 1D Laplacian
 *  
 *  \tparam MatrixType The type of the expected matrix
 *  \param _size Size of the domain
 *
 *  \return tmpMatrix The matrix to be returned
 */
template<typename MatrixType>
MatrixType* create1DLaplace(const unsigned int _size)
{
	const unsigned int nnz = (_size-2)*3 + 4;
	MatrixType* tmpMatrix = new MatrixType(_size,_size, nnz);
	tmpMatrix->setValue(0,0,2);
	tmpMatrix->setValue(0,1,-1);
	for(unsigned int i=1;i<_size-1;++i)
	{
		tmpMatrix->setValue(i,i-1,-1);
		tmpMatrix->setValue(i,i,2);
		tmpMatrix->setValue(i,i+1,-1);
	}
	tmpMatrix->setValue(_size-1,_size-2,-1);
	tmpMatrix->setValue(_size-1,_size-1,2);
	return returnMatrix(tmpMatrix);
}

/*! 
 *  \brief Create a 2D Laplacian
 *  
 *  \tparam MatrixType The type of the expected matrix
 *  \param _size Size of the domain (the actual size of the matrix will be _size*_size)
 *
 *  \return tmpMatrix The matrix to be returned
 */
template<typename MatrixType>
MatrixType* create2DLaplace(const unsigned int _size)
{
	// Building the matrix
	//                   5 pts stencil           3 pts stencil  4 pts stencil
	const unsigned int nnz = 5*(_size-2)*(_size-2) + 3 * 4 +        4 * (_size-2) * 4;
	MatrixType* tmpMatrix = new MatrixType(_size*_size,_size*_size,nnz);
	// First row
	// First element
	tmpMatrix->setValue(0,0,4);
	tmpMatrix->setValue(0,1,-1);
	tmpMatrix->setValue(0,_size,-1);
	// All elements except first and last
	for(unsigned int i=1;i<_size-1;++i)
	{
		tmpMatrix->setValue(i,i-1,-1);
		tmpMatrix->setValue(i,i,4);
		tmpMatrix->setValue(i,i+1,-1);
		tmpMatrix->setValue(i,i+_size,-1);
	}
	// Last element
	tmpMatrix->setValue(_size-1,_size-2,-1);
	tmpMatrix->setValue(_size-1,_size-1,4);
	tmpMatrix->setValue(_size-1,2*_size-1,-1);
	// All rows except first and last
	for(unsigned int i=_size;i<_size*(_size-1);i+=_size)
	{
		// First element
		tmpMatrix->setValue(i,i-_size,-1);
		tmpMatrix->setValue(i,i,4);
		tmpMatrix->setValue(i,i+1,-1);
		tmpMatrix->setValue(i,i+_size,-1);
		// All elements except first and last
		for(unsigned int j=i+1;j<i+_size-1;++j)
		{
			tmpMatrix->setValue(j,j-_size,-1);
			tmpMatrix->setValue(j,j-1,-1);
			tmpMatrix->setValue(j,j,4);
			tmpMatrix->setValue(j,j+1,-1);
			tmpMatrix->setValue(j,j+_size,-1);
		}
		// Last element
		tmpMatrix->setValue(i+_size-1,i-1,-1);
		tmpMatrix->setValue(i+_size-1,i+_size-2,-1);
		tmpMatrix->setValue(i+_size-1,i+_size-1,4);
		tmpMatrix->setValue(i+_size-1,i+2*_size-1,-1);
	}
	// Last row
	// First element
	tmpMatrix->setValue(_size*(_size-1),_size*(_size-2),-1);
	tmpMatrix->setValue(_size*(_size-1),_size*(_size-1),4);
	tmpMatrix->setValue(_size*(_size-1),_size*(_size-1)+1,-1);
	// All elements except first and last
	for(unsigned int i=_size*(_size-1)+1;i<_size*_size-1;++i)
	{
		tmpMatrix->setValue(i,i-_size,-1);
		tmpMatrix->setValue(i,i-1,-1);
		tmpMatrix->setValue(i,i,4);
		tmpMatrix->setValue(i,i+1,-1);
	}
	// Last element
	tmpMatrix->setValue(_size*_size-1,_size*(_size-1)-1,-1);
	tmpMatrix->setValue(_size*_size-1,_size*_size-2,-1);
	tmpMatrix->setValue(_size*_size-1,_size*_size-1,4);
	return returnMatrix(tmpMatrix);
}

/*! 
 *  \brief Create an Identity matrix
 *  
 *  \tparam MatrixType The type of the expected matrix
 *  \param _size Size of the matrix
 *
 *  \return tmpMatrix The matrix to be returned
 */
template<typename MatrixType>
MatrixType* createIdentity(const unsigned int _size)
{
	// Building the matrix
	MatrixType* tmpMatrix = new MatrixType(_size,_size, _size);
	for(unsigned int i=0;i<_size;++i)
		tmpMatrix->setValue(i,i,1);

	return returnMatrix(tmpMatrix);
}

/*! \brief Return the name of this class
 *
 *  \return "Gallery" Name of this class
 */
const char* getClassName()
{
	return "Gallery";
}

YALLA_END_NAMESPACE
YALLA_END_NAMESPACE
#endif
