#include <iostream>
#include "yalla/Utils/MemoryCheck/MemoryCheck.h"
#ifdef _WIN32
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#define DEBUG_NEW new(_NORMAL_BLOCK,__FILE__, __LINE__)
#define new DEBUG_NEW
#endif
#endif

#define NUMCORES 4

//#include <chrono>
#include "yalla/BasicTypes/Matrix/Dist/DistSparseMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Seq/SeqCSRMKLMatrix.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMatrix.h"
#include "yalla/BasicTypes/Vector/Seq/SeqVector.h"
#include "yalla/BasicTypes/Vector/Dist/DistVector.h"
#include "yalla/Utils/BLAS/BLAS.h"
#include "yalla/LinearSolver/BiCGStab/BiCGStab.h"
#ifdef WITH_TRILINOS
#include "yalla/BasicTypes/Matrix/Implementation/Dist/EpetraMatrix.h"
#include "yalla/BasicTypes/Vector/Dist/EpetraVector.h"
#include "Epetra_MpiComm.h"
#include "Epetra_Map.h"
#include "Epetra_Vector.h"
#include "Epetra_CrsMatrix.h"
#include "Galeri_Maps.h"
#include "Galeri_CrsMatrices.h"
#endif
//#include "VLD/vld.h"
#ifdef WITH_MPI
#include <stddef.h>
#include "mpi.h"
#include "yalla/Utils/ParUtils/ParUtils.h"
#include "yalla/Utils/DataDistribution/DataDistribution.h"
#include "yalla/Utils/DataDistribution/RowDistribution.h"
#include "yalla/Utils/DataDistribution/MetisDistribution.h"
#include "yalla/Utils/DataDistribution/ColumnDistribution.h"
#include "yalla/LinearSolver/SuperLU/SuperLU.h"
#include "yalla/LinearSolver/AMG/Setup/CoarseningStrategy/PMIS.h"
#include "yalla/BasicTypes/Matrix/Implementation/Dist/DistCSRMKLMatrix.h"
#endif
#ifdef _WIN32
#include "OpenMP/omp.h"
#endif

#include "yalla/IO/Reader/Sparse/SparseMatrixReaderCSRFormat.h"
#include "yalla/IO/Reader/Sparse/SparseMatrixReaderMMFormat.h"
#include "yalla/IO/Writer/Sparse/SparseMatrixWriterMMFormat.h"
using namespace yalla;
#ifdef WITH_TRILINOS
using namespace Teuchos;
using namespace Galeri;
#endif
#include "gallery/Gallery.h"

#include "yalla/Utils/singleton/SingletonDynamic.h"
#include "yalla/Utils/synchronizer/SynchronizerMng.h"
#include "yalla/Utils/stackTrace/StackTrace.h"
#include "yalla/Utils/ParUtils/GeneralSynchronizer.h"
#include "yalla/IO/console/log.h"

#include "yalla/Utils/Profiler/Profiler.h"

#ifdef WITH_CPP11
#include <chrono>
#endif

#ifdef WITH_SUPERLU
#include "yalla/LinearSolver/SuperLU/SuperLU.h"
#endif

int main(int argc, char** argv)
{
#ifndef WITH_MPI
  {
		/*
		// Set a few standards to make formatting easier.
		const string NC = "\E[0m"; // No Color (reset to default)
		const string HOME_CURSOR  = "\E[0;0H"; // Place the cursor at 0;0 position.
		const string CLEAR_SCREEN = "\E[2J";

		// Clear the screen and reset the cursor to the top left.
		cout << CLEAR_SCREEN << HOME_CURSOR;
		
		// print program name.
		cout << endl << argv[0] << endl;
		
		// display the color table.
		cout << "B;FG;BG\t";
		for (int i = 40; i < 48; i++)
      cout << "  " << i << "m\t";
		cout << endl;
		for (int fg = 30; fg < 38; fg++)
		{
      for (int h = 0; h < 2; h++)
      {
				cout << NC << h << ";" << fg << "m";
				for (int bg = 40; bg < 48; bg++)
				{
					cout << "\t"
							 << "\E[" << h << "m"
							 << "\E[" << fg << "m"
							 << "\E[" << bg << "m"
							 << "  RgB  ";
				}
				cout << endl;
      }
		}
   // Reset the console to no colors.
   cout << NC << endl;

	 cout << "\E[" << 0 << "m" << "\E[" << 32 << "m" << "\E[" << 40 << "m" << "Green fg on black bg" << endl;
	 cout << "\E[" << 0 << "m" << "\E[" << 31 << "m" << "\E[" << 40 << "m" << "Red fg on black bg" << endl;
	 cout << "\E[" << 0 << "m" << "\E[" << 33 << "m" << "\E[" << 40 << "m" << "Yellow fg on black bg" << endl;

	 cout << "\E[0m" << endl;
		*/

		/*
#ifdef WITH_GTEST
		std::cout << "WITH_GTEST\n";
		::testing::InitGoogleTest(&argc, argv);
		YALLA_ERROR_CHECK(RUN_ALL_TESTS());
#endif
		std::cout << "SEQ\n";

		StackTrace::printStack();
		*/

		/*
		int size = 250;
		typedef dCSRMatrix MatrixType;
		//typedef dDenseMatrixRowMajor MatrixType;
		MatrixType* toto = Gallery::create2DLaplace<MatrixType>(size);

		//RowSum rowSum;
		//rowSum.computeRowSum(*toto);

		delete toto;
		*/

		/*
		SparseMatrixWriterMMFormat<dCSRMatrix> writer;
		writer.Write(*toto,"../../../../workspace/TrilinosAMG/SPE10_BO_15x55x21_pressure_mat.mtx");
		*/

		/*
    TestMatrices myTest;
    YALLA_ERROR_CHECK(myTest.runTest());

    TestVectors myTest2;
    YALLA_ERROR_CHECK(myTest2.runTest());

		TestGaussianElimination myTest3;
    YALLA_ERROR_CHECK(myTest3.runTest());

    TestGaussSeidel myTest4;
    YALLA_ERROR_CHECK(myTest4.runTest());

    TestBiCGStab myTest5;
    YALLA_ERROR_CHECK(myTest5.runTest());

    TestBiCGStabL myTest6;
    YALLA_ERROR_CHECK(myTest6.runTest());

    TestAMG myTest7;
    YALLA_ERROR_CHECK(myTest7.runTest());
		*/

		/*
		std::cout << "sizeof(unsigned int): " << sizeof(unsigned int) << "\n" << std::flush;
		std::cout << "sizeof(int): " << sizeof(int) << "\n" << std::flush;
		std::cout << "sizeof(float): " << sizeof(float) << "\n" << std::flush;
		std::cout << "sizeof(double): " << sizeof(double) << "\n" << std::flush;

		const int size = (1L << 16);
		std::cout << size << "\n";
		double* __restrict__ toto = new double[size];
		double* __restrict__ tata = new double[size];

		for(int i=0;i<size;++i)
			toto[i] += tata[i];

		delete toto;
		delete tata;
		*/

		//int size = 2500;
		//typedef dCSRMatrix csrtype;
		typedef SeqSparseMatrix<double, SeqCSRMKLMatrix<double> > csrtype;
		//typedef dDenseMatrixRowMajor densetype;
		//csrtype* toto = Gallery::create2DLaplace<csrtype>(size);
		//densetype* tata = Gallery::create2DLaplace<densetype>(size);
        
    //std::chrono::time_point<std::chrono::high_resolution_clock> start;
		//std::chrono::time_point<std::chrono::high_resolution_clock> end;
    // we may not have chrono (i.e. VS2010), so let's stick with the library profiler
    Profiler profiler = Profiler();

		SparseMatrixReaderMMFormat<csrtype> reader;
		//start = std::chrono::system_clock::now();
    profiler.start("maxtrix_read");
		csrtype* A = reader.Read("../../EXXON_MM_EN/1.mtx");
		//end = std::chrono::system_clock::now();
    profiler.stop("matrix_read");

		//double elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
		//std::cout << elapsed_seconds << " s to read\n";
    std::cout << profiler.getTime("matrix_read") << " s to read\n";

		std::cout << "matrix nb rows: " << A->getNbRows() << " nb cols: " << A->getNbCols() << " nbNnz: " << A->getNnz() << "\n";

		SeqVector<double> x(A->getNbRows(),1), y(A->getNbRows(),1);
		x.fill(0.);
		y.fill(1.);

		std::cout << "vector size: " << x.getSize() << "\n";

		BLAS::gemv(1.,*A,y,1.,x);
		YALLA_LOG(BLAS::nrm2(x));
		//YALLA_LOG(*A);

		/*
		BiCGStab<csrtype> myBiCGStab;
		myBiCGStab.setMaxIterations(501);
		myBiCGStab.setTolerance(1e-7);
		myBiCGStab.setVerbose(3);

		start = std::chrono::system_clock::now();
		myBiCGStab.setup(*A,y,x);
		myBiCGStab.solve(*A,y,x);
		end = std::chrono::system_clock::now();

		myBiCGStab.printReport();
		*/

		/*

		y = (*A) * x;

		elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
		std::cout << "nrm2(y): " << BLAS::nrm2(y) << " " << elapsed_seconds << " s for full version\n";
		*/

		/*
		y.fill(0.);
		const unsigned int nbRows = A->getNbRows();
		const typename csrtype::impl_type::graph_type graph = A->getMatrixGraph();
		const unsigned int* rowPtr = graph.getRowPtr();
		const unsigned int* colPtr = graph.getColPtr();

		start = std::chrono::system_clock::now();
		for(unsigned int i=0;i<nbRows;++i)
		{
			for(unsigned int j=rowPtr[i];j<rowPtr[i+1];++j)
			{
				y(i) += (*A)[j] * x(colPtr[j]);
			}
		}
		end = std::chrono::system_clock::now();

		elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
		std::cout << "nrm2(y): " << BLAS::nrm2(y) << " " << elapsed_seconds << " s for raw pointers\n";

		typedef typename csrtype::impl_type::const_row_iterator_type rowIt;
		typedef typename csrtype::impl_type::const_col_iterator_type colIt;
		y.fill(0.);

		start = std::chrono::system_clock::now();
		for(rowIt row=A->rowBegin(); row != A->rowEnd();++row)
		{
			const unsigned int rowIndex = row.index();
			for(colIt col=A->colBegin(row);col!=A->colEnd(row);++col)
			{
				const unsigned int colIndex = col.index();
				y(rowIndex) += (*col) * x(colIndex);
			}
		}
		end = std::chrono::system_clock::now();
		elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
		std::cout << "nrm2(y): " << BLAS::nrm2(y) << " " << elapsed_seconds << " s for iterators\n";
		*/

		/*
		typedef typename densetype::impl_type::const_row_iterator_type rowItDense;
		typedef typename densetype::impl_type::const_col_iterator_type colItDense;

		start = std::chrono::system_clock::now();
		for(rowItDense row=tata->rowBegin(); row != tata->rowEnd();++row)
		{
			double sum = 0;
			for(colItDense col=tata->colBegin(row);col!=tata->colEnd(row);++col)
				sum += *col;
		}
		end = std::chrono::system_clock::now();
		elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
		std::cout << elapsed_seconds << " s for dense\n";
		*/

		delete A;
		//delete tata;
  }
#endif
#ifdef WITH_MPI
  {
    MPI_Init(&argc,&argv);

		/*
#ifdef WITH_GTEST
		::testing::InitGoogleTest(&argc, argv);
		YALLA_ERROR_CHECK(RUN_ALL_TESTS());
#endif
		*/

		//typedef dCSRMatrix mySeqCSRMatrix;
		typedef SeqSparseMatrix<double, SeqCSRMKLMatrix<double> > mySeqCSRMatrix;
		typedef PromoteToVector<mySeqCSRMatrix, mySeqCSRMatrix::data_type>::data_type mySeqCSRVectorType;
		typedef PromoteToDist<mySeqCSRMatrix, mySeqCSRMatrix::data_type>::data_type myDistCSRMatrixType;
		typedef PromoteToVector<myDistCSRMatrixType, myDistCSRMatrixType::data_type>::data_type myDistCSRVectorType;
		
		mySeqCSRMatrix* A = 0;
    mySeqCSRVectorType *x = 0, *y = 0;

		//std::chrono::time_point<std::chrono::high_resolution_clock> start;
		//std::chrono::time_point<std::chrono::high_resolution_clock> end;
    Profiler profiler = Profiler();

		int size = 25;
		
    if(ParUtils::iAmMaster())
    {
			SparseMatrixReaderMMFormat<mySeqCSRMatrix> reader;
			//start = std::chrono::system_clock::now();
      profiler.start("maxtrix_read");
      A = reader.Read("../../EXXON_MM_EN/1.mtx");// Gallery::create2DLaplace<mySeqCSRMatrix>(size);
			//end = std::chrono::system_clock::now();
      profiler.stop("maxtrix_read");

			//double elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
			//YALLA_LOG("Time to read matrix: " << elapsed_seconds);
      YALLA_LOG("Time to read matrix: " << profiler.getTime("maxtrix_read"););

			YALLA_LOG("A: " << A->getNbRows() << "x" << A->getNbCols() << ", nnz: " << A->getNnz());
      x = new mySeqCSRVectorType(A->getNbRows(),1);
      y = new mySeqCSRVectorType(A->getNbRows(),1);
			YALLA_LOG("x: " << x->getSize());
			YALLA_LOG("y: " << y->getSize());
			x->fill(0.);
			y->fill(1.);
    }

		//start = std::chrono::system_clock::now();
    profiler.start("maxtrix_read");
		static DataDistribution<RowDistribution<mySeqCSRMatrix::data_type> >* sDistrib = 0; 
		sDistrib = new DataDistribution<RowDistribution<mySeqCSRMatrix::data_type> >();
		myDistCSRMatrixType* loca = sDistrib->DistributeMatrix(*A);
    myDistCSRVectorType* locx = sDistrib->DistributeVector(*x);
    myDistCSRVectorType* locy = sDistrib->DistributeVector(*y);
		delete loca;
		delete locx;
		delete locy;
		delete sDistrib;
    //DataDistribution<RowDistribution<mySeqCSRMatrix::data_type> > distrib;
    DataDistribution<MetisDistribution<mySeqCSRMatrix::data_type> > distrib;
    myDistCSRMatrixType* locA = distrib.DistributeMatrix(*A);
		//YALLA_LOG(*locA);
    myDistCSRVectorType* locX = distrib.DistributeVector(*x);
    myDistCSRVectorType* locY = distrib.DistributeVector(*y);
		//end = std::chrono::system_clock::now();
    profiler.stop("maxtrix_read");

		double elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
		YALLA_LOG("Time to distribute linear system: " << elapsed_seconds);

		//std::cout << "Proc: " << ParUtils::getProcRank() << " locA: " << locA->getNbRows() << "x" << locA->getNbCols() << ", nnz: " << locA->getNnz() << "\n" << std::flush;

		/*
		typedef typename myDistCSRMatrixType::impl_type::const_row_iterator_type rowIt;
		typedef typename myDistCSRMatrixType::impl_type::const_col_iterator_type colIt;

		auto llocA = locA->getImpl();
		if(ParUtils::getProcRank()==1)
		{
			for(rowIt row = llocA->rowBeginConst(); row != llocA->rowEndConst(); ++row)
			{
				for(colIt col = llocA->colBeginConst(row); col != llocA->colEndConst(row); ++col)
					std::cout << "(" << row.globalIndex() << "," << col.globalIndex() << ") : " << *col << "\n";
			}
		}
		*/

		//start = std::chrono::system_clock::now();
    profiler.start("maxtrix_read");
		for(int i=0;i<10000;++i)
			BLAS::gemv(1.,*locA,*locY,1.,*locX);
		//end = std::chrono::system_clock::now();
    profiler.start("maxtrix_read");
		//elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
		//std::cout << "Proc: " << ParUtils::getProcRank() << " Time to gemv: " << elapsed_seconds << "\n";
    std::cout << "Proc: " << ParUtils::getProcRank() << " Time to gemv: " << profiler.getTime("maxtrix_read") << "\n";

		YALLA_LOG(BLAS::nrm2(*locX));

		//std::cout << *locA << "\n";

		/*
		BiCGStab<myDistCSRMatrixType> myBiCGStab;
		myBiCGStab.setMaxIterations(501);
		myBiCGStab.setTolerance(1e-7);
		myBiCGStab.setVerbose(3);

		start = std::chrono::system_clock::now();		
		myBiCGStab.setup(*locA,*locY,*locX);
		myBiCGStab.solve(*locA,*locY,*locX);
		end = std::chrono::system_clock::now();

		elapsed_seconds = std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()/1e9;
		YALLA_LOG("Time to solve the linear system: " << elapsed_seconds);
		myBiCGStab.printReport();
		*/
		//BLAS::gemv(*locA,*locY,*locX);

		//YALLA_LOG(BLAS::nrm2(*locX));

		//SynchronizerSingleton& sync = SynchronizerSingleton::instance();
		
		//std::cout << "proc: " << ParUtils::getProcRank() << "\n" << std::flush;
		//locX->getSync()->print();
		
		//RowSum rowSum;
		//rowSum.computeRowSum(*locA);
		
		//PMIS<myDistCSRMatrixType> myPMIS;
		//myPMIS.setup();
		//myPMIS.apply(*locA,*locB, *locX, 0);
		
		delete A;
		delete x;
		delete y;
		delete locA;
		delete locY;
		delete locX;
		
		//YALLA_LOG("ALL DONE");
		/*
#ifdef WITH_TRILINOS
		Epetra_MpiComm Comm(MPI_COMM_WORLD);

		int nx;
		if (argc > 1)
			nx = (int) strtol(argv[1],NULL,10);
		else
			nx = 256;
		int ny = nx;

		ParameterList GaleriList;
		GaleriList.set("nx", nx);
		GaleriList.set("ny", ny);
		GaleriList.set("mx", 1);
		GaleriList.set("my", Comm.NumProc());

		Epetra_Map* Map = 0;
		Epetra_CrsMatrix* A = 0, *B = 0;

		Map = CreateMap("Cartesian2D", Comm, GaleriList);
	  A = CreateCrsMatrix("Laplace2D", Map, GaleriList);

		EpetraMatrix<Epetra_CrsMatrix> tmp(A);
		DistSparseMatrix<double,EpetraMatrix<Epetra_CrsMatrix> > AA(tmp);
		DistSparseMatrix<double,EpetraMatrix<Epetra_CrsMatrix> > BB;

		BB = AA;

		Epetra_Vector LHS(A->Map()); LHS.PutScalar(1.0);
		Epetra_Vector RHS(A->Map()); RHS.PutScalar(1.0);

		EpetraVector<Epetra_Vector> LLHS(&LHS);
		EpetraVector<Epetra_Vector> RRHS(&RHS);
		EpetraVector<Epetra_Vector> LRHS(LLHS);
		RRHS.fill(0.);


		RRHS.print();
		BLAS::computeNormAndResidual(AA,LLHS,RRHS,LRHS);
		RRHS.print();

		delete A;
		delete B;
		delete Map;

#endif
		*/
    //omp_set_num_threads(NUMCORES/ParUtils::getNumProc());
    MPI_Finalize();
  }
#endif

#ifndef WITH_MPI
#ifdef _WIN32
  FILE *pFile;
  freopen_s( &pFile, "memoryLeaks.txt", "w", stdout);
  _CrtSetReportMode( _CRT_WARN, _CRTDBG_MODE_FILE );
  _CrtSetReportFile( _CRT_WARN, _CRTDBG_FILE_STDOUT );
  _CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_FILE );
  _CrtSetReportFile( _CRT_ERROR, _CRTDBG_FILE_STDOUT );
  _CrtSetReportMode( _CRT_ASSERT, _CRTDBG_MODE_FILE );
  _CrtSetReportFile( _CRT_ASSERT, _CRTDBG_FILE_STDOUT );

  int memory_leak = _CrtDumpMemoryLeaks();
  freopen_s( &pFile, "CON", "w", stdout);
  if(memory_leak)
    std::cout << "WARNING : Memory Leak \nIf no filename/line number appears, no actual memory leak, it's just static attributes (deleted atexit()) \n";
  if(pFile)
    fclose(pFile);
#endif
#endif
  return 0;
}
