//
//  main.cpp
//  yalla
//
//  Created by Sylvain Desroziers on 17/03/13.
//  Copyright (c) 2013 Sylvain Desroziers. All rights reserved.
//

#include "gtest/gtest.h"

#ifdef WITH_ATLAS
#ifdef __cplusplus
extern "C"
{
#endif
#include "atlas/cblas.h"
#ifdef __cplusplus
}
#endif
#endif
#ifdef WITH_MKL
#include "mkl.h"
#endif

#ifndef WITH_MPI
#include "tests/unitary/TestVectorsGTest.h"
#include "tests/unitary/TestMatricesGTest.h"
#include "tests/unitary/TestAMGGTest.h"
#include "tests/unitary/TestBiCGStabGTest.h"
#include "tests/unitary/TestGaussianEliminationGTest.h"
#include "tests/unitary/TestGaussSeidelGTest.h"
#ifdef WITH_SUPERLU
#include "tests/unitary/TestSuperLUGTest.h"
#endif
#else
#include "yalla/Utils/gtestlistener/MPIListener.h"
#include "tests/unitary/TestDistVectorsGTest.h"
#include "tests/unitary/TestDistMatricesGTest.h"
#include "tests/unitary/TestDistBiCGStabGTest.h"
#ifdef WITH_SUPERLU
#include "tests/unitary/TestDistSuperLUGTest.h"
#endif
#endif

YALLA_USING_NAMESPACE(yalla);

int main(int argc, char * argv[])
{
#ifdef WITH_MPI
	MPI_Init(&argc,&argv);

  UnitTest& unit_test = *UnitTest::GetInstance();
	
	TestEventListeners& listeners = unit_test.listeners();

	delete listeners.Release(listeners.default_result_printer());
	listeners.Append(new MPIGTestListener);
#endif

	::testing::InitGoogleTest(&argc, argv);
    
	int ret = RUN_ALL_TESTS();
#ifdef WITH_MPI
	MPI_Finalize();
#endif
	return ret;
}
