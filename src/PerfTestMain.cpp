#include "gtest/gtest.h"
#include "yalla/Utils/gtestlistener/PerfListener.h"

#ifdef WITH_ATLAS
#ifdef __cplusplus
extern "C"
{
#endif
#include "atlas/cblas.h"
#ifdef __cplusplus
}
#endif
#endif
#ifdef WITH_MKL
#include "mkl.h"
#endif

#ifndef WITH_MPI
#include "tests/perfs/TestPerfSeqGEMV.h"
#include "tests/perfs/TestPerfSeqAXPY.h"
#include "tests/perfs/TestPerfSeqDot.h"
#include "tests/perfs/TestPerfSeqNrm.h"
#include "tests/perfs/TestPerfSeqScal.h"
#include "tests/perfs/TestPerfSeqCopy.h"
#include "tests/perfs/TestPerfSeqBiCGStab.h"
#else
#include <mpi.h>
#include "tests/perfs/TestPerfDistGEMV.h"
#include "tests/perfs/TestPerfDistAXPY.h"
#include "tests/perfs/TestPerfDistDot.h"
#include "tests/perfs/TestPerfDistNrm.h"
#include "tests/perfs/TestPerfDistScal.h"
#include "tests/perfs/TestPerfDistCopy.h"
#include "tests/perfs/TestPerfDistBiCGStab.h"
#endif

using namespace yalla;

int main(int argc, char * argv[])
{
#ifdef WITH_MPI
	MPI_Init(&argc,&argv);
#endif
	
	PerfTestFramework perf(argc,argv);

  UnitTest& unit_test = *UnitTest::GetInstance();
	
	TestEventListeners& listeners = unit_test.listeners();

	// Removes the default console output listener from the list so it will
	// not receive events from Google Test and won't print any output. Since
	// this operation transfers ownership of the listener to the caller we
	// have to delete it as well.
	delete listeners.Release(listeners.default_result_printer());
	
	// Adds the custom output listener to the list. It will now receive
	// events from Google Test and print the alternative output. We don't
	// have to worry about deleting it since Google Test assumes ownership
	// over it after adding it to the list.
	listeners.Append(perf.getListener());
	
	::testing::InitGoogleTest(perf.getNumArgument(), perf.getCmdLineArgument());
  
	int ret = RUN_ALL_TESTS();
#ifdef WITH_MPI
	MPI_Finalize();
#endif
	return ret;
}

