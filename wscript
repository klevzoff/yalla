#! /usr/bin/env python
# encoding: utf-8

top = '.' 
out = 'build'

from glob import glob
from waflib import Logs

def options(opt):
    
    opt.load('compiler_cxx')
    opt.load('compilation_mode',tooldir='waf_tools')
    opt.load('configuration',tooldir='waf_tools')

def configure(conf):
    conf.check_waf_version(mini='1.7.5')

    conf.load('compiler_cxx')

    conf.load('compilation_mode',tooldir='waf_tools')
    conf.load('configuration',tooldir='waf_tools')

    conf.env.INCLUDES += ['src','external/ThirdParty/Include']

def build(bld):
    
    sources  = glob('src/yalla/Utils/*/*.cpp')
    sources += glob('src/yalla/LinearSolver/*/*/*.cpp')
    sources += glob('src/tests/*.cpp')
    sources += glob('src/tests/*/*.cpp')
    sources += glob('src/gallery/*.cpp')
    
    bld.stlib(source=sources, target='yalla', use='MPI ATLAS MKL PARMETIS METIS SUPERLU')

    #bld.program(source='src/myApp.cpp',
    #            target='LinearSolver.exe',
    #            use='yalla')
    
    if 'WITH_GTEST=1' in bld.env.DEFINES:
        bld.program(source='src/unitary_test_main.cpp',
                    target='unitary_tests.exe',
                    use='yalla GTEST MPI ATLAS PARMETIS METIS SUPERLU')

        if bld.env.CPP11:
            bld.program(source='src/PerfTestMain.cpp',
                        target='PerfTestMain.exe',
                        use='yalla GTEST')
        else:
            Logs.warn('Performance tests cannot be compiled without c++11\n')
    else:
        Logs.warn('Tests cannot be compiled without gtest\n')

