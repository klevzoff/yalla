CXXFLAGS := -Wall -Wno-unused-function -Wno-unknown-pragmas -Wno-comment  -Wno-unused-value -std=c++0x
INSTALL_DIR := bin/
EXEC_NAME := LinearSolver
BASE_DIR := $(shell pwd)
INCLUDES := -I"$(BASE_DIR)/src" -I"$(BASE_DIR)/external"
LIBS_DIR :=
LIBS := -lm
SOURCES := $(shell find . -name '*.cpp')
OBJECTS := $(patsubst %.cpp,%.o,$(SOURCES))
LOG_FILE := compilation.log
DEFINED := -D_UNIX

ifdef release
CXXFLAGS += -Ofast -pipe
BUILD_DIR := ./build/Release
DEFINED += -D_WITH_ATLAS
LIBS += -lblas
else
CXXFLAGS += -g
BUILD_DIR := ./build/Debug
endif

ifdef mpi
CXX := mpicxx
MPI_INC := /usr/lib/mpich2/include/
MPI_LIB := /usr/lib/mpich2/lib/
INCLUDES += -I"$(MPI_INC)"
LIBS_DIR += -L"$(MPI_LIB)"
LIBS += -lmpichcxx -lmpich -lopa -lmpl -lrt -lcr -lpthread
DEFINED += -D_WITH_MPI
else
CXX := g++
endif

ifdef superlu
ifdef mpi
SUPERLU_LIB := /home/kokocha/workspace/SuperLU_DIST_3.1/lib/
PARMETIS_DIR := /home/kokocha/Downloads/parmetis-4.0.2/build/Linux-i686/libparmetis/
METIS_DIR := /home/kokocha/Downloads/parmetis-4.0.2/build/Linux-i686/libmetis
LIBS_DIR += -L"$(SUPERLU_LIB)"
LIBS_DIR += -L"$(PARMETIS_DIR)"
LIBS_DIR += -L"$(METIS_DIR)"
LIBS_DIR += -lsuperlu_dist_3.1
LIBS_DIR += -lparmetis -lmetis
DEFINED += -D_WITH_SUPERLU
else

endif
else

endif

ifdef trilinos
ifdef release
TRILINOS_INSTALL_DIR = /home/kokocha/workspace/ML_INSTALL/release
else
TRILINOS_INSTALL_DIR = /home/kokocha/workspace/ML_INSTALL/debug
endif
include $(TRILINOS_INSTALL_DIR)/include/Makefile.export.ML
INCLUDES += -I$(TRILINOS_INSTALL_DIR)/include
LIBS_DIR += -L$(TRILINOS_INSTALL_DIR)/lib
LIBS += $(ML_LIBRARIES) $(ML_TPL_LIBRARIES)
CXXFLAGS += -fopenmp
DEFINED += -D_WITH_TRILINOS
endif

ifdef tri_blas
DEFINED += -D_WITH_TRILINOS_BLAS
endif

ifdef gtest
INCLUDES += -I"/home/kokocha/Downloads/gtest-1.6.0/include"
GTEST_LIB := /home/kokocha/Downloads/gtest-1.6.0/
LIBS_DIR += -L"$(GTEST_LIB)"
LIBS_DIR += -lgtest
CXXFLAGS += -lpthread
DEFINED += -D_WITH_GTEST
endif

.SILENT : 
.PHONY : #clean base all

base : all
	@echo 
	@echo ---------------------------------
	@echo Compilation successfully finished
	@echo Compiled with: $(CXXFLAGS)
	@echo Linked with: $(LIBS)
	@echo Preprocessor definitions: $(DEFINED)
	@echo ---------------------------------

all : $(EXEC_NAME)

clean :
	$(shell find ./ -name '*.o' -delete) 
	$(shell find ./ -name '*~' -delete)
	$(shell find ./ -name '*.log*' -delete)
	$(shell find ./ -name "*.txt" -delete)
	$(shell find ./ -name "*.obj" -delete)
	$(shell find ./ -name "*.suo" -delete)
	$(shell find ./ -name "*.user" -delete)
	$(shell find ./ -name "*.ncb" -delete)
	$(shell find ./ -name "*.sbr" -delete)
	$(shell if [ -d $(INSTALL_DIR) ] ; then find $(INSTALL_DIR) -mindepth 1 -name "*" -delete ; fi)
	$(shell if [ -d ./build/ ] ; then find ./build/ -mindepth 1 -name "*" -delete ; fi)
	$(shell if [ -d ./ipch/ ] ; then find ./ipch/ -name "*" -delete ; fi)

$(EXEC_NAME) : $(OBJECTS)
	$(CXX) -o $(INSTALL_DIR)$(EXEC_NAME).exe $(OBJECTS) $(LIBS_DIR) $(LIBS) $(CXXFLAGS)

%.o: %.cpp
ifdef verbose
	@echo Compiling file: $(CXX) $(CXXFLAGS) $(INCLUDES) $(DEFINED) -o $@ -c $<
else
	@echo Compiling file: $<
endif
	$(CXX) $(CXXFLAGS) $(INCLUDES) $(DEFINED) -o $@ -c $<

install :
	mv $(OBJECTS) $(BUILD_DIR)

cleandoc :
	$(shell find $(BASE_DIR)/Doc -mindepth 1 -not -name "Doxyfile" -delete)

cleanall : clean cleandoc