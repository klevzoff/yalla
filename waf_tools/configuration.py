#! /usr/bin/env python
# encoding: utf-8

from xml.dom import minidom

def options(opt):

    opt.add_option("--configuration-file",
                   default='default_conf.xml',
                   help='path to configuration file',
                   dest='configuration_file')

def configure(ctx):
    
    ctx.start_msg('Configuration file')
    ctx.end_msg(ctx.options.configuration_file)
    
    ctx.find_file(ctx.options.configuration_file,['.'])
    
    doc = minidom.parse(ctx.options.configuration_file)

    packages = doc.getElementsByTagName('package')

    for package in packages:
        lib_name = package.getAttribute('name').encode('ascii','ignore').upper()
        if lib_name in ctx.skip_list:
            ctx.start_msg('%s' % lib_name)
            ctx.end_msg('Skipped')
            continue

        ctx.start_msg('Checking package')
        ctx.end_msg('%s' % lib_name)

        ctx.start_msg('Mandatory: ')
        mandatory_packages = package.getElementsByTagName('mandatory')

        if not mandatory_packages:
            mandatory_package = False
        else:
            if mandatory_packages[0].getAttribute('name').encode('ascii','ignore') == "yes":
                mandatory_package = True
            else:
                mandatory_package = False
        
        ctx.end_msg('%s' % mandatory_package)

        paths = package.getElementsByTagName('include-path')
        for path in paths:
            name = path.getAttribute('name').encode('ascii','ignore')
            ctx.env['INCLUDES_%s' % lib_name] += [name]
        paths = package.getElementsByTagName('library-path')
        for path in paths:
            name = path.getAttribute('name').encode('ascii','ignore')
            ctx.env['LIBPATH_%s' % lib_name] += [name]
        libs = package.getElementsByTagName('library')
        for lib in libs:
            name = lib.getAttribute('name').encode('ascii','ignore')
            ctx.env['LIB_%s' % lib_name] += [name]

        headers = package.getElementsByTagName('check-header')
        package_included = "yes"
        for header in headers:
            name = header.getAttribute('name').encode('ascii','ignore')
            dependencies = package.getElementsByTagName('dependency')
            libs = lib_name
            for dependency in dependencies:
                libs += ' '
                libs += dependency.getAttribute('name').encode('ascii','ignore').upper()

            if not ctx.check(header_name=name, features='cxx cxxprogram', use=libs, mandatory=mandatory_package):
                package_included = "no"
                break
            else:
                package_included = "yes"

        #ctx.env.DEFINES += ['HAVE_%s=1' % lib_name]
        if package_included == "yes":
            ctx.env.DEFINES += ['WITH_%s=1' % lib_name]
    
        ctx.start_msg('%s included' % lib_name)
        ctx.end_msg('%s' % package_included)
