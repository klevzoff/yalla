#! /usr/bin/env python
# encoding: utf-8

def options(opt):
    opt.add_option("--seq",
                   default=False,
                   action='store_true',
                   help='enable compilation for sequential mode',
                   dest='seq_mode')

    opt.add_option("--release",
                   default=False,
                   action='store_true',
                   help='enable release compilation mode',
                   dest='release_mode')

    opt.add_option("--metis",
                   default=False,
                   action='store_true',
                   help='enable use of metis',
                   dest='with_metis')

    opt.add_option("--parmetis",
                   default=False,
                   action='store_true',
                   help='enable use of parmetis',
                   dest='with_parmetis')

    opt.add_option("--superlu",
                   default=False,
                   action='store_true',
                   help='enable use of superlu',
                   dest='with_superlu')

    opt.add_option("--disable-cxx11",
                   default=False,
                   action='store_true',
                   help='disable C++11 support',
                   dest='no_cxx11')
        
    opt.add_option("--vec-report",
                   default=False,
                   action='store_true',
                   help='enable vectorization report',
                   dest='vec_report')

    opt.add_option("--mkl",
                   default=False,
                   action='store_true',
                   help='use the mkl instead of default blas',
                   dest='with_mkl')
                   
    opt.add_option("--warnings",
                   default=False,
                   action='store_true',
                   help='enable all compiler warnings',
                   dest='warnings')

def configure(conf):
    
    
    conf.skip_list = []

    if not conf.env.CXX:
		conf.fatal('load a c++ compiler first, conf.load("compiler_cxx")')

    conf.env.DEFINES += ['_UNIX=1']

    if conf.env.CXX_NAME == 'gcc' or conf.env.CXX_NAME == 'icc':
        flags = ['-g', '-Wno-unused-function', '-Wno-unknown-pragmas', '-Wno-comment']
        if conf.options.warnings:
            flags += ['-Wall']
    if conf.env.CXX_NAME == 'msvc':
        flags = ['/Zi', '/EHsc'] 
        if conf.options.warnings:
            flags += ['/Wall']

    conf.start_msg('Compilation mode')

    if not conf.options.release_mode:
        conf.env.DEFINES += ['_DEBUG']
        conf.end_msg('debug')
        conf.skip_list.append('ATLAS')
        conf.skip_list.append('MKL')
    else:
        conf.end_msg('release')

        if conf.env.CXX_NAME == 'gcc' or conf.env.CXX_NAME == 'icc':
            flags += ['-O3', '-pipe']
        if conf.env.CXX_NAME == 'msvc':
            flags += ['/O2']
        
        if conf.env.CXX_NAME == 'gcc':
            if conf.options.vec_report:
                flags += ['-ftree-vectorizer-verbose=2','-march=native','-ffast-math','-msse2']

        if conf.env.CXX_NAME == 'icc':
            if conf.options.vec_report:
                flags += ['-vec-report=5']
        if conf.env.CXX_NAME == 'msvc':
            if conf.options.vec_report:
                flags += ['/Qvec-report:1']

    conf.env.CXXFLAGS += flags
    
    conf.env.CPP11 = False
    if not conf.options.no_cxx11:
        conf.env.CXXFLAGS += ['-std=c++0x']
        conf.env.DEFINES += ['_WITH_CPP11=1']
        conf.env.CPP11 = True
    
    if conf.options.seq_mode:
        conf.skip_list.append('MPI')
        conf.skip_list.append('PARMETIS')

    if conf.options.with_mkl:
        conf.skip_list.append('ATLAS')

    if not conf.options.with_mkl:
        conf.skip_list.append('MKL')
        
    if not conf.options.with_superlu:
        conf.skip_list.append('SUPERLU')

    conf.start_msg('Compilation options')
    conf.end_msg(conf.env.CXXFLAGS)
